package i.c.a.a;

import java.util.concurrent.Executor;

public class ArchTaskExecutor extends TaskExecutor {
    public static volatile ArchTaskExecutor c;
    public static final Executor d = new a();
    public TaskExecutor a;
    public TaskExecutor b;

    public static class a implements Executor {
        public void execute(Runnable runnable) {
            ArchTaskExecutor.b().a.a(runnable);
        }
    }

    public ArchTaskExecutor() {
        DefaultTaskExecutor defaultTaskExecutor = new DefaultTaskExecutor();
        this.b = super;
        this.a = super;
    }

    public static ArchTaskExecutor b() {
        if (c != null) {
            return c;
        }
        synchronized (ArchTaskExecutor.class) {
            if (c == null) {
                c = new ArchTaskExecutor();
            }
        }
        return c;
    }

    public void a(Runnable runnable) {
        this.a.a(runnable);
    }

    public boolean a() {
        return this.a.a();
    }
}
