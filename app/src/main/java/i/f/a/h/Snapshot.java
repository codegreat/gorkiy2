package i.f.a.h;

import i.f.a.h.ConstraintAnchor;
import i.f.a.h.m;
import java.util.ArrayList;

public class Snapshot {
    public int a;
    public int b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public ArrayList<m.a> f1135e = new ArrayList<>();

    public static class a {
        public ConstraintAnchor a;
        public ConstraintAnchor b;
        public int c;
        public ConstraintAnchor.b d;

        /* renamed from: e  reason: collision with root package name */
        public int f1136e;

        public a(ConstraintAnchor constraintAnchor) {
            this.a = constraintAnchor;
            this.b = constraintAnchor.d;
            this.c = constraintAnchor.a();
            this.d = constraintAnchor.g;
            this.f1136e = constraintAnchor.h;
        }
    }

    public Snapshot(ConstraintWidget constraintWidget) {
        this.a = constraintWidget.I;
        this.b = constraintWidget.J;
        this.c = constraintWidget.i();
        this.d = constraintWidget.d();
        ArrayList<c> b2 = constraintWidget.b();
        int size = b2.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f1135e.add(new a(b2.get(i2)));
        }
    }
}
