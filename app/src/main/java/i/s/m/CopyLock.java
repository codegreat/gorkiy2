package i.s.m;

import j.a.a.a.outline;
import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CopyLock {

    /* renamed from: e  reason: collision with root package name */
    public static final Map<String, Lock> f1440e = new HashMap();
    public final File a;
    public final Lock b;
    public final boolean c;
    public FileChannel d;

    public CopyLock(String str, File file, boolean z) {
        File file2 = new File(file, outline.a(str, ".lck"));
        this.a = file2;
        this.b = a(file2.getAbsolutePath());
        this.c = z;
    }

    public void a() {
        FileChannel fileChannel = this.d;
        if (fileChannel != null) {
            try {
                fileChannel.close();
            } catch (IOException unused) {
            }
        }
        this.b.unlock();
    }

    public static Lock a(String str) {
        Lock lock;
        synchronized (f1440e) {
            lock = f1440e.get(str);
            if (lock == null) {
                lock = new ReentrantLock();
                f1440e.put(str, lock);
            }
        }
        return lock;
    }
}
