package i.s;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import i.s.IMultiInstanceInvalidationCallback;
import i.s.IMultiInstanceInvalidationService;
import i.s.InvalidationTracker;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

public class MultiInstanceInvalidationClient {
    public final Context a;
    public final String b;
    public int c;
    public final InvalidationTracker d;

    /* renamed from: e  reason: collision with root package name */
    public final InvalidationTracker.c f1428e;

    /* renamed from: f  reason: collision with root package name */
    public IMultiInstanceInvalidationService f1429f;
    public final Executor g;
    public final IMultiInstanceInvalidationCallback h = new a();

    /* renamed from: i  reason: collision with root package name */
    public final AtomicBoolean f1430i = new AtomicBoolean(false);

    /* renamed from: j  reason: collision with root package name */
    public final ServiceConnection f1431j = new b();

    /* renamed from: k  reason: collision with root package name */
    public final Runnable f1432k = new c();

    /* renamed from: l  reason: collision with root package name */
    public final Runnable f1433l = new d();

    public class a extends IMultiInstanceInvalidationCallback.a {

        /* renamed from: i.s.MultiInstanceInvalidationClient$a$a  reason: collision with other inner class name */
        public class C0008a implements Runnable {
            public final /* synthetic */ String[] b;

            public C0008a(String[] strArr) {
                this.b = strArr;
            }

            public void run() {
                MultiInstanceInvalidationClient.this.d.a(this.b);
            }
        }

        public a() {
        }

        public void a(String[] strArr) {
            MultiInstanceInvalidationClient.this.g.execute(new C0008a(strArr));
        }
    }

    public class b implements ServiceConnection {
        public b() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MultiInstanceInvalidationClient.this.f1429f = IMultiInstanceInvalidationService.a.a(iBinder);
            MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient.g.execute(multiInstanceInvalidationClient.f1432k);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient.g.execute(multiInstanceInvalidationClient.f1433l);
            MultiInstanceInvalidationClient.this.f1429f = null;
        }
    }

    public class c implements Runnable {
        public c() {
        }

        public void run() {
            try {
                IMultiInstanceInvalidationService iMultiInstanceInvalidationService = MultiInstanceInvalidationClient.this.f1429f;
                if (iMultiInstanceInvalidationService != null) {
                    MultiInstanceInvalidationClient.this.c = iMultiInstanceInvalidationService.a(MultiInstanceInvalidationClient.this.h, MultiInstanceInvalidationClient.this.b);
                    MultiInstanceInvalidationClient.this.d.a(MultiInstanceInvalidationClient.this.f1428e);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e2);
            }
        }
    }

    public class d implements Runnable {
        public d() {
        }

        public void run() {
            MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient.d.b(multiInstanceInvalidationClient.f1428e);
        }
    }

    public class e extends InvalidationTracker.c {
        public e(String[] strArr) {
            super(strArr);
        }

        public void a(Set<String> set) {
            if (!MultiInstanceInvalidationClient.this.f1430i.get()) {
                try {
                    IMultiInstanceInvalidationService iMultiInstanceInvalidationService = MultiInstanceInvalidationClient.this.f1429f;
                    if (iMultiInstanceInvalidationService != null) {
                        iMultiInstanceInvalidationService.a(MultiInstanceInvalidationClient.this.c, (String[]) set.toArray(new String[0]));
                    }
                } catch (RemoteException e2) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e2);
                }
            }
        }
    }

    public MultiInstanceInvalidationClient(Context context, String str, InvalidationTracker invalidationTracker, Executor executor) {
        this.a = context.getApplicationContext();
        this.b = str;
        this.d = invalidationTracker;
        this.g = executor;
        this.f1428e = new e(invalidationTracker.b);
        this.a.bindService(new Intent(this.a, MultiInstanceInvalidationService.class), this.f1431j, 1);
    }
}
