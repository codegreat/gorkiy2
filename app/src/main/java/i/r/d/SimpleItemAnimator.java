package i.r.d;

import androidx.recyclerview.widget.RecyclerView;
import i.r.d.DefaultItemAnimator7;

public abstract class SimpleItemAnimator extends RecyclerView.l {
    public boolean g = true;

    public abstract boolean a(RecyclerView.d0 d0Var, int i2, int i3, int i4, int i5);

    public boolean a(RecyclerView.d0 d0Var, RecyclerView.d0 d0Var2, RecyclerView.l.c cVar, RecyclerView.l.c cVar2) {
        int i2;
        int i3;
        int i4 = cVar.a;
        int i5 = cVar.b;
        if (d0Var2.o()) {
            int i6 = cVar.a;
            i2 = cVar.b;
            i3 = i6;
        } else {
            i3 = cVar2.a;
            i2 = cVar2.b;
        }
        DefaultItemAnimator7 defaultItemAnimator7 = (DefaultItemAnimator7) this;
        if (d0Var == d0Var2) {
            return defaultItemAnimator7.a(d0Var, i4, i5, i3, i2);
        }
        float translationX = d0Var.a.getTranslationX();
        float translationY = d0Var.a.getTranslationY();
        float alpha = d0Var.a.getAlpha();
        defaultItemAnimator7.e(d0Var);
        d0Var.a.setTranslationX(translationX);
        d0Var.a.setTranslationY(translationY);
        d0Var.a.setAlpha(alpha);
        defaultItemAnimator7.e(d0Var2);
        d0Var2.a.setTranslationX((float) (-((int) (((float) (i3 - i4)) - translationX))));
        d0Var2.a.setTranslationY((float) (-((int) (((float) (i2 - i5)) - translationY))));
        d0Var2.a.setAlpha(0.0f);
        defaultItemAnimator7.f1371k.add(new DefaultItemAnimator7.a(d0Var, d0Var2, i4, i5, i3, i2));
        return true;
    }
}
