package i.u.a;

import android.content.SharedPreferences;
import android.util.ArraySet;
import android.util.Pair;
import j.a.a.a.outline;
import j.c.b.a.Aead;
import j.c.b.a.DeterministicAead;
import j.c.b.a.c0.Base64;
import j.c.b.a.t.AeadKeyTemplates;
import j.c.b.a.v.DeterministicAeadKeyTemplates;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.z1;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public final class EncryptedSharedPreferences implements SharedPreferences {
    public final SharedPreferences a;
    public final List<SharedPreferences.OnSharedPreferenceChangeListener> b = new ArrayList();
    public final String c;
    public final Aead d;

    /* renamed from: e  reason: collision with root package name */
    public final DeterministicAead f1445e;

    public enum b {
        STRING(0),
        STRING_SET(1),
        INT(2),
        LONG(3),
        FLOAT(4),
        BOOLEAN(5);
        
        public int mId;

        /* access modifiers changed from: public */
        b(int i2) {
            this.mId = i2;
        }

        public static b a(int i2) {
            if (i2 == 0) {
                return STRING;
            }
            if (i2 == 1) {
                return STRING_SET;
            }
            if (i2 == 2) {
                return INT;
            }
            if (i2 == 3) {
                return LONG;
            }
            if (i2 == 4) {
                return FLOAT;
            }
            if (i2 != 5) {
                return null;
            }
            return BOOLEAN;
        }
    }

    public enum c {
        AES256_SIV(DeterministicAeadKeyTemplates.a);
        
        public KeyTemplate mDeterministicAeadKeyTemplate;

        /* access modifiers changed from: public */
        c(z1 z1Var) {
            this.mDeterministicAeadKeyTemplate = z1Var;
        }
    }

    public enum d {
        AES256_GCM(AeadKeyTemplates.a);
        
        public KeyTemplate mAeadKeyTemplate;

        /* access modifiers changed from: public */
        d(z1 z1Var) {
            this.mAeadKeyTemplate = z1Var;
        }
    }

    public EncryptedSharedPreferences(String str, String str2, SharedPreferences sharedPreferences, Aead aead, DeterministicAead deterministicAead) {
        this.c = str;
        this.a = sharedPreferences;
        this.d = aead;
        this.f1445e = deterministicAead;
    }

    public String a(String str) {
        if (str == null) {
            str = "__NULL__";
        }
        try {
            return Base64.a(this.f1445e.a(str.getBytes(StandardCharsets.UTF_8), this.c.getBytes()));
        } catch (GeneralSecurityException e2) {
            StringBuilder a2 = outline.a("Could not encrypt key. ");
            a2.append(e2.getMessage());
            throw new SecurityException(a2.toString(), e2);
        }
    }

    public final Object b(String str) {
        if (!c(str)) {
            if (str == null) {
                str = "__NULL__";
            }
            try {
                String a2 = a(str);
                String string = this.a.getString(a2, null);
                if (string == null) {
                    return null;
                }
                boolean z = false;
                ByteBuffer wrap = ByteBuffer.wrap(this.d.b(Base64.a(string, 0), a2.getBytes(StandardCharsets.UTF_8)));
                wrap.position(0);
                int ordinal = b.a(wrap.getInt()).ordinal();
                if (ordinal == 0) {
                    int i2 = wrap.getInt();
                    ByteBuffer slice = wrap.slice();
                    wrap.limit(i2);
                    String charBuffer = StandardCharsets.UTF_8.decode(slice).toString();
                    if (charBuffer.equals("__NULL__")) {
                        return null;
                    }
                    return charBuffer;
                } else if (ordinal == 1) {
                    ArraySet arraySet = new ArraySet();
                    while (wrap.hasRemaining()) {
                        int i3 = wrap.getInt();
                        ByteBuffer slice2 = wrap.slice();
                        slice2.limit(i3);
                        wrap.position(wrap.position() + i3);
                        arraySet.add(StandardCharsets.UTF_8.decode(slice2).toString());
                    }
                    if (arraySet.size() != 1 || !"__NULL__".equals(arraySet.valueAt(0))) {
                        return arraySet;
                    }
                    return null;
                } else if (ordinal == 2) {
                    return Integer.valueOf(wrap.getInt());
                } else {
                    if (ordinal == 3) {
                        return Long.valueOf(wrap.getLong());
                    }
                    if (ordinal == 4) {
                        return Float.valueOf(wrap.getFloat());
                    }
                    if (ordinal != 5) {
                        return null;
                    }
                    if (wrap.get() != 0) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                }
            } catch (GeneralSecurityException e2) {
                StringBuilder a3 = outline.a("Could not decrypt value. ");
                a3.append(e2.getMessage());
                throw new SecurityException(a3.toString(), e2);
            }
        } else {
            throw new SecurityException(outline.a(str, " is a reserved key for the encryption keyset."));
        }
    }

    public boolean c(String str) {
        return "__androidx_security_crypto_encrypted_prefs_key_keyset__".equals(str) || "__androidx_security_crypto_encrypted_prefs_value_keyset__".equals(str);
    }

    public boolean contains(String str) {
        if (!c(str)) {
            return this.a.contains(a(str));
        }
        throw new SecurityException(outline.a(str, " is a reserved key for the encryption keyset."));
    }

    public SharedPreferences.Editor edit() {
        return new a(this, this.a.edit());
    }

    public Map<String, ?> getAll() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.a.getAll().entrySet()) {
            if (!c((String) next.getKey())) {
                try {
                    String str = new String(this.f1445e.b(Base64.a((String) next.getKey(), 0), this.c.getBytes()), StandardCharsets.UTF_8);
                    if (str.equals("__NULL__")) {
                        str = null;
                    }
                    hashMap.put(str, b(str));
                } catch (GeneralSecurityException e2) {
                    StringBuilder a2 = outline.a("Could not decrypt key. ");
                    a2.append(e2.getMessage());
                    throw new SecurityException(a2.toString(), e2);
                }
            }
        }
        return hashMap;
    }

    public boolean getBoolean(String str, boolean z) {
        Object b2 = b(str);
        return (b2 == null || !(b2 instanceof Boolean)) ? z : ((Boolean) b2).booleanValue();
    }

    public float getFloat(String str, float f2) {
        Object b2 = b(str);
        return (b2 == null || !(b2 instanceof Float)) ? f2 : ((Float) b2).floatValue();
    }

    public int getInt(String str, int i2) {
        Object b2 = b(str);
        return (b2 == null || !(b2 instanceof Integer)) ? i2 : ((Integer) b2).intValue();
    }

    public long getLong(String str, long j2) {
        Object b2 = b(str);
        return (b2 == null || !(b2 instanceof Long)) ? j2 : ((Long) b2).longValue();
    }

    public String getString(String str, String str2) {
        Object b2 = b(str);
        return (b2 == null || !(b2 instanceof String)) ? str2 : (String) b2;
    }

    public Set<String> getStringSet(String str, Set<String> set) {
        Set<String> set2;
        Object b2 = b(str);
        if (b2 instanceof Set) {
            set2 = (Set) b2;
        } else {
            set2 = new ArraySet<>();
        }
        return set2.size() > 0 ? set2 : set;
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.b.add(onSharedPreferenceChangeListener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.b.remove(onSharedPreferenceChangeListener);
    }

    public static final class a implements SharedPreferences.Editor {
        public final EncryptedSharedPreferences a;
        public final SharedPreferences.Editor b;
        public final List<String> c;
        public AtomicBoolean d = new AtomicBoolean(false);

        public a(EncryptedSharedPreferences encryptedSharedPreferences, SharedPreferences.Editor editor) {
            this.a = encryptedSharedPreferences;
            this.b = editor;
            this.c = new CopyOnWriteArrayList();
        }

        public final void a(String str, byte[] bArr) {
            if (!this.a.c(str)) {
                this.c.add(str);
                if (str == null) {
                    str = "__NULL__";
                }
                try {
                    EncryptedSharedPreferences encryptedSharedPreferences = this.a;
                    String a2 = encryptedSharedPreferences.a(str);
                    Pair pair = new Pair(a2, Base64.a(encryptedSharedPreferences.d.a(bArr, a2.getBytes(StandardCharsets.UTF_8))));
                    this.b.putString((String) pair.first, (String) pair.second);
                } catch (GeneralSecurityException e2) {
                    StringBuilder a3 = outline.a("Could not encrypt data: ");
                    a3.append(e2.getMessage());
                    throw new SecurityException(a3.toString(), e2);
                }
            } else {
                throw new SecurityException(outline.a(str, " is a reserved key for the encryption keyset."));
            }
        }

        public void apply() {
            this.b.apply();
            a();
        }

        public SharedPreferences.Editor clear() {
            this.d.set(true);
            return this;
        }

        public boolean commit() {
            if (this.d.getAndSet(false)) {
                for (String str : ((HashMap) this.a.getAll()).keySet()) {
                    if (!this.c.contains(str) && !this.a.c(str)) {
                        this.b.remove(this.a.a(str));
                    }
                }
            }
            try {
                return this.b.commit();
            } finally {
                a();
                this.c.clear();
            }
        }

        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            ByteBuffer allocate = ByteBuffer.allocate(5);
            allocate.putInt(b.BOOLEAN.mId);
            allocate.put(z ? (byte) 1 : 0);
            a(str, allocate.array());
            return this;
        }

        public SharedPreferences.Editor putFloat(String str, float f2) {
            ByteBuffer allocate = ByteBuffer.allocate(8);
            allocate.putInt(b.FLOAT.mId);
            allocate.putFloat(f2);
            a(str, allocate.array());
            return this;
        }

        public SharedPreferences.Editor putInt(String str, int i2) {
            ByteBuffer allocate = ByteBuffer.allocate(8);
            allocate.putInt(b.INT.mId);
            allocate.putInt(i2);
            a(str, allocate.array());
            return this;
        }

        public SharedPreferences.Editor putLong(String str, long j2) {
            ByteBuffer allocate = ByteBuffer.allocate(12);
            allocate.putInt(b.LONG.mId);
            allocate.putLong(j2);
            a(str, allocate.array());
            return this;
        }

        public SharedPreferences.Editor putString(String str, String str2) {
            if (str2 == null) {
                str2 = "__NULL__";
            }
            byte[] bytes = str2.getBytes(StandardCharsets.UTF_8);
            int length = bytes.length;
            ByteBuffer allocate = ByteBuffer.allocate(length + 8);
            allocate.putInt(b.STRING.mId);
            allocate.putInt(length);
            allocate.put(bytes);
            a(str, allocate.array());
            return this;
        }

        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            if (set == null) {
                set = new ArraySet<>();
                set.add("__NULL__");
            }
            ArrayList arrayList = new ArrayList(set.size());
            int size = set.size() * 4;
            for (String bytes : set) {
                byte[] bytes2 = bytes.getBytes(StandardCharsets.UTF_8);
                arrayList.add(bytes2);
                size += bytes2.length;
            }
            ByteBuffer allocate = ByteBuffer.allocate(size + 4);
            allocate.putInt(b.STRING_SET.mId);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                byte[] bArr = (byte[]) it.next();
                allocate.putInt(bArr.length);
                allocate.put(bArr);
            }
            a(str, allocate.array());
            return this;
        }

        public SharedPreferences.Editor remove(String str) {
            if (!this.a.c(str)) {
                this.b.remove(this.a.a(str));
                this.c.remove(str);
                return this;
            }
            throw new SecurityException(outline.a(str, " is a reserved key for the encryption keyset."));
        }

        public final void a() {
            for (SharedPreferences.OnSharedPreferenceChangeListener next : this.a.b) {
                for (String onSharedPreferenceChanged : this.c) {
                    next.onSharedPreferenceChanged(this.a, onSharedPreferenceChanged);
                }
            }
        }
    }
}
