package i.t;

import android.annotation.SuppressLint;
import android.os.Bundle;
import i.c.a.b.SafeIterableMap;
import i.t.a;

@SuppressLint({"RestrictedApi"})
public final class SavedStateRegistry {
    public SafeIterableMap<String, a.b> a = new SafeIterableMap<>();
    public Bundle b;
    public boolean c;
    public boolean d;

    public interface a {
        void a(SavedStateRegistryOwner savedStateRegistryOwner);
    }

    public interface b {
        Bundle a();
    }
}
