package i.l.a;

import android.view.View;

public class Fragment extends FragmentContainer {
    public final /* synthetic */ androidx.fragment.app.Fragment a;

    public Fragment(androidx.fragment.app.Fragment fragment) {
        this.a = fragment;
    }

    public View a(int i2) {
        View view = this.a.H;
        if (view != null) {
            return view.findViewById(i2);
        }
        throw new IllegalStateException("Fragment " + this + " does not have a view");
    }

    public boolean b() {
        return this.a.H != null;
    }
}
