package i.l.a;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.fragment.app.Fragment;

/* compiled from: FragmentManagerImpl */
public class FragmentManagerImpl0 implements Animation.AnimationListener {
    public final /* synthetic */ ViewGroup a;
    public final /* synthetic */ Fragment b;
    public final /* synthetic */ FragmentManagerImpl c;

    /* compiled from: FragmentManagerImpl */
    public class a implements Runnable {
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
         arg types: [androidx.fragment.app.Fragment, int, int, int, int]
         candidates:
          i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
          i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
        public void run() {
            if (FragmentManagerImpl0.this.b.m() != null) {
                FragmentManagerImpl0.this.b.a((View) null);
                FragmentManagerImpl0 fragmentManagerImpl0 = FragmentManagerImpl0.this;
                FragmentManagerImpl fragmentManagerImpl = fragmentManagerImpl0.c;
                Fragment fragment = fragmentManagerImpl0.b;
                fragmentManagerImpl.a(fragment, fragment.x(), 0, 0, false);
            }
        }
    }

    public FragmentManagerImpl0(FragmentManagerImpl fragmentManagerImpl, ViewGroup viewGroup, Fragment fragment) {
        this.c = fragmentManagerImpl;
        this.a = viewGroup;
        this.b = fragment;
    }

    public void onAnimationEnd(Animation animation) {
        this.a.post(new a());
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
