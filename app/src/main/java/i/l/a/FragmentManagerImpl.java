package i.l.a;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import i.a.OnBackPressedCallback;
import i.b.k.ResourcesFlusher;
import i.e.ArraySet;
import i.h.k.LogWriter;
import i.h.l.OneShotPreDrawListener;
import i.l.a.FragmentTransaction;
import i.l.a.k;
import i.o.Lifecycle;
import i.p.a.LoaderManager;
import j.a.a.a.outline;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class FragmentManagerImpl extends FragmentManager implements LayoutInflater.Factory2 {
    public static boolean H;
    public static final Interpolator I = new DecelerateInterpolator(2.5f);
    public static final Interpolator J = new DecelerateInterpolator(1.5f);
    public ArrayList<Boolean> A;
    public ArrayList<Fragment> B;
    public Bundle C = null;
    public SparseArray<Parcelable> D = null;
    public ArrayList<k.j> E;
    public FragmentManagerViewModel F;
    public Runnable G = new b();
    public ArrayList<k.h> d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1295e;

    /* renamed from: f  reason: collision with root package name */
    public int f1296f = 0;
    public final ArrayList<Fragment> g = new ArrayList<>();
    public final HashMap<String, Fragment> h = new HashMap<>();

    /* renamed from: i  reason: collision with root package name */
    public ArrayList<a> f1297i;

    /* renamed from: j  reason: collision with root package name */
    public ArrayList<Fragment> f1298j;

    /* renamed from: k  reason: collision with root package name */
    public OnBackPressedDispatcher f1299k;

    /* renamed from: l  reason: collision with root package name */
    public final OnBackPressedCallback f1300l = new a(false);

    /* renamed from: m  reason: collision with root package name */
    public ArrayList<a> f1301m;

    /* renamed from: n  reason: collision with root package name */
    public ArrayList<Integer> f1302n;

    /* renamed from: o  reason: collision with root package name */
    public final CopyOnWriteArrayList<k.f> f1303o = new CopyOnWriteArrayList<>();

    /* renamed from: p  reason: collision with root package name */
    public int f1304p = 0;

    /* renamed from: q  reason: collision with root package name */
    public FragmentHostCallback f1305q;

    /* renamed from: r  reason: collision with root package name */
    public FragmentContainer f1306r;

    /* renamed from: s  reason: collision with root package name */
    public Fragment f1307s;

    /* renamed from: t  reason: collision with root package name */
    public Fragment f1308t;
    public boolean u;
    public boolean v;
    public boolean w;
    public boolean x;
    public boolean y;
    public ArrayList<a> z;

    public class a extends OnBackPressedCallback {
        public a(boolean z) {
            super(z);
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            FragmentManagerImpl.this.j();
        }
    }

    public class c extends FragmentFactory {
        public c() {
        }

        public Fragment a(ClassLoader classLoader, String str) {
            FragmentHostCallback fragmentHostCallback = FragmentManagerImpl.this.f1305q;
            Context context = fragmentHostCallback.c;
            if (fragmentHostCallback != null) {
                return Fragment.a(context, str, (Bundle) null);
            }
            throw null;
        }
    }

    public static final class f {
        public final boolean a;
    }

    public static class g {
        public static final int[] a = {16842755, 16842960, 16842961};
    }

    public interface h {
        boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2);
    }

    public class i implements h {
        public final String a;
        public final int b;
        public final int c;

        public i(String str, int i2, int i3) {
            this.a = str;
            this.b = i2;
            this.c = i3;
        }

        public boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
            Fragment fragment = FragmentManagerImpl.this.f1308t;
            if (fragment != null && this.b < 0 && this.a == null && fragment.o().b()) {
                return false;
            }
            return FragmentManagerImpl.this.a(arrayList, arrayList2, this.a, this.b, this.c);
        }
    }

    public static class j implements Fragment.c {
        public final boolean a;
        public final BackStackRecord b;
        public int c;

        public j(BackStackRecord backStackRecord, boolean z) {
            this.a = z;
            this.b = backStackRecord;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void
         arg types: [i.l.a.BackStackRecord, boolean, boolean, int]
         candidates:
          i.l.a.FragmentManagerImpl.a(float, float, float, float):i.l.a.FragmentManagerImpl$d
          i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d
          i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
          i.l.a.FragmentManagerImpl.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
          i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
          i.l.a.FragmentManager.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
          i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void */
        public void a() {
            boolean z;
            boolean z2 = this.c > 0;
            FragmentManagerImpl fragmentManagerImpl = this.b.f1275s;
            int size = fragmentManagerImpl.g.size();
            for (int i2 = 0; i2 < size; i2++) {
                Fragment fragment = fragmentManagerImpl.g.get(i2);
                fragment.a((Fragment.c) null);
                if (z2) {
                    Fragment.b bVar = fragment.L;
                    if (bVar == null) {
                        z = false;
                    } else {
                        z = bVar.f244q;
                    }
                    if (z) {
                        fragment.M();
                    }
                }
            }
            BackStackRecord backStackRecord = this.b;
            backStackRecord.f1275s.a(backStackRecord, this.a, !z2, true);
        }
    }

    public static int d(int i2) {
        if (i2 == 4097) {
            return 8194;
        }
        if (i2 != 4099) {
            return i2 != 8194 ? 0 : 4097;
        }
        return 4099;
    }

    public final void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
        FragmentHostCallback fragmentHostCallback = this.f1305q;
        if (fragmentHostCallback != null) {
            try {
                FragmentActivity.this.dump("  ", null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    public boolean b() {
        d();
        j();
        c(true);
        Fragment fragment = this.f1308t;
        if (fragment != null && fragment.o().b()) {
            return true;
        }
        boolean a2 = a(this.z, this.A, (String) null, -1, 0);
        if (a2) {
            this.f1295e = true;
            try {
                c(this.z, this.A);
            } finally {
                e();
            }
        }
        r();
        i();
        c();
        return a2;
    }

    public void c(int i2) {
        synchronized (this) {
            this.f1301m.set(i2, null);
            if (this.f1302n == null) {
                this.f1302n = new ArrayList<>();
            }
            this.f1302n.add(Integer.valueOf(i2));
        }
    }

    public final void d() {
        if (m()) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    public boolean e(Fragment fragment) {
        if (fragment == null) {
            return true;
        }
        FragmentManagerImpl fragmentManagerImpl = fragment.f232s;
        if (fragment != fragmentManagerImpl.f1308t || !e(fragmentManagerImpl.f1307s)) {
            return false;
        }
        return true;
    }

    public void f(Fragment fragment) {
        if (this.h.get(fragment.f221f) == null) {
            this.h.put(fragment.f221f, fragment);
            if (fragment.C) {
                if (fragment.B) {
                    if (!m()) {
                        this.F.b.add(fragment);
                    }
                } else if (!m()) {
                    this.F.b.remove(fragment);
                }
                fragment.C = false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d
     arg types: [androidx.fragment.app.Fragment, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(float, float, float, float):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void
      i.l.a.FragmentManagerImpl.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      i.l.a.FragmentManager.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d */
    public void g(Fragment fragment) {
        Animator animator;
        ViewGroup viewGroup;
        int indexOfChild;
        int indexOfChild2;
        if (fragment != null && this.h.containsKey(fragment.f221f)) {
            int i2 = this.f1304p;
            if (fragment.f226m) {
                if (fragment.A()) {
                    i2 = Math.min(i2, 1);
                } else {
                    i2 = Math.min(i2, 0);
                }
            }
            a(fragment, i2, fragment.t(), fragment.u(), false);
            View view = fragment.H;
            if (view != null) {
                ViewGroup viewGroup2 = fragment.G;
                Fragment fragment2 = null;
                if (viewGroup2 != null && view != null) {
                    int indexOf = this.g.indexOf(fragment);
                    while (true) {
                        indexOf--;
                        if (indexOf < 0) {
                            break;
                        }
                        Fragment fragment3 = this.g.get(indexOf);
                        if (fragment3.G == viewGroup2 && fragment3.H != null) {
                            fragment2 = fragment3;
                            break;
                        }
                    }
                }
                if (fragment2 != null && (indexOfChild2 = viewGroup.indexOfChild(fragment.H)) < (indexOfChild = (viewGroup = fragment.G).indexOfChild(fragment2.H))) {
                    viewGroup.removeViewAt(indexOfChild2);
                    viewGroup.addView(fragment.H, indexOfChild);
                }
                if (fragment.M && fragment.G != null) {
                    float f2 = fragment.O;
                    if (f2 > 0.0f) {
                        fragment.H.setAlpha(f2);
                    }
                    fragment.O = 0.0f;
                    fragment.M = false;
                    d a2 = a(fragment, fragment.t(), true, fragment.u());
                    if (a2 != null) {
                        Animation animation = a2.a;
                        if (animation != null) {
                            fragment.H.startAnimation(animation);
                        } else {
                            a2.b.setTarget(fragment.H);
                            a2.b.start();
                        }
                    }
                }
            }
            if (fragment.N) {
                if (fragment.H != null) {
                    d a3 = a(fragment, fragment.t(), !fragment.z, fragment.u());
                    if (a3 == null || (animator = a3.b) == null) {
                        if (a3 != null) {
                            fragment.H.startAnimation(a3.a);
                            a3.a.start();
                        }
                        fragment.H.setVisibility((!fragment.z || fragment.z()) ? 0 : 8);
                        if (fragment.z()) {
                            fragment.a(false);
                        }
                    } else {
                        animator.setTarget(fragment.H);
                        if (!fragment.z) {
                            fragment.H.setVisibility(0);
                        } else if (fragment.z()) {
                            fragment.a(false);
                        } else {
                            ViewGroup viewGroup3 = fragment.G;
                            View view2 = fragment.H;
                            viewGroup3.startViewTransition(view2);
                            a3.b.addListener(new FragmentManagerImpl2(this, viewGroup3, view2, fragment));
                        }
                        a3.b.start();
                    }
                }
                if (fragment.f225l && d(fragment)) {
                    this.u = true;
                }
                fragment.N = false;
            }
        }
    }

    public void h(Fragment fragment) {
        boolean z2 = !fragment.A();
        if (!fragment.A || z2) {
            synchronized (this.g) {
                this.g.remove(fragment);
            }
            if (d(fragment)) {
                this.u = true;
            }
            fragment.f225l = false;
            fragment.f226m = true;
        }
    }

    public void i() {
        if (this.y) {
            this.y = false;
            q();
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean j() {
        c(true);
        boolean z2 = false;
        while (b(this.z, this.A)) {
            this.f1295e = true;
            try {
                c(this.z, this.A);
                e();
                z2 = true;
            } catch (Throwable th) {
                e();
                throw th;
            }
        }
        r();
        i();
        c();
        return z2;
    }

    public final void k() {
        if (this.E != null) {
            while (!this.E.isEmpty()) {
                this.E.remove(0).a();
            }
        }
    }

    public FragmentFactory l() {
        if (super.b == null) {
            super.b = FragmentManager.c;
        }
        if (super.b == FragmentManager.c) {
            Fragment fragment = this.f1307s;
            if (fragment != null) {
                return fragment.f232s.l();
            }
            super.b = new c();
        }
        if (super.b == null) {
            super.b = FragmentManager.c;
        }
        return super.b;
    }

    public boolean m() {
        return this.v || this.w;
    }

    public void n() {
        this.v = false;
        this.w = false;
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null) {
                fragment.u.n();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public Parcelable o() {
        BackStackState[] backStackStateArr;
        ArrayList<String> arrayList;
        int size;
        k();
        Iterator<Fragment> it = this.h.values().iterator();
        while (true) {
            backStackStateArr = null;
            if (!it.hasNext()) {
                break;
            }
            Fragment next = it.next();
            if (next != null) {
                if (next.m() != null) {
                    int x2 = next.x();
                    View m2 = next.m();
                    Animation animation = m2.getAnimation();
                    if (animation != null) {
                        animation.cancel();
                        m2.clearAnimation();
                    }
                    next.a((View) null);
                    a(next, x2, 0, 0, false);
                } else if (next.n() != null) {
                    next.n().end();
                }
            }
        }
        j();
        this.v = true;
        if (this.h.isEmpty()) {
            return null;
        }
        ArrayList<q> arrayList2 = new ArrayList<>(this.h.size());
        boolean z2 = false;
        for (Fragment next2 : this.h.values()) {
            if (next2 != null) {
                if (next2.f232s == this) {
                    FragmentState fragmentState = new FragmentState(next2);
                    arrayList2.add(fragmentState);
                    if (next2.b <= 0 || fragmentState.f1322n != null) {
                        fragmentState.f1322n = next2.c;
                    } else {
                        fragmentState.f1322n = i(next2);
                        String str = next2.f222i;
                        if (str != null) {
                            Fragment fragment = this.h.get(str);
                            if (fragment != null) {
                                if (fragmentState.f1322n == null) {
                                    fragmentState.f1322n = new Bundle();
                                }
                                Bundle bundle = fragmentState.f1322n;
                                if (fragment.f232s == this) {
                                    bundle.putString("android:target_state", fragment.f221f);
                                    int i2 = next2.f223j;
                                    if (i2 != 0) {
                                        fragmentState.f1322n.putInt("android:target_req_state", i2);
                                    }
                                } else {
                                    a(new IllegalStateException(outline.a("Fragment ", fragment, " is not currently in the FragmentManager")));
                                    throw null;
                                }
                            } else {
                                a(new IllegalStateException("Failure saving state: " + next2 + " has target not in fragment manager: " + next2.f222i));
                                throw null;
                            }
                        }
                    }
                    z2 = true;
                } else {
                    a(new IllegalStateException(outline.a("Failure saving state: active ", next2, " was removed from the FragmentManager")));
                    throw null;
                }
            }
        }
        if (!z2) {
            return null;
        }
        int size2 = this.g.size();
        if (size2 > 0) {
            arrayList = new ArrayList<>(size2);
            Iterator<Fragment> it2 = this.g.iterator();
            while (it2.hasNext()) {
                Fragment next3 = it2.next();
                arrayList.add(next3.f221f);
                if (next3.f232s != this) {
                    a(new IllegalStateException(outline.a("Failure saving state: active ", next3, " was removed from the FragmentManager")));
                    throw null;
                }
            }
        } else {
            arrayList = null;
        }
        ArrayList<a> arrayList3 = this.f1297i;
        if (arrayList3 != null && (size = arrayList3.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i3 = 0; i3 < size; i3++) {
                backStackStateArr[i3] = new BackStackState(this.f1297i.get(i3));
            }
        }
        FragmentManagerState fragmentManagerState = new FragmentManagerState();
        fragmentManagerState.b = arrayList2;
        fragmentManagerState.c = arrayList;
        fragmentManagerState.d = backStackStateArr;
        Fragment fragment2 = this.f1308t;
        if (fragment2 != null) {
            fragmentManagerState.f1311e = fragment2.f221f;
        }
        fragmentManagerState.f1312f = this.f1296f;
        return fragmentManagerState;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(int, boolean):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        AttributeSet attributeSet2 = attributeSet;
        Fragment fragment = null;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet2.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet2, g.a);
        int i2 = 0;
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        String str2 = attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (str2 == null || !FragmentFactory.b(context.getClassLoader(), str2)) {
            return null;
        }
        if (view != null) {
            i2 = view.getId();
        }
        if (i2 == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str2);
        }
        if (resourceId != -1) {
            fragment = a(resourceId);
        }
        if (fragment == null && string != null) {
            fragment = a(string);
        }
        if (fragment == null && i2 != -1) {
            fragment = a(i2);
        }
        if (fragment == null) {
            fragment = l().a(context.getClassLoader(), str2);
            fragment.f227n = true;
            fragment.w = resourceId != 0 ? resourceId : i2;
            fragment.x = i2;
            fragment.y = string;
            fragment.f228o = true;
            fragment.f232s = this;
            FragmentHostCallback fragmentHostCallback = this.f1305q;
            fragment.f233t = fragmentHostCallback;
            Context context2 = fragmentHostCallback.c;
            fragment.a(attributeSet2, fragment.c);
            a(fragment, true);
        } else if (!fragment.f228o) {
            fragment.f228o = true;
            FragmentHostCallback fragmentHostCallback2 = this.f1305q;
            fragment.f233t = fragmentHostCallback2;
            Context context3 = fragmentHostCallback2.c;
            fragment.a(attributeSet2, fragment.c);
        } else {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(i2) + " with another fragment for " + str2);
        }
        Fragment fragment2 = fragment;
        if (this.f1304p >= 1 || !fragment2.f227n) {
            a(fragment2, this.f1304p, 0, 0, false);
        } else {
            a(fragment2, 1, 0, 0, false);
        }
        View view2 = fragment2.H;
        if (view2 != null) {
            if (resourceId != 0) {
                view2.setId(resourceId);
            }
            if (fragment2.H.getTag() == null) {
                fragment2.H.setTag(string);
            }
            return fragment2.H;
        }
        throw new IllegalStateException(outline.a("Fragment ", str2, " did not create a view."));
    }

    public void p() {
        synchronized (this) {
            boolean z2 = false;
            boolean z3 = this.E != null && !this.E.isEmpty();
            if (this.d != null && this.d.size() == 1) {
                z2 = true;
            }
            if (z3 || z2) {
                this.f1305q.d.removeCallbacks(this.G);
                this.f1305q.d.post(this.G);
                r();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public void q() {
        for (Fragment next : this.h.values()) {
            if (next != null && next.J) {
                if (this.f1295e) {
                    this.y = true;
                } else {
                    next.J = false;
                    a(next, this.f1304p, 0, 0, false);
                }
            }
        }
    }

    public final void r() {
        ArrayList<k.h> arrayList = this.d;
        boolean z2 = true;
        if (arrayList == null || arrayList.isEmpty()) {
            OnBackPressedCallback onBackPressedCallback = this.f1300l;
            ArrayList<a> arrayList2 = this.f1297i;
            if ((arrayList2 != null ? arrayList2.size() : 0) <= 0 || !e(this.f1307s)) {
                z2 = false;
            }
            onBackPressedCallback.a = z2;
            return;
        }
        this.f1300l.a = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
     arg types: [androidx.fragment.app.Fragment, java.lang.StringBuilder]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
     arg types: [i.l.a.FragmentHostCallback, java.lang.StringBuilder]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void */
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        Fragment fragment = this.f1307s;
        if (fragment != null) {
            ResourcesFlusher.a((Object) fragment, sb);
        } else {
            ResourcesFlusher.a((Object) this.f1305q, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.d(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.d(androidx.fragment.app.Fragment, boolean):void");
    }

    public static class d {
        public final Animation a;
        public final Animator b;

        public d(Animation animation) {
            this.a = animation;
            this.b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }

        public d(Animator animator) {
            this.a = null;
            this.b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }
    }

    public Bundle i(Fragment fragment) {
        if (this.C == null) {
            this.C = new Bundle();
        }
        Bundle bundle = this.C;
        fragment.d(bundle);
        fragment.V.b(bundle);
        Parcelable o2 = fragment.u.o();
        if (o2 != null) {
            bundle.putParcelable("android:support:fragments", o2);
        }
        d(fragment, this.C, false);
        Bundle bundle2 = null;
        if (!this.C.isEmpty()) {
            Bundle bundle3 = this.C;
            this.C = null;
            bundle2 = bundle3;
        }
        if (fragment.H != null) {
            j(fragment);
        }
        if (fragment.d != null) {
            if (bundle2 == null) {
                bundle2 = new Bundle();
            }
            bundle2.putSparseParcelableArray("android:view_state", fragment.d);
        }
        if (!fragment.K) {
            if (bundle2 == null) {
                bundle2 = new Bundle();
            }
            bundle2.putBoolean("android:user_visible_hint", fragment.K);
        }
        return bundle2;
    }

    public void k(Fragment fragment) {
        if (fragment == null || (this.h.get(fragment.f221f) == fragment && (fragment.f233t == null || fragment.f232s == this))) {
            Fragment fragment2 = this.f1308t;
            this.f1308t = fragment;
            c(fragment2);
            c(this.f1308t);
            return;
        }
        throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
    }

    public final void e() {
        this.f1295e = false;
        this.A.clear();
        this.z.clear();
    }

    public static class e extends AnimationSet implements Runnable {
        public final ViewGroup b;
        public final View c;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f1309e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f1310f = true;

        public e(Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.b = viewGroup;
            this.c = view;
            addAnimation(animation);
            this.b.post(this);
        }

        public boolean getTransformation(long j2, Transformation transformation) {
            this.f1310f = true;
            if (this.d) {
                return !this.f1309e;
            }
            if (!super.getTransformation(j2, transformation)) {
                this.d = true;
                OneShotPreDrawListener.a(this.b, this);
            }
            return true;
        }

        public void run() {
            if (this.d || !this.f1310f) {
                this.b.endViewTransition(this.c);
                this.f1309e = true;
                return;
            }
            this.f1310f = false;
            this.b.post(this);
        }

        public boolean getTransformation(long j2, Transformation transformation, float f2) {
            this.f1310f = true;
            if (this.d) {
                return !this.f1309e;
            }
            if (!super.getTransformation(j2, transformation, f2)) {
                this.d = true;
                OneShotPreDrawListener.a(this.b, this);
            }
            return true;
        }
    }

    public final void c(boolean z2) {
        if (this.f1295e) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (this.f1305q == null) {
            throw new IllegalStateException("Fragment host has been destroyed");
        } else if (Looper.myLooper() == this.f1305q.d.getLooper()) {
            if (!z2) {
                d();
            }
            if (this.z == null) {
                this.z = new ArrayList<>();
                this.A = new ArrayList<>();
            }
            this.f1295e = true;
            try {
                a((ArrayList<a>) null, (ArrayList<Boolean>) null);
            } finally {
                this.f1295e = false;
            }
        } else {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.e(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.e(androidx.fragment.app.Fragment, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(androidx.fragment.app.Fragment r3, android.os.Bundle r4, boolean r5) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.d(r3, r4, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r5 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.d(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void");
    }

    public void j(Fragment fragment) {
        if (fragment.I != null) {
            SparseArray<Parcelable> sparseArray = this.D;
            if (sparseArray == null) {
                this.D = new SparseArray<>();
            } else {
                sparseArray.clear();
            }
            fragment.I.saveHierarchyState(this.D);
            if (this.D.size() > 0) {
                fragment.d = this.D;
                this.D = null;
            }
        }
    }

    public void h() {
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null) {
                fragment.F = true;
                fragment.u.h();
            }
        }
    }

    public List<Fragment> a() {
        List<Fragment> list;
        if (this.g.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.g) {
            list = (List) this.g.clone();
        }
        return list;
    }

    public void f() {
        this.v = false;
        this.w = false;
        b(1);
    }

    public void b(Fragment fragment) {
        if (!fragment.A) {
            fragment.A = true;
            if (fragment.f225l) {
                synchronized (this.g) {
                    this.g.remove(fragment);
                }
                if (d(fragment)) {
                    this.u = true;
                }
                fragment.f225l = false;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.f(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.f(androidx.fragment.app.Fragment, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.h(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.h(androidx.fragment.app.Fragment, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.BackStackRecord.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      i.l.a.BackStackRecord.a(java.util.ArrayList<i.l.a.a>, int, int):boolean
      i.l.a.BackStackRecord.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        String str2;
        String a2 = outline.a(str, "    ");
        if (!this.h.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (Fragment next : this.h.values()) {
                printWriter.print(str);
                printWriter.println(next);
                if (next != null) {
                    printWriter.print(a2);
                    printWriter.print("mFragmentId=#");
                    printWriter.print(Integer.toHexString(next.w));
                    printWriter.print(" mContainerId=#");
                    printWriter.print(Integer.toHexString(next.x));
                    printWriter.print(" mTag=");
                    printWriter.println(next.y);
                    printWriter.print(a2);
                    printWriter.print("mState=");
                    printWriter.print(next.b);
                    printWriter.print(" mWho=");
                    printWriter.print(next.f221f);
                    printWriter.print(" mBackStackNesting=");
                    printWriter.println(next.f231r);
                    printWriter.print(a2);
                    printWriter.print("mAdded=");
                    printWriter.print(next.f225l);
                    printWriter.print(" mRemoving=");
                    printWriter.print(next.f226m);
                    printWriter.print(" mFromLayout=");
                    printWriter.print(next.f227n);
                    printWriter.print(" mInLayout=");
                    printWriter.println(next.f228o);
                    printWriter.print(a2);
                    printWriter.print("mHidden=");
                    printWriter.print(next.z);
                    printWriter.print(" mDetached=");
                    printWriter.print(next.A);
                    printWriter.print(" mMenuVisible=");
                    printWriter.print(next.E);
                    printWriter.print(" mHasMenu=");
                    printWriter.println(next.D);
                    printWriter.print(a2);
                    printWriter.print("mRetainInstance=");
                    printWriter.print(next.B);
                    printWriter.print(" mUserVisibleHint=");
                    printWriter.println(next.K);
                    if (next.f232s != null) {
                        printWriter.print(a2);
                        printWriter.print("mFragmentManager=");
                        printWriter.println(next.f232s);
                    }
                    if (next.f233t != null) {
                        printWriter.print(a2);
                        printWriter.print("mHost=");
                        printWriter.println(next.f233t);
                    }
                    if (next.v != null) {
                        printWriter.print(a2);
                        printWriter.print("mParentFragment=");
                        printWriter.println(next.v);
                    }
                    if (next.g != null) {
                        printWriter.print(a2);
                        printWriter.print("mArguments=");
                        printWriter.println(next.g);
                    }
                    if (next.c != null) {
                        printWriter.print(a2);
                        printWriter.print("mSavedFragmentState=");
                        printWriter.println(next.c);
                    }
                    if (next.d != null) {
                        printWriter.print(a2);
                        printWriter.print("mSavedViewState=");
                        printWriter.println(next.d);
                    }
                    Fragment fragment = next.h;
                    if (fragment == null) {
                        FragmentManagerImpl fragmentManagerImpl = next.f232s;
                        fragment = (fragmentManagerImpl == null || (str2 = next.f222i) == null) ? null : fragmentManagerImpl.h.get(str2);
                    }
                    if (fragment != null) {
                        printWriter.print(a2);
                        printWriter.print("mTarget=");
                        printWriter.print(fragment);
                        printWriter.print(" mTargetRequestCode=");
                        printWriter.println(next.f223j);
                    }
                    if (next.s() != 0) {
                        printWriter.print(a2);
                        printWriter.print("mNextAnim=");
                        printWriter.println(next.s());
                    }
                    if (next.G != null) {
                        printWriter.print(a2);
                        printWriter.print("mContainer=");
                        printWriter.println(next.G);
                    }
                    if (next.H != null) {
                        printWriter.print(a2);
                        printWriter.print("mView=");
                        printWriter.println(next.H);
                    }
                    if (next.I != null) {
                        printWriter.print(a2);
                        printWriter.print("mInnerView=");
                        printWriter.println(next.H);
                    }
                    if (next.m() != null) {
                        printWriter.print(a2);
                        printWriter.print("mAnimatingAway=");
                        printWriter.println(next.m());
                        printWriter.print(a2);
                        printWriter.print("mStateAfterAnimating=");
                        printWriter.println(next.x());
                    }
                    if (next.p() != null) {
                        LoaderManager.a(next).a(a2, fileDescriptor, printWriter, strArr);
                    }
                    printWriter.print(a2);
                    printWriter.println("Child " + next.u + ":");
                    next.u.a(outline.a(a2, "  "), fileDescriptor, printWriter, strArr);
                }
            }
        }
        int size5 = this.g.size();
        if (size5 > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i2 = 0; i2 < size5; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(this.g.get(i2).toString());
            }
        }
        ArrayList<Fragment> arrayList = this.f1298j;
        if (arrayList != null && (size4 = arrayList.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i3 = 0; i3 < size4; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(this.f1298j.get(i3).toString());
            }
        }
        ArrayList<a> arrayList2 = this.f1297i;
        if (arrayList2 != null && (size3 = arrayList2.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i4 = 0; i4 < size3; i4++) {
                BackStackRecord backStackRecord = this.f1297i.get(i4);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(backStackRecord.toString());
                backStackRecord.a(a2, printWriter, true);
            }
        }
        synchronized (this) {
            if (this.f1301m != null && (size2 = this.f1301m.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i5 = 0; i5 < size2; i5++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i5);
                    printWriter.print(": ");
                    printWriter.println(this.f1301m.get(i5));
                }
            }
            if (this.f1302n != null && this.f1302n.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.f1302n.toArray()));
            }
        }
        ArrayList<k.h> arrayList3 = this.d;
        if (arrayList3 != null && (size = arrayList3.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i6 = 0; i6 < size; i6++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i6);
                printWriter.print(": ");
                printWriter.println(this.d.get(i6));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.f1305q);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.f1306r);
        if (this.f1307s != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.f1307s);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.f1304p);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.v);
        printWriter.print(" mStopped=");
        printWriter.print(this.w);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.x);
        if (this.u) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.u);
        }
    }

    public final boolean d(Fragment fragment) {
        boolean z2;
        if (fragment.D && fragment.E) {
            return true;
        }
        FragmentManagerImpl fragmentManagerImpl = fragment.u;
        Iterator<Fragment> it = fragmentManagerImpl.h.values().iterator();
        boolean z3 = false;
        while (true) {
            if (!it.hasNext()) {
                z2 = false;
                break;
            }
            Fragment next = it.next();
            if (next != null) {
                z3 = fragmentManagerImpl.d(next);
                continue;
            }
            if (z3) {
                z2 = true;
                break;
            }
        }
        if (z2) {
            return true;
        }
        return false;
    }

    public final void c(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
        if (arrayList != null && !arrayList.isEmpty()) {
            if (arrayList2 == null || arrayList.size() != arrayList2.size()) {
                throw new IllegalStateException("Internal error with the back stack records");
            }
            a(arrayList, arrayList2);
            int size = arrayList.size();
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                if (!arrayList.get(i2).f1334q) {
                    if (i3 != i2) {
                        a(arrayList, arrayList2, i3, i2);
                    }
                    i3 = i2 + 1;
                    if (arrayList2.get(i2).booleanValue()) {
                        while (i3 < size && arrayList2.get(i3).booleanValue() && !arrayList.get(i3).f1334q) {
                            i3++;
                        }
                    }
                    a(arrayList, arrayList2, i2, i3);
                    i2 = i3 - 1;
                }
                i2++;
            }
            if (i3 != size) {
                a(arrayList, arrayList2, i3, size);
            }
        }
    }

    public Fragment b(String str) {
        for (Fragment next : this.h.values()) {
            if (next != null) {
                if (!str.equals(next.f221f)) {
                    next = next.u.b(str);
                }
                if (next != null) {
                    return next;
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean b(java.util.ArrayList<i.l.a.a> r5, java.util.ArrayList<java.lang.Boolean> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.ArrayList<i.l.a.k$h> r0 = r4.d     // Catch:{ all -> 0x003a }
            r1 = 0
            if (r0 == 0) goto L_0x0038
            java.util.ArrayList<i.l.a.k$h> r0 = r4.d     // Catch:{ all -> 0x003a }
            int r0 = r0.size()     // Catch:{ all -> 0x003a }
            if (r0 != 0) goto L_0x000f
            goto L_0x0038
        L_0x000f:
            java.util.ArrayList<i.l.a.k$h> r0 = r4.d     // Catch:{ all -> 0x003a }
            int r0 = r0.size()     // Catch:{ all -> 0x003a }
            r2 = 0
        L_0x0016:
            if (r1 >= r0) goto L_0x0028
            java.util.ArrayList<i.l.a.k$h> r3 = r4.d     // Catch:{ all -> 0x003a }
            java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x003a }
            i.l.a.FragmentManagerImpl$h r3 = (i.l.a.FragmentManagerImpl.h) r3     // Catch:{ all -> 0x003a }
            boolean r3 = r3.a(r5, r6)     // Catch:{ all -> 0x003a }
            r2 = r2 | r3
            int r1 = r1 + 1
            goto L_0x0016
        L_0x0028:
            java.util.ArrayList<i.l.a.k$h> r5 = r4.d     // Catch:{ all -> 0x003a }
            r5.clear()     // Catch:{ all -> 0x003a }
            i.l.a.FragmentHostCallback r5 = r4.f1305q     // Catch:{ all -> 0x003a }
            android.os.Handler r5 = r5.d     // Catch:{ all -> 0x003a }
            java.lang.Runnable r6 = r4.G     // Catch:{ all -> 0x003a }
            r5.removeCallbacks(r6)     // Catch:{ all -> 0x003a }
            monitor-exit(r4)     // Catch:{ all -> 0x003a }
            return r2
        L_0x0038:
            monitor-exit(r4)     // Catch:{ all -> 0x003a }
            return r1
        L_0x003a:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003a }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.b(java.util.ArrayList, java.util.ArrayList):boolean");
    }

    public final void c() {
        this.h.values().removeAll(Collections.singleton(null));
    }

    public final void c(Fragment fragment) {
        if (fragment != null && this.h.get(fragment.f221f) == fragment) {
            boolean e2 = fragment.f232s.e(fragment);
            Boolean bool = fragment.f224k;
            if (bool == null || bool.booleanValue() != e2) {
                fragment.f224k = Boolean.valueOf(e2);
                FragmentManagerImpl fragmentManagerImpl = fragment.u;
                fragmentManagerImpl.r();
                fragmentManagerImpl.c(fragmentManagerImpl.f1308t);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(int, boolean):void
     arg types: [int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(int, boolean):void */
    public final void b(int i2) {
        try {
            this.f1295e = true;
            a(i2, false);
            this.f1295e = false;
            j();
        } catch (Throwable th) {
            this.f1295e = false;
            throw th;
        }
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    public void b(boolean z2) {
        int size = this.g.size();
        while (true) {
            size--;
            if (size >= 0) {
                Fragment fragment = this.g.get(size);
                if (fragment != null) {
                    fragment.u.b(z2);
                }
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(androidx.fragment.app.Fragment r3, android.os.Bundle r4, boolean r5) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.c(r3, r4, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r5 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.c(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void");
    }

    public boolean b(Menu menu) {
        boolean z2;
        if (this.f1304p < 1) {
            return false;
        }
        boolean z3 = false;
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null) {
                if (!fragment.z) {
                    z2 = fragment.u.b(menu) | (fragment.D && fragment.E);
                } else {
                    z2 = false;
                }
                if (z2) {
                    z3 = true;
                }
            }
        }
        return z3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.c(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.c(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.c(androidx.fragment.app.Fragment, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.c(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.c(androidx.fragment.app.Fragment, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x002d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(android.view.MenuItem r6) {
        /*
            r5 = this;
            int r0 = r5.f1304p
            r1 = 0
            r2 = 1
            if (r0 >= r2) goto L_0x0007
            return r1
        L_0x0007:
            r0 = 0
        L_0x0008:
            java.util.ArrayList<androidx.fragment.app.Fragment> r3 = r5.g
            int r3 = r3.size()
            if (r0 >= r3) goto L_0x0031
            java.util.ArrayList<androidx.fragment.app.Fragment> r3 = r5.g
            java.lang.Object r3 = r3.get(r0)
            androidx.fragment.app.Fragment r3 = (androidx.fragment.app.Fragment) r3
            if (r3 == 0) goto L_0x002e
            boolean r4 = r3.z
            if (r4 != 0) goto L_0x002a
            boolean r4 = r3.D
            i.l.a.FragmentManagerImpl r3 = r3.u
            boolean r3 = r3.b(r6)
            if (r3 == 0) goto L_0x002a
            r3 = 1
            goto L_0x002b
        L_0x002a:
            r3 = 0
        L_0x002b:
            if (r3 == 0) goto L_0x002e
            return r2
        L_0x002e:
            int r0 = r0 + 1
            goto L_0x0008
        L_0x0031:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.b(android.view.MenuItem):boolean");
    }

    public void g() {
        this.x = true;
        j();
        b(0);
        this.f1305q = null;
        this.f1306r = null;
        this.f1307s = null;
        if (this.f1299k != null) {
            Iterator<i.a.a> it = this.f1300l.b.iterator();
            while (it.hasNext()) {
                it.next().cancel();
            }
            this.f1299k = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(androidx.fragment.app.Fragment r3, android.content.Context r4, boolean r5) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.b(r3, r4, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r5 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(androidx.fragment.app.Fragment r3, android.os.Bundle r4, boolean r5) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.b(r3, r4, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r5 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.g(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.g(androidx.fragment.app.Fragment, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.b(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):boolean
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(androidx.fragment.app.Fragment r3, boolean r4) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.b(r3, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r3.next()
            i.l.a.FragmentManagerImpl$f r0 = (i.l.a.FragmentManagerImpl.f) r0
            if (r4 == 0) goto L_0x0027
            boolean r0 = r0.a
            if (r0 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, boolean):void");
    }

    public static d a(float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(I);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(J);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return new d(animationSet);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0042 A[SYNTHETIC, Splitter:B:20:0x0042] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public i.l.a.FragmentManagerImpl.d a(androidx.fragment.app.Fragment r7, int r8, boolean r9, int r10) {
        /*
            r6 = this;
            int r0 = r7.s()
            r1 = 0
            r7.b(r1)
            android.view.ViewGroup r7 = r7.G
            r2 = 0
            if (r7 == 0) goto L_0x0014
            android.animation.LayoutTransition r7 = r7.getLayoutTransition()
            if (r7 == 0) goto L_0x0014
            return r2
        L_0x0014:
            r7 = 1
            if (r0 == 0) goto L_0x0066
            i.l.a.FragmentHostCallback r3 = r6.f1305q
            android.content.Context r3 = r3.c
            android.content.res.Resources r3 = r3.getResources()
            java.lang.String r3 = r3.getResourceTypeName(r0)
            java.lang.String r4 = "anim"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x003f
            i.l.a.FragmentHostCallback r4 = r6.f1305q     // Catch:{ NotFoundException -> 0x003d, RuntimeException -> 0x003f }
            android.content.Context r4 = r4.c     // Catch:{ NotFoundException -> 0x003d, RuntimeException -> 0x003f }
            android.view.animation.Animation r4 = android.view.animation.AnimationUtils.loadAnimation(r4, r0)     // Catch:{ NotFoundException -> 0x003d, RuntimeException -> 0x003f }
            if (r4 == 0) goto L_0x003b
            i.l.a.FragmentManagerImpl$d r5 = new i.l.a.FragmentManagerImpl$d     // Catch:{ NotFoundException -> 0x003d, RuntimeException -> 0x003f }
            r5.<init>(r4)     // Catch:{ NotFoundException -> 0x003d, RuntimeException -> 0x003f }
            return r5
        L_0x003b:
            r4 = 1
            goto L_0x0040
        L_0x003d:
            r7 = move-exception
            throw r7
        L_0x003f:
            r4 = 0
        L_0x0040:
            if (r4 != 0) goto L_0x0066
            i.l.a.FragmentHostCallback r4 = r6.f1305q     // Catch:{ RuntimeException -> 0x0052 }
            android.content.Context r4 = r4.c     // Catch:{ RuntimeException -> 0x0052 }
            android.animation.Animator r4 = android.animation.AnimatorInflater.loadAnimator(r4, r0)     // Catch:{ RuntimeException -> 0x0052 }
            if (r4 == 0) goto L_0x0066
            i.l.a.FragmentManagerImpl$d r5 = new i.l.a.FragmentManagerImpl$d     // Catch:{ RuntimeException -> 0x0052 }
            r5.<init>(r4)     // Catch:{ RuntimeException -> 0x0052 }
            return r5
        L_0x0052:
            r4 = move-exception
            if (r3 != 0) goto L_0x0065
            i.l.a.FragmentHostCallback r3 = r6.f1305q
            android.content.Context r3 = r3.c
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r3, r0)
            if (r0 == 0) goto L_0x0066
            i.l.a.FragmentManagerImpl$d r7 = new i.l.a.FragmentManagerImpl$d
            r7.<init>(r0)
            return r7
        L_0x0065:
            throw r4
        L_0x0066:
            if (r8 != 0) goto L_0x0069
            return r2
        L_0x0069:
            r0 = 4097(0x1001, float:5.741E-42)
            if (r8 == r0) goto L_0x0083
            r0 = 4099(0x1003, float:5.744E-42)
            if (r8 == r0) goto L_0x007d
            r0 = 8194(0x2002, float:1.1482E-41)
            if (r8 == r0) goto L_0x0077
            r8 = -1
            goto L_0x0088
        L_0x0077:
            if (r9 == 0) goto L_0x007b
            r8 = 3
            goto L_0x0088
        L_0x007b:
            r8 = 4
            goto L_0x0088
        L_0x007d:
            if (r9 == 0) goto L_0x0081
            r8 = 5
            goto L_0x0088
        L_0x0081:
            r8 = 6
            goto L_0x0088
        L_0x0083:
            if (r9 == 0) goto L_0x0087
            r8 = 1
            goto L_0x0088
        L_0x0087:
            r8 = 2
        L_0x0088:
            if (r8 >= 0) goto L_0x008b
            return r2
        L_0x008b:
            r3 = 220(0xdc, double:1.087E-321)
            r9 = 1064933786(0x3f79999a, float:0.975)
            r0 = 0
            r5 = 1065353216(0x3f800000, float:1.0)
            switch(r8) {
                case 1: goto L_0x00d3;
                case 2: goto L_0x00ce;
                case 3: goto L_0x00c9;
                case 4: goto L_0x00c1;
                case 5: goto L_0x00ae;
                case 6: goto L_0x009b;
                default: goto L_0x0096;
            }
        L_0x0096:
            if (r10 != 0) goto L_0x00fa
            i.l.a.FragmentHostCallback r8 = r6.f1305q
            goto L_0x00da
        L_0x009b:
            android.view.animation.AlphaAnimation r7 = new android.view.animation.AlphaAnimation
            r7.<init>(r5, r0)
            android.view.animation.Interpolator r8 = i.l.a.FragmentManagerImpl.J
            r7.setInterpolator(r8)
            r7.setDuration(r3)
            i.l.a.FragmentManagerImpl$d r8 = new i.l.a.FragmentManagerImpl$d
            r8.<init>(r7)
            return r8
        L_0x00ae:
            android.view.animation.AlphaAnimation r7 = new android.view.animation.AlphaAnimation
            r7.<init>(r0, r5)
            android.view.animation.Interpolator r8 = i.l.a.FragmentManagerImpl.J
            r7.setInterpolator(r8)
            r7.setDuration(r3)
            i.l.a.FragmentManagerImpl$d r8 = new i.l.a.FragmentManagerImpl$d
            r8.<init>(r7)
            return r8
        L_0x00c1:
            r7 = 1065982362(0x3f89999a, float:1.075)
            i.l.a.FragmentManagerImpl$d r7 = a(r5, r7, r5, r0)
            return r7
        L_0x00c9:
            i.l.a.FragmentManagerImpl$d r7 = a(r9, r5, r0, r5)
            return r7
        L_0x00ce:
            i.l.a.FragmentManagerImpl$d r7 = a(r5, r9, r5, r0)
            return r7
        L_0x00d3:
            r7 = 1066401792(0x3f900000, float:1.125)
            i.l.a.FragmentManagerImpl$d r7 = a(r7, r5, r0, r5)
            return r7
        L_0x00da:
            i.l.a.FragmentActivity$a r8 = (i.l.a.FragmentActivity.a) r8
            i.l.a.FragmentActivity r8 = i.l.a.FragmentActivity.this
            android.view.Window r8 = r8.getWindow()
            if (r8 == 0) goto L_0x00e5
            r1 = 1
        L_0x00e5:
            if (r1 == 0) goto L_0x00fa
            i.l.a.FragmentHostCallback r7 = r6.f1305q
            i.l.a.FragmentActivity$a r7 = (i.l.a.FragmentActivity.a) r7
            i.l.a.FragmentActivity r7 = i.l.a.FragmentActivity.this
            android.view.Window r7 = r7.getWindow()
            if (r7 != 0) goto L_0x00f4
            goto L_0x00fa
        L_0x00f4:
            android.view.WindowManager$LayoutParams r7 = r7.getAttributes()
            int r7 = r7.windowAnimations
        L_0x00fa:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d");
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:176:0x0370 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:127:0x0272 */
    /* JADX WARN: Type inference failed for: r11v1 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentHostCallback, i.l.a.FragmentContainer, androidx.fragment.app.Fragment):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(float, float, float, float):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void
      i.l.a.FragmentManagerImpl.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      i.l.a.FragmentManager.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentHostCallback, i.l.a.FragmentContainer, androidx.fragment.app.Fragment):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d
     arg types: [androidx.fragment.app.Fragment, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(float, float, float, float):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void
      i.l.a.FragmentManagerImpl.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      i.l.a.FragmentManager.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.b(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):boolean
      i.l.a.FragmentManagerImpl.b(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.c(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.c(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.c(androidx.fragment.app.Fragment, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0084, code lost:
        if (r0 != 3) goto L_0x0723;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0372  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x03b9  */
    /* JADX WARNING: Removed duplicated region for block: B:350:0x0728  */
    /* JADX WARNING: Removed duplicated region for block: B:358:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.Fragment r17, int r18, int r19, int r20, boolean r21) {
        /*
            r16 = this;
            r6 = r16
            r7 = r17
            boolean r0 = r7.f225l
            r8 = 1
            if (r0 == 0) goto L_0x0011
            boolean r0 = r7.A
            if (r0 == 0) goto L_0x000e
            goto L_0x0011
        L_0x000e:
            r0 = r18
            goto L_0x0016
        L_0x0011:
            r0 = r18
            if (r0 <= r8) goto L_0x0016
            r0 = 1
        L_0x0016:
            boolean r1 = r7.f226m
            if (r1 == 0) goto L_0x002a
            int r1 = r7.b
            if (r0 <= r1) goto L_0x002a
            if (r1 != 0) goto L_0x0028
            boolean r0 = r17.A()
            if (r0 == 0) goto L_0x0028
            r0 = 1
            goto L_0x002a
        L_0x0028:
            int r0 = r7.b
        L_0x002a:
            boolean r1 = r7.J
            r9 = 3
            r10 = 2
            if (r1 == 0) goto L_0x0037
            int r1 = r7.b
            if (r1 >= r9) goto L_0x0037
            if (r0 <= r10) goto L_0x0037
            r0 = 2
        L_0x0037:
            i.o.Lifecycle$b r1 = r7.R
            i.o.Lifecycle$b r2 = i.o.Lifecycle.b.CREATED
            if (r1 != r2) goto L_0x0042
            int r0 = java.lang.Math.min(r0, r8)
            goto L_0x004a
        L_0x0042:
            int r1 = r1.ordinal()
            int r0 = java.lang.Math.min(r0, r1)
        L_0x004a:
            r11 = r0
            int r0 = r7.b
            java.lang.String r12 = "Fragment "
            r13 = 0
            r14 = 0
            if (r0 > r11) goto L_0x0407
            boolean r0 = r7.f227n
            if (r0 == 0) goto L_0x005c
            boolean r0 = r7.f228o
            if (r0 != 0) goto L_0x005c
            return
        L_0x005c:
            android.view.View r0 = r17.m()
            if (r0 != 0) goto L_0x0068
            android.animation.Animator r0 = r17.n()
            if (r0 == 0) goto L_0x007c
        L_0x0068:
            r7.a(r13)
            r7.a(r13)
            int r2 = r17.x()
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r16
            r1 = r17
            r0.a(r1, r2, r3, r4, r5)
        L_0x007c:
            int r0 = r7.b
            if (r0 == 0) goto L_0x0088
            if (r0 == r8) goto L_0x0238
            if (r0 == r10) goto L_0x0370
            if (r0 == r9) goto L_0x03b6
            goto L_0x0723
        L_0x0088:
            if (r11 <= 0) goto L_0x0238
            android.os.Bundle r0 = r7.c
            if (r0 == 0) goto L_0x0111
            i.l.a.FragmentHostCallback r1 = r6.f1305q
            android.content.Context r1 = r1.c
            java.lang.ClassLoader r1 = r1.getClassLoader()
            r0.setClassLoader(r1)
            android.os.Bundle r0 = r7.c
            java.lang.String r1 = "android:view_state"
            android.util.SparseArray r0 = r0.getSparseParcelableArray(r1)
            r7.d = r0
            android.os.Bundle r0 = r7.c
            java.lang.String r1 = "android:target_state"
            java.lang.String r0 = r0.getString(r1)
            if (r0 != 0) goto L_0x00af
            r2 = r13
            goto L_0x00b9
        L_0x00af:
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r2 = r6.h
            java.lang.Object r2 = r2.get(r0)
            androidx.fragment.app.Fragment r2 = (androidx.fragment.app.Fragment) r2
            if (r2 == 0) goto L_0x00ef
        L_0x00b9:
            if (r2 == 0) goto L_0x00be
            java.lang.String r0 = r2.f221f
            goto L_0x00bf
        L_0x00be:
            r0 = r13
        L_0x00bf:
            r7.f222i = r0
            if (r0 == 0) goto L_0x00cd
            android.os.Bundle r0 = r7.c
            java.lang.String r1 = "android:target_req_state"
            int r0 = r0.getInt(r1, r14)
            r7.f223j = r0
        L_0x00cd:
            java.lang.Boolean r0 = r7.f220e
            if (r0 == 0) goto L_0x00da
            boolean r0 = r0.booleanValue()
            r7.K = r0
            r7.f220e = r13
            goto L_0x00e4
        L_0x00da:
            android.os.Bundle r0 = r7.c
            java.lang.String r1 = "android:user_visible_hint"
            boolean r0 = r0.getBoolean(r1, r8)
            r7.K = r0
        L_0x00e4:
            boolean r0 = r7.K
            if (r0 != 0) goto L_0x0111
            r7.J = r8
            if (r11 <= r10) goto L_0x0111
            r0 = 2
            r11 = 2
            goto L_0x0111
        L_0x00ef:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Fragment no longer exists for key "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r1 = ": unique id "
            r3.append(r1)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.<init>(r0)
            r6.a(r2)
            throw r13
        L_0x0111:
            i.l.a.FragmentHostCallback r0 = r6.f1305q
            r7.f233t = r0
            androidx.fragment.app.Fragment r1 = r6.f1307s
            r7.v = r1
            if (r1 == 0) goto L_0x011e
            i.l.a.FragmentManagerImpl r0 = r1.u
            goto L_0x0120
        L_0x011e:
            i.l.a.FragmentManagerImpl r0 = r0.f1294f
        L_0x0120:
            r7.f232s = r0
            androidx.fragment.app.Fragment r0 = r7.h
            java.lang.String r9 = " that does not belong to this FragmentManager!"
            java.lang.String r15 = " declared target fragment "
            if (r0 == 0) goto L_0x016c
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.h
            java.lang.String r0 = r0.f221f
            java.lang.Object r0 = r1.get(r0)
            androidx.fragment.app.Fragment r1 = r7.h
            if (r0 != r1) goto L_0x014c
            int r0 = r1.b
            if (r0 >= r8) goto L_0x0143
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r16
            r0.a(r1, r2, r3, r4, r5)
        L_0x0143:
            androidx.fragment.app.Fragment r0 = r7.h
            java.lang.String r0 = r0.f221f
            r7.f222i = r0
            r7.h = r13
            goto L_0x016c
        L_0x014c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r12)
            r1.append(r7)
            r1.append(r15)
            androidx.fragment.app.Fragment r2 = r7.h
            r1.append(r2)
            r1.append(r9)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x016c:
            java.lang.String r0 = r7.f222i
            if (r0 == 0) goto L_0x01a3
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.h
            java.lang.Object r0 = r1.get(r0)
            r1 = r0
            androidx.fragment.app.Fragment r1 = (androidx.fragment.app.Fragment) r1
            if (r1 == 0) goto L_0x0189
            int r0 = r1.b
            if (r0 >= r8) goto L_0x01a3
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r16
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x01a3
        L_0x0189:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r12)
            r1.append(r7)
            r1.append(r15)
            java.lang.String r2 = r7.f222i
            java.lang.String r1 = j.a.a.a.outline.a(r1, r2, r9)
            r0.<init>(r1)
            throw r0
        L_0x01a3:
            i.l.a.FragmentHostCallback r0 = r6.f1305q
            android.content.Context r0 = r0.c
            r6.b(r7, r0, r14)
            i.l.a.FragmentManagerImpl r0 = r7.u
            i.l.a.FragmentHostCallback r1 = r7.f233t
            i.l.a.Fragment r2 = new i.l.a.Fragment
            r2.<init>(r7)
            r0.a(r1, r2, r7)
            r7.F = r14
            i.l.a.FragmentHostCallback r0 = r7.f233t
            android.content.Context r0 = r0.c
            r7.a(r0)
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x022c
            androidx.fragment.app.Fragment r0 = r7.v
            if (r0 != 0) goto L_0x01d1
            i.l.a.FragmentHostCallback r0 = r6.f1305q
            i.l.a.FragmentActivity$a r0 = (i.l.a.FragmentActivity.a) r0
            i.l.a.FragmentActivity r0 = i.l.a.FragmentActivity.this
            if (r0 == 0) goto L_0x01d0
            goto L_0x01d1
        L_0x01d0:
            throw r13
        L_0x01d1:
            i.l.a.FragmentHostCallback r0 = r6.f1305q
            android.content.Context r0 = r0.c
            r6.a(r7, r0, r14)
            boolean r0 = r7.Q
            if (r0 != 0) goto L_0x0213
            android.os.Bundle r0 = r7.c
            r6.c(r7, r0, r14)
            android.os.Bundle r0 = r7.c
            i.l.a.FragmentManagerImpl r1 = r7.u
            r1.n()
            r7.b = r8
            r7.F = r14
            i.t.SavedStateRegistryController r1 = r7.V
            r1.a(r0)
            r7.b(r0)
            r7.Q = r8
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x0207
            i.o.LifecycleRegistry r0 = r7.S
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_CREATE
            r0.a(r1)
            android.os.Bundle r0 = r7.c
            r6.b(r7, r0, r14)
            goto L_0x0238
        L_0x0207:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onCreate()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x0213:
            android.os.Bundle r0 = r7.c
            if (r0 == 0) goto L_0x0229
            java.lang.String r1 = "android:support:fragments"
            android.os.Parcelable r0 = r0.getParcelable(r1)
            if (r0 == 0) goto L_0x0229
            i.l.a.FragmentManagerImpl r1 = r7.u
            r1.a(r0)
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.f()
        L_0x0229:
            r7.b = r8
            goto L_0x0238
        L_0x022c:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onAttach()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x0238:
            r0 = 8
            if (r11 <= 0) goto L_0x0272
            boolean r1 = r7.f227n
            if (r1 == 0) goto L_0x0272
            boolean r1 = r7.f230q
            if (r1 != 0) goto L_0x0272
            android.os.Bundle r1 = r7.c
            android.view.LayoutInflater r1 = r7.e(r1)
            android.os.Bundle r2 = r7.c
            r7.b(r1, r13, r2)
            android.view.View r1 = r7.H
            if (r1 == 0) goto L_0x0270
            r7.I = r1
            r1.setSaveFromParentEnabled(r14)
            boolean r1 = r7.z
            if (r1 == 0) goto L_0x0261
            android.view.View r1 = r7.H
            r1.setVisibility(r0)
        L_0x0261:
            android.view.View r1 = r7.H
            android.os.Bundle r2 = r7.c
            r7.a(r1, r2)
            android.view.View r1 = r7.H
            android.os.Bundle r2 = r7.c
            r6.a(r7, r1, r2, r14)
            goto L_0x0272
        L_0x0270:
            r7.I = r13
        L_0x0272:
            if (r11 <= r8) goto L_0x0370
            boolean r1 = r7.f227n
            if (r1 != 0) goto L_0x0321
            int r1 = r7.x
            if (r1 == 0) goto L_0x02d8
            r2 = -1
            if (r1 == r2) goto L_0x02c7
            i.l.a.FragmentContainer r2 = r6.f1306r
            android.view.View r1 = r2.a(r1)
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            if (r1 != 0) goto L_0x02d9
            boolean r2 = r7.f229p
            if (r2 == 0) goto L_0x028e
            goto L_0x02d9
        L_0x028e:
            android.content.res.Resources r0 = r17.v()     // Catch:{ NotFoundException -> 0x0299 }
            int r1 = r7.x     // Catch:{ NotFoundException -> 0x0299 }
            java.lang.String r0 = r0.getResourceName(r1)     // Catch:{ NotFoundException -> 0x0299 }
            goto L_0x029b
        L_0x0299:
            java.lang.String r0 = "unknown"
        L_0x029b:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "No view found for id 0x"
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            int r3 = r7.x
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r3 = " ("
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = ") for fragment "
            r2.append(r0)
            r2.append(r7)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            r6.a(r1)
            throw r13
        L_0x02c7:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Cannot create fragment "
            java.lang.String r2 = " for a container view with no id"
            java.lang.String r1 = j.a.a.a.outline.a(r1, r7, r2)
            r0.<init>(r1)
            r6.a(r0)
            throw r13
        L_0x02d8:
            r1 = r13
        L_0x02d9:
            r7.G = r1
            android.os.Bundle r2 = r7.c
            android.view.LayoutInflater r2 = r7.e(r2)
            android.os.Bundle r3 = r7.c
            r7.b(r2, r1, r3)
            android.view.View r2 = r7.H
            if (r2 == 0) goto L_0x031f
            r7.I = r2
            r2.setSaveFromParentEnabled(r14)
            if (r1 == 0) goto L_0x02f6
            android.view.View r2 = r7.H
            r1.addView(r2)
        L_0x02f6:
            boolean r1 = r7.z
            if (r1 == 0) goto L_0x02ff
            android.view.View r1 = r7.H
            r1.setVisibility(r0)
        L_0x02ff:
            android.view.View r0 = r7.H
            android.os.Bundle r1 = r7.c
            r7.a(r0, r1)
            android.view.View r0 = r7.H
            android.os.Bundle r1 = r7.c
            r6.a(r7, r0, r1, r14)
            android.view.View r0 = r7.H
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x031b
            android.view.ViewGroup r0 = r7.G
            if (r0 == 0) goto L_0x031b
            r0 = 1
            goto L_0x031c
        L_0x031b:
            r0 = 0
        L_0x031c:
            r7.M = r0
            goto L_0x0321
        L_0x031f:
            r7.I = r13
        L_0x0321:
            android.os.Bundle r0 = r7.c
            i.l.a.FragmentManagerImpl r1 = r7.u
            r1.n()
            r7.b = r10
            r7.F = r14
            r7.a(r0)
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x0364
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.v = r14
            r0.w = r14
            r0.b(r10)
            android.os.Bundle r0 = r7.c
            r6.a(r7, r0, r14)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x0361
            android.util.SparseArray<android.os.Parcelable> r0 = r7.d
            if (r0 == 0) goto L_0x0350
            android.view.View r1 = r7.I
            r1.restoreHierarchyState(r0)
            r7.d = r13
        L_0x0350:
            r7.F = r14
            r7.F = r8
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x0361
            i.l.a.FragmentViewLifecycleOwner r0 = r7.T
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_CREATE
            i.o.LifecycleRegistry r0 = r0.b
            r0.a(r1)
        L_0x0361:
            r7.c = r13
            goto L_0x0370
        L_0x0364:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onActivityCreated()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x0370:
            if (r11 <= r10) goto L_0x03b6
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.n()
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.j()
            r0 = 3
            r7.b = r0
            r7.F = r14
            r17.G()
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x03aa
            i.o.LifecycleRegistry r0 = r7.S
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_START
            r0.a(r1)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x039c
            i.l.a.FragmentViewLifecycleOwner r0 = r7.T
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_START
            i.o.LifecycleRegistry r0 = r0.b
            r0.a(r1)
        L_0x039c:
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.v = r14
            r0.w = r14
            r1 = 3
            r0.b(r1)
            r6.f(r7, r14)
            goto L_0x03b6
        L_0x03aa:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onStart()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x03b6:
            r0 = 3
            if (r11 <= r0) goto L_0x0723
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.n()
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.j()
            r0 = 4
            r7.b = r0
            r7.F = r14
            r17.F()
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x03fb
            i.o.LifecycleRegistry r0 = r7.S
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_RESUME
            r0.a(r1)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x03e3
            i.l.a.FragmentViewLifecycleOwner r0 = r7.T
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_RESUME
            i.o.LifecycleRegistry r0 = r0.b
            r0.a(r1)
        L_0x03e3:
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.v = r14
            r0.w = r14
            r1 = 4
            r0.b(r1)
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.j()
            r6.e(r7, r14)
            r7.c = r13
            r7.d = r13
            goto L_0x0723
        L_0x03fb:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onResume()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x0407:
            if (r0 <= r11) goto L_0x0723
            if (r0 == r8) goto L_0x057f
            if (r0 == r10) goto L_0x0485
            r1 = 3
            if (r0 == r1) goto L_0x044c
            r2 = 4
            if (r0 == r2) goto L_0x0415
            goto L_0x0723
        L_0x0415:
            if (r11 >= r2) goto L_0x044c
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.b(r1)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x0429
            i.l.a.FragmentViewLifecycleOwner r0 = r7.T
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_PAUSE
            i.o.LifecycleRegistry r0 = r0.b
            r0.a(r1)
        L_0x0429:
            i.o.LifecycleRegistry r0 = r7.S
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_PAUSE
            r0.a(r1)
            r0 = 3
            r7.b = r0
            r7.F = r14
            r17.E()
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x0440
            r6.d(r7, r14)
            goto L_0x044c
        L_0x0440:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onPause()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x044c:
            r0 = 3
            if (r11 >= r0) goto L_0x0485
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.w = r8
            r0.b(r10)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x0463
            i.l.a.FragmentViewLifecycleOwner r0 = r7.T
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_STOP
            i.o.LifecycleRegistry r0 = r0.b
            r0.a(r1)
        L_0x0463:
            i.o.LifecycleRegistry r0 = r7.S
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_STOP
            r0.a(r1)
            r7.b = r10
            r7.F = r14
            r17.H()
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x0479
            r6.g(r7, r14)
            goto L_0x0485
        L_0x0479:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onStop()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x0485:
            if (r11 >= r10) goto L_0x057f
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x049f
            i.l.a.FragmentHostCallback r0 = r6.f1305q
            i.l.a.FragmentActivity$a r0 = (i.l.a.FragmentActivity.a) r0
            i.l.a.FragmentActivity r0 = i.l.a.FragmentActivity.this
            boolean r0 = r0.isFinishing()
            r0 = r0 ^ r8
            if (r0 == 0) goto L_0x049f
            android.util.SparseArray<android.os.Parcelable> r0 = r7.d
            if (r0 != 0) goto L_0x049f
            r16.j(r17)
        L_0x049f:
            i.l.a.FragmentManagerImpl r0 = r7.u
            r0.b(r8)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x04b1
            i.l.a.FragmentViewLifecycleOwner r0 = r7.T
            i.o.Lifecycle$a r1 = i.o.Lifecycle.a.ON_DESTROY
            i.o.LifecycleRegistry r0 = r0.b
            r0.a(r1)
        L_0x04b1:
            r7.b = r8
            r7.F = r14
            r17.C()
            boolean r0 = r7.F
            if (r0 == 0) goto L_0x0573
            i.p.a.LoaderManager r0 = i.p.a.LoaderManager.a(r17)
            i.p.a.LoaderManagerImpl r0 = (i.p.a.LoaderManagerImpl) r0
            i.p.a.LoaderManagerImpl$c r0 = r0.b
            i.e.SparseArrayCompat<i.p.a.b$a> r1 = r0.b
            int r1 = r1.c()
            r2 = 0
        L_0x04cb:
            if (r2 >= r1) goto L_0x04da
            i.e.SparseArrayCompat<i.p.a.b$a> r3 = r0.b
            java.lang.Object r3 = r3.d(r2)
            i.p.a.LoaderManagerImpl$a r3 = (i.p.a.LoaderManagerImpl.a) r3
            i.o.LifecycleOwner r3 = r3.f1362j
            int r2 = r2 + 1
            goto L_0x04cb
        L_0x04da:
            r7.f230q = r14
            r6.h(r7, r14)
            android.view.View r0 = r7.H
            if (r0 == 0) goto L_0x0563
            android.view.ViewGroup r1 = r7.G
            if (r1 == 0) goto L_0x0563
            r1.endViewTransition(r0)
            android.view.View r0 = r7.H
            r0.clearAnimation()
            androidx.fragment.app.Fragment r0 = r7.v
            if (r0 == 0) goto L_0x04f7
            boolean r0 = r0.f226m
            if (r0 != 0) goto L_0x0563
        L_0x04f7:
            int r0 = r6.f1304p
            r1 = 0
            if (r0 <= 0) goto L_0x0517
            boolean r0 = r6.x
            if (r0 != 0) goto L_0x0517
            android.view.View r0 = r7.H
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x0517
            float r0 = r7.O
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0517
            r0 = r19
            r2 = r20
            i.l.a.FragmentManagerImpl$d r0 = r6.a(r7, r0, r14, r2)
            goto L_0x0518
        L_0x0517:
            r0 = r13
        L_0x0518:
            r7.O = r1
            if (r0 == 0) goto L_0x055c
            android.view.View r1 = r7.H
            android.view.ViewGroup r2 = r7.G
            r2.startViewTransition(r1)
            androidx.fragment.app.Fragment$b r3 = r17.k()
            r3.c = r11
            android.view.animation.Animation r3 = r0.a
            if (r3 == 0) goto L_0x0547
            i.l.a.FragmentManagerImpl$e r3 = new i.l.a.FragmentManagerImpl$e
            android.view.animation.Animation r0 = r0.a
            r3.<init>(r0, r2, r1)
            android.view.View r0 = r7.H
            r7.a(r0)
            i.l.a.FragmentManagerImpl0 r0 = new i.l.a.FragmentManagerImpl0
            r0.<init>(r6, r2, r7)
            r3.setAnimationListener(r0)
            android.view.View r0 = r7.H
            r0.startAnimation(r3)
            goto L_0x055c
        L_0x0547:
            android.animation.Animator r0 = r0.b
            r7.a(r0)
            i.l.a.FragmentManagerImpl1 r3 = new i.l.a.FragmentManagerImpl1
            r3.<init>(r6, r2, r1, r7)
            r0.addListener(r3)
            android.view.View r1 = r7.H
            r0.setTarget(r1)
            r0.start()
        L_0x055c:
            android.view.ViewGroup r0 = r7.G
            android.view.View r1 = r7.H
            r0.removeView(r1)
        L_0x0563:
            r7.G = r13
            r7.H = r13
            r7.T = r13
            i.o.MutableLiveData<i.o.k> r0 = r7.U
            r0.a(r13)
            r7.I = r13
            r7.f228o = r14
            goto L_0x057f
        L_0x0573:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onDestroyView()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x057f:
            if (r11 >= r8) goto L_0x0723
            boolean r0 = r6.x
            if (r0 == 0) goto L_0x05a6
            android.view.View r0 = r17.m()
            if (r0 == 0) goto L_0x0596
            android.view.View r0 = r17.m()
            r7.a(r13)
            r0.clearAnimation()
            goto L_0x05a6
        L_0x0596:
            android.animation.Animator r0 = r17.n()
            if (r0 == 0) goto L_0x05a6
            android.animation.Animator r0 = r17.n()
            r7.a(r13)
            r0.cancel()
        L_0x05a6:
            android.view.View r0 = r17.m()
            if (r0 != 0) goto L_0x071c
            android.animation.Animator r0 = r17.n()
            if (r0 == 0) goto L_0x05b4
            goto L_0x071c
        L_0x05b4:
            boolean r0 = r7.f226m
            if (r0 == 0) goto L_0x05c0
            boolean r0 = r17.A()
            if (r0 != 0) goto L_0x05c0
            r0 = 1
            goto L_0x05c1
        L_0x05c0:
            r0 = 0
        L_0x05c1:
            if (r0 != 0) goto L_0x05cf
            i.l.a.FragmentManagerViewModel r1 = r6.F
            boolean r1 = r1.a(r7)
            if (r1 == 0) goto L_0x05cc
            goto L_0x05cf
        L_0x05cc:
            r7.b = r14
            goto L_0x0637
        L_0x05cf:
            i.l.a.FragmentHostCallback r1 = r6.f1305q
            boolean r2 = r1 instanceof i.o.ViewModelStoreOwner
            if (r2 == 0) goto L_0x05da
            i.l.a.FragmentManagerViewModel r1 = r6.F
            boolean r8 = r1.f1314f
            goto L_0x05e7
        L_0x05da:
            android.content.Context r1 = r1.c
            boolean r2 = r1 instanceof android.app.Activity
            if (r2 == 0) goto L_0x05e7
            android.app.Activity r1 = (android.app.Activity) r1
            boolean r1 = r1.isChangingConfigurations()
            r8 = r8 ^ r1
        L_0x05e7:
            if (r0 != 0) goto L_0x05eb
            if (r8 == 0) goto L_0x061b
        L_0x05eb:
            i.l.a.FragmentManagerViewModel r1 = r6.F
            if (r1 == 0) goto L_0x071b
            java.util.HashMap<java.lang.String, i.l.a.p> r2 = r1.c
            java.lang.String r3 = r7.f221f
            java.lang.Object r2 = r2.get(r3)
            i.l.a.FragmentManagerViewModel r2 = (i.l.a.FragmentManagerViewModel) r2
            if (r2 == 0) goto L_0x0605
            r2.b()
            java.util.HashMap<java.lang.String, i.l.a.p> r2 = r1.c
            java.lang.String r3 = r7.f221f
            r2.remove(r3)
        L_0x0605:
            java.util.HashMap<java.lang.String, i.o.x> r2 = r1.d
            java.lang.String r3 = r7.f221f
            java.lang.Object r2 = r2.get(r3)
            i.o.ViewModelStore r2 = (i.o.ViewModelStore) r2
            if (r2 == 0) goto L_0x061b
            r2.a()
            java.util.HashMap<java.lang.String, i.o.x> r1 = r1.d
            java.lang.String r2 = r7.f221f
            r1.remove(r2)
        L_0x061b:
            i.l.a.FragmentManagerImpl r1 = r7.u
            r1.g()
            i.o.LifecycleRegistry r1 = r7.S
            i.o.Lifecycle$a r2 = i.o.Lifecycle.a.ON_DESTROY
            r1.a(r2)
            r7.b = r14
            r7.F = r14
            r7.Q = r14
            r17.B()
            boolean r1 = r7.F
            if (r1 == 0) goto L_0x070f
            r6.b(r7, r14)
        L_0x0637:
            r7.F = r14
            r17.D()
            r7.P = r13
            boolean r1 = r7.F
            if (r1 == 0) goto L_0x0703
            i.l.a.FragmentManagerImpl r1 = r7.u
            boolean r2 = r1.x
            if (r2 != 0) goto L_0x0652
            r1.g()
            i.l.a.FragmentManagerImpl r1 = new i.l.a.FragmentManagerImpl
            r1.<init>()
            r7.u = r1
        L_0x0652:
            r6.c(r7, r14)
            if (r21 != 0) goto L_0x0723
            if (r0 != 0) goto L_0x067e
            i.l.a.FragmentManagerViewModel r0 = r6.F
            boolean r0 = r0.a(r7)
            if (r0 == 0) goto L_0x0662
            goto L_0x067e
        L_0x0662:
            r7.f233t = r13
            r7.v = r13
            r7.f232s = r13
            java.lang.String r0 = r7.f222i
            if (r0 == 0) goto L_0x0723
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.h
            java.lang.Object r0 = r1.get(r0)
            androidx.fragment.app.Fragment r0 = (androidx.fragment.app.Fragment) r0
            if (r0 == 0) goto L_0x0723
            boolean r1 = r0.B
            if (r1 == 0) goto L_0x0723
            r7.h = r0
            goto L_0x0723
        L_0x067e:
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r0 = r6.h
            java.lang.String r1 = r7.f221f
            java.lang.Object r0 = r0.get(r1)
            if (r0 != 0) goto L_0x068a
            goto L_0x0723
        L_0x068a:
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r0 = r6.h
            java.util.Collection r0 = r0.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x0694:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x06b1
            java.lang.Object r1 = r0.next()
            androidx.fragment.app.Fragment r1 = (androidx.fragment.app.Fragment) r1
            if (r1 == 0) goto L_0x0694
            java.lang.String r2 = r7.f221f
            java.lang.String r3 = r1.f222i
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0694
            r1.h = r7
            r1.f222i = r13
            goto L_0x0694
        L_0x06b1:
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r0 = r6.h
            java.lang.String r1 = r7.f221f
            r0.put(r1, r13)
            boolean r0 = r16.m()
            if (r0 == 0) goto L_0x06bf
            goto L_0x06c6
        L_0x06bf:
            i.l.a.FragmentManagerViewModel r0 = r6.F
            java.util.HashSet<androidx.fragment.app.Fragment> r0 = r0.b
            r0.remove(r7)
        L_0x06c6:
            java.lang.String r0 = r7.f222i
            if (r0 == 0) goto L_0x06d4
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.h
            java.lang.Object r0 = r1.get(r0)
            androidx.fragment.app.Fragment r0 = (androidx.fragment.app.Fragment) r0
            r7.h = r0
        L_0x06d4:
            r17.y()
            java.util.UUID r0 = java.util.UUID.randomUUID()
            java.lang.String r0 = r0.toString()
            r7.f221f = r0
            r7.f225l = r14
            r7.f226m = r14
            r7.f227n = r14
            r7.f228o = r14
            r7.f229p = r14
            r7.f231r = r14
            r7.f232s = r13
            i.l.a.FragmentManagerImpl r0 = new i.l.a.FragmentManagerImpl
            r0.<init>()
            r7.u = r0
            r7.f233t = r13
            r7.w = r14
            r7.x = r14
            r7.y = r13
            r7.z = r14
            r7.A = r14
            goto L_0x0723
        L_0x0703:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onDetach()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x070f:
            i.l.a.SuperNotCalledException r0 = new i.l.a.SuperNotCalledException
            java.lang.String r1 = " did not call through to super.onDestroy()"
            java.lang.String r1 = j.a.a.a.outline.a(r12, r7, r1)
            r0.<init>(r1)
            throw r0
        L_0x071b:
            throw r13
        L_0x071c:
            androidx.fragment.app.Fragment$b r0 = r17.k()
            r0.c = r11
            goto L_0x0724
        L_0x0723:
            r8 = r11
        L_0x0724:
            int r0 = r7.b
            if (r0 == r8) goto L_0x0752
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveToState: Fragment state for "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r1 = " not updated inline; expected state "
            r0.append(r1)
            r0.append(r8)
            java.lang.String r1 = " found "
            r0.append(r1)
            int r1 = r7.b
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "FragmentManager"
            android.util.Log.w(r1, r0)
            r7.b = r8
        L_0x0752:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void");
    }

    public void a(int i2, boolean z2) {
        FragmentHostCallback fragmentHostCallback;
        if (this.f1305q == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z2 || i2 != this.f1304p) {
            this.f1304p = i2;
            int size = this.g.size();
            for (int i3 = 0; i3 < size; i3++) {
                g(this.g.get(i3));
            }
            for (Fragment next : this.h.values()) {
                if (next != null && ((next.f226m || next.A) && !next.M)) {
                    g(next);
                }
            }
            q();
            if (this.u && (fragmentHostCallback = this.f1305q) != null && this.f1304p == 4) {
                FragmentActivity.this.l();
                this.u = false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public void a(Fragment fragment, boolean z2) {
        f(fragment);
        if (fragment.A) {
            return;
        }
        if (!this.g.contains(fragment)) {
            synchronized (this.g) {
                this.g.add(fragment);
            }
            fragment.f225l = true;
            fragment.f226m = false;
            if (fragment.H == null) {
                fragment.N = false;
            }
            if (d(fragment)) {
                this.u = true;
            }
            if (z2) {
                a(fragment, this.f1304p, 0, 0, false);
                return;
            }
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }

    public void a(Fragment fragment) {
        if (fragment.A) {
            fragment.A = false;
            if (fragment.f225l) {
                return;
            }
            if (!this.g.contains(fragment)) {
                synchronized (this.g) {
                    this.g.add(fragment);
                }
                fragment.f225l = true;
                if (d(fragment)) {
                    this.u = true;
                    return;
                }
                return;
            }
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
    }

    public Fragment a(int i2) {
        for (int size = this.g.size() - 1; size >= 0; size--) {
            Fragment fragment = this.g.get(size);
            if (fragment != null && fragment.w == i2) {
                return fragment;
            }
        }
        for (Fragment next : this.h.values()) {
            if (next != null && next.w == i2) {
                return next;
            }
        }
        return null;
    }

    public Fragment a(String str) {
        if (str != null) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = this.g.get(size);
                if (fragment != null && str.equals(fragment.y)) {
                    return fragment;
                }
            }
        }
        if (str == null) {
            return null;
        }
        for (Fragment next : this.h.values()) {
            if (next != null && str.equals(next.y)) {
                return next;
            }
        }
        return null;
    }

    public void a(h hVar, boolean z2) {
        if (!z2) {
            d();
        }
        synchronized (this) {
            if (!this.x) {
                if (this.f1305q != null) {
                    if (this.d == null) {
                        this.d = new ArrayList<>();
                    }
                    this.d.add(hVar);
                    p();
                    return;
                }
            }
            if (!z2) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    public int a(BackStackRecord backStackRecord) {
        synchronized (this) {
            if (this.f1302n != null) {
                if (this.f1302n.size() > 0) {
                    int intValue = this.f1302n.remove(this.f1302n.size() - 1).intValue();
                    this.f1301m.set(intValue, backStackRecord);
                    return intValue;
                }
            }
            if (this.f1301m == null) {
                this.f1301m = new ArrayList<>();
            }
            int size = this.f1301m.size();
            this.f1301m.add(backStackRecord);
            return size;
        }
    }

    public void a(int i2, BackStackRecord backStackRecord) {
        synchronized (this) {
            if (this.f1301m == null) {
                this.f1301m = new ArrayList<>();
            }
            int size = this.f1301m.size();
            if (i2 < size) {
                this.f1301m.set(i2, backStackRecord);
            } else {
                while (size < i2) {
                    this.f1301m.add(null);
                    if (this.f1302n == null) {
                        this.f1302n = new ArrayList<>();
                    }
                    this.f1302n.add(Integer.valueOf(size));
                    size++;
                }
                this.f1301m.add(backStackRecord);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void
     arg types: [i.l.a.BackStackRecord, boolean, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(float, float, float, float):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      i.l.a.FragmentManager.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void */
    public final void a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
        int indexOf;
        int indexOf2;
        ArrayList<k.j> arrayList3 = this.E;
        int size = arrayList3 == null ? 0 : arrayList3.size();
        int i2 = 0;
        while (i2 < size) {
            j jVar = this.E.get(i2);
            if (arrayList == null || jVar.a || (indexOf2 = arrayList.indexOf(jVar.b)) == -1 || !arrayList2.get(indexOf2).booleanValue()) {
                if ((jVar.c == 0) || (arrayList != null && jVar.b.a(arrayList, 0, arrayList.size()))) {
                    this.E.remove(i2);
                    i2--;
                    size--;
                    if (arrayList == null || jVar.a || (indexOf = arrayList.indexOf(jVar.b)) == -1 || !arrayList2.get(indexOf).booleanValue()) {
                        jVar.a();
                    } else {
                        BackStackRecord backStackRecord = jVar.b;
                        backStackRecord.f1275s.a(backStackRecord, jVar.a, false, false);
                    }
                }
            } else {
                this.E.remove(i2);
                i2--;
                size--;
                BackStackRecord backStackRecord2 = jVar.b;
                backStackRecord2.f1275s.a(backStackRecord2, jVar.a, false, false);
            }
            i2++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.k, java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void
     arg types: [i.l.a.FragmentManagerImpl, java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int, int]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, java.lang.Object, java.lang.Object, java.lang.Object, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, java.lang.Object, i.e.ArrayMap<java.lang.String, android.view.View>, boolean, i.l.a.a):void
      i.l.a.FragmentTransition3.a(i.l.a.k, java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(int, boolean):void
     arg types: [int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(int, boolean):void */
    public final void a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        boolean z2;
        int i7;
        int i8;
        ArrayList<a> arrayList3 = arrayList;
        ArrayList<Boolean> arrayList4 = arrayList2;
        int i9 = i2;
        int i10 = i3;
        boolean z3 = arrayList3.get(i9).f1334q;
        ArrayList<Fragment> arrayList5 = this.B;
        if (arrayList5 == null) {
            this.B = new ArrayList<>();
        } else {
            arrayList5.clear();
        }
        this.B.addAll(this.g);
        Fragment fragment = this.f1308t;
        int i11 = i9;
        boolean z4 = false;
        while (true) {
            int i12 = 1;
            if (i11 < i10) {
                BackStackRecord backStackRecord = arrayList3.get(i11);
                int i13 = 3;
                if (!arrayList4.get(i11).booleanValue()) {
                    ArrayList<Fragment> arrayList6 = this.B;
                    int i14 = 0;
                    while (i14 < backStackRecord.a.size()) {
                        FragmentTransaction.a aVar = backStackRecord.a.get(i14);
                        int i15 = aVar.a;
                        if (i15 != i12) {
                            if (i15 == 2) {
                                Fragment fragment2 = aVar.b;
                                int i16 = fragment2.x;
                                int size = arrayList6.size() - 1;
                                boolean z5 = false;
                                while (size >= 0) {
                                    Fragment fragment3 = arrayList6.get(size);
                                    if (fragment3.x != i16) {
                                        i8 = i16;
                                    } else if (fragment3 == fragment2) {
                                        i8 = i16;
                                        z5 = true;
                                    } else {
                                        if (fragment3 == fragment) {
                                            i8 = i16;
                                            backStackRecord.a.add(i14, new FragmentTransaction.a(9, fragment3));
                                            i14++;
                                            fragment = null;
                                        } else {
                                            i8 = i16;
                                        }
                                        FragmentTransaction.a aVar2 = new FragmentTransaction.a(3, fragment3);
                                        aVar2.c = aVar.c;
                                        aVar2.f1336e = aVar.f1336e;
                                        aVar2.d = aVar.d;
                                        aVar2.f1337f = aVar.f1337f;
                                        backStackRecord.a.add(i14, aVar2);
                                        arrayList6.remove(fragment3);
                                        i14++;
                                    }
                                    size--;
                                    i16 = i8;
                                }
                                if (z5) {
                                    backStackRecord.a.remove(i14);
                                    i14--;
                                } else {
                                    i7 = 1;
                                    aVar.a = 1;
                                    arrayList6.add(fragment2);
                                    i14 += i7;
                                    i13 = 3;
                                    i12 = 1;
                                }
                            } else if (i15 == i13 || i15 == 6) {
                                arrayList6.remove(aVar.b);
                                Fragment fragment4 = aVar.b;
                                if (fragment4 == fragment) {
                                    backStackRecord.a.add(i14, new FragmentTransaction.a(9, fragment4));
                                    i14++;
                                    fragment = null;
                                }
                            } else if (i15 != 7) {
                                if (i15 == 8) {
                                    backStackRecord.a.add(i14, new FragmentTransaction.a(9, fragment));
                                    i14++;
                                    fragment = aVar.b;
                                }
                            }
                            i7 = 1;
                            i14 += i7;
                            i13 = 3;
                            i12 = 1;
                        }
                        i7 = 1;
                        arrayList6.add(aVar.b);
                        i14 += i7;
                        i13 = 3;
                        i12 = 1;
                    }
                } else {
                    int i17 = 1;
                    ArrayList<Fragment> arrayList7 = this.B;
                    int size2 = backStackRecord.a.size() - 1;
                    while (size2 >= 0) {
                        FragmentTransaction.a aVar3 = backStackRecord.a.get(size2);
                        int i18 = aVar3.a;
                        if (i18 != i17) {
                            if (i18 != 3) {
                                switch (i18) {
                                    case 8:
                                        fragment = null;
                                        break;
                                    case 9:
                                        fragment = aVar3.b;
                                        break;
                                    case 10:
                                        aVar3.h = aVar3.g;
                                        break;
                                }
                                size2--;
                                i17 = 1;
                            }
                            arrayList7.add(aVar3.b);
                            size2--;
                            i17 = 1;
                        }
                        arrayList7.remove(aVar3.b);
                        size2--;
                        i17 = 1;
                    }
                }
                z4 = z4 || backStackRecord.h;
                i11++;
                arrayList4 = arrayList2;
            } else {
                this.B.clear();
                if (!z3) {
                    FragmentTransition3.a((k) this, arrayList, arrayList2, i2, i3, false);
                }
                int i19 = i2;
                while (i19 < i10) {
                    BackStackRecord backStackRecord2 = arrayList3.get(i19);
                    if (arrayList2.get(i19).booleanValue()) {
                        backStackRecord2.a(-1);
                        backStackRecord2.b(i19 == i10 + -1);
                    } else {
                        backStackRecord2.a(1);
                        backStackRecord2.c();
                    }
                    i19++;
                }
                ArrayList<Boolean> arrayList8 = arrayList2;
                if (z3) {
                    ArraySet arraySet = new ArraySet();
                    a(arraySet);
                    i4 = i2;
                    int i20 = i10;
                    for (int i21 = i10 - 1; i21 >= i4; i21--) {
                        BackStackRecord backStackRecord3 = arrayList3.get(i21);
                        boolean booleanValue = arrayList8.get(i21).booleanValue();
                        int i22 = 0;
                        while (true) {
                            if (i22 >= backStackRecord3.a.size()) {
                                z2 = false;
                            } else if (BackStackRecord.b(backStackRecord3.a.get(i22))) {
                                z2 = true;
                            } else {
                                i22++;
                            }
                        }
                        if (z2 && !backStackRecord3.a(arrayList3, i21 + 1, i10)) {
                            if (this.E == null) {
                                this.E = new ArrayList<>();
                            }
                            j jVar = new j(backStackRecord3, booleanValue);
                            this.E.add(jVar);
                            for (int i23 = 0; i23 < backStackRecord3.a.size(); i23++) {
                                FragmentTransaction.a aVar4 = backStackRecord3.a.get(i23);
                                if (BackStackRecord.b(aVar4)) {
                                    aVar4.b.a(jVar);
                                }
                            }
                            if (booleanValue) {
                                backStackRecord3.c();
                            } else {
                                backStackRecord3.b(false);
                            }
                            i20--;
                            if (i21 != i20) {
                                arrayList3.remove(i21);
                                arrayList3.add(i20, backStackRecord3);
                            }
                            a(arraySet);
                        }
                    }
                    int i24 = arraySet.d;
                    for (int i25 = 0; i25 < i24; i25++) {
                        Fragment fragment5 = (Fragment) arraySet.c[i25];
                        if (!fragment5.f225l) {
                            View L = fragment5.L();
                            fragment5.O = L.getAlpha();
                            L.setAlpha(0.0f);
                        }
                    }
                    i5 = i20;
                } else {
                    i4 = i2;
                    i5 = i10;
                }
                if (i5 != i4 && z3) {
                    FragmentTransition3.a((k) this, arrayList, arrayList2, i2, i5, true);
                    a(this.f1304p, true);
                }
                while (i4 < i10) {
                    BackStackRecord backStackRecord4 = arrayList3.get(i4);
                    if (arrayList8.get(i4).booleanValue() && (i6 = backStackRecord4.u) >= 0) {
                        c(i6);
                        backStackRecord4.u = -1;
                    }
                    if (backStackRecord4.f1335r != null) {
                        for (int i26 = 0; i26 < backStackRecord4.f1335r.size(); i26++) {
                            backStackRecord4.f1335r.get(i26).run();
                        }
                        backStackRecord4.f1335r = null;
                    }
                    i4++;
                }
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.k, java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void
     arg types: [i.l.a.FragmentManagerImpl, java.util.ArrayList, java.util.ArrayList, int, int, int]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, java.lang.Object, java.lang.Object, java.lang.Object, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, java.lang.Object, i.e.ArrayMap<java.lang.String, android.view.View>, boolean, i.l.a.a):void
      i.l.a.FragmentTransition3.a(i.l.a.k, java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(int, boolean):void
     arg types: [int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(int, boolean):void */
    public void a(BackStackRecord backStackRecord, boolean z2, boolean z3, boolean z4) {
        if (z2) {
            backStackRecord.b(z4);
        } else {
            backStackRecord.c();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(backStackRecord);
        arrayList2.add(Boolean.valueOf(z2));
        if (z3) {
            FragmentTransition3.a((k) this, (ArrayList<a>) arrayList, (ArrayList<Boolean>) arrayList2, 0, 1, true);
        }
        if (z4) {
            a(this.f1304p, true);
        }
        for (Fragment next : this.h.values()) {
            if (next != null && next.H != null && next.M && backStackRecord.b(next.x)) {
                float f2 = next.O;
                if (f2 > 0.0f) {
                    next.H.setAlpha(f2);
                }
                if (z4) {
                    next.O = 0.0f;
                } else {
                    next.O = -1.0f;
                    next.M = false;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public final void a(ArraySet<Fragment> arraySet) {
        int i2 = this.f1304p;
        if (i2 >= 1) {
            int min = Math.min(i2, 3);
            int size = this.g.size();
            for (int i3 = 0; i3 < size; i3++) {
                Fragment fragment = this.g.get(i3);
                if (fragment.b < min) {
                    a(fragment, min, fragment.s(), fragment.t(), false);
                    if (fragment.H != null && !fragment.z && fragment.M) {
                        arraySet.add(fragment);
                    }
                }
            }
        }
    }

    public boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, String str, int i2, int i3) {
        int i4;
        ArrayList<a> arrayList3 = this.f1297i;
        if (arrayList3 == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = arrayList3.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.f1297i.remove(size));
            arrayList2.add(true);
        } else {
            if (str != null || i2 >= 0) {
                int size2 = this.f1297i.size() - 1;
                while (size2 >= 0) {
                    BackStackRecord backStackRecord = this.f1297i.get(size2);
                    if ((str != null && str.equals(backStackRecord.f1327j)) || (i2 >= 0 && i2 == backStackRecord.u)) {
                        break;
                    }
                    size2--;
                }
                if (size2 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    while (true) {
                        size2--;
                        if (size2 < 0) {
                            break;
                        }
                        BackStackRecord backStackRecord2 = this.f1297i.get(size2);
                        if ((str == null || !str.equals(backStackRecord2.f1327j)) && (i2 < 0 || i2 != backStackRecord2.u)) {
                            break;
                        }
                    }
                }
                i4 = size2;
            } else {
                i4 = -1;
            }
            if (i4 == this.f1297i.size() - 1) {
                return false;
            }
            for (int size3 = this.f1297i.size() - 1; size3 > i4; size3--) {
                arrayList.add(this.f1297i.remove(size3));
                arrayList2.add(true);
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public void a(Parcelable parcelable) {
        FragmentState fragmentState;
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.b != null) {
                for (Fragment next : this.F.b) {
                    Iterator<q> it = fragmentManagerState.b.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            fragmentState = null;
                            break;
                        }
                        fragmentState = (FragmentState) it.next();
                        if (fragmentState.c.equals(next.f221f)) {
                            break;
                        }
                    }
                    if (fragmentState == null) {
                        Fragment fragment = next;
                        a(fragment, 1, 0, 0, false);
                        next.f226m = true;
                        a(fragment, 0, 0, 0, false);
                    } else {
                        fragmentState.f1323o = next;
                        next.d = null;
                        next.f231r = 0;
                        next.f228o = false;
                        next.f225l = false;
                        Fragment fragment2 = next.h;
                        next.f222i = fragment2 != null ? fragment2.f221f : null;
                        next.h = null;
                        Bundle bundle = fragmentState.f1322n;
                        if (bundle != null) {
                            bundle.setClassLoader(this.f1305q.c.getClassLoader());
                            next.d = fragmentState.f1322n.getSparseParcelableArray("android:view_state");
                            next.c = fragmentState.f1322n;
                        }
                    }
                }
                this.h.clear();
                Iterator<q> it2 = fragmentManagerState.b.iterator();
                while (it2.hasNext()) {
                    FragmentState next2 = it2.next();
                    if (next2 != null) {
                        ClassLoader classLoader = this.f1305q.c.getClassLoader();
                        FragmentFactory l2 = l();
                        if (next2.f1323o == null) {
                            Bundle bundle2 = next2.f1319k;
                            if (bundle2 != null) {
                                bundle2.setClassLoader(classLoader);
                            }
                            Fragment a2 = l2.a(classLoader, next2.b);
                            next2.f1323o = a2;
                            a2.f(next2.f1319k);
                            Bundle bundle3 = next2.f1322n;
                            if (bundle3 != null) {
                                bundle3.setClassLoader(classLoader);
                                next2.f1323o.c = next2.f1322n;
                            } else {
                                next2.f1323o.c = new Bundle();
                            }
                            Fragment fragment3 = next2.f1323o;
                            fragment3.f221f = next2.c;
                            fragment3.f227n = next2.d;
                            fragment3.f229p = true;
                            fragment3.w = next2.f1315e;
                            fragment3.x = next2.f1316f;
                            fragment3.y = next2.g;
                            fragment3.B = next2.h;
                            fragment3.f226m = next2.f1317i;
                            fragment3.A = next2.f1318j;
                            fragment3.z = next2.f1320l;
                            fragment3.R = Lifecycle.b.values()[next2.f1321m];
                            if (H) {
                                StringBuilder a3 = outline.a("Instantiated fragment ");
                                a3.append(next2.f1323o);
                                Log.v("FragmentManager", a3.toString());
                            }
                        }
                        Fragment fragment4 = next2.f1323o;
                        fragment4.f232s = this;
                        this.h.put(fragment4.f221f, fragment4);
                        next2.f1323o = null;
                    }
                }
                this.g.clear();
                ArrayList<String> arrayList = fragmentManagerState.c;
                if (arrayList != null) {
                    Iterator<String> it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        String next3 = it3.next();
                        Fragment fragment5 = this.h.get(next3);
                        if (fragment5 != null) {
                            fragment5.f225l = true;
                            if (!this.g.contains(fragment5)) {
                                synchronized (this.g) {
                                    this.g.add(fragment5);
                                }
                            } else {
                                throw new IllegalStateException("Already added " + fragment5);
                            }
                        } else {
                            a(new IllegalStateException(outline.a("No instantiated fragment for (", next3, ")")));
                            throw null;
                        }
                    }
                }
                if (fragmentManagerState.d != null) {
                    this.f1297i = new ArrayList<>(fragmentManagerState.d.length);
                    int i2 = 0;
                    while (true) {
                        BackStackState[] backStackStateArr = fragmentManagerState.d;
                        if (i2 >= backStackStateArr.length) {
                            break;
                        }
                        BackStackState backStackState = backStackStateArr[i2];
                        if (backStackState != null) {
                            BackStackRecord backStackRecord = new BackStackRecord(this);
                            int i3 = 0;
                            int i4 = 0;
                            while (i3 < backStackState.b.length) {
                                FragmentTransaction.a aVar = new FragmentTransaction.a();
                                int i5 = i3 + 1;
                                aVar.a = backStackState.b[i3];
                                if (H) {
                                    Log.v("FragmentManager", "Instantiate " + backStackRecord + " op #" + i4 + " base fragment #" + backStackState.b[i5]);
                                }
                                String str = backStackState.c.get(i4);
                                if (str != null) {
                                    aVar.b = this.h.get(str);
                                } else {
                                    aVar.b = null;
                                }
                                aVar.g = Lifecycle.b.values()[backStackState.d[i4]];
                                aVar.h = Lifecycle.b.values()[backStackState.f1277e[i4]];
                                int[] iArr = backStackState.b;
                                int i6 = i5 + 1;
                                int i7 = iArr[i5];
                                aVar.c = i7;
                                int i8 = i6 + 1;
                                int i9 = iArr[i6];
                                aVar.d = i9;
                                int i10 = i8 + 1;
                                int i11 = iArr[i8];
                                aVar.f1336e = i11;
                                int i12 = iArr[i10];
                                aVar.f1337f = i12;
                                backStackRecord.b = i7;
                                backStackRecord.c = i9;
                                backStackRecord.d = i11;
                                backStackRecord.f1324e = i12;
                                backStackRecord.a(aVar);
                                i4++;
                                i3 = i10 + 1;
                            }
                            backStackRecord.f1325f = backStackState.f1278f;
                            backStackRecord.g = backStackState.g;
                            backStackRecord.f1327j = backStackState.h;
                            backStackRecord.u = backStackState.f1279i;
                            backStackRecord.h = true;
                            backStackRecord.f1328k = backStackState.f1280j;
                            backStackRecord.f1329l = backStackState.f1281k;
                            backStackRecord.f1330m = backStackState.f1282l;
                            backStackRecord.f1331n = backStackState.f1283m;
                            backStackRecord.f1332o = backStackState.f1284n;
                            backStackRecord.f1333p = backStackState.f1285o;
                            backStackRecord.f1334q = backStackState.f1286p;
                            backStackRecord.a(1);
                            this.f1297i.add(backStackRecord);
                            int i13 = backStackRecord.u;
                            if (i13 >= 0) {
                                a(i13, backStackRecord);
                            }
                            i2++;
                        } else {
                            throw null;
                        }
                    }
                } else {
                    this.f1297i = null;
                }
                String str2 = fragmentManagerState.f1311e;
                if (str2 != null) {
                    Fragment fragment6 = this.h.get(str2);
                    this.f1308t = fragment6;
                    c(fragment6);
                }
                this.f1296f = fragmentManagerState.f1312f;
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v17, resolved type: i.a.OnBackPressedDispatcherOwner} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v18, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v21, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v22, resolved type: androidx.fragment.app.Fragment} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(i.l.a.FragmentHostCallback r5, i.l.a.FragmentContainer r6, androidx.fragment.app.Fragment r7) {
        /*
            r4 = this;
            i.l.a.FragmentHostCallback r0 = r4.f1305q
            if (r0 != 0) goto L_0x00bf
            r4.f1305q = r5
            r4.f1306r = r6
            r4.f1307s = r7
            if (r7 == 0) goto L_0x000f
            r4.r()
        L_0x000f:
            boolean r6 = r5 instanceof i.a.OnBackPressedDispatcherOwner
            if (r6 == 0) goto L_0x0040
            r6 = r5
            i.a.OnBackPressedDispatcherOwner r6 = (i.a.OnBackPressedDispatcherOwner) r6
            androidx.activity.OnBackPressedDispatcher r0 = r6.c()
            r4.f1299k = r0
            if (r7 == 0) goto L_0x001f
            r6 = r7
        L_0x001f:
            androidx.activity.OnBackPressedDispatcher r0 = r4.f1299k
            i.a.OnBackPressedCallback r1 = r4.f1300l
            if (r0 == 0) goto L_0x003e
            i.o.Lifecycle r6 = r6.a()
            r2 = r6
            i.o.LifecycleRegistry r2 = (i.o.LifecycleRegistry) r2
            i.o.Lifecycle$b r2 = r2.b
            i.o.Lifecycle$b r3 = i.o.Lifecycle.b.DESTROYED
            if (r2 != r3) goto L_0x0033
            goto L_0x0040
        L_0x0033:
            androidx.activity.OnBackPressedDispatcher$LifecycleOnBackPressedCancellable r2 = new androidx.activity.OnBackPressedDispatcher$LifecycleOnBackPressedCancellable
            r2.<init>(r6, r1)
            java.util.concurrent.CopyOnWriteArrayList<i.a.a> r6 = r1.b
            r6.add(r2)
            goto L_0x0040
        L_0x003e:
            r5 = 0
            throw r5
        L_0x0040:
            if (r7 == 0) goto L_0x0063
            i.l.a.FragmentManagerImpl r5 = r7.f232s
            i.l.a.FragmentManagerViewModel r5 = r5.F
            java.util.HashMap<java.lang.String, i.l.a.p> r6 = r5.c
            java.lang.String r0 = r7.f221f
            java.lang.Object r6 = r6.get(r0)
            i.l.a.FragmentManagerViewModel r6 = (i.l.a.FragmentManagerViewModel) r6
            if (r6 != 0) goto L_0x0060
            i.l.a.FragmentManagerViewModel r6 = new i.l.a.FragmentManagerViewModel
            boolean r0 = r5.f1313e
            r6.<init>(r0)
            java.util.HashMap<java.lang.String, i.l.a.p> r5 = r5.c
            java.lang.String r7 = r7.f221f
            r5.put(r7, r6)
        L_0x0060:
            r4.F = r6
            goto L_0x00be
        L_0x0063:
            boolean r6 = r5 instanceof i.o.ViewModelStoreOwner
            if (r6 == 0) goto L_0x00b6
            i.o.ViewModelStoreOwner r5 = (i.o.ViewModelStoreOwner) r5
            i.o.ViewModelStore r5 = r5.f()
            i.o.ViewModelProvider$b r6 = i.l.a.FragmentManagerViewModel.h
            java.lang.Class<i.l.a.FragmentManagerViewModel> r7 = i.l.a.FragmentManagerViewModel.class
            java.lang.String r0 = r7.getCanonicalName()
            if (r0 == 0) goto L_0x00ae
            java.lang.String r1 = "androidx.lifecycle.ViewModelProvider.DefaultKey:"
            java.lang.String r0 = j.a.a.a.outline.a(r1, r0)
            java.util.HashMap<java.lang.String, i.o.v> r1 = r5.a
            java.lang.Object r1 = r1.get(r0)
            i.o.ViewModel r1 = (i.o.ViewModel) r1
            boolean r2 = r7.isInstance(r1)
            if (r2 == 0) goto L_0x008c
            goto L_0x00a9
        L_0x008c:
            boolean r1 = r6 instanceof i.o.ViewModelProvider.c
            if (r1 == 0) goto L_0x0097
            i.o.ViewModelProvider$c r6 = (i.o.ViewModelProvider.c) r6
            i.o.ViewModel r6 = r6.a(r0, r7)
            goto L_0x009b
        L_0x0097:
            i.o.ViewModel r6 = r6.a(r7)
        L_0x009b:
            r1 = r6
            java.util.HashMap<java.lang.String, i.o.v> r5 = r5.a
            java.lang.Object r5 = r5.put(r0, r1)
            i.o.ViewModel r5 = (i.o.ViewModel) r5
            if (r5 == 0) goto L_0x00a9
            r5.b()
        L_0x00a9:
            i.l.a.FragmentManagerViewModel r1 = (i.l.a.FragmentManagerViewModel) r1
            r4.F = r1
            goto L_0x00be
        L_0x00ae:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Local and anonymous classes can not be ViewModels"
            r5.<init>(r6)
            throw r5
        L_0x00b6:
            i.l.a.FragmentManagerViewModel r5 = new i.l.a.FragmentManagerViewModel
            r6 = 0
            r5.<init>(r6)
            r4.F = r5
        L_0x00be:
            return
        L_0x00bf:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "Already attached"
            r5.<init>(r6)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.a(i.l.a.FragmentHostCallback, i.l.a.FragmentContainer, androidx.fragment.app.Fragment):void");
    }

    public void a(boolean z2) {
        int size = this.g.size();
        while (true) {
            size--;
            if (size >= 0) {
                Fragment fragment = this.g.get(size);
                if (fragment != null) {
                    fragment.u.a(z2);
                }
            } else {
                return;
            }
        }
    }

    public void a(Configuration configuration) {
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null) {
                fragment.F = true;
                fragment.u.a(configuration);
            }
        }
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z2;
        if (this.f1304p < 1) {
            return false;
        }
        ArrayList<Fragment> arrayList = null;
        boolean z3 = false;
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null) {
                if (!fragment.z) {
                    z2 = (fragment.D && fragment.E) | fragment.u.a(menu, menuInflater);
                } else {
                    z2 = false;
                }
                if (z2) {
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(fragment);
                    z3 = true;
                }
            }
        }
        if (this.f1298j != null) {
            for (int i3 = 0; i3 < this.f1298j.size(); i3++) {
                Fragment fragment2 = this.f1298j.get(i3);
                if ((arrayList == null || !arrayList.contains(fragment2)) && fragment2 == null) {
                    throw null;
                }
            }
        }
        this.f1298j = arrayList;
        return z3;
    }

    public boolean a(MenuItem menuItem) {
        if (this.f1304p < 1) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null) {
                if (!fragment.z && fragment.u.a(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void a(Menu menu) {
        if (this.f1304p >= 1) {
            for (int i2 = 0; i2 < this.g.size(); i2++) {
                Fragment fragment = this.g.get(i2);
                if (fragment != null && !fragment.z) {
                    boolean z2 = fragment.D;
                    fragment.u.a(menu);
                }
            }
        }
    }

    public void a(Fragment fragment, Lifecycle.b bVar) {
        if (this.h.get(fragment.f221f) == fragment && (fragment.f233t == null || fragment.f232s == this)) {
            fragment.R = bVar;
            return;
        }
        throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentHostCallback, i.l.a.FragmentContainer, androidx.fragment.app.Fragment):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.Fragment r3, android.content.Context r4, boolean r5) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.a(r3, r4, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r5 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentHostCallback, i.l.a.FragmentContainer, androidx.fragment.app.Fragment):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.Fragment r3, android.os.Bundle r4, boolean r5) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.a(r3, r4, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r5 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(float, float, float, float):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, boolean, int):i.l.a.FragmentManagerImpl$d
      i.l.a.FragmentManagerImpl.a(i.l.a.BackStackRecord, boolean, boolean, boolean):void
      i.l.a.FragmentManagerImpl.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      i.l.a.FragmentManager.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.Fragment r3, android.view.View r4, android.os.Bundle r5, boolean r6) {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.f1307s
            if (r0 == 0) goto L_0x000e
            i.l.a.FragmentManagerImpl r0 = r0.f232s
            boolean r1 = r0 instanceof i.l.a.FragmentManagerImpl
            if (r1 == 0) goto L_0x000e
            r1 = 1
            r0.a(r3, r4, r5, r1)
        L_0x000e:
            java.util.concurrent.CopyOnWriteArrayList<i.l.a.k$f> r3 = r2.f1303o
            java.util.Iterator r3 = r3.iterator()
        L_0x0014:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0029
            java.lang.Object r4 = r3.next()
            i.l.a.FragmentManagerImpl$f r4 = (i.l.a.FragmentManagerImpl.f) r4
            if (r6 == 0) goto L_0x0027
            boolean r4 = r4.a
            if (r4 != 0) goto L_0x0027
            goto L_0x0014
        L_0x0027:
            r3 = 0
            throw r3
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void");
    }
}
