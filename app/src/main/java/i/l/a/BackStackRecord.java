package i.l.a;

import android.util.Log;
import androidx.fragment.app.Fragment;
import i.h.k.LogWriter;
import i.l.a.FragmentManager;
import i.l.a.FragmentManagerImpl;
import i.l.a.FragmentTransaction;
import j.a.a.a.outline;
import java.io.PrintWriter;
import java.util.ArrayList;
import o.m0.c.DiskLruCache;

public final class BackStackRecord extends FragmentTransaction implements FragmentManager.a, FragmentManagerImpl.h {

    /* renamed from: s  reason: collision with root package name */
    public final FragmentManagerImpl f1275s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f1276t;
    public int u = -1;

    public BackStackRecord(FragmentManagerImpl fragmentManagerImpl) {
        this.f1275s = fragmentManagerImpl;
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(super.f1327j);
            printWriter.print(" mIndex=");
            printWriter.print(this.u);
            printWriter.print(" mCommitted=");
            printWriter.println(this.f1276t);
            if (super.f1325f != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(super.f1325f));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(super.g));
            }
            if (!(super.b == 0 && super.c == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(super.b));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(super.c));
            }
            if (!(super.d == 0 && super.f1324e == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(super.d));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(super.f1324e));
            }
            if (!(super.f1328k == 0 && super.f1329l == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(super.f1328k));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(super.f1329l);
            }
            if (!(super.f1330m == 0 && super.f1331n == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(super.f1330m));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(super.f1331n);
            }
        }
        if (!super.a.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = super.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                FragmentTransaction.a aVar = super.a.get(i2);
                switch (aVar.a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = DiskLruCache.y;
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        StringBuilder a = outline.a("cmd=");
                        a.append(aVar.a);
                        str2 = a.toString();
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.b);
                if (z) {
                    if (!(aVar.c == 0 && aVar.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.d));
                    }
                    if (aVar.f1336e != 0 || aVar.f1337f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.f1336e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f1337f));
                    }
                }
            }
        }
    }

    public int b() {
        return a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(int, boolean):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(int, boolean):void
     arg types: [int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(int, boolean):void */
    public void c() {
        int size = super.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            FragmentTransaction.a aVar = super.a.get(i2);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                int i3 = super.f1325f;
                int i4 = super.g;
                if (!(fragment.L == null && i3 == 0 && i4 == 0)) {
                    fragment.k();
                    Fragment.b bVar = fragment.L;
                    bVar.f234e = i3;
                    bVar.f235f = i4;
                }
            }
            switch (aVar.a) {
                case 1:
                    fragment.b(aVar.c);
                    this.f1275s.a(fragment, false);
                    break;
                case 2:
                default:
                    StringBuilder a = outline.a("Unknown cmd: ");
                    a.append(aVar.a);
                    throw new IllegalArgumentException(a.toString());
                case 3:
                    fragment.b(aVar.d);
                    this.f1275s.h(fragment);
                    break;
                case 4:
                    fragment.b(aVar.d);
                    if (this.f1275s != null) {
                        if (!fragment.z) {
                            fragment.z = true;
                            fragment.N = !fragment.N;
                            break;
                        }
                    } else {
                        throw null;
                    }
                    break;
                case 5:
                    fragment.b(aVar.c);
                    if (this.f1275s != null) {
                        if (fragment.z) {
                            fragment.z = false;
                            fragment.N = !fragment.N;
                            break;
                        }
                    } else {
                        throw null;
                    }
                    break;
                case 6:
                    fragment.b(aVar.d);
                    this.f1275s.b(fragment);
                    break;
                case 7:
                    fragment.b(aVar.c);
                    this.f1275s.a(fragment);
                    break;
                case 8:
                    this.f1275s.k(fragment);
                    break;
                case 9:
                    this.f1275s.k(null);
                    break;
                case 10:
                    this.f1275s.a(fragment, aVar.h);
                    break;
            }
            if (!(super.f1334q || aVar.a == 1 || fragment == null)) {
                this.f1275s.g(fragment);
            }
        }
        if (!super.f1334q) {
            FragmentManagerImpl fragmentManagerImpl = this.f1275s;
            fragmentManagerImpl.a(fragmentManagerImpl.f1304p, true);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.u >= 0) {
            sb.append(" #");
            sb.append(this.u);
        }
        if (super.f1327j != null) {
            sb.append(" ");
            sb.append(super.f1327j);
        }
        sb.append("}");
        return sb.toString();
    }

    public boolean b(int i2) {
        int size = super.a.size();
        for (int i3 = 0; i3 < size; i3++) {
            Fragment fragment = super.a.get(i3).b;
            int i4 = fragment != null ? fragment.x : 0;
            if (i4 != 0 && i4 == i2) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(int, boolean):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(int, boolean):void
     arg types: [int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(int, boolean):void */
    public void b(boolean z) {
        for (int size = super.a.size() - 1; size >= 0; size--) {
            FragmentTransaction.a aVar = super.a.get(size);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                int d = FragmentManagerImpl.d(super.f1325f);
                int i2 = super.g;
                if (!(fragment.L == null && d == 0 && i2 == 0)) {
                    fragment.k();
                    Fragment.b bVar = fragment.L;
                    bVar.f234e = d;
                    bVar.f235f = i2;
                }
            }
            switch (aVar.a) {
                case 1:
                    fragment.b(aVar.f1337f);
                    this.f1275s.h(fragment);
                    break;
                case 2:
                default:
                    StringBuilder a = outline.a("Unknown cmd: ");
                    a.append(aVar.a);
                    throw new IllegalArgumentException(a.toString());
                case 3:
                    fragment.b(aVar.f1336e);
                    this.f1275s.a(fragment, false);
                    break;
                case 4:
                    fragment.b(aVar.f1336e);
                    if (this.f1275s != null) {
                        if (fragment.z) {
                            fragment.z = false;
                            fragment.N = !fragment.N;
                            break;
                        }
                    } else {
                        throw null;
                    }
                    break;
                case 5:
                    fragment.b(aVar.f1337f);
                    if (this.f1275s != null) {
                        if (!fragment.z) {
                            fragment.z = true;
                            fragment.N = !fragment.N;
                            break;
                        }
                    } else {
                        throw null;
                    }
                    break;
                case 6:
                    fragment.b(aVar.f1336e);
                    this.f1275s.a(fragment);
                    break;
                case 7:
                    fragment.b(aVar.f1337f);
                    this.f1275s.b(fragment);
                    break;
                case 8:
                    this.f1275s.k(null);
                    break;
                case 9:
                    this.f1275s.k(fragment);
                    break;
                case 10:
                    this.f1275s.a(fragment, aVar.g);
                    break;
            }
            if (!(super.f1334q || aVar.a == 3 || fragment == null)) {
                this.f1275s.g(fragment);
            }
        }
        if (!super.f1334q && z) {
            FragmentManagerImpl fragmentManagerImpl = this.f1275s;
            fragmentManagerImpl.a(fragmentManagerImpl.f1304p, true);
        }
    }

    public static boolean b(FragmentTransaction.a aVar) {
        boolean z;
        Fragment fragment = aVar.b;
        if (fragment == null || !fragment.f225l || fragment.H == null || fragment.A || fragment.z) {
            return false;
        }
        Fragment.b bVar = fragment.L;
        if (bVar == null) {
            z = false;
        } else {
            z = bVar.f244q;
        }
        if (z) {
            return true;
        }
        return false;
    }

    public void a(int i2, Fragment fragment, String str, int i3) {
        super.a(i2, fragment, str, i3);
        fragment.f232s = this.f1275s;
    }

    public void a(int i2) {
        if (super.h) {
            if (FragmentManagerImpl.H) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            int size = super.a.size();
            for (int i3 = 0; i3 < size; i3++) {
                FragmentTransaction.a aVar = super.a.get(i3);
                Fragment fragment = aVar.b;
                if (fragment != null) {
                    fragment.f231r += i2;
                    if (FragmentManagerImpl.H) {
                        StringBuilder a = outline.a("Bump nesting of ");
                        a.append(aVar.b);
                        a.append(" to ");
                        a.append(aVar.b.f231r);
                        Log.v("FragmentManager", a.toString());
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.BackStackRecord.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      i.l.a.BackStackRecord.a(java.util.ArrayList<i.l.a.a>, int, int):boolean
      i.l.a.BackStackRecord.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public int a(boolean z) {
        if (!this.f1276t) {
            if (FragmentManagerImpl.H) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
                a("  ", printWriter, true);
                printWriter.close();
            }
            this.f1276t = true;
            if (super.h) {
                this.u = this.f1275s.a(this);
            } else {
                this.u = -1;
            }
            this.f1275s.a(this, z);
            return this.u;
        }
        throw new IllegalStateException("commit already called");
    }

    public boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
        if (FragmentManagerImpl.H) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!super.h) {
            return true;
        }
        FragmentManagerImpl fragmentManagerImpl = this.f1275s;
        if (fragmentManagerImpl.f1297i == null) {
            fragmentManagerImpl.f1297i = new ArrayList<>();
        }
        fragmentManagerImpl.f1297i.add(this);
        return true;
    }

    public boolean a(ArrayList<a> arrayList, int i2, int i3) {
        if (i3 == i2) {
            return false;
        }
        int size = super.a.size();
        int i4 = -1;
        for (int i5 = 0; i5 < size; i5++) {
            Fragment fragment = super.a.get(i5).b;
            int i6 = fragment != null ? fragment.x : 0;
            if (!(i6 == 0 || i6 == i4)) {
                for (int i7 = i2; i7 < i3; i7++) {
                    BackStackRecord backStackRecord = arrayList.get(i7);
                    int size2 = super.a.size();
                    for (int i8 = 0; i8 < size2; i8++) {
                        Fragment fragment2 = super.a.get(i8).b;
                        if ((fragment2 != null ? fragment2.x : 0) == i6) {
                            return true;
                        }
                    }
                }
                i4 = i6;
            }
        }
        return false;
    }

    public String a() {
        return super.f1327j;
    }
}
