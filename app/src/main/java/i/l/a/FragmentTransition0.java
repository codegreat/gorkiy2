package i.l.a;

import android.view.View;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;

/* compiled from: FragmentTransition */
public final class FragmentTransition0 implements Runnable {
    public final /* synthetic */ Object b;
    public final /* synthetic */ FragmentTransitionImpl0 c;
    public final /* synthetic */ View d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Fragment f1338e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ ArrayList f1339f;
    public final /* synthetic */ ArrayList g;
    public final /* synthetic */ ArrayList h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ Object f1340i;

    public FragmentTransition0(Object obj, FragmentTransitionImpl0 fragmentTransitionImpl0, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
        this.b = obj;
        this.c = fragmentTransitionImpl0;
        this.d = view;
        this.f1338e = fragment;
        this.f1339f = arrayList;
        this.g = arrayList2;
        this.h = arrayList3;
        this.f1340i = obj2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
     arg types: [i.l.a.FragmentTransitionImpl0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList, android.view.View]
     candidates:
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void
      i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View> */
    public void run() {
        Object obj = this.b;
        if (obj != null) {
            this.c.b(obj, this.d);
            this.g.addAll(FragmentTransition3.a((b0) this.c, this.b, this.f1338e, (ArrayList<View>) this.f1339f, this.d));
        }
        if (this.h != null) {
            if (this.f1340i != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(this.d);
                this.c.a(this.f1340i, (ArrayList<View>) this.h, (ArrayList<View>) arrayList);
            }
            this.h.clear();
            this.h.add(this.d);
        }
    }
}
