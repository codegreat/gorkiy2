package i.o;

import i.o.Lifecycle;

public interface LifecycleEventObserver extends LifecycleObserver {
    void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar);
}
