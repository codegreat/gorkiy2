package i.h.d;

import android.util.Log;

/* compiled from: ActivityRecreator */
public final class ActivityRecreator0 implements Runnable {
    public final /* synthetic */ Object b;
    public final /* synthetic */ Object c;

    public ActivityRecreator0(Object obj, Object obj2) {
        this.b = obj;
        this.c = obj2;
    }

    public void run() {
        try {
            if (ActivityRecreator.d != null) {
                ActivityRecreator.d.invoke(this.b, this.c, false, "AppCompat recreation");
                return;
            }
            ActivityRecreator.f1153e.invoke(this.b, this.c, false);
        } catch (RuntimeException e2) {
            if (e2.getClass() == RuntimeException.class && e2.getMessage() != null && e2.getMessage().startsWith("Unable to stop")) {
                throw e2;
            }
        } catch (Throwable th) {
            Log.e("ActivityRecreator", "Exception while invoking performStopActivity", th);
        }
    }
}
