package i.h.f;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import i.e.LruCache;
import i.h.e.b.FontResourcesParserCompat;
import i.h.e.b.FontResourcesParserCompat1;
import i.h.e.b.FontResourcesParserCompat2;
import i.h.e.b.ResourcesCompat;
import i.h.i.FontsContractCompat;

@SuppressLint({"NewApi"})
public class TypefaceCompat {
    public static final TypefaceCompatBaseImpl a;
    public static final LruCache<String, Typeface> b = new LruCache<>(16);

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 29) {
            a = new TypefaceCompatApi29Impl();
        } else if (i2 >= 28) {
            a = new TypefaceCompatApi28Impl();
        } else if (i2 >= 26) {
            a = new TypefaceCompatApi26Impl();
        } else {
            if (i2 >= 24) {
                if (TypefaceCompatApi24Impl.d == null) {
                    Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
                }
                if (TypefaceCompatApi24Impl.d != null) {
                    a = new TypefaceCompatApi24Impl();
                }
            }
            a = new TypefaceCompatApi21Impl();
        }
    }

    public static String a(Resources resources, int i2, int i3) {
        return resources.getResourcePackageName(i2) + "-" + i2 + "-" + i3;
    }

    public static Typeface a(Context context, FontResourcesParserCompat fontResourcesParserCompat, Resources resources, int i2, int i3, ResourcesCompat resourcesCompat, Handler handler, boolean z) {
        Typeface typeface;
        if (fontResourcesParserCompat instanceof FontResourcesParserCompat2) {
            FontResourcesParserCompat2 fontResourcesParserCompat2 = (FontResourcesParserCompat2) fontResourcesParserCompat;
            boolean z2 = false;
            if (!z ? resourcesCompat == null : fontResourcesParserCompat2.c == 0) {
                z2 = true;
            }
            typeface = FontsContractCompat.a(context, fontResourcesParserCompat2.a, resourcesCompat, handler, z2, z ? fontResourcesParserCompat2.b : -1, i3);
        } else {
            typeface = a.a(context, (FontResourcesParserCompat1) fontResourcesParserCompat, resources, i3);
            if (resourcesCompat != null) {
                if (typeface != null) {
                    resourcesCompat.a(typeface, handler);
                } else {
                    resourcesCompat.a(-3, handler);
                }
            }
        }
        if (typeface != null) {
            b.a(a(resources, i2, i3), typeface);
        }
        return typeface;
    }

    public static Typeface a(Context context, Resources resources, int i2, String str, int i3) {
        Typeface a2 = a.a(context, resources, i2, str, i3);
        if (a2 != null) {
            b.a(a(resources, i2, i3), a2);
        }
        return a2;
    }

    public static Typeface a(Context context, Typeface typeface, int i2) {
        if (context != null) {
            return Typeface.create(typeface, i2);
        }
        throw new IllegalArgumentException("Context cannot be null");
    }
}
