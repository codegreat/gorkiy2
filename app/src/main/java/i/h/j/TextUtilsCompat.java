package i.h.j;

import android.text.TextUtils;
import java.util.Locale;

public final class TextUtilsCompat {
    public static final Locale a = new Locale("", "");

    public static int a(Locale locale) {
        return TextUtils.getLayoutDirectionFromLocale(locale);
    }
}
