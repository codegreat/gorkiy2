package i.h.k;

/* compiled from: Pools */
public class Pools0<T> implements Pools<T> {
    public final Object[] a;
    public int b;

    public Pools0(int i2) {
        if (i2 > 0) {
            this.a = new Object[i2];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }

    public T a() {
        int i2 = this.b;
        if (i2 <= 0) {
            return null;
        }
        int i3 = i2 - 1;
        T[] tArr = this.a;
        T t2 = tArr[i3];
        tArr[i3] = null;
        this.b = i2 - 1;
        return t2;
    }

    public boolean a(T t2) {
        boolean z;
        int i2 = 0;
        while (true) {
            if (i2 >= this.b) {
                z = false;
                break;
            } else if (this.a[i2] == t2) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (!z) {
            int i3 = this.b;
            Object[] objArr = this.a;
            if (i3 >= objArr.length) {
                return false;
            }
            objArr[i3] = t2;
            this.b = i3 + 1;
            return true;
        }
        throw new IllegalStateException("Already in the pool!");
    }
}
