package i.w;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.graphics.Path;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import androidx.recyclerview.widget.RecyclerView;
import i.e.ArrayMap;
import i.e.ContainerHelpers;
import i.e.LongSparseArray;
import i.h.l.ViewCompat;
import i.w.i;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class Transition implements Cloneable {
    public static final int[] G = {2, 1, 3, 4};
    public static final PathMotion H = new a();
    public static ThreadLocal<ArrayMap<Animator, i.b>> I = new ThreadLocal<>();
    public boolean A = false;
    public ArrayList<i.d> B = null;
    public ArrayList<Animator> C = new ArrayList<>();
    public TransitionPropagation D;
    public c E;
    public PathMotion F = H;
    public String b = getClass().getName();
    public long c = -1;
    public long d = -1;

    /* renamed from: e  reason: collision with root package name */
    public TimeInterpolator f1452e = null;

    /* renamed from: f  reason: collision with root package name */
    public ArrayList<Integer> f1453f = new ArrayList<>();
    public ArrayList<View> g = new ArrayList<>();
    public ArrayList<String> h = null;

    /* renamed from: i  reason: collision with root package name */
    public ArrayList<Class<?>> f1454i = null;

    /* renamed from: j  reason: collision with root package name */
    public ArrayList<Integer> f1455j = null;

    /* renamed from: k  reason: collision with root package name */
    public ArrayList<View> f1456k = null;

    /* renamed from: l  reason: collision with root package name */
    public ArrayList<Class<?>> f1457l = null;

    /* renamed from: m  reason: collision with root package name */
    public ArrayList<String> f1458m = null;

    /* renamed from: n  reason: collision with root package name */
    public ArrayList<Integer> f1459n = null;

    /* renamed from: o  reason: collision with root package name */
    public ArrayList<View> f1460o = null;

    /* renamed from: p  reason: collision with root package name */
    public ArrayList<Class<?>> f1461p = null;

    /* renamed from: q  reason: collision with root package name */
    public TransitionValuesMaps f1462q = new TransitionValuesMaps();

    /* renamed from: r  reason: collision with root package name */
    public TransitionValuesMaps f1463r = new TransitionValuesMaps();

    /* renamed from: s  reason: collision with root package name */
    public TransitionSet f1464s = null;

    /* renamed from: t  reason: collision with root package name */
    public int[] f1465t = G;
    public ArrayList<q> u;
    public ArrayList<q> v;
    public boolean w = false;
    public ArrayList<Animator> x = new ArrayList<>();
    public int y = 0;
    public boolean z = false;

    public static class a extends PathMotion {
        public Path a(float f2, float f3, float f4, float f5) {
            Path path = new Path();
            path.moveTo(f2, f3);
            path.lineTo(f4, f5);
            return path;
        }
    }

    public static class b {
        public View a;
        public String b;
        public TransitionValues c;
        public WindowIdImpl d;

        /* renamed from: e  reason: collision with root package name */
        public Transition f1466e;

        public b(View view, String str, Transition transition, WindowIdImpl windowIdImpl, TransitionValues transitionValues) {
            this.a = view;
            this.b = str;
            this.c = transitionValues;
            this.d = windowIdImpl;
            this.f1466e = transition;
        }
    }

    public static abstract class c {
    }

    public interface d {
        void a(Transition transition);

        void b(Transition transition);

        void c(Transition transition);

        void d(Transition transition);

        void e(Transition transition);
    }

    public static ArrayMap<Animator, i.b> i() {
        ArrayMap<Animator, i.b> arrayMap = I.get();
        if (arrayMap != null) {
            return arrayMap;
        }
        ArrayMap<Animator, i.b> arrayMap2 = new ArrayMap<>();
        I.set(arrayMap2);
        return arrayMap2;
    }

    public Animator a(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return null;
    }

    public Transition a(long j2) {
        this.d = j2;
        return this;
    }

    public abstract void a(TransitionValues transitionValues);

    public Transition b(long j2) {
        this.c = j2;
        return this;
    }

    public void b(TransitionValues transitionValues) {
    }

    public TransitionValues c(View view, boolean z2) {
        TransitionSet transitionSet = this.f1464s;
        if (transitionSet != null) {
            return transitionSet.c(view, z2);
        }
        return (z2 ? this.f1462q : this.f1463r).a.getOrDefault(view, null);
    }

    public abstract void c(TransitionValues transitionValues);

    public String[] c() {
        return null;
    }

    public void cancel() {
        for (int size = this.x.size() - 1; size >= 0; size--) {
            this.x.get(size).cancel();
        }
        ArrayList<i.d> arrayList = this.B;
        if (arrayList != null && arrayList.size() > 0) {
            ArrayList arrayList2 = (ArrayList) this.B.clone();
            int size2 = arrayList2.size();
            for (int i2 = 0; i2 < size2; i2++) {
                ((d) arrayList2.get(i2)).d(this);
            }
        }
    }

    public void d() {
        e();
        ArrayMap<Animator, i.b> i2 = i();
        Iterator<Animator> it = this.C.iterator();
        while (it.hasNext()) {
            Animator next = it.next();
            if (i2.containsKey(next)) {
                e();
                if (next != null) {
                    next.addListener(new Transition0(this, i2));
                    long j2 = this.d;
                    if (j2 >= 0) {
                        next.setDuration(j2);
                    }
                    long j3 = this.c;
                    if (j3 >= 0) {
                        next.setStartDelay(next.getStartDelay() + j3);
                    }
                    TimeInterpolator timeInterpolator = this.f1452e;
                    if (timeInterpolator != null) {
                        next.setInterpolator(timeInterpolator);
                    }
                    next.addListener(new Transition1(this));
                    next.start();
                }
            }
        }
        this.C.clear();
        b();
    }

    public void e(View view) {
        if (this.z) {
            if (!this.A) {
                ArrayMap<Animator, i.b> i2 = i();
                int i3 = i2.d;
                WindowIdImpl c2 = ViewUtils.c(view);
                for (int i4 = i3 - 1; i4 >= 0; i4--) {
                    b e2 = i2.e(i4);
                    if (e2.a != null && c2.equals(e2.d)) {
                        i2.c(i4).resume();
                    }
                }
                ArrayList<i.d> arrayList = this.B;
                if (arrayList != null && arrayList.size() > 0) {
                    ArrayList arrayList2 = (ArrayList) this.B.clone();
                    int size = arrayList2.size();
                    for (int i5 = 0; i5 < size; i5++) {
                        ((d) arrayList2.get(i5)).b(this);
                    }
                }
            }
            this.z = false;
        }
    }

    public String toString() {
        return a("");
    }

    public Transition a(TimeInterpolator timeInterpolator) {
        this.f1452e = timeInterpolator;
        return this;
    }

    public boolean b(View view) {
        ArrayList<Class<?>> arrayList;
        ArrayList<String> arrayList2;
        int id = view.getId();
        ArrayList<Integer> arrayList3 = this.f1455j;
        if (arrayList3 != null && arrayList3.contains(Integer.valueOf(id))) {
            return false;
        }
        ArrayList<View> arrayList4 = this.f1456k;
        if (arrayList4 != null && arrayList4.contains(view)) {
            return false;
        }
        ArrayList<Class<?>> arrayList5 = this.f1457l;
        if (arrayList5 != null) {
            int size = arrayList5.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.f1457l.get(i2).isInstance(view)) {
                    return false;
                }
            }
        }
        if (this.f1458m != null && ViewCompat.p(view) != null && this.f1458m.contains(view.getTransitionName())) {
            return false;
        }
        if ((this.f1453f.size() == 0 && this.g.size() == 0 && (((arrayList = this.f1454i) == null || arrayList.isEmpty()) && ((arrayList2 = this.h) == null || arrayList2.isEmpty()))) || this.f1453f.contains(Integer.valueOf(id)) || this.g.contains(view)) {
            return true;
        }
        ArrayList<String> arrayList6 = this.h;
        if (arrayList6 != null && arrayList6.contains(ViewCompat.p(view))) {
            return true;
        }
        if (this.f1454i != null) {
            for (int i3 = 0; i3 < this.f1454i.size(); i3++) {
                if (this.f1454i.get(i3).isInstance(view)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Transition clone() {
        try {
            Transition transition = (Transition) super.clone();
            transition.C = new ArrayList<>();
            transition.f1462q = new TransitionValuesMaps();
            transition.f1463r = new TransitionValuesMaps();
            transition.u = null;
            transition.v = null;
            return transition;
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public void a(ViewGroup viewGroup, r rVar, r rVar2, ArrayList<q> arrayList, ArrayList<q> arrayList2) {
        int i2;
        Animator animator;
        TransitionValues transitionValues;
        View view;
        TransitionValues transitionValues2;
        Animator animator2;
        ArrayMap<Animator, i.b> i3 = i();
        SparseIntArray sparseIntArray = new SparseIntArray();
        int size = arrayList.size();
        int i4 = 0;
        while (i4 < size) {
            TransitionValues transitionValues3 = arrayList.get(i4);
            TransitionValues transitionValues4 = arrayList2.get(i4);
            if (transitionValues3 != null && !transitionValues3.c.contains(this)) {
                transitionValues3 = null;
            }
            if (transitionValues4 != null && !transitionValues4.c.contains(this)) {
                transitionValues4 = null;
            }
            if (!(transitionValues3 == null && transitionValues4 == null)) {
                if (transitionValues3 == null || transitionValues4 == null || a(transitionValues3, transitionValues4)) {
                    Animator a2 = a(viewGroup, transitionValues3, transitionValues4);
                    if (a2 != null) {
                        if (transitionValues4 != null) {
                            View view2 = transitionValues4.b;
                            String[] c2 = c();
                            if (c2 != null && c2.length > 0) {
                                transitionValues2 = new TransitionValues(view2);
                                TransitionValues transitionValues5 = rVar2.a.get(view2);
                                if (transitionValues5 != null) {
                                    int i5 = 0;
                                    while (i5 < c2.length) {
                                        transitionValues2.a.put(c2[i5], transitionValues5.a.get(c2[i5]));
                                        i5++;
                                        a2 = a2;
                                        size = size;
                                        transitionValues5 = transitionValues5;
                                    }
                                }
                                Animator animator3 = a2;
                                i2 = size;
                                int i6 = i3.d;
                                int i7 = 0;
                                while (true) {
                                    if (i7 >= i6) {
                                        animator2 = animator3;
                                        break;
                                    }
                                    b bVar = i3.get(i3.c(i7));
                                    if (bVar.c != null && bVar.a == view2 && bVar.b.equals(this.b) && bVar.c.equals(transitionValues2)) {
                                        animator2 = null;
                                        break;
                                    }
                                    i7++;
                                }
                            } else {
                                i2 = size;
                                animator2 = a2;
                                transitionValues2 = null;
                            }
                            view = view2;
                            animator = animator2;
                            transitionValues = transitionValues2;
                        } else {
                            i2 = size;
                            view = transitionValues3.b;
                            animator = a2;
                            transitionValues = null;
                        }
                        if (animator != null) {
                            i3.put(animator, new b(view, this.b, this, ViewUtils.c(viewGroup), transitionValues));
                            this.C.add(animator);
                        }
                        i4++;
                        size = i2;
                    }
                    i2 = size;
                    i4++;
                    size = i2;
                }
            }
            i2 = size;
            i4++;
            size = i2;
        }
        if (sparseIntArray.size() != 0) {
            for (int i8 = 0; i8 < sparseIntArray.size(); i8++) {
                Animator animator4 = this.C.get(sparseIntArray.keyAt(i8));
                animator4.setStartDelay(animator4.getStartDelay() + (((long) sparseIntArray.valueAt(i8)) - RecyclerView.FOREVER_NS));
            }
        }
    }

    public void c(View view) {
        if (!this.A) {
            ArrayMap<Animator, i.b> i2 = i();
            int i3 = i2.d;
            WindowIdImpl c2 = ViewUtils.c(view);
            for (int i4 = i3 - 1; i4 >= 0; i4--) {
                b e2 = i2.e(i4);
                if (e2.a != null && c2.equals(e2.d)) {
                    i2.c(i4).pause();
                }
            }
            ArrayList<i.d> arrayList = this.B;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.B.clone();
                int size = arrayList2.size();
                for (int i5 = 0; i5 < size; i5++) {
                    ((d) arrayList2.get(i5)).a(this);
                }
            }
            this.z = true;
        }
    }

    public void e() {
        if (this.y == 0) {
            ArrayList<i.d> arrayList = this.B;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.B.clone();
                int size = arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((d) arrayList2.get(i2)).c(this);
                }
            }
            this.A = false;
        }
        this.y++;
    }

    public Transition d(View view) {
        this.g.remove(view);
        return this;
    }

    public TransitionValues b(View view, boolean z2) {
        TransitionSet transitionSet = this.f1464s;
        if (transitionSet != null) {
            return transitionSet.b(view, z2);
        }
        ArrayList<q> arrayList = z2 ? this.u : this.v;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        int i2 = -1;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                break;
            }
            TransitionValues transitionValues = (TransitionValues) arrayList.get(i3);
            if (transitionValues == null) {
                return null;
            }
            if (transitionValues.b == view) {
                i2 = i3;
                break;
            }
            i3++;
        }
        if (i2 < 0) {
            return null;
        }
        return (TransitionValues) (z2 ? this.v : this.u).get(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.h.l.ViewCompat.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      i.h.l.ViewCompat.b(android.view.View, int):void
      i.h.l.ViewCompat.b(android.view.View, android.view.KeyEvent):boolean
      i.h.l.ViewCompat.b(android.view.View, boolean):void */
    public void b() {
        int i2 = this.y - 1;
        this.y = i2;
        if (i2 == 0) {
            ArrayList<i.d> arrayList = this.B;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.B.clone();
                int size = arrayList2.size();
                for (int i3 = 0; i3 < size; i3++) {
                    ((d) arrayList2.get(i3)).e(this);
                }
            }
            for (int i4 = 0; i4 < this.f1462q.c.d(); i4++) {
                View a2 = this.f1462q.c.a(i4);
                if (a2 != null) {
                    ViewCompat.b(a2, false);
                }
            }
            for (int i5 = 0; i5 < this.f1463r.c.d(); i5++) {
                View a3 = this.f1463r.c.a(i5);
                if (a3 != null) {
                    ViewCompat.b(a3, false);
                }
            }
            this.A = true;
        }
    }

    public Transition a(View view) {
        this.g.add(view);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.w.Transition.a(android.view.View, boolean):void
     arg types: [android.view.ViewGroup, boolean]
     candidates:
      i.w.Transition.a(android.view.ViewGroup, boolean):void
      i.w.Transition.a(i.w.TransitionValues, i.w.TransitionValues):boolean
      i.w.Transition.a(android.view.View, boolean):void */
    public void a(ViewGroup viewGroup, boolean z2) {
        ArrayList<String> arrayList;
        ArrayList<Class<?>> arrayList2;
        a(z2);
        if ((this.f1453f.size() > 0 || this.g.size() > 0) && (((arrayList = this.h) == null || arrayList.isEmpty()) && ((arrayList2 = this.f1454i) == null || arrayList2.isEmpty()))) {
            for (int i2 = 0; i2 < this.f1453f.size(); i2++) {
                View findViewById = viewGroup.findViewById(this.f1453f.get(i2).intValue());
                if (findViewById != null) {
                    TransitionValues transitionValues = new TransitionValues(findViewById);
                    if (z2) {
                        c(transitionValues);
                    } else {
                        a(transitionValues);
                    }
                    transitionValues.c.add(this);
                    b(transitionValues);
                    if (z2) {
                        a(this.f1462q, findViewById, transitionValues);
                    } else {
                        a(this.f1463r, findViewById, transitionValues);
                    }
                }
            }
            for (int i3 = 0; i3 < this.g.size(); i3++) {
                View view = this.g.get(i3);
                TransitionValues transitionValues2 = new TransitionValues(view);
                if (z2) {
                    c(transitionValues2);
                } else {
                    a(transitionValues2);
                }
                transitionValues2.c.add(this);
                b(transitionValues2);
                if (z2) {
                    a(this.f1462q, view, transitionValues2);
                } else {
                    a(this.f1463r, view, transitionValues2);
                }
            }
            return;
        }
        a((View) viewGroup, z2);
    }

    public Transition b(d dVar) {
        ArrayList<i.d> arrayList = this.B;
        if (arrayList == null) {
            return this;
        }
        arrayList.remove(dVar);
        if (this.B.size() == 0) {
            this.B = null;
        }
        return this;
    }

    public static void a(TransitionValuesMaps transitionValuesMaps, View view, TransitionValues transitionValues) {
        transitionValuesMaps.a.put(view, transitionValues);
        int id = view.getId();
        if (id >= 0) {
            if (transitionValuesMaps.b.indexOfKey(id) >= 0) {
                transitionValuesMaps.b.put(id, null);
            } else {
                transitionValuesMaps.b.put(id, view);
            }
        }
        String p2 = ViewCompat.p(view);
        if (p2 != null) {
            if (transitionValuesMaps.d.a(p2) >= 0) {
                transitionValuesMaps.d.put(p2, null);
            } else {
                transitionValuesMaps.d.put(p2, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                LongSparseArray<View> longSparseArray = transitionValuesMaps.c;
                if (longSparseArray.b) {
                    longSparseArray.c();
                }
                if (ContainerHelpers.a(longSparseArray.c, longSparseArray.f1064e, itemIdAtPosition) >= 0) {
                    View a2 = transitionValuesMaps.c.a(itemIdAtPosition);
                    if (a2 != null) {
                        a2.setHasTransientState(false);
                        transitionValuesMaps.c.c(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                view.setHasTransientState(true);
                transitionValuesMaps.c.c(itemIdAtPosition, view);
            }
        }
    }

    public void a(boolean z2) {
        if (z2) {
            this.f1462q.a.clear();
            this.f1462q.b.clear();
            this.f1462q.c.b();
            return;
        }
        this.f1463r.a.clear();
        this.f1463r.b.clear();
        this.f1463r.c.b();
    }

    public final void a(View view, boolean z2) {
        if (view != null) {
            int id = view.getId();
            ArrayList<Integer> arrayList = this.f1455j;
            if (arrayList == null || !arrayList.contains(Integer.valueOf(id))) {
                ArrayList<View> arrayList2 = this.f1456k;
                if (arrayList2 == null || !arrayList2.contains(view)) {
                    ArrayList<Class<?>> arrayList3 = this.f1457l;
                    if (arrayList3 != null) {
                        int size = arrayList3.size();
                        int i2 = 0;
                        while (i2 < size) {
                            if (!this.f1457l.get(i2).isInstance(view)) {
                                i2++;
                            } else {
                                return;
                            }
                        }
                    }
                    if (view.getParent() instanceof ViewGroup) {
                        TransitionValues transitionValues = new TransitionValues(view);
                        if (z2) {
                            c(transitionValues);
                        } else {
                            a(transitionValues);
                        }
                        transitionValues.c.add(this);
                        b(transitionValues);
                        if (z2) {
                            a(this.f1462q, view, transitionValues);
                        } else {
                            a(this.f1463r, view, transitionValues);
                        }
                    }
                    if (view instanceof ViewGroup) {
                        ArrayList<Integer> arrayList4 = this.f1459n;
                        if (arrayList4 == null || !arrayList4.contains(Integer.valueOf(id))) {
                            ArrayList<View> arrayList5 = this.f1460o;
                            if (arrayList5 == null || !arrayList5.contains(view)) {
                                ArrayList<Class<?>> arrayList6 = this.f1461p;
                                if (arrayList6 != null) {
                                    int size2 = arrayList6.size();
                                    int i3 = 0;
                                    while (i3 < size2) {
                                        if (!this.f1461p.get(i3).isInstance(view)) {
                                            i3++;
                                        } else {
                                            return;
                                        }
                                    }
                                }
                                ViewGroup viewGroup = (ViewGroup) view;
                                for (int i4 = 0; i4 < viewGroup.getChildCount(); i4++) {
                                    a(viewGroup.getChildAt(i4), z2);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean a(TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null || transitionValues2 == null) {
            return false;
        }
        String[] c2 = c();
        if (c2 != null) {
            int length = c2.length;
            int i2 = 0;
            while (i2 < length) {
                if (!a(transitionValues, transitionValues2, c2[i2])) {
                    i2++;
                }
            }
            return false;
        }
        for (String a2 : transitionValues.a.keySet()) {
            if (a(transitionValues, transitionValues2, a2)) {
            }
        }
        return false;
        return true;
    }

    public static boolean a(TransitionValues transitionValues, TransitionValues transitionValues2, String str) {
        Object obj = transitionValues.a.get(str);
        Object obj2 = transitionValues2.a.get(str);
        if (obj == null && obj2 == null) {
            return false;
        }
        if (obj == null || obj2 == null) {
            return true;
        }
        return true ^ obj.equals(obj2);
    }

    public Transition a(d dVar) {
        if (this.B == null) {
            this.B = new ArrayList<>();
        }
        this.B.add(dVar);
        return this;
    }

    public void a(PathMotion pathMotion) {
        if (pathMotion == null) {
            this.F = H;
        } else {
            this.F = pathMotion;
        }
    }

    public void a(c cVar) {
        this.E = cVar;
    }

    public void a(TransitionPropagation transitionPropagation) {
        this.D = transitionPropagation;
    }

    public String a(String str) {
        StringBuilder a2 = outline.a(str);
        a2.append(getClass().getSimpleName());
        a2.append("@");
        a2.append(Integer.toHexString(hashCode()));
        a2.append(": ");
        String sb = a2.toString();
        if (this.d != -1) {
            StringBuilder b2 = outline.b(sb, "dur(");
            b2.append(this.d);
            b2.append(") ");
            sb = b2.toString();
        }
        if (this.c != -1) {
            StringBuilder b3 = outline.b(sb, "dly(");
            b3.append(this.c);
            b3.append(") ");
            sb = b3.toString();
        }
        if (this.f1452e != null) {
            StringBuilder b4 = outline.b(sb, "interp(");
            b4.append(this.f1452e);
            b4.append(") ");
            sb = b4.toString();
        }
        if (this.f1453f.size() <= 0 && this.g.size() <= 0) {
            return sb;
        }
        String a3 = outline.a(sb, "tgts(");
        if (this.f1453f.size() > 0) {
            for (int i2 = 0; i2 < this.f1453f.size(); i2++) {
                if (i2 > 0) {
                    a3 = outline.a(a3, ", ");
                }
                StringBuilder a4 = outline.a(a3);
                a4.append(this.f1453f.get(i2));
                a3 = a4.toString();
            }
        }
        if (this.g.size() > 0) {
            for (int i3 = 0; i3 < this.g.size(); i3++) {
                if (i3 > 0) {
                    a3 = outline.a(a3, ", ");
                }
                StringBuilder a5 = outline.a(a3);
                a5.append(this.g.get(i3));
                a3 = a5.toString();
            }
        }
        return outline.a(a3, ")");
    }
}
