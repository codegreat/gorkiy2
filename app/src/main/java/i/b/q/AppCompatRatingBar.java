package i.b.q;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.RatingBar;

public class AppCompatRatingBar extends RatingBar {
    public final AppCompatProgressBarHelper b;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatRatingBar(android.content.Context r2, android.util.AttributeSet r3) {
        /*
            r1 = this;
            int r0 = i.b.a.ratingBarStyle
            r1.<init>(r2, r3, r0)
            i.b.q.AppCompatProgressBarHelper r2 = new i.b.q.AppCompatProgressBarHelper
            r2.<init>(r1)
            r1.b = r2
            r2.a(r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.AppCompatRatingBar.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public synchronized void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        Bitmap bitmap = this.b.b;
        if (bitmap != null) {
            setMeasuredDimension(View.resolveSizeAndState(bitmap.getWidth() * getNumStars(), i2, 0), getMeasuredHeight());
        }
    }
}
