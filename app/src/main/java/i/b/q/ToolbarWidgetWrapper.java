package i.b.q;

import android.view.View;
import android.view.Window;
import i.b.p.i.ActionMenuItem;

public class ToolbarWidgetWrapper implements View.OnClickListener {
    public final ActionMenuItem b = new ActionMenuItem(this.c.a.getContext(), 0, 16908332, 0, 0, this.c.f1032i);
    public final /* synthetic */ ToolbarWidgetWrapper0 c;

    public ToolbarWidgetWrapper(ToolbarWidgetWrapper0 toolbarWidgetWrapper0) {
        this.c = toolbarWidgetWrapper0;
    }

    public void onClick(View view) {
        ToolbarWidgetWrapper0 toolbarWidgetWrapper0 = this.c;
        Window.Callback callback = toolbarWidgetWrapper0.f1035l;
        if (callback != null && toolbarWidgetWrapper0.f1036m) {
            callback.onMenuItemSelected(0, this.b);
        }
    }
}
