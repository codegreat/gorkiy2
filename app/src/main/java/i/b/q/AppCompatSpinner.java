package i.b.q;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;
import androidx.appcompat.app.AlertController;
import i.b.k.AlertDialog;
import i.b.l.a.AppCompatResources;
import i.b.q.v;
import i.h.l.ViewCompat;

public class AppCompatSpinner extends Spinner {

    /* renamed from: j  reason: collision with root package name */
    public static final int[] f958j = {16843505};
    public final AppCompatBackgroundHelper b;
    public final Context c;
    public ForwardingListener d;

    /* renamed from: e  reason: collision with root package name */
    public SpinnerAdapter f959e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f960f;
    public f g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public final Rect f961i = new Rect();

    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public a() {
        }

        public void onGlobalLayout() {
            if (!AppCompatSpinner.this.getInternalPopup().b()) {
                AppCompatSpinner.this.a();
            }
            ViewTreeObserver viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
            if (viewTreeObserver != null) {
                viewTreeObserver.removeOnGlobalLayoutListener(this);
            }
        }
    }

    public class b implements f, DialogInterface.OnClickListener {
        public AlertDialog b;
        public ListAdapter c;
        public CharSequence d;

        public b() {
        }

        public void a(ListAdapter listAdapter) {
            this.c = listAdapter;
        }

        public boolean b() {
            AlertDialog alertDialog = this.b;
            if (alertDialog != null) {
                return alertDialog.isShowing();
            }
            return false;
        }

        public int c() {
            return 0;
        }

        public void c(int i2) {
            Log.e("AppCompatSpinner", "Cannot set horizontal (original) offset for MODE_DIALOG, ignoring");
        }

        public int d() {
            return 0;
        }

        public void dismiss() {
            AlertDialog alertDialog = this.b;
            if (alertDialog != null) {
                alertDialog.dismiss();
                this.b = null;
            }
        }

        public Drawable e() {
            return null;
        }

        public CharSequence f() {
            return this.d;
        }

        public void onClick(DialogInterface dialogInterface, int i2) {
            AppCompatSpinner.this.setSelection(i2);
            if (AppCompatSpinner.this.getOnItemClickListener() != null) {
                AppCompatSpinner.this.performItemClick(null, i2, this.c.getItemId(i2));
            }
            AlertDialog alertDialog = this.b;
            if (alertDialog != null) {
                alertDialog.dismiss();
                this.b = null;
            }
        }

        public void a(CharSequence charSequence) {
            this.d = charSequence;
        }

        public void b(int i2) {
            Log.e("AppCompatSpinner", "Cannot set vertical offset for MODE_DIALOG, ignoring");
        }

        public void a(int i2, int i3) {
            if (this.c != null) {
                AlertDialog.a aVar = new AlertDialog.a(AppCompatSpinner.this.getPopupContext());
                CharSequence charSequence = this.d;
                if (charSequence != null) {
                    aVar.a.f19f = charSequence;
                }
                ListAdapter listAdapter = this.c;
                int selectedItemPosition = AppCompatSpinner.this.getSelectedItemPosition();
                AlertController.b bVar = aVar.a;
                bVar.f28q = listAdapter;
                bVar.f29r = this;
                bVar.u = selectedItemPosition;
                bVar.f31t = true;
                AlertDialog a = aVar.a();
                this.b = a;
                ListView listView = a.d.g;
                listView.setTextDirection(i2);
                listView.setTextAlignment(i3);
                this.b.show();
            }
        }

        public void a(Drawable drawable) {
            Log.e("AppCompatSpinner", "Cannot set popup background for MODE_DIALOG, ignoring");
        }

        public void a(int i2) {
            Log.e("AppCompatSpinner", "Cannot set horizontal offset for MODE_DIALOG, ignoring");
        }
    }

    public static class c implements ListAdapter, SpinnerAdapter {
        public SpinnerAdapter b;
        public ListAdapter c;

        public c(SpinnerAdapter spinnerAdapter, Resources.Theme theme) {
            this.b = spinnerAdapter;
            if (spinnerAdapter instanceof ListAdapter) {
                this.c = (ListAdapter) spinnerAdapter;
            }
            if (theme == null) {
                return;
            }
            if (Build.VERSION.SDK_INT >= 23 && (spinnerAdapter instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme);
                }
            } else if (spinnerAdapter instanceof ThemedSpinnerAdapter) {
                ThemedSpinnerAdapter themedSpinnerAdapter2 = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter2.getDropDownViewTheme() == null) {
                    themedSpinnerAdapter2.setDropDownViewTheme(theme);
                }
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.c;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public int getCount() {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter == null) {
                return 0;
            }
            return spinnerAdapter.getCount();
        }

        public View getDropDownView(int i2, View view, ViewGroup viewGroup) {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter == null) {
                return null;
            }
            return spinnerAdapter.getDropDownView(i2, view, viewGroup);
        }

        public Object getItem(int i2) {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter == null) {
                return null;
            }
            return spinnerAdapter.getItem(i2);
        }

        public long getItemId(int i2) {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter == null) {
                return -1;
            }
            return spinnerAdapter.getItemId(i2);
        }

        public int getItemViewType(int i2) {
            return 0;
        }

        public View getView(int i2, View view, ViewGroup viewGroup) {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter == null) {
                return null;
            }
            return spinnerAdapter.getDropDownView(i2, view, viewGroup);
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean hasStableIds() {
            SpinnerAdapter spinnerAdapter = this.b;
            return spinnerAdapter != null && spinnerAdapter.hasStableIds();
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }

        public boolean isEnabled(int i2) {
            ListAdapter listAdapter = this.c;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i2);
            }
            return true;
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter != null) {
                spinnerAdapter.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            SpinnerAdapter spinnerAdapter = this.b;
            if (spinnerAdapter != null) {
                spinnerAdapter.unregisterDataSetObserver(dataSetObserver);
            }
        }
    }

    public class d extends ListPopupWindow0 implements f {
        public CharSequence G;
        public ListAdapter H;
        public final Rect I = new Rect();
        public int J;

        public class a implements AdapterView.OnItemClickListener {
            public a(AppCompatSpinner appCompatSpinner) {
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
                AppCompatSpinner.this.setSelection(i2);
                if (AppCompatSpinner.this.getOnItemClickListener() != null) {
                    d dVar = d.this;
                    AppCompatSpinner.this.performItemClick(view, i2, dVar.H.getItemId(i2));
                }
                d.this.dismiss();
            }
        }

        public class b implements ViewTreeObserver.OnGlobalLayoutListener {
            public b() {
            }

            public void onGlobalLayout() {
                d dVar = d.this;
                AppCompatSpinner appCompatSpinner = AppCompatSpinner.this;
                if (dVar != null) {
                    if (!(ViewCompat.v(appCompatSpinner) && appCompatSpinner.getGlobalVisibleRect(dVar.I))) {
                        d.this.dismiss();
                        return;
                    }
                    d.this.h();
                    d.super.a();
                    return;
                }
                throw null;
            }
        }

        public class c implements PopupWindow.OnDismissListener {
            public final /* synthetic */ ViewTreeObserver.OnGlobalLayoutListener b;

            public c(ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
                this.b = onGlobalLayoutListener;
            }

            public void onDismiss() {
                ViewTreeObserver viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
                if (viewTreeObserver != null) {
                    viewTreeObserver.removeGlobalOnLayoutListener(this.b);
                }
            }
        }

        public d(Context context, AttributeSet attributeSet, int i2) {
            super(context, attributeSet, i2, 0);
            super.f1005s = AppCompatSpinner.this;
            a(true);
            super.f1003q = 0;
            super.f1006t = new a(AppCompatSpinner.this);
        }

        public void c(int i2) {
            this.J = i2;
        }

        public CharSequence f() {
            return this.G;
        }

        public void h() {
            int i2;
            Drawable e2 = e();
            int i3 = 0;
            if (e2 != null) {
                e2.getPadding(AppCompatSpinner.this.f961i);
                i3 = ViewUtils.a(AppCompatSpinner.this) ? AppCompatSpinner.this.f961i.right : -AppCompatSpinner.this.f961i.left;
            } else {
                Rect rect = AppCompatSpinner.this.f961i;
                rect.right = 0;
                rect.left = 0;
            }
            int paddingLeft = AppCompatSpinner.this.getPaddingLeft();
            int paddingRight = AppCompatSpinner.this.getPaddingRight();
            int width = AppCompatSpinner.this.getWidth();
            AppCompatSpinner appCompatSpinner = AppCompatSpinner.this;
            int i4 = appCompatSpinner.h;
            if (i4 == -2) {
                int a2 = appCompatSpinner.a((SpinnerAdapter) this.H, e());
                int i5 = AppCompatSpinner.this.getContext().getResources().getDisplayMetrics().widthPixels;
                Rect rect2 = AppCompatSpinner.this.f961i;
                int i6 = (i5 - rect2.left) - rect2.right;
                if (a2 > i6) {
                    a2 = i6;
                }
                d(Math.max(a2, (width - paddingLeft) - paddingRight));
            } else if (i4 == -1) {
                d((width - paddingLeft) - paddingRight);
            } else {
                d(i4);
            }
            if (ViewUtils.a(AppCompatSpinner.this)) {
                i2 = (((width - paddingRight) - super.f994f) - this.J) + i3;
            } else {
                i2 = paddingLeft + this.J + i3;
            }
            super.g = i2;
        }

        public void a(ListAdapter listAdapter) {
            super.a(listAdapter);
            this.H = listAdapter;
        }

        public void a(CharSequence charSequence) {
            this.G = charSequence;
        }

        public void a(int i2, int i3) {
            ViewTreeObserver viewTreeObserver;
            boolean b2 = b();
            h();
            super.C.setInputMethodMode(2);
            super.a();
            DropDownListView dropDownListView = super.d;
            dropDownListView.setChoiceMode(1);
            dropDownListView.setTextDirection(i2);
            dropDownListView.setTextAlignment(i3);
            int selectedItemPosition = AppCompatSpinner.this.getSelectedItemPosition();
            DropDownListView dropDownListView2 = super.d;
            if (b() && dropDownListView2 != null) {
                dropDownListView2.setListSelectionHidden(false);
                dropDownListView2.setSelection(selectedItemPosition);
                if (dropDownListView2.getChoiceMode() != 0) {
                    dropDownListView2.setItemChecked(selectedItemPosition, true);
                }
            }
            if (!b2 && (viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver()) != null) {
                b bVar = new b();
                viewTreeObserver.addOnGlobalLayoutListener(bVar);
                super.C.setOnDismissListener(new c(bVar));
            }
        }
    }

    public static class e extends View.BaseSavedState {
        public static final Parcelable.Creator<v.e> CREATOR = new a();
        public boolean b;

        public static class a implements Parcelable.Creator<v.e> {
            public Object createFromParcel(Parcel parcel) {
                return new e(parcel);
            }

            public Object[] newArray(int i2) {
                return new e[i2];
            }
        }

        public e(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i2) {
            super.writeToParcel(parcel, i2);
            parcel.writeByte(this.b ? (byte) 1 : 0);
        }

        public e(Parcel parcel) {
            super(parcel);
            this.b = parcel.readByte() != 0;
        }
    }

    public interface f {
        void a(int i2);

        void a(int i2, int i3);

        void a(Drawable drawable);

        void a(ListAdapter listAdapter);

        void a(CharSequence charSequence);

        void b(int i2);

        boolean b();

        int c();

        void c(int i2);

        int d();

        void dismiss();

        Drawable e();

        CharSequence f();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        if (r4 != null) goto L_0x0052;
     */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatSpinner(android.content.Context r9, android.util.AttributeSet r10, int r11) {
        /*
            r8 = this;
            r8.<init>(r9, r10, r11)
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r8.f961i = r0
            int[] r0 = i.b.j.Spinner
            r1 = 0
            android.content.res.TypedArray r0 = r9.obtainStyledAttributes(r10, r0, r11, r1)
            i.b.q.AppCompatBackgroundHelper r2 = new i.b.q.AppCompatBackgroundHelper
            r2.<init>(r8)
            r8.b = r2
            int r2 = i.b.j.Spinner_popupTheme
            int r2 = r0.getResourceId(r2, r1)
            if (r2 == 0) goto L_0x0028
            i.b.p.ContextThemeWrapper r3 = new i.b.p.ContextThemeWrapper
            r3.<init>(r9, r2)
            r8.c = r3
            goto L_0x002a
        L_0x0028:
            r8.c = r9
        L_0x002a:
            r2 = 0
            r3 = -1
            int[] r4 = i.b.q.AppCompatSpinner.f958j     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            android.content.res.TypedArray r4 = r9.obtainStyledAttributes(r10, r4, r11, r1)     // Catch:{ Exception -> 0x0046, all -> 0x0043 }
            boolean r5 = r4.hasValue(r1)     // Catch:{ Exception -> 0x0041 }
            if (r5 == 0) goto L_0x0052
            int r3 = r4.getInt(r1, r1)     // Catch:{ Exception -> 0x0041 }
            goto L_0x0052
        L_0x003d:
            r9 = move-exception
            r2 = r4
            goto L_0x00d1
        L_0x0041:
            r5 = move-exception
            goto L_0x0049
        L_0x0043:
            r9 = move-exception
            goto L_0x00d1
        L_0x0046:
            r4 = move-exception
            r5 = r4
            r4 = r2
        L_0x0049:
            java.lang.String r6 = "AppCompatSpinner"
            java.lang.String r7 = "Could not read android:spinnerMode"
            android.util.Log.i(r6, r7, r5)     // Catch:{ all -> 0x003d }
            if (r4 == 0) goto L_0x0055
        L_0x0052:
            r4.recycle()
        L_0x0055:
            r4 = 1
            if (r3 == 0) goto L_0x0095
            if (r3 == r4) goto L_0x005b
            goto L_0x00a5
        L_0x005b:
            i.b.q.AppCompatSpinner$d r3 = new i.b.q.AppCompatSpinner$d
            android.content.Context r5 = r8.c
            r3.<init>(r5, r10, r11)
            android.content.Context r5 = r8.c
            int[] r6 = i.b.j.Spinner
            i.b.q.TintTypedArray r1 = i.b.q.TintTypedArray.a(r5, r10, r6, r11, r1)
            int r5 = i.b.j.Spinner_android_dropDownWidth
            r6 = -2
            int r5 = r1.e(r5, r6)
            r8.h = r5
            int r5 = i.b.j.Spinner_android_popupBackground
            android.graphics.drawable.Drawable r5 = r1.b(r5)
            android.widget.PopupWindow r6 = r3.C
            r6.setBackgroundDrawable(r5)
            int r5 = i.b.j.Spinner_android_prompt
            java.lang.String r5 = r0.getString(r5)
            r3.G = r5
            android.content.res.TypedArray r1 = r1.b
            r1.recycle()
            r8.g = r3
            i.b.q.AppCompatSpinner0 r1 = new i.b.q.AppCompatSpinner0
            r1.<init>(r8, r8, r3)
            r8.d = r1
            goto L_0x00a5
        L_0x0095:
            i.b.q.AppCompatSpinner$b r1 = new i.b.q.AppCompatSpinner$b
            r1.<init>()
            r8.g = r1
            int r3 = i.b.j.Spinner_android_prompt
            java.lang.String r3 = r0.getString(r3)
            r1.a(r3)
        L_0x00a5:
            int r1 = i.b.j.Spinner_android_entries
            java.lang.CharSequence[] r1 = r0.getTextArray(r1)
            if (r1 == 0) goto L_0x00bd
            android.widget.ArrayAdapter r3 = new android.widget.ArrayAdapter
            r5 = 17367048(0x1090008, float:2.5162948E-38)
            r3.<init>(r9, r5, r1)
            int r9 = i.b.g.support_simple_spinner_dropdown_item
            r3.setDropDownViewResource(r9)
            r8.setAdapter(r3)
        L_0x00bd:
            r0.recycle()
            r8.f960f = r4
            android.widget.SpinnerAdapter r9 = r8.f959e
            if (r9 == 0) goto L_0x00cb
            r8.setAdapter(r9)
            r8.f959e = r2
        L_0x00cb:
            i.b.q.AppCompatBackgroundHelper r9 = r8.b
            r9.a(r10, r11)
            return
        L_0x00d1:
            if (r2 == 0) goto L_0x00d6
            r2.recycle()
        L_0x00d6:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.AppCompatSpinner.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public int a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        int i2 = 0;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        View view = null;
        int i3 = 0;
        for (int max2 = Math.max(0, max - (15 - (min - max))); max2 < min; max2++) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i2) {
                view = null;
                i2 = itemViewType;
            }
            view = spinnerAdapter.getView(max2, view, this);
            if (view.getLayoutParams() == null) {
                view.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            }
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            i3 = Math.max(i3, view.getMeasuredWidth());
        }
        if (drawable == null) {
            return i3;
        }
        drawable.getPadding(this.f961i);
        Rect rect = this.f961i;
        return i3 + rect.left + rect.right;
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            appCompatBackgroundHelper.a();
        }
    }

    public int getDropDownHorizontalOffset() {
        f fVar = this.g;
        if (fVar != null) {
            return fVar.c();
        }
        return super.getDropDownHorizontalOffset();
    }

    public int getDropDownVerticalOffset() {
        f fVar = this.g;
        if (fVar != null) {
            return fVar.d();
        }
        return super.getDropDownVerticalOffset();
    }

    public int getDropDownWidth() {
        if (this.g != null) {
            return this.h;
        }
        return super.getDropDownWidth();
    }

    public final f getInternalPopup() {
        return this.g;
    }

    public Drawable getPopupBackground() {
        f fVar = this.g;
        if (fVar != null) {
            return fVar.e();
        }
        return super.getPopupBackground();
    }

    public Context getPopupContext() {
        return this.c;
    }

    public CharSequence getPrompt() {
        f fVar = this.g;
        return fVar != null ? fVar.f() : super.getPrompt();
    }

    public ColorStateList getSupportBackgroundTintList() {
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            return appCompatBackgroundHelper.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            return appCompatBackgroundHelper.c();
        }
        return null;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        f fVar = this.g;
        if (fVar != null && fVar.b()) {
            this.g.dismiss();
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.g != null && View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(getAdapter(), getBackground())), View.MeasureSpec.getSize(i2)), getMeasuredHeight());
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        ViewTreeObserver viewTreeObserver;
        e eVar = (e) parcelable;
        super.onRestoreInstanceState(eVar.getSuperState());
        if (eVar.b && (viewTreeObserver = getViewTreeObserver()) != null) {
            viewTreeObserver.addOnGlobalLayoutListener(new a());
        }
    }

    public Parcelable onSaveInstanceState() {
        e eVar = new e(super.onSaveInstanceState());
        f fVar = this.g;
        eVar.b = fVar != null && fVar.b();
        return eVar;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        ForwardingListener forwardingListener = this.d;
        if (forwardingListener == null || !forwardingListener.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public boolean performClick() {
        f fVar = this.g;
        if (fVar == null) {
            return super.performClick();
        }
        if (fVar.b()) {
            return true;
        }
        a();
        return true;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            appCompatBackgroundHelper.d();
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            appCompatBackgroundHelper.a(i2);
        }
    }

    public void setDropDownHorizontalOffset(int i2) {
        f fVar = this.g;
        if (fVar != null) {
            fVar.c(i2);
            this.g.a(i2);
            return;
        }
        super.setDropDownHorizontalOffset(i2);
    }

    public void setDropDownVerticalOffset(int i2) {
        f fVar = this.g;
        if (fVar != null) {
            fVar.b(i2);
        } else {
            super.setDropDownVerticalOffset(i2);
        }
    }

    public void setDropDownWidth(int i2) {
        if (this.g != null) {
            this.h = i2;
        } else {
            super.setDropDownWidth(i2);
        }
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        f fVar = this.g;
        if (fVar != null) {
            fVar.a(drawable);
        } else {
            super.setPopupBackgroundDrawable(drawable);
        }
    }

    public void setPopupBackgroundResource(int i2) {
        setPopupBackgroundDrawable(AppCompatResources.c(getPopupContext(), i2));
    }

    public void setPrompt(CharSequence charSequence) {
        f fVar = this.g;
        if (fVar != null) {
            fVar.a(charSequence);
        } else {
            super.setPrompt(charSequence);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            appCompatBackgroundHelper.b(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AppCompatBackgroundHelper appCompatBackgroundHelper = this.b;
        if (appCompatBackgroundHelper != null) {
            appCompatBackgroundHelper.a(mode);
        }
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        if (!this.f960f) {
            this.f959e = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.g != null) {
            Context context = this.c;
            if (context == null) {
                context = getContext();
            }
            this.g.a(new c(spinnerAdapter, context.getTheme()));
        }
    }

    public void a() {
        this.g.a(getTextDirection(), getTextAlignment());
    }
}
