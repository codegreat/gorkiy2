package i.b.q;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.ListMenuItemView;
import i.b.p.i.MenuAdapter;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuItemImpl;
import java.lang.reflect.Method;

public class MenuPopupWindow extends ListPopupWindow0 implements MenuItemHoverListener {
    public static Method H;
    public MenuItemHoverListener G;

    public static class a extends DropDownListView {

        /* renamed from: p  reason: collision with root package name */
        public final int f1007p;

        /* renamed from: q  reason: collision with root package name */
        public final int f1008q;

        /* renamed from: r  reason: collision with root package name */
        public MenuItemHoverListener f1009r;

        /* renamed from: s  reason: collision with root package name */
        public MenuItem f1010s;

        public a(Context context, boolean z) {
            super(context, z);
            if (1 == context.getResources().getConfiguration().getLayoutDirection()) {
                this.f1007p = 21;
                this.f1008q = 22;
                return;
            }
            this.f1007p = 22;
            this.f1008q = 21;
        }

        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i2;
            MenuAdapter menuAdapter;
            int pointToPosition;
            int i3;
            if (this.f1009r != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i2 = headerViewListAdapter.getHeadersCount();
                    menuAdapter = (MenuAdapter) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i2 = 0;
                    menuAdapter = (MenuAdapter) adapter;
                }
                MenuItemImpl menuItemImpl = null;
                if (motionEvent.getAction() != 10 && (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) != -1 && (i3 = pointToPosition - i2) >= 0 && i3 < menuAdapter.getCount()) {
                    menuItemImpl = menuAdapter.getItem(i3);
                }
                MenuItem menuItem = this.f1010s;
                if (menuItem != menuItemImpl) {
                    MenuBuilder menuBuilder = menuAdapter.b;
                    if (menuItem != null) {
                        this.f1009r.b(menuBuilder, menuItem);
                    }
                    this.f1010s = menuItemImpl;
                    if (menuItemImpl != null) {
                        this.f1009r.a(menuBuilder, menuItemImpl);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        public boolean onKeyDown(int i2, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i2 == this.f1007p) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i2 != this.f1008q) {
                return super.onKeyDown(i2, keyEvent);
            } else {
                setSelection(-1);
                ((MenuAdapter) getAdapter()).b.a(false);
                return true;
            }
        }

        public void setHoverListener(MenuItemHoverListener menuItemHoverListener) {
            this.f1009r = menuItemHoverListener;
        }

        public /* bridge */ /* synthetic */ void setSelector(Drawable drawable) {
            super.setSelector(drawable);
        }
    }

    static {
        try {
            if (Build.VERSION.SDK_INT <= 28) {
                H = PopupWindow.class.getDeclaredMethod("setTouchModal", Boolean.TYPE);
            }
        } catch (NoSuchMethodException unused) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }

    public MenuPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
    }

    public DropDownListView a(Context context, boolean z) {
        a aVar = new a(context, z);
        aVar.setHoverListener(this);
        return aVar;
    }

    public void b(MenuBuilder menuBuilder, MenuItem menuItem) {
        MenuItemHoverListener menuItemHoverListener = this.G;
        if (menuItemHoverListener != null) {
            menuItemHoverListener.b(menuBuilder, menuItem);
        }
    }

    public void a(MenuBuilder menuBuilder, MenuItem menuItem) {
        MenuItemHoverListener menuItemHoverListener = this.G;
        if (menuItemHoverListener != null) {
            menuItemHoverListener.a(menuBuilder, menuItem);
        }
    }
}
