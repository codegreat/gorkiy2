package i.b.p.i;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.crashlytics.android.core.CodedOutputStream;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.h.g.a.SupportMenuItem;
import i.h.l.ActionProvider;

public final class MenuItemImpl implements SupportMenuItem {
    public ActionProvider A;
    public MenuItem.OnActionExpandListener B;
    public boolean C = false;
    public ContextMenu.ContextMenuInfo D;
    public final int a;
    public final int b;
    public final int c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public CharSequence f896e;

    /* renamed from: f  reason: collision with root package name */
    public CharSequence f897f;
    public Intent g;
    public char h;

    /* renamed from: i  reason: collision with root package name */
    public int f898i = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: j  reason: collision with root package name */
    public char f899j;

    /* renamed from: k  reason: collision with root package name */
    public int f900k = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: l  reason: collision with root package name */
    public Drawable f901l;

    /* renamed from: m  reason: collision with root package name */
    public int f902m = 0;

    /* renamed from: n  reason: collision with root package name */
    public MenuBuilder f903n;

    /* renamed from: o  reason: collision with root package name */
    public SubMenuBuilder f904o;

    /* renamed from: p  reason: collision with root package name */
    public MenuItem.OnMenuItemClickListener f905p;

    /* renamed from: q  reason: collision with root package name */
    public CharSequence f906q;

    /* renamed from: r  reason: collision with root package name */
    public CharSequence f907r;

    /* renamed from: s  reason: collision with root package name */
    public ColorStateList f908s = null;

    /* renamed from: t  reason: collision with root package name */
    public PorterDuff.Mode f909t = null;
    public boolean u = false;
    public boolean v = false;
    public boolean w = false;
    public int x = 16;
    public int y = 0;
    public View z;

    public class a implements ActionProvider.a {
        public a() {
        }
    }

    public MenuItemImpl(MenuBuilder menuBuilder, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.f903n = menuBuilder;
        this.a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.f896e = charSequence;
        this.y = i6;
    }

    public static void a(StringBuilder sb, int i2, int i3, String str) {
        if ((i2 & i3) == i3) {
            sb.append(str);
        }
    }

    public char b() {
        return this.f903n.f() ? this.f899j : this.h;
    }

    public boolean c(boolean z2) {
        int i2 = this.x;
        int i3 = (z2 ? 0 : 8) | (i2 & -9);
        this.x = i3;
        if (i2 != i3) {
            return true;
        }
        return false;
    }

    public boolean collapseActionView() {
        if ((this.y & 8) == 0) {
            return false;
        }
        if (this.z == null) {
            return true;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.B;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionCollapse(this)) {
            return this.f903n.a(this);
        }
        return false;
    }

    public boolean d() {
        return (this.x & 32) == 32;
    }

    public boolean e() {
        return (this.x & 4) != 0;
    }

    public boolean expandActionView() {
        if (!c()) {
            return false;
        }
        MenuItem.OnActionExpandListener onActionExpandListener = this.B;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionExpand(this)) {
            return this.f903n.b(this);
        }
        return false;
    }

    public boolean f() {
        return this.f903n.g() && b() != 0;
    }

    public android.view.ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public View getActionView() {
        View view = this.z;
        if (view != null) {
            return view;
        }
        ActionProvider actionProvider = this.A;
        if (actionProvider == null) {
            return null;
        }
        View a2 = actionProvider.a(this);
        this.z = a2;
        return a2;
    }

    public int getAlphabeticModifiers() {
        return this.f900k;
    }

    public char getAlphabeticShortcut() {
        return this.f899j;
    }

    public CharSequence getContentDescription() {
        return this.f906q;
    }

    public int getGroupId() {
        return this.b;
    }

    public Drawable getIcon() {
        Drawable drawable = this.f901l;
        if (drawable != null) {
            return a(drawable);
        }
        int i2 = this.f902m;
        if (i2 == 0) {
            return null;
        }
        Drawable c2 = AppCompatResources.c(this.f903n.a, i2);
        this.f902m = 0;
        this.f901l = c2;
        return a(c2);
    }

    public ColorStateList getIconTintList() {
        return this.f908s;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f909t;
    }

    public Intent getIntent() {
        return this.g;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.a;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.D;
    }

    public int getNumericModifiers() {
        return this.f898i;
    }

    public char getNumericShortcut() {
        return this.h;
    }

    public int getOrder() {
        return this.c;
    }

    public SubMenu getSubMenu() {
        return this.f904o;
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.f896e;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f897f;
        return charSequence != null ? charSequence : this.f896e;
    }

    public CharSequence getTooltipText() {
        return this.f907r;
    }

    public boolean hasSubMenu() {
        return this.f904o != null;
    }

    public boolean isActionViewExpanded() {
        return this.C;
    }

    public boolean isCheckable() {
        return (this.x & 1) == 1;
    }

    public boolean isChecked() {
        return (this.x & 2) == 2;
    }

    public boolean isEnabled() {
        return (this.x & 16) != 0;
    }

    public boolean isVisible() {
        ActionProvider actionProvider = this.A;
        if (actionProvider == null || !actionProvider.b()) {
            if ((this.x & 8) == 0) {
                return true;
            }
            return false;
        } else if ((this.x & 8) != 0 || !this.A.a()) {
            return false;
        } else {
            return true;
        }
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.f899j == c2) {
            return this;
        }
        this.f899j = Character.toLowerCase(c2);
        this.f903n.b(false);
        return this;
    }

    public MenuItem setCheckable(boolean z2) {
        int i2 = this.x;
        boolean z3 = z2 | (i2 & true);
        this.x = z3 ? 1 : 0;
        if (i2 != z3) {
            this.f903n.b(false);
        }
        return this;
    }

    public MenuItem setChecked(boolean z2) {
        if ((this.x & 4) != 0) {
            MenuBuilder menuBuilder = this.f903n;
            if (menuBuilder != null) {
                int groupId = getGroupId();
                int size = menuBuilder.f883f.size();
                menuBuilder.j();
                for (int i2 = 0; i2 < size; i2++) {
                    MenuItemImpl menuItemImpl = (MenuItemImpl) menuBuilder.f883f.get(i2);
                    if (menuItemImpl.b == groupId && menuItemImpl.e() && menuItemImpl.isCheckable()) {
                        menuItemImpl.a(menuItemImpl == this);
                    }
                }
                menuBuilder.i();
            } else {
                throw null;
            }
        } else {
            a(z2);
        }
        return this;
    }

    public MenuItem setContentDescription(CharSequence charSequence) {
        this.f906q = charSequence;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.x |= 16;
        } else {
            this.x &= -17;
        }
        this.f903n.b(false);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f902m = 0;
        this.f901l = drawable;
        this.w = true;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f908s = colorStateList;
        this.u = true;
        this.w = true;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f909t = mode;
        this.v = true;
        this.w = true;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.g = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        if (this.h == c2) {
            return this;
        }
        this.h = c2;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.B = onActionExpandListener;
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f905p = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.h = c2;
        this.f899j = Character.toLowerCase(c3);
        this.f903n.b(false);
        return this;
    }

    public void setShowAsAction(int i2) {
        int i3 = i2 & 3;
        if (i3 == 0 || i3 == 1 || i3 == 2) {
            this.y = i2;
            this.f903n.h();
            return;
        }
        throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
    }

    public MenuItem setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f896e = charSequence;
        this.f903n.b(false);
        SubMenuBuilder subMenuBuilder = this.f904o;
        if (subMenuBuilder != null) {
            subMenuBuilder.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f897f = charSequence;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setTooltipText(CharSequence charSequence) {
        this.f907r = charSequence;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            MenuBuilder menuBuilder = this.f903n;
            menuBuilder.h = true;
            menuBuilder.b(true);
        }
        return this;
    }

    public String toString() {
        CharSequence charSequence = this.f896e;
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    public final Drawable a(Drawable drawable) {
        if (drawable != null && this.w && (this.u || this.v)) {
            drawable = ResourcesFlusher.d(drawable).mutate();
            if (this.u) {
                drawable.setTintList(this.f908s);
            }
            if (this.v) {
                drawable.setTintMode(this.f909t);
            }
            this.w = false;
        }
        return drawable;
    }

    public void b(boolean z2) {
        if (z2) {
            this.x |= 32;
        } else {
            this.x &= -33;
        }
    }

    public SupportMenuItem setActionView(View view) {
        int i2;
        this.z = view;
        this.A = null;
        if (view != null && view.getId() == -1 && (i2 = this.a) > 0) {
            view.setId(i2);
        }
        this.f903n.h();
        return this;
    }

    public boolean c() {
        ActionProvider actionProvider;
        if ((this.y & 8) == 0) {
            return false;
        }
        if (this.z == null && (actionProvider = this.A) != null) {
            this.z = actionProvider.a(this);
        }
        if (this.z != null) {
            return true;
        }
        return false;
    }

    /* renamed from: setContentDescription  reason: collision with other method in class */
    public SupportMenuItem m8setContentDescription(CharSequence charSequence) {
        this.f906q = charSequence;
        this.f903n.b(false);
        return this;
    }

    /* renamed from: setTooltipText  reason: collision with other method in class */
    public SupportMenuItem m9setTooltipText(CharSequence charSequence) {
        this.f907r = charSequence;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        if (this.f899j == c2 && this.f900k == i2) {
            return this;
        }
        this.f899j = Character.toLowerCase(c2);
        this.f900k = KeyEvent.normalizeMetaState(i2);
        this.f903n.b(false);
        return this;
    }

    public MenuItem setNumericShortcut(char c2, int i2) {
        if (this.h == c2 && this.f898i == i2) {
            return this;
        }
        this.h = c2;
        this.f898i = KeyEvent.normalizeMetaState(i2);
        this.f903n.b(false);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.h = c2;
        this.f898i = KeyEvent.normalizeMetaState(i2);
        this.f899j = Character.toLowerCase(c3);
        this.f900k = KeyEvent.normalizeMetaState(i3);
        this.f903n.b(false);
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.f901l = null;
        this.f902m = i2;
        this.w = true;
        this.f903n.b(false);
        return this;
    }

    public MenuItem setTitle(int i2) {
        setTitle(this.f903n.a.getString(i2));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public MenuItem setActionView(int i2) {
        Context context = this.f903n.a;
        setActionView(LayoutInflater.from(context).inflate(i2, (ViewGroup) new LinearLayout(context), false));
        return this;
    }

    public void a(boolean z2) {
        int i2 = this.x;
        int i3 = (z2 ? 2 : 0) | (i2 & -3);
        this.x = i3;
        if (i2 != i3) {
            this.f903n.b(false);
        }
    }

    public ActionProvider a() {
        return this.A;
    }

    public SupportMenuItem a(ActionProvider actionProvider) {
        ActionProvider actionProvider2 = this.A;
        if (actionProvider2 != null) {
            actionProvider2.a = null;
        }
        this.z = null;
        this.A = actionProvider;
        this.f903n.b(true);
        ActionProvider actionProvider3 = this.A;
        if (actionProvider3 != null) {
            actionProvider3.a(new a());
        }
        return this;
    }
}
