package i.b.p.i;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import i.h.g.a.SupportSubMenu;

public class SubMenuWrapperICS extends MenuWrapperICS implements SubMenu {

    /* renamed from: e  reason: collision with root package name */
    public final SupportSubMenu f931e;

    public SubMenuWrapperICS(Context context, SupportSubMenu supportSubMenu) {
        super(context, supportSubMenu);
        this.f931e = supportSubMenu;
    }

    public void clearHeader() {
        this.f931e.clearHeader();
    }

    public MenuItem getItem() {
        return a(this.f931e.getItem());
    }

    public SubMenu setHeaderIcon(int i2) {
        this.f931e.setHeaderIcon(i2);
        return this;
    }

    public SubMenu setHeaderTitle(int i2) {
        this.f931e.setHeaderTitle(i2);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        this.f931e.setHeaderView(view);
        return this;
    }

    public SubMenu setIcon(int i2) {
        this.f931e.setIcon(i2);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        this.f931e.setHeaderIcon(drawable);
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        this.f931e.setHeaderTitle(charSequence);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        this.f931e.setIcon(drawable);
        return this;
    }
}
