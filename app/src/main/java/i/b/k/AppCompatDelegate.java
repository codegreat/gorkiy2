package i.b.k;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import i.e.ArraySet;
import java.lang.ref.WeakReference;
import java.util.Iterator;

public abstract class AppCompatDelegate {
    public static final ArraySet<WeakReference<j>> b = new ArraySet<>();
    public static final Object c = new Object();

    public static AppCompatDelegate a(Activity activity, AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(activity, null, appCompatCallback, activity);
    }

    public static void b(AppCompatDelegate appCompatDelegate) {
        synchronized (c) {
            c(appCompatDelegate);
        }
    }

    public static void c(AppCompatDelegate appCompatDelegate) {
        synchronized (c) {
            Iterator<WeakReference<j>> it = b.iterator();
            while (it.hasNext()) {
                AppCompatDelegate appCompatDelegate2 = (AppCompatDelegate) it.next().get();
                if (appCompatDelegate2 == appCompatDelegate || appCompatDelegate2 == null) {
                    it.remove();
                }
            }
        }
    }

    public abstract void a();

    public abstract void a(Bundle bundle);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(CharSequence charSequence);

    public abstract boolean a(int i2);

    public abstract void b();

    public abstract void b(int i2);

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void c();

    public static AppCompatDelegate a(Dialog dialog, AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(dialog.getContext(), dialog.getWindow(), appCompatCallback, dialog);
    }

    public static void a(AppCompatDelegate appCompatDelegate) {
        synchronized (c) {
            c(appCompatDelegate);
            b.add(new WeakReference(appCompatDelegate));
        }
    }
}
