package i.b.k;

import android.app.Dialog;
import i.l.a.DialogFragment;

public class AppCompatDialogFragment extends DialogFragment {
    public void a(Dialog dialog, int i2) {
        if (dialog instanceof AppCompatDialog) {
            AppCompatDialog appCompatDialog = (AppCompatDialog) dialog;
            if (!(i2 == 1 || i2 == 2)) {
                if (i2 == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            appCompatDialog.a(1);
            return;
        }
        super.a(dialog, i2);
    }
}
