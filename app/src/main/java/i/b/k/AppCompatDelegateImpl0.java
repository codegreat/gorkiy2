package i.b.k;

import android.view.View;
import i.h.l.ViewCompat;
import i.h.l.ViewPropertyAnimatorCompat;
import i.h.l.ViewPropertyAnimatorListener;
import i.h.l.ViewPropertyAnimatorListenerAdapter;

/* compiled from: AppCompatDelegateImpl */
public class AppCompatDelegateImpl0 implements Runnable {
    public final /* synthetic */ AppCompatDelegateImpl b;

    /* compiled from: AppCompatDelegateImpl */
    public class a extends ViewPropertyAnimatorListenerAdapter {
        public a() {
        }

        public void a(View view) {
            AppCompatDelegateImpl0.this.b.f747p.setAlpha(1.0f);
            AppCompatDelegateImpl0.this.b.f750s.a((ViewPropertyAnimatorListener) null);
            AppCompatDelegateImpl0.this.b.f750s = null;
        }

        public void b(View view) {
            AppCompatDelegateImpl0.this.b.f747p.setVisibility(0);
        }
    }

    public AppCompatDelegateImpl0(AppCompatDelegateImpl appCompatDelegateImpl) {
        this.b = appCompatDelegateImpl;
    }

    public void run() {
        AppCompatDelegateImpl appCompatDelegateImpl = this.b;
        appCompatDelegateImpl.f748q.showAtLocation(appCompatDelegateImpl.f747p, 55, 0, 0);
        this.b.e();
        if (this.b.k()) {
            this.b.f747p.setAlpha(0.0f);
            AppCompatDelegateImpl appCompatDelegateImpl2 = this.b;
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(appCompatDelegateImpl2.f747p);
            a2.a(1.0f);
            appCompatDelegateImpl2.f750s = a2;
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.b.f750s;
            a aVar = new a();
            View view = viewPropertyAnimatorCompat.a.get();
            if (view != null) {
                viewPropertyAnimatorCompat.a(view, aVar);
                return;
            }
            return;
        }
        this.b.f747p.setAlpha(1.0f);
        this.b.f747p.setVisibility(0);
    }
}
