package i.b.m.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseArray;
import i.b.k.ResourcesFlusher;
import i.b.m.a.StateListDrawable;

public class DrawableContainer extends Drawable implements Drawable.Callback {
    public c b;
    public Rect c;
    public Drawable d;

    /* renamed from: e  reason: collision with root package name */
    public Drawable f801e;

    /* renamed from: f  reason: collision with root package name */
    public int f802f = 255;
    public boolean g;
    public int h = -1;

    /* renamed from: i  reason: collision with root package name */
    public boolean f803i;

    /* renamed from: j  reason: collision with root package name */
    public Runnable f804j;

    /* renamed from: k  reason: collision with root package name */
    public long f805k;

    /* renamed from: l  reason: collision with root package name */
    public long f806l;

    /* renamed from: m  reason: collision with root package name */
    public b f807m;

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            DrawableContainer.this.a(true);
            DrawableContainer.this.invalidateSelf();
        }
    }

    public static class b implements Drawable.Callback {
        public Drawable.Callback b;

        public void invalidateDrawable(Drawable drawable) {
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
            Drawable.Callback callback = this.b;
            if (callback != null) {
                callback.scheduleDrawable(drawable, runnable, j2);
            }
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            Drawable.Callback callback = this.b;
            if (callback != null) {
                callback.unscheduleDrawable(drawable, runnable);
            }
        }
    }

    public c a() {
        throw null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(int r10) {
        /*
            r9 = this;
            int r0 = r9.h
            r1 = 0
            if (r10 != r0) goto L_0x0006
            return r1
        L_0x0006:
            long r2 = android.os.SystemClock.uptimeMillis()
            i.b.m.a.DrawableContainer$c r0 = r9.b
            int r0 = r0.B
            r4 = 0
            r5 = 0
            if (r0 <= 0) goto L_0x002e
            android.graphics.drawable.Drawable r0 = r9.f801e
            if (r0 == 0) goto L_0x001a
            r0.setVisible(r1, r1)
        L_0x001a:
            android.graphics.drawable.Drawable r0 = r9.d
            if (r0 == 0) goto L_0x0029
            r9.f801e = r0
            i.b.m.a.DrawableContainer$c r0 = r9.b
            int r0 = r0.B
            long r0 = (long) r0
            long r0 = r0 + r2
            r9.f806l = r0
            goto L_0x0035
        L_0x0029:
            r9.f801e = r4
            r9.f806l = r5
            goto L_0x0035
        L_0x002e:
            android.graphics.drawable.Drawable r0 = r9.d
            if (r0 == 0) goto L_0x0035
            r0.setVisible(r1, r1)
        L_0x0035:
            if (r10 < 0) goto L_0x0055
            i.b.m.a.DrawableContainer$c r0 = r9.b
            int r1 = r0.h
            if (r10 >= r1) goto L_0x0055
            android.graphics.drawable.Drawable r0 = r0.a(r10)
            r9.d = r0
            r9.h = r10
            if (r0 == 0) goto L_0x005a
            i.b.m.a.DrawableContainer$c r10 = r9.b
            int r10 = r10.A
            if (r10 <= 0) goto L_0x0051
            long r7 = (long) r10
            long r2 = r2 + r7
            r9.f805k = r2
        L_0x0051:
            r9.a(r0)
            goto L_0x005a
        L_0x0055:
            r9.d = r4
            r10 = -1
            r9.h = r10
        L_0x005a:
            long r0 = r9.f805k
            r10 = 1
            int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r2 != 0) goto L_0x0067
            long r0 = r9.f806l
            int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r2 == 0) goto L_0x0079
        L_0x0067:
            java.lang.Runnable r0 = r9.f804j
            if (r0 != 0) goto L_0x0073
            i.b.m.a.DrawableContainer$a r0 = new i.b.m.a.DrawableContainer$a
            r0.<init>()
            r9.f804j = r0
            goto L_0x0076
        L_0x0073:
            r9.unscheduleSelf(r0)
        L_0x0076:
            r9.a(r10)
        L_0x0079:
            r9.invalidateSelf()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.m.a.DrawableContainer.a(int):boolean");
    }

    public boolean canApplyTheme() {
        return this.b.canApplyTheme();
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.d;
        if (drawable != null) {
            super.draw(canvas);
        }
        Drawable drawable2 = this.f801e;
        if (drawable2 != null) {
            super.draw(canvas);
        }
    }

    public int getAlpha() {
        return this.f802f;
    }

    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.b.getChangingConfigurations();
    }

    public final Drawable.ConstantState getConstantState() {
        if (!this.b.a()) {
            return null;
        }
        this.b.d = getChangingConfigurations();
        return this.b;
    }

    public Drawable getCurrent() {
        return this.d;
    }

    public void getHotspotBounds(Rect rect) {
        Rect rect2 = this.c;
        if (rect2 != null) {
            rect.set(rect2);
        } else {
            super.getHotspotBounds(rect);
        }
    }

    public int getIntrinsicHeight() {
        c cVar = this.b;
        if (cVar.f813l) {
            if (!cVar.f814m) {
                cVar.b();
            }
            return cVar.f816o;
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return super.getIntrinsicHeight();
        }
        return -1;
    }

    public int getIntrinsicWidth() {
        c cVar = this.b;
        if (cVar.f813l) {
            if (!cVar.f814m) {
                cVar.b();
            }
            return cVar.f815n;
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return super.getIntrinsicWidth();
        }
        return -1;
    }

    public int getMinimumHeight() {
        c cVar = this.b;
        if (cVar.f813l) {
            if (!cVar.f814m) {
                cVar.b();
            }
            return cVar.f818q;
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return super.getMinimumHeight();
        }
        return 0;
    }

    public int getMinimumWidth() {
        c cVar = this.b;
        if (cVar.f813l) {
            if (!cVar.f814m) {
                cVar.b();
            }
            return cVar.f817p;
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return super.getMinimumWidth();
        }
        return 0;
    }

    public int getOpacity() {
        Drawable drawable = this.d;
        int i2 = -2;
        if (drawable == null || !super.isVisible()) {
            return -2;
        }
        c cVar = this.b;
        if (cVar.f819r) {
            return cVar.f820s;
        }
        cVar.c();
        int i3 = cVar.h;
        Drawable[] drawableArr = cVar.g;
        if (i3 > 0) {
            i2 = drawableArr[0].getOpacity();
        }
        for (int i4 = 1; i4 < i3; i4++) {
            i2 = Drawable.resolveOpacity(i2, drawableArr[i4].getOpacity());
        }
        cVar.f820s = i2;
        cVar.f819r = true;
        return i2;
    }

    public void getOutline(Outline outline) {
        Drawable drawable = this.d;
        if (drawable != null) {
            super.getOutline(outline);
        }
    }

    public boolean getPadding(Rect rect) {
        boolean z;
        c cVar = this.b;
        Rect rect2 = null;
        boolean z2 = true;
        if (!cVar.f810i) {
            if (cVar.f812k != null || cVar.f811j) {
                rect2 = cVar.f812k;
            } else {
                cVar.c();
                Rect rect3 = new Rect();
                int i2 = cVar.h;
                Drawable[] drawableArr = cVar.g;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3].getPadding(rect3)) {
                        if (rect2 == null) {
                            rect2 = new Rect(0, 0, 0, 0);
                        }
                        int i4 = rect3.left;
                        if (i4 > rect2.left) {
                            rect2.left = i4;
                        }
                        int i5 = rect3.top;
                        if (i5 > rect2.top) {
                            rect2.top = i5;
                        }
                        int i6 = rect3.right;
                        if (i6 > rect2.right) {
                            rect2.right = i6;
                        }
                        int i7 = rect3.bottom;
                        if (i7 > rect2.bottom) {
                            rect2.bottom = i7;
                        }
                    }
                }
                cVar.f811j = true;
                cVar.f812k = rect2;
            }
        }
        if (rect2 != null) {
            rect.set(rect2);
            z = (((rect2.left | rect2.top) | rect2.bottom) | rect2.right) != 0;
        } else {
            Drawable drawable = this.d;
            if (drawable != null) {
                z = super.getPadding(rect);
            } else {
                z = super.getPadding(rect);
            }
        }
        if (!this.b.C || ResourcesFlusher.b((Drawable) super) != 1) {
            z2 = false;
        }
        if (z2) {
            int i8 = rect.left;
            rect.left = rect.right;
            rect.right = i8;
        }
        return z;
    }

    public void invalidateDrawable(Drawable drawable) {
        c cVar = this.b;
        if (cVar != null) {
            cVar.f819r = false;
            cVar.f821t = false;
        }
        if (drawable == this.d && getCallback() != null) {
            getCallback().invalidateDrawable(super);
        }
    }

    public boolean isAutoMirrored() {
        return this.b.C;
    }

    public void jumpToCurrentState() {
        boolean z;
        Drawable drawable = this.f801e;
        boolean z2 = true;
        if (drawable != null) {
            super.jumpToCurrentState();
            this.f801e = null;
            z = true;
        } else {
            z = false;
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            super.jumpToCurrentState();
            if (this.g) {
                this.d.setAlpha(this.f802f);
            }
        }
        if (this.f806l != 0) {
            this.f806l = 0;
            z = true;
        }
        if (this.f805k != 0) {
            this.f805k = 0;
        } else {
            z2 = z;
        }
        if (z2) {
            invalidateSelf();
        }
    }

    public Drawable mutate() {
        if (!this.f803i && super.mutate() == this) {
            c a2 = a();
            a2.d();
            a(a2);
            this.f803i = true;
        }
        return super;
    }

    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f801e;
        if (drawable != null) {
            super.setBounds(rect);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            super.setBounds(rect);
        }
    }

    public boolean onLayoutDirectionChanged(int i2) {
        c cVar = this.b;
        int i3 = this.h;
        int i4 = cVar.h;
        Drawable[] drawableArr = cVar.g;
        boolean z = false;
        for (int i5 = 0; i5 < i4; i5++) {
            if (drawableArr[i5] != null) {
                boolean layoutDirection = Build.VERSION.SDK_INT >= 23 ? drawableArr[i5].setLayoutDirection(i2) : false;
                if (i5 == i3) {
                    z = layoutDirection;
                }
            }
        }
        cVar.z = i2;
        return z;
    }

    public boolean onLevelChange(int i2) {
        Drawable drawable = this.f801e;
        if (drawable != null) {
            return super.setLevel(i2);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            return super.setLevel(i2);
        }
        return false;
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        if (drawable == this.d && getCallback() != null) {
            getCallback().scheduleDrawable(super, runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        if (!this.g || this.f802f != i2) {
            this.g = true;
            this.f802f = i2;
            Drawable drawable = this.d;
            if (drawable == null) {
                return;
            }
            if (this.f805k == 0) {
                super.setAlpha(i2);
            } else {
                a(false);
            }
        }
    }

    public void setAutoMirrored(boolean z) {
        c cVar = this.b;
        if (cVar.C != z) {
            cVar.C = z;
            Drawable drawable = this.d;
            if (drawable != null) {
                super.setAutoMirrored(z);
            }
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        c cVar = this.b;
        cVar.E = true;
        if (cVar.D != colorFilter) {
            cVar.D = colorFilter;
            Drawable drawable = this.d;
            if (drawable != null) {
                super.setColorFilter(colorFilter);
            }
        }
    }

    public void setDither(boolean z) {
        c cVar = this.b;
        if (cVar.x != z) {
            cVar.x = z;
            Drawable drawable = this.d;
            if (drawable != null) {
                super.setDither(z);
            }
        }
    }

    public void setHotspot(float f2, float f3) {
        Drawable drawable = this.d;
        if (drawable != null) {
            super.setHotspot(f2, f3);
        }
    }

    public void setHotspotBounds(int i2, int i3, int i4, int i5) {
        Rect rect = this.c;
        if (rect == null) {
            this.c = new Rect(i2, i3, i4, i5);
        } else {
            rect.set(i2, i3, i4, i5);
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            super.setHotspotBounds(i2, i3, i4, i5);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        c cVar = this.b;
        cVar.H = true;
        if (cVar.F != colorStateList) {
            cVar.F = colorStateList;
            ResourcesFlusher.a(this.d, colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.b;
        cVar.I = true;
        if (cVar.G != mode) {
            cVar.G = mode;
            ResourcesFlusher.a(this.d, mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        Drawable drawable = this.f801e;
        if (drawable != null) {
            super.setVisible(z, z2);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            super.setVisible(z, z2);
        }
        return visible;
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        if (drawable == this.d && getCallback() != null) {
            getCallback().unscheduleDrawable(super, runnable);
        }
    }

    public static abstract class c extends Drawable.ConstantState {
        public int A;
        public int B;
        public boolean C;
        public ColorFilter D;
        public boolean E;
        public ColorStateList F;
        public PorterDuff.Mode G;
        public boolean H;
        public boolean I;
        public final DrawableContainer a;
        public Resources b;
        public int c = 160;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f808e;

        /* renamed from: f  reason: collision with root package name */
        public SparseArray<Drawable.ConstantState> f809f;
        public Drawable[] g;
        public int h;

        /* renamed from: i  reason: collision with root package name */
        public boolean f810i;

        /* renamed from: j  reason: collision with root package name */
        public boolean f811j;

        /* renamed from: k  reason: collision with root package name */
        public Rect f812k;

        /* renamed from: l  reason: collision with root package name */
        public boolean f813l;

        /* renamed from: m  reason: collision with root package name */
        public boolean f814m;

        /* renamed from: n  reason: collision with root package name */
        public int f815n;

        /* renamed from: o  reason: collision with root package name */
        public int f816o;

        /* renamed from: p  reason: collision with root package name */
        public int f817p;

        /* renamed from: q  reason: collision with root package name */
        public int f818q;

        /* renamed from: r  reason: collision with root package name */
        public boolean f819r;

        /* renamed from: s  reason: collision with root package name */
        public int f820s;

        /* renamed from: t  reason: collision with root package name */
        public boolean f821t;
        public boolean u;
        public boolean v;
        public boolean w;
        public boolean x;
        public boolean y;
        public int z;

        public c(c cVar, DrawableContainer drawableContainer, Resources resources) {
            Resources resources2;
            this.f810i = false;
            this.f813l = false;
            this.x = true;
            this.A = 0;
            this.B = 0;
            this.a = drawableContainer;
            if (resources != null) {
                resources2 = resources;
            } else {
                resources2 = cVar != null ? cVar.b : null;
            }
            this.b = resources2;
            int a2 = DrawableContainer.a(resources, cVar != null ? cVar.c : 0);
            this.c = a2;
            if (cVar != null) {
                this.d = cVar.d;
                this.f808e = cVar.f808e;
                this.v = true;
                this.w = true;
                this.f810i = cVar.f810i;
                this.f813l = cVar.f813l;
                this.x = cVar.x;
                this.y = cVar.y;
                this.z = cVar.z;
                this.A = cVar.A;
                this.B = cVar.B;
                this.C = cVar.C;
                this.D = cVar.D;
                this.E = cVar.E;
                this.F = cVar.F;
                this.G = cVar.G;
                this.H = cVar.H;
                this.I = cVar.I;
                if (cVar.c == a2) {
                    if (cVar.f811j) {
                        this.f812k = new Rect(cVar.f812k);
                        this.f811j = true;
                    }
                    if (cVar.f814m) {
                        this.f815n = cVar.f815n;
                        this.f816o = cVar.f816o;
                        this.f817p = cVar.f817p;
                        this.f818q = cVar.f818q;
                        this.f814m = true;
                    }
                }
                if (cVar.f819r) {
                    this.f820s = cVar.f820s;
                    this.f819r = true;
                }
                if (cVar.f821t) {
                    this.u = cVar.u;
                    this.f821t = true;
                }
                Drawable[] drawableArr = cVar.g;
                this.g = new Drawable[drawableArr.length];
                this.h = cVar.h;
                SparseArray<Drawable.ConstantState> sparseArray = cVar.f809f;
                if (sparseArray != null) {
                    this.f809f = sparseArray.clone();
                } else {
                    this.f809f = new SparseArray<>(this.h);
                }
                int i2 = this.h;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null) {
                        Drawable.ConstantState constantState = drawableArr[i3].getConstantState();
                        if (constantState != null) {
                            this.f809f.put(i3, constantState);
                        } else {
                            this.g[i3] = drawableArr[i3];
                        }
                    }
                }
                return;
            }
            this.g = new Drawable[10];
            this.h = 0;
        }

        public final int a(Drawable drawable) {
            int i2 = this.h;
            if (i2 >= this.g.length) {
                int i3 = i2 + 10;
                StateListDrawable.a aVar = (StateListDrawable.a) this;
                Drawable[] drawableArr = new Drawable[i3];
                System.arraycopy(aVar.g, 0, drawableArr, 0, i2);
                aVar.g = drawableArr;
                int[][] iArr = new int[i3][];
                System.arraycopy(aVar.J, 0, iArr, 0, i2);
                aVar.J = iArr;
            }
            drawable.mutate();
            drawable.setVisible(false, true);
            drawable.setCallback(this.a);
            this.g[i2] = drawable;
            this.h++;
            this.f808e = drawable.getChangingConfigurations() | this.f808e;
            this.f819r = false;
            this.f821t = false;
            this.f812k = null;
            this.f811j = false;
            this.f814m = false;
            this.v = false;
            return i2;
        }

        public void b() {
            this.f814m = true;
            c();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            this.f816o = -1;
            this.f815n = -1;
            this.f818q = 0;
            this.f817p = 0;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                int intrinsicWidth = drawable.getIntrinsicWidth();
                if (intrinsicWidth > this.f815n) {
                    this.f815n = intrinsicWidth;
                }
                int intrinsicHeight = drawable.getIntrinsicHeight();
                if (intrinsicHeight > this.f816o) {
                    this.f816o = intrinsicHeight;
                }
                int minimumWidth = drawable.getMinimumWidth();
                if (minimumWidth > this.f817p) {
                    this.f817p = minimumWidth;
                }
                int minimumHeight = drawable.getMinimumHeight();
                if (minimumHeight > this.f818q) {
                    this.f818q = minimumHeight;
                }
            }
        }

        public final void c() {
            SparseArray<Drawable.ConstantState> sparseArray = this.f809f;
            if (sparseArray != null) {
                int size = sparseArray.size();
                for (int i2 = 0; i2 < size; i2++) {
                    int keyAt = this.f809f.keyAt(i2);
                    Drawable[] drawableArr = this.g;
                    Drawable newDrawable = this.f809f.valueAt(i2).newDrawable(this.b);
                    if (Build.VERSION.SDK_INT >= 23) {
                        newDrawable.setLayoutDirection(this.z);
                    }
                    Drawable mutate = newDrawable.mutate();
                    mutate.setCallback(this.a);
                    drawableArr[keyAt] = mutate;
                }
                this.f809f = null;
            }
        }

        public boolean canApplyTheme() {
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                if (drawable == null) {
                    Drawable.ConstantState constantState = this.f809f.get(i3);
                    if (constantState != null && super.canApplyTheme()) {
                        return true;
                    }
                } else if (drawable.canApplyTheme()) {
                    return true;
                }
            }
            return false;
        }

        public abstract void d();

        public int getChangingConfigurations() {
            return this.d | this.f808e;
        }

        public final Drawable a(int i2) {
            int indexOfKey;
            Drawable drawable = this.g[i2];
            if (drawable != null) {
                return drawable;
            }
            SparseArray<Drawable.ConstantState> sparseArray = this.f809f;
            if (sparseArray == null || (indexOfKey = sparseArray.indexOfKey(i2)) < 0) {
                return null;
            }
            Drawable newDrawable = this.f809f.valueAt(indexOfKey).newDrawable(this.b);
            if (Build.VERSION.SDK_INT >= 23) {
                newDrawable.setLayoutDirection(this.z);
            }
            Drawable mutate = newDrawable.mutate();
            mutate.setCallback(this.a);
            this.g[i2] = mutate;
            this.f809f.removeAt(indexOfKey);
            if (this.f809f.size() == 0) {
                this.f809f = null;
            }
            return mutate;
        }

        public final void a(Resources resources) {
            if (resources != null) {
                this.b = resources;
                int a2 = DrawableContainer.a(resources, this.c);
                int i2 = this.c;
                this.c = a2;
                if (i2 != a2) {
                    this.f814m = false;
                    this.f811j = false;
                }
            }
        }

        public synchronized boolean a() {
            if (this.v) {
                return this.w;
            }
            c();
            this.v = true;
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            for (int i3 = 0; i3 < i2; i3++) {
                if (drawableArr[i3].getConstantState() == null) {
                    this.w = false;
                    return false;
                }
            }
            this.w = true;
            return true;
        }
    }

    public final void a(Drawable drawable) {
        if (this.f807m == null) {
            this.f807m = new b();
        }
        b bVar = this.f807m;
        bVar.b = super.getCallback();
        super.setCallback(bVar);
        try {
            if (this.b.A <= 0 && this.g) {
                super.setAlpha(this.f802f);
            }
            if (this.b.E) {
                super.setColorFilter(this.b.D);
            } else {
                if (this.b.H) {
                    super.setTintList(this.b.F);
                }
                if (this.b.I) {
                    super.setTintMode(this.b.G);
                }
            }
            super.setVisible(isVisible(), true);
            super.setDither(this.b.x);
            super.setState(getState());
            super.setLevel(getLevel());
            super.setBounds(getBounds());
            if (Build.VERSION.SDK_INT >= 23) {
                super.setLayoutDirection(getLayoutDirection());
            }
            super.setAutoMirrored(this.b.C);
            Rect rect = this.c;
            if (rect != null) {
                super.setHotspotBounds(rect.left, rect.top, rect.right, rect.bottom);
            }
        } finally {
            b bVar2 = this.f807m;
            Drawable.Callback callback = bVar2.b;
            bVar2.b = null;
            super.setCallback(callback);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(boolean r14) {
        /*
            r13 = this;
            r0 = 1
            r13.g = r0
            long r1 = android.os.SystemClock.uptimeMillis()
            android.graphics.drawable.Drawable r3 = r13.d
            r4 = 255(0xff, double:1.26E-321)
            r6 = 0
            r7 = 0
            if (r3 == 0) goto L_0x0038
            long r9 = r13.f805k
            int r11 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r11 == 0) goto L_0x003a
            int r11 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r11 > 0) goto L_0x0022
            int r9 = r13.f802f
            r3.setAlpha(r9)
            r13.f805k = r7
            goto L_0x003a
        L_0x0022:
            long r9 = r9 - r1
            long r9 = r9 * r4
            int r10 = (int) r9
            i.b.m.a.DrawableContainer$c r9 = r13.b
            int r9 = r9.A
            int r10 = r10 / r9
            int r9 = 255 - r10
            int r10 = r13.f802f
            int r9 = r9 * r10
            int r9 = r9 / 255
            r3.setAlpha(r9)
            r3 = 1
            goto L_0x003b
        L_0x0038:
            r13.f805k = r7
        L_0x003a:
            r3 = 0
        L_0x003b:
            android.graphics.drawable.Drawable r9 = r13.f801e
            if (r9 == 0) goto L_0x0065
            long r10 = r13.f806l
            int r12 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r12 == 0) goto L_0x0067
            int r12 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r12 > 0) goto L_0x0052
            r9.setVisible(r6, r6)
            r0 = 0
            r13.f801e = r0
            r13.f806l = r7
            goto L_0x0067
        L_0x0052:
            long r10 = r10 - r1
            long r10 = r10 * r4
            int r3 = (int) r10
            i.b.m.a.DrawableContainer$c r4 = r13.b
            int r4 = r4.B
            int r3 = r3 / r4
            int r4 = r13.f802f
            int r3 = r3 * r4
            int r3 = r3 / 255
            r9.setAlpha(r3)
            goto L_0x0068
        L_0x0065:
            r13.f806l = r7
        L_0x0067:
            r0 = r3
        L_0x0068:
            if (r14 == 0) goto L_0x0074
            if (r0 == 0) goto L_0x0074
            java.lang.Runnable r14 = r13.f804j
            r3 = 16
            long r1 = r1 + r3
            r13.scheduleSelf(r14, r1)
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.m.a.DrawableContainer.a(boolean):void");
    }

    public void a(c cVar) {
        this.b = cVar;
        int i2 = this.h;
        if (i2 >= 0) {
            Drawable a2 = cVar.a(i2);
            this.d = super;
            if (a2 != null) {
                a(super);
            }
        }
        this.f801e = null;
    }

    public static int a(Resources resources, int i2) {
        if (resources != null) {
            i2 = resources.getDisplayMetrics().densityDpi;
        }
        if (i2 == 0) {
            return 160;
        }
        return i2;
    }
}
