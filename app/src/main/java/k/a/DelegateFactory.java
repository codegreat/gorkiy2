package k.a;

import m.a.Provider;

public final class DelegateFactory<T> implements Factory<T> {
    public Provider<T> a;

    public T get() {
        Provider<T> provider = this.a;
        if (provider != null) {
            return provider.get();
        }
        throw new IllegalStateException();
    }
}
