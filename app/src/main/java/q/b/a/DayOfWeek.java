package q.b.a;

import j.a.a.a.outline;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;
import q.b.a.v.e;
import q.b.a.v.f;

public enum DayOfWeek implements e, f {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
    
    public static final DayOfWeek[] ENUMS = values();
    public static final TemporalQuery<a> FROM = new a();

    public class a implements TemporalQuery<a> {
        /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public Object a(TemporalAccessor temporalAccessor) {
            if (temporalAccessor instanceof DayOfWeek) {
                return (DayOfWeek) temporalAccessor;
            }
            try {
                return DayOfWeek.a(temporalAccessor.b(ChronoField.DAY_OF_WEEK));
            } catch (DateTimeException e2) {
                throw new DateTimeException(outline.a(temporalAccessor, outline.a("Unable to obtain DayOfWeek from TemporalAccessor: ", temporalAccessor, ", type ")), e2);
            }
        }
    }

    public static DayOfWeek a(int i2) {
        if (i2 >= 1 && i2 <= 7) {
            return ENUMS[i2 - 1];
        }
        throw new DateTimeException(outline.b("Invalid value for DayOfWeek: ", i2));
    }

    public int b(TemporalField temporalField) {
        if (temporalField == ChronoField.DAY_OF_WEEK) {
            return getValue();
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.DayOfWeek] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.DAY_OF_WEEK) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.DayOfWeek] */
    public long d(TemporalField temporalField) {
        if (temporalField == ChronoField.DAY_OF_WEEK) {
            return (long) getValue();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public int getValue() {
        return ordinal() + 1;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.DayOfWeek] */
    public <R> R a(TemporalQuery<R> temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.DAYS;
        }
        if (temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return temporalQuery.a(this);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.DayOfWeek] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.DAY_OF_WEEK) {
            return temporalField.g();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.DAY_OF_WEEK, (long) getValue());
    }
}
