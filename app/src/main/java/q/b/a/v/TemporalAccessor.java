package q.b.a.v;

public interface TemporalAccessor {
    <R> R a(TemporalQuery temporalQuery);

    ValueRange a(TemporalField temporalField);

    int b(TemporalField temporalField);

    boolean c(TemporalField temporalField);

    long d(TemporalField temporalField);
}
