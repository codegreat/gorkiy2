package q.b.a.v;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Map;
import q.b.a.t.i;

public enum ChronoField implements j {
    NANO_OF_SECOND("NanoOfSecond", ChronoUnit.NANOS, ChronoUnit.SECONDS, ValueRange.a(0, 999999999)),
    NANO_OF_DAY("NanoOfDay", ChronoUnit.NANOS, ChronoUnit.DAYS, ValueRange.a(0, 86399999999999L)),
    MICRO_OF_SECOND("MicroOfSecond", ChronoUnit.MICROS, ChronoUnit.SECONDS, ValueRange.a(0, 999999)),
    MICRO_OF_DAY("MicroOfDay", ChronoUnit.MICROS, ChronoUnit.DAYS, ValueRange.a(0, 86399999999L)),
    MILLI_OF_SECOND("MilliOfSecond", ChronoUnit.MILLIS, ChronoUnit.SECONDS, ValueRange.a(0, 999)),
    MILLI_OF_DAY("MilliOfDay", ChronoUnit.MILLIS, ChronoUnit.DAYS, ValueRange.a(0, 86399999)),
    SECOND_OF_MINUTE("SecondOfMinute", ChronoUnit.SECONDS, ChronoUnit.MINUTES, ValueRange.a(0, 59)),
    SECOND_OF_DAY("SecondOfDay", ChronoUnit.SECONDS, ChronoUnit.DAYS, ValueRange.a(0, 86399)),
    MINUTE_OF_HOUR("MinuteOfHour", ChronoUnit.MINUTES, ChronoUnit.HOURS, ValueRange.a(0, 59)),
    MINUTE_OF_DAY("MinuteOfDay", ChronoUnit.MINUTES, ChronoUnit.DAYS, ValueRange.a(0, 1439)),
    HOUR_OF_AMPM("HourOfAmPm", ChronoUnit.HOURS, ChronoUnit.HALF_DAYS, ValueRange.a(0, 11)),
    CLOCK_HOUR_OF_AMPM("ClockHourOfAmPm", ChronoUnit.HOURS, ChronoUnit.HALF_DAYS, ValueRange.a(1, 12)),
    HOUR_OF_DAY("HourOfDay", ChronoUnit.HOURS, ChronoUnit.DAYS, ValueRange.a(0, 23)),
    CLOCK_HOUR_OF_DAY("ClockHourOfDay", ChronoUnit.HOURS, ChronoUnit.DAYS, ValueRange.a(1, 24)),
    AMPM_OF_DAY("AmPmOfDay", ChronoUnit.HALF_DAYS, ChronoUnit.DAYS, ValueRange.a(0, 1)),
    DAY_OF_WEEK("DayOfWeek", ChronoUnit.DAYS, ChronoUnit.WEEKS, ValueRange.a(1, 7)),
    ALIGNED_DAY_OF_WEEK_IN_MONTH("AlignedDayOfWeekInMonth", ChronoUnit.DAYS, ChronoUnit.WEEKS, ValueRange.a(1, 7)),
    ALIGNED_DAY_OF_WEEK_IN_YEAR("AlignedDayOfWeekInYear", ChronoUnit.DAYS, ChronoUnit.WEEKS, ValueRange.a(1, 7)),
    DAY_OF_MONTH("DayOfMonth", ChronoUnit.DAYS, ChronoUnit.MONTHS, ValueRange.a(1, 28, 31)),
    DAY_OF_YEAR("DayOfYear", ChronoUnit.DAYS, ChronoUnit.YEARS, ValueRange.a(1, 365, 366)),
    EPOCH_DAY("EpochDay", ChronoUnit.DAYS, ChronoUnit.FOREVER, ValueRange.a(-365243219162L, 365241780471L)),
    ALIGNED_WEEK_OF_MONTH("AlignedWeekOfMonth", ChronoUnit.WEEKS, ChronoUnit.MONTHS, ValueRange.a(1, 4, 5)),
    ALIGNED_WEEK_OF_YEAR("AlignedWeekOfYear", ChronoUnit.WEEKS, ChronoUnit.YEARS, ValueRange.a(1, 53)),
    MONTH_OF_YEAR("MonthOfYear", ChronoUnit.MONTHS, ChronoUnit.YEARS, ValueRange.a(1, 12)),
    PROLEPTIC_MONTH("ProlepticMonth", ChronoUnit.MONTHS, ChronoUnit.FOREVER, ValueRange.a(-11999999988L, 11999999999L)),
    YEAR_OF_ERA("YearOfEra", ChronoUnit.YEARS, ChronoUnit.FOREVER, ValueRange.a(1, 999999999, 1000000000)),
    YEAR("Year", ChronoUnit.YEARS, ChronoUnit.FOREVER, ValueRange.a(-999999999, 999999999)),
    ERA("Era", ChronoUnit.ERAS, ChronoUnit.FOREVER, ValueRange.a(0, 1)),
    INSTANT_SECONDS("InstantSeconds", ChronoUnit.SECONDS, ChronoUnit.FOREVER, ValueRange.a(Long.MIN_VALUE, (long) RecyclerView.FOREVER_NS)),
    OFFSET_SECONDS("OffsetSeconds", ChronoUnit.SECONDS, ChronoUnit.FOREVER, ValueRange.a(-64800, 64800));
    
    public final TemporalUnit baseUnit;
    public final String name;
    public final ValueRange range;
    public final TemporalUnit rangeUnit;

    /* access modifiers changed from: public */
    ChronoField(String str, m mVar, m mVar2, n nVar) {
        this.name = str;
        this.baseUnit = mVar;
        this.rangeUnit = mVar2;
        this.range = nVar;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public int a(long j2) {
        return this.range.a(j2, (TemporalField) this);
    }

    public e a(Map<j, Long> map, e eVar, i iVar) {
        return null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public long b(TemporalAccessor temporalAccessor) {
        return temporalAccessor.d(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public ValueRange c(TemporalAccessor temporalAccessor) {
        return temporalAccessor.a((TemporalField) this);
    }

    public boolean f() {
        return ordinal() >= 15 && ordinal() <= 27;
    }

    public ValueRange g() {
        return this.range;
    }

    public boolean h() {
        return ordinal() < 15;
    }

    public String toString() {
        return this.name;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public boolean a(TemporalAccessor temporalAccessor) {
        return temporalAccessor.c(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public <R extends d> R a(R r2, long j2) {
        return r2.a(this, j2);
    }
}
