package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.crashlytics.android.answers.RetryManager;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.t.DateTimeFormatter;
import q.b.a.u.c;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class Instant extends c implements d, f, Comparable<c>, Serializable {
    public static final Instant d = new Instant(0, 0);
    public final long b;
    public final int c;

    static {
        b(-31557014167219200L, 0);
        b(31556889864403199L, 999999999);
    }

    public Instant(long j2, int i2) {
        this.b = j2;
        this.c = i2;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 2, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.Instant, q.b.a.v.TemporalAccessor] */
    public <R> R a(TemporalQuery<R> temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.NANOS;
        }
        if (temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return temporalQuery.a(this);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.Instant, q.b.a.v.TemporalAccessor] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.NANO_OF_SECOND || temporalField == ChronoField.MICRO_OF_SECOND || temporalField == ChronoField.MILLI_OF_SECOND) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public int compareTo(Object obj) {
        Instant instant = (Instant) obj;
        int a = Collections.a(this.b, instant.b);
        return a != 0 ? a : this.c - instant.c;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.Instant, q.b.a.v.TemporalAccessor] */
    public long d(TemporalField temporalField) {
        int i2;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 0) {
            i2 = this.c;
        } else if (ordinal == 2) {
            i2 = this.c / AnswersRetryFilesSender.BACKOFF_MS;
        } else if (ordinal == 4) {
            i2 = this.c / 1000000;
        } else if (ordinal == 28) {
            return this.b;
        } else {
            throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
        return (long) i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Instant)) {
            return false;
        }
        Instant instant = (Instant) obj;
        if (this.b == instant.b && this.c == instant.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j2 = this.b;
        return (this.c * 51) + ((int) (j2 ^ (j2 >>> 32)));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.Instant, q.b.a.v.TemporalAccessor] */
    public String toString() {
        return DateTimeFormatter.f3130m.a((TemporalAccessor) this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public static Instant b(long j2, long j3) {
        return a(Collections.d(j2, Collections.b(j3, 1000000000L)), Collections.a(j3, 1000000000));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public static Instant b(long j2) {
        return a(Collections.b(j2, 1000L), Collections.a(j2, (int) AnswersRetryFilesSender.BACKOFF_MS) * 1000000);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.Instant, q.b.a.v.TemporalAccessor] */
    public int b(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return a(temporalField).a(temporalField.b(this), temporalField);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 0) {
            return this.c;
        }
        if (ordinal == 2) {
            return this.c / AnswersRetryFilesSender.BACKOFF_MS;
        }
        if (ordinal == 4) {
            return this.c / 1000000;
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static Instant a(TemporalAccessor temporalAccessor) {
        try {
            return b(temporalAccessor.d(ChronoField.INSTANT_SECONDS), (long) temporalAccessor.b(ChronoField.NANO_OF_SECOND));
        } catch (DateTimeException e2) {
            throw new DateTimeException(outline.a(temporalAccessor, outline.a("Unable to obtain Instant from TemporalAccessor: ", temporalAccessor, ", type ")), e2);
        }
    }

    public static Instant a(long j2, int i2) {
        if ((((long) i2) | j2) == 0) {
            return d;
        }
        if (j2 >= -31557014167219200L && j2 <= 31556889864403199L) {
            return new Instant(j2, i2);
        }
        throw new DateTimeException("Instant exceeds minimum or maximum instant");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Instant.a(long, long):q.b.a.Instant
     arg types: [long, int]
     candidates:
      q.b.a.Instant.a(long, int):q.b.a.Instant
      q.b.a.Instant.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Instant.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.Instant.a(long, long):q.b.a.Instant */
    public Instant b(long j2, TemporalUnit temporalUnit) {
        if (!(temporalUnit instanceof ChronoUnit)) {
            return (Instant) temporalUnit.a(this, j2);
        }
        switch (((ChronoUnit) temporalUnit).ordinal()) {
            case 0:
                return a(0, j2);
            case 1:
                return a(j2 / RetryManager.NANOSECONDS_IN_MS, (j2 % RetryManager.NANOSECONDS_IN_MS) * 1000);
            case 2:
                return a(j2 / 1000, (j2 % 1000) * RetryManager.NANOSECONDS_IN_MS);
            case 3:
                return a(j2, 0L);
            case 4:
                return a(Collections.b(j2, 60));
            case 5:
                return a(Collections.b(j2, 3600));
            case 6:
                return a(Collections.b(j2, 43200));
            case 7:
                return a(Collections.b(j2, 86400));
            default:
                throw new UnsupportedTemporalTypeException("Unsupported unit: " + temporalUnit);
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.Instant, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        return Instant.super.a(temporalField);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    public Temporal a(TemporalAdjuster temporalAdjuster) {
        return (Instant) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v3, types: [q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r3v7, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r3v10, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r3v13, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r3v17, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q.b.a.v.Temporal a(q.b.a.v.TemporalField r3, long r4) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof q.b.a.v.ChronoField
            if (r0 == 0) goto L_0x0065
            r0 = r3
            q.b.a.v.ChronoField r0 = (q.b.a.v.ChronoField) r0
            q.b.a.v.ValueRange r1 = r0.range
            r1.b(r4, r0)
            int r0 = r0.ordinal()
            if (r0 == 0) goto L_0x0054
            r1 = 2
            if (r0 == r1) goto L_0x0046
            r1 = 4
            if (r0 == r1) goto L_0x0035
            r1 = 28
            if (r0 != r1) goto L_0x0029
            long r0 = r2.b
            int r3 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r3 == 0) goto L_0x0063
            int r3 = r2.c
            q.b.a.Instant r3 = a(r4, r3)
            goto L_0x006b
        L_0x0029:
            org.threeten.bp.temporal.UnsupportedTemporalTypeException r4 = new org.threeten.bp.temporal.UnsupportedTemporalTypeException
            java.lang.String r5 = "Unsupported field: "
            java.lang.String r3 = j.a.a.a.outline.a(r5, r3)
            r4.<init>(r3)
            throw r4
        L_0x0035:
            int r3 = (int) r4
            r4 = 1000000(0xf4240, float:1.401298E-39)
            int r3 = r3 * r4
            int r4 = r2.c
            if (r3 == r4) goto L_0x0063
            long r4 = r2.b
            q.b.a.Instant r3 = a(r4, r3)
            goto L_0x006b
        L_0x0046:
            int r3 = (int) r4
            int r3 = r3 * 1000
            int r4 = r2.c
            if (r3 == r4) goto L_0x0063
            long r4 = r2.b
            q.b.a.Instant r3 = a(r4, r3)
            goto L_0x006b
        L_0x0054:
            int r3 = r2.c
            long r0 = (long) r3
            int r3 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r3 == 0) goto L_0x0063
            long r0 = r2.b
            int r3 = (int) r4
            q.b.a.Instant r3 = a(r0, r3)
            goto L_0x006b
        L_0x0063:
            r3 = r2
            goto L_0x006b
        L_0x0065:
            q.b.a.v.Temporal r3 = r3.a(r2, r4)
            q.b.a.Instant r3 = (q.b.a.Instant) r3
        L_0x006b:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.Instant.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Instant.a(long, long):q.b.a.Instant
     arg types: [long, int]
     candidates:
      q.b.a.Instant.a(long, int):q.b.a.Instant
      q.b.a.Instant.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Instant.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.Instant.a(long, long):q.b.a.Instant */
    public Instant a(long j2) {
        return a(j2, 0L);
    }

    public final Instant a(long j2, long j3) {
        if ((j2 | j3) == 0) {
            return this;
        }
        return b(Collections.d(Collections.d(this.b, j2), j3 / 1000000000), ((long) this.c) + (j3 % 1000000000));
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.Instant, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.Instant
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.Instant.b(long, long):q.b.a.Instant
      q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.Instant */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.Instant
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.Instant.b(long, long):q.b.a.Instant
      q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.Instant */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.Instant
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.Instant.b(long, long):q.b.a.Instant
      q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Instant.b(long, q.b.a.v.TemporalUnit):q.b.a.Instant */
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.INSTANT_SECONDS, this.b).a((TemporalField) ChronoField.NANO_OF_SECOND, (long) this.c);
    }

    public static Instant a(DataInput dataInput) {
        return b(dataInput.readLong(), (long) dataInput.readInt());
    }
}
