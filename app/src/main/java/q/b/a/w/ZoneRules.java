package q.b.a.w;

import j.a.a.a.outline;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import q.b.a.Instant;
import q.b.a.LocalDateTime;
import q.b.a.ZoneOffset;
import q.b.a.e;
import q.b.a.p;

public abstract class ZoneRules {

    public static final class a extends ZoneRules implements Serializable {
        public final ZoneOffset b;

        public a(ZoneOffset zoneOffset) {
            this.b = zoneOffset;
        }

        public ZoneOffset a(Instant instant) {
            return this.b;
        }

        public ZoneOffsetTransition a(LocalDateTime localDateTime) {
            return null;
        }

        public boolean a() {
            return true;
        }

        public List<p> b(e eVar) {
            return Collections.singletonList(this.b);
        }

        public boolean b(Instant instant) {
            return false;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                return this.b.equals(((a) obj).b);
            }
            if (!(obj instanceof StandardZoneRules)) {
                return false;
            }
            StandardZoneRules standardZoneRules = (StandardZoneRules) obj;
            if (!standardZoneRules.a() || !this.b.equals(standardZoneRules.a(Instant.d))) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i2 = this.b.c;
            return ((i2 + 31) ^ (((i2 + 31) ^ 1) ^ 1)) ^ 1;
        }

        public String toString() {
            StringBuilder a = outline.a("FixedRules:");
            a.append(this.b);
            return a.toString();
        }

        public boolean a(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
            return this.b.equals(zoneOffset);
        }
    }

    public abstract ZoneOffset a(Instant instant);

    public abstract ZoneOffsetTransition a(LocalDateTime localDateTime);

    public abstract boolean a();

    public abstract boolean a(LocalDateTime localDateTime, ZoneOffset zoneOffset);

    public abstract List<p> b(e eVar);

    public abstract boolean b(Instant instant);
}
