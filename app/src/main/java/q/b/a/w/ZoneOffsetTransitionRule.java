package q.b.a.w;

import j.a.a.a.outline;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.Serializable;
import n.i.Collections;
import q.b.a.DayOfWeek;
import q.b.a.LocalTime;
import q.b.a.Month;
import q.b.a.ZoneOffset;

public final class ZoneOffsetTransitionRule implements Serializable {
    public final Month b;
    public final byte c;
    public final DayOfWeek d;

    /* renamed from: e  reason: collision with root package name */
    public final LocalTime f3162e;

    /* renamed from: f  reason: collision with root package name */
    public final int f3163f;
    public final a g;
    public final ZoneOffset h;

    /* renamed from: i  reason: collision with root package name */
    public final ZoneOffset f3164i;

    /* renamed from: j  reason: collision with root package name */
    public final ZoneOffset f3165j;

    public enum a {
        UTC,
        WALL,
        STANDARD
    }

    public ZoneOffsetTransitionRule(Month month, int i2, DayOfWeek dayOfWeek, LocalTime localTime, int i3, a aVar, ZoneOffset zoneOffset, ZoneOffset zoneOffset2, ZoneOffset zoneOffset3) {
        this.b = month;
        this.c = (byte) i2;
        this.d = dayOfWeek;
        this.f3162e = localTime;
        this.f3163f = i3;
        this.g = aVar;
        this.h = zoneOffset;
        this.f3164i = zoneOffset2;
        this.f3165j = zoneOffset3;
    }

    private Object writeReplace() {
        return new Ser((byte) 3, this);
    }

    public void a(DataOutput dataOutput) {
        byte b2;
        int i2;
        int i3;
        int j2 = (this.f3163f * 86400) + this.f3162e.j();
        int i4 = this.h.c;
        int i5 = this.f3164i.c - i4;
        int i6 = this.f3165j.c - i4;
        if (j2 % 3600 != 0 || j2 > 86400) {
            b2 = 31;
        } else {
            b2 = j2 == 86400 ? 24 : this.f3162e.b;
        }
        int i7 = i4 % 900 == 0 ? (i4 / 900) + 128 : 255;
        if (i5 == 0 || i5 == 1800 || i5 == 3600) {
            i2 = i5 / 1800;
        } else {
            i2 = 3;
        }
        if (i6 == 0 || i6 == 1800 || i6 == 3600) {
            i3 = i6 / 1800;
        } else {
            i3 = 3;
        }
        DayOfWeek dayOfWeek = this.d;
        dataOutput.writeInt((this.b.getValue() << 28) + ((this.c + 32) << 22) + ((dayOfWeek == null ? 0 : dayOfWeek.getValue()) << 19) + (b2 << 14) + (this.g.ordinal() << 12) + (i7 << 4) + (i2 << 2) + i3);
        if (b2 == 31) {
            dataOutput.writeInt(j2);
        }
        if (i7 == 255) {
            dataOutput.writeInt(i4);
        }
        if (i2 == 3) {
            dataOutput.writeInt(this.f3164i.c);
        }
        if (i3 == 3) {
            dataOutput.writeInt(this.f3165j.c);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ZoneOffsetTransitionRule)) {
            return false;
        }
        ZoneOffsetTransitionRule zoneOffsetTransitionRule = (ZoneOffsetTransitionRule) obj;
        if (this.b == zoneOffsetTransitionRule.b && this.c == zoneOffsetTransitionRule.c && this.d == zoneOffsetTransitionRule.d && this.g == zoneOffsetTransitionRule.g && this.f3163f == zoneOffsetTransitionRule.f3163f && this.f3162e.equals(zoneOffsetTransitionRule.f3162e) && this.h.equals(zoneOffsetTransitionRule.h) && this.f3164i.equals(zoneOffsetTransitionRule.f3164i) && this.f3165j.equals(zoneOffsetTransitionRule.f3165j)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int j2 = ((this.f3162e.j() + this.f3163f) << 15) + (this.b.ordinal() << 11) + ((this.c + 32) << 5);
        DayOfWeek dayOfWeek = this.d;
        return ((this.h.c ^ (this.g.ordinal() + (j2 + ((dayOfWeek == null ? 7 : dayOfWeek.ordinal()) << 2)))) ^ this.f3164i.c) ^ this.f3165j.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public String toString() {
        StringBuilder a2 = outline.a("TransitionRule[");
        ZoneOffset zoneOffset = this.f3164i;
        ZoneOffset zoneOffset2 = this.f3165j;
        if (zoneOffset != null) {
            a2.append(zoneOffset2.c - zoneOffset.c > 0 ? "Gap " : "Overlap ");
            a2.append(this.f3164i);
            a2.append(" to ");
            a2.append(this.f3165j);
            a2.append(", ");
            DayOfWeek dayOfWeek = this.d;
            if (dayOfWeek != null) {
                byte b2 = this.c;
                if (b2 == -1) {
                    a2.append(dayOfWeek.name());
                    a2.append(" on or before last day of ");
                    a2.append(this.b.name());
                } else if (b2 < 0) {
                    a2.append(dayOfWeek.name());
                    a2.append(" on or before last day minus ");
                    a2.append((-this.c) - 1);
                    a2.append(" of ");
                    a2.append(this.b.name());
                } else {
                    a2.append(dayOfWeek.name());
                    a2.append(" on or after ");
                    a2.append(this.b.name());
                    a2.append(' ');
                    a2.append((int) this.c);
                }
            } else {
                a2.append(this.b.name());
                a2.append(' ');
                a2.append((int) this.c);
            }
            a2.append(" at ");
            if (this.f3163f == 0) {
                a2.append(this.f3162e);
            } else {
                long j2 = (long) ((this.f3163f * 24 * 60) + (this.f3162e.j() / 60));
                long b3 = Collections.b(j2, 60L);
                if (b3 < 10) {
                    a2.append(0);
                }
                a2.append(b3);
                a2.append(':');
                long a3 = (long) Collections.a(j2, 60);
                if (a3 < 10) {
                    a2.append(0);
                }
                a2.append(a3);
            }
            a2.append(" ");
            a2.append(this.g);
            a2.append(", standard offset ");
            a2.append(this.h);
            a2.append(']');
            return a2.toString();
        }
        throw null;
    }

    public static ZoneOffsetTransitionRule a(DataInput dataInput) {
        DayOfWeek dayOfWeek;
        ZoneOffset zoneOffset;
        ZoneOffset zoneOffset2;
        int readInt = dataInput.readInt();
        Month a2 = Month.a(readInt >>> 28);
        int i2 = ((264241152 & readInt) >>> 22) - 32;
        int i3 = (3670016 & readInt) >>> 19;
        if (i3 == 0) {
            dayOfWeek = null;
        } else {
            dayOfWeek = DayOfWeek.a(i3);
        }
        DayOfWeek dayOfWeek2 = dayOfWeek;
        int i4 = (507904 & readInt) >>> 14;
        a aVar = a.values()[(readInt & 12288) >>> 12];
        int i5 = (readInt & 4080) >>> 4;
        int i6 = (readInt & 12) >>> 2;
        int i7 = readInt & 3;
        int readInt2 = i4 == 31 ? dataInput.readInt() : i4 * 3600;
        ZoneOffset a3 = ZoneOffset.a(i5 == 255 ? dataInput.readInt() : (i5 - 128) * 900);
        if (i6 == 3) {
            zoneOffset = ZoneOffset.a(dataInput.readInt());
        } else {
            zoneOffset = ZoneOffset.a((i6 * 1800) + a3.c);
        }
        ZoneOffset zoneOffset3 = zoneOffset;
        if (i7 == 3) {
            zoneOffset2 = ZoneOffset.a(dataInput.readInt());
        } else {
            zoneOffset2 = ZoneOffset.a((i7 * 1800) + a3.c);
        }
        if (i2 < -28 || i2 > 31 || i2 == 0) {
            throw new IllegalArgumentException("Day of month indicator must be between -28 and 31 inclusive excluding zero");
        }
        return new ZoneOffsetTransitionRule(a2, i2, dayOfWeek2, LocalTime.f((long) Collections.b(readInt2, 86400)), readInt2 >= 0 ? readInt2 / 86400 : ((readInt2 + 1) / 86400) - 1, aVar, a3, zoneOffset3, zoneOffset2);
    }
}
