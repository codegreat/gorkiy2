package q.b.a.w;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.Externalizable;
import java.io.InvalidClassException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;
import q.b.a.ZoneOffset;

public final class Ser implements Externalizable {
    public byte b;
    public Object c;

    public Ser() {
    }

    public static Object a(byte b2, DataInput dataInput) {
        if (b2 == 1) {
            int readInt = dataInput.readInt();
            long[] jArr = new long[readInt];
            for (int i2 = 0; i2 < readInt; i2++) {
                jArr[i2] = a(dataInput);
            }
            int i3 = readInt + 1;
            ZoneOffset[] zoneOffsetArr = new ZoneOffset[i3];
            for (int i4 = 0; i4 < i3; i4++) {
                zoneOffsetArr[i4] = b(dataInput);
            }
            int readInt2 = dataInput.readInt();
            long[] jArr2 = new long[readInt2];
            for (int i5 = 0; i5 < readInt2; i5++) {
                jArr2[i5] = a(dataInput);
            }
            int i6 = readInt2 + 1;
            ZoneOffset[] zoneOffsetArr2 = new ZoneOffset[i6];
            for (int i7 = 0; i7 < i6; i7++) {
                zoneOffsetArr2[i7] = b(dataInput);
            }
            int readByte = dataInput.readByte();
            ZoneOffsetTransitionRule[] zoneOffsetTransitionRuleArr = new ZoneOffsetTransitionRule[readByte];
            for (int i8 = 0; i8 < readByte; i8++) {
                zoneOffsetTransitionRuleArr[i8] = ZoneOffsetTransitionRule.a(dataInput);
            }
            return new StandardZoneRules(jArr, zoneOffsetArr, jArr2, zoneOffsetArr2, zoneOffsetTransitionRuleArr);
        } else if (b2 == 2) {
            long a = a(dataInput);
            ZoneOffset b3 = b(dataInput);
            ZoneOffset b4 = b(dataInput);
            if (!b3.equals(b4)) {
                return new ZoneOffsetTransition(a, b3, b4);
            }
            throw new IllegalArgumentException("Offsets must not be equal");
        } else if (b2 == 3) {
            return ZoneOffsetTransitionRule.a(dataInput);
        } else {
            throw new StreamCorruptedException("Unknown serialized type");
        }
    }

    public static ZoneOffset b(DataInput dataInput) {
        byte readByte = dataInput.readByte();
        return readByte == Byte.MAX_VALUE ? ZoneOffset.a(dataInput.readInt()) : ZoneOffset.a(readByte * 900);
    }

    private Object readResolve() {
        return this.c;
    }

    public void readExternal(ObjectInput objectInput) {
        byte readByte = objectInput.readByte();
        this.b = readByte;
        this.c = a(readByte, objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        byte b2 = this.b;
        Object obj = this.c;
        objectOutput.writeByte(b2);
        if (b2 == 1) {
            StandardZoneRules standardZoneRules = (StandardZoneRules) obj;
            objectOutput.writeInt(standardZoneRules.b.length);
            for (long a : standardZoneRules.b) {
                a(a, objectOutput);
            }
            for (ZoneOffset a2 : standardZoneRules.c) {
                a(a2, objectOutput);
            }
            objectOutput.writeInt(standardZoneRules.d.length);
            for (long a3 : standardZoneRules.d) {
                a(a3, objectOutput);
            }
            for (ZoneOffset a4 : standardZoneRules.f3161f) {
                a(a4, objectOutput);
            }
            objectOutput.writeByte(standardZoneRules.g.length);
            for (ZoneOffsetTransitionRule a5 : standardZoneRules.g) {
                a5.a(objectOutput);
            }
        } else if (b2 == 2) {
            ZoneOffsetTransition zoneOffsetTransition = (ZoneOffsetTransition) obj;
            a(zoneOffsetTransition.b.a(zoneOffsetTransition.c), objectOutput);
            a(zoneOffsetTransition.c, objectOutput);
            a(zoneOffsetTransition.d, objectOutput);
        } else if (b2 == 3) {
            ((ZoneOffsetTransitionRule) obj).a(objectOutput);
        } else {
            throw new InvalidClassException("Unknown serialized type");
        }
    }

    public Ser(byte b2, Object obj) {
        this.b = b2;
        this.c = obj;
    }

    public static void a(long j2, DataOutput dataOutput) {
        if (j2 < -4575744000L || j2 >= 10413792000L || j2 % 900 != 0) {
            dataOutput.writeByte(255);
            dataOutput.writeLong(j2);
            return;
        }
        int i2 = (int) ((j2 + 4575744000L) / 900);
        dataOutput.writeByte((i2 >>> 16) & 255);
        dataOutput.writeByte((i2 >>> 8) & 255);
        dataOutput.writeByte(i2 & 255);
    }

    public static long a(DataInput dataInput) {
        byte readByte = dataInput.readByte() & 255;
        if (readByte == 255) {
            return dataInput.readLong();
        }
        return (((long) (((readByte << 16) + ((dataInput.readByte() & 255) << 8)) + (dataInput.readByte() & 255))) * 900) - 4575744000L;
    }

    public static void a(ZoneOffset zoneOffset, DataOutput dataOutput) {
        int i2 = zoneOffset.c;
        int i3 = i2 % 900 == 0 ? i2 / 900 : 127;
        dataOutput.writeByte(i3);
        if (i3 == 127) {
            dataOutput.writeInt(i2);
        }
    }
}
