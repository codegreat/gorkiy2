package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.RetryManager;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.ChronoLocalDateTime;
import q.b.a.s.ChronoZonedDateTime;
import q.b.a.t.DateTimeFormatter;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class LocalDateTime extends ChronoLocalDateTime<d> implements d, f, Serializable {
    public static final LocalDateTime d = b(LocalDate.f3088e, LocalTime.f3092f);

    /* renamed from: e  reason: collision with root package name */
    public static final LocalDateTime f3090e = b(LocalDate.f3089f, LocalTime.g);

    /* renamed from: f  reason: collision with root package name */
    public static final TemporalQuery<e> f3091f = new a();
    public final LocalDate b;
    public final LocalTime c;

    public class a implements TemporalQuery<e> {
        public Object a(TemporalAccessor temporalAccessor) {
            return LocalDateTime.a(temporalAccessor);
        }
    }

    public LocalDateTime(LocalDate localDate, LocalTime localTime) {
        this.b = localDate;
        this.c = localTime;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 4, this);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalDateTime] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField.f() || temporalField.h()) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalDateTime] */
    public long d(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h() ? this.c.d(temporalField) : this.b.d(temporalField);
        }
        return temporalField.b(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalDateTime)) {
            return false;
        }
        LocalDateTime localDateTime = (LocalDateTime) obj;
        if (!this.b.equals(localDateTime.b) || !this.c.equals(localDateTime.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.b.hashCode() ^ this.c.hashCode();
    }

    public ChronoLocalDate j() {
        return this.b;
    }

    public LocalTime l() {
        return this.c;
    }

    public String toString() {
        return this.b.toString() + 'T' + this.c.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static LocalDateTime b(ZoneId zoneId) {
        Collections.a((Object) zoneId, "zone");
        Instant b2 = Instant.b(System.currentTimeMillis());
        return a(b2.b, b2.c, zoneId.g().a(b2));
    }

    public LocalDateTime c(long j2) {
        return a(this.b, 0, 0, j2, 0, 1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalDateTime] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public String a(DateTimeFormatter dateTimeFormatter) {
        Collections.a((Object) dateTimeFormatter, "formatter");
        return dateTimeFormatter.a((TemporalAccessor) this);
    }

    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.f3151f) {
            return this.b;
        }
        return super.a(temporalQuery);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDate, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalTime, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static LocalDateTime b(LocalDate localDate, LocalTime localTime) {
        Collections.a((Object) localDate, "date");
        Collections.a((Object) localTime, "time");
        return new LocalDateTime(localDate, localTime);
    }

    public static LocalDateTime a(int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return new LocalDateTime(LocalDate.a(i2, i3, i4), LocalTime.b(i5, i6, i7, i8));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.LocalDateTime] */
    public int b(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h() ? this.c.b(temporalField) : this.b.b(temporalField);
        }
        return LocalDateTime.super.b(temporalField);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public static LocalDateTime a(long j2, int i2, ZoneOffset zoneOffset) {
        Collections.a((Object) zoneOffset, "offset");
        long j3 = j2 + ((long) zoneOffset.c);
        return new LocalDateTime(LocalDate.e(Collections.b(j3, 86400L)), LocalTime.a((long) Collections.a(j3, 86400), i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate */
    public LocalDateTime b(long j2, TemporalUnit temporalUnit) {
        long j3 = j2;
        TemporalUnit temporalUnit2 = temporalUnit;
        if (!(temporalUnit2 instanceof ChronoUnit)) {
            return (LocalDateTime) temporalUnit2.a(this, j3);
        }
        switch (((ChronoUnit) temporalUnit2).ordinal()) {
            case 0:
                return b(j2);
            case 1:
                return a(j3 / 86400000000L).b((j3 % 86400000000L) * 1000);
            case 2:
                return a(j3 / 86400000).b((j3 % 86400000) * RetryManager.NANOSECONDS_IN_MS);
            case 3:
                return c(j2);
            case 4:
                return a(this.b, 0, j2, 0, 0, 1);
            case 5:
                return a(this.b, j2, 0, 0, 0, 1);
            case 6:
                LocalDateTime a2 = a(j3 / 256);
                return a2.a(a2.b, (j3 % 256) * 12, 0, 0, 0, 1);
            default:
                return a(this.b.b(j3, temporalUnit2), this.c);
        }
    }

    public static LocalDateTime a(TemporalAccessor temporalAccessor) {
        if (temporalAccessor instanceof LocalDateTime) {
            return (LocalDateTime) temporalAccessor;
        }
        if (temporalAccessor instanceof ZonedDateTime) {
            return ((ZonedDateTime) temporalAccessor).b;
        }
        try {
            return new LocalDateTime(LocalDate.a(temporalAccessor), LocalTime.a(temporalAccessor));
        } catch (DateTimeException unused) {
            throw new DateTimeException(outline.a(temporalAccessor, outline.a("Unable to obtain LocalDateTime from TemporalAccessor: ", temporalAccessor, ", type ")));
        }
    }

    public LocalDateTime b(long j2) {
        return a(this.b, 0, 0, 0, j2, 1);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.s.ChronoLocalDate] */
    public boolean b(ChronoLocalDateTime<?> chronoLocalDateTime) {
        if (!(chronoLocalDateTime instanceof LocalDateTime)) {
            int i2 = (j().l() > super.j().l() ? 1 : (j().l() == super.j().l() ? 0 : -1));
            if (i2 < 0 || (i2 == 0 && l().i() < super.l().i())) {
                return true;
            }
            return false;
        } else if (a((LocalDateTime) chronoLocalDateTime) < 0) {
            return true;
        } else {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static LocalDateTime a(CharSequence charSequence, DateTimeFormatter dateTimeFormatter) {
        Collections.a((Object) dateTimeFormatter, "formatter");
        return (LocalDateTime) dateTimeFormatter.a(charSequence, f3091f);
    }

    public final LocalDateTime a(LocalDate localDate, LocalTime localTime) {
        if (this.b == localDate && this.c == localTime) {
            return this;
        }
        return new LocalDateTime(localDate, localTime);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalDateTime] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h() ? this.c.a(temporalField) : this.b.a(temporalField);
        }
        return temporalField.c(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.Temporal, q.b.a.LocalDateTime] */
    public LocalDateTime a(TemporalAdjuster temporalAdjuster) {
        if (temporalAdjuster instanceof LocalDate) {
            return a((LocalDate) temporalAdjuster, this.c);
        }
        if (temporalAdjuster instanceof LocalTime) {
            return a(this.b, (LocalTime) temporalAdjuster);
        }
        if (temporalAdjuster instanceof LocalDateTime) {
            return (LocalDateTime) temporalAdjuster;
        }
        return (LocalDateTime) temporalAdjuster.a(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalTime
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalTime.a(int, int):q.b.a.LocalTime
      q.b.a.LocalTime.a(long, int):q.b.a.LocalTime
      q.b.a.LocalTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalDate.a(int, int):q.b.a.LocalDate
      q.b.a.LocalDate.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate */
    public LocalDateTime a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (LocalDateTime) temporalField.a(this, j2);
        }
        if (temporalField.h()) {
            return a(this.b, this.c.a(temporalField, j2));
        }
        return a(this.b.a(temporalField, j2), this.c);
    }

    public LocalDateTime a(long j2) {
        return a(this.b.a(j2), this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDateTime.b(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDateTime.b(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDateTime.b(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime */
    public LocalDateTime a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public final LocalDateTime a(LocalDate localDate, long j2, long j3, long j4, long j5, int i2) {
        LocalDate localDate2 = localDate;
        if ((j2 | j3 | j4 | j5) == 0) {
            return a(localDate2, this.c);
        }
        long j6 = j2 / 24;
        long j7 = j6 + (j3 / 1440) + (j4 / 86400) + (j5 / 86400000000000L);
        long j8 = (long) i2;
        long j9 = ((j2 % 24) * 3600000000000L) + ((j3 % 1440) * 60000000000L) + ((j4 % 86400) * 1000000000) + (j5 % 86400000000000L);
        long i3 = this.c.i();
        long j10 = (j9 * j8) + i3;
        long b2 = Collections.b(j10, 86400000000000L) + (j7 * j8);
        long c2 = Collections.c(j10, 86400000000000L);
        return a(localDate2.a(b2), c2 == i3 ? this.c : LocalTime.e(c2));
    }

    public Temporal a(Temporal temporal) {
        return super.a(temporal);
    }

    public ChronoZonedDateTime a(ZoneId zoneId) {
        return ZonedDateTime.a(this, zoneId);
    }

    /* renamed from: a */
    public int compareTo(ChronoLocalDateTime<?> chronoLocalDateTime) {
        if (chronoLocalDateTime instanceof LocalDateTime) {
            return a((LocalDateTime) chronoLocalDateTime);
        }
        return super.compareTo(super);
    }

    public final int a(LocalDateTime localDateTime) {
        int a2 = this.b.a(localDateTime.b);
        return a2 == 0 ? this.c.compareTo(localDateTime.c) : a2;
    }

    public void a(DataOutput dataOutput) {
        LocalDate localDate = this.b;
        dataOutput.writeInt(localDate.b);
        dataOutput.writeByte(localDate.c);
        dataOutput.writeByte(localDate.d);
        this.c.a(dataOutput);
    }

    public static LocalDateTime a(DataInput dataInput) {
        return b(LocalDate.a(dataInput), LocalTime.a(dataInput));
    }
}
