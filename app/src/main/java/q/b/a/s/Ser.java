package q.b.a.s;

import java.io.Externalizable;
import java.io.InvalidClassException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;
import q.b.a.LocalDate;
import q.b.a.LocalTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.f;
import q.b.a.o;
import q.b.a.v.ChronoField;

public final class Ser implements Externalizable {
    public byte b;
    public Object c;

    public Ser() {
    }

    private Object readResolve() {
        return this.c;
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    public void readExternal(ObjectInput objectInput) {
        Object obj;
        Object obj2;
        byte readByte = objectInput.readByte();
        this.b = readByte;
        switch (readByte) {
            case 1:
                obj = JapaneseDate.a(objectInput);
                break;
            case 2:
                obj = JapaneseEra.a(objectInput);
                break;
            case 3:
                obj = HijrahDate.a(objectInput);
                break;
            case 4:
                obj = HijrahEra.a(objectInput);
                break;
            case 5:
                int readInt = objectInput.readInt();
                byte readByte2 = objectInput.readByte();
                byte readByte3 = objectInput.readByte();
                MinguoChronology minguoChronology = MinguoChronology.d;
                obj2 = new MinguoDate(LocalDate.a(readInt + 1911, readByte2, readByte3));
                obj = obj2;
                break;
            case 6:
                obj = MinguoEra.a(objectInput);
                break;
            case 7:
                int readInt2 = objectInput.readInt();
                byte readByte4 = objectInput.readByte();
                byte readByte5 = objectInput.readByte();
                ThaiBuddhistChronology thaiBuddhistChronology = ThaiBuddhistChronology.d;
                obj2 = new ThaiBuddhistDate(LocalDate.a(readInt2 - 543, readByte4, readByte5));
                obj = obj2;
                break;
            case 8:
                obj = ThaiBuddhistEra.a(objectInput);
                break;
            case 9:
            case 10:
            default:
                throw new StreamCorruptedException("Unknown serialized type");
            case 11:
                obj = Chronology.a(objectInput);
                break;
            case 12:
                obj = ((ChronoLocalDate) objectInput.readObject()).a((f) ((LocalTime) objectInput.readObject()));
                break;
            case 13:
                obj = ((ChronoLocalDateTime) objectInput.readObject()).a((o) ((ZoneOffset) objectInput.readObject())).a((o) ((ZoneId) objectInput.readObject()));
                break;
        }
        this.c = obj;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.s.JapaneseDate] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v3, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.s.HijrahDate] */
    /* JADX WARN: Type inference failed for: r0v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v10, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v12, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v5, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.s.MinguoDate] */
    /* JADX WARN: Type inference failed for: r0v15, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v17, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v19, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v7, types: [q.b.a.s.ThaiBuddhistDate, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    /* JADX WARN: Type inference failed for: r0v22, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v24, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v26, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public void writeExternal(ObjectOutput objectOutput) {
        byte b2 = this.b;
        Object obj = this.c;
        objectOutput.writeByte(b2);
        switch (b2) {
            case 1:
                ? r1 = (JapaneseDate) obj;
                if (r1 != 0) {
                    objectOutput.writeInt(r1.b(ChronoField.YEAR));
                    objectOutput.writeByte(r1.b(ChronoField.MONTH_OF_YEAR));
                    objectOutput.writeByte(r1.b(ChronoField.DAY_OF_MONTH));
                    return;
                }
                throw null;
            case 2:
                objectOutput.writeByte(((JapaneseEra) obj).b);
                return;
            case 3:
                ? r12 = (HijrahDate) obj;
                if (r12 != 0) {
                    objectOutput.writeInt(r12.b(ChronoField.YEAR));
                    objectOutput.writeByte(r12.b(ChronoField.MONTH_OF_YEAR));
                    objectOutput.writeByte(r12.b(ChronoField.DAY_OF_MONTH));
                    return;
                }
                throw null;
            case 4:
                objectOutput.writeByte(((HijrahEra) obj).ordinal());
                return;
            case 5:
                ? r13 = (MinguoDate) obj;
                if (r13 != 0) {
                    objectOutput.writeInt(r13.b(ChronoField.YEAR));
                    objectOutput.writeByte(r13.b(ChronoField.MONTH_OF_YEAR));
                    objectOutput.writeByte(r13.b(ChronoField.DAY_OF_MONTH));
                    return;
                }
                throw null;
            case 6:
                objectOutput.writeByte(((MinguoEra) obj).ordinal());
                return;
            case 7:
                ? r14 = (ThaiBuddhistDate) obj;
                if (r14 != 0) {
                    objectOutput.writeInt(r14.b(ChronoField.YEAR));
                    objectOutput.writeByte(r14.b(ChronoField.MONTH_OF_YEAR));
                    objectOutput.writeByte(r14.b(ChronoField.DAY_OF_MONTH));
                    return;
                }
                throw null;
            case 8:
                objectOutput.writeByte(((ThaiBuddhistEra) obj).ordinal());
                return;
            case 9:
            case 10:
            default:
                throw new InvalidClassException("Unknown serialized type");
            case 11:
                objectOutput.writeUTF(((Chronology) obj).g());
                return;
            case 12:
                ChronoLocalDateTimeImpl chronoLocalDateTimeImpl = (ChronoLocalDateTimeImpl) obj;
                objectOutput.writeObject(chronoLocalDateTimeImpl.b);
                objectOutput.writeObject(chronoLocalDateTimeImpl.c);
                return;
            case 13:
                ChronoZonedDateTimeImpl chronoZonedDateTimeImpl = (ChronoZonedDateTimeImpl) obj;
                objectOutput.writeObject(chronoZonedDateTimeImpl.b);
                objectOutput.writeObject(chronoZonedDateTimeImpl.c);
                objectOutput.writeObject(chronoZonedDateTimeImpl.d);
                return;
        }
    }

    public Ser(byte b2, Object obj) {
        this.b = b2;
        this.c = obj;
    }
}
