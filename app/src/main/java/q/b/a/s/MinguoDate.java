package q.b.a.s;

import j.a.a.a.outline;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.LocalDate;
import q.b.a.f;
import q.b.a.v.ChronoField;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAmount;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.m;

public final class MinguoDate extends ChronoDateImpl<s> implements Serializable {
    public final LocalDate b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDate, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public MinguoDate(LocalDate localDate) {
        Collections.a((Object) localDate, "date");
        this.b = localDate;
    }

    private Object writeReplace() {
        return new Ser((byte) 5, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.MinguoDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.MinguoDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    public ChronoDateImpl b(long j2, TemporalUnit temporalUnit) {
        return (MinguoDate) super.b(j2, (m) temporalUnit);
    }

    public ChronoDateImpl c(long j2) {
        return a(this.b.d(j2));
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.MinguoDate] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int i2 = 1;
        switch (((ChronoField) temporalField).ordinal()) {
            case 24:
                return o();
            case 25:
                int p2 = p();
                if (p2 < 1) {
                    p2 = 1 - p2;
                }
                return (long) p2;
            case 26:
                return (long) p();
            case 27:
                if (p() < 1) {
                    i2 = 0;
                }
                return (long) i2;
            default:
                return this.b.d(temporalField);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MinguoDate) {
            return this.b.equals(((MinguoDate) obj).b);
        }
        return false;
    }

    public int hashCode() {
        MinguoChronology minguoChronology = MinguoChronology.d;
        return -1990173233 ^ this.b.hashCode();
    }

    public Chronology i() {
        return MinguoChronology.d;
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.s.Era, q.b.a.s.MinguoEra] */
    public Era j() {
        return (MinguoEra) MinguoDate.super.j();
    }

    public long l() {
        return this.b.l();
    }

    public final long o() {
        return ((((long) p()) * 12) + ((long) this.b.c)) - 1;
    }

    public final int p() {
        return this.b.b - 1911;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.MinguoDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.MinguoDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    /* renamed from: b  reason: collision with other method in class */
    public ChronoLocalDate m26b(long j2, TemporalUnit temporalUnit) {
        return (MinguoDate) super.b(j2, (m) temporalUnit);
    }

    public final ChronoLocalDateTime<s> a(f fVar) {
        return new ChronoLocalDateTimeImpl(this, fVar);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.Temporal, q.b.a.s.MinguoDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.MinguoDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.MinguoDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    /* renamed from: b  reason: collision with other method in class */
    public Temporal m27b(long j2, TemporalUnit temporalUnit) {
        return (MinguoDate) super.b(j2, (m) temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    public ChronoLocalDate a(TemporalAdjuster temporalAdjuster) {
        return (MinguoDate) MinguoChronology.d.a((d) temporalAdjuster.a(this));
    }

    public ChronoDateImpl b(long j2) {
        return a(this.b.b(j2));
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.v.Temporal, q.b.a.s.MinguoDate] */
    /* renamed from: a  reason: collision with other method in class */
    public Temporal m25a(TemporalAdjuster temporalAdjuster) {
        return (MinguoDate) MinguoChronology.d.a((d) temporalAdjuster.a(this));
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    public ValueRange a(TemporalField temporalField) {
        long j2;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (c(temporalField)) {
            ChronoField chronoField = (ChronoField) temporalField;
            int ordinal = chronoField.ordinal();
            if (ordinal == 18 || ordinal == 19 || ordinal == 21) {
                return this.b.a(temporalField);
            }
            if (ordinal != 25) {
                return MinguoChronology.d.a(chronoField);
            }
            ValueRange valueRange = ChronoField.YEAR.range;
            if (p() <= 0) {
                j2 = (-valueRange.b) + 1 + 1911;
            } else {
                j2 = valueRange.f3152e - 1911;
            }
            return ValueRange.a(1, j2);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalDate.a(int, int):q.b.a.LocalDate
      q.b.a.LocalDate.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate */
    public MinguoDate a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (MinguoDate) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        if (d(r0) == j2) {
            return this;
        }
        switch (r0.ordinal()) {
            case 24:
                MinguoChronology.d.a((ChronoField) r0).b(j2, r0);
                return a(this.b.b(j2 - o()));
            case 25:
            case 26:
            case 27:
                int a = MinguoChronology.d.a((ChronoField) r0).a(j2, (TemporalField) r0);
                switch (r0.ordinal()) {
                    case 25:
                        return a(this.b.a(p() >= 1 ? a + 1911 : (1 - a) + 1911));
                    case 26:
                        return a(this.b.a(a + 1911));
                    case 27:
                        return a(this.b.a((1 - p()) + 1911));
                }
        }
        return a(this.b.a(temporalField, j2));
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.MinguoDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.MinguoDate.a(q.b.a.v.TemporalField, long):q.b.a.s.MinguoDate
      q.b.a.s.MinguoDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.MinguoDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
    public ChronoLocalDate a(long j2, TemporalUnit temporalUnit) {
        return (MinguoDate) MinguoDate.super.a(j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.Temporal, q.b.a.s.MinguoDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.MinguoDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.MinguoDate.a(q.b.a.v.TemporalField, long):q.b.a.s.MinguoDate
      q.b.a.s.MinguoDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.MinguoDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
    /* renamed from: a  reason: collision with other method in class */
    public Temporal m24a(long j2, TemporalUnit temporalUnit) {
        return (MinguoDate) MinguoDate.super.a(j2, temporalUnit);
    }

    public ChronoDateImpl a(long j2) {
        return a(this.b.a(j2));
    }

    public final MinguoDate a(LocalDate localDate) {
        return localDate.equals(this.b) ? this : new MinguoDate(localDate);
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    public ChronoLocalDate a(TemporalAmount temporalAmount) {
        return (MinguoDate) i().a((d) temporalAmount.a(this));
    }
}
