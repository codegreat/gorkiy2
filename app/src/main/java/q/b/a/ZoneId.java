package q.b.a;

import j.a.a.a.outline;
import java.io.DataOutput;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.threeten.bp.DateTimeException;
import q.b.a.w.ZoneRules;
import q.b.a.w.ZoneRulesProvider;

public abstract class ZoneId implements Serializable {
    public static final Map<String, String> b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("ACT", "Australia/Darwin");
        hashMap.put("AET", "Australia/Sydney");
        hashMap.put("AGT", "America/Argentina/Buenos_Aires");
        hashMap.put("ART", "Africa/Cairo");
        hashMap.put("AST", "America/Anchorage");
        hashMap.put("BET", "America/Sao_Paulo");
        hashMap.put("BST", "Asia/Dhaka");
        hashMap.put("CAT", "Africa/Harare");
        hashMap.put("CNT", "America/St_Johns");
        hashMap.put("CST", "America/Chicago");
        hashMap.put("CTT", "Asia/Shanghai");
        hashMap.put("EAT", "Africa/Addis_Ababa");
        hashMap.put("ECT", "Europe/Paris");
        hashMap.put("IET", "America/Indiana/Indianapolis");
        hashMap.put("IST", "Asia/Kolkata");
        hashMap.put("JST", "Asia/Tokyo");
        hashMap.put("MIT", "Pacific/Apia");
        hashMap.put("NET", "Asia/Yerevan");
        hashMap.put("NST", "Pacific/Auckland");
        hashMap.put("PLT", "Asia/Karachi");
        hashMap.put("PNT", "America/Phoenix");
        hashMap.put("PRT", "America/Puerto_Rico");
        hashMap.put("PST", "America/Los_Angeles");
        hashMap.put("SST", "Pacific/Guadalcanal");
        hashMap.put("VST", "Asia/Ho_Chi_Minh");
        hashMap.put("EST", "-05:00");
        hashMap.put("MST", "-07:00");
        hashMap.put("HST", "-10:00");
        b = Collections.unmodifiableMap(hashMap);
    }

    public ZoneId() {
        if (getClass() != ZoneOffset.class && getClass() != ZoneRegion.class) {
            throw new AssertionError("Invalid subclass");
        }
    }

    /* JADX WARN: Type inference failed for: r5v2, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    /* JADX WARN: Type inference failed for: r5v12, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.ZoneRegion.a(java.lang.String, boolean):q.b.a.ZoneRegion
     arg types: [java.lang.String, int]
     candidates:
      q.b.a.ZoneId.a(java.lang.String, q.b.a.ZoneOffset):q.b.a.ZoneId
      q.b.a.ZoneRegion.a(java.lang.String, boolean):q.b.a.ZoneRegion */
    public static ZoneId a(String str) {
        n.i.Collections.a((Object) str, "zoneId");
        if (str.equals("Z")) {
            return ZoneOffset.g;
        }
        if (str.length() == 1) {
            throw new DateTimeException(outline.a("Invalid zone: ", str));
        } else if (str.startsWith("+") || str.startsWith("-")) {
            return ZoneOffset.a(str);
        } else {
            if (str.equals("UTC") || str.equals("GMT") || str.equals("UT")) {
                return new ZoneRegion(str, ZoneOffset.g.g());
            }
            if (str.startsWith("UTC+") || str.startsWith("GMT+") || str.startsWith("UTC-") || str.startsWith("GMT-")) {
                ZoneOffset a = ZoneOffset.a(str.substring(3));
                if (a.c == 0) {
                    return new ZoneRegion(str.substring(0, 3), a.g());
                }
                return new ZoneRegion(str.substring(0, 3) + a.d, a.g());
            } else if (!str.startsWith("UT+") && !str.startsWith("UT-")) {
                return ZoneRegion.a(str, true);
            } else {
                ZoneOffset a2 = ZoneOffset.a(str.substring(2));
                if (a2.c == 0) {
                    return new ZoneRegion("UT", a2.g());
                }
                StringBuilder a3 = outline.a("UT");
                a3.append(a2.d);
                return new ZoneRegion(a3.toString(), a2.g());
            }
        }
    }

    public static Set<String> h() {
        return new HashSet(Collections.unmodifiableSet(ZoneRulesProvider.b.keySet()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static ZoneId i() {
        String id = TimeZone.getDefault().getID();
        Map<String, String> map = b;
        n.i.Collections.a((Object) id, "zoneId");
        n.i.Collections.a((Object) map, "aliasMap");
        String str = map.get(id);
        if (str != null) {
            id = str;
        }
        return a(id);
    }

    public abstract void a(DataOutput dataOutput);

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ZoneId) {
            return f().equals(((ZoneId) obj).f());
        }
        return false;
    }

    public abstract String f();

    public abstract ZoneRules g();

    public int hashCode() {
        return f().hashCode();
    }

    public String toString() {
        return f();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.ZoneId, java.lang.Object, q.b.a.ZoneOffset] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static q.b.a.ZoneId a(java.lang.String r2, q.b.a.ZoneOffset r3) {
        /*
            java.lang.String r0 = "prefix"
            n.i.Collections.a(r2, r0)
            java.lang.String r0 = "offset"
            n.i.Collections.a(r3, r0)
            int r0 = r2.length()
            if (r0 != 0) goto L_0x0011
            return r3
        L_0x0011:
            java.lang.String r0 = "GMT"
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "UTC"
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "UT"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x002a
            goto L_0x0036
        L_0x002a:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Invalid prefix, must be GMT, UTC or UT: "
            java.lang.String r2 = j.a.a.a.outline.a(r0, r2)
            r3.<init>(r2)
            throw r3
        L_0x0036:
            int r0 = r3.c
            if (r0 != 0) goto L_0x0044
            q.b.a.ZoneRegion r0 = new q.b.a.ZoneRegion
            q.b.a.w.ZoneRules r3 = r3.g()
            r0.<init>(r2, r3)
            return r0
        L_0x0044:
            q.b.a.ZoneRegion r0 = new q.b.a.ZoneRegion
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.String r1 = r3.d
            r2.append(r1)
            java.lang.String r2 = r2.toString()
            q.b.a.w.ZoneRules r3 = r3.g()
            r0.<init>(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.ZoneId.a(java.lang.String, q.b.a.ZoneOffset):q.b.a.ZoneId");
    }
}
