package n.o;

import n.n.c.Intrinsics;

public final class d extends Ranges {
    public static final Progressions a(int i2, int i3) {
        return new Progressions(i2, i3, -1);
    }

    public static final Ranges0 b(int i2, int i3) {
        if (i3 > Integer.MIN_VALUE) {
            return new Ranges0(i2, i3 - 1);
        }
        Ranges0 ranges0 = Ranges0.f2791f;
        return Ranges0.f2790e;
    }

    public static final Progressions a(Progressions progressions, int i2) {
        if (progressions != null) {
            boolean z = i2 > 0;
            Integer valueOf = Integer.valueOf(i2);
            if (valueOf == null) {
                Intrinsics.a("step");
                throw null;
            } else if (z) {
                int i3 = progressions.b;
                int i4 = progressions.c;
                if (progressions.d <= 0) {
                    i2 = -i2;
                }
                return new Progressions(i3, i4, i2);
            } else {
                throw new IllegalArgumentException("Step must be positive, was: " + valueOf + '.');
            }
        } else {
            Intrinsics.a("$this$step");
            throw null;
        }
    }
}
