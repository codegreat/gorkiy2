package n.j;

import java.util.Comparator;
import n.n.c.Intrinsics;

/* compiled from: Comparisons.kt */
public final class Comparisons0 implements Comparator<Comparable<? super Object>> {
    public static final Comparisons0 b = new Comparisons0();

    public int compare(Object obj, Object obj2) {
        Comparable comparable = (Comparable) obj;
        Comparable comparable2 = (Comparable) obj2;
        if (comparable == null) {
            Intrinsics.a("a");
            throw null;
        } else if (comparable2 != null) {
            return comparable2.compareTo(comparable);
        } else {
            Intrinsics.a("b");
            throw null;
        }
    }

    public final Comparator<Comparable<Object>> reversed() {
        return Comparisons.b;
    }
}
