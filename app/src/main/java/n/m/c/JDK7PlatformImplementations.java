package n.m.c;

import n.m.PlatformImplementations;
import n.n.c.Intrinsics;

/* compiled from: JDK7PlatformImplementations.kt */
public class JDK7PlatformImplementations extends PlatformImplementations {
    public void a(Throwable th, Throwable th2) {
        if (th == null) {
            Intrinsics.a("cause");
            throw null;
        } else if (th2 != null) {
            th.addSuppressed(th2);
        } else {
            Intrinsics.a("exception");
            throw null;
        }
    }
}
