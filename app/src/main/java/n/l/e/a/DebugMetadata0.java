package n.l.e.a;

import java.lang.reflect.Method;

/* compiled from: DebugMetadata.kt */
public final class DebugMetadata0 {
    public static final a a = new a(null, null, null);
    public static a b;
    public static final DebugMetadata0 c = new DebugMetadata0();

    /* compiled from: DebugMetadata.kt */
    public static final class a {
        public final Method a;
        public final Method b;
        public final Method c;

        public a(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }
    }
}
