package n.p;

import n.n.b.Functions0;
import n.p.KProperty;

/* compiled from: KProperty.kt */
public interface KProperty1<T, R> extends KProperty<R>, Functions0<T, R> {

    /* compiled from: KProperty.kt */
    public interface a<T, R> extends KProperty.a<R>, Functions0<T, R> {
    }

    a<T, R> a();

    R get(T t2);
}
