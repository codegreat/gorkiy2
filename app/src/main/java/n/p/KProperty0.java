package n.p;

import n.n.b.Functions;
import n.p.KProperty;

/* compiled from: KProperty.kt */
public interface KProperty0<R> extends KProperty<R>, Functions<R> {

    /* compiled from: KProperty.kt */
    public interface a<R> extends KProperty.a<R>, Functions<R> {
    }

    a<R> a();

    R get();
}
