package n.q;

import j.a.a.a.outline;
import java.util.Iterator;
import n.n.c.Intrinsics;

/* compiled from: Sequences.kt */
public final class Sequences<T> implements Sequence<T>, Sequences0<T> {
    public final Sequence<T> a;
    public final int b;

    /* compiled from: Sequences.kt */
    public static final class a implements Iterator<T>, n.n.c.t.a {
        public final Iterator<T> b;
        public int c;

        public a(Sequences sequences) {
            this.b = sequences.a.iterator();
            this.c = sequences.b;
        }

        public final void b() {
            while (this.c > 0 && this.b.hasNext()) {
                this.b.next();
                this.c--;
            }
        }

        public boolean hasNext() {
            b();
            return this.b.hasNext();
        }

        public T next() {
            b();
            return this.b.next();
        }

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    public Sequences(Sequence<? extends T> sequence, int i2) {
        if (sequence != null) {
            this.a = sequence;
            this.b = i2;
            if (!(i2 >= 0)) {
                StringBuilder a2 = outline.a("count must be non-negative, but was ");
                a2.append(this.b);
                a2.append('.');
                throw new IllegalArgumentException(a2.toString().toString());
            }
            return;
        }
        Intrinsics.a("sequence");
        throw null;
    }

    public Sequence<T> a(int i2) {
        int i3 = this.b + i2;
        return i3 < 0 ? new Sequences(this, i2) : new Sequences(this.a, i3);
    }

    public Iterator<T> iterator() {
        return new a(this);
    }
}
