package n.n.c;

import j.a.a.a.outline;
import n.Function;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.b.Functions1;

public class TypeIntrinsics {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
     arg types: [java.lang.ClassCastException, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T */
    public static ClassCastException a(ClassCastException classCastException) {
        Intrinsics.a((Throwable) classCastException, TypeIntrinsics.class.getName());
        throw classCastException;
    }

    public static void a(Object obj, String str) {
        a(new ClassCastException(outline.a(obj == null ? "null" : obj.getClass().getName(), " cannot be cast to ", str)));
        throw null;
    }

    public static Object a(Object obj, int i2) {
        int i3;
        if (obj != null) {
            boolean z = false;
            if (obj instanceof Function) {
                if (obj instanceof FunctionBase) {
                    i3 = ((FunctionBase) obj).c();
                } else if (obj instanceof Functions) {
                    i3 = 0;
                } else if (obj instanceof Functions0) {
                    i3 = 1;
                } else {
                    i3 = obj instanceof Functions1 ? 2 : -1;
                }
                if (i3 == i2) {
                    z = true;
                }
            }
            if (!z) {
                a(obj, "kotlin.jvm.functions.Function" + i2);
                throw null;
            }
        }
        return obj;
    }
}
