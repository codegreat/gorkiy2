package n.n.c;

import n.p.KCallable;
import n.p.KProperty0;

public abstract class PropertyReference0 extends PropertyReference implements KProperty0 {
    public KProperty0.a a() {
        return ((KProperty0) h()).a();
    }

    public Object b() {
        return get();
    }

    public KCallable e() {
        if (Reflection.a != null) {
            return this;
        }
        throw null;
    }
}
