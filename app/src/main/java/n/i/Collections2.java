package n.i;

import j.a.a.a.outline;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import n.n.c.CollectionToArray;
import n.n.c.Intrinsics;
import n.n.c.t.KMarkers;

/* compiled from: Collections.kt */
public final class Collections2 implements List, Serializable, RandomAccess, KMarkers {
    public static final Collections2 b = new Collections2();

    public /* synthetic */ void add(int i2, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(int i2, Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean contains(Object obj) {
        if (!(obj instanceof Void) || ((Void) obj) != null) {
            return false;
        }
        Intrinsics.a("element");
        throw null;
    }

    public boolean containsAll(Collection collection) {
        if (collection != null) {
            return collection.isEmpty();
        }
        Intrinsics.a("elements");
        throw null;
    }

    public boolean equals(Object obj) {
        return (obj instanceof List) && ((List) obj).isEmpty();
    }

    public Object get(int i2) {
        throw new IndexOutOfBoundsException("Empty list doesn't contain element at index " + i2 + '.');
    }

    public int hashCode() {
        return 1;
    }

    public final int indexOf(Object obj) {
        if (!(obj instanceof Void) || ((Void) obj) != null) {
            return -1;
        }
        Intrinsics.a("element");
        throw null;
    }

    public boolean isEmpty() {
        return true;
    }

    public Iterator iterator() {
        return Collections1.b;
    }

    public final int lastIndexOf(Object obj) {
        if (!(obj instanceof Void) || ((Void) obj) != null) {
            return -1;
        }
        Intrinsics.a("element");
        throw null;
    }

    public ListIterator listIterator() {
        return Collections1.b;
    }

    public /* synthetic */ Object remove(int i2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ Object set(int i2, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final int size() {
        return 0;
    }

    public List subList(int i2, int i3) {
        if (i2 == 0 && i3 == 0) {
            return this;
        }
        throw new IndexOutOfBoundsException(outline.a("fromIndex: ", i2, ", toIndex: ", i3));
    }

    public Object[] toArray() {
        return CollectionToArray.a(this);
    }

    public <T> T[] toArray(T[] tArr) {
        return CollectionToArray.a(this, tArr);
    }

    public String toString() {
        return "[]";
    }

    public ListIterator listIterator(int i2) {
        if (i2 == 0) {
            return Collections1.b;
        }
        throw new IndexOutOfBoundsException(outline.b("Index: ", i2));
    }
}
