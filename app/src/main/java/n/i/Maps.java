package n.i;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import n.n.c.Intrinsics;
import n.n.c.t.KMarkers;

/* compiled from: Maps.kt */
public final class Maps implements Map, Serializable, KMarkers {
    public static final Maps b = new Maps();

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean containsKey(Object obj) {
        return false;
    }

    public final boolean containsValue(Object obj) {
        if (!(obj instanceof Void) || ((Void) obj) != null) {
            return false;
        }
        Intrinsics.a("value");
        throw null;
    }

    public final Set<Map.Entry> entrySet() {
        return Sets.b;
    }

    public boolean equals(Object obj) {
        return (obj instanceof Map) && ((Map) obj).isEmpty();
    }

    public final Object get(Object obj) {
        return null;
    }

    public int hashCode() {
        return 0;
    }

    public boolean isEmpty() {
        return true;
    }

    public final Set<Object> keySet() {
        return Sets.b;
    }

    public /* synthetic */ Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void putAll(Map map) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public Object remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final int size() {
        return 0;
    }

    public String toString() {
        return "{}";
    }

    public final Collection values() {
        return Collections2.b;
    }
}
