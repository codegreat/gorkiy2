package n.r;

import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: Regex.kt */
public final /* synthetic */ class Regex2 extends h implements Functions0<c, c> {

    /* renamed from: f  reason: collision with root package name */
    public static final Regex2 f2793f = new Regex2();

    public Regex2() {
        super(1);
    }

    public Object a(Object obj) {
        MatchResult matchResult = (MatchResult) obj;
        if (matchResult != null) {
            return matchResult.next();
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "next";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(MatchResult.class);
    }

    public final String i() {
        return "next()Lkotlin/text/MatchResult;";
    }
}
