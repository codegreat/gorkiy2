package n.r;

import java.nio.charset.Charset;
import n.n.c.Intrinsics;

/* compiled from: Charsets.kt */
public final class Charsets {
    public static final Charset a;
    public static Charset b;
    public static Charset c;
    public static final Charsets d = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.charset.Charset, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    static {
        Charset forName = Charset.forName("UTF-8");
        Intrinsics.a((Object) forName, "Charset.forName(\"UTF-8\")");
        a = forName;
        Intrinsics.a((Object) Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        Intrinsics.a((Object) Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        Intrinsics.a((Object) Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        Intrinsics.a((Object) Charset.forName("US-ASCII"), "Charset.forName(\"US-ASCII\")");
        Intrinsics.a((Object) Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }
}
