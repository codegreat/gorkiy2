package p;

import j.c.a.a.c.n.c;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import kotlin.TypeCastException;
import n.i.Collections0;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import p.b0.ByteString;

/* compiled from: Options.kt */
public final class Options extends AbstractList<j> implements RandomAccess {
    public static final a d = new a(null);
    public final ByteString[] b;
    public final int[] c;

    public /* synthetic */ Options(ByteString[] byteStringArr, int[] iArr, DefaultConstructorMarker defaultConstructorMarker) {
        this.b = byteStringArr;
        this.c = iArr;
    }

    public final boolean contains(Object obj) {
        if (obj != null ? obj instanceof ByteString : true) {
            return super.contains((ByteString) obj);
        }
        return false;
    }

    public Object get(int i2) {
        return this.b[i2];
    }

    public final int indexOf(Object obj) {
        if (obj != null ? obj instanceof ByteString : true) {
            return super.indexOf((ByteString) obj);
        }
        return -1;
    }

    public final int lastIndexOf(Object obj) {
        if (obj != null ? obj instanceof ByteString : true) {
            return super.lastIndexOf((ByteString) obj);
        }
        return -1;
    }

    public final boolean remove(Object obj) {
        if (obj != null ? obj instanceof ByteString : true) {
            return super.remove((ByteString) obj);
        }
        return false;
    }

    public final int size() {
        return this.b.length;
    }

    /* compiled from: Options.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
         arg types: [java.lang.Comparable, p.ByteString]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(java.lang.Object, java.lang.String):T
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int */
        public final Options a(ByteString... byteStringArr) {
            int i2;
            ByteString[] byteStringArr2 = byteStringArr;
            if (byteStringArr2 != null) {
                int i3 = 0;
                if (byteStringArr2.length == 0) {
                    return new Options(new ByteString[0], new int[]{0, -1}, null);
                }
                List a = c.a((Object[]) byteStringArr);
                ArrayList arrayList = (ArrayList) a;
                if (arrayList.size() > 1) {
                    Collections.sort(a);
                }
                ArrayList arrayList2 = new ArrayList(byteStringArr2.length);
                for (ByteString byteString : byteStringArr2) {
                    arrayList2.add(-1);
                }
                Object[] array = arrayList2.toArray(new Integer[0]);
                if (array != null) {
                    Integer[] numArr = (Integer[]) array;
                    Integer[] numArr2 = (Integer[]) Arrays.copyOf(numArr, numArr.length);
                    if (numArr2 != null) {
                        ArrayList arrayList3 = numArr2.length == 0 ? new ArrayList() : new ArrayList(new Collections0(numArr2, true));
                        int length = byteStringArr2.length;
                        int i4 = 0;
                        int i5 = 0;
                        while (i4 < length) {
                            ByteString byteString2 = byteStringArr2[i4];
                            int i6 = i5 + 1;
                            int size = arrayList.size();
                            int size2 = arrayList.size();
                            if (size < 0) {
                                throw new IllegalArgumentException("fromIndex (" + 0 + ") is greater than toIndex (" + size + ").");
                            } else if (size <= size2) {
                                int i7 = size - 1;
                                int i8 = 0;
                                while (true) {
                                    if (i8 > i7) {
                                        i2 = -(i8 + 1);
                                        break;
                                    }
                                    i2 = (i8 + i7) >>> 1;
                                    int a2 = n.i.Collections.a((Comparable) arrayList.get(i2), (Comparable) byteString2);
                                    if (a2 >= 0) {
                                        if (a2 <= 0) {
                                            break;
                                        }
                                        i7 = i2 - 1;
                                    } else {
                                        i8 = i2 + 1;
                                    }
                                }
                                arrayList3.set(i2, Integer.valueOf(i5));
                                i4++;
                                i5 = i6;
                            } else {
                                throw new IndexOutOfBoundsException("toIndex (" + size + ") is greater than size (" + size2 + ").");
                            }
                        }
                        if (((ByteString) arrayList.get(0)).g() > 0) {
                            int i9 = 0;
                            while (i9 < arrayList.size()) {
                                ByteString byteString3 = (ByteString) arrayList.get(i9);
                                int i10 = i9 + 1;
                                int i11 = i10;
                                while (i11 < arrayList.size()) {
                                    ByteString byteString4 = (ByteString) arrayList.get(i11);
                                    if (byteString4 == null) {
                                        throw null;
                                    } else if (byteString3 == null) {
                                        Intrinsics.a("prefix");
                                        throw null;
                                    } else if (!ByteString.b(byteString4, byteString3)) {
                                        continue;
                                        break;
                                    } else {
                                        if (!(byteString4.g() != byteString3.g())) {
                                            throw new IllegalArgumentException(("duplicate option: " + byteString4).toString());
                                        } else if (((Number) arrayList3.get(i11)).intValue() > ((Number) arrayList3.get(i9)).intValue()) {
                                            arrayList.remove(i11);
                                            arrayList3.remove(i11);
                                        } else {
                                            i11++;
                                        }
                                    }
                                }
                                i9 = i10;
                            }
                            Buffer buffer = new Buffer();
                            a(0, buffer, 0, a, 0, arrayList.size(), arrayList3);
                            int[] iArr = new int[((int) a(buffer))];
                            while (!buffer.j()) {
                                iArr[i3] = buffer.readInt();
                                i3++;
                            }
                            return new Options((ByteString[]) byteStringArr.clone(), iArr, null);
                        }
                        throw new IllegalArgumentException("the empty byte string is not a supported option".toString());
                    }
                    Intrinsics.a("elements");
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            Intrinsics.a("byteStrings");
            throw null;
        }

        public final long a(Buffer buffer) {
            return buffer.c / ((long) 4);
        }

        public final void a(long j2, f fVar, int i2, List<? extends j> list, int i3, int i4, List<Integer> list2) {
            int i5;
            int i6;
            int i7;
            int i8;
            Buffer buffer;
            f fVar2 = fVar;
            int i9 = i2;
            List<? extends j> list3 = list;
            int i10 = i3;
            int i11 = i4;
            List<Integer> list4 = list2;
            if (i10 < i11) {
                int i12 = i10;
                while (i12 < i11) {
                    if (((ByteString) list3.get(i12)).g() >= i9) {
                        i12++;
                    } else {
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                }
                ByteString byteString = (ByteString) list.get(i3);
                ByteString byteString2 = (ByteString) list3.get(i11 - 1);
                if (i9 == byteString.g()) {
                    int intValue = list4.get(i10).intValue();
                    int i13 = i10 + 1;
                    i5 = i13;
                    i6 = intValue;
                    byteString = (ByteString) list3.get(i13);
                } else {
                    i5 = i10;
                    i6 = -1;
                }
                if (byteString.a(i9) != byteString2.a(i9)) {
                    int i14 = 1;
                    for (int i15 = i5 + 1; i15 < i11; i15++) {
                        if (((ByteString) list3.get(i15 - 1)).a(i9) != ((ByteString) list3.get(i15)).a(i9)) {
                            i14++;
                        }
                    }
                    long a = a((Buffer) fVar2) + j2 + ((long) 2) + ((long) (i14 * 2));
                    fVar2.writeInt(i14);
                    fVar2.writeInt(i6);
                    for (int i16 = i5; i16 < i11; i16++) {
                        byte a2 = ((ByteString) list3.get(i16)).a(i9);
                        if (i16 == i5 || a2 != ((ByteString) list3.get(i16 - 1)).a(i9)) {
                            fVar2.writeInt(a2 & 255);
                        }
                    }
                    Buffer buffer2 = new Buffer();
                    while (i5 < i11) {
                        byte a3 = ((ByteString) list3.get(i5)).a(i9);
                        int i17 = i5 + 1;
                        int i18 = i17;
                        while (true) {
                            if (i18 >= i11) {
                                i7 = i11;
                                break;
                            } else if (a3 != ((ByteString) list3.get(i18)).a(i9)) {
                                i7 = i18;
                                break;
                            } else {
                                i18++;
                            }
                        }
                        if (i17 == i7 && i9 + 1 == ((ByteString) list3.get(i5)).g()) {
                            fVar2.writeInt(list4.get(i5).intValue());
                            i8 = i7;
                            buffer = buffer2;
                        } else {
                            fVar2.writeInt(((int) (a(buffer2) + a)) * -1);
                            i8 = i7;
                            buffer = buffer2;
                            a(a, buffer2, i9 + 1, list, i5, i7, list2);
                        }
                        buffer2 = buffer;
                        i5 = i8;
                    }
                    fVar2.a(buffer2);
                    return;
                }
                int min = Math.min(byteString.g(), byteString2.g());
                int i19 = i9;
                int i20 = 0;
                while (i19 < min && byteString.a(i19) == byteString2.a(i19)) {
                    i20++;
                    i19++;
                }
                long a4 = a((Buffer) fVar2) + j2 + ((long) 2) + ((long) i20) + 1;
                fVar2.writeInt(-i20);
                fVar2.writeInt(i6);
                int i21 = i9 + i20;
                while (i9 < i21) {
                    fVar2.writeInt(byteString.a(i9) & 255);
                    i9++;
                }
                if (i5 + 1 == i11) {
                    if (i21 == ((ByteString) list3.get(i5)).g()) {
                        fVar2.writeInt(list4.get(i5).intValue());
                        return;
                    }
                    throw new IllegalStateException("Check failed.".toString());
                }
                Buffer buffer3 = new Buffer();
                fVar2.writeInt(((int) (a(buffer3) + a4)) * -1);
                a(a4, buffer3, i21, list, i5, i4, list2);
                fVar2.a(buffer3);
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }
}
