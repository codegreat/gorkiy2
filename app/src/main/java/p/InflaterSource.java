package p;

import java.util.zip.Inflater;
import n.n.c.Intrinsics;

/* compiled from: InflaterSource.kt */
public final class InflaterSource implements Source {
    public int b;
    public boolean c;
    public final BufferedSource d;

    /* renamed from: e  reason: collision with root package name */
    public final Inflater f3067e;

    public InflaterSource(BufferedSource bufferedSource, Inflater inflater) {
        if (bufferedSource == null) {
            Intrinsics.a("source");
            throw null;
        } else if (inflater != null) {
            this.d = bufferedSource;
            this.f3067e = inflater;
        } else {
            Intrinsics.a("inflater");
            throw null;
        }
    }

    public final void a() {
        int i2 = this.b;
        if (i2 != 0) {
            int remaining = i2 - this.f3067e.getRemaining();
            this.b -= remaining;
            this.d.skip((long) remaining);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x007b A[Catch:{ DataFormatException -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x006f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long b(p.Buffer r10, long r11) {
        /*
            r9 = this;
            r0 = 0
            if (r10 == 0) goto L_0x00ea
            r1 = 0
            r3 = 0
            r4 = 1
            int r5 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r5 < 0) goto L_0x000d
            r6 = 1
            goto L_0x000e
        L_0x000d:
            r6 = 0
        L_0x000e:
            if (r6 == 0) goto L_0x00cf
            boolean r6 = r9.c
            r6 = r6 ^ r4
            if (r6 == 0) goto L_0x00c3
            if (r5 != 0) goto L_0x0018
            return r1
        L_0x0018:
            java.util.zip.Inflater r1 = r9.f3067e
            boolean r1 = r1.needsInput()
            if (r1 != 0) goto L_0x0022
        L_0x0020:
            r1 = 0
            goto L_0x0055
        L_0x0022:
            r9.a()
            java.util.zip.Inflater r1 = r9.f3067e
            int r1 = r1.getRemaining()
            if (r1 != 0) goto L_0x002f
            r1 = 1
            goto L_0x0030
        L_0x002f:
            r1 = 0
        L_0x0030:
            if (r1 == 0) goto L_0x00b7
            p.BufferedSource r1 = r9.d
            boolean r1 = r1.j()
            if (r1 == 0) goto L_0x003c
            r1 = 1
            goto L_0x0055
        L_0x003c:
            p.BufferedSource r1 = r9.d
            p.Buffer r1 = r1.getBuffer()
            p.Segment r1 = r1.b
            if (r1 == 0) goto L_0x00b3
            int r2 = r1.c
            int r5 = r1.b
            int r2 = r2 - r5
            r9.b = r2
            java.util.zip.Inflater r6 = r9.f3067e
            byte[] r1 = r1.a
            r6.setInput(r1, r5, r2)
            goto L_0x0020
        L_0x0055:
            p.Segment r2 = r10.a(r4)     // Catch:{ DataFormatException -> 0x00ac }
            int r5 = r2.c     // Catch:{ DataFormatException -> 0x00ac }
            int r5 = 8192 - r5
            long r5 = (long) r5     // Catch:{ DataFormatException -> 0x00ac }
            long r5 = java.lang.Math.min(r11, r5)     // Catch:{ DataFormatException -> 0x00ac }
            int r6 = (int) r5     // Catch:{ DataFormatException -> 0x00ac }
            java.util.zip.Inflater r5 = r9.f3067e     // Catch:{ DataFormatException -> 0x00ac }
            byte[] r7 = r2.a     // Catch:{ DataFormatException -> 0x00ac }
            int r8 = r2.c     // Catch:{ DataFormatException -> 0x00ac }
            int r5 = r5.inflate(r7, r8, r6)     // Catch:{ DataFormatException -> 0x00ac }
            if (r5 <= 0) goto L_0x007b
            int r11 = r2.c     // Catch:{ DataFormatException -> 0x00ac }
            int r11 = r11 + r5
            r2.c = r11     // Catch:{ DataFormatException -> 0x00ac }
            long r11 = r10.c     // Catch:{ DataFormatException -> 0x00ac }
            long r0 = (long) r5     // Catch:{ DataFormatException -> 0x00ac }
            long r11 = r11 + r0
            r10.c = r11     // Catch:{ DataFormatException -> 0x00ac }
            return r0
        L_0x007b:
            java.util.zip.Inflater r5 = r9.f3067e     // Catch:{ DataFormatException -> 0x00ac }
            boolean r5 = r5.finished()     // Catch:{ DataFormatException -> 0x00ac }
            if (r5 != 0) goto L_0x0097
            java.util.zip.Inflater r5 = r9.f3067e     // Catch:{ DataFormatException -> 0x00ac }
            boolean r5 = r5.needsDictionary()     // Catch:{ DataFormatException -> 0x00ac }
            if (r5 == 0) goto L_0x008c
            goto L_0x0097
        L_0x008c:
            if (r1 != 0) goto L_0x008f
            goto L_0x0018
        L_0x008f:
            java.io.EOFException r10 = new java.io.EOFException     // Catch:{ DataFormatException -> 0x00ac }
            java.lang.String r11 = "source exhausted prematurely"
            r10.<init>(r11)     // Catch:{ DataFormatException -> 0x00ac }
            throw r10     // Catch:{ DataFormatException -> 0x00ac }
        L_0x0097:
            r9.a()     // Catch:{ DataFormatException -> 0x00ac }
            int r11 = r2.b     // Catch:{ DataFormatException -> 0x00ac }
            int r12 = r2.c     // Catch:{ DataFormatException -> 0x00ac }
            if (r11 != r12) goto L_0x00a9
            p.Segment r11 = r2.a()     // Catch:{ DataFormatException -> 0x00ac }
            r10.b = r11     // Catch:{ DataFormatException -> 0x00ac }
            p.SegmentPool.a(r2)     // Catch:{ DataFormatException -> 0x00ac }
        L_0x00a9:
            r10 = -1
            return r10
        L_0x00ac:
            r10 = move-exception
            java.io.IOException r11 = new java.io.IOException
            r11.<init>(r10)
            throw r11
        L_0x00b3:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x00b7:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "?"
            java.lang.String r11 = r11.toString()
            r10.<init>(r11)
            throw r10
        L_0x00c3:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "closed"
            java.lang.String r11 = r11.toString()
            r10.<init>(r11)
            throw r10
        L_0x00cf:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r0 = "byteCount < 0: "
            r10.append(r0)
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            java.lang.IllegalArgumentException r11 = new java.lang.IllegalArgumentException
            java.lang.String r10 = r10.toString()
            r11.<init>(r10)
            throw r11
        L_0x00ea:
            java.lang.String r10 = "sink"
            n.n.c.Intrinsics.a(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p.InflaterSource.b(p.Buffer, long):long");
    }

    public void close() {
        if (!this.c) {
            this.f3067e.end();
            this.c = true;
            this.d.close();
        }
    }

    public Timeout b() {
        return this.d.b();
    }
}
