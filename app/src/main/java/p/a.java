package p;

import n.n.c.Intrinsics;
import n.r.Charsets;

/* compiled from: -Base64.kt */
public final class a {
    public static final byte[] a = ByteString.f3063f.b("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/").d;

    static {
        byte[] bArr = ByteString.f3063f.b("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_").d;
    }

    public static final byte[] a(String str) {
        int i2;
        String str2 = str;
        if (str2 != null) {
            int length = str.length();
            while (length > 0 && ((r7 = str2.charAt(length - 1)) == '=' || r7 == 10 || r7 == 13 || r7 == ' ' || r7 == 9)) {
                length--;
            }
            int i3 = (int) ((((long) length) * 6) / 8);
            byte[] bArr = new byte[i3];
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < length; i7++) {
                char charAt = str2.charAt(i7);
                if ('A' <= charAt && 'Z' >= charAt) {
                    i2 = charAt - 'A';
                } else if ('a' <= charAt && 'z' >= charAt) {
                    i2 = charAt - 'G';
                } else if ('0' <= charAt && '9' >= charAt) {
                    i2 = charAt + 4;
                } else if (charAt == '+' || charAt == '-') {
                    i2 = 62;
                } else if (charAt == '/' || charAt == '_') {
                    i2 = 63;
                } else {
                    if (!(charAt == 10 || charAt == 13 || charAt == ' ' || charAt == 9)) {
                        return null;
                    }
                }
                i5 = (i5 << 6) | i2;
                i4++;
                if (i4 % 4 == 0) {
                    int i8 = i6 + 1;
                    bArr[i6] = (byte) (i5 >> 16);
                    int i9 = i8 + 1;
                    bArr[i8] = (byte) (i5 >> 8);
                    bArr[i9] = (byte) i5;
                    i6 = i9 + 1;
                }
            }
            int i10 = i4 % 4;
            if (i10 == 1) {
                return null;
            }
            if (i10 == 2) {
                bArr[i6] = (byte) ((i5 << 12) >> 16);
                i6++;
            } else if (i10 == 3) {
                int i11 = i5 << 6;
                int i12 = i6 + 1;
                bArr[i6] = (byte) (i11 >> 16);
                i6 = i12 + 1;
                bArr[i12] = (byte) (i11 >> 8);
            }
            if (i6 == i3) {
                return bArr;
            }
            byte[] bArr2 = new byte[i6];
            System.arraycopy(bArr, 0, bArr2, 0, i6);
            return bArr2;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static String a(byte[] bArr, byte[] bArr2, int i2) {
        if ((i2 & 1) != 0) {
            bArr2 = a;
        }
        if (bArr == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (bArr2 != null) {
            byte[] bArr3 = new byte[(((bArr.length + 2) / 3) * 4)];
            int length = bArr.length - (bArr.length % 3);
            int i3 = 0;
            int i4 = 0;
            while (i3 < length) {
                int i5 = i3 + 1;
                byte b = bArr[i3];
                int i6 = i5 + 1;
                byte b2 = bArr[i5];
                int i7 = i6 + 1;
                byte b3 = bArr[i6];
                int i8 = i4 + 1;
                bArr3[i4] = bArr2[(b & 255) >> 2];
                int i9 = i8 + 1;
                bArr3[i8] = bArr2[((b & 3) << 4) | ((b2 & 255) >> 4)];
                int i10 = i9 + 1;
                bArr3[i9] = bArr2[((b2 & 15) << 2) | ((b3 & 255) >> 6)];
                i4 = i10 + 1;
                bArr3[i10] = bArr2[b3 & 63];
                i3 = i7;
            }
            int length2 = bArr.length - length;
            if (length2 == 1) {
                byte b4 = bArr[i3];
                int i11 = i4 + 1;
                bArr3[i4] = bArr2[(b4 & 255) >> 2];
                int i12 = i11 + 1;
                bArr3[i11] = bArr2[(b4 & 3) << 4];
                byte b5 = (byte) 61;
                bArr3[i12] = b5;
                bArr3[i12 + 1] = b5;
            } else if (length2 == 2) {
                int i13 = i3 + 1;
                byte b6 = bArr[i3];
                byte b7 = bArr[i13];
                int i14 = i4 + 1;
                bArr3[i4] = bArr2[(b6 & 255) >> 2];
                int i15 = i14 + 1;
                bArr3[i14] = bArr2[((b6 & 3) << 4) | ((b7 & 255) >> 4)];
                bArr3[i15] = bArr2[(b7 & 15) << 2];
                bArr3[i15 + 1] = (byte) 61;
            }
            return new String(bArr3, Charsets.a);
        } else {
            Intrinsics.a("map");
            throw null;
        }
    }
}
