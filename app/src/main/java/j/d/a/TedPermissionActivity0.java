package j.d.a;

import android.content.DialogInterface;
import com.gun0912.tedpermission.TedPermissionActivity;
import i.h.d.ActivityCompat;
import java.util.List;

/* compiled from: TedPermissionActivity */
public class TedPermissionActivity0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ List b;
    public final /* synthetic */ TedPermissionActivity c;

    public TedPermissionActivity0(TedPermissionActivity tedPermissionActivity, List list) {
        this.c = tedPermissionActivity;
        this.b = list;
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        TedPermissionActivity tedPermissionActivity = this.c;
        List list = this.b;
        if (tedPermissionActivity != null) {
            ActivityCompat.a(tedPermissionActivity, (String[]) list.toArray(new String[list.size()]), 10);
            return;
        }
        throw null;
    }
}
