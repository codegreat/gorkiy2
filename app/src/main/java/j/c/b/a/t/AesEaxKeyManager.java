package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.Aead;
import j.c.b.a.KeyManager;
import j.c.b.a.a;
import j.c.b.a.c0.AesEaxJce;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.AesEaxKey;
import j.c.b.a.z.AesEaxKeyFormat;
import j.c.b.a.z.AesEaxParams;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class AesEaxKeyManager implements KeyManager<a> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesEaxKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r4v5, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesEaxKeyFormat) {
            AesEaxKeyFormat aesEaxKeyFormat = (AesEaxKeyFormat) messageLite;
            Validators.a(aesEaxKeyFormat.f2413f);
            if (aesEaxKeyFormat.g().f2415e == 12 || aesEaxKeyFormat.g().f2415e == 16) {
                AesEaxKey.b bVar = (AesEaxKey.b) AesEaxKey.h.e();
                ByteString a = ByteString.a(Random.a(aesEaxKeyFormat.f2413f));
                bVar.m();
                bVar.c.g = a;
                AesEaxParams g = aesEaxKeyFormat.g();
                bVar.m();
                AesEaxKey.a(bVar.c, g);
                bVar.m();
                bVar.c.f2410e = 0;
                return bVar.k();
            }
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        }
        throw new GeneralSecurityException("expected AesEaxKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.AesEaxKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesEaxKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesEaxKey) GeneratedMessageLite.a((GeneratedMessageLite) AesEaxKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesEaxKey proto", e2);
        }
    }

    public Aead a(MessageLite messageLite) {
        if (messageLite instanceof AesEaxKey) {
            AesEaxKey aesEaxKey = (AesEaxKey) messageLite;
            Validators.a(aesEaxKey.f2410e, 0);
            Validators.a(aesEaxKey.g.size());
            if (aesEaxKey.g().f2415e == 12 || aesEaxKey.g().f2415e == 16) {
                return new AesEaxJce(aesEaxKey.g.d(), aesEaxKey.g().f2415e);
            }
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        }
        throw new GeneralSecurityException("expected AesEaxKey proto");
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesEaxKey");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesEaxKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesEaxKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesEaxKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesEaxKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesEaxKeyFormat proto", e2);
        }
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesEaxKey");
        ByteString a = ((AesEaxKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }
}
