package j.c.b.a;

public interface Aead {
    byte[] a(byte[] bArr, byte[] bArr2);

    byte[] b(byte[] bArr, byte[] bArr2);
}
