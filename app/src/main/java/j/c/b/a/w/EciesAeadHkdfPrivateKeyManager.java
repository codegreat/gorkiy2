package j.c.b.a.w;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.HybridDecrypt;
import j.c.b.a.PrivateKeyManager;
import j.c.b.a.c0.EciesAeadHkdfHybridDecrypt;
import j.c.b.a.c0.Validators;
import j.c.b.a.e;
import j.c.b.a.z.EciesAeadHkdfKeyFormat;
import j.c.b.a.z.EciesAeadHkdfParams;
import j.c.b.a.z.EciesAeadHkdfPrivateKey;
import j.c.b.a.z.EciesAeadHkdfPublicKey;
import j.c.b.a.z.EciesHkdfKemParams;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;

public class EciesAeadHkdfPrivateKeyManager implements PrivateKeyManager<e> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r6v15, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof EciesAeadHkdfKeyFormat) {
            EciesAeadHkdfKeyFormat eciesAeadHkdfKeyFormat = (EciesAeadHkdfKeyFormat) messageLite;
            c.a(eciesAeadHkdfKeyFormat.g());
            KeyPair a = c.a(c.a(eciesAeadHkdfKeyFormat.g().k().g()));
            ECPoint w = ((ECPublicKey) a.getPublic()).getW();
            EciesAeadHkdfPublicKey.b bVar = (EciesAeadHkdfPublicKey.b) EciesAeadHkdfPublicKey.f2456i.e();
            bVar.m();
            bVar.c.f2458e = 0;
            EciesAeadHkdfParams g = eciesAeadHkdfKeyFormat.g();
            bVar.m();
            EciesAeadHkdfPublicKey.a(bVar.c, g);
            ByteString a2 = ByteString.a(w.getAffineX().toByteArray());
            bVar.m();
            EciesAeadHkdfPublicKey.a(bVar.c, a2);
            ByteString a3 = ByteString.a(w.getAffineY().toByteArray());
            bVar.m();
            EciesAeadHkdfPublicKey.b(bVar.c, a3);
            EciesAeadHkdfPrivateKey.b bVar2 = (EciesAeadHkdfPrivateKey.b) EciesAeadHkdfPrivateKey.h.e();
            bVar2.m();
            bVar2.c.f2454e = 0;
            bVar2.m();
            EciesAeadHkdfPrivateKey.a(bVar2.c, (EciesAeadHkdfPublicKey) bVar.k());
            ByteString a4 = ByteString.a(((ECPrivateKey) a.getPrivate()).getS().toByteArray());
            bVar2.m();
            EciesAeadHkdfPrivateKey.a(bVar2.c, a4);
            return bVar2.k();
        }
        throw new GeneralSecurityException("expected EciesAeadHkdfKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.EciesAeadHkdfPrivateKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.EciesAeadHkdfPrivateKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((EciesAeadHkdfPrivateKey) GeneratedMessageLite.a((GeneratedMessageLite) EciesAeadHkdfPrivateKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPrivateKey proto", e2);
        }
    }

    public HybridDecrypt a(MessageLite messageLite) {
        if (messageLite instanceof EciesAeadHkdfPrivateKey) {
            EciesAeadHkdfPrivateKey eciesAeadHkdfPrivateKey = (EciesAeadHkdfPrivateKey) messageLite;
            Validators.a(eciesAeadHkdfPrivateKey.f2454e, 0);
            c.a(eciesAeadHkdfPrivateKey.g().g());
            EciesAeadHkdfParams g = eciesAeadHkdfPrivateKey.g().g();
            EciesHkdfKemParams k2 = g.k();
            return new EciesAeadHkdfHybridDecrypt(c.a(c.a(k2.g()), eciesAeadHkdfPrivateKey.g.d()), k2.g.d(), c.b(k2.h()), c.a(g.h()), new RegistryEciesAeadHkdfDemHelper(g.g().g()));
        }
        throw new GeneralSecurityException("expected EciesAeadHkdfPrivateKey proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.EciesAeadHkdfKeyFormat, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.EciesAeadHkdfKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((EciesAeadHkdfKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) EciesAeadHkdfKeyFormat.f2448f, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("invalid EciesAeadHkdf key format", e2);
        }
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey".equals(str);
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey");
        ByteString a = ((EciesAeadHkdfPrivateKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.ASYMMETRIC_PRIVATE);
        return (KeyData) g.k();
    }
}
