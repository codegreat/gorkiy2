package j.c.b.a.c0;

import j.c.b.a.PublicKeySign;

public final class Ed25519Sign implements PublicKeySign {
    public final byte[] a;

    public Ed25519Sign(byte[] bArr) {
        if (bArr.length == 32) {
            byte[] a2 = Ed25519.a(bArr);
            this.a = a2;
            Ed25519.b(a2);
            return;
        }
        throw new IllegalArgumentException(String.format("Given private key's length is not %s", 32));
    }
}
