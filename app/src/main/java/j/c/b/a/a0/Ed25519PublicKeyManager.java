package j.c.b.a.a0;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.KeyManager;
import j.c.b.a.PublicKeyVerify;
import j.c.b.a.c0.Ed25519Verify;
import j.c.b.a.c0.Validators;
import j.c.b.a.q;
import j.c.b.a.z.Ed25519PublicKey;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class Ed25519PublicKeyManager implements KeyManager<q> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.Ed25519PublicKey";
    }

    public int b() {
        return 0;
    }

    public MessageLite b(MessageLite messageLite) {
        throw new GeneralSecurityException("Not implemented");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.Ed25519PublicKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.Ed25519PublicKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((Ed25519PublicKey) GeneratedMessageLite.a((GeneratedMessageLite) Ed25519PublicKey.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("invalid Ed25519 public key", e2);
        }
    }

    public PublicKeyVerify a(MessageLite messageLite) {
        if (messageLite instanceof Ed25519PublicKey) {
            Ed25519PublicKey ed25519PublicKey = (Ed25519PublicKey) messageLite;
            Validators.a(ed25519PublicKey.f2466e, 0);
            if (ed25519PublicKey.f2467f.size() == 32) {
                return new Ed25519Verify(ed25519PublicKey.f2467f.d());
            }
            throw new GeneralSecurityException("invalid Ed25519 public key: incorrect key length");
        }
        throw new GeneralSecurityException("expected Ed25519PublicKey proto");
    }

    public KeyData b(ByteString byteString) {
        throw new GeneralSecurityException("Not implemented");
    }

    public MessageLite a(ByteString byteString) {
        throw new GeneralSecurityException("Not implemented");
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.Ed25519PublicKey".equals(str);
    }
}
