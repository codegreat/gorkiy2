package j.c.b.a.v;

import j.c.b.a.DeterministicAead;
import j.c.b.a.KeyManager;
import j.c.b.a.KeysetHandle;
import j.c.b.a.PrimitiveSet;
import j.c.b.a.Registry;
import j.c.b.a.h;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public final class DeterministicAeadFactory {
    public static final Logger a = Logger.getLogger(DeterministicAeadFactory.class.getName());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.b.a.Registry.a(j.c.b.a.h, j.c.b.a.KeyManager):j.c.b.a.PrimitiveSet<P>
     arg types: [j.c.b.a.KeysetHandle, ?[OBJECT, ARRAY]]
     candidates:
      j.c.b.a.Registry.a(java.lang.String, j.c.e.o):j.c.e.o
      j.c.b.a.Registry.a(java.lang.String, byte[]):P
      j.c.b.a.Registry.a(j.c.b.a.KeyManager, boolean):void
      j.c.b.a.Registry.a(java.lang.String, j.c.b.a.Catalogue):void
      j.c.b.a.Registry.a(j.c.b.a.h, j.c.b.a.KeyManager):j.c.b.a.PrimitiveSet<P> */
    public static DeterministicAead a(KeysetHandle keysetHandle) {
        PrimitiveSet a2 = Registry.a((h) keysetHandle, (KeyManager) null);
        for (List<PrimitiveSet.a<P>> it : a2.a.values()) {
            Iterator it2 = it.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (!(((PrimitiveSet.a) it2.next()).a instanceof DeterministicAead)) {
                        throw new GeneralSecurityException("invalid Deterministic AEAD key material");
                    }
                }
            }
        }
        return new DeterministicAeadFactory0(a2);
    }
}
