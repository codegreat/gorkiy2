package j.c.b.a.c0;

import j.a.a.a.outline;
import java.security.InvalidAlgorithmParameterException;
import java.util.Arrays;

public final class AesGcmHkdfStreaming extends NonceBasedStreamingAead {
    public final int a;

    public AesGcmHkdfStreaming(byte[] bArr, String str, int i2, int i3, int i4) {
        if (bArr.length < 16 || bArr.length < i2) {
            StringBuilder a2 = outline.a("ikm too short, must be >= ");
            a2.append(Math.max(16, i2));
            throw new InvalidAlgorithmParameterException(a2.toString());
        }
        Validators.a(i2);
        if (i3 > this.a + 1 + 7 + i4 + 16) {
            Arrays.copyOf(bArr, bArr.length);
            this.a = i2;
            return;
        }
        throw new InvalidAlgorithmParameterException("ciphertextSegmentSize too small");
    }
}
