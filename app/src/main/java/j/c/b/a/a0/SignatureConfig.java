package j.c.b.a.a0;

import j.c.a.a.c.n.c;
import j.c.b.a.Registry;
import j.c.b.a.z.RegistryConfig;
import java.security.GeneralSecurityException;

public final class SignatureConfig {
    @Deprecated
    public static final RegistryConfig a;
    @Deprecated
    public static final RegistryConfig b;
    public static final RegistryConfig c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      j.c.a.a.c.n.c.a(int, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.security.spec.ECParameterSpec
      j.c.a.a.c.n.c.a(byte, byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(byte[], int, byte[], int, int):byte[]
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry */
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.m();
        RegistryConfig.a((RegistryConfig) g.c, "TINK_SIGNATURE_1_0_0");
        g.a(c.a("TinkPublicKeySign", "PublicKeySign", "EcdsaPrivateKey", 0, true));
        g.a(c.a("TinkPublicKeySign", "PublicKeySign", "Ed25519PrivateKey", 0, true));
        g.a(c.a("TinkPublicKeyVerify", "PublicKeyVerify", "EcdsaPublicKey", 0, true));
        g.a(c.a("TinkPublicKeyVerify", "PublicKeyVerify", "Ed25519PublicKey", 0, true));
        a = (RegistryConfig) g.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(a);
        RegistryConfig.b bVar = g2;
        bVar.m();
        RegistryConfig.a((RegistryConfig) bVar.c, "TINK_SIGNATURE_1_1_0");
        b = (RegistryConfig) bVar.k();
        RegistryConfig.b g3 = RegistryConfig.g();
        g3.m();
        RegistryConfig.a((RegistryConfig) g3.c, "TINK_SIGNATURE");
        g3.a(c.a("TinkPublicKeySign", "PublicKeySign", "EcdsaPrivateKey", 0, true));
        g3.a(c.a("TinkPublicKeySign", "PublicKeySign", "Ed25519PrivateKey", 0, true));
        g3.a(c.a("TinkPublicKeyVerify", "PublicKeyVerify", "EcdsaPublicKey", 0, true));
        g3.a(c.a("TinkPublicKeyVerify", "PublicKeyVerify", "Ed25519PublicKey", 0, true));
        c = (RegistryConfig) g3.k();
        try {
            a();
        } catch (GeneralSecurityException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static void a() {
        Registry.a("TinkPublicKeySign", new PublicKeySignCatalogue());
        Registry.a("TinkPublicKeyVerify", new PublicKeyVerifyCatalogue());
        c.a(c);
    }
}
