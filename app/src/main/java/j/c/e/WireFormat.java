package j.c.e;

public /* synthetic */ class WireFormat {
    public static final /* synthetic */ int[] a;

    /* JADX WARNING: Can't wrap try/catch for region: R(40:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|(2:37|38)|39|(2:41|42)|43|45|46|47|(2:49|50)|51|53|54|55|56|57|58|59|60|61|62|64) */
    /* JADX WARNING: Can't wrap try/catch for region: R(43:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|(2:17|18)|19|(2:21|22)|23|(2:25|26)|27|29|30|31|(2:33|34)|35|(2:37|38)|39|41|42|43|45|46|47|(2:49|50)|51|53|54|55|56|57|58|59|60|61|62|64) */
    /* JADX WARNING: Can't wrap try/catch for region: R(48:0|1|2|3|5|6|7|9|10|11|13|14|15|(2:17|18)|19|21|22|23|25|26|27|29|30|31|(2:33|34)|35|37|38|39|41|42|43|45|46|47|(2:49|50)|51|53|54|55|56|57|58|59|60|61|62|64) */
    /* JADX WARNING: Can't wrap try/catch for region: R(51:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|25|26|27|29|30|31|33|34|35|37|38|39|41|42|43|45|46|47|49|50|51|53|54|55|56|57|58|59|60|61|62|64) */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0077 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x007d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x0083 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x0089 */
    static {
        /*
            j.c.e.WireFormat0[] r0 = j.c.e.WireFormat0.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            j.c.e.WireFormat.a = r0
            r1 = 1
            j.c.e.WireFormat0 r2 = j.c.e.WireFormat0.DOUBLE     // Catch:{ NoSuchFieldError -> 0x000f }
            r2 = 0
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
        L_0x000f:
            r0 = 2
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0016 }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.FLOAT     // Catch:{ NoSuchFieldError -> 0x0016 }
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0016 }
        L_0x0016:
            r1 = 3
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x001d }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.INT64     // Catch:{ NoSuchFieldError -> 0x001d }
            r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x001d }
        L_0x001d:
            r0 = 4
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0024 }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.UINT64     // Catch:{ NoSuchFieldError -> 0x0024 }
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0024 }
        L_0x0024:
            r1 = 5
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x002b }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.INT32     // Catch:{ NoSuchFieldError -> 0x002b }
            r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x002b }
        L_0x002b:
            r0 = 6
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0032 }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.FIXED64     // Catch:{ NoSuchFieldError -> 0x0032 }
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
        L_0x0032:
            r1 = 7
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0039 }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.FIXED32     // Catch:{ NoSuchFieldError -> 0x0039 }
            r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0039 }
        L_0x0039:
            r0 = 8
            int[] r2 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0041 }
            j.c.e.WireFormat0 r3 = j.c.e.WireFormat0.BOOL     // Catch:{ NoSuchFieldError -> 0x0041 }
            r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0041 }
        L_0x0041:
            r1 = 9
            r2 = 11
            int[] r3 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x004b }
            j.c.e.WireFormat0 r4 = j.c.e.WireFormat0.BYTES     // Catch:{ NoSuchFieldError -> 0x004b }
            r3[r2] = r1     // Catch:{ NoSuchFieldError -> 0x004b }
        L_0x004b:
            r3 = 10
            r4 = 12
            int[] r5 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0055 }
            j.c.e.WireFormat0 r6 = j.c.e.WireFormat0.UINT32     // Catch:{ NoSuchFieldError -> 0x0055 }
            r5[r4] = r3     // Catch:{ NoSuchFieldError -> 0x0055 }
        L_0x0055:
            r5 = 14
            int[] r6 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x005d }
            j.c.e.WireFormat0 r7 = j.c.e.WireFormat0.SFIXED32     // Catch:{ NoSuchFieldError -> 0x005d }
            r6[r5] = r2     // Catch:{ NoSuchFieldError -> 0x005d }
        L_0x005d:
            r2 = 15
            int[] r6 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0065 }
            j.c.e.WireFormat0 r7 = j.c.e.WireFormat0.SFIXED64     // Catch:{ NoSuchFieldError -> 0x0065 }
            r6[r2] = r4     // Catch:{ NoSuchFieldError -> 0x0065 }
        L_0x0065:
            r4 = 13
            r6 = 16
            int[] r7 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x006f }
            j.c.e.WireFormat0 r8 = j.c.e.WireFormat0.SINT32     // Catch:{ NoSuchFieldError -> 0x006f }
            r7[r6] = r4     // Catch:{ NoSuchFieldError -> 0x006f }
        L_0x006f:
            r7 = 17
            int[] r8 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0077 }
            j.c.e.WireFormat0 r9 = j.c.e.WireFormat0.SINT64     // Catch:{ NoSuchFieldError -> 0x0077 }
            r8[r7] = r5     // Catch:{ NoSuchFieldError -> 0x0077 }
        L_0x0077:
            int[] r5 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x007d }
            j.c.e.WireFormat0 r8 = j.c.e.WireFormat0.STRING     // Catch:{ NoSuchFieldError -> 0x007d }
            r5[r0] = r2     // Catch:{ NoSuchFieldError -> 0x007d }
        L_0x007d:
            int[] r0 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0083 }
            j.c.e.WireFormat0 r2 = j.c.e.WireFormat0.GROUP     // Catch:{ NoSuchFieldError -> 0x0083 }
            r0[r1] = r6     // Catch:{ NoSuchFieldError -> 0x0083 }
        L_0x0083:
            int[] r0 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0089 }
            j.c.e.WireFormat0 r1 = j.c.e.WireFormat0.MESSAGE     // Catch:{ NoSuchFieldError -> 0x0089 }
            r0[r3] = r7     // Catch:{ NoSuchFieldError -> 0x0089 }
        L_0x0089:
            int[] r0 = j.c.e.WireFormat.a     // Catch:{ NoSuchFieldError -> 0x0091 }
            j.c.e.WireFormat0 r1 = j.c.e.WireFormat0.ENUM     // Catch:{ NoSuchFieldError -> 0x0091 }
            r1 = 18
            r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0091 }
        L_0x0091:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.e.WireFormat.<clinit>():void");
    }
}
