package j.c.e;

import j.c.e.FieldSet.a;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.SmallSortedMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class FieldSet<FieldDescriptorType extends a<FieldDescriptorType>> {
    public final SmallSortedMap<FieldDescriptorType, Object> a;
    public boolean b;
    public boolean c;

    public interface a<T extends a<T>> extends Comparable<T> {
        MessageLite.a a(MessageLite.a aVar, MessageLite messageLite);

        boolean k();

        WireFormat0 m();

        WireFormat1 n();
    }

    static {
        new FieldSet(true);
    }

    public FieldSet() {
        this.c = false;
        this.a = SmallSortedMap.c(16);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if ((r3 instanceof j.c.e.Internal.a) == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
        if ((r3 instanceof j.c.e.LazyField) == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(j.c.e.WireFormat0 r2, java.lang.Object r3) {
        /*
            if (r3 == 0) goto L_0x0048
            j.c.e.WireFormat1 r2 = r2.javaType
            int r2 = r2.ordinal()
            r0 = 1
            r1 = 0
            switch(r2) {
                case 0: goto L_0x003b;
                case 1: goto L_0x0038;
                case 2: goto L_0x0035;
                case 3: goto L_0x0032;
                case 4: goto L_0x002f;
                case 5: goto L_0x002c;
                case 6: goto L_0x0020;
                case 7: goto L_0x0017;
                case 8: goto L_0x000e;
                default: goto L_0x000d;
            }
        L_0x000d:
            goto L_0x003d
        L_0x000e:
            boolean r2 = r3 instanceof j.c.e.MessageLite
            if (r2 != 0) goto L_0x002a
            boolean r2 = r3 instanceof j.c.e.LazyField
            if (r2 == 0) goto L_0x0029
            goto L_0x002a
        L_0x0017:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x002a
            boolean r2 = r3 instanceof j.c.e.Internal.a
            if (r2 == 0) goto L_0x0029
            goto L_0x002a
        L_0x0020:
            boolean r2 = r3 instanceof j.c.e.ByteString
            if (r2 != 0) goto L_0x002a
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0029
            goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            r1 = r0
            goto L_0x003d
        L_0x002c:
            boolean r1 = r3 instanceof java.lang.String
            goto L_0x003d
        L_0x002f:
            boolean r1 = r3 instanceof java.lang.Boolean
            goto L_0x003d
        L_0x0032:
            boolean r1 = r3 instanceof java.lang.Double
            goto L_0x003d
        L_0x0035:
            boolean r1 = r3 instanceof java.lang.Float
            goto L_0x003d
        L_0x0038:
            boolean r1 = r3 instanceof java.lang.Long
            goto L_0x003d
        L_0x003b:
            boolean r1 = r3 instanceof java.lang.Integer
        L_0x003d:
            if (r1 == 0) goto L_0x0040
            return
        L_0x0040:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            throw r2
        L_0x0048:
            r2 = 0
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.e.FieldSet.a(j.c.e.WireFormat0, java.lang.Object):void");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FieldSet)) {
            return false;
        }
        return this.a.equals(((FieldSet) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public FieldSet<FieldDescriptorType> clone() {
        FieldSet<FieldDescriptorType> fieldSet = new FieldSet<>();
        for (int i2 = 0; i2 < this.a.b(); i2++) {
            Map.Entry<FieldDescriptorType, Object> a2 = this.a.a(i2);
            fieldSet.a((a) a2.getKey(), a2.getValue());
        }
        for (Map.Entry next : this.a.c()) {
            fieldSet.a((a) next.getKey(), next.getValue());
        }
        fieldSet.c = this.c;
        return fieldSet;
    }

    public FieldSet(boolean z) {
        this.c = false;
        SmallSortedMap.a aVar = new SmallSortedMap.a(0);
        this.a = aVar;
        if (!this.b) {
            aVar.e();
            this.b = true;
        }
    }

    public void a(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.k()) {
            a(fielddescriptortype.m(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                a(fielddescriptortype.m(), it.next());
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof LazyField) {
            this.c = true;
        }
        this.a.put(fielddescriptortype, obj);
    }

    public final Object a(Object obj) {
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    public final void a(Map.Entry<FieldDescriptorType, Object> entry) {
        a aVar = (a) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof LazyField) {
            value = ((LazyField) value).a();
        }
        if (aVar.k()) {
            Object obj = this.a.get(aVar);
            if (obj instanceof LazyField) {
                obj = ((LazyField) obj).a();
            }
            if (obj == null) {
                obj = new ArrayList();
            }
            for (Object a2 : (List) value) {
                ((List) obj).add(a(a2));
            }
            this.a.put(aVar, obj);
        } else if (aVar.n() == WireFormat1.MESSAGE) {
            Object obj2 = this.a.get(aVar);
            if (obj2 instanceof LazyField) {
                obj2 = ((LazyField) obj2).a();
            }
            if (obj2 == null) {
                this.a.put(aVar, a(value));
                return;
            }
            this.a.put(aVar, ((GeneratedMessageLite.b) aVar.a(((MessageLite) obj2).e(), (MessageLite) value)).k());
        } else {
            this.a.put(aVar, a(value));
        }
    }
}
