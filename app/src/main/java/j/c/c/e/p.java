package j.c.c.e;

import i.b.k.ResourcesFlusher;
import j.c.c.f.a;
import j.c.c.f.b;
import j.c.c.f.c;
import j.c.c.f.d;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class p implements d, c {
    public final Map<Class<?>, ConcurrentHashMap<b<Object>, Executor>> a = new HashMap();
    public Queue<a<?>> b = new ArrayDeque();
    public final Executor c;

    public p(Executor executor) {
        this.c = executor;
    }

    public final synchronized Set<Map.Entry<b<Object>, Executor>> a(a<?> aVar) {
        Map map;
        Map<Class<?>, ConcurrentHashMap<b<Object>, Executor>> map2 = this.a;
        if (aVar != null) {
            map = map2.get(null);
        } else {
            throw null;
        }
        return map == null ? Collections.emptySet() : map.entrySet();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r0.hasNext() == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1 = r0.next();
        ((java.util.concurrent.Executor) r1.getValue()).execute(new j.c.c.e.o(r1, r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = a(r5).iterator();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(j.c.c.f.a<?> r5) {
        /*
            r4 = this;
            i.b.k.ResourcesFlusher.b(r5)
            monitor-enter(r4)
            java.util.Queue<j.c.c.f.a<?>> r0 = r4.b     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x000f
            java.util.Queue<j.c.c.f.a<?>> r0 = r4.b     // Catch:{ all -> 0x0034 }
            r0.add(r5)     // Catch:{ all -> 0x0034 }
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            return
        L_0x000f:
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            java.util.Set r0 = r4.a(r5)
            java.util.Iterator r0 = r0.iterator()
        L_0x0018:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0033
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getValue()
            java.util.concurrent.Executor r2 = (java.util.concurrent.Executor) r2
            j.c.c.e.o r3 = new j.c.c.e.o
            r3.<init>(r1, r5)
            r2.execute(r3)
            goto L_0x0018
        L_0x0033:
            return
        L_0x0034:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.e.p.b(j.c.c.f.a):void");
    }

    public synchronized <T> void a(Class<T> cls, Executor executor, b<? super T> bVar) {
        ResourcesFlusher.b(cls);
        ResourcesFlusher.b(bVar);
        ResourcesFlusher.b(executor);
        if (!this.a.containsKey(cls)) {
            this.a.put(cls, new ConcurrentHashMap());
        }
        this.a.get(cls).put(bVar, executor);
    }

    public <T> void a(Class<T> cls, b<? super T> bVar) {
        a(cls, this.c, bVar);
    }

    public void a() {
        Queue<a<?>> queue;
        synchronized (this) {
            queue = null;
            if (this.b != null) {
                Queue<a<?>> queue2 = this.b;
                this.b = null;
                queue = queue2;
            }
        }
        if (queue != null) {
            for (a b2 : queue) {
                b(b2);
            }
        }
    }
}
