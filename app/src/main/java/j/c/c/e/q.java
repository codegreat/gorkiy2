package j.c.c.e;

import j.c.c.h.a;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class q<T> implements a<T> {
    public static final Object c = new Object();
    public volatile Object a = c;
    public volatile a<T> b;

    public q(a<T> aVar) {
        this.b = aVar;
    }

    public T get() {
        T t2 = this.a;
        if (t2 == c) {
            synchronized (this) {
                t2 = this.a;
                if (t2 == c) {
                    t2 = this.b.get();
                    this.a = t2;
                    this.b = null;
                }
            }
        }
        return t2;
    }
}
