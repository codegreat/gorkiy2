package j.c.c.j;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class d {
    public static volatile d b;
    public final Set<e> a = new HashSet();

    public static d b() {
        d dVar = b;
        if (dVar == null) {
            synchronized (d.class) {
                dVar = b;
                if (dVar == null) {
                    dVar = new d();
                    b = dVar;
                }
            }
        }
        return dVar;
    }

    public Set<e> a() {
        Set<e> unmodifiableSet;
        synchronized (this.a) {
            unmodifiableSet = Collections.unmodifiableSet(this.a);
        }
        return unmodifiableSet;
    }
}
