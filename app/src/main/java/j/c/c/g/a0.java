package j.c.c.g;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import javax.annotation.Nullable;

public final class a0 extends BroadcastReceiver {
    @Nullable
    public x a;

    public a0(x xVar) {
        this.a = xVar;
    }

    public final void onReceive(Context context, Intent intent) {
        x xVar = this.a;
        if (xVar != null && xVar.c()) {
            if (FirebaseInstanceId.h()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.a(this.a, 0);
            this.a.a().unregisterReceiver(super);
            this.a = null;
        }
    }
}
