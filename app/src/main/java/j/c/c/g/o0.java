package j.c.c.g;

import android.os.Bundle;
import j.c.a.a.i.f;
import java.io.IOException;

public final /* synthetic */ class o0 implements Runnable {
    public final p0 b;
    public final Bundle c;
    public final f d;

    public o0(p0 p0Var, Bundle bundle, f fVar) {
        this.b = p0Var;
        this.c = bundle;
        this.d = fVar;
    }

    public final void run() {
        p0 p0Var = this.b;
        Bundle bundle = this.c;
        f fVar = this.d;
        if (p0Var != null) {
            try {
                fVar.a.a(p0Var.c.a(bundle));
            } catch (IOException e2) {
                fVar.a.a((Exception) e2);
            }
        } else {
            throw null;
        }
    }
}
