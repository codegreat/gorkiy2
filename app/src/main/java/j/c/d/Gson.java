package j.c.d;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.MalformedJsonException;
import j.c.d.b0.ConstructorConstructor;
import j.c.d.b0.Excluder;
import j.c.d.b0.Primitives;
import j.c.d.b0.a0.ArrayTypeAdapter;
import j.c.d.b0.a0.CollectionTypeAdapterFactory;
import j.c.d.b0.a0.DateTypeAdapter;
import j.c.d.b0.a0.JsonAdapterAnnotationTypeAdapterFactory;
import j.c.d.b0.a0.MapTypeAdapterFactory;
import j.c.d.b0.a0.ObjectTypeAdapter;
import j.c.d.b0.a0.ReflectiveTypeAdapterFactory;
import j.c.d.b0.a0.SqlDateTypeAdapter;
import j.c.d.b0.a0.TimeTypeAdapter;
import j.c.d.b0.a0.TypeAdapters;
import j.c.d.b0.a0.TypeAdapters0;
import j.c.d.b0.o;
import j.c.d.c0.TypeToken;
import java.io.EOFException;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;

public final class Gson {

    /* renamed from: j  reason: collision with root package name */
    public static final TypeToken<?> f2534j = new TypeToken<>(Object.class);
    public final ThreadLocal<Map<TypeToken<?>, a<?>>> a;
    public final Map<TypeToken<?>, TypeAdapter<?>> b;
    public final ConstructorConstructor c;
    public final JsonAdapterAnnotationTypeAdapterFactory d;

    /* renamed from: e  reason: collision with root package name */
    public final List<z> f2535e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f2536f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f2537i;

    /* JADX WARN: Type inference failed for: r2v0, types: [j.c.d.FieldNamingStrategy, j.c.d.FieldNamingPolicy] */
    public Gson() {
        this(Excluder.g, FieldNamingPolicy.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, false, LongSerializationPolicy.DEFAULT, null, 2, 2, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
    }

    public static void a(double d2) {
        if (Double.isNaN(d2) || Double.isInfinite(d2)) {
            throw new IllegalArgumentException(d2 + " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.f2536f + ",factories:" + this.f2535e + ",instanceCreators:" + this.c + "}";
    }

    public static class a<T> extends TypeAdapter<T> {
        public TypeAdapter<T> a;

        public T a(JsonReader jsonReader) {
            TypeAdapter<T> typeAdapter = this.a;
            if (typeAdapter != null) {
                return super.a(jsonReader);
            }
            throw new IllegalStateException();
        }

        public void a(JsonWriter jsonWriter, T t2) {
            TypeAdapter<T> typeAdapter = this.a;
            if (typeAdapter != null) {
                super.a(jsonWriter, t2);
                return;
            }
            throw new IllegalStateException();
        }
    }

    public <T> TypeAdapter<T> a(TypeToken<T> typeToken) {
        TypeAdapter<T> typeAdapter = this.b.get(typeToken == null ? f2534j : typeToken);
        if (typeAdapter != null) {
            return typeAdapter;
        }
        Map map = this.a.get();
        boolean z = false;
        if (map == null) {
            map = new HashMap();
            this.a.set(map);
            z = true;
        }
        a aVar = (a) map.get(typeToken);
        if (aVar != null) {
            return aVar;
        }
        try {
            a aVar2 = new a();
            map.put(typeToken, aVar2);
            Iterator<z> it = this.f2535e.iterator();
            while (it.hasNext()) {
                TypeAdapter<T> a2 = it.next().a(this, typeToken);
                if (a2 != null) {
                    if (aVar2.a == null) {
                        aVar2.a = a2;
                        this.b.put(typeToken, a2);
                        return a2;
                    }
                    throw new AssertionError();
                }
            }
            throw new IllegalArgumentException("GSON (2.8.5) cannot handle " + typeToken);
        } finally {
            map.remove(typeToken);
            if (z) {
                this.a.remove();
            }
        }
    }

    public Gson(o oVar, e eVar, Map<Type, InstanceCreator<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, w wVar, String str, int i2, int i3, List<z> list, List<z> list2, List<z> list3) {
        TypeAdapter typeAdapter;
        TypeAdapter typeAdapter2;
        TypeAdapter typeAdapter3;
        this.a = new ThreadLocal<>();
        this.b = new ConcurrentHashMap();
        this.c = new ConstructorConstructor(map);
        this.f2536f = z;
        this.g = z3;
        this.h = z5;
        this.f2537i = z6;
        ArrayList arrayList = new ArrayList();
        arrayList.add(TypeAdapters.Y);
        arrayList.add(ObjectTypeAdapter.b);
        arrayList.add(oVar);
        arrayList.addAll(list3);
        arrayList.add(TypeAdapters.D);
        arrayList.add(TypeAdapters.f2577m);
        arrayList.add(TypeAdapters.g);
        arrayList.add(TypeAdapters.f2573i);
        arrayList.add(TypeAdapters.f2575k);
        if (wVar == LongSerializationPolicy.DEFAULT) {
            typeAdapter = TypeAdapters.f2584t;
        } else {
            typeAdapter = new Gson2();
        }
        arrayList.add(new TypeAdapters0(Long.TYPE, Long.class, typeAdapter));
        Class cls = Double.TYPE;
        Class<Double> cls2 = Double.class;
        if (z7) {
            typeAdapter2 = TypeAdapters.v;
        } else {
            typeAdapter2 = new Gson0(this);
        }
        arrayList.add(new TypeAdapters0(cls, cls2, typeAdapter2));
        Class cls3 = Float.TYPE;
        Class<Float> cls4 = Float.class;
        if (z7) {
            typeAdapter3 = TypeAdapters.u;
        } else {
            typeAdapter3 = new Gson1(this);
        }
        arrayList.add(new TypeAdapters0(cls3, cls4, typeAdapter3));
        arrayList.add(TypeAdapters.x);
        arrayList.add(TypeAdapters.f2579o);
        arrayList.add(TypeAdapters.f2581q);
        arrayList.add(new TypeAdapters.y(AtomicLong.class, new TypeAdapter0(new Gson3(typeAdapter))));
        arrayList.add(new TypeAdapters.y(AtomicLongArray.class, new TypeAdapter0(new Gson4(typeAdapter))));
        arrayList.add(TypeAdapters.f2583s);
        arrayList.add(TypeAdapters.z);
        arrayList.add(TypeAdapters.F);
        arrayList.add(TypeAdapters.H);
        arrayList.add(new TypeAdapters.y(BigDecimal.class, TypeAdapters.B));
        arrayList.add(new TypeAdapters.y(BigInteger.class, TypeAdapters.C));
        arrayList.add(TypeAdapters.J);
        arrayList.add(TypeAdapters.L);
        arrayList.add(TypeAdapters.P);
        arrayList.add(TypeAdapters.R);
        arrayList.add(TypeAdapters.W);
        arrayList.add(TypeAdapters.N);
        arrayList.add(TypeAdapters.d);
        arrayList.add(DateTypeAdapter.b);
        arrayList.add(TypeAdapters.U);
        arrayList.add(TimeTypeAdapter.b);
        arrayList.add(SqlDateTypeAdapter.b);
        arrayList.add(TypeAdapters.S);
        arrayList.add(ArrayTypeAdapter.c);
        arrayList.add(TypeAdapters.b);
        arrayList.add(new CollectionTypeAdapterFactory(this.c));
        arrayList.add(new MapTypeAdapterFactory(this.c, z2));
        JsonAdapterAnnotationTypeAdapterFactory jsonAdapterAnnotationTypeAdapterFactory = new JsonAdapterAnnotationTypeAdapterFactory(this.c);
        this.d = jsonAdapterAnnotationTypeAdapterFactory;
        arrayList.add(jsonAdapterAnnotationTypeAdapterFactory);
        arrayList.add(TypeAdapters.Z);
        arrayList.add(new ReflectiveTypeAdapterFactory(this.c, eVar, oVar, this.d));
        this.f2535e = Collections.unmodifiableList(arrayList);
    }

    public <T> TypeAdapter<T> a(z zVar, TypeToken<T> typeToken) {
        if (!this.f2535e.contains(zVar)) {
            zVar = this.d;
        }
        boolean z = false;
        for (TypeAdapterFactory next : this.f2535e) {
            if (z) {
                TypeAdapter<T> a2 = next.a(this, typeToken);
                if (a2 != null) {
                    return a2;
                }
            } else if (next == zVar) {
                z = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + typeToken);
    }

    public <T> T a(String str, Class<T> cls) {
        Object obj;
        if (str == null) {
            obj = null;
        } else {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            jsonReader.setLenient(this.f2537i);
            boolean isLenient = jsonReader.isLenient();
            jsonReader.setLenient(true);
            try {
                jsonReader.peek();
                obj = a(new TypeToken(cls)).a(jsonReader);
            } catch (EOFException e2) {
                if (1 != 0) {
                    obj = null;
                } else {
                    throw new JsonSyntaxException(e2);
                }
            } catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            } catch (IOException e4) {
                throw new JsonSyntaxException(e4);
            } catch (AssertionError e5) {
                throw new AssertionError("AssertionError (GSON 2.8.5): " + e5.getMessage(), e5);
            } catch (Throwable th) {
                jsonReader.setLenient(isLenient);
                throw th;
            }
            jsonReader.setLenient(isLenient);
            if (obj != null) {
                try {
                    if (jsonReader.peek() != JsonToken.END_DOCUMENT) {
                        throw new JsonIOException("JSON document was not fully consumed.");
                    }
                } catch (MalformedJsonException e6) {
                    throw new JsonSyntaxException(e6);
                } catch (IOException e7) {
                    throw new JsonIOException(e7);
                }
            }
        }
        Map<Class<?>, Class<?>> map = Primitives.a;
        if (cls != null) {
            Class<T> cls2 = map.get(cls);
            if (cls2 != null) {
                cls = cls2;
            }
            return cls.cast(obj);
        }
        throw null;
    }
}
