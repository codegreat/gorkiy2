package j.c.d.b0.a0;

import j.a.a.a.outline;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.c0.TypeToken;
import j.c.d.k;

/* compiled from: TypeAdapters */
public final class TypeAdapters1 implements TypeAdapterFactory {
    public final /* synthetic */ Class b;
    public final /* synthetic */ Class c;
    public final /* synthetic */ TypeAdapter d;

    public TypeAdapters1(Class cls, Class cls2, TypeAdapter typeAdapter) {
        this.b = cls;
        this.c = cls2;
        this.d = typeAdapter;
    }

    public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
        Class<? super T> cls = typeToken.a;
        if (cls == this.b || cls == this.c) {
            return this.d;
        }
        return null;
    }

    public String toString() {
        StringBuilder a = outline.a("Factory[type=");
        a.append(this.b.getName());
        a.append("+");
        a.append(this.c.getName());
        a.append(",adapter=");
        a.append(this.d);
        a.append("]");
        return a.toString();
    }
}
