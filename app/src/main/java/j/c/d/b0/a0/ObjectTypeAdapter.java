package j.c.d.b0.a0;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import j.c.d.Gson;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.b0.LinkedTreeMap;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import java.util.ArrayList;

public final class ObjectTypeAdapter extends TypeAdapter<Object> {
    public static final TypeAdapterFactory b = new a();
    public final Gson a;

    public static class a implements TypeAdapterFactory {
        public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
            if (typeToken.a == Object.class) {
                return new ObjectTypeAdapter(kVar);
            }
            return null;
        }
    }

    public ObjectTypeAdapter(Gson gson) {
        this.a = gson;
    }

    public Object a(JsonReader jsonReader) {
        int ordinal = jsonReader.peek().ordinal();
        if (ordinal == 0) {
            ArrayList arrayList = new ArrayList();
            jsonReader.beginArray();
            while (jsonReader.hasNext()) {
                arrayList.add(a(jsonReader));
            }
            jsonReader.endArray();
            return arrayList;
        } else if (ordinal == 2) {
            LinkedTreeMap linkedTreeMap = new LinkedTreeMap();
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                linkedTreeMap.put(jsonReader.nextName(), a(jsonReader));
            }
            jsonReader.endObject();
            return linkedTreeMap;
        } else if (ordinal == 5) {
            return jsonReader.nextString();
        } else {
            if (ordinal == 6) {
                return Double.valueOf(jsonReader.nextDouble());
            }
            if (ordinal == 7) {
                return Boolean.valueOf(jsonReader.nextBoolean());
            }
            if (ordinal == 8) {
                jsonReader.nextNull();
                return null;
            }
            throw new IllegalStateException();
        }
    }

    public void a(JsonWriter jsonWriter, Object obj) {
        if (obj == null) {
            jsonWriter.nullValue();
            return;
        }
        Gson gson = this.a;
        Class<?> cls = obj.getClass();
        if (gson != null) {
            TypeAdapter a2 = gson.a(new TypeToken(cls));
            if (a2 instanceof ObjectTypeAdapter) {
                jsonWriter.beginObject();
                jsonWriter.endObject();
                return;
            }
            super.a(jsonWriter, obj);
            return;
        }
        throw null;
    }
}
