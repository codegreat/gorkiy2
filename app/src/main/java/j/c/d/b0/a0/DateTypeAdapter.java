package j.c.d.b0.a0;

import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.c.a.a.c.n.c;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.b0.JavaVersion;
import j.c.d.b0.a0.s.ISO8601Utils;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class DateTypeAdapter extends TypeAdapter<Date> {
    public static final TypeAdapterFactory b = new a();
    public final List<DateFormat> a;

    public static class a implements TypeAdapterFactory {
        public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
            if (typeToken.a == Date.class) {
                return new DateTypeAdapter();
            }
            return null;
        }
    }

    public DateTypeAdapter() {
        ArrayList arrayList = new ArrayList();
        this.a = arrayList;
        arrayList.add(DateFormat.getDateTimeInstance(2, 2, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.a.add(DateFormat.getDateTimeInstance(2, 2));
        }
        if (JavaVersion.a >= 9) {
            this.a.add(c.a(2, 2));
        }
    }

    public Object a(JsonReader jsonReader) {
        if (jsonReader.peek() != JsonToken.NULL) {
            return a(jsonReader.nextString());
        }
        jsonReader.nextNull();
        return null;
    }

    public final synchronized Date a(String str) {
        for (DateFormat parse : this.a) {
            try {
                return parse.parse(str);
            } catch (ParseException unused) {
            }
        }
        try {
            return ISO8601Utils.a(str, new ParsePosition(0));
        } catch (ParseException e2) {
            throw new JsonSyntaxException(str, e2);
        }
    }

    public synchronized void a(JsonWriter jsonWriter, Date date) {
        if (date == null) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(this.a.get(0).format(date));
        }
    }
}
