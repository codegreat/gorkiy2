package j.c.d.b0;

import j.a.a.a.outline;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ConstructorConstructor */
public class ConstructorConstructor5 implements ObjectConstructor<T> {
    public final /* synthetic */ Constructor a;

    public ConstructorConstructor5(ConstructorConstructor constructorConstructor, Constructor constructor) {
        this.a = constructor;
    }

    public T a() {
        try {
            return this.a.newInstance(null);
        } catch (InstantiationException e2) {
            StringBuilder a2 = outline.a("Failed to invoke ");
            a2.append(this.a);
            a2.append(" with no args");
            throw new RuntimeException(a2.toString(), e2);
        } catch (InvocationTargetException e3) {
            StringBuilder a3 = outline.a("Failed to invoke ");
            a3.append(this.a);
            a3.append(" with no args");
            throw new RuntimeException(a3.toString(), e3.getTargetException());
        } catch (IllegalAccessException e4) {
            throw new AssertionError(e4);
        }
    }
}
