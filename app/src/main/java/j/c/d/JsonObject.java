package j.c.d;

import j.c.d.b0.LinkedTreeMap;

public final class JsonObject extends JsonElement {
    public final LinkedTreeMap<String, q> a = new LinkedTreeMap<>();

    public void a(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = JsonNull.a;
        }
        this.a.put(str, jsonElement);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonObject) && ((JsonObject) obj).a.equals(this.a));
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public void a(String str, String str2) {
        a(str, str2 == null ? JsonNull.a : new JsonPrimitive((Object) str2));
    }
}
