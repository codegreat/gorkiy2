package j.c.d;

import j.c.d.b0.LazilyParsedNumber;
import java.math.BigInteger;

public final class JsonPrimitive extends JsonElement {
    public static final Class<?>[] b = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};
    public Object a;

    public JsonPrimitive(Boolean bool) {
        a(bool);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r0 != false) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Object r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof java.lang.Character
            if (r0 == 0) goto L_0x0011
            java.lang.Character r8 = (java.lang.Character) r8
            char r8 = r8.charValue()
            java.lang.String r8 = java.lang.String.valueOf(r8)
            r7.a = r8
            goto L_0x003c
        L_0x0011:
            boolean r0 = r8 instanceof java.lang.Number
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0036
            boolean r0 = r8 instanceof java.lang.String
            if (r0 == 0) goto L_0x001d
        L_0x001b:
            r0 = 1
            goto L_0x0034
        L_0x001d:
            java.lang.Class r0 = r8.getClass()
            java.lang.Class<?>[] r3 = j.c.d.JsonPrimitive.b
            int r4 = r3.length
            r5 = 0
        L_0x0025:
            if (r5 >= r4) goto L_0x0033
            r6 = r3[r5]
            boolean r6 = r6.isAssignableFrom(r0)
            if (r6 == 0) goto L_0x0030
            goto L_0x001b
        L_0x0030:
            int r5 = r5 + 1
            goto L_0x0025
        L_0x0033:
            r0 = 0
        L_0x0034:
            if (r0 == 0) goto L_0x0037
        L_0x0036:
            r1 = 1
        L_0x0037:
            j.c.a.a.c.n.c.a(r1)
            r7.a = r8
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.d.JsonPrimitive.a(java.lang.Object):void");
    }

    public String d() {
        Object obj = this.a;
        if (obj instanceof Number) {
            return f().toString();
        }
        if (obj instanceof Boolean) {
            return ((Boolean) obj).toString();
        }
        return (String) obj;
    }

    public boolean e() {
        Object obj = this.a;
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue();
        }
        return Boolean.parseBoolean(d());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || JsonPrimitive.class != obj.getClass()) {
            return false;
        }
        JsonPrimitive jsonPrimitive = (JsonPrimitive) obj;
        if (this.a == null) {
            if (jsonPrimitive.a == null) {
                return true;
            }
            return false;
        } else if (!a(this) || !a(jsonPrimitive)) {
            if (!(this.a instanceof Number) || !(jsonPrimitive.a instanceof Number)) {
                return this.a.equals(jsonPrimitive.a);
            }
            double doubleValue = f().doubleValue();
            double doubleValue2 = jsonPrimitive.f().doubleValue();
            if (doubleValue == doubleValue2) {
                return true;
            }
            if (!Double.isNaN(doubleValue) || !Double.isNaN(doubleValue2)) {
                return false;
            }
            return true;
        } else if (f().longValue() == jsonPrimitive.f().longValue()) {
            return true;
        } else {
            return false;
        }
    }

    public Number f() {
        Object obj = this.a;
        return obj instanceof String ? new LazilyParsedNumber((String) this.a) : (Number) obj;
    }

    public int hashCode() {
        long doubleToLongBits;
        if (this.a == null) {
            return 31;
        }
        if (a(this)) {
            doubleToLongBits = f().longValue();
        } else {
            Object obj = this.a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(f().doubleValue());
        }
        return (int) ((doubleToLongBits >>> 32) ^ doubleToLongBits);
    }

    public JsonPrimitive(Number number) {
        a(number);
    }

    public JsonPrimitive(String str) {
        a(str);
    }

    public JsonPrimitive(Object obj) {
        a(obj);
    }

    public static boolean a(JsonPrimitive jsonPrimitive) {
        Object obj = jsonPrimitive.a;
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return true;
        }
        return false;
    }
}
