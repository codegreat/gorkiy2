package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: Gson */
public final class Gson3 extends TypeAdapter<AtomicLong> {
    public final /* synthetic */ TypeAdapter a;

    public Gson3(TypeAdapter typeAdapter) {
        this.a = super;
    }

    public void a(JsonWriter jsonWriter, Object obj) {
        this.a.a(jsonWriter, Long.valueOf(((AtomicLong) obj).get()));
    }

    public Object a(JsonReader jsonReader) {
        return new AtomicLong(((Number) this.a.a(jsonReader)).longValue());
    }
}
