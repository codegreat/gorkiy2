package j.c.a.b.w;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import j.c.a.b.w.c;

public interface CircularRevealWidget {

    public static class b implements TypeEvaluator<c.e> {
        public static final TypeEvaluator<c.e> b = new b();
        public final e a = new e(null);

        public Object evaluate(float f2, Object obj, Object obj2) {
            e eVar = (e) obj;
            e eVar2 = (e) obj2;
            e eVar3 = this.a;
            float a2 = j.c.a.a.c.n.c.a(eVar.a, eVar2.a, f2);
            float a3 = j.c.a.a.c.n.c.a(eVar.b, eVar2.b, f2);
            float a4 = j.c.a.a.c.n.c.a(eVar.c, eVar2.c, f2);
            eVar3.a = a2;
            eVar3.b = a3;
            eVar3.c = a4;
            return this.a;
        }
    }

    public static class c extends Property<c, c.e> {
        public static final Property<c, c.e> a = new c("circularReveal");

        public c(String str) {
            super(e.class, str);
        }

        public Object get(Object obj) {
            return ((CircularRevealWidget) obj).getRevealInfo();
        }

        public void set(Object obj, Object obj2) {
            ((CircularRevealWidget) obj).setRevealInfo((e) obj2);
        }
    }

    public static class d extends Property<c, Integer> {
        public static final Property<c, Integer> a = new d("circularRevealScrimColor");

        public d(String str) {
            super(Integer.class, str);
        }

        public Object get(Object obj) {
            return Integer.valueOf(((CircularRevealWidget) obj).getCircularRevealScrimColor());
        }

        public void set(Object obj, Object obj2) {
            ((CircularRevealWidget) obj).setCircularRevealScrimColor(((Integer) obj2).intValue());
        }
    }

    public static class e {
        public float a;
        public float b;
        public float c;

        public e() {
        }

        public /* synthetic */ e(a aVar) {
        }

        public e(float f2, float f3, float f4) {
            this.a = f2;
            this.b = f3;
            this.c = f4;
        }
    }

    void a();

    void b();

    int getCircularRevealScrimColor();

    e getRevealInfo();

    void setCircularRevealOverlayDrawable(Drawable drawable);

    void setCircularRevealScrimColor(int i2);

    void setRevealInfo(e eVar);
}
