package j.c.a.b.a0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import j.c.a.b.a0.FloatingActionButtonImpl1;

public class FloatingActionButtonImpl extends AnimatorListenerAdapter {
    public boolean a;
    public final /* synthetic */ boolean b;
    public final /* synthetic */ FloatingActionButtonImpl1.f c;
    public final /* synthetic */ FloatingActionButtonImpl1 d;

    public FloatingActionButtonImpl(FloatingActionButtonImpl1 floatingActionButtonImpl1, boolean z, FloatingActionButtonImpl1.f fVar) {
        this.d = floatingActionButtonImpl1;
        this.b = z;
        this.c = fVar;
    }

    public void onAnimationCancel(Animator animator) {
        this.a = true;
    }

    public void onAnimationEnd(Animator animator) {
        FloatingActionButtonImpl1 floatingActionButtonImpl1 = this.d;
        floatingActionButtonImpl1.f2158p = 0;
        floatingActionButtonImpl1.f2153k = null;
        if (!this.a) {
            floatingActionButtonImpl1.f2162t.a(this.b ? 8 : 4, this.b);
            FloatingActionButtonImpl1.f fVar = this.c;
            if (fVar != null) {
                FloatingActionButton floatingActionButton = (FloatingActionButton) fVar;
                floatingActionButton.a.a(floatingActionButton.b);
            }
        }
    }

    public void onAnimationStart(Animator animator) {
        this.d.f2162t.a(0, this.b);
        FloatingActionButtonImpl1 floatingActionButtonImpl1 = this.d;
        floatingActionButtonImpl1.f2158p = 1;
        floatingActionButtonImpl1.f2153k = animator;
        this.a = false;
    }
}
