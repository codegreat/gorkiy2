package j.c.a.b.k0;

import android.view.View;

/* compiled from: DropdownMenuEndIconDelegate */
public class DropdownMenuEndIconDelegate1 implements View.OnFocusChangeListener {
    public final /* synthetic */ DropdownMenuEndIconDelegate b;

    public DropdownMenuEndIconDelegate1(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate) {
        this.b = dropdownMenuEndIconDelegate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, boolean):void
     arg types: [j.c.a.b.k0.DropdownMenuEndIconDelegate, int]
     candidates:
      j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, android.widget.EditText):android.widget.AutoCompleteTextView
      j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, android.widget.AutoCompleteTextView):void
      j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, boolean):void */
    public void onFocusChange(View view, boolean z) {
        this.b.a.setEndIconActivated(z);
        if (!z) {
            DropdownMenuEndIconDelegate.a(this.b, false);
            this.b.g = false;
        }
    }
}
