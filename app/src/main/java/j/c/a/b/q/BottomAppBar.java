package j.c.a.b.q;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

public class BottomAppBar extends AnimatorListenerAdapter {
    public final /* synthetic */ com.google.android.material.bottomappbar.BottomAppBar a;

    public BottomAppBar(com.google.android.material.bottomappbar.BottomAppBar bottomAppBar) {
        this.a = bottomAppBar;
    }

    public void onAnimationEnd(Animator animator) {
        com.google.android.material.bottomappbar.BottomAppBar.a(this.a);
    }

    public void onAnimationStart(Animator animator) {
        this.a.V++;
    }
}
