package j.c.a.b.n;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.OverScroller;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import java.lang.ref.WeakReference;

public abstract class HeaderBehavior<V extends View> extends ViewOffsetBehavior<V> {
    public Runnable d;

    /* renamed from: e  reason: collision with root package name */
    public OverScroller f2297e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f2298f;
    public int g = -1;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f2299i = -1;

    /* renamed from: j  reason: collision with root package name */
    public VelocityTracker f2300j;

    public class a implements Runnable {
        public final CoordinatorLayout b;
        public final V c;

        public a(CoordinatorLayout coordinatorLayout, V v) {
            this.b = coordinatorLayout;
            this.c = v;
        }

        public void run() {
            OverScroller overScroller;
            if (this.c != null && (overScroller = HeaderBehavior.this.f2297e) != null) {
                if (overScroller.computeScrollOffset()) {
                    HeaderBehavior headerBehavior = HeaderBehavior.this;
                    headerBehavior.c(this.b, this.c, headerBehavior.f2297e.getCurrY());
                    this.c.postOnAnimation(this);
                    return;
                }
                HeaderBehavior headerBehavior2 = HeaderBehavior.this;
                CoordinatorLayout coordinatorLayout = this.b;
                V v = this.c;
                AppBarLayout.BaseBehavior baseBehavior = (AppBarLayout.BaseBehavior) headerBehavior2;
                if (baseBehavior != null) {
                    AppBarLayout appBarLayout = (AppBarLayout) v;
                    baseBehavior.a(coordinatorLayout, appBarLayout);
                    if (appBarLayout.f389k) {
                        appBarLayout.a(appBarLayout.a(baseBehavior.a(coordinatorLayout)));
                        return;
                    }
                    return;
                }
                throw null;
            }
        }
    }

    public HeaderBehavior() {
    }

    public boolean a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
        View view2;
        int findPointerIndex;
        if (this.f2299i < 0) {
            this.f2299i = ViewConfiguration.get(coordinatorLayout.getContext()).getScaledTouchSlop();
        }
        if (motionEvent.getActionMasked() == 2 && this.f2298f) {
            int i2 = this.g;
            if (i2 == -1 || (findPointerIndex = motionEvent.findPointerIndex(i2)) == -1) {
                return false;
            }
            int y = (int) motionEvent.getY(findPointerIndex);
            if (Math.abs(y - this.h) > this.f2299i) {
                this.h = y;
                return true;
            }
        }
        if (motionEvent.getActionMasked() == 0) {
            this.g = -1;
            int x = (int) motionEvent.getX();
            int y2 = (int) motionEvent.getY();
            AppBarLayout appBarLayout = (AppBarLayout) view;
            WeakReference<View> weakReference = ((AppBarLayout.BaseBehavior) this).f401q;
            boolean z = (weakReference == null || ((view2 = weakReference.get()) != null && view2.isShown() && !view2.canScrollVertically(-1))) && coordinatorLayout.a(view, x, y2);
            this.f2298f = z;
            if (z) {
                this.h = y2;
                this.g = motionEvent.getPointerId(0);
                if (this.f2300j == null) {
                    this.f2300j = VelocityTracker.obtain();
                }
                OverScroller overScroller = this.f2297e;
                if (overScroller != null && !overScroller.isFinished()) {
                    this.f2297e.abortAnimation();
                    return true;
                }
            }
        }
        VelocityTracker velocityTracker = this.f2300j;
        if (velocityTracker != null) {
            velocityTracker.addMovement(motionEvent);
        }
        return false;
    }

    public abstract int b(CoordinatorLayout coordinatorLayout, V v, int i2, int i3, int i4);

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ee A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(androidx.coordinatorlayout.widget.CoordinatorLayout r21, V r22, android.view.MotionEvent r23) {
        /*
            r20 = this;
            r6 = r20
            r1 = r21
            r2 = r22
            r7 = r23
            int r0 = r23.getActionMasked()
            r3 = 0
            r4 = -1
            r8 = 1
            r9 = 0
            if (r0 == r8) goto L_0x005e
            r5 = 2
            if (r0 == r5) goto L_0x0036
            r1 = 3
            if (r0 == r1) goto L_0x00d5
            r1 = 6
            if (r0 == r1) goto L_0x001c
            goto L_0x005b
        L_0x001c:
            int r0 = r23.getActionIndex()
            if (r0 != 0) goto L_0x0024
            r0 = 1
            goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            int r1 = r7.getPointerId(r0)
            r6.g = r1
            float r0 = r7.getY(r0)
            r1 = 1056964608(0x3f000000, float:0.5)
            float r0 = r0 + r1
            int r0 = (int) r0
            r6.h = r0
            goto L_0x005b
        L_0x0036:
            int r0 = r6.g
            int r0 = r7.findPointerIndex(r0)
            if (r0 != r4) goto L_0x003f
            return r9
        L_0x003f:
            float r0 = r7.getY(r0)
            int r0 = (int) r0
            int r3 = r6.h
            int r3 = r3 - r0
            r6.h = r0
            r0 = r2
            com.google.android.material.appbar.AppBarLayout r0 = (com.google.android.material.appbar.AppBarLayout) r0
            int r0 = r0.getDownNestedScrollRange()
            int r4 = -r0
            r5 = 0
            r0 = r20
            r1 = r21
            r2 = r22
            r0.a(r1, r2, r3, r4, r5)
        L_0x005b:
            r0 = 0
            goto L_0x00e3
        L_0x005e:
            android.view.VelocityTracker r0 = r6.f2300j
            if (r0 == 0) goto L_0x00d5
            r0.addMovement(r7)
            android.view.VelocityTracker r0 = r6.f2300j
            r5 = 1000(0x3e8, float:1.401E-42)
            r0.computeCurrentVelocity(r5)
            android.view.VelocityTracker r0 = r6.f2300j
            int r5 = r6.g
            float r0 = r0.getYVelocity(r5)
            r5 = r2
            com.google.android.material.appbar.AppBarLayout r5 = (com.google.android.material.appbar.AppBarLayout) r5
            int r10 = r5.getTotalScrollRange()
            int r10 = -r10
            r19 = 0
            java.lang.Runnable r11 = r6.d
            if (r11 == 0) goto L_0x0087
            r2.removeCallbacks(r11)
            r6.d = r3
        L_0x0087:
            android.widget.OverScroller r11 = r6.f2297e
            if (r11 != 0) goto L_0x0096
            android.widget.OverScroller r11 = new android.widget.OverScroller
            android.content.Context r12 = r22.getContext()
            r11.<init>(r12)
            r6.f2297e = r11
        L_0x0096:
            android.widget.OverScroller r11 = r6.f2297e
            r12 = 0
            int r13 = r20.b()
            r14 = 0
            int r15 = java.lang.Math.round(r0)
            r16 = 0
            r17 = 0
            r18 = r10
            r11.fling(r12, r13, r14, r15, r16, r17, r18, r19)
            android.widget.OverScroller r0 = r6.f2297e
            boolean r0 = r0.computeScrollOffset()
            if (r0 == 0) goto L_0x00be
            j.c.a.b.n.HeaderBehavior$a r0 = new j.c.a.b.n.HeaderBehavior$a
            r0.<init>(r1, r2)
            r6.d = r0
            i.h.l.ViewCompat.a(r2, r0)
            goto L_0x00d3
        L_0x00be:
            r0 = r6
            com.google.android.material.appbar.AppBarLayout$BaseBehavior r0 = (com.google.android.material.appbar.AppBarLayout.BaseBehavior) r0
            r0.a(r1, r5)
            boolean r2 = r5.f389k
            if (r2 == 0) goto L_0x00d3
            android.view.View r0 = r0.a(r1)
            boolean r0 = r5.a(r0)
            r5.a(r0)
        L_0x00d3:
            r0 = 1
            goto L_0x00d6
        L_0x00d5:
            r0 = 0
        L_0x00d6:
            r6.f2298f = r9
            r6.g = r4
            android.view.VelocityTracker r1 = r6.f2300j
            if (r1 == 0) goto L_0x00e3
            r1.recycle()
            r6.f2300j = r3
        L_0x00e3:
            android.view.VelocityTracker r1 = r6.f2300j
            if (r1 == 0) goto L_0x00ea
            r1.addMovement(r7)
        L_0x00ea:
            boolean r1 = r6.f2298f
            if (r1 != 0) goto L_0x00f2
            if (r0 == 0) goto L_0x00f1
            goto L_0x00f2
        L_0x00f1:
            r8 = 0
        L_0x00f2:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.n.HeaderBehavior.b(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    public abstract int c();

    public int c(CoordinatorLayout coordinatorLayout, View view, int i2) {
        return b(coordinatorLayout, view, i2, RecyclerView.UNDEFINED_DURATION, Integer.MAX_VALUE);
    }

    public HeaderBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final int a(CoordinatorLayout coordinatorLayout, View view, int i2, int i3, int i4) {
        return b(coordinatorLayout, view, c() - i2, i3, i4);
    }
}
