package j.c.a.b.k0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.material.textfield.TextInputLayout;
import i.h.l.ViewCompat;
import j.c.a.a.c.n.c;
import j.c.a.b.d;
import j.c.a.b.m.AnimationUtils;
import java.util.ArrayList;
import java.util.List;

public final class IndicatorViewController {
    public final Context a;
    public final TextInputLayout b;
    public LinearLayout c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public FrameLayout f2275e;

    /* renamed from: f  reason: collision with root package name */
    public int f2276f;
    public Animator g;
    public final float h;

    /* renamed from: i  reason: collision with root package name */
    public int f2277i;

    /* renamed from: j  reason: collision with root package name */
    public int f2278j;

    /* renamed from: k  reason: collision with root package name */
    public CharSequence f2279k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f2280l;

    /* renamed from: m  reason: collision with root package name */
    public TextView f2281m;

    /* renamed from: n  reason: collision with root package name */
    public CharSequence f2282n;

    /* renamed from: o  reason: collision with root package name */
    public int f2283o;

    /* renamed from: p  reason: collision with root package name */
    public ColorStateList f2284p;

    /* renamed from: q  reason: collision with root package name */
    public CharSequence f2285q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f2286r;

    /* renamed from: s  reason: collision with root package name */
    public TextView f2287s;

    /* renamed from: t  reason: collision with root package name */
    public int f2288t;
    public ColorStateList u;
    public Typeface v;

    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ int a;
        public final /* synthetic */ TextView b;
        public final /* synthetic */ int c;
        public final /* synthetic */ TextView d;

        public a(int i2, TextView textView, int i3, TextView textView2) {
            this.a = i2;
            this.b = textView;
            this.c = i3;
            this.d = textView2;
        }

        public void onAnimationEnd(Animator animator) {
            TextView textView;
            IndicatorViewController indicatorViewController = IndicatorViewController.this;
            indicatorViewController.f2277i = this.a;
            indicatorViewController.g = null;
            TextView textView2 = this.b;
            if (textView2 != null) {
                textView2.setVisibility(4);
                if (this.c == 1 && (textView = IndicatorViewController.this.f2281m) != null) {
                    textView.setText((CharSequence) null);
                }
            }
            TextView textView3 = this.d;
            if (textView3 != null) {
                textView3.setTranslationY(0.0f);
                this.d.setAlpha(1.0f);
            }
        }

        public void onAnimationStart(Animator animator) {
            TextView textView = this.d;
            if (textView != null) {
                textView.setVisibility(0);
            }
        }
    }

    public IndicatorViewController(TextInputLayout textInputLayout) {
        Context context = textInputLayout.getContext();
        this.a = context;
        this.b = textInputLayout;
        this.h = (float) context.getResources().getDimensionPixelSize(d.design_textinput_caption_translate_y);
    }

    public final boolean a(TextView textView, CharSequence charSequence) {
        return ViewCompat.w(this.b) && this.b.isEnabled() && (this.f2278j != this.f2277i || textView == null || !TextUtils.equals(textView.getText(), charSequence));
    }

    public void b() {
        Animator animator = this.g;
        if (animator != null) {
            animator.cancel();
        }
    }

    public boolean c() {
        if (this.f2278j != 1 || this.f2281m == null || TextUtils.isEmpty(this.f2279k)) {
            return false;
        }
        return true;
    }

    public int d() {
        TextView textView = this.f2281m;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    public void e() {
        this.f2279k = null;
        b();
        if (this.f2277i == 1) {
            if (!this.f2286r || TextUtils.isEmpty(this.f2285q)) {
                this.f2278j = 0;
            } else {
                this.f2278j = 2;
            }
        }
        a(this.f2277i, this.f2278j, a(this.f2281m, (CharSequence) null));
    }

    public void b(TextView textView, int i2) {
        FrameLayout frameLayout;
        if (this.c != null) {
            if (!(i2 == 0 || i2 == 1) || (frameLayout = this.f2275e) == null) {
                this.c.removeView(textView);
            } else {
                int i3 = this.f2276f - 1;
                this.f2276f = i3;
                if (i3 == 0) {
                    frameLayout.setVisibility(8);
                }
                this.f2275e.removeView(textView);
            }
            int i4 = this.d - 1;
            this.d = i4;
            LinearLayout linearLayout = this.c;
            if (i4 == 0) {
                linearLayout.setVisibility(8);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    public final void a(int i2, int i3, boolean z) {
        TextView a2;
        TextView a3;
        int i4 = i2;
        int i5 = i3;
        boolean z2 = z;
        if (i4 != i5) {
            if (z2) {
                AnimatorSet animatorSet = new AnimatorSet();
                this.g = animatorSet;
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = arrayList;
                int i6 = i2;
                int i7 = i3;
                a(arrayList2, this.f2286r, this.f2287s, 2, i6, i7);
                a(arrayList2, this.f2280l, this.f2281m, 1, i6, i7);
                c.a(animatorSet, arrayList);
                animatorSet.addListener(new a(i3, a(i2), i2, a(i5)));
                animatorSet.start();
            } else if (i4 != i5) {
                if (!(i5 == 0 || (a3 = a(i5)) == null)) {
                    a3.setVisibility(0);
                    a3.setAlpha(1.0f);
                }
                if (!(i4 == 0 || (a2 = a(i2)) == null)) {
                    a2.setVisibility(4);
                    if (i4 == 1) {
                        a2.setText((CharSequence) null);
                    }
                }
                this.f2277i = i5;
            }
            this.b.n();
            this.b.a(z2, false);
            this.b.t();
        }
    }

    public final void a(List<Animator> list, boolean z, TextView textView, int i2, int i3, int i4) {
        if (textView != null && z) {
            if (i2 == i4 || i2 == i3) {
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.ALPHA, i4 == i2 ? 1.0f : 0.0f);
                ofFloat.setDuration(167L);
                ofFloat.setInterpolator(AnimationUtils.a);
                list.add(ofFloat);
                if (i4 == i2) {
                    ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(textView, View.TRANSLATION_Y, -this.h, 0.0f);
                    ofFloat2.setDuration(217L);
                    ofFloat2.setInterpolator(AnimationUtils.d);
                    list.add(ofFloat2);
                }
            }
        }
    }

    public final TextView a(int i2) {
        if (i2 == 1) {
            return this.f2281m;
        }
        if (i2 != 2) {
            return null;
        }
        return this.f2287s;
    }

    public void a() {
        if ((this.c == null || this.b.getEditText() == null) ? false : true) {
            this.c.setPaddingRelative(ViewCompat.o(this.b.getEditText()), 0, this.b.getEditText().getPaddingEnd(), 0);
        }
    }

    public void a(TextView textView, int i2) {
        if (this.c == null && this.f2275e == null) {
            LinearLayout linearLayout = new LinearLayout(this.a);
            this.c = linearLayout;
            linearLayout.setOrientation(0);
            this.b.addView(this.c, -1, -2);
            this.f2275e = new FrameLayout(this.a);
            this.c.addView(this.f2275e, new LinearLayout.LayoutParams(0, -2, 1.0f));
            if (this.b.getEditText() != null) {
                a();
            }
        }
        if (i2 == 0 || i2 == 1) {
            this.f2275e.setVisibility(0);
            this.f2275e.addView(textView);
            this.f2276f++;
        } else {
            this.c.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        }
        this.c.setVisibility(0);
        this.d++;
    }
}
