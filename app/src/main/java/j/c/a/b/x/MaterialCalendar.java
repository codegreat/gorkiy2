package j.c.a.b.x;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;

public final class MaterialCalendar<S> extends PickerFragment<S> {
    public static final Object i0 = "MONTHS_VIEW_GROUP_TAG";
    public static final Object j0 = "NAVIGATION_PREV_TAG";
    public static final Object k0 = "NAVIGATION_NEXT_TAG";
    public static final Object l0 = "SELECTOR_TOGGLE_TAG";
    public int Y;
    public DateSelector<S> Z;
    public CalendarConstraints a0;
    public Month b0;
    public e c0;
    public CalendarStyle d0;
    public RecyclerView e0;
    public RecyclerView f0;
    public View g0;
    public View h0;

    public class a implements Runnable {
        public final /* synthetic */ int b;

        public a(int i2) {
            this.b = i2;
        }

        public void run() {
            MaterialCalendar.this.f0.smoothScrollToPosition(this.b);
        }
    }

    public class b extends AccessibilityDelegateCompat {
        public b(MaterialCalendar materialCalendar) {
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            accessibilityNodeInfoCompat.a((Object) null);
        }
    }

    public class c extends SmoothCalendarLayoutManager {
        public final /* synthetic */ int H;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Context context, int i2, boolean z, int i3) {
            super(context, i2, z);
            this.H = i3;
        }

        public void a(RecyclerView.a0 a0Var, int[] iArr) {
            if (this.H == 0) {
                iArr[0] = MaterialCalendar.this.f0.getWidth();
                iArr[1] = MaterialCalendar.this.f0.getWidth();
                return;
            }
            iArr[0] = MaterialCalendar.this.f0.getHeight();
            iArr[1] = MaterialCalendar.this.f0.getHeight();
        }
    }

    public class d implements f {
        public d() {
        }
    }

    public enum e {
        DAY,
        YEAR
    }

    public interface f {
    }

    public LinearLayoutManager N() {
        return (LinearLayoutManager) this.f0.getLayoutManager();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.<init>(android.content.Context, int, int, boolean):void
     arg types: [android.view.ContextThemeWrapper, int, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.<init>(android.content.Context, android.util.AttributeSet, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.<init>(android.content.Context, int, int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0135, code lost:
        r13 = new i.r.d.LinearSnapHelper();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View a(android.view.LayoutInflater r11, android.view.ViewGroup r12, android.os.Bundle r13) {
        /*
            r10 = this;
            android.view.ContextThemeWrapper r13 = new android.view.ContextThemeWrapper
            android.content.Context r0 = r10.p()
            int r1 = r10.Y
            r13.<init>(r0, r1)
            j.c.a.b.x.CalendarStyle r0 = new j.c.a.b.x.CalendarStyle
            r0.<init>(r13)
            r10.d0 = r0
            android.view.LayoutInflater r11 = r11.cloneInContext(r13)
            j.c.a.b.x.CalendarConstraints r0 = r10.a0
            j.c.a.b.x.Month r0 = r0.b
            boolean r1 = j.c.a.b.x.MaterialDatePicker.b(r13)
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0026
            int r1 = j.c.a.b.h.mtrl_calendar_vertical
            r9 = 1
            goto L_0x0029
        L_0x0026:
            int r1 = j.c.a.b.h.mtrl_calendar_horizontal
            r9 = 0
        L_0x0029:
            android.view.View r11 = r11.inflate(r1, r12, r3)
            int r12 = j.c.a.b.f.mtrl_calendar_days_of_week
            android.view.View r12 = r11.findViewById(r12)
            android.widget.GridView r12 = (android.widget.GridView) r12
            j.c.a.b.x.MaterialCalendar$b r1 = new j.c.a.b.x.MaterialCalendar$b
            r1.<init>(r10)
            i.h.l.ViewCompat.a(r12, r1)
            j.c.a.b.x.DaysOfWeekAdapter r1 = new j.c.a.b.x.DaysOfWeekAdapter
            r1.<init>()
            r12.setAdapter(r1)
            int r0 = r0.f2364f
            r12.setNumColumns(r0)
            r12.setEnabled(r3)
            int r12 = j.c.a.b.f.mtrl_calendar_months
            android.view.View r12 = r11.findViewById(r12)
            androidx.recyclerview.widget.RecyclerView r12 = (androidx.recyclerview.widget.RecyclerView) r12
            r10.f0 = r12
            j.c.a.b.x.MaterialCalendar$c r12 = new j.c.a.b.x.MaterialCalendar$c
            android.content.Context r6 = r10.p()
            r8 = 0
            r4 = r12
            r5 = r10
            r7 = r9
            r4.<init>(r6, r7, r8, r9)
            androidx.recyclerview.widget.RecyclerView r0 = r10.f0
            r0.setLayoutManager(r12)
            androidx.recyclerview.widget.RecyclerView r12 = r10.f0
            java.lang.Object r0 = j.c.a.b.x.MaterialCalendar.i0
            r12.setTag(r0)
            j.c.a.b.x.MonthsPagerAdapter0 r12 = new j.c.a.b.x.MonthsPagerAdapter0
            j.c.a.b.x.DateSelector<S> r0 = r10.Z
            j.c.a.b.x.CalendarConstraints r1 = r10.a0
            j.c.a.b.x.MaterialCalendar$d r4 = new j.c.a.b.x.MaterialCalendar$d
            r4.<init>()
            r12.<init>(r13, r0, r1, r4)
            androidx.recyclerview.widget.RecyclerView r0 = r10.f0
            r0.setAdapter(r12)
            android.content.res.Resources r0 = r13.getResources()
            int r1 = j.c.a.b.g.mtrl_calendar_year_selector_span
            int r0 = r0.getInteger(r1)
            int r1 = j.c.a.b.f.mtrl_calendar_year_selector_frame
            android.view.View r1 = r11.findViewById(r1)
            androidx.recyclerview.widget.RecyclerView r1 = (androidx.recyclerview.widget.RecyclerView) r1
            r10.e0 = r1
            if (r1 == 0) goto L_0x00ba
            r1.setHasFixedSize(r2)
            androidx.recyclerview.widget.RecyclerView r1 = r10.e0
            androidx.recyclerview.widget.GridLayoutManager r4 = new androidx.recyclerview.widget.GridLayoutManager
            r4.<init>(r13, r0, r2, r3)
            r1.setLayoutManager(r4)
            androidx.recyclerview.widget.RecyclerView r0 = r10.e0
            j.c.a.b.x.YearGridAdapter0 r1 = new j.c.a.b.x.YearGridAdapter0
            r1.<init>(r10)
            r0.setAdapter(r1)
            androidx.recyclerview.widget.RecyclerView r0 = r10.e0
            j.c.a.b.x.MaterialCalendar3 r1 = new j.c.a.b.x.MaterialCalendar3
            r1.<init>(r10)
            r0.addItemDecoration(r1)
        L_0x00ba:
            int r0 = j.c.a.b.f.month_navigation_fragment_toggle
            android.view.View r0 = r11.findViewById(r0)
            if (r0 == 0) goto L_0x012f
            int r0 = j.c.a.b.f.month_navigation_fragment_toggle
            android.view.View r0 = r11.findViewById(r0)
            com.google.android.material.button.MaterialButton r0 = (com.google.android.material.button.MaterialButton) r0
            java.lang.Object r1 = j.c.a.b.x.MaterialCalendar.l0
            r0.setTag(r1)
            j.c.a.b.x.MaterialCalendar4 r1 = new j.c.a.b.x.MaterialCalendar4
            r1.<init>(r10)
            i.h.l.ViewCompat.a(r0, r1)
            int r1 = j.c.a.b.f.month_navigation_previous
            android.view.View r1 = r11.findViewById(r1)
            com.google.android.material.button.MaterialButton r1 = (com.google.android.material.button.MaterialButton) r1
            java.lang.Object r2 = j.c.a.b.x.MaterialCalendar.j0
            r1.setTag(r2)
            int r2 = j.c.a.b.f.month_navigation_next
            android.view.View r2 = r11.findViewById(r2)
            com.google.android.material.button.MaterialButton r2 = (com.google.android.material.button.MaterialButton) r2
            java.lang.Object r3 = j.c.a.b.x.MaterialCalendar.k0
            r2.setTag(r3)
            int r3 = j.c.a.b.f.mtrl_calendar_year_selector_frame
            android.view.View r3 = r11.findViewById(r3)
            r10.g0 = r3
            int r3 = j.c.a.b.f.mtrl_calendar_day_selector_frame
            android.view.View r3 = r11.findViewById(r3)
            r10.h0 = r3
            j.c.a.b.x.MaterialCalendar$e r3 = j.c.a.b.x.MaterialCalendar.e.DAY
            r10.a(r3)
            j.c.a.b.x.Month r3 = r10.b0
            java.lang.String r3 = r3.c
            r0.setText(r3)
            androidx.recyclerview.widget.RecyclerView r3 = r10.f0
            j.c.a.b.x.MaterialCalendar5 r4 = new j.c.a.b.x.MaterialCalendar5
            r4.<init>(r10, r12, r0)
            r3.addOnScrollListener(r4)
            j.c.a.b.x.MaterialCalendar0 r3 = new j.c.a.b.x.MaterialCalendar0
            r3.<init>(r10)
            r0.setOnClickListener(r3)
            j.c.a.b.x.MaterialCalendar1 r0 = new j.c.a.b.x.MaterialCalendar1
            r0.<init>(r10, r12)
            r2.setOnClickListener(r0)
            j.c.a.b.x.MaterialCalendar2 r0 = new j.c.a.b.x.MaterialCalendar2
            r0.<init>(r10, r12)
            r1.setOnClickListener(r0)
        L_0x012f:
            boolean r13 = j.c.a.b.x.MaterialDatePicker.b(r13)
            if (r13 != 0) goto L_0x0182
            i.r.d.LinearSnapHelper r13 = new i.r.d.LinearSnapHelper
            r13.<init>()
            androidx.recyclerview.widget.RecyclerView r0 = r10.f0
            androidx.recyclerview.widget.RecyclerView r1 = r13.a
            if (r1 != r0) goto L_0x0141
            goto L_0x0182
        L_0x0141:
            if (r1 == 0) goto L_0x014e
            androidx.recyclerview.widget.RecyclerView$t r2 = r13.c
            r1.removeOnScrollListener(r2)
            androidx.recyclerview.widget.RecyclerView r1 = r13.a
            r2 = 0
            r1.setOnFlingListener(r2)
        L_0x014e:
            r13.a = r0
            if (r0 == 0) goto L_0x0182
            androidx.recyclerview.widget.RecyclerView$r r0 = r0.getOnFlingListener()
            if (r0 != 0) goto L_0x017a
            androidx.recyclerview.widget.RecyclerView r0 = r13.a
            androidx.recyclerview.widget.RecyclerView$t r1 = r13.c
            r0.addOnScrollListener(r1)
            androidx.recyclerview.widget.RecyclerView r0 = r13.a
            r0.setOnFlingListener(r13)
            android.widget.Scroller r0 = new android.widget.Scroller
            androidx.recyclerview.widget.RecyclerView r1 = r13.a
            android.content.Context r1 = r1.getContext()
            android.view.animation.DecelerateInterpolator r2 = new android.view.animation.DecelerateInterpolator
            r2.<init>()
            r0.<init>(r1, r2)
            r13.b = r0
            r13.a()
            goto L_0x0182
        L_0x017a:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "An instance of OnFlingListener already set."
            r11.<init>(r12)
            throw r11
        L_0x0182:
            androidx.recyclerview.widget.RecyclerView r13 = r10.f0
            j.c.a.b.x.Month r0 = r10.b0
            int r12 = r12.a(r0)
            r13.scrollToPosition(r12)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.x.MaterialCalendar.a(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View");
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        if (bundle == null) {
            bundle = this.g;
        }
        this.Y = bundle.getInt("THEME_RES_ID_KEY");
        this.Z = (DateSelector) bundle.getParcelable("GRID_SELECTOR_KEY");
        this.a0 = (CalendarConstraints) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.b0 = (Month) bundle.getParcelable("CURRENT_MONTH_KEY");
    }

    public final void c(int i2) {
        this.f0.post(new a(i2));
    }

    public void d(Bundle bundle) {
        bundle.putInt("THEME_RES_ID_KEY", this.Y);
        bundle.putParcelable("GRID_SELECTOR_KEY", this.Z);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.a0);
        bundle.putParcelable("CURRENT_MONTH_KEY", this.b0);
    }

    public static int b(Context context) {
        return context.getResources().getDimensionPixelSize(j.c.a.b.d.mtrl_calendar_day_height);
    }

    public void a(Month month) {
        MonthsPagerAdapter0 monthsPagerAdapter0 = (MonthsPagerAdapter0) this.f0.getAdapter();
        int b2 = monthsPagerAdapter0.c.b.b(month);
        int a2 = b2 - monthsPagerAdapter0.a(this.b0);
        boolean z = true;
        boolean z2 = Math.abs(a2) > 3;
        if (a2 <= 0) {
            z = false;
        }
        this.b0 = month;
        if (z2 && z) {
            this.f0.scrollToPosition(b2 - 3);
            c(b2);
        } else if (z2) {
            this.f0.scrollToPosition(b2 + 3);
            c(b2);
        } else {
            c(b2);
        }
    }

    public void a(e eVar) {
        this.c0 = eVar;
        if (eVar == e.YEAR) {
            this.e0.getLayoutManager().h(((YearGridAdapter0) this.e0.getAdapter()).b(this.b0.f2363e));
            this.g0.setVisibility(0);
            this.h0.setVisibility(8);
        } else if (eVar == e.DAY) {
            this.g0.setVisibility(8);
            this.h0.setVisibility(0);
            a(this.b0);
        }
    }
}
