package j.c.a.b.o;

import android.graphics.Rect;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;

public class BadgeUtils {
    public static void a(BadgeDrawable badgeDrawable, View view, FrameLayout frameLayout) {
        Rect rect = new Rect();
        view.getDrawingRect(rect);
        badgeDrawable.setBounds(rect);
        badgeDrawable.f2316p = new WeakReference<>(view);
        badgeDrawable.f2317q = new WeakReference<>(frameLayout);
        badgeDrawable.e();
        badgeDrawable.invalidateSelf();
    }
}
