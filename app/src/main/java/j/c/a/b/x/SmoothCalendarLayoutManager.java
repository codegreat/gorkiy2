package j.c.a.b.x;

import android.content.Context;
import android.util.DisplayMetrics;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import i.r.d.LinearSmoothScroller;

public class SmoothCalendarLayoutManager extends LinearLayoutManager {

    public class a extends LinearSmoothScroller {
        public a(SmoothCalendarLayoutManager smoothCalendarLayoutManager, Context context) {
            super(context);
        }

        public float a(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    public SmoothCalendarLayoutManager(Context context, int i2, boolean z) {
        super(i2, z);
    }

    public void a(RecyclerView recyclerView, RecyclerView.a0 a0Var, int i2) {
        a aVar = new a(this, recyclerView.getContext());
        aVar.a = i2;
        a(aVar);
    }
}
