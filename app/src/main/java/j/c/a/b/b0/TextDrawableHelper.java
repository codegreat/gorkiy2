package j.c.a.b.b0;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import j.c.a.b.b0.h;
import j.c.a.b.d0.TextAppearance;
import j.c.a.b.d0.TextAppearance0;
import j.c.a.b.d0.TextAppearanceFontCallback;
import java.lang.ref.WeakReference;

public class TextDrawableHelper {
    public final TextPaint a = new TextPaint(1);
    public final TextAppearanceFontCallback b = new a();
    public float c;
    public boolean d = true;

    /* renamed from: e  reason: collision with root package name */
    public WeakReference<h.b> f2192e = new WeakReference<>(null);

    /* renamed from: f  reason: collision with root package name */
    public TextAppearance f2193f;

    public interface b {
        void a();

        int[] getState();

        boolean onStateChange(int[] iArr);
    }

    public TextDrawableHelper(b bVar) {
        this.f2192e = new WeakReference<>(bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.measureText(java.lang.CharSequence, int, int):float}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{android.graphics.Paint.measureText(java.lang.String, int, int):float}
      ClspMth{android.graphics.Paint.measureText(char[], int, int):float}
      ClspMth{android.graphics.Paint.measureText(java.lang.CharSequence, int, int):float} */
    public float a(String str) {
        float f2;
        if (!this.d) {
            return this.c;
        }
        if (str == null) {
            f2 = 0.0f;
        } else {
            f2 = this.a.measureText((CharSequence) str, 0, str.length());
        }
        this.c = f2;
        this.d = false;
        return f2;
    }

    public class a extends TextAppearanceFontCallback {
        public a() {
        }

        public void a(Typeface typeface, boolean z) {
            if (!z) {
                TextDrawableHelper textDrawableHelper = TextDrawableHelper.this;
                textDrawableHelper.d = true;
                b bVar = textDrawableHelper.f2192e.get();
                if (bVar != null) {
                    bVar.a();
                }
            }
        }

        public void a(int i2) {
            TextDrawableHelper textDrawableHelper = TextDrawableHelper.this;
            textDrawableHelper.d = true;
            b bVar = textDrawableHelper.f2192e.get();
            if (bVar != null) {
                bVar.a();
            }
        }
    }

    public void a(TextAppearance textAppearance, Context context) {
        if (this.f2193f != textAppearance) {
            this.f2193f = textAppearance;
            if (textAppearance != null) {
                TextPaint textPaint = this.a;
                TextAppearanceFontCallback textAppearanceFontCallback = this.b;
                textAppearance.a();
                textAppearance.a(textPaint, textAppearance.f2201l);
                textAppearance.a(context, new TextAppearance0(textAppearance, textPaint, textAppearanceFontCallback));
                b bVar = this.f2192e.get();
                if (bVar != null) {
                    this.a.drawableState = bVar.getState();
                }
                textAppearance.a(context, this.a, this.b);
                this.d = true;
            }
            b bVar2 = this.f2192e.get();
            if (bVar2 != null) {
                bVar2.a();
                bVar2.onStateChange(bVar2.getState());
            }
        }
    }
}
