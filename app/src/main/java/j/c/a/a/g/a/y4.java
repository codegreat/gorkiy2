package j.c.a.a.g.a;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class y4 implements Callable<List<z8>> {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ s4 f2131e;

    public y4(s4 s4Var, d9 d9Var, String str, String str2) {
        this.f2131e = s4Var;
        this.b = d9Var;
        this.c = str;
        this.d = str2;
    }

    public final /* synthetic */ Object call() {
        this.f2131e.a.o();
        return this.f2131e.a.e().a(this.b.b, this.c, this.d);
    }
}
