package j.c.a.a.g.a;

import android.os.Bundle;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a extends c2 {
    public final Map<String, Long> b = new ArrayMap();
    public final Map<String, Integer> c = new ArrayMap();
    public long d;

    public a(r4 r4Var) {
        super(r4Var);
    }

    public final void a(String str, long j2) {
        if (str == null || str.length() == 0) {
            a().f2046f.a("Ad unit id must be a non-empty string");
            return;
        }
        l4 i2 = i();
        b1 b1Var = new b1(this, str, j2);
        i2.o();
        ResourcesFlusher.b(b1Var);
        i2.a((p4<?>) new p4(i2, b1Var, "Task exception on worker thread"));
    }

    public final void b(String str, long j2) {
        if (str == null || str.length() == 0) {
            a().f2046f.a("Ad unit id must be a non-empty string");
            return;
        }
        l4 i2 = i();
        a0 a0Var = new a0(this, str, j2);
        i2.o();
        ResourcesFlusher.b(a0Var);
        i2.a((p4<?>) new p4(i2, a0Var, "Task exception on worker thread"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
     arg types: [j.c.a.a.g.a.w6, android.os.Bundle, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void */
    public final void a(long j2, w6 w6Var) {
        if (w6Var == null) {
            a().f2052n.a("Not logging ad exposure. No active activity");
        } else if (j2 < 1000) {
            a().f2052n.a("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j2));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j2);
            y6.a(w6Var, bundle, true);
            p().a("am", "_xa", bundle);
        }
    }

    public final void b(long j2) {
        for (String put : this.b.keySet()) {
            this.b.put(put, Long.valueOf(j2));
        }
        if (!this.b.isEmpty()) {
            this.d = j2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
     arg types: [j.c.a.a.g.a.w6, android.os.Bundle, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void */
    public final void a(String str, long j2, w6 w6Var) {
        if (w6Var == null) {
            a().f2052n.a("Not logging ad unit exposure. No active activity");
        } else if (j2 < 1000) {
            a().f2052n.a("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j2));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j2);
            y6.a(w6Var, bundle, true);
            p().a("am", "_xu", bundle);
        }
    }

    public final void a(long j2) {
        w6 z = s().z();
        for (String next : this.b.keySet()) {
            a(next, j2 - this.b.get(next).longValue(), z);
        }
        if (!this.b.isEmpty()) {
            a(j2 - this.d, z);
        }
        b(j2);
    }
}
