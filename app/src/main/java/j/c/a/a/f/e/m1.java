package j.c.a.a.f.e;

import java.util.Map;
import javax.annotation.Nullable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class m1 {
    @Nullable
    public final Map<String, Map<String, String>> a;

    public m1(@Nullable Map<String, Map<String, String>> map) {
        this.a = map;
    }
}
