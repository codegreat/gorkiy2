package j.c.a.a.e;

import android.os.IBinder;
import android.os.Parcel;
import j.c.a.a.d.a;
import j.c.a.a.f.c.a;
import j.c.a.a.f.c.c;

public final class i extends a implements h {
    public i(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    public final j.c.a.a.d.a a(j.c.a.a.d.a aVar, String str, int i2, j.c.a.a.d.a aVar2) {
        Parcel a = a();
        c.a(a, aVar);
        a.writeString(str);
        a.writeInt(i2);
        c.a(a, aVar2);
        Parcel a2 = a(2, a);
        j.c.a.a.d.a a3 = a.C0025a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final j.c.a.a.d.a b(j.c.a.a.d.a aVar, String str, int i2, j.c.a.a.d.a aVar2) {
        Parcel a = a();
        c.a(a, aVar);
        a.writeString(str);
        a.writeInt(i2);
        c.a(a, aVar2);
        Parcel a2 = a(3, a);
        j.c.a.a.d.a a3 = a.C0025a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
