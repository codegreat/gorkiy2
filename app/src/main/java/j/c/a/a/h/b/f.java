package j.c.a.a.h.b;

import android.os.IBinder;
import android.os.IInterface;

public final class f implements e, IInterface {
    public final IBinder a;
    public final String b = "com.google.android.gms.signin.internal.ISignInService";

    public f(IBinder iBinder) {
        this.a = iBinder;
    }

    public IBinder asBinder() {
        return this.a;
    }
}
