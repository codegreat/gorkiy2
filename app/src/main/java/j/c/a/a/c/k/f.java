package j.c.a.a.c.k;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import i.b.k.ResourcesFlusher;

public final class f implements Parcelable.Creator<Scope> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = ResourcesFlusher.b(parcel);
        int i2 = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            int i3 = 65535 & readInt;
            if (i3 == 1) {
                i2 = ResourcesFlusher.f(parcel, readInt);
            } else if (i3 != 2) {
                ResourcesFlusher.i(parcel, readInt);
            } else {
                str = ResourcesFlusher.b(parcel, readInt);
            }
        }
        ResourcesFlusher.c(parcel, b);
        return new Scope(i2, str);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new Scope[i2];
    }
}
