package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a7 implements Runnable {
    public final /* synthetic */ w6 b;
    public final /* synthetic */ y6 c;

    public a7(y6 y6Var, w6 w6Var) {
        this.c = y6Var;
        this.b = w6Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
     arg types: [j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void */
    public final void run() {
        y6.a(this.c, this.b, false);
        y6 y6Var = this.c;
        y6Var.c = null;
        z6 r2 = y6Var.r();
        r2.d();
        r2.w();
        r2.a(new h7(r2, null));
    }
}
