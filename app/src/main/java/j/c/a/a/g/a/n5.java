package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class n5 extends o5 {
    public boolean b;

    public n5(r4 r4Var) {
        super(r4Var);
        super.a.D++;
    }

    public void n() {
    }

    public final void o() {
        if (!s()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void p() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!r()) {
            super.a.E.incrementAndGet();
            this.b = true;
        }
    }

    public final void q() {
        if (!this.b) {
            n();
            super.a.E.incrementAndGet();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    public abstract boolean r();

    public final boolean s() {
        return this.b;
    }
}
