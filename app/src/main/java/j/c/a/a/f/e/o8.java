package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class o8 implements p8 {
    public static final p1<Boolean> a;
    public static final p1<Boolean> b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1
     arg types: [j.c.a.a.f.e.v1, java.lang.String, int]
     candidates:
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, long):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, java.lang.String):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1 */
    static {
        v1 v1Var = new v1(q1.a("com.google.android.gms.measurement"));
        a = p1.a(v1Var, "measurement.collection.efficient_engagement_reporting_enabled", false);
        b = p1.a(v1Var, "measurement.collection.redundant_engagement_removal_enabled", false);
    }

    public final boolean a() {
        return a.b().booleanValue();
    }

    public final boolean b() {
        return b.b().booleanValue();
    }
}
