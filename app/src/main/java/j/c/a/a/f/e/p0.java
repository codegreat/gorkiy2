package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class p0 extends x3<p0, a> implements g5 {
    public static final p0 zzf;
    public static volatile m5<p0> zzg;
    public int zzc;
    public int zzd;
    public long zze;

    static {
        p0 p0Var = new p0();
        zzf = p0Var;
        x3.zzd.put(p0.class, p0Var);
    }

    public final boolean a() {
        return (this.zzc & 1) != 0;
    }

    public final boolean i() {
        return (this.zzc & 2) != 0;
    }

    public final long j() {
        return this.zze;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<p0, a> implements g5 {
        public a() {
            super(p0.zzf);
        }

        public /* synthetic */ a(z0 z0Var) {
            super(p0.zzf);
        }
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new p0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                m5<p0> m5Var = zzg;
                if (m5Var == null) {
                    synchronized (p0.class) {
                        m5Var = zzg;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzf);
                            zzg = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
