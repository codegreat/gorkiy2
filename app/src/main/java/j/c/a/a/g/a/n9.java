package j.c.a.a.g.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.c.a.a.c.n.b;
import j.c.a.a.f.e.a0;
import j.c.a.a.f.e.d0;
import j.c.a.a.f.e.j3;
import j.c.a.a.f.e.q0;
import j.c.a.a.f.e.s0;
import j.c.a.a.f.e.u0;
import j.c.a.a.f.e.x3;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class n9 extends o8 {

    /* renamed from: f  reason: collision with root package name */
    public static final String[] f2057f = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    public static final String[] g = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    public static final String[] h = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;"};

    /* renamed from: i  reason: collision with root package name */
    public static final String[] f2058i = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};

    /* renamed from: j  reason: collision with root package name */
    public static final String[] f2059j = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};

    /* renamed from: k  reason: collision with root package name */
    public static final String[] f2060k = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};

    /* renamed from: l  reason: collision with root package name */
    public static final String[] f2061l = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};

    /* renamed from: m  reason: collision with root package name */
    public static final String[] f2062m = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    public final o9 d = new o9(this, this.a.a, "google_app_measurement.db");

    /* renamed from: e  reason: collision with root package name */
    public final k8 f2063e = new k8(this.a.f2092n);

    public n9(q8 q8Var) {
        super(q8Var);
    }

    public final void A() {
        o();
        v().endTransaction();
    }

    public final void B() {
        d();
        o();
        if (y()) {
            long a = l().h.a();
            if (((b) this.a.f2092n) != null) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (Math.abs(elapsedRealtime - a) > k.D.a(null).longValue()) {
                    l().h.a(elapsedRealtime);
                    d();
                    o();
                    if (y()) {
                        SQLiteDatabase v = v();
                        String[] strArr = new String[2];
                        if (((b) this.a.f2092n) != null) {
                            strArr[0] = String.valueOf(System.currentTimeMillis());
                            strArr[1] = String.valueOf(i9.u());
                            int delete = v.delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", strArr);
                            if (delete > 0) {
                                a().f2052n.a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                                return;
                            }
                            return;
                        }
                        throw null;
                    }
                    return;
                }
                return;
            }
            throw null;
        }
    }

    public final long a(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j3 = rawQuery.getLong(0);
                rawQuery.close();
                return j3;
            }
            rawQuery.close();
            return j2;
        } catch (SQLiteException e2) {
            a().f2046f.a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final long b(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j2 = cursor.getLong(0);
                cursor.close();
                return j2;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            a().f2046f.a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.g.a.z8 c(java.lang.String r19, java.lang.String r20) {
        /*
            r18 = this;
            r8 = r20
            i.b.k.ResourcesFlusher.b(r19)
            i.b.k.ResourcesFlusher.b(r20)
            r18.d()
            r18.o()
            r9 = 0
            android.database.sqlite.SQLiteDatabase r10 = r18.v()     // Catch:{ SQLiteException -> 0x007c, all -> 0x0078 }
            java.lang.String r11 = "user_attributes"
            java.lang.String r0 = "set_timestamp"
            java.lang.String r1 = "value"
            java.lang.String r2 = "origin"
            java.lang.String[] r12 = new java.lang.String[]{r0, r1, r2}     // Catch:{ SQLiteException -> 0x007c, all -> 0x0078 }
            java.lang.String r13 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r14 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x007c, all -> 0x0078 }
            r1 = 0
            r14[r1] = r19     // Catch:{ SQLiteException -> 0x007c, all -> 0x0078 }
            r2 = 1
            r14[r2] = r8     // Catch:{ SQLiteException -> 0x007c, all -> 0x0078 }
            r15 = 0
            r16 = 0
            r17 = 0
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0078 }
            boolean r3 = r10.moveToFirst()     // Catch:{ SQLiteException -> 0x0074, all -> 0x0070 }
            if (r3 != 0) goto L_0x003d
            r10.close()
            return r9
        L_0x003d:
            long r5 = r10.getLong(r1)     // Catch:{ SQLiteException -> 0x0074, all -> 0x0070 }
            r11 = r18
            java.lang.Object r7 = r11.a(r10, r2)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r3 = r10.getString(r0)     // Catch:{ SQLiteException -> 0x006e }
            j.c.a.a.g.a.z8 r0 = new j.c.a.a.g.a.z8     // Catch:{ SQLiteException -> 0x006e }
            r1 = r0
            r2 = r19
            r4 = r20
            r1.<init>(r2, r3, r4, r5, r7)     // Catch:{ SQLiteException -> 0x006e }
            boolean r1 = r10.moveToNext()     // Catch:{ SQLiteException -> 0x006e }
            if (r1 == 0) goto L_0x006a
            j.c.a.a.g.a.n3 r1 = r18.a()     // Catch:{ SQLiteException -> 0x006e }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r2 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r19)     // Catch:{ SQLiteException -> 0x006e }
            r1.a(r2, r3)     // Catch:{ SQLiteException -> 0x006e }
        L_0x006a:
            r10.close()
            return r0
        L_0x006e:
            r0 = move-exception
            goto L_0x0080
        L_0x0070:
            r0 = move-exception
            r11 = r18
            goto L_0x009e
        L_0x0074:
            r0 = move-exception
            r11 = r18
            goto L_0x0080
        L_0x0078:
            r0 = move-exception
            r11 = r18
            goto L_0x009f
        L_0x007c:
            r0 = move-exception
            r11 = r18
            r10 = r9
        L_0x0080:
            j.c.a.a.g.a.n3 r1 = r18.a()     // Catch:{ all -> 0x009d }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ all -> 0x009d }
            java.lang.String r2 = "Error querying user property. appId"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r19)     // Catch:{ all -> 0x009d }
            j.c.a.a.g.a.l3 r4 = r18.g()     // Catch:{ all -> 0x009d }
            java.lang.String r4 = r4.c(r8)     // Catch:{ all -> 0x009d }
            r1.a(r2, r3, r4, r0)     // Catch:{ all -> 0x009d }
            if (r10 == 0) goto L_0x009c
            r10.close()
        L_0x009c:
            return r9
        L_0x009d:
            r0 = move-exception
        L_0x009e:
            r9 = r10
        L_0x009f:
            if (r9 == 0) goto L_0x00a4
            r9.close()
        L_0x00a4:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.c(java.lang.String, java.lang.String):j.c.a.a.g.a.z8");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x011d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.g.a.g9 d(java.lang.String r30, java.lang.String r31) {
        /*
            r29 = this;
            r7 = r31
            i.b.k.ResourcesFlusher.b(r30)
            i.b.k.ResourcesFlusher.b(r31)
            r29.d()
            r29.o()
            r8 = 0
            android.database.sqlite.SQLiteDatabase r9 = r29.v()     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f4 }
            java.lang.String r10 = "conditional_properties"
            java.lang.String r11 = "origin"
            java.lang.String r12 = "value"
            java.lang.String r13 = "active"
            java.lang.String r14 = "trigger_event_name"
            java.lang.String r15 = "trigger_timeout"
            java.lang.String r16 = "timed_out_event"
            java.lang.String r17 = "creation_timestamp"
            java.lang.String r18 = "triggered_event"
            java.lang.String r19 = "triggered_timestamp"
            java.lang.String r20 = "time_to_live"
            java.lang.String r21 = "expired_event"
            java.lang.String[] r11 = new java.lang.String[]{r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21}     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f4 }
            java.lang.String r12 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r13 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f4 }
            r1 = 0
            r13[r1] = r30     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f4 }
            r2 = 1
            r13[r2] = r7     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f4 }
            r14 = 0
            r15 = 0
            r16 = 0
            android.database.Cursor r9 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ SQLiteException -> 0x00f8, all -> 0x00f4 }
            boolean r3 = r9.moveToFirst()     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00ec }
            if (r3 != 0) goto L_0x004c
            r9.close()
            return r8
        L_0x004c:
            java.lang.String r16 = r9.getString(r1)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00ec }
            r10 = r29
            java.lang.Object r5 = r10.a(r9, r2)     // Catch:{ SQLiteException -> 0x00ea }
            int r0 = r9.getInt(r0)     // Catch:{ SQLiteException -> 0x00ea }
            if (r0 == 0) goto L_0x005f
            r20 = 1
            goto L_0x0061
        L_0x005f:
            r20 = 0
        L_0x0061:
            r0 = 3
            java.lang.String r21 = r9.getString(r0)     // Catch:{ SQLiteException -> 0x00ea }
            r0 = 4
            long r23 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.u8 r0 = r29.r()     // Catch:{ SQLiteException -> 0x00ea }
            r1 = 5
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00ea }
            android.os.Parcelable$Creator<j.c.a.a.g.a.i> r2 = j.c.a.a.g.a.i.CREATOR     // Catch:{ SQLiteException -> 0x00ea }
            android.os.Parcelable r0 = r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x00ea }
            r22 = r0
            j.c.a.a.g.a.i r22 = (j.c.a.a.g.a.i) r22     // Catch:{ SQLiteException -> 0x00ea }
            r0 = 6
            long r18 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.u8 r0 = r29.r()     // Catch:{ SQLiteException -> 0x00ea }
            r1 = 7
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00ea }
            android.os.Parcelable$Creator<j.c.a.a.g.a.i> r2 = j.c.a.a.g.a.i.CREATOR     // Catch:{ SQLiteException -> 0x00ea }
            android.os.Parcelable r0 = r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x00ea }
            r25 = r0
            j.c.a.a.g.a.i r25 = (j.c.a.a.g.a.i) r25     // Catch:{ SQLiteException -> 0x00ea }
            r0 = 8
            long r3 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00ea }
            r0 = 9
            long r26 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.u8 r0 = r29.r()     // Catch:{ SQLiteException -> 0x00ea }
            r1 = 10
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00ea }
            android.os.Parcelable$Creator<j.c.a.a.g.a.i> r2 = j.c.a.a.g.a.i.CREATOR     // Catch:{ SQLiteException -> 0x00ea }
            android.os.Parcelable r0 = r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x00ea }
            r28 = r0
            j.c.a.a.g.a.i r28 = (j.c.a.a.g.a.i) r28     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.x8 r17 = new j.c.a.a.g.a.x8     // Catch:{ SQLiteException -> 0x00ea }
            r1 = r17
            r2 = r31
            r6 = r16
            r1.<init>(r2, r3, r5, r6)     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.g9 r0 = new j.c.a.a.g.a.g9     // Catch:{ SQLiteException -> 0x00ea }
            r14 = r0
            r15 = r30
            r14.<init>(r15, r16, r17, r18, r20, r21, r22, r23, r25, r26, r28)     // Catch:{ SQLiteException -> 0x00ea }
            boolean r1 = r9.moveToNext()     // Catch:{ SQLiteException -> 0x00ea }
            if (r1 == 0) goto L_0x00e6
            j.c.a.a.g.a.n3 r1 = r29.a()     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ SQLiteException -> 0x00ea }
            java.lang.String r2 = "Got multiple records for conditional property, expected one"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r30)     // Catch:{ SQLiteException -> 0x00ea }
            j.c.a.a.g.a.l3 r4 = r29.g()     // Catch:{ SQLiteException -> 0x00ea }
            java.lang.String r4 = r4.c(r7)     // Catch:{ SQLiteException -> 0x00ea }
            r1.a(r2, r3, r4)     // Catch:{ SQLiteException -> 0x00ea }
        L_0x00e6:
            r9.close()
            return r0
        L_0x00ea:
            r0 = move-exception
            goto L_0x00fc
        L_0x00ec:
            r0 = move-exception
            r10 = r29
            goto L_0x011a
        L_0x00f0:
            r0 = move-exception
            r10 = r29
            goto L_0x00fc
        L_0x00f4:
            r0 = move-exception
            r10 = r29
            goto L_0x011b
        L_0x00f8:
            r0 = move-exception
            r10 = r29
            r9 = r8
        L_0x00fc:
            j.c.a.a.g.a.n3 r1 = r29.a()     // Catch:{ all -> 0x0119 }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ all -> 0x0119 }
            java.lang.String r2 = "Error querying conditional property"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r30)     // Catch:{ all -> 0x0119 }
            j.c.a.a.g.a.l3 r4 = r29.g()     // Catch:{ all -> 0x0119 }
            java.lang.String r4 = r4.c(r7)     // Catch:{ all -> 0x0119 }
            r1.a(r2, r3, r4, r0)     // Catch:{ all -> 0x0119 }
            if (r9 == 0) goto L_0x0118
            r9.close()
        L_0x0118:
            return r8
        L_0x0119:
            r0 = move-exception
        L_0x011a:
            r8 = r9
        L_0x011b:
            if (r8 == 0) goto L_0x0120
            r8.close()
        L_0x0120:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.d(java.lang.String, java.lang.String):j.c.a.a.g.a.g9");
    }

    public final int e(String str, String str2) {
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(str2);
        d();
        o();
        try {
            return v().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            a().f2046f.a("Error deleting conditional property", n3.a(str), g().c(str2), e2);
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<j.c.a.a.f.e.a0>> f(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.o()
            r12.d()
            i.b.k.ResourcesFlusher.b(r13)
            i.b.k.ResourcesFlusher.b(r14)
            i.e.ArrayMap r0 = new i.e.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.v()
            r9 = 0
            java.lang.String r2 = "event_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            java.lang.String r4 = "app_id=? AND event_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            r10 = 0
            r5[r10] = r13     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            r11 = 1
            r5[r11] = r14     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x008a }
            if (r1 != 0) goto L_0x0040
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x008a }
            r14.close()
            return r13
        L_0x0040:
            byte[] r1 = r14.getBlob(r11)     // Catch:{ SQLiteException -> 0x008a }
            j.c.a.a.f.e.j3 r2 = j.c.a.a.f.e.j3.b()     // Catch:{ IOException -> 0x0070 }
            j.c.a.a.f.e.a0 r3 = j.c.a.a.f.e.a0.zzl     // Catch:{ IOException -> 0x0070 }
            j.c.a.a.f.e.x3 r1 = j.c.a.a.f.e.x3.a(r3, r1, r2)     // Catch:{ IOException -> 0x0070 }
            j.c.a.a.f.e.a0 r1 = (j.c.a.a.f.e.a0) r1     // Catch:{ IOException -> 0x0070 }
            int r2 = r14.getInt(r10)     // Catch:{ SQLiteException -> 0x008a }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x008a }
            java.lang.Object r3 = r0.get(r3)     // Catch:{ SQLiteException -> 0x008a }
            java.util.List r3 = (java.util.List) r3     // Catch:{ SQLiteException -> 0x008a }
            if (r3 != 0) goto L_0x006c
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x008a }
            r3.<init>()     // Catch:{ SQLiteException -> 0x008a }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x008a }
            r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x008a }
        L_0x006c:
            r3.add(r1)     // Catch:{ SQLiteException -> 0x008a }
            goto L_0x0080
        L_0x0070:
            r1 = move-exception
            j.c.a.a.g.a.n3 r2 = r12.a()     // Catch:{ SQLiteException -> 0x008a }
            j.c.a.a.g.a.p3 r2 = r2.f2046f     // Catch:{ SQLiteException -> 0x008a }
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r13)     // Catch:{ SQLiteException -> 0x008a }
            r2.a(r3, r4, r1)     // Catch:{ SQLiteException -> 0x008a }
        L_0x0080:
            boolean r1 = r14.moveToNext()     // Catch:{ SQLiteException -> 0x008a }
            if (r1 != 0) goto L_0x0040
            r14.close()
            return r0
        L_0x008a:
            r0 = move-exception
            goto L_0x0090
        L_0x008c:
            r13 = move-exception
            goto L_0x00a7
        L_0x008e:
            r0 = move-exception
            r14 = r9
        L_0x0090:
            j.c.a.a.g.a.n3 r1 = r12.a()     // Catch:{ all -> 0x00a5 }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r13 = j.c.a.a.g.a.n3.a(r13)     // Catch:{ all -> 0x00a5 }
            r1.a(r2, r13, r0)     // Catch:{ all -> 0x00a5 }
            if (r14 == 0) goto L_0x00a4
            r14.close()
        L_0x00a4:
            return r9
        L_0x00a5:
            r13 = move-exception
            r9 = r14
        L_0x00a7:
            if (r9 == 0) goto L_0x00ac
            r9.close()
        L_0x00ac:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.f(java.lang.String, java.lang.String):java.util.Map");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<j.c.a.a.f.e.d0>> g(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.o()
            r12.d()
            i.b.k.ResourcesFlusher.b(r13)
            i.b.k.ResourcesFlusher.b(r14)
            i.e.ArrayMap r0 = new i.e.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.v()
            r9 = 0
            java.lang.String r2 = "property_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            java.lang.String r4 = "app_id=? AND property_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            r10 = 0
            r5[r10] = r13     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            r11 = 1
            r5[r11] = r14     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x008e, all -> 0x008c }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x008a }
            if (r1 != 0) goto L_0x0040
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x008a }
            r14.close()
            return r13
        L_0x0040:
            byte[] r1 = r14.getBlob(r11)     // Catch:{ SQLiteException -> 0x008a }
            j.c.a.a.f.e.j3 r2 = j.c.a.a.f.e.j3.b()     // Catch:{ IOException -> 0x0070 }
            j.c.a.a.f.e.d0 r3 = j.c.a.a.f.e.d0.zzj     // Catch:{ IOException -> 0x0070 }
            j.c.a.a.f.e.x3 r1 = j.c.a.a.f.e.x3.a(r3, r1, r2)     // Catch:{ IOException -> 0x0070 }
            j.c.a.a.f.e.d0 r1 = (j.c.a.a.f.e.d0) r1     // Catch:{ IOException -> 0x0070 }
            int r2 = r14.getInt(r10)     // Catch:{ SQLiteException -> 0x008a }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x008a }
            java.lang.Object r3 = r0.get(r3)     // Catch:{ SQLiteException -> 0x008a }
            java.util.List r3 = (java.util.List) r3     // Catch:{ SQLiteException -> 0x008a }
            if (r3 != 0) goto L_0x006c
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x008a }
            r3.<init>()     // Catch:{ SQLiteException -> 0x008a }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x008a }
            r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x008a }
        L_0x006c:
            r3.add(r1)     // Catch:{ SQLiteException -> 0x008a }
            goto L_0x0080
        L_0x0070:
            r1 = move-exception
            j.c.a.a.g.a.n3 r2 = r12.a()     // Catch:{ SQLiteException -> 0x008a }
            j.c.a.a.g.a.p3 r2 = r2.f2046f     // Catch:{ SQLiteException -> 0x008a }
            java.lang.String r3 = "Failed to merge filter"
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r13)     // Catch:{ SQLiteException -> 0x008a }
            r2.a(r3, r4, r1)     // Catch:{ SQLiteException -> 0x008a }
        L_0x0080:
            boolean r1 = r14.moveToNext()     // Catch:{ SQLiteException -> 0x008a }
            if (r1 != 0) goto L_0x0040
            r14.close()
            return r0
        L_0x008a:
            r0 = move-exception
            goto L_0x0090
        L_0x008c:
            r13 = move-exception
            goto L_0x00a7
        L_0x008e:
            r0 = move-exception
            r14 = r9
        L_0x0090:
            j.c.a.a.g.a.n3 r1 = r12.a()     // Catch:{ all -> 0x00a5 }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r13 = j.c.a.a.g.a.n3.a(r13)     // Catch:{ all -> 0x00a5 }
            r1.a(r2, r13, r0)     // Catch:{ all -> 0x00a5 }
            if (r14 == 0) goto L_0x00a4
            r14.close()
        L_0x00a4:
            return r9
        L_0x00a5:
            r13 = move-exception
            r9 = r14
        L_0x00a7:
            if (r9 == 0) goto L_0x00ac
            r9.close()
        L_0x00ac:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.g(java.lang.String, java.lang.String):java.util.Map");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final long h(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(str2);
        d();
        o();
        SQLiteDatabase v = v();
        v.beginTransaction();
        long j2 = 0;
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str4);
            sb.append(" from app2 where app_id=?");
            try {
                long a = a(sb.toString(), new String[]{str3}, -1);
                if (a == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str3);
                    contentValues.put("first_open_count", (Integer) 0);
                    contentValues.put("previous_install_count", (Integer) 0);
                    if (v.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                        a().f2046f.a("Failed to insert column (got -1). appId", n3.a(str), str4);
                        v.endTransaction();
                        return -1;
                    }
                    a = 0;
                }
                try {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("app_id", str3);
                    contentValues2.put(str4, Long.valueOf(1 + a));
                    if (((long) v.update("app2", contentValues2, "app_id = ?", new String[]{str3})) == 0) {
                        a().f2046f.a("Failed to update column (got 0). appId", n3.a(str), str4);
                        v.endTransaction();
                        return -1;
                    }
                    v.setTransactionSuccessful();
                    v.endTransaction();
                    return a;
                } catch (SQLiteException e2) {
                    e = e2;
                    j2 = a;
                    try {
                        a().f2046f.a("Error inserting column. appId", n3.a(str), str4, e);
                        v.endTransaction();
                        return j2;
                    } catch (Throwable th) {
                        th = th;
                        v.endTransaction();
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                a().f2046f.a("Error inserting column. appId", n3.a(str), str4, e);
                v.endTransaction();
                return j2;
            }
        } catch (SQLiteException e4) {
            e = e4;
            a().f2046f.a("Error inserting column. appId", n3.a(str), str4, e);
            v.endTransaction();
            return j2;
        } catch (Throwable th2) {
            th = th2;
            v.endTransaction();
            throw th;
        }
    }

    public final boolean q() {
        return false;
    }

    public final void u() {
        o();
        v().setTransactionSuccessful();
    }

    public final SQLiteDatabase v() {
        d();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            a().f2047i.a("Error opening database", e2);
            throw e2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String w() {
        /*
            r6 = this;
            android.database.sqlite.SQLiteDatabase r0 = r6.v()
            r1 = 0
            java.lang.String r2 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch:{ SQLiteException -> 0x0025, all -> 0x0020 }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x001e }
            if (r2 == 0) goto L_0x001a
            r2 = 0
            java.lang.String r1 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x001e }
            r0.close()
            return r1
        L_0x001a:
            r0.close()
            return r1
        L_0x001e:
            r2 = move-exception
            goto L_0x0027
        L_0x0020:
            r0 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0039
        L_0x0025:
            r2 = move-exception
            r0 = r1
        L_0x0027:
            j.c.a.a.g.a.n3 r3 = r6.a()     // Catch:{ all -> 0x0038 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ all -> 0x0038 }
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.a(r4, r2)     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0037
            r0.close()
        L_0x0037:
            return r1
        L_0x0038:
            r1 = move-exception
        L_0x0039:
            if (r0 == 0) goto L_0x003e
            r0.close()
        L_0x003e:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.w():java.lang.String");
    }

    public final long x() {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
            if (!cursor.moveToFirst()) {
                cursor.close();
                return -1;
            }
            long j2 = cursor.getLong(0);
            cursor.close();
            return j2;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error querying raw events", e2);
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final boolean y() {
        return this.a.a.getDatabasePath("google_app_measurement.db").exists();
    }

    public final void z() {
        o();
        v().beginTransaction();
    }

    /* JADX WARNING: Removed duplicated region for block: B:66:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x015d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.g.a.e a(java.lang.String r27, java.lang.String r28) {
        /*
            r26 = this;
            r15 = r27
            r14 = r28
            i.b.k.ResourcesFlusher.b(r27)
            i.b.k.ResourcesFlusher.b(r28)
            r26.d()
            r26.o()
            r12 = r26
            j.c.a.a.g.a.r4 r0 = r12.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.x0
            boolean r0 = r0.d(r15, r1)
            java.util.ArrayList r1 = new java.util.ArrayList
            java.lang.String r2 = "lifetime_count"
            java.lang.String r3 = "current_bundle_count"
            java.lang.String r4 = "last_fire_timestamp"
            java.lang.String r5 = "last_bundled_timestamp"
            java.lang.String r6 = "last_bundled_day"
            java.lang.String r7 = "last_sampled_complex_event_id"
            java.lang.String r8 = "last_sampling_rate"
            java.lang.String r9 = "last_exempt_from_sampling"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3, r4, r5, r6, r7, r8, r9}
            java.util.List r2 = java.util.Arrays.asList(r2)
            r1.<init>(r2)
            if (r0 == 0) goto L_0x0040
            java.lang.String r2 = "current_session_count"
            r1.add(r2)
        L_0x0040:
            r18 = 0
            android.database.sqlite.SQLiteDatabase r2 = r26.v()     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            java.lang.String r3 = "events"
            r10 = 0
            java.lang.String[] r4 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            java.lang.Object[] r1 = r1.toArray(r4)     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            r4 = r1
            java.lang.String[] r4 = (java.lang.String[]) r4     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            java.lang.String r5 = "app_id=? and name=?"
            r1 = 2
            java.lang.String[] r6 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            r6[r10] = r15     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            r11 = 1
            r6[r11] = r14     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r13 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0136, all -> 0x0134 }
            boolean r2 = r13.moveToFirst()     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 != 0) goto L_0x006d
            r13.close()
            return r18
        L_0x006d:
            long r4 = r13.getLong(r10)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            long r6 = r13.getLong(r11)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            long r16 = r13.getLong(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r1 = 3
            boolean r2 = r13.isNull(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r8 = 0
            if (r2 == 0) goto L_0x0085
            r19 = r8
            goto L_0x008b
        L_0x0085:
            long r1 = r13.getLong(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r19 = r1
        L_0x008b:
            r1 = 4
            boolean r2 = r13.isNull(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x0095
            r21 = r18
            goto L_0x009f
        L_0x0095:
            long r1 = r13.getLong(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r21 = r1
        L_0x009f:
            r1 = 5
            boolean r2 = r13.isNull(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x00a9
            r22 = r18
            goto L_0x00b3
        L_0x00a9:
            long r1 = r13.getLong(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r22 = r1
        L_0x00b3:
            r1 = 6
            boolean r2 = r13.isNull(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 == 0) goto L_0x00bd
            r23 = r18
            goto L_0x00c7
        L_0x00bd:
            long r1 = r13.getLong(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r23 = r1
        L_0x00c7:
            r1 = 7
            boolean r2 = r13.isNull(r1)     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            if (r2 != 0) goto L_0x00e2
            long r1 = r13.getLong(r1)     // Catch:{ SQLiteException -> 0x00e0 }
            r24 = 1
            int r3 = (r1 > r24 ? 1 : (r1 == r24 ? 0 : -1))
            if (r3 != 0) goto L_0x00d9
            r10 = 1
        L_0x00d9:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r10)     // Catch:{ SQLiteException -> 0x00e0 }
            r24 = r1
            goto L_0x00e4
        L_0x00e0:
            r0 = move-exception
            goto L_0x0139
        L_0x00e2:
            r24 = r18
        L_0x00e4:
            if (r0 == 0) goto L_0x00f3
            r0 = 8
            boolean r1 = r13.isNull(r0)     // Catch:{ SQLiteException -> 0x00e0 }
            if (r1 != 0) goto L_0x00f3
            long r0 = r13.getLong(r0)     // Catch:{ SQLiteException -> 0x00e0 }
            r8 = r0
        L_0x00f3:
            j.c.a.a.g.a.e r0 = new j.c.a.a.g.a.e     // Catch:{ SQLiteException -> 0x0130, all -> 0x012a }
            r1 = r0
            r2 = r27
            r3 = r28
            r10 = r16
            r25 = r13
            r12 = r19
            r14 = r21
            r15 = r22
            r16 = r23
            r17 = r24
            r1.<init>(r2, r3, r4, r6, r8, r10, r12, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0124 }
            boolean r1 = r25.moveToNext()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0124 }
            if (r1 == 0) goto L_0x0120
            j.c.a.a.g.a.n3 r1 = r26.a()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0124 }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ SQLiteException -> 0x0126, all -> 0x0124 }
            java.lang.String r2 = "Got multiple records for event aggregates, expected one. appId"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r27)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0124 }
            r1.a(r2, r3)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0124 }
        L_0x0120:
            r25.close()
            return r0
        L_0x0124:
            r0 = move-exception
            goto L_0x012d
        L_0x0126:
            r0 = move-exception
            r13 = r25
            goto L_0x0139
        L_0x012a:
            r0 = move-exception
            r25 = r13
        L_0x012d:
            r18 = r25
            goto L_0x015b
        L_0x0130:
            r0 = move-exception
            r25 = r13
            goto L_0x0139
        L_0x0134:
            r0 = move-exception
            goto L_0x015b
        L_0x0136:
            r0 = move-exception
            r13 = r18
        L_0x0139:
            j.c.a.a.g.a.n3 r1 = r26.a()     // Catch:{ all -> 0x0158 }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ all -> 0x0158 }
            java.lang.String r2 = "Error querying events. appId"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r27)     // Catch:{ all -> 0x0158 }
            j.c.a.a.g.a.l3 r4 = r26.g()     // Catch:{ all -> 0x0158 }
            r5 = r28
            java.lang.String r4 = r4.a(r5)     // Catch:{ all -> 0x0158 }
            r1.a(r2, r3, r4, r0)     // Catch:{ all -> 0x0158 }
            if (r13 == 0) goto L_0x0157
            r13.close()
        L_0x0157:
            return r18
        L_0x0158:
            r0 = move-exception
            r18 = r13
        L_0x015b:
            if (r18 == 0) goto L_0x0160
            r18.close()
        L_0x0160:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.a(java.lang.String, java.lang.String):j.c.a.a.g.a.e");
    }

    public final void b(String str, String str2) {
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(str2);
        d();
        o();
        try {
            a().f2052n.a("Deleted user attribute rows", Integer.valueOf(v().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e2) {
            a().f2046f.a("Error deleting user attribute. appId", n3.a(str), g().c(str2), e2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, j.c.a.a.f.e.w0> e(java.lang.String r12) {
        /*
            r11 = this;
            r11.o()
            r11.d()
            i.b.k.ResourcesFlusher.b(r12)
            android.database.sqlite.SQLiteDatabase r0 = r11.v()
            r8 = 0
            java.lang.String r1 = "audience_filter_values"
            java.lang.String r2 = "audience_id"
            java.lang.String r3 = "current_results"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3}     // Catch:{ SQLiteException -> 0x0074, all -> 0x0072 }
            java.lang.String r3 = "app_id=?"
            r9 = 1
            java.lang.String[] r4 = new java.lang.String[r9]     // Catch:{ SQLiteException -> 0x0074, all -> 0x0072 }
            r10 = 0
            r4[r10] = r12     // Catch:{ SQLiteException -> 0x0074, all -> 0x0072 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0074, all -> 0x0072 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0070 }
            if (r1 != 0) goto L_0x0031
            r0.close()
            return r8
        L_0x0031:
            i.e.ArrayMap r1 = new i.e.ArrayMap     // Catch:{ SQLiteException -> 0x0070 }
            r1.<init>()     // Catch:{ SQLiteException -> 0x0070 }
        L_0x0036:
            int r2 = r0.getInt(r10)     // Catch:{ SQLiteException -> 0x0070 }
            byte[] r3 = r0.getBlob(r9)     // Catch:{ SQLiteException -> 0x0070 }
            j.c.a.a.f.e.j3 r4 = j.c.a.a.f.e.j3.b()     // Catch:{ IOException -> 0x0052 }
            j.c.a.a.f.e.w0 r5 = j.c.a.a.f.e.w0.zzg     // Catch:{ IOException -> 0x0052 }
            j.c.a.a.f.e.x3 r3 = j.c.a.a.f.e.x3.a(r5, r3, r4)     // Catch:{ IOException -> 0x0052 }
            j.c.a.a.f.e.w0 r3 = (j.c.a.a.f.e.w0) r3     // Catch:{ IOException -> 0x0052 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0070 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0070 }
            goto L_0x0066
        L_0x0052:
            r3 = move-exception
            j.c.a.a.g.a.n3 r4 = r11.a()     // Catch:{ SQLiteException -> 0x0070 }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ SQLiteException -> 0x0070 }
            java.lang.String r5 = "Failed to merge filter results. appId, audienceId, error"
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r12)     // Catch:{ SQLiteException -> 0x0070 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0070 }
            r4.a(r5, r6, r2, r3)     // Catch:{ SQLiteException -> 0x0070 }
        L_0x0066:
            boolean r2 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0070 }
            if (r2 != 0) goto L_0x0036
            r0.close()
            return r1
        L_0x0070:
            r1 = move-exception
            goto L_0x0076
        L_0x0072:
            r12 = move-exception
            goto L_0x008d
        L_0x0074:
            r1 = move-exception
            r0 = r8
        L_0x0076:
            j.c.a.a.g.a.n3 r2 = r11.a()     // Catch:{ all -> 0x008b }
            j.c.a.a.g.a.p3 r2 = r2.f2046f     // Catch:{ all -> 0x008b }
            java.lang.String r3 = "Database error querying filter results. appId"
            java.lang.Object r12 = j.c.a.a.g.a.n3.a(r12)     // Catch:{ all -> 0x008b }
            r2.a(r3, r12, r1)     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x008a
            r0.close()
        L_0x008a:
            return r8
        L_0x008b:
            r12 = move-exception
            r8 = r0
        L_0x008d:
            if (r8 == 0) goto L_0x0092
            r8.close()
        L_0x0092:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.e(java.lang.String):java.util.Map");
    }

    public final List<g9> b(String str, String str2, String str3) {
        ResourcesFlusher.b(str);
        d();
        o();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return a(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    public final long c(String str) {
        ResourcesFlusher.b(str);
        d();
        o();
        try {
            return (long) v().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, this.a.g.b(str, k.u))))});
        } catch (SQLiteException e2) {
            a().f2046f.a("Error deleting over the limit events. appId", n3.a(str), e2);
            return 0;
        }
    }

    public final long f(String str) {
        ResourcesFlusher.b(str);
        return a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<java.lang.Integer>> d(java.lang.String r8) {
        /*
            r7 = this;
            r7.o()
            r7.d()
            i.b.k.ResourcesFlusher.b(r8)
            i.e.ArrayMap r0 = new i.e.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r7.v()
            r2 = 0
            java.lang.String r3 = "select audience_id, filter_id from event_filters where app_id = ? and session_scoped = 1 UNION select audience_id, filter_id from property_filters where app_id = ? and session_scoped = 1;"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0065, all -> 0x0063 }
            r5 = 0
            r4[r5] = r8     // Catch:{ SQLiteException -> 0x0065, all -> 0x0063 }
            r6 = 1
            r4[r6] = r8     // Catch:{ SQLiteException -> 0x0065, all -> 0x0063 }
            android.database.Cursor r1 = r1.rawQuery(r3, r4)     // Catch:{ SQLiteException -> 0x0065, all -> 0x0063 }
            boolean r3 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0061 }
            if (r3 != 0) goto L_0x0030
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0061 }
            r1.close()
            return r8
        L_0x0030:
            int r3 = r1.getInt(r5)     // Catch:{ SQLiteException -> 0x0061 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0061 }
            java.lang.Object r4 = r0.get(r4)     // Catch:{ SQLiteException -> 0x0061 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ SQLiteException -> 0x0061 }
            if (r4 != 0) goto L_0x004c
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0061 }
            r4.<init>()     // Catch:{ SQLiteException -> 0x0061 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0061 }
            r0.put(r3, r4)     // Catch:{ SQLiteException -> 0x0061 }
        L_0x004c:
            int r3 = r1.getInt(r6)     // Catch:{ SQLiteException -> 0x0061 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0061 }
            r4.add(r3)     // Catch:{ SQLiteException -> 0x0061 }
            boolean r3 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0061 }
            if (r3 != 0) goto L_0x0030
            r1.close()
            return r0
        L_0x0061:
            r0 = move-exception
            goto L_0x0067
        L_0x0063:
            r8 = move-exception
            goto L_0x007e
        L_0x0065:
            r0 = move-exception
            r1 = r2
        L_0x0067:
            j.c.a.a.g.a.n3 r3 = r7.a()     // Catch:{ all -> 0x007c }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ all -> 0x007c }
            java.lang.String r4 = "Database error querying scoped filters. appId"
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ all -> 0x007c }
            r3.a(r4, r8, r0)     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x007b
            r1.close()
        L_0x007b:
            return r2
        L_0x007c:
            r8 = move-exception
            r2 = r1
        L_0x007e:
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.d(java.lang.String):java.util.Map");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ee A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00f0 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x010d A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x010f A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x012c A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x012e A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x014b A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x014d A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x016d A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0171 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0199 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x019b A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01b8 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01ba A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01d3 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01d5 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01e4 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01f9 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0215 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0216 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0225 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0248 A[Catch:{ SQLiteException -> 0x025b }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0286  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.g.a.e4 b(java.lang.String r34) {
        /*
            r33 = this;
            r1 = r34
            i.b.k.ResourcesFlusher.b(r34)
            r33.d()
            r33.o()
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r33.v()     // Catch:{ SQLiteException -> 0x0269, all -> 0x0265 }
            java.lang.String r4 = "apps"
            java.lang.String r5 = "app_instance_id"
            java.lang.String r6 = "gmp_app_id"
            java.lang.String r7 = "resettable_device_id_hash"
            java.lang.String r8 = "last_bundle_index"
            java.lang.String r9 = "last_bundle_start_timestamp"
            java.lang.String r10 = "last_bundle_end_timestamp"
            java.lang.String r11 = "app_version"
            java.lang.String r12 = "app_store"
            java.lang.String r13 = "gmp_version"
            java.lang.String r14 = "dev_cert_hash"
            java.lang.String r15 = "measurement_enabled"
            java.lang.String r16 = "day"
            java.lang.String r17 = "daily_public_events_count"
            java.lang.String r18 = "daily_events_count"
            java.lang.String r19 = "daily_conversions_count"
            java.lang.String r20 = "config_fetched_time"
            java.lang.String r21 = "failed_config_fetch_time"
            java.lang.String r22 = "app_version_int"
            java.lang.String r23 = "firebase_instance_id"
            java.lang.String r24 = "daily_error_events_count"
            java.lang.String r25 = "daily_realtime_events_count"
            java.lang.String r26 = "health_monitor_sample"
            java.lang.String r27 = "android_id"
            java.lang.String r28 = "adid_reporting_enabled"
            java.lang.String r29 = "ssaid_reporting_enabled"
            java.lang.String r30 = "admob_app_id"
            java.lang.String r31 = "dynamite_version"
            java.lang.String r32 = "safelisted_events"
            java.lang.String[] r5 = new java.lang.String[]{r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32}     // Catch:{ SQLiteException -> 0x0269, all -> 0x0265 }
            java.lang.String r6 = "app_id=?"
            r0 = 1
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0269, all -> 0x0265 }
            r11 = 0
            r7[r11] = r1     // Catch:{ SQLiteException -> 0x0269, all -> 0x0265 }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0269, all -> 0x0265 }
            boolean r4 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0261, all -> 0x025d }
            if (r4 != 0) goto L_0x0067
            r3.close()
            return r2
        L_0x0067:
            j.c.a.a.g.a.e4 r4 = new j.c.a.a.g.a.e4     // Catch:{ SQLiteException -> 0x0261, all -> 0x025d }
            r5 = r33
            j.c.a.a.g.a.q8 r6 = r5.b     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r6 = r6.f2073i     // Catch:{ SQLiteException -> 0x025b }
            r4.<init>(r6, r1)     // Catch:{ SQLiteException -> 0x025b }
            java.lang.String r6 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x025b }
            r4.a(r6)     // Catch:{ SQLiteException -> 0x025b }
            java.lang.String r6 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x025b }
            r4.b(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 2
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.d(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 3
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.g(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 4
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.a(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 5
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.b(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 6
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.f(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 7
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.g(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 8
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.d(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 9
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.e(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 10
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r7 != 0) goto L_0x00d3
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r6 == 0) goto L_0x00d1
            goto L_0x00d3
        L_0x00d1:
            r6 = 0
            goto L_0x00d4
        L_0x00d3:
            r6 = 1
        L_0x00d4:
            r4.a(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 11
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r8 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r8 = r8.i()     // Catch:{ SQLiteException -> 0x025b }
            r8.d()     // Catch:{ SQLiteException -> 0x025b }
            boolean r8 = r4.D     // Catch:{ SQLiteException -> 0x025b }
            long r9 = r4.w     // Catch:{ SQLiteException -> 0x025b }
            int r12 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r12 == 0) goto L_0x00f0
            r9 = 1
            goto L_0x00f1
        L_0x00f0:
            r9 = 0
        L_0x00f1:
            r8 = r8 | r9
            r4.D = r8     // Catch:{ SQLiteException -> 0x025b }
            r4.w = r6     // Catch:{ SQLiteException -> 0x025b }
            r6 = 12
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r8 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r8 = r8.i()     // Catch:{ SQLiteException -> 0x025b }
            r8.d()     // Catch:{ SQLiteException -> 0x025b }
            boolean r8 = r4.D     // Catch:{ SQLiteException -> 0x025b }
            long r9 = r4.x     // Catch:{ SQLiteException -> 0x025b }
            int r12 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r12 == 0) goto L_0x010f
            r9 = 1
            goto L_0x0110
        L_0x010f:
            r9 = 0
        L_0x0110:
            r8 = r8 | r9
            r4.D = r8     // Catch:{ SQLiteException -> 0x025b }
            r4.x = r6     // Catch:{ SQLiteException -> 0x025b }
            r6 = 13
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r8 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r8 = r8.i()     // Catch:{ SQLiteException -> 0x025b }
            r8.d()     // Catch:{ SQLiteException -> 0x025b }
            boolean r8 = r4.D     // Catch:{ SQLiteException -> 0x025b }
            long r9 = r4.y     // Catch:{ SQLiteException -> 0x025b }
            int r12 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r12 == 0) goto L_0x012e
            r9 = 1
            goto L_0x012f
        L_0x012e:
            r9 = 0
        L_0x012f:
            r8 = r8 | r9
            r4.D = r8     // Catch:{ SQLiteException -> 0x025b }
            r4.y = r6     // Catch:{ SQLiteException -> 0x025b }
            r6 = 14
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r8 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r8 = r8.i()     // Catch:{ SQLiteException -> 0x025b }
            r8.d()     // Catch:{ SQLiteException -> 0x025b }
            boolean r8 = r4.D     // Catch:{ SQLiteException -> 0x025b }
            long r9 = r4.z     // Catch:{ SQLiteException -> 0x025b }
            int r12 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r12 == 0) goto L_0x014d
            r9 = 1
            goto L_0x014e
        L_0x014d:
            r9 = 0
        L_0x014e:
            r8 = r8 | r9
            r4.D = r8     // Catch:{ SQLiteException -> 0x025b }
            r4.z = r6     // Catch:{ SQLiteException -> 0x025b }
            r6 = 15
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.h(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 16
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.i(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 17
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r7 == 0) goto L_0x0171
            r6 = -2147483648(0xffffffff80000000, double:NaN)
            goto L_0x0176
        L_0x0171:
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x025b }
            long r6 = (long) r6     // Catch:{ SQLiteException -> 0x025b }
        L_0x0176:
            r4.c(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 18
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.e(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 19
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r8 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r8 = r8.i()     // Catch:{ SQLiteException -> 0x025b }
            r8.d()     // Catch:{ SQLiteException -> 0x025b }
            boolean r8 = r4.D     // Catch:{ SQLiteException -> 0x025b }
            long r9 = r4.A     // Catch:{ SQLiteException -> 0x025b }
            int r12 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r12 == 0) goto L_0x019b
            r9 = 1
            goto L_0x019c
        L_0x019b:
            r9 = 0
        L_0x019c:
            r8 = r8 | r9
            r4.D = r8     // Catch:{ SQLiteException -> 0x025b }
            r4.A = r6     // Catch:{ SQLiteException -> 0x025b }
            r6 = 20
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.r4 r8 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r8 = r8.i()     // Catch:{ SQLiteException -> 0x025b }
            r8.d()     // Catch:{ SQLiteException -> 0x025b }
            boolean r8 = r4.D     // Catch:{ SQLiteException -> 0x025b }
            long r9 = r4.B     // Catch:{ SQLiteException -> 0x025b }
            int r12 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r12 == 0) goto L_0x01ba
            r9 = 1
            goto L_0x01bb
        L_0x01ba:
            r9 = 0
        L_0x01bb:
            r8 = r8 | r9
            r4.D = r8     // Catch:{ SQLiteException -> 0x025b }
            r4.B = r6     // Catch:{ SQLiteException -> 0x025b }
            r6 = 21
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x025b }
            r4.h(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 22
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x025b }
            r8 = 0
            if (r7 == 0) goto L_0x01d5
            r6 = r8
            goto L_0x01d9
        L_0x01d5:
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x025b }
        L_0x01d9:
            r4.j(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 23
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r7 != 0) goto L_0x01ed
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r6 == 0) goto L_0x01eb
            goto L_0x01ed
        L_0x01eb:
            r6 = 0
            goto L_0x01ee
        L_0x01ed:
            r6 = 1
        L_0x01ee:
            r4.b(r6)     // Catch:{ SQLiteException -> 0x025b }
            r6 = 24
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r7 != 0) goto L_0x0201
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x025b }
            if (r6 == 0) goto L_0x0200
            goto L_0x0201
        L_0x0200:
            r0 = 0
        L_0x0201:
            r4.c(r0)     // Catch:{ SQLiteException -> 0x025b }
            r0 = 25
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x025b }
            r4.c(r0)     // Catch:{ SQLiteException -> 0x025b }
            r0 = 26
            boolean r6 = r3.isNull(r0)     // Catch:{ SQLiteException -> 0x025b }
            if (r6 == 0) goto L_0x0216
            goto L_0x021a
        L_0x0216:
            long r8 = r3.getLong(r0)     // Catch:{ SQLiteException -> 0x025b }
        L_0x021a:
            r4.f(r8)     // Catch:{ SQLiteException -> 0x025b }
            r0 = 27
            boolean r6 = r3.isNull(r0)     // Catch:{ SQLiteException -> 0x025b }
            if (r6 != 0) goto L_0x0237
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x025b }
            java.lang.String r6 = ","
            r7 = -1
            java.lang.String[] r0 = r0.split(r6, r7)     // Catch:{ SQLiteException -> 0x025b }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ SQLiteException -> 0x025b }
            r4.a(r0)     // Catch:{ SQLiteException -> 0x025b }
        L_0x0237:
            j.c.a.a.g.a.r4 r0 = r4.a     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.l4 r0 = r0.i()     // Catch:{ SQLiteException -> 0x025b }
            r0.d()     // Catch:{ SQLiteException -> 0x025b }
            r4.D = r11     // Catch:{ SQLiteException -> 0x025b }
            boolean r0 = r3.moveToNext()     // Catch:{ SQLiteException -> 0x025b }
            if (r0 == 0) goto L_0x0257
            j.c.a.a.g.a.n3 r0 = r33.a()     // Catch:{ SQLiteException -> 0x025b }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ SQLiteException -> 0x025b }
            java.lang.String r6 = "Got multiple records for app, expected one. appId"
            java.lang.Object r7 = j.c.a.a.g.a.n3.a(r34)     // Catch:{ SQLiteException -> 0x025b }
            r0.a(r6, r7)     // Catch:{ SQLiteException -> 0x025b }
        L_0x0257:
            r3.close()
            return r4
        L_0x025b:
            r0 = move-exception
            goto L_0x026d
        L_0x025d:
            r0 = move-exception
            r5 = r33
            goto L_0x0283
        L_0x0261:
            r0 = move-exception
            r5 = r33
            goto L_0x026d
        L_0x0265:
            r0 = move-exception
            r5 = r33
            goto L_0x0284
        L_0x0269:
            r0 = move-exception
            r5 = r33
            r3 = r2
        L_0x026d:
            j.c.a.a.g.a.n3 r4 = r33.a()     // Catch:{ all -> 0x0282 }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ all -> 0x0282 }
            java.lang.String r6 = "Error querying app. appId"
            java.lang.Object r1 = j.c.a.a.g.a.n3.a(r34)     // Catch:{ all -> 0x0282 }
            r4.a(r6, r1, r0)     // Catch:{ all -> 0x0282 }
            if (r3 == 0) goto L_0x0281
            r3.close()
        L_0x0281:
            return r2
        L_0x0282:
            r0 = move-exception
        L_0x0283:
            r2 = r3
        L_0x0284:
            if (r2 == 0) goto L_0x0289
            r2.close()
        L_0x0289:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.b(java.lang.String):j.c.a.a.g.a.e4");
    }

    public final void a(e eVar) {
        ResourcesFlusher.b(eVar);
        d();
        o();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", eVar.a);
        contentValues.put(DefaultAppMeasurementEventListenerRegistrar.NAME, eVar.b);
        contentValues.put("lifetime_count", Long.valueOf(eVar.c));
        contentValues.put("current_bundle_count", Long.valueOf(eVar.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(eVar.f1966f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(eVar.g));
        contentValues.put("last_bundled_day", eVar.h);
        contentValues.put("last_sampled_complex_event_id", eVar.f1967i);
        contentValues.put("last_sampling_rate", eVar.f1968j);
        if (this.a.g.d(eVar.a, k.x0)) {
            contentValues.put("current_session_count", Long.valueOf(eVar.f1965e));
        }
        Boolean bool = eVar.f1969k;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (v().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                a().f2046f.a("Failed to insert/update event aggregates (got -1). appId", n3.a(eVar.a));
            }
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing event aggregates. appId", n3.a(eVar.a), e2);
        }
    }

    public final boolean a(z8 z8Var) {
        ResourcesFlusher.b(z8Var);
        d();
        o();
        if (c(z8Var.a, z8Var.c) == null) {
            if (y8.e(z8Var.c)) {
                if (b("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{z8Var.a}) >= 25) {
                    return false;
                }
            } else if (!this.a.g.d(z8Var.a, k.i0)) {
                if (b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{z8Var.a, z8Var.b}) >= 25) {
                    return false;
                }
            } else if (!"_npa".equals(z8Var.c)) {
                if (b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{z8Var.a, z8Var.b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", z8Var.a);
        contentValues.put("origin", z8Var.b);
        contentValues.put(DefaultAppMeasurementEventListenerRegistrar.NAME, z8Var.c);
        contentValues.put("set_timestamp", Long.valueOf(z8Var.d));
        a(contentValues, "value", z8Var.f2143e);
        try {
            if (v().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                a().f2046f.a("Failed to insert/update user property (got -1). appId", n3.a(z8Var.a));
            }
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing user property. appId", n3.a(z8Var.a), e2);
        }
        return true;
    }

    public final boolean b(String str, List<Integer> list) {
        ResourcesFlusher.b(str);
        o();
        d();
        SQLiteDatabase v = v();
        try {
            long b = b("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min((int) RecyclerView.MAX_SCROLL_DURATION, this.a.g.b(str, k.K)));
            if (b <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(outline.a(join, 2));
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(outline.a(sb2, 140));
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return v.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            a().f2046f.a("Database error querying filters. appId", n3.a(str), e2);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<j.c.a.a.g.a.z8> a(java.lang.String r14) {
        /*
            r13 = this;
            i.b.k.ResourcesFlusher.b(r14)
            r13.d()
            r13.o()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r13.v()     // Catch:{ SQLiteException -> 0x007e, all -> 0x007c }
            java.lang.String r3 = "user_attributes"
            java.lang.String r4 = "name"
            java.lang.String r5 = "origin"
            java.lang.String r6 = "set_timestamp"
            java.lang.String r7 = "value"
            java.lang.String[] r4 = new java.lang.String[]{r4, r5, r6, r7}     // Catch:{ SQLiteException -> 0x007e, all -> 0x007c }
            java.lang.String r5 = "app_id=?"
            r11 = 1
            java.lang.String[] r6 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x007e, all -> 0x007c }
            r12 = 0
            r6[r12] = r14     // Catch:{ SQLiteException -> 0x007e, all -> 0x007c }
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            java.lang.String r10 = "1000"
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x007e, all -> 0x007c }
            boolean r3 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x007a }
            if (r3 != 0) goto L_0x003d
            r2.close()
            return r0
        L_0x003d:
            java.lang.String r7 = r2.getString(r12)     // Catch:{ SQLiteException -> 0x007a }
            java.lang.String r3 = r2.getString(r11)     // Catch:{ SQLiteException -> 0x007a }
            if (r3 != 0) goto L_0x0049
            java.lang.String r3 = ""
        L_0x0049:
            r6 = r3
            r3 = 2
            long r8 = r2.getLong(r3)     // Catch:{ SQLiteException -> 0x007a }
            r3 = 3
            java.lang.Object r10 = r13.a(r2, r3)     // Catch:{ SQLiteException -> 0x007a }
            if (r10 != 0) goto L_0x0066
            j.c.a.a.g.a.n3 r3 = r13.a()     // Catch:{ SQLiteException -> 0x007a }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ SQLiteException -> 0x007a }
            java.lang.String r4 = "Read invalid user property value, ignoring it. appId"
            java.lang.Object r5 = j.c.a.a.g.a.n3.a(r14)     // Catch:{ SQLiteException -> 0x007a }
            r3.a(r4, r5)     // Catch:{ SQLiteException -> 0x007a }
            goto L_0x0070
        L_0x0066:
            j.c.a.a.g.a.z8 r3 = new j.c.a.a.g.a.z8     // Catch:{ SQLiteException -> 0x007a }
            r4 = r3
            r5 = r14
            r4.<init>(r5, r6, r7, r8, r10)     // Catch:{ SQLiteException -> 0x007a }
            r0.add(r3)     // Catch:{ SQLiteException -> 0x007a }
        L_0x0070:
            boolean r3 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x007a }
            if (r3 != 0) goto L_0x003d
            r2.close()
            return r0
        L_0x007a:
            r0 = move-exception
            goto L_0x0080
        L_0x007c:
            r14 = move-exception
            goto L_0x0097
        L_0x007e:
            r0 = move-exception
            r2 = r1
        L_0x0080:
            j.c.a.a.g.a.n3 r3 = r13.a()     // Catch:{ all -> 0x0095 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ all -> 0x0095 }
            java.lang.String r4 = "Error querying user properties. appId"
            java.lang.Object r14 = j.c.a.a.g.a.n3.a(r14)     // Catch:{ all -> 0x0095 }
            r3.a(r4, r14, r0)     // Catch:{ all -> 0x0095 }
            if (r2 == 0) goto L_0x0094
            r2.close()
        L_0x0094:
            return r1
        L_0x0095:
            r14 = move-exception
            r1 = r2
        L_0x0097:
            if (r1 == 0) goto L_0x009c
            r1.close()
        L_0x009c:
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00fc, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x011d, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00f8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<j.c.a.a.g.a.z8> a(java.lang.String r22, java.lang.String r23, java.lang.String r24) {
        /*
            r21 = this;
            i.b.k.ResourcesFlusher.b(r22)
            r21.d()
            r21.o()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00fc, all -> 0x00f8 }
            r3 = 3
            r2.<init>(r3)     // Catch:{ SQLiteException -> 0x00fc, all -> 0x00f8 }
            r11 = r22
            r2.add(r11)     // Catch:{ SQLiteException -> 0x00f4, all -> 0x00f8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00f4, all -> 0x00f8 }
            java.lang.String r5 = "app_id=?"
            r4.<init>(r5)     // Catch:{ SQLiteException -> 0x00f4, all -> 0x00f8 }
            boolean r5 = android.text.TextUtils.isEmpty(r23)     // Catch:{ SQLiteException -> 0x00f4, all -> 0x00f8 }
            if (r5 != 0) goto L_0x0032
            r5 = r23
            r2.add(r5)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.String r6 = " and origin=?"
            r4.append(r6)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            goto L_0x0034
        L_0x0032:
            r5 = r23
        L_0x0034:
            boolean r6 = android.text.TextUtils.isEmpty(r24)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            if (r6 != 0) goto L_0x004c
            java.lang.String r6 = java.lang.String.valueOf(r24)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.String r7 = "*"
            java.lang.String r6 = r6.concat(r7)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            r2.add(r6)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.String r6 = " and name glob ?"
            r4.append(r6)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
        L_0x004c:
            int r6 = r2.size()     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.Object[] r2 = r2.toArray(r6)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            r16 = r2
            java.lang.String[] r16 = (java.lang.String[]) r16     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            android.database.sqlite.SQLiteDatabase r12 = r21.v()     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.String r13 = "user_attributes"
            java.lang.String r2 = "name"
            java.lang.String r6 = "set_timestamp"
            java.lang.String r7 = "value"
            java.lang.String r8 = "origin"
            java.lang.String[] r14 = new java.lang.String[]{r2, r6, r7, r8}     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            java.lang.String r15 = r4.toString()     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            r17 = 0
            r18 = 0
            java.lang.String r19 = "rowid"
            java.lang.String r20 = "1001"
            android.database.Cursor r2 = r12.query(r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ SQLiteException -> 0x00f0, all -> 0x00f8 }
            boolean r4 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            if (r4 != 0) goto L_0x0086
            r2.close()
            return r0
        L_0x0086:
            int r4 = r0.size()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            r6 = 1000(0x3e8, float:1.401E-42)
            if (r4 < r6) goto L_0x00a0
            j.c.a.a.g.a.n3 r3 = r21.a()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            java.lang.String r4 = "Read more than the max allowed user properties, ignoring excess"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            r3.a(r4, r6)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            r12 = r21
            goto L_0x00dd
        L_0x00a0:
            r4 = 0
            java.lang.String r7 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            r4 = 1
            long r8 = r2.getLong(r4)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x00e8 }
            r4 = 2
            r12 = r21
            java.lang.Object r10 = r12.a(r2, r4)     // Catch:{ SQLiteException -> 0x00e6 }
            java.lang.String r13 = r2.getString(r3)     // Catch:{ SQLiteException -> 0x00e6 }
            if (r10 != 0) goto L_0x00c9
            j.c.a.a.g.a.n3 r4 = r21.a()     // Catch:{ SQLiteException -> 0x00e3 }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ SQLiteException -> 0x00e3 }
            java.lang.String r5 = "(2)Read invalid user property value, ignoring it"
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r22)     // Catch:{ SQLiteException -> 0x00e3 }
            r14 = r24
            r4.a(r5, r6, r13, r14)     // Catch:{ SQLiteException -> 0x00e3 }
            goto L_0x00d7
        L_0x00c9:
            r14 = r24
            j.c.a.a.g.a.z8 r15 = new j.c.a.a.g.a.z8     // Catch:{ SQLiteException -> 0x00e3 }
            r4 = r15
            r5 = r22
            r6 = r13
            r4.<init>(r5, r6, r7, r8, r10)     // Catch:{ SQLiteException -> 0x00e3 }
            r0.add(r15)     // Catch:{ SQLiteException -> 0x00e3 }
        L_0x00d7:
            boolean r4 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x00e3 }
            if (r4 != 0) goto L_0x00e1
        L_0x00dd:
            r2.close()
            return r0
        L_0x00e1:
            r5 = r13
            goto L_0x0086
        L_0x00e3:
            r0 = move-exception
            r5 = r13
            goto L_0x0104
        L_0x00e6:
            r0 = move-exception
            goto L_0x0104
        L_0x00e8:
            r0 = move-exception
            r12 = r21
            goto L_0x011a
        L_0x00ec:
            r0 = move-exception
            r12 = r21
            goto L_0x0104
        L_0x00f0:
            r0 = move-exception
            r12 = r21
            goto L_0x0103
        L_0x00f4:
            r0 = move-exception
            r12 = r21
            goto L_0x0101
        L_0x00f8:
            r0 = move-exception
            r12 = r21
            goto L_0x011b
        L_0x00fc:
            r0 = move-exception
            r12 = r21
            r11 = r22
        L_0x0101:
            r5 = r23
        L_0x0103:
            r2 = r1
        L_0x0104:
            j.c.a.a.g.a.n3 r3 = r21.a()     // Catch:{ all -> 0x0119 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ all -> 0x0119 }
            java.lang.String r4 = "(2)Error querying user properties"
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r22)     // Catch:{ all -> 0x0119 }
            r3.a(r4, r6, r5, r0)     // Catch:{ all -> 0x0119 }
            if (r2 == 0) goto L_0x0118
            r2.close()
        L_0x0118:
            return r1
        L_0x0119:
            r0 = move-exception
        L_0x011a:
            r1 = r2
        L_0x011b:
            if (r1 == 0) goto L_0x0120
            r1.close()
        L_0x0120:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.a(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    public final boolean a(g9 g9Var) {
        ResourcesFlusher.b(g9Var);
        d();
        o();
        if (c(g9Var.b, g9Var.d.c) == null) {
            if (b("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{g9Var.b}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", g9Var.b);
        contentValues.put("origin", g9Var.c);
        contentValues.put(DefaultAppMeasurementEventListenerRegistrar.NAME, g9Var.d.c);
        a(contentValues, "value", g9Var.d.a());
        contentValues.put("active", Boolean.valueOf(g9Var.f1999f));
        contentValues.put("trigger_event_name", g9Var.g);
        contentValues.put("trigger_timeout", Long.valueOf(g9Var.f2000i));
        k();
        contentValues.put("timed_out_event", y8.a(g9Var.h));
        contentValues.put("creation_timestamp", Long.valueOf(g9Var.f1998e));
        k();
        contentValues.put("triggered_event", y8.a(g9Var.f2001j));
        contentValues.put("triggered_timestamp", Long.valueOf(g9Var.d.d));
        contentValues.put("time_to_live", Long.valueOf(g9Var.f2002k));
        k();
        contentValues.put("expired_event", y8.a(g9Var.f2003l));
        try {
            if (v().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                a().f2046f.a("Failed to insert/update conditional user property (got -1)", n3.a(g9Var.b));
            }
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing conditional user property", n3.a(g9Var.b), e2);
        }
        return true;
    }

    public final List<g9> a(String str, String[] strArr) {
        d();
        o();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = v().query("conditional_properties", new String[]{"app_id", "origin", DefaultAppMeasurementEventListenerRegistrar.NAME, "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, null, null, "rowid", "1001");
            if (!cursor.moveToFirst()) {
                cursor.close();
                return arrayList;
            }
            while (true) {
                if (arrayList.size() < 1000) {
                    boolean z = false;
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    Object a = a(cursor, 3);
                    if (cursor.getInt(4) != 0) {
                        z = true;
                    }
                    String string4 = cursor.getString(5);
                    long j2 = cursor.getLong(6);
                    long j3 = cursor.getLong(8);
                    long j4 = cursor.getLong(10);
                    boolean z2 = z;
                    g9 g9Var = r3;
                    g9 g9Var2 = new g9(string, string2, new x8(string3, j4, a, string2), j3, z2, string4, (i) r().a(cursor.getBlob(7), i.CREATOR), j2, (i) r().a(cursor.getBlob(9), i.CREATOR), cursor.getLong(11), (i) r().a(cursor.getBlob(12), i.CREATOR));
                    arrayList.add(g9Var);
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } else {
                    a().f2046f.a("Read more than the max allowed conditional properties, ignoring extra", Integer.valueOf((int) AnswersRetryFilesSender.BACKOFF_MS));
                    break;
                }
            }
            cursor.close();
            return arrayList;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error querying conditional user property value", e2);
            List<g9> emptyList = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final void a(e4 e4Var) {
        ResourcesFlusher.b(e4Var);
        d();
        o();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", e4Var.g());
        contentValues.put("app_instance_id", e4Var.h());
        contentValues.put("gmp_app_id", e4Var.i());
        e4Var.a.i().d();
        contentValues.put("resettable_device_id_hash", e4Var.f1970e);
        contentValues.put("last_bundle_index", Long.valueOf(e4Var.u()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(e4Var.l()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(e4Var.m()));
        contentValues.put("app_version", e4Var.n());
        contentValues.put("app_store", e4Var.p());
        contentValues.put("gmp_version", Long.valueOf(e4Var.q()));
        contentValues.put("dev_cert_hash", Long.valueOf(e4Var.r()));
        contentValues.put("measurement_enabled", Boolean.valueOf(e4Var.t()));
        e4Var.a.i().d();
        contentValues.put("day", Long.valueOf(e4Var.w));
        e4Var.a.i().d();
        contentValues.put("daily_public_events_count", Long.valueOf(e4Var.x));
        e4Var.a.i().d();
        contentValues.put("daily_events_count", Long.valueOf(e4Var.y));
        e4Var.a.i().d();
        contentValues.put("daily_conversions_count", Long.valueOf(e4Var.z));
        e4Var.a.i().d();
        contentValues.put("config_fetched_time", Long.valueOf(e4Var.E));
        e4Var.a.i().d();
        contentValues.put("failed_config_fetch_time", Long.valueOf(e4Var.F));
        contentValues.put("app_version_int", Long.valueOf(e4Var.o()));
        contentValues.put("firebase_instance_id", e4Var.k());
        e4Var.a.i().d();
        contentValues.put("daily_error_events_count", Long.valueOf(e4Var.A));
        e4Var.a.i().d();
        contentValues.put("daily_realtime_events_count", Long.valueOf(e4Var.B));
        e4Var.a.i().d();
        contentValues.put("health_monitor_sample", e4Var.C);
        contentValues.put("android_id", Long.valueOf(e4Var.b()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(e4Var.c()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(e4Var.d()));
        contentValues.put("admob_app_id", e4Var.j());
        contentValues.put("dynamite_version", Long.valueOf(e4Var.s()));
        if (e4Var.f() != null) {
            if (e4Var.f().size() == 0) {
                a().f2047i.a("Safelisted events should not be an empty list. appId", e4Var.g());
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", e4Var.f()));
            }
        }
        try {
            SQLiteDatabase v = v();
            if (((long) v.update("apps", contentValues, "app_id = ?", new String[]{e4Var.g()})) == 0 && v.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                a().f2046f.a("Failed to insert/update app (got -1). appId", n3.a(e4Var.g()));
            }
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing app. appId", n3.a(e4Var.g()), e2);
        }
    }

    public final m9 a(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        ResourcesFlusher.b(str);
        d();
        o();
        String[] strArr = {str};
        m9 m9Var = new m9();
        Cursor cursor = null;
        try {
            SQLiteDatabase v = v();
            cursor = v.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            if (!cursor.moveToFirst()) {
                a().f2047i.a("Not updating daily counts, app is not known. appId", n3.a(str));
                cursor.close();
                return m9Var;
            }
            if (cursor.getLong(0) == j2) {
                m9Var.b = cursor.getLong(1);
                m9Var.a = cursor.getLong(2);
                m9Var.c = cursor.getLong(3);
                m9Var.d = cursor.getLong(4);
                m9Var.f2044e = cursor.getLong(5);
            }
            if (z) {
                m9Var.b++;
            }
            if (z2) {
                m9Var.a++;
            }
            if (z3) {
                m9Var.c++;
            }
            if (z4) {
                m9Var.d++;
            }
            if (z5) {
                m9Var.f2044e++;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("day", Long.valueOf(j2));
            contentValues.put("daily_public_events_count", Long.valueOf(m9Var.a));
            contentValues.put("daily_events_count", Long.valueOf(m9Var.b));
            contentValues.put("daily_conversions_count", Long.valueOf(m9Var.c));
            contentValues.put("daily_error_events_count", Long.valueOf(m9Var.d));
            contentValues.put("daily_realtime_events_count", Long.valueOf(m9Var.f2044e));
            v.update("apps", contentValues, "app_id=?", strArr);
            cursor.close();
            return m9Var;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error updating daily counts. appId", n3.a(str), e2);
            if (cursor != null) {
                cursor.close();
            }
            return m9Var;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final boolean a(u0 u0Var, boolean z) {
        d();
        o();
        ResourcesFlusher.b(u0Var);
        ResourcesFlusher.b(u0Var.zzs);
        ResourcesFlusher.b((u0Var.zzc & 8) != 0);
        B();
        if (((b) this.a.f2092n) != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (u0Var.zzj < currentTimeMillis - i9.u() || u0Var.zzj > i9.u() + currentTimeMillis) {
                a().f2047i.a("Storing bundle outside of the max uploading time span. appId, now, timestamp", n3.a(u0Var.zzs), Long.valueOf(currentTimeMillis), Long.valueOf(u0Var.zzj));
            }
            try {
                byte[] c = r().c(u0Var.f());
                a().f2052n.a("Saving bundle, size", Integer.valueOf(c.length));
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_id", u0Var.zzs);
                contentValues.put("bundle_end_timestamp", Long.valueOf(u0Var.zzj));
                contentValues.put("data", c);
                contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
                if ((u0Var.zzd & 2) != 0) {
                    contentValues.put("retry_count", Integer.valueOf(u0Var.zzao));
                }
                try {
                    if (v().insert("queue", null, contentValues) != -1) {
                        return true;
                    }
                    a().f2046f.a("Failed to insert bundle (got -1). appId", n3.a(u0Var.zzs));
                    return false;
                } catch (SQLiteException e2) {
                    a().f2046f.a("Error storing bundle. appId", n3.a(u0Var.zzs), e2);
                    return false;
                }
            } catch (IOException e3) {
                a().f2046f.a("Data loss. Failed to serialize bundle. appId", n3.a(u0Var.zzs), e3);
                return false;
            }
        } else {
            throw null;
        }
    }

    public final List<Pair<u0, Long>> a(String str, int i2, int i3) {
        d();
        o();
        ResourcesFlusher.a(i2 > 0);
        ResourcesFlusher.a(i3 > 0);
        ResourcesFlusher.b(str);
        Cursor cursor = null;
        try {
            cursor = v().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i2));
            if (!cursor.moveToFirst()) {
                List<Pair<u0, Long>> emptyList = Collections.emptyList();
                cursor.close();
                return emptyList;
            }
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            do {
                long j2 = cursor.getLong(0);
                try {
                    byte[] b = r().b(cursor.getBlob(1));
                    if (!arrayList.isEmpty() && b.length + i4 > i3) {
                        break;
                    }
                    try {
                        u0.a aVar = (u0.a) u0.o().a(b, j3.b());
                        if (!cursor.isNull(2)) {
                            int i5 = cursor.getInt(2);
                            aVar.i();
                            u0 u0Var = (u0) aVar.c;
                            u0Var.zzd = 2 | u0Var.zzd;
                            u0Var.zzao = i5;
                        }
                        i4 += b.length;
                        arrayList.add(Pair.create((u0) ((x3) aVar.m()), Long.valueOf(j2)));
                    } catch (IOException e2) {
                        a().f2046f.a("Failed to merge queued bundle. appId", n3.a(str), e2);
                    }
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } catch (IOException e3) {
                    a().f2046f.a("Failed to unzip queued bundle. appId", n3.a(str), e3);
                }
            } while (i4 <= i3);
            cursor.close();
            return arrayList;
        } catch (SQLiteException e4) {
            a().f2046f.a("Error querying bundles. appId", n3.a(str), e4);
            List<Pair<u0, Long>> emptyList2 = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final void a(List<Long> list) {
        d();
        o();
        ResourcesFlusher.b(list);
        if (list.size() == 0) {
            throw new IllegalArgumentException("Given Integer is zero");
        } else if (y()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(outline.a(join, 2));
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(outline.a(sb2, 80));
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (b(sb3.toString(), (String[]) null) > 0) {
                a().f2047i.a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase v = v();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                v.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                a().f2046f.a("Error incrementing retry count. error", e2);
            }
        }
    }

    public final boolean a(String str, int i2, a0 a0Var) {
        o();
        d();
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(a0Var);
        Integer num = null;
        if (TextUtils.isEmpty(a0Var.zze)) {
            p3 p3Var = a().f2047i;
            Object a = n3.a(str);
            Integer valueOf = Integer.valueOf(i2);
            if (a0Var.a()) {
                num = Integer.valueOf(a0Var.zzd);
            }
            p3Var.a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] f2 = a0Var.f();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", a0Var.a() ? Integer.valueOf(a0Var.zzd) : null);
        contentValues.put("event_name", a0Var.zze);
        if (this.a.g.d(str, k.w0)) {
            contentValues.put("session_scoped", (a0Var.zzc & 64) != 0 ? Boolean.valueOf(a0Var.zzk) : null);
        }
        contentValues.put("data", f2);
        try {
            if (v().insertWithOnConflict("event_filters", null, contentValues, 5) == -1) {
                a().f2046f.a("Failed to insert event filter (got -1). appId", n3.a(str));
            }
            return true;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing event filter. appId", n3.a(str), e2);
            return false;
        }
    }

    public final boolean a(String str, int i2, d0 d0Var) {
        o();
        d();
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(d0Var);
        Integer num = null;
        if (TextUtils.isEmpty(d0Var.zze)) {
            p3 p3Var = a().f2047i;
            Object a = n3.a(str);
            Integer valueOf = Integer.valueOf(i2);
            if (d0Var.a()) {
                num = Integer.valueOf(d0Var.zzd);
            }
            p3Var.a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] f2 = d0Var.f();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", d0Var.a() ? Integer.valueOf(d0Var.zzd) : null);
        contentValues.put("property_name", d0Var.zze);
        if (this.a.g.d(str, k.w0)) {
            contentValues.put("session_scoped", (d0Var.zzc & 32) != 0 ? Boolean.valueOf(d0Var.zzi) : null);
        }
        contentValues.put("data", f2);
        try {
            if (v().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                return true;
            }
            a().f2046f.a("Failed to insert property filter (got -1). appId", n3.a(str));
            return false;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing property filter. appId", n3.a(str), e2);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<java.lang.Integer>> a(java.lang.String r13, java.util.List<java.lang.String> r14) {
        /*
            r12 = this;
            r12.o()
            r12.d()
            i.b.k.ResourcesFlusher.b(r13)
            i.b.k.ResourcesFlusher.b(r14)
            i.e.ArrayMap r0 = new i.e.ArrayMap
            r0.<init>()
            boolean r1 = r14.isEmpty()
            if (r1 == 0) goto L_0x0018
            return r0
        L_0x0018:
            java.lang.String r1 = "app_id=? AND property_name in ("
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            r2 = 0
            r3 = 0
        L_0x0020:
            int r4 = r14.size()
            if (r3 >= r4) goto L_0x0035
            if (r3 <= 0) goto L_0x002d
            java.lang.String r4 = ","
            r1.append(r4)
        L_0x002d:
            java.lang.String r4 = "?"
            r1.append(r4)
            int r3 = r3 + 1
            goto L_0x0020
        L_0x0035:
            java.lang.String r3 = ")"
            r1.append(r3)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r14)
            r3.add(r2, r13)
            android.database.sqlite.SQLiteDatabase r4 = r12.v()
            r14 = 0
            java.lang.String r5 = "property_filters"
            java.lang.String r6 = "audience_id"
            java.lang.String r7 = "filter_id"
            java.lang.String[] r6 = new java.lang.String[]{r6, r7}     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a3 }
            java.lang.String r7 = r1.toString()     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a3 }
            java.lang.String[] r1 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a3 }
            java.lang.Object[] r1 = r3.toArray(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a3 }
            r8 = r1
            java.lang.String[] r8 = (java.lang.String[]) r8     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a3 }
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r1 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a3 }
            boolean r3 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00a1 }
            if (r3 != 0) goto L_0x006f
            r1.close()
            return r0
        L_0x006f:
            int r3 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x00a1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a1 }
            java.lang.Object r4 = r0.get(r4)     // Catch:{ SQLiteException -> 0x00a1 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ SQLiteException -> 0x00a1 }
            if (r4 != 0) goto L_0x008b
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00a1 }
            r4.<init>()     // Catch:{ SQLiteException -> 0x00a1 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a1 }
            r0.put(r3, r4)     // Catch:{ SQLiteException -> 0x00a1 }
        L_0x008b:
            r3 = 1
            int r3 = r1.getInt(r3)     // Catch:{ SQLiteException -> 0x00a1 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a1 }
            r4.add(r3)     // Catch:{ SQLiteException -> 0x00a1 }
            boolean r3 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x00a1 }
            if (r3 != 0) goto L_0x006f
            r1.close()
            return r0
        L_0x00a1:
            r0 = move-exception
            goto L_0x00a7
        L_0x00a3:
            r13 = move-exception
            goto L_0x00be
        L_0x00a5:
            r0 = move-exception
            r1 = r14
        L_0x00a7:
            j.c.a.a.g.a.n3 r2 = r12.a()     // Catch:{ all -> 0x00bc }
            j.c.a.a.g.a.p3 r2 = r2.f2046f     // Catch:{ all -> 0x00bc }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r13 = j.c.a.a.g.a.n3.a(r13)     // Catch:{ all -> 0x00bc }
            r2.a(r3, r13, r0)     // Catch:{ all -> 0x00bc }
            if (r1 == 0) goto L_0x00bb
            r1.close()
        L_0x00bb:
            return r14
        L_0x00bc:
            r13 = move-exception
            r14 = r1
        L_0x00be:
            if (r14 == 0) goto L_0x00c3
            r14.close()
        L_0x00c3:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.a(java.lang.String, java.util.List):java.util.Map");
    }

    public static void a(ContentValues contentValues, String str, Object obj) {
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    public final Object a(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            a().f2046f.a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                a().f2046f.a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            a().f2046f.a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    public final long a(u0 u0Var) {
        d();
        o();
        ResourcesFlusher.b(u0Var);
        ResourcesFlusher.b(u0Var.zzs);
        byte[] f2 = u0Var.f();
        long a = r().a(f2);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", u0Var.zzs);
        contentValues.put("metadata_fingerprint", Long.valueOf(a));
        contentValues.put("metadata", f2);
        try {
            v().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
            return a;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing raw event metadata. appId", n3.a(u0Var.zzs), e2);
            throw e2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(long r5) {
        /*
            r4 = this;
            r4.d()
            r4.o()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r4.v()     // Catch:{ SQLiteException -> 0x003c, all -> 0x003a }
            java.lang.String r2 = "select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x003c, all -> 0x003a }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x003c, all -> 0x003a }
            r6 = 0
            r3[r6] = r5     // Catch:{ SQLiteException -> 0x003c, all -> 0x003a }
            android.database.Cursor r5 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x003c, all -> 0x003a }
            boolean r1 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x0038 }
            if (r1 != 0) goto L_0x0030
            j.c.a.a.g.a.n3 r6 = r4.a()     // Catch:{ SQLiteException -> 0x0038 }
            j.c.a.a.g.a.p3 r6 = r6.f2052n     // Catch:{ SQLiteException -> 0x0038 }
            java.lang.String r1 = "No expired configs for apps with pending events"
            r6.a(r1)     // Catch:{ SQLiteException -> 0x0038 }
            r5.close()
            return r0
        L_0x0030:
            java.lang.String r6 = r5.getString(r6)     // Catch:{ SQLiteException -> 0x0038 }
            r5.close()
            return r6
        L_0x0038:
            r6 = move-exception
            goto L_0x003e
        L_0x003a:
            r6 = move-exception
            goto L_0x0051
        L_0x003c:
            r6 = move-exception
            r5 = r0
        L_0x003e:
            j.c.a.a.g.a.n3 r1 = r4.a()     // Catch:{ all -> 0x004f }
            j.c.a.a.g.a.p3 r1 = r1.f2046f     // Catch:{ all -> 0x004f }
            java.lang.String r2 = "Error selecting expired configs"
            r1.a(r2, r6)     // Catch:{ all -> 0x004f }
            if (r5 == 0) goto L_0x004e
            r5.close()
        L_0x004e:
            return r0
        L_0x004f:
            r6 = move-exception
            r0 = r5
        L_0x0051:
            if (r0 == 0) goto L_0x0056
            r0.close()
        L_0x0056:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.a(long):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair<j.c.a.a.f.e.q0, java.lang.Long> a(java.lang.String r8, java.lang.Long r9) {
        /*
            r7 = this;
            r7.d()
            r7.o()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r7.v()     // Catch:{ SQLiteException -> 0x006b, all -> 0x0069 }
            java.lang.String r2 = "select main_event, children_to_process from main_event_params where app_id=? and event_id=?"
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x006b, all -> 0x0069 }
            r4 = 0
            r3[r4] = r8     // Catch:{ SQLiteException -> 0x006b, all -> 0x0069 }
            java.lang.String r5 = java.lang.String.valueOf(r9)     // Catch:{ SQLiteException -> 0x006b, all -> 0x0069 }
            r6 = 1
            r3[r6] = r5     // Catch:{ SQLiteException -> 0x006b, all -> 0x0069 }
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x006b, all -> 0x0069 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0067 }
            if (r2 != 0) goto L_0x0033
            j.c.a.a.g.a.n3 r8 = r7.a()     // Catch:{ SQLiteException -> 0x0067 }
            j.c.a.a.g.a.p3 r8 = r8.f2052n     // Catch:{ SQLiteException -> 0x0067 }
            java.lang.String r9 = "Main event not found"
            r8.a(r9)     // Catch:{ SQLiteException -> 0x0067 }
            r1.close()
            return r0
        L_0x0033:
            byte[] r2 = r1.getBlob(r4)     // Catch:{ SQLiteException -> 0x0067 }
            long r3 = r1.getLong(r6)     // Catch:{ SQLiteException -> 0x0067 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0067 }
            j.c.a.a.f.e.j3 r4 = j.c.a.a.f.e.j3.b()     // Catch:{ IOException -> 0x0053 }
            j.c.a.a.f.e.q0 r5 = j.c.a.a.f.e.q0.zzi     // Catch:{ IOException -> 0x0053 }
            j.c.a.a.f.e.x3 r2 = j.c.a.a.f.e.x3.a(r5, r2, r4)     // Catch:{ IOException -> 0x0053 }
            j.c.a.a.f.e.q0 r2 = (j.c.a.a.f.e.q0) r2     // Catch:{ IOException -> 0x0053 }
            android.util.Pair r8 = android.util.Pair.create(r2, r3)     // Catch:{ SQLiteException -> 0x0067 }
            r1.close()
            return r8
        L_0x0053:
            r2 = move-exception
            j.c.a.a.g.a.n3 r3 = r7.a()     // Catch:{ SQLiteException -> 0x0067 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ SQLiteException -> 0x0067 }
            java.lang.String r4 = "Failed to merge main event. appId, eventId"
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ SQLiteException -> 0x0067 }
            r3.a(r4, r8, r9, r2)     // Catch:{ SQLiteException -> 0x0067 }
            r1.close()
            return r0
        L_0x0067:
            r8 = move-exception
            goto L_0x006d
        L_0x0069:
            r8 = move-exception
            goto L_0x0080
        L_0x006b:
            r8 = move-exception
            r1 = r0
        L_0x006d:
            j.c.a.a.g.a.n3 r9 = r7.a()     // Catch:{ all -> 0x007e }
            j.c.a.a.g.a.p3 r9 = r9.f2046f     // Catch:{ all -> 0x007e }
            java.lang.String r2 = "Error selecting main event"
            r9.a(r2, r8)     // Catch:{ all -> 0x007e }
            if (r1 == 0) goto L_0x007d
            r1.close()
        L_0x007d:
            return r0
        L_0x007e:
            r8 = move-exception
            r0 = r1
        L_0x0080:
            if (r0 == 0) goto L_0x0085
            r0.close()
        L_0x0085:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.n9.a(java.lang.String, java.lang.Long):android.util.Pair");
    }

    public final boolean a(String str, Long l2, long j2, q0 q0Var) {
        d();
        o();
        ResourcesFlusher.b(q0Var);
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(l2);
        byte[] f2 = q0Var.f();
        a().f2052n.a("Saving complex main event, appId, data size", g().a(str), Integer.valueOf(f2.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("event_id", l2);
        contentValues.put("children_to_process", Long.valueOf(j2));
        contentValues.put("main_event", f2);
        try {
            if (v().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            a().f2046f.a("Failed to insert complex main event (got -1). appId", n3.a(str));
            return false;
        } catch (SQLiteException e2) {
            a().f2046f.a("Error storing complex main event. appId", n3.a(str), e2);
            return false;
        }
    }

    public final boolean a(f fVar, long j2, boolean z) {
        d();
        o();
        ResourcesFlusher.b(fVar);
        ResourcesFlusher.b(fVar.a);
        q0.a o2 = q0.o();
        long j3 = fVar.f1985e;
        o2.i();
        q0 q0Var = (q0) o2.c;
        q0Var.zzc |= 4;
        q0Var.zzg = j3;
        h hVar = fVar.f1986f;
        if (hVar != null) {
            for (String next : hVar.b.keySet()) {
                s0.a p2 = s0.p();
                p2.a(next);
                r().a(p2, fVar.f1986f.b.get(next));
                o2.i();
                q0.a((q0) o2.c, p2);
            }
            byte[] f2 = ((q0) ((x3) o2.m())).f();
            a().f2052n.a("Saving event, name, data size", g().a(fVar.b), Integer.valueOf(f2.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", fVar.a);
            contentValues.put(DefaultAppMeasurementEventListenerRegistrar.NAME, fVar.b);
            contentValues.put("timestamp", Long.valueOf(fVar.d));
            contentValues.put("metadata_fingerprint", Long.valueOf(j2));
            contentValues.put("data", f2);
            contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (v().insert("raw_events", null, contentValues) != -1) {
                    return true;
                }
                a().f2046f.a("Failed to insert raw event (got -1). appId", n3.a(fVar.a));
                return false;
            } catch (SQLiteException e2) {
                a().f2046f.a("Error storing raw event. appId", n3.a(fVar.a), e2);
                return false;
            }
        } else {
            throw null;
        }
    }
}
