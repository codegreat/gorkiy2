package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import j.c.a.a.f.e.x3;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class u5 {
    public static final Class<?> a;
    public static final g6<?, ?> b = a(false);
    public static final g6<?, ?> c = a(true);
    public static final g6<?, ?> d = new h6();

    static {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            cls = null;
        }
        a = cls;
    }

    public static void a(Class<?> cls) {
        Class<?> cls2;
        if (!x3.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void b(int i2, List<Float> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        list.get(i5).floatValue();
                        i4 += 4;
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        zzek zzek = g3Var.a;
                        float floatValue = list.get(i3).floatValue();
                        if (zzek != null) {
                            zzek.c(Float.floatToRawIntBits(floatValue));
                            i3++;
                        } else {
                            throw null;
                        }
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek zzek2 = g3Var.a;
                    float floatValue2 = list.get(i3).floatValue();
                    if (zzek2 != null) {
                        int floatToRawIntBits = Float.floatToRawIntBits(floatValue2);
                        zzek.a aVar = (zzek.a) zzek2;
                        aVar.b((i2 << 3) | 5);
                        aVar.c(floatToRawIntBits);
                        i3++;
                    } else {
                        throw null;
                    }
                }
                return;
            }
            throw null;
        }
    }

    public static void c(int i2, List<Long> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i3 = 0;
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        i3 += zzek.c(list.get(i4).longValue());
                    }
                    g3Var.a.m1b(i3);
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        g3Var.a.a(list.get(i5).longValue());
                    }
                    return;
                }
                for (int i6 = 0; i6 < list.size(); i6++) {
                    zzek zzek = g3Var.a;
                    long longValue = list.get(i6).longValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 0);
                    aVar.a(longValue);
                }
                return;
            }
            throw null;
        }
    }

    public static void d(int i2, List<Long> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i3 = 0;
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        i3 += zzek.c(list.get(i4).longValue());
                    }
                    g3Var.a.m1b(i3);
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        g3Var.a.a(list.get(i5).longValue());
                    }
                    return;
                }
                for (int i6 = 0; i6 < list.size(); i6++) {
                    zzek zzek = g3Var.a;
                    long longValue = list.get(i6).longValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 0);
                    aVar.a(longValue);
                }
                return;
            }
            throw null;
        }
    }

    public static void e(int i2, List<Long> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i3 = 0;
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        i3 += zzek.d(list.get(i4).longValue());
                    }
                    g3Var.a.m1b(i3);
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        g3Var.a.a(zzek.e(list.get(i5).longValue()));
                    }
                    return;
                }
                int i6 = 0;
                while (i6 < list.size()) {
                    zzek zzek = g3Var.a;
                    long longValue = list.get(i6).longValue();
                    if (zzek != null) {
                        long e2 = zzek.e(longValue);
                        zzek.a aVar = (zzek.a) zzek;
                        aVar.b((i2 << 3) | 0);
                        aVar.a(e2);
                        i6++;
                    } else {
                        throw null;
                    }
                }
                return;
            }
            throw null;
        }
    }

    public static void f(int i2, List<Long> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        list.get(i5).longValue();
                        i4 += 8;
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        g3Var.a.b(list.get(i3).longValue());
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek zzek = g3Var.a;
                    long longValue = list.get(i3).longValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 1);
                    aVar.b(longValue);
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static void g(int i2, List<Long> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        list.get(i5).longValue();
                        i4 += 8;
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        g3Var.a.b(list.get(i3).longValue());
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek zzek = g3Var.a;
                    long longValue = list.get(i3).longValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 1);
                    aVar.b(longValue);
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static void h(int i2, List<Integer> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        i4 += zzek.g(list.get(i5).intValue());
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        g3Var.a.a(list.get(i3).intValue());
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    g3Var.a.a(i2, list.get(i3).intValue());
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static void i(int i2, List<Integer> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i3 = 0;
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        i3 += zzek.h(list.get(i4).intValue());
                    }
                    g3Var.a.m1b(i3);
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        g3Var.a.m1b(list.get(i5).intValue());
                    }
                    return;
                }
                for (int i6 = 0; i6 < list.size(); i6++) {
                    zzek zzek = g3Var.a;
                    int intValue = list.get(i6).intValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 0);
                    aVar.b(intValue);
                }
                return;
            }
            throw null;
        }
    }

    public static void j(int i2, List<Integer> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i3 = 0;
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        i3 += zzek.j(list.get(i4).intValue());
                    }
                    g3Var.a.m1b(i3);
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        g3Var.a.m1b(zzek.n(list.get(i5).intValue()));
                    }
                    return;
                }
                int i6 = 0;
                while (i6 < list.size()) {
                    zzek zzek = g3Var.a;
                    int intValue = list.get(i6).intValue();
                    if (zzek != null) {
                        int n2 = zzek.n(intValue);
                        zzek.a aVar = (zzek.a) zzek;
                        aVar.b((i2 << 3) | 0);
                        aVar.b(n2);
                        i6++;
                    } else {
                        throw null;
                    }
                }
                return;
            }
            throw null;
        }
    }

    public static void k(int i2, List<Integer> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        list.get(i5).intValue();
                        i4 += 4;
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        g3Var.a.c(list.get(i3).intValue());
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek zzek = g3Var.a;
                    int intValue = list.get(i3).intValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 5);
                    aVar.c(intValue);
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static void l(int i2, List<Integer> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        list.get(i5).intValue();
                        i4 += 4;
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        g3Var.a.c(list.get(i3).intValue());
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek zzek = g3Var.a;
                    int intValue = list.get(i3).intValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 5);
                    aVar.c(intValue);
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static void m(int i2, List<Integer> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        i4 += zzek.g(list.get(i5).intValue());
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        g3Var.a.a(list.get(i3).intValue());
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    g3Var.a.a(i2, list.get(i3).intValue());
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static void n(int i2, List<Boolean> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i3 = 0;
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        list.get(i4).booleanValue();
                        i3++;
                    }
                    g3Var.a.m1b(i3);
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        g3Var.a.a(list.get(i5).booleanValue() ? (byte) 1 : 0);
                    }
                    return;
                }
                for (int i6 = 0; i6 < list.size(); i6++) {
                    zzek zzek = g3Var.a;
                    boolean booleanValue = list.get(i6).booleanValue();
                    zzek.a aVar = (zzek.a) zzek;
                    aVar.b((i2 << 3) | 0);
                    aVar.a(booleanValue ? (byte) 1 : 0);
                }
                return;
            }
            throw null;
        }
    }

    public static void a(int i2, List<Double> list, c7 c7Var, boolean z) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (z) {
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < list.size(); i5++) {
                        list.get(i5).doubleValue();
                        i4 += 8;
                    }
                    g3Var.a.m1b(i4);
                    while (i3 < list.size()) {
                        zzek zzek = g3Var.a;
                        double doubleValue = list.get(i3).doubleValue();
                        if (zzek != null) {
                            zzek.b(Double.doubleToRawLongBits(doubleValue));
                            i3++;
                        } else {
                            throw null;
                        }
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek zzek2 = g3Var.a;
                    double doubleValue2 = list.get(i3).doubleValue();
                    if (zzek2 != null) {
                        long doubleToRawLongBits = Double.doubleToRawLongBits(doubleValue2);
                        zzek.a aVar = (zzek.a) zzek2;
                        aVar.b((i2 << 3) | 1);
                        aVar.b(doubleToRawLongBits);
                        i3++;
                    } else {
                        throw null;
                    }
                }
                return;
            }
            throw null;
        }
    }

    public static int h(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzek.f(i2) * size) + f(list);
    }

    public static int d(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzek.f(i2) * size) + b(list);
    }

    public static int f(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzek.f(i2) * size) + d(list);
    }

    public static int i(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzek.f(i2) * size) + g(list);
    }

    public static int k(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzek.i(i2) * size;
    }

    public static int h(List<?> list) {
        return list.size() << 2;
    }

    public static int g(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzek.f(i2) * size) + e(list);
    }

    public static int l(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzek.e(i2) * size;
    }

    public static void b(int i2, List<w2> list, c7 c7Var) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                for (int i3 = 0; i3 < list.size(); i3++) {
                    zzek.a aVar = (zzek.a) g3Var.a;
                    aVar.b((i2 << 3) | 2);
                    aVar.b(list.get(i3));
                }
                return;
            }
            throw null;
        }
    }

    public static int c(List<Long> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u4) {
            u4 u4Var = (u4) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.d(u4Var.b(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.d(list.get(i3).longValue());
                i3++;
            }
        }
        return i2;
    }

    public static int d(List<Integer> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z3) {
            z3 z3Var = (z3) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.g(z3Var.c(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.g(list.get(i3).intValue());
                i3++;
            }
        }
        return i2;
    }

    public static int e(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzek.f(i2) * size) + c(list);
    }

    public static int f(List<Integer> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z3) {
            z3 z3Var = (z3) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.h(z3Var.c(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.h(list.get(i3).intValue());
                i3++;
            }
        }
        return i2;
    }

    public static int i(List<?> list) {
        return list.size() << 3;
    }

    public static int j(int i2, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzek.l(i2) * size;
    }

    public static int g(List<Integer> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z3) {
            z3 z3Var = (z3) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.j(z3Var.c(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.j(list.get(i3).intValue());
                i3++;
            }
        }
        return i2;
    }

    public static int j(List<?> list) {
        return list.size();
    }

    public static void a(int i2, List<String> list, c7 c7Var) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i3 = 0;
                if (list instanceof m4) {
                    m4 m4Var = (m4) list;
                    while (i3 < list.size()) {
                        Object b2 = m4Var.b(i3);
                        if (b2 instanceof String) {
                            zzek.a aVar = (zzek.a) g3Var.a;
                            aVar.b((i2 << 3) | 2);
                            aVar.b((String) b2);
                        } else {
                            zzek.a aVar2 = (zzek.a) g3Var.a;
                            aVar2.b((i2 << 3) | 2);
                            aVar2.b((w2) b2);
                        }
                        i3++;
                    }
                    return;
                }
                while (i3 < list.size()) {
                    zzek.a aVar3 = (zzek.a) g3Var.a;
                    aVar3.b((i2 << 3) | 2);
                    aVar3.b(list.get(i3));
                    i3++;
                }
                return;
            }
            throw null;
        }
    }

    public static int e(List<Integer> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof z3) {
            z3 z3Var = (z3) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.g(z3Var.c(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.g(list.get(i3).intValue());
                i3++;
            }
        }
        return i2;
    }

    public static int c(int i2, List<w2> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int f2 = zzek.f(i2) * size;
        for (int i3 = 0; i3 < list.size(); i3++) {
            f2 += zzek.a(list.get(i3));
        }
        return f2;
    }

    public static void b(int i2, List<?> list, c7 c7Var, t5 t5Var) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                for (int i3 = 0; i3 < list.size(); i3++) {
                    g3Var.b(i2, list.get(i3), t5Var);
                }
                return;
            }
            throw null;
        }
    }

    public static int b(int i2, List list) {
        if (list.size() == 0) {
            return 0;
        }
        return (zzek.f(i2) * list.size()) + a(list);
    }

    public static int b(List<Long> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u4) {
            u4 u4Var = (u4) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.c(u4Var.b(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.c(list.get(i3).longValue());
                i3++;
            }
        }
        return i2;
    }

    public static int b(int i2, List<f5> list, t5 t5Var) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += zzek.a(i2, list.get(i4), t5Var);
        }
        return i3;
    }

    public static void a(int i2, List<?> list, c7 c7Var, t5 t5Var) {
        if (list != null && !list.isEmpty()) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                for (int i3 = 0; i3 < list.size(); i3++) {
                    g3Var.a(i2, list.get(i3), t5Var);
                }
                return;
            }
            throw null;
        }
    }

    public static int a(List<Long> list) {
        int i2;
        int size = list.size();
        int i3 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u4) {
            u4 u4Var = (u4) list;
            i2 = 0;
            while (i3 < size) {
                i2 += zzek.c(u4Var.b(i3));
                i3++;
            }
        } else {
            int i4 = 0;
            while (i3 < size) {
                i4 = i2 + zzek.c(list.get(i3).longValue());
                i3++;
            }
        }
        return i2;
    }

    public static int a(int i2, List<?> list) {
        int i3;
        int i4;
        int size = list.size();
        int i5 = 0;
        if (size == 0) {
            return 0;
        }
        int f2 = zzek.f(i2) * size;
        if (list instanceof m4) {
            m4 m4Var = (m4) list;
            while (i5 < size) {
                Object b2 = m4Var.b(i5);
                if (b2 instanceof w2) {
                    i4 = zzek.a((w2) b2);
                } else {
                    i4 = zzek.a((String) b2);
                }
                f2 = i4 + f2;
                i5++;
            }
        } else {
            while (i5 < size) {
                Object obj = list.get(i5);
                if (obj instanceof w2) {
                    i3 = zzek.a((w2) obj);
                } else {
                    i3 = zzek.a((String) obj);
                }
                f2 = i3 + f2;
                i5++;
            }
        }
        return f2;
    }

    public static int a(int i2, Object obj, t5 t5Var) {
        if (obj instanceof k4) {
            int f2 = zzek.f(i2);
            int a2 = ((k4) obj).a();
            return zzek.h(a2) + a2 + f2;
        }
        return zzek.a((f5) obj, t5Var) + zzek.f(i2);
    }

    public static int a(int i2, List<?> list, t5 t5Var) {
        int i3;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int f2 = zzek.f(i2) * size;
        for (int i4 = 0; i4 < size; i4++) {
            Object obj = list.get(i4);
            if (obj instanceof k4) {
                i3 = zzek.a((k4) obj);
            } else {
                i3 = zzek.a((f5) obj, t5Var);
            }
            f2 = i3 + f2;
        }
        return f2;
    }

    public static g6<?, ?> a(boolean z) {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            cls = null;
        }
        if (cls == null) {
            return null;
        }
        try {
            return (g6) cls.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused2) {
            return null;
        }
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static <T> void a(y4 y4Var, Object obj, Object obj2, long j2) {
        m6.a(obj, j2, y4Var.a(m6.f(obj, j2), m6.f(obj2, j2)));
    }

    public static <T, FT extends o3<FT>> void a(l3 l3Var, Object obj, Object obj2) {
        if (((k3) l3Var) != null) {
            m3<Object> m3Var = ((x3.b) obj2).zzc;
            if (!m3Var.a.isEmpty()) {
                m3<Object> a2 = ((x3.b) obj).a();
                if (a2 != null) {
                    for (int i2 = 0; i2 < m3Var.a.b(); i2++) {
                        a2.a((Map.Entry) m3Var.a.a(i2));
                    }
                    for (Map.Entry<FieldDescriptorType, Object> entry : m3Var.a.c()) {
                        a2.a((Map.Entry) entry);
                    }
                    return;
                }
                throw null;
            }
            return;
        }
        throw null;
    }

    public static <T, UT, UB> void a(g6 g6Var, Object obj, Object obj2) {
        if (((h6) g6Var) != null) {
            x3 x3Var = (x3) obj;
            i6 i6Var = x3Var.zzb;
            i6 i6Var2 = ((x3) obj2).zzb;
            if (!i6Var2.equals(i6.f1866f)) {
                int i2 = i6Var.a + i6Var2.a;
                int[] copyOf = Arrays.copyOf(i6Var.b, i2);
                System.arraycopy(i6Var2.b, 0, copyOf, i6Var.a, i6Var2.a);
                Object[] copyOf2 = Arrays.copyOf(i6Var.c, i2);
                System.arraycopy(i6Var2.c, 0, copyOf2, i6Var.a, i6Var2.a);
                i6Var = new i6(i2, copyOf, copyOf2, true);
            }
            x3Var.zzb = i6Var;
            return;
        }
        throw null;
    }

    public static <UT, UB> UB a(int i2, List<Integer> list, d4 d4Var, UB ub, g6<UT, UB> g6Var) {
        if (d4Var == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i3 = 0;
            for (int i4 = 0; i4 < size; i4++) {
                int intValue = list.get(i4).intValue();
                if (d4Var.a(intValue)) {
                    if (i4 != i3) {
                        list.set(i3, Integer.valueOf(intValue));
                    }
                    i3++;
                } else {
                    ub = a(i2, intValue, ub, g6Var);
                }
            }
            if (i3 != size) {
                list.subList(i3, size).clear();
            }
        } else {
            Iterator<Integer> it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = it.next().intValue();
                if (!d4Var.a(intValue2)) {
                    ub = a(i2, intValue2, ub, g6Var);
                    it.remove();
                }
            }
        }
        return ub;
    }

    public static <UT, UB> UB a(int i2, int i3, Object obj, g6 g6Var) {
        if (obj == null) {
            if (((h6) g6Var) != null) {
                obj = i6.b();
            } else {
                throw null;
            }
        }
        long j2 = (long) i3;
        if (((h6) g6Var) != null) {
            ((i6) obj).a(i2 << 3, Long.valueOf(j2));
            return obj;
        }
        throw null;
    }
}
