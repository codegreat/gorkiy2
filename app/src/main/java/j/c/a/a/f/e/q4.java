package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class q4 extends p4 {
    public /* synthetic */ q4(o4 o4Var) {
        super(null);
    }

    public static <E> e4<E> b(Object obj, long j2) {
        return (e4) m6.f(obj, j2);
    }

    public final void a(Object obj, long j2) {
        b(obj, j2).o();
    }

    public final <E> void a(Object obj, Object obj2, long j2) {
        e4 b = b(obj, j2);
        e4 b2 = b(obj2, j2);
        int size = b.size();
        int size2 = b2.size();
        if (size > 0 && size2 > 0) {
            if (!b.a()) {
                b = b.a(size2 + size);
            }
            b.addAll(b2);
        }
        if (size > 0) {
            b2 = b;
        }
        m6.a(obj, j2, b2);
    }
}
