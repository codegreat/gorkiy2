package j.c.a.a.g.a;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i7 implements Runnable {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ boolean c;
    public final /* synthetic */ z6 d;

    public i7(z6 z6Var, d9 d9Var, boolean z) {
        this.d = z6Var;
        this.b = d9Var;
        this.c = z;
    }

    public final void run() {
        z6 z6Var = this.d;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            f3Var.a(this.b);
            if (this.c) {
                this.d.t().A();
            }
            this.d.a(f3Var, null, this.b);
            this.d.D();
        } catch (RemoteException e2) {
            this.d.a().f2046f.a("Failed to send app launch to the service", e2);
        }
    }
}
