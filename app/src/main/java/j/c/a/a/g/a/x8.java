package j.c.a.a.g.a;

import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.p.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class x8 extends a {
    public static final Parcelable.Creator<x8> CREATOR = new w8();
    public final int b;
    public final String c;
    public final long d;

    /* renamed from: e  reason: collision with root package name */
    public final Long f2128e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2129f;
    public final String g;
    public final Double h;

    public x8(z8 z8Var) {
        this(z8Var.c, z8Var.d, z8Var.f2143e, z8Var.b);
    }

    public final Object a() {
        Long l2 = this.f2128e;
        if (l2 != null) {
            return l2;
        }
        Double d2 = this.h;
        if (d2 != null) {
            return d2;
        }
        String str = this.f2129f;
        if (str != null) {
            return str;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 1, this.b);
        ResourcesFlusher.a(parcel, 2, this.c, false);
        ResourcesFlusher.a(parcel, 3, this.d);
        Long l2 = this.f2128e;
        if (l2 != null) {
            ResourcesFlusher.d(parcel, 4, 8);
            parcel.writeLong(l2.longValue());
        }
        ResourcesFlusher.a(parcel, 6, this.f2129f, false);
        ResourcesFlusher.a(parcel, 7, this.g, false);
        Double d2 = this.h;
        if (d2 != null) {
            ResourcesFlusher.d(parcel, 8, 8);
            parcel.writeDouble(d2.doubleValue());
        }
        ResourcesFlusher.k(parcel, a);
    }

    public x8(String str, long j2, Object obj, String str2) {
        ResourcesFlusher.b(str);
        this.b = 2;
        this.c = str;
        this.d = j2;
        this.g = str2;
        if (obj == null) {
            this.f2128e = null;
            this.h = null;
            this.f2129f = null;
        } else if (obj instanceof Long) {
            this.f2128e = (Long) obj;
            this.h = null;
            this.f2129f = null;
        } else if (obj instanceof String) {
            this.f2128e = null;
            this.h = null;
            this.f2129f = (String) obj;
        } else if (obj instanceof Double) {
            this.f2128e = null;
            this.h = (Double) obj;
            this.f2129f = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    public x8(String str) {
        ResourcesFlusher.b(str);
        this.b = 2;
        this.c = str;
        this.d = 0;
        this.f2128e = null;
        this.h = null;
        this.f2129f = null;
        this.g = null;
    }

    public x8(int i2, String str, long j2, Long l2, Float f2, String str2, String str3, Double d2) {
        this.b = i2;
        this.c = str;
        this.d = j2;
        this.f2128e = l2;
        if (i2 == 1) {
            this.h = f2 != null ? Double.valueOf(f2.doubleValue()) : null;
        } else {
            this.h = d2;
        }
        this.f2129f = str2;
        this.g = str3;
    }
}
