package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class b extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f1830f;
    public final /* synthetic */ String g;
    public final /* synthetic */ m9 h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ pb f1831i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(pb pbVar, String str, String str2, m9 m9Var) {
        super(true);
        this.f1831i = pbVar;
        this.f1830f = str;
        this.g = str2;
        this.h = m9Var;
    }

    public final void a() {
        this.f1831i.g.getConditionalUserProperties(this.f1830f, this.g, this.h);
    }

    public final void b() {
        this.h.a((Bundle) null);
    }
}
