package j.c.a.a.g.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.p.a;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d9 extends a {
    public static final Parcelable.Creator<d9> CREATOR = new c9();
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f1951e;

    /* renamed from: f  reason: collision with root package name */
    public final long f1952f;
    public final long g;
    public final String h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f1953i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f1954j;

    /* renamed from: k  reason: collision with root package name */
    public final long f1955k;

    /* renamed from: l  reason: collision with root package name */
    public final String f1956l;

    /* renamed from: m  reason: collision with root package name */
    public final long f1957m;

    /* renamed from: n  reason: collision with root package name */
    public final long f1958n;

    /* renamed from: o  reason: collision with root package name */
    public final int f1959o;

    /* renamed from: p  reason: collision with root package name */
    public final boolean f1960p;

    /* renamed from: q  reason: collision with root package name */
    public final boolean f1961q;

    /* renamed from: r  reason: collision with root package name */
    public final boolean f1962r;

    /* renamed from: s  reason: collision with root package name */
    public final String f1963s;

    /* renamed from: t  reason: collision with root package name */
    public final Boolean f1964t;
    public final long u;
    public final List<String> v;

    public d9(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z, boolean z2, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5, String str7, Boolean bool, long j7, List<String> list) {
        ResourcesFlusher.b(str);
        this.b = str;
        this.c = TextUtils.isEmpty(str2) ? null : str2;
        this.d = str3;
        this.f1955k = j2;
        this.f1951e = str4;
        this.f1952f = j3;
        this.g = j4;
        this.h = str5;
        this.f1953i = z;
        this.f1954j = z2;
        this.f1956l = str6;
        this.f1957m = j5;
        this.f1958n = j6;
        this.f1959o = i2;
        this.f1960p = z3;
        this.f1961q = z4;
        this.f1962r = z5;
        this.f1963s = str7;
        this.f1964t = bool;
        this.u = j7;
        this.v = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 2, this.b, false);
        ResourcesFlusher.a(parcel, 3, this.c, false);
        ResourcesFlusher.a(parcel, 4, this.d, false);
        ResourcesFlusher.a(parcel, 5, this.f1951e, false);
        ResourcesFlusher.a(parcel, 6, this.f1952f);
        ResourcesFlusher.a(parcel, 7, this.g);
        ResourcesFlusher.a(parcel, 8, this.h, false);
        ResourcesFlusher.a(parcel, 9, this.f1953i);
        ResourcesFlusher.a(parcel, 10, this.f1954j);
        ResourcesFlusher.a(parcel, 11, this.f1955k);
        ResourcesFlusher.a(parcel, 12, this.f1956l, false);
        ResourcesFlusher.a(parcel, 13, this.f1957m);
        ResourcesFlusher.a(parcel, 14, this.f1958n);
        ResourcesFlusher.a(parcel, 15, this.f1959o);
        ResourcesFlusher.a(parcel, 16, this.f1960p);
        ResourcesFlusher.a(parcel, 17, this.f1961q);
        ResourcesFlusher.a(parcel, 18, this.f1962r);
        ResourcesFlusher.a(parcel, 19, this.f1963s, false);
        Boolean bool = this.f1964t;
        if (bool != null) {
            ResourcesFlusher.d(parcel, 21, 4);
            parcel.writeInt(bool.booleanValue() ? 1 : 0);
        }
        ResourcesFlusher.a(parcel, 22, this.u);
        List<String> list = this.v;
        if (list != null) {
            int j2 = ResourcesFlusher.j(parcel, 23);
            parcel.writeStringList(list);
            ResourcesFlusher.k(parcel, j2);
        }
        ResourcesFlusher.k(parcel, a);
    }

    public d9(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z, boolean z2, long j4, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5, String str7, Boolean bool, long j7, List<String> list) {
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.f1955k = j4;
        this.f1951e = str4;
        this.f1952f = j2;
        this.g = j3;
        this.h = str5;
        this.f1953i = z;
        this.f1954j = z2;
        this.f1956l = str6;
        this.f1957m = j5;
        this.f1958n = j6;
        this.f1959o = i2;
        this.f1960p = z3;
        this.f1961q = z4;
        this.f1962r = z5;
        this.f1963s = str7;
        this.f1964t = bool;
        this.u = j7;
        this.v = list;
    }
}
