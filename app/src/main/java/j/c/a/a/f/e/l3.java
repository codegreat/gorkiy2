package j.c.a.a.f.e;

import j.c.a.a.f.e.o3;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class l3<T extends o3<T>> {
    public abstract int a(Map.Entry<?, ?> entry);

    public abstract void a(c7 c7Var, Map.Entry<?, ?> entry);
}
