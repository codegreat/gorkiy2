package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a6 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ Object d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ long f1935e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ x5 f1936f;

    public a6(x5 x5Var, String str, String str2, Object obj, long j2) {
        this.f1936f = x5Var;
        this.b = str;
        this.c = str2;
        this.d = obj;
        this.f1935e = j2;
    }

    public final void run() {
        this.f1936f.a(this.b, this.c, this.d, this.f1935e);
    }
}
