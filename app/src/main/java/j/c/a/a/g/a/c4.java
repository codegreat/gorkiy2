package j.c.a.a.g.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import j.c.a.a.c.m.a;
import j.c.a.a.c.o.b;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class c4 {
    public final r4 a;

    public c4(r4 r4Var) {
        this.a = r4Var;
    }

    public final void a(String str) {
        if (str == null || str.isEmpty()) {
            this.a.a().f2050l.a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.a.i().d();
        if (!a()) {
            this.a.a().f2050l.a("Install Referrer Reporter is not available");
            return;
        }
        this.a.a().f2050l.a("Install Referrer Reporter is initializing");
        g4 g4Var = new g4(this, str);
        this.a.i().d();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.a.a.getPackageManager();
        if (packageManager == null) {
            this.a.a().f2047i.a("Failed to obtain Package Manager to verify binding conditions");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.a.a().f2050l.a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (serviceInfo.name == null || !"com.android.vending".equals(str2) || !a()) {
                this.a.a().f2050l.a("Play Store missing or incompatible. Version 8.3.73 or later required");
                return;
            }
            Intent intent2 = new Intent(intent);
            try {
                a a2 = a.a();
                Context context = this.a.a;
                if (a2 != null) {
                    context.getClass().getName();
                    this.a.a().f2050l.a("Install Referrer Service is", a2.b(context, intent2, g4Var, 1) ? "available" : "not available");
                    return;
                }
                throw null;
            } catch (Exception e2) {
                this.a.a().f2046f.a("Exception occurred while binding to Install Referrer Service", e2.getMessage());
            }
        }
    }

    public final boolean a() {
        try {
            j.c.a.a.c.o.a b = b.b(this.a.a);
            if (b == null) {
                this.a.a().f2050l.a("Failed to retrieve Package Manager to check Play Store compatibility");
                return false;
            } else if (b.a.getPackageManager().getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e2) {
            this.a.a().f2050l.a("Failed to retrieve Play Store version", e2);
            return false;
        }
    }
}
