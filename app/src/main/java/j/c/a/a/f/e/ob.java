package j.c.a.a.f.e;

import android.content.Context;
import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class ob extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f1885f;
    public final /* synthetic */ String g;
    public final /* synthetic */ Context h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ Bundle f1886i;

    /* renamed from: j  reason: collision with root package name */
    public final /* synthetic */ pb f1887j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ob(pb pbVar, String str, String str2, Context context, Bundle bundle) {
        super(true);
        this.f1887j = pbVar;
        this.f1885f = str;
        this.g = str2;
        this.h = context;
        this.f1886i = bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.dynamite.DynamiteModule.a(android.content.Context, java.lang.String, boolean):int
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.android.gms.dynamite.DynamiteModule.a(android.content.Context, com.google.android.gms.dynamite.DynamiteModule$a, java.lang.String):com.google.android.gms.dynamite.DynamiteModule
      com.google.android.gms.dynamite.DynamiteModule.a(android.content.Context, java.lang.String, int):com.google.android.gms.dynamite.DynamiteModule
      com.google.android.gms.dynamite.DynamiteModule.a(android.content.Context, java.lang.String, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ad A[Catch:{ RemoteException -> 0x00ae }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r14 = this;
            r0 = 0
            r1 = 1
            j.c.a.a.f.e.pb r2 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ RemoteException -> 0x00ae }
            r3.<init>()     // Catch:{ RemoteException -> 0x00ae }
            r2.d = r3     // Catch:{ RemoteException -> 0x00ae }
            java.lang.String r2 = r14.f1885f     // Catch:{ RemoteException -> 0x00ae }
            java.lang.String r3 = r14.g     // Catch:{ RemoteException -> 0x00ae }
            boolean r2 = j.c.a.a.f.e.pb.a(r2, r3)     // Catch:{ RemoteException -> 0x00ae }
            r3 = 0
            if (r2 == 0) goto L_0x0022
            java.lang.String r2 = r14.g     // Catch:{ RemoteException -> 0x00ae }
            java.lang.String r4 = r14.f1885f     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.pb r5 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            java.lang.String r5 = r5.a     // Catch:{ RemoteException -> 0x00ae }
            r11 = r2
            r10 = r4
            r9 = r5
            goto L_0x0025
        L_0x0022:
            r9 = r3
            r10 = r9
            r11 = r10
        L_0x0025:
            android.content.Context r2 = r14.h     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.pb.b(r2)     // Catch:{ RemoteException -> 0x00ae }
            java.lang.Boolean r2 = j.c.a.a.f.e.pb.f1897j     // Catch:{ RemoteException -> 0x00ae }
            boolean r2 = r2.booleanValue()     // Catch:{ RemoteException -> 0x00ae }
            if (r2 != 0) goto L_0x0037
            if (r10 == 0) goto L_0x0035
            goto L_0x0037
        L_0x0035:
            r2 = 0
            goto L_0x0038
        L_0x0037:
            r2 = 1
        L_0x0038:
            j.c.a.a.f.e.pb r4 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.pb r5 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            android.content.Context r6 = r14.h     // Catch:{ RemoteException -> 0x00ae }
            if (r5 == 0) goto L_0x00ad
            java.lang.String r7 = "com.google.android.gms.measurement.dynamite"
            if (r2 == 0) goto L_0x0047
            com.google.android.gms.dynamite.DynamiteModule$a r8 = com.google.android.gms.dynamite.DynamiteModule.f381k     // Catch:{ LoadingException -> 0x0058 }
            goto L_0x0049
        L_0x0047:
            com.google.android.gms.dynamite.DynamiteModule$a r8 = com.google.android.gms.dynamite.DynamiteModule.f379i     // Catch:{ LoadingException -> 0x0058 }
        L_0x0049:
            com.google.android.gms.dynamite.DynamiteModule r6 = com.google.android.gms.dynamite.DynamiteModule.a(r6, r8, r7)     // Catch:{ LoadingException -> 0x0058 }
            java.lang.String r8 = "com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"
            android.os.IBinder r6 = r6.a(r8)     // Catch:{ LoadingException -> 0x0058 }
            j.c.a.a.f.e.l8 r3 = j.c.a.a.f.e.db.asInterface(r6)     // Catch:{ LoadingException -> 0x0058 }
            goto L_0x005c
        L_0x0058:
            r6 = move-exception
            r5.a(r6, r1, r0)     // Catch:{ RemoteException -> 0x00ae }
        L_0x005c:
            r4.g = r3     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.pb r3 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.l8 r3 = r3.g     // Catch:{ RemoteException -> 0x00ae }
            if (r3 != 0) goto L_0x006e
            j.c.a.a.f.e.pb r2 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            java.lang.String r2 = r2.a     // Catch:{ RemoteException -> 0x00ae }
            java.lang.String r3 = "Failed to connect to measurement client."
            android.util.Log.w(r2, r3)     // Catch:{ RemoteException -> 0x00ae }
            return
        L_0x006e:
            android.content.Context r3 = r14.h     // Catch:{ RemoteException -> 0x00ae }
            int r3 = com.google.android.gms.dynamite.DynamiteModule.a(r3, r7)     // Catch:{ RemoteException -> 0x00ae }
            android.content.Context r4 = r14.h     // Catch:{ RemoteException -> 0x00ae }
            int r4 = com.google.android.gms.dynamite.DynamiteModule.a(r4, r7, r0)     // Catch:{ RemoteException -> 0x00ae }
            if (r2 == 0) goto L_0x0087
            int r2 = java.lang.Math.max(r3, r4)     // Catch:{ RemoteException -> 0x00ae }
            if (r4 >= r3) goto L_0x0084
            r3 = 1
            goto L_0x0085
        L_0x0084:
            r3 = 0
        L_0x0085:
            r8 = r3
            goto L_0x0091
        L_0x0087:
            if (r3 <= 0) goto L_0x008a
            r4 = r3
        L_0x008a:
            if (r3 <= 0) goto L_0x008e
            r2 = 1
            goto L_0x008f
        L_0x008e:
            r2 = 0
        L_0x008f:
            r8 = r2
            r2 = r4
        L_0x0091:
            j.c.a.a.f.e.nb r13 = new j.c.a.a.f.e.nb     // Catch:{ RemoteException -> 0x00ae }
            r4 = 18079(0x469f, double:8.932E-320)
            long r6 = (long) r2     // Catch:{ RemoteException -> 0x00ae }
            android.os.Bundle r12 = r14.f1886i     // Catch:{ RemoteException -> 0x00ae }
            r3 = r13
            r3.<init>(r4, r6, r8, r9, r10, r11, r12)     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.pb r2 = r14.f1887j     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.f.e.l8 r2 = r2.g     // Catch:{ RemoteException -> 0x00ae }
            android.content.Context r3 = r14.h     // Catch:{ RemoteException -> 0x00ae }
            j.c.a.a.d.b r4 = new j.c.a.a.d.b     // Catch:{ RemoteException -> 0x00ae }
            r4.<init>(r3)     // Catch:{ RemoteException -> 0x00ae }
            long r5 = r14.b     // Catch:{ RemoteException -> 0x00ae }
            r2.initialize(r4, r13, r5)     // Catch:{ RemoteException -> 0x00ae }
            return
        L_0x00ad:
            throw r3     // Catch:{ RemoteException -> 0x00ae }
        L_0x00ae:
            r2 = move-exception
            j.c.a.a.f.e.pb r3 = r14.f1887j
            r3.a(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.ob.a():void");
    }
}
