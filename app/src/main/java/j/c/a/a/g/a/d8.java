package j.c.a.a.g.a;

import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class d8 implements Runnable {
    public final /* synthetic */ q8 b;
    public final /* synthetic */ Runnable c;

    public d8(q8 q8Var, Runnable runnable) {
        this.b = q8Var;
        this.c = runnable;
    }

    public final void run() {
        this.b.o();
        q8 q8Var = this.b;
        Runnable runnable = this.c;
        q8Var.r();
        if (q8Var.f2078n == null) {
            q8Var.f2078n = new ArrayList();
        }
        q8Var.f2078n.add(runnable);
        this.b.n();
    }
}
