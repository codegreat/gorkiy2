package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class p extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Long f1888f;
    public final /* synthetic */ String g;
    public final /* synthetic */ String h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ Bundle f1889i;

    /* renamed from: j  reason: collision with root package name */
    public final /* synthetic */ boolean f1890j;

    /* renamed from: k  reason: collision with root package name */
    public final /* synthetic */ boolean f1891k;

    /* renamed from: l  reason: collision with root package name */
    public final /* synthetic */ pb f1892l;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(pb pbVar, Long l2, String str, String str2, Bundle bundle, boolean z, boolean z2) {
        super(true);
        this.f1892l = pbVar;
        this.f1888f = l2;
        this.g = str;
        this.h = str2;
        this.f1889i = bundle;
        this.f1890j = z;
        this.f1891k = z2;
    }

    public final void a() {
        Long l2 = this.f1888f;
        this.f1892l.g.logEvent(this.g, this.h, this.f1889i, this.f1890j, this.f1891k, l2 == null ? super.b : l2.longValue());
    }
}
