package j.c.a.a.e;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

public final class b implements DynamiteModule.a {
    public final DynamiteModule.a.b a(Context context, String str, DynamiteModule.a.C0002a aVar) {
        DynamiteModule.a.b bVar = new DynamiteModule.a.b();
        int a = aVar.a(context, str);
        bVar.a = a;
        if (a != 0) {
            bVar.c = -1;
        } else {
            int a2 = aVar.a(context, str, true);
            bVar.b = a2;
            if (a2 != 0) {
                bVar.c = 1;
            }
        }
        return bVar;
    }
}
