package j.c.a.a.g.a;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final /* synthetic */ class v6 implements Runnable {
    public final t6 b;
    public final int c;
    public final Exception d;

    /* renamed from: e  reason: collision with root package name */
    public final byte[] f2109e;

    /* renamed from: f  reason: collision with root package name */
    public final Map f2110f;

    public v6(t6 t6Var, int i2, Exception exc, byte[] bArr, Map map) {
        this.b = t6Var;
        this.c = i2;
        this.d = exc;
        this.f2109e = bArr;
        this.f2110f = map;
    }

    public final void run() {
        List<ResolveInfo> queryIntentActivities;
        t6 t6Var = this.b;
        int i2 = this.c;
        Exception exc = this.d;
        byte[] bArr = this.f2109e;
        r4 r4Var = t6Var.c.a;
        boolean z = true;
        boolean z2 = false;
        if (!((i2 == 200 || i2 == 204 || i2 == 304) && exc == null)) {
            r4Var.a().f2047i.a("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i2), exc);
            return;
        }
        r4Var.l().z.a(true);
        if (bArr.length == 0) {
            r4Var.a().f2051m.a("Deferred Deep Link response empty.");
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            String optString = jSONObject.optString("deeplink", "");
            String optString2 = jSONObject.optString("gclid", "");
            y8 n2 = r4Var.n();
            n2.b();
            if (TextUtils.isEmpty(optString) || (queryIntentActivities = n2.a.a.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) == null || queryIntentActivities.isEmpty()) {
                z = false;
            }
            if (!z) {
                r4Var.a().f2047i.a("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("gclid", optString2);
            bundle.putString("_cis", "ddp");
            r4Var.f2094p.a("auto", "_cmp", bundle);
            y8 n3 = r4Var.n();
            if (TextUtils.isEmpty(optString)) {
                return;
            }
            if (n3 != null) {
                try {
                    SharedPreferences.Editor edit = n3.a.a.getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
                    edit.putString("deeplink", optString);
                    z2 = edit.commit();
                } catch (Exception e2) {
                    n3.a().f2046f.a("Failed to persist Deferred Deep Link. exception", e2);
                }
                if (z2) {
                    n3.a.a.sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
                    return;
                }
                return;
            }
            throw null;
        } catch (JSONException e3) {
            r4Var.a().f2046f.a("Failed to parse the Deferred Deep Link response. exception", e3);
        }
    }
}
