package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public enum z6 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(w2.c),
    ENUM(null),
    MESSAGE(null);
    
    public final Object zzj;

    /* access modifiers changed from: public */
    z6(Object obj) {
        this.zzj = obj;
    }
}
