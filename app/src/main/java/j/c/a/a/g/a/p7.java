package j.c.a.a.g.a;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class p7 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ String f2067e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ boolean f2068f;
    public final /* synthetic */ d9 g;
    public final /* synthetic */ z6 h;

    public p7(z6 z6Var, AtomicReference atomicReference, String str, String str2, String str3, boolean z, d9 d9Var) {
        this.h = z6Var;
        this.b = atomicReference;
        this.c = str;
        this.d = str2;
        this.f2067e = str3;
        this.f2068f = z;
        this.g = d9Var;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                f3 f3Var = this.h.d;
                if (f3Var == null) {
                    this.h.a().f2046f.a("Failed to get user properties", n3.a(this.c), this.d, this.f2067e);
                    this.b.set(Collections.emptyList());
                    this.b.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.c)) {
                    this.b.set(f3Var.a(this.d, this.f2067e, this.f2068f, this.g));
                } else {
                    this.b.set(f3Var.a(this.c, this.d, this.f2067e, this.f2068f));
                }
                this.h.D();
                this.b.notify();
            } catch (RemoteException e2) {
                try {
                    this.h.a().f2046f.a("Failed to get user properties", n3.a(this.c), this.d, e2);
                    this.b.set(Collections.emptyList());
                    this.b.notify();
                } catch (Throwable th) {
                    this.b.notify();
                    throw th;
                }
            }
        }
    }
}
