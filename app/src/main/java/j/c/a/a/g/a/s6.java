package j.c.a.a.g.a;

import android.os.Bundle;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public interface s6 {
    String a();

    List<Bundle> a(String str, String str2);

    Map<String, Object> a(String str, String str2, boolean z);

    void a(Bundle bundle);

    void a(v5 v5Var);

    void a(String str);

    void a(String str, String str2, Bundle bundle);

    int b(String str);

    String b();

    void b(String str, String str2, Bundle bundle);

    String c();

    void c(String str);

    String d();

    long e();
}
