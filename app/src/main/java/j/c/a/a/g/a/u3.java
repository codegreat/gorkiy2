package j.c.a.a.g.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public class u3 extends BroadcastReceiver {
    public final q8 a;
    public boolean b;
    public boolean c;

    static {
        Class<u3> cls = u3.class;
    }

    public u3(q8 q8Var) {
        ResourcesFlusher.b(q8Var);
        this.a = q8Var;
    }

    public final void a() {
        this.a.m();
        this.a.i().d();
        this.a.i().d();
        if (this.b) {
            this.a.a().f2052n.a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.a.f2073i.a.unregisterReceiver(super);
            } catch (IllegalArgumentException e2) {
                this.a.a().f2046f.a("Failed to unregister the network broadcast receiver", e2);
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        this.a.m();
        String action = intent.getAction();
        this.a.a().f2052n.a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean u = this.a.d().u();
            if (this.c != u) {
                this.c = u;
                l4 i2 = this.a.i();
                x3 x3Var = new x3(this, u);
                i2.o();
                ResourcesFlusher.b(x3Var);
                i2.a((p4<?>) new p4(i2, x3Var, "Task exception on worker thread"));
                return;
            }
            return;
        }
        this.a.a().f2047i.a("NetworkBroadcastReceiver received unknown action", action);
    }
}
