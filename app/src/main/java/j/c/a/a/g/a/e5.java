package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class e5 extends c2 {
    public boolean b;

    public e5(r4 r4Var) {
        super(r4Var);
        this.a.D++;
    }

    public void v() {
    }

    public final void w() {
        if (!this.b) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void x() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!y()) {
            this.a.E.incrementAndGet();
            this.b = true;
        }
    }

    public abstract boolean y();
}
