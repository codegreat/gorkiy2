package j.c.a.a.f.e;

import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public interface e4<E> extends List<E>, RandomAccess {
    e4<E> a(int i2);

    boolean a();

    void o();
}
