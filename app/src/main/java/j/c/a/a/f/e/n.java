package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class n extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f1881f;
    public final /* synthetic */ m9 g;
    public final /* synthetic */ pb h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(pb pbVar, String str, m9 m9Var) {
        super(true);
        this.h = pbVar;
        this.f1881f = str;
        this.g = m9Var;
    }

    public final void a() {
        this.h.g.getMaxUserProperties(this.f1881f, this.g);
    }

    public final void b() {
        this.g.a((Bundle) null);
    }
}
