package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y9 implements v9 {
    public static final p1<Boolean> a = p1.a(new v1(q1.a("com.google.android.gms.measurement")), "measurement.experiment.enable_experiment_reporting", true);

    public final boolean a() {
        return a.b().booleanValue();
    }
}
