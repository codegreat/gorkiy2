package j.c.a.a.f.e;

import j.c.a.a.f.e.u0;
import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class t0 extends x3<t0, a> implements g5 {
    public static final t0 zzd;
    public static volatile m5<t0> zze;
    public e4<u0> zzc = s5.f1905e;

    static {
        t0 t0Var = new t0();
        zzd = t0Var;
        x3.zzd.put(t0.class, t0Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new t0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", u0.class});
            case 4:
                return zzd;
            case 5:
                m5<t0> m5Var = zze;
                if (m5Var == null) {
                    synchronized (t0.class) {
                        m5Var = zze;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzd);
                            zze = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<t0, a> implements g5 {
        public a() {
            super(t0.zzd);
        }

        public final a a(u0.a aVar) {
            i();
            t0 t0Var = (t0) super.c;
            if (!t0Var.zzc.a()) {
                t0Var.zzc = x3.a(t0Var.zzc);
            }
            t0Var.zzc.add((u0) ((x3) super.m()));
            return this;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(t0.zzd);
        }
    }
}
