package j.c.a.a.c;

import java.lang.ref.WeakReference;

public abstract class v extends t {
    public static final WeakReference<byte[]> c = new WeakReference<>(null);
    public WeakReference<byte[]> b = c;

    public v(byte[] bArr) {
        super(bArr);
    }

    public final byte[] g() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.b.get();
            if (bArr == null) {
                bArr = h();
                this.b = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    public abstract byte[] h();
}
