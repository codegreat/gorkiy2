package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n8 implements j8 {
    public static final p1<Boolean> a = p1.a(new v1(q1.a("com.google.android.gms.measurement")), "measurement.service.fix_gmp_version", false);

    public final boolean a() {
        return a.b().booleanValue();
    }
}
