package j.c.a.a.g.a;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n7 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ String f2053e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ d9 f2054f;
    public final /* synthetic */ z6 g;

    public n7(z6 z6Var, AtomicReference atomicReference, String str, String str2, String str3, d9 d9Var) {
        this.g = z6Var;
        this.b = atomicReference;
        this.c = str;
        this.d = str2;
        this.f2053e = str3;
        this.f2054f = d9Var;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                f3 f3Var = this.g.d;
                if (f3Var == null) {
                    this.g.a().f2046f.a("Failed to get conditional properties", n3.a(this.c), this.d, this.f2053e);
                    this.b.set(Collections.emptyList());
                    this.b.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.c)) {
                    this.b.set(f3Var.a(this.d, this.f2053e, this.f2054f));
                } else {
                    this.b.set(f3Var.a(this.c, this.d, this.f2053e));
                }
                this.g.D();
                this.b.notify();
            } catch (RemoteException e2) {
                try {
                    this.g.a().f2046f.a("Failed to get conditional properties", n3.a(this.c), this.d, e2);
                    this.b.set(Collections.emptyList());
                    this.b.notify();
                } catch (Throwable th) {
                    this.b.notify();
                    throw th;
                }
            }
        }
    }
}
