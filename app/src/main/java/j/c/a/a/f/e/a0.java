package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class a0 extends x3<a0, a> implements g5 {
    public static final a0 zzl;
    public static volatile m5<a0> zzm;
    public int zzc;
    public int zzd;
    public String zze = "";
    public e4<b0> zzf = s5.f1905e;
    public boolean zzg;
    public c0 zzh;
    public boolean zzi;
    public boolean zzj;
    public boolean zzk;

    static {
        a0 a0Var = new a0();
        zzl = a0Var;
        x3.zzd.put(a0.class, a0Var);
    }

    public final boolean a() {
        return (this.zzc & 1) != 0;
    }

    public final int i() {
        return this.zzd;
    }

    public final String j() {
        return this.zze;
    }

    public final boolean m() {
        return this.zzi;
    }

    public final boolean n() {
        return this.zzj;
    }

    public final boolean o() {
        return this.zzk;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<a0, a> implements g5 {
        public a() {
            super(a0.zzl);
        }

        public /* synthetic */ a(f0 f0Var) {
            super(a0.zzl);
        }
    }

    public static /* synthetic */ void a(a0 a0Var, String str) {
        if (str != null) {
            a0Var.zzc |= 2;
            a0Var.zze = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(a0 a0Var, int i2, b0 b0Var) {
        if (b0Var != null) {
            if (!a0Var.zzf.a()) {
                a0Var.zzf = x3.a(a0Var.zzf);
            }
            a0Var.zzf.set(i2, b0Var);
            return;
        }
        throw null;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (f0.a[i2 - 1]) {
            case 1:
                return new a0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\u001b\u0004\u0007\u0002\u0005\t\u0003\u0006\u0007\u0004\u0007\u0007\u0005\b\u0007\u0006", new Object[]{"zzc", "zzd", "zze", "zzf", b0.class, "zzg", "zzh", "zzi", "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                m5<a0> m5Var = zzm;
                if (m5Var == null) {
                    synchronized (a0.class) {
                        m5Var = zzm;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzl);
                            zzm = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
