package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class k extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ m9 f1869f;
    public final /* synthetic */ pb g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(pb pbVar, m9 m9Var) {
        super(true);
        this.g = pbVar;
        this.f1869f = m9Var;
    }

    public final void a() {
        this.g.g.getCurrentScreenName(this.f1869f);
    }

    public final void b() {
        this.f1869f.a((Bundle) null);
    }
}
