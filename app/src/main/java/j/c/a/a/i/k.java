package j.c.a.a.i;

import com.google.android.gms.tasks.RuntimeExecutionException;

public final class k implements Runnable {
    public final /* synthetic */ e b;
    public final /* synthetic */ j c;

    public k(j jVar, e eVar) {
        this.c = jVar;
        this.b = eVar;
    }

    public final void run() {
        if (((y) this.b).d) {
            this.c.c.e();
            return;
        }
        try {
            this.c.c.a((Object) this.c.b.a(this.b));
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.c.c.a((Exception) e2.getCause());
            } else {
                this.c.c.a((Exception) e2);
            }
        } catch (Exception e3) {
            this.c.c.a(e3);
        }
    }
}
