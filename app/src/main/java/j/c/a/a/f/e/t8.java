package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class t8 implements q8 {
    public static final p1<Boolean> a = p1.a(new v1(q1.a("com.google.android.gms.measurement")), "measurement.collection.firebase_global_collection_flag_enabled", true);

    public final boolean a() {
        return a.b().booleanValue();
    }
}
