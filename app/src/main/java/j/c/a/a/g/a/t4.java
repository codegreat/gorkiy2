package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class t4 implements Runnable {
    public final /* synthetic */ u5 b;
    public final /* synthetic */ r4 c;

    public t4(r4 r4Var, u5 u5Var) {
        this.c = r4Var;
        this.b = u5Var;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01f4, code lost:
        if (android.text.TextUtils.isEmpty(r1.f1997m) == false) goto L_0x01f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0335, code lost:
        if (android.text.TextUtils.isEmpty(r1.f1997m) == false) goto L_0x0337;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r10 = this;
            j.c.a.a.g.a.r4 r0 = r10.c
            j.c.a.a.g.a.u5 r1 = r10.b
            j.c.a.a.g.a.l4 r2 = r0.i()
            r2.d()
            j.c.a.a.g.a.b3<java.lang.String> r2 = j.c.a.a.g.a.k.f2010f
            r3 = 0
            java.lang.Object r2 = r2.a(r3)
            java.lang.String r2 = (java.lang.String) r2
            j.c.a.a.g.a.c r2 = new j.c.a.a.g.a.c
            r2.<init>(r0)
            r2.p()
            r0.u = r2
            j.c.a.a.g.a.g3 r2 = new j.c.a.a.g.a.g3
            long r4 = r1.f2106f
            r2.<init>(r0, r4)
            r2.x()
            r0.v = r2
            j.c.a.a.g.a.j3 r1 = new j.c.a.a.g.a.j3
            r1.<init>(r0)
            r1.x()
            r0.f2097s = r1
            j.c.a.a.g.a.z6 r1 = new j.c.a.a.g.a.z6
            r1.<init>(r0)
            r1.x()
            r0.f2098t = r1
            j.c.a.a.g.a.y8 r1 = r0.f2090l
            r1.q()
            j.c.a.a.g.a.w3 r1 = r0.h
            r1.q()
            j.c.a.a.g.a.c4 r1 = new j.c.a.a.g.a.c4
            r1.<init>(r0)
            r0.w = r1
            j.c.a.a.g.a.g3 r1 = r0.v
            boolean r4 = r1.b
            if (r4 != 0) goto L_0x03a2
            r1.v()
            j.c.a.a.g.a.r4 r4 = r1.a
            java.util.concurrent.atomic.AtomicInteger r4 = r4.E
            r4.incrementAndGet()
            r4 = 1
            r1.b = r4
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2050l
            j.c.a.a.g.a.i9 r5 = r0.g
            r5.o()
            r5 = 18079(0x469f, double:8.932E-320)
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            java.lang.String r6 = "App measurement is starting up, version"
            r1.a(r6, r5)
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2050l
            java.lang.String r5 = "To enable debug logging run: adb shell setprop log.tag.FA VERBOSE"
            r1.a(r5)
            r2.w()
            java.lang.String r1 = r2.c
            java.lang.String r2 = r0.b
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x00c5
            j.c.a.a.g.a.y8 r2 = r0.n()
            boolean r2 = r2.d(r1)
            if (r2 == 0) goto L_0x00a3
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2050l
            java.lang.String r2 = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none."
            goto L_0x00c2
        L_0x00a3:
            j.c.a.a.g.a.n3 r2 = r0.a()
            j.c.a.a.g.a.p3 r2 = r2.f2050l
            java.lang.String r5 = "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app "
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r6 = r1.length()
            if (r6 == 0) goto L_0x00ba
            java.lang.String r1 = r5.concat(r1)
            goto L_0x00bf
        L_0x00ba:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r5)
        L_0x00bf:
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x00c2:
            r1.a(r2)
        L_0x00c5:
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2051m
            java.lang.String r2 = "Debug-level message logging enabled"
            r1.a(r2)
            int r1 = r0.D
            java.util.concurrent.atomic.AtomicInteger r2 = r0.E
            int r2 = r2.get()
            if (r1 == r2) goto L_0x00f5
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2046f
            int r2 = r0.D
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            java.util.concurrent.atomic.AtomicInteger r5 = r0.E
            int r5 = r5.get()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            java.lang.String r6 = "Not all components initialized"
            r1.a(r6, r2, r5)
        L_0x00f5:
            r0.x = r4
            j.c.a.a.g.a.r4 r0 = r10.c
            j.c.a.a.g.a.l4 r1 = r0.i()
            r1.d()
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.b4 r1 = r1.f2111e
            long r1 = r1.a()
            r5 = 0
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x0125
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.b4 r1 = r1.f2111e
            j.c.a.a.c.n.a r2 = r0.f2092n
            j.c.a.a.c.n.b r2 = (j.c.a.a.c.n.b) r2
            if (r2 == 0) goto L_0x0124
            long r7 = java.lang.System.currentTimeMillis()
            r1.a(r7)
            goto L_0x0125
        L_0x0124:
            throw r3
        L_0x0125:
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.b4 r1 = r1.f2114j
            long r1 = r1.a()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            long r1 = r1.longValue()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x0157
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            long r5 = r0.F
            java.lang.Long r2 = java.lang.Long.valueOf(r5)
            java.lang.String r5 = "Persisting first open"
            r1.a(r5, r2)
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.b4 r1 = r1.f2114j
            long r5 = r0.F
            r1.a(r5)
        L_0x0157:
            boolean r1 = r0.e()
            if (r1 != 0) goto L_0x01d8
            boolean r1 = r0.c()
            if (r1 == 0) goto L_0x037f
            j.c.a.a.g.a.y8 r1 = r0.n()
            java.lang.String r2 = "android.permission.INTERNET"
            boolean r1 = r1.c(r2)
            if (r1 != 0) goto L_0x017a
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2046f
            java.lang.String r2 = "App is missing INTERNET permission"
            r1.a(r2)
        L_0x017a:
            j.c.a.a.g.a.y8 r1 = r0.n()
            java.lang.String r2 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r1 = r1.c(r2)
            if (r1 != 0) goto L_0x0191
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2046f
            java.lang.String r2 = "App is missing ACCESS_NETWORK_STATE permission"
            r1.a(r2)
        L_0x0191:
            android.content.Context r1 = r0.a
            j.c.a.a.c.o.a r1 = j.c.a.a.c.o.b.b(r1)
            boolean r1 = r1.a()
            if (r1 != 0) goto L_0x01cb
            j.c.a.a.g.a.i9 r1 = r0.g
            boolean r1 = r1.t()
            if (r1 != 0) goto L_0x01cb
            android.content.Context r1 = r0.a
            boolean r1 = j.c.a.a.g.a.i4.a(r1)
            if (r1 != 0) goto L_0x01b8
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2046f
            java.lang.String r2 = "AppMeasurementReceiver not registered/enabled"
            r1.a(r2)
        L_0x01b8:
            android.content.Context r1 = r0.a
            boolean r1 = j.c.a.a.g.a.y8.a(r1)
            if (r1 != 0) goto L_0x01cb
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2046f
            java.lang.String r2 = "AppMeasurementService not registered/enabled"
            r1.a(r2)
        L_0x01cb:
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2046f
            java.lang.String r2 = "Uploading is not possible. App measurement disabled"
            r1.a(r2)
            goto L_0x037f
        L_0x01d8:
            j.c.a.a.g.a.g3 r1 = r0.t()
            r1.w()
            java.lang.String r1 = r1.f1996l
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x01f6
            j.c.a.a.g.a.g3 r1 = r0.t()
            r1.w()
            java.lang.String r1 = r1.f1997m
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0306
        L_0x01f6:
            r0.n()
            j.c.a.a.g.a.g3 r1 = r0.t()
            r1.w()
            java.lang.String r1 = r1.f1996l
            j.c.a.a.g.a.w3 r2 = r0.l()
            r2.d()
            android.content.SharedPreferences r2 = r2.v()
            java.lang.String r5 = "gmp_app_id"
            java.lang.String r2 = r2.getString(r5, r3)
            j.c.a.a.g.a.g3 r6 = r0.t()
            r6.w()
            java.lang.String r6 = r6.f1997m
            j.c.a.a.g.a.w3 r7 = r0.l()
            r7.d()
            android.content.SharedPreferences r7 = r7.v()
            java.lang.String r8 = "admob_app_id"
            java.lang.String r7 = r7.getString(r8, r3)
            boolean r1 = j.c.a.a.g.a.y8.a(r1, r2, r6, r7)
            if (r1 == 0) goto L_0x02ca
            j.c.a.a.g.a.n3 r1 = r0.a()
            j.c.a.a.g.a.p3 r1 = r1.f2050l
            java.lang.String r2 = "Rechecking which service to use due to a GMP App Id change"
            r1.a(r2)
            j.c.a.a.g.a.w3 r1 = r0.l()
            r1.d()
            j.c.a.a.g.a.n3 r2 = r1.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            java.lang.String r6 = "Clearing collection preferences."
            r2.a(r6)
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.g.a.i9 r2 = r2.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r6 = j.c.a.a.g.a.k.k0
            boolean r2 = r2.a(r6)
            if (r2 == 0) goto L_0x0278
            java.lang.Boolean r2 = r1.u()
            android.content.SharedPreferences r4 = r1.v()
            android.content.SharedPreferences$Editor r4 = r4.edit()
            r4.clear()
            r4.apply()
            if (r2 == 0) goto L_0x02a2
            boolean r2 = r2.booleanValue()
            r1.a(r2)
            goto L_0x02a2
        L_0x0278:
            android.content.SharedPreferences r2 = r1.v()
            java.lang.String r6 = "measurement_enabled"
            boolean r2 = r2.contains(r6)
            if (r2 == 0) goto L_0x028f
            r1.d()
            android.content.SharedPreferences r7 = r1.v()
            boolean r4 = r7.getBoolean(r6, r4)
        L_0x028f:
            android.content.SharedPreferences r6 = r1.v()
            android.content.SharedPreferences$Editor r6 = r6.edit()
            r6.clear()
            r6.apply()
            if (r2 == 0) goto L_0x02a2
            r1.a(r4)
        L_0x02a2:
            j.c.a.a.g.a.j3 r1 = r0.f2097s
            j.c.a.a.g.a.r4.a(r1)
            j.c.a.a.g.a.j3 r1 = r0.f2097s
            r1.z()
            j.c.a.a.g.a.z6 r1 = r0.f2098t
            r1.B()
            j.c.a.a.g.a.z6 r1 = r0.f2098t
            r1.A()
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.b4 r1 = r1.f2114j
            long r6 = r0.F
            r1.a(r6)
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.d4 r1 = r1.f2116l
            r1.a(r3)
        L_0x02ca:
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.g3 r2 = r0.t()
            r2.w()
            java.lang.String r2 = r2.f1996l
            r1.d()
            android.content.SharedPreferences r1 = r1.v()
            android.content.SharedPreferences$Editor r1 = r1.edit()
            r1.putString(r5, r2)
            r1.apply()
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.g3 r2 = r0.t()
            r2.w()
            java.lang.String r2 = r2.f1997m
            r1.d()
            android.content.SharedPreferences r1 = r1.v()
            android.content.SharedPreferences$Editor r1 = r1.edit()
            r1.putString(r8, r2)
            r1.apply()
        L_0x0306:
            j.c.a.a.g.a.x5 r1 = r0.m()
            j.c.a.a.g.a.w3 r2 = r0.l()
            j.c.a.a.g.a.d4 r2 = r2.f2116l
            java.lang.String r2 = r2.a()
            java.util.concurrent.atomic.AtomicReference<java.lang.String> r1 = r1.g
            r1.set(r2)
            j.c.a.a.g.a.g3 r1 = r0.t()
            r1.w()
            java.lang.String r1 = r1.f1996l
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x0337
            j.c.a.a.g.a.g3 r1 = r0.t()
            r1.w()
            java.lang.String r1 = r1.f1997m
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x037f
        L_0x0337:
            boolean r1 = r0.c()
            j.c.a.a.g.a.w3 r2 = r0.l()
            android.content.SharedPreferences r2 = r2.c
            java.lang.String r3 = "deferred_analytics_collection"
            boolean r2 = r2.contains(r3)
            if (r2 != 0) goto L_0x035a
            j.c.a.a.g.a.i9 r2 = r0.g
            boolean r2 = r2.q()
            if (r2 != 0) goto L_0x035a
            j.c.a.a.g.a.w3 r2 = r0.l()
            r3 = r1 ^ 1
            r2.b(r3)
        L_0x035a:
            if (r1 == 0) goto L_0x0363
            j.c.a.a.g.a.x5 r1 = r0.m()
            r1.A()
        L_0x0363:
            j.c.a.a.g.a.z6 r1 = r0.r()
            java.util.concurrent.atomic.AtomicReference r2 = new java.util.concurrent.atomic.AtomicReference
            r2.<init>()
            r1.d()
            r1.w()
            r3 = 0
            j.c.a.a.g.a.d9 r3 = r1.a(r3)
            j.c.a.a.g.a.g7 r4 = new j.c.a.a.g.a.g7
            r4.<init>(r1, r2, r3)
            r1.a(r4)
        L_0x037f:
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.y3 r1 = r1.f2124t
            j.c.a.a.g.a.i9 r2 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r3 = j.c.a.a.g.a.k.s0
            boolean r2 = r2.a(r3)
            r1.a(r2)
            j.c.a.a.g.a.w3 r1 = r0.l()
            j.c.a.a.g.a.y3 r1 = r1.u
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.t0
            boolean r0 = r0.a(r2)
            r1.a(r0)
            return
        L_0x03a2:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Can't initialize twice"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.t4.run():void");
    }
}
