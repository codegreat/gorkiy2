package j.c.a.a.g.a;

import android.os.IBinder;
import android.os.Parcel;
import j.c.a.a.f.e.c2;
import j.c.a.a.f.e.q;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h3 extends q implements f3 {
    public h3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    public final void a(i iVar, d9 d9Var) {
        Parcel g = g();
        c2.a(g, iVar);
        c2.a(g, d9Var);
        b(1, g);
    }

    public final String b(d9 d9Var) {
        Parcel g = g();
        c2.a(g, d9Var);
        Parcel a = a(11, g);
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    public final void c(d9 d9Var) {
        Parcel g = g();
        c2.a(g, d9Var);
        b(6, g);
    }

    public final void d(d9 d9Var) {
        Parcel g = g();
        c2.a(g, d9Var);
        b(18, g);
    }

    public final void a(x8 x8Var, d9 d9Var) {
        Parcel g = g();
        c2.a(g, x8Var);
        c2.a(g, d9Var);
        b(2, g);
    }

    public final void a(d9 d9Var) {
        Parcel g = g();
        c2.a(g, d9Var);
        b(4, g);
    }

    public final void a(i iVar, String str, String str2) {
        Parcel g = g();
        c2.a(g, iVar);
        g.writeString(str);
        g.writeString(str2);
        b(5, g);
    }

    public final List<x8> a(d9 d9Var, boolean z) {
        Parcel g = g();
        c2.a(g, d9Var);
        g.writeInt(z ? 1 : 0);
        Parcel a = a(7, g);
        ArrayList createTypedArrayList = a.createTypedArrayList(x8.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    public final byte[] a(i iVar, String str) {
        Parcel g = g();
        c2.a(g, iVar);
        g.writeString(str);
        Parcel a = a(9, g);
        byte[] createByteArray = a.createByteArray();
        a.recycle();
        return createByteArray;
    }

    public final void a(long j2, String str, String str2, String str3) {
        Parcel g = g();
        g.writeLong(j2);
        g.writeString(str);
        g.writeString(str2);
        g.writeString(str3);
        b(10, g);
    }

    public final void a(g9 g9Var, d9 d9Var) {
        Parcel g = g();
        c2.a(g, g9Var);
        c2.a(g, d9Var);
        b(12, g);
    }

    public final void a(g9 g9Var) {
        Parcel g = g();
        c2.a(g, g9Var);
        b(13, g);
    }

    public final List<x8> a(String str, String str2, boolean z, d9 d9Var) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, z);
        c2.a(g, d9Var);
        Parcel a = a(14, g);
        ArrayList createTypedArrayList = a.createTypedArrayList(x8.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    public final List<x8> a(String str, String str2, String str3, boolean z) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        g.writeString(str3);
        c2.a(g, z);
        Parcel a = a(15, g);
        ArrayList createTypedArrayList = a.createTypedArrayList(x8.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    public final List<g9> a(String str, String str2, d9 d9Var) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, d9Var);
        Parcel a = a(16, g);
        ArrayList createTypedArrayList = a.createTypedArrayList(g9.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    public final List<g9> a(String str, String str2, String str3) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        g.writeString(str3);
        Parcel a = a(17, g);
        ArrayList createTypedArrayList = a.createTypedArrayList(g9.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }
}
