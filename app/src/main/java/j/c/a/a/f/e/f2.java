package j.c.a.a.f.e;

import java.io.Serializable;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f2<T> implements b2<T>, Serializable {
    @NullableDecl
    public final T b;

    public f2(@NullableDecl T t2) {
        this.b = t2;
    }

    public final T a() {
        return this.b;
    }

    public final boolean equals(@NullableDecl Object obj) {
        if (!(obj instanceof f2)) {
            return false;
        }
        T t2 = this.b;
        T t3 = ((f2) obj).b;
        if (t2 == t3) {
            return true;
        }
        if (t2 == null || !t2.equals(t3)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.b});
    }

    public final String toString() {
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(valueOf.length() + 22);
        sb.append("Suppliers.ofInstance(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }
}
