package j.c.a.a.c.l;

import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.List;

public final class n {
    public final List<String> a = new ArrayList();
    public final Object b;

    public /* synthetic */ n(Object obj, h0 h0Var) {
        ResourcesFlusher.b(obj);
        this.b = obj;
    }

    public final n a(String str, Object obj) {
        List<String> list = this.a;
        ResourcesFlusher.b((Object) str);
        String str2 = str;
        String valueOf = String.valueOf(obj);
        StringBuilder sb = new StringBuilder(valueOf.length() + outline.a(str2, 1));
        sb.append(str2);
        sb.append("=");
        sb.append(valueOf);
        list.add(sb.toString());
        return this;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(100);
        sb.append(this.b.getClass().getSimpleName());
        sb.append('{');
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append(this.a.get(i2));
            if (i2 < size - 1) {
                sb.append(", ");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
