package j.c.a.a.g.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.f;
import j.c.a.a.c.g;
import j.c.a.a.c.n.b;
import j.c.a.a.c.n.c;
import j.c.a.a.f.e.fb;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;
import l.a.a.a.o.d.EventsFilesManager;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y8 extends n5 {
    public static final String[] g = {"firebase_", "google_", "ga_"};
    public SecureRandom c;
    public final AtomicLong d = new AtomicLong(0);

    /* renamed from: e  reason: collision with root package name */
    public int f2138e;

    /* renamed from: f  reason: collision with root package name */
    public Integer f2139f = null;

    public y8(r4 r4Var) {
        super(r4Var);
    }

    public static boolean d(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    public static boolean e(String str) {
        ResourcesFlusher.b(str);
        if (str.charAt(0) != '_' || str.equals("_ep")) {
            return true;
        }
        return false;
    }

    public static boolean f(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
    }

    public static int g(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        return "_id".equals(str) ? 256 : 36;
    }

    public static MessageDigest x() {
        int i2 = 0;
        while (i2 < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                if (instance != null) {
                    return instance;
                }
                i2++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        return null;
    }

    public final Bundle a(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri == null) {
            return null;
        }
        try {
            if (uri.isHierarchical()) {
                str4 = uri.getQueryParameter("utm_campaign");
                str3 = uri.getQueryParameter("utm_source");
                str2 = uri.getQueryParameter("utm_medium");
                str = uri.getQueryParameter("gclid");
            } else {
                str4 = null;
                str3 = null;
                str2 = null;
                str = null;
            }
            if (TextUtils.isEmpty(str4) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str)) {
                return null;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str4)) {
                bundle.putString("campaign", str4);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle.putString("source", str3);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle.putString("medium", str2);
            }
            if (!TextUtils.isEmpty(str)) {
                bundle.putString("gclid", str);
            }
            String queryParameter = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("term", queryParameter);
            }
            String queryParameter2 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("content", queryParameter2);
            }
            String queryParameter3 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("aclid", queryParameter3);
            }
            String queryParameter4 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("cp1", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("anid", queryParameter5);
            }
            return bundle;
        } catch (UnsupportedOperationException e2) {
            a().f2047i.a("Install referrer url isn't a hierarchical URI", e2);
            return null;
        }
    }

    public final boolean b(String str, String str2) {
        if (str2 == null) {
            a().h.a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            a().h.a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        a().h.a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            a().h.a("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    public final boolean c(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            ResourcesFlusher.b((Object) str);
            if (str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$")) {
                return true;
            }
            if (this.a.p()) {
                a().h.a("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", n3.a(str));
            }
            return false;
        } else if (!TextUtils.isEmpty(str2)) {
            ResourcesFlusher.b((Object) str2);
            if (str2.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$")) {
                return true;
            }
            a().h.a("Invalid admob_app_id. Analytics disabled.", n3.a(str2));
            return false;
        } else {
            if (this.a.p()) {
                a().h.a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            }
            return false;
        }
    }

    public final void n() {
        d();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                a().f2047i.a("Utils falling back to Random for random id");
            }
        }
        this.d.set(nextLong);
    }

    public final boolean r() {
        return true;
    }

    public final long t() {
        long andIncrement;
        long j2;
        if (this.d.get() == 0) {
            synchronized (this.d) {
                long nanoTime = System.nanoTime();
                if (((b) this.a.f2092n) != null) {
                    long nextLong = new Random(nanoTime ^ System.currentTimeMillis()).nextLong();
                    int i2 = this.f2138e + 1;
                    this.f2138e = i2;
                    j2 = nextLong + ((long) i2);
                } else {
                    throw null;
                }
            }
            return j2;
        }
        synchronized (this.d) {
            this.d.compareAndSet(-1, 1);
            andIncrement = this.d.getAndIncrement();
        }
        return andIncrement;
    }

    public final SecureRandom u() {
        d();
        if (this.c == null) {
            this.c = new SecureRandom();
        }
        return this.c;
    }

    public final int v() {
        if (this.f2139f == null) {
            f fVar = f.b;
            Context context = this.a.a;
            if (fVar != null) {
                this.f2139f = Integer.valueOf(g.getApkVersion(context) / AnswersRetryFilesSender.BACKOFF_MS);
            } else {
                throw null;
            }
        }
        return this.f2139f.intValue();
    }

    public final String w() {
        byte[] bArr = new byte[16];
        u().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new BigInteger(1, bArr));
    }

    public final boolean d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String a = this.a.g.a("debug.firebase.analytics.app", "");
        h9 h9Var = this.a.f2086f;
        return a.equals(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
     arg types: [int, java.lang.Object, int]
     candidates:
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object */
    public final Object c(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return a(g(str), obj, true);
        }
        return a(g(str), obj, false);
    }

    public final int b(String str) {
        if (!b("user property", str)) {
            return 6;
        }
        if (!a("user property", t5.a, str)) {
            return 15;
        }
        if (!a("user property", 24, str)) {
            return 6;
        }
        return 0;
    }

    public static boolean c(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) == null || !serviceInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String, int, java.lang.Object, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int, java.lang.Object, int]
     candidates:
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String, android.os.Bundle, java.lang.String, long):j.c.a.a.g.a.i
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String, java.lang.String, int):void
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String, int, java.lang.Object, boolean):boolean */
    public final int b(String str, Object obj) {
        boolean z;
        if ("_ldl".equals(str)) {
            z = a("user property referrer", str, g(str), obj, false);
        } else {
            z = a("user property", str, g(str), obj, false);
        }
        return z ? 0 : 7;
    }

    public final boolean c(String str) {
        d();
        if (j.c.a.a.c.o.b.b(this.a.a).a.checkCallingOrSelfPermission(str) == 0) {
            return true;
        }
        a().f2051m.a("Permission not granted", str);
        return false;
    }

    public final boolean b(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo packageInfo = j.c.a.a.c.o.b.b(context).a.getPackageManager().getPackageInfo(str, 64);
            if (packageInfo == null || packageInfo.signatures == null || packageInfo.signatures.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(packageInfo.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (CertificateException e2) {
            a().f2046f.a("Error obtaining certificate", e2);
            return true;
        } catch (PackageManager.NameNotFoundException e3) {
            a().f2046f.a("Package name not found", e3);
            return true;
        }
    }

    public static boolean a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    public final boolean a(String str, String str2) {
        if (str2 == null) {
            a().h.a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            a().h.a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                a().h.a("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    a().h.a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    public static ArrayList<Bundle> b(List<g9> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (g9 next : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", next.b);
            bundle.putString("origin", next.c);
            bundle.putLong("creation_timestamp", next.f1998e);
            bundle.putString(DefaultAppMeasurementEventListenerRegistrar.NAME, next.d.c);
            c.a(bundle, next.d.a());
            bundle.putBoolean("active", next.f1999f);
            String str = next.g;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            i iVar = next.h;
            if (iVar != null) {
                bundle.putString("timed_out_event_name", iVar.b);
                h hVar = next.h.c;
                if (hVar != null) {
                    bundle.putBundle("timed_out_event_params", hVar.b());
                }
            }
            bundle.putLong("trigger_timeout", next.f2000i);
            i iVar2 = next.f2001j;
            if (iVar2 != null) {
                bundle.putString("triggered_event_name", iVar2.b);
                h hVar2 = next.f2001j.c;
                if (hVar2 != null) {
                    bundle.putBundle("triggered_event_params", hVar2.b());
                }
            }
            bundle.putLong("triggered_timestamp", next.d.d);
            bundle.putLong("time_to_live", next.f2002k);
            i iVar3 = next.f2003l;
            if (iVar3 != null) {
                bundle.putString("expired_event_name", iVar3.b);
                h hVar3 = next.f2003l.c;
                if (hVar3 != null) {
                    bundle.putBundle("expired_event_params", hVar3.b());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    public final boolean a(String str, String[] strArr, String str2) {
        boolean z;
        boolean z2;
        if (str2 == null) {
            a().h.a("Name is required and can't be null. Type", str);
            return false;
        }
        ResourcesFlusher.b((Object) str2);
        String[] strArr2 = g;
        int length = strArr2.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                z = false;
                break;
            } else if (str2.startsWith(strArr2[i2])) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            a().h.a("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        }
        if (strArr != null) {
            ResourcesFlusher.b(strArr);
            int length2 = strArr.length;
            int i3 = 0;
            while (true) {
                if (i3 >= length2) {
                    z2 = false;
                    break;
                } else if (d(str2, strArr[i3])) {
                    z2 = true;
                    break;
                } else {
                    i3++;
                }
            }
            if (z2) {
                a().h.a("Name is reserved. Type, name", str, str2);
                return false;
            }
        }
        return true;
    }

    public final boolean a(String str, int i2, String str2) {
        if (str2 == null) {
            a().h.a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i2) {
            return true;
        } else {
            a().h.a("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i2), str2);
            return false;
        }
    }

    public final int a(String str) {
        if (!b("event", str)) {
            return 2;
        }
        if (!a("event", r5.a, str)) {
            return 13;
        }
        if (!a("event", 40, str)) {
            return 2;
        }
        return 0;
    }

    public final boolean a(String str, String str2, int i2, Object obj, boolean z) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                String valueOf = String.valueOf(obj);
                if (valueOf.codePointCount(0, valueOf.length()) > i2) {
                    a().f2049k.a("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                    return false;
                }
            } else if ((obj instanceof Bundle) && z) {
                return true;
            } else {
                if ((obj instanceof Parcelable[]) && z) {
                    for (Parcelable parcelable : (Parcelable[]) obj) {
                        if (!(parcelable instanceof Bundle)) {
                            a().f2049k.a("All Parcelable[] elements must be of type Bundle. Value type, name", parcelable.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof ArrayList) || !z) {
                    return false;
                } else {
                    ArrayList arrayList = (ArrayList) obj;
                    int size = arrayList.size();
                    int i3 = 0;
                    while (i3 < size) {
                        Object obj2 = arrayList.get(i3);
                        i3++;
                        if (!(obj2 instanceof Bundle)) {
                            a().f2049k.a("All ArrayList elements must be of type Bundle. Value type, name", obj2.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return true;
    }

    public static boolean a(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    public static Object a(int i2, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return a(String.valueOf(obj), i2, z);
            }
            return null;
        }
    }

    public static String a(String str, int i2, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i2) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i2))).concat("...");
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
     arg types: [int, java.lang.Object, int]
     candidates:
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object */
    public final Object a(String str, Object obj) {
        int i2 = 256;
        if ("_ev".equals(str)) {
            return a(256, obj, true);
        }
        if (!f(str)) {
            i2 = 100;
        }
        return a(i2, obj, false);
    }

    /* JADX WARN: Type inference failed for: r0v21, types: [java.lang.Throwable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        if (a("event param", 40, r15) == false) goto L_0x0056;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle a(java.lang.String r18, java.lang.String r19, android.os.Bundle r20, java.util.List<java.lang.String> r21, boolean r22, boolean r23) {
        /*
            r17 = this;
            r6 = r17
            r7 = r18
            r8 = r20
            r9 = r21
            r10 = 0
            if (r8 == 0) goto L_0x0190
            android.os.Bundle r11 = new android.os.Bundle
            r11.<init>(r8)
            j.c.a.a.g.a.r4 r0 = r6.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.u0
            boolean r0 = r0.d(r7, r1)
            if (r0 == 0) goto L_0x0026
            java.util.TreeSet r0 = new java.util.TreeSet
            java.util.Set r1 = r20.keySet()
            r0.<init>(r1)
            goto L_0x002a
        L_0x0026:
            java.util.Set r0 = r20.keySet()
        L_0x002a:
            java.util.Iterator r12 = r0.iterator()
            r14 = 0
        L_0x002f:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x018e
            java.lang.Object r0 = r12.next()
            r15 = r0
            java.lang.String r15 = (java.lang.String) r15
            r5 = 40
            r0 = 3
            if (r9 == 0) goto L_0x004a
            boolean r1 = r9.contains(r15)
            if (r1 != 0) goto L_0x0048
            goto L_0x004a
        L_0x0048:
            r3 = 0
            goto L_0x0083
        L_0x004a:
            r1 = 14
            java.lang.String r2 = "event param"
            if (r22 == 0) goto L_0x0068
            boolean r3 = r6.a(r2, r15)
            if (r3 != 0) goto L_0x0058
        L_0x0056:
            r3 = 3
            goto L_0x0069
        L_0x0058:
            boolean r3 = r6.a(r2, r10, r15)
            if (r3 != 0) goto L_0x0061
            r3 = 14
            goto L_0x0069
        L_0x0061:
            boolean r3 = r6.a(r2, r5, r15)
            if (r3 != 0) goto L_0x0068
            goto L_0x0056
        L_0x0068:
            r3 = 0
        L_0x0069:
            if (r3 != 0) goto L_0x0083
            boolean r3 = r6.b(r2, r15)
            if (r3 != 0) goto L_0x0073
        L_0x0071:
            r1 = 3
            goto L_0x0082
        L_0x0073:
            boolean r3 = r6.a(r2, r10, r15)
            if (r3 != 0) goto L_0x007a
            goto L_0x0082
        L_0x007a:
            boolean r1 = r6.a(r2, r5, r15)
            if (r1 != 0) goto L_0x0081
            goto L_0x0071
        L_0x0081:
            r1 = 0
        L_0x0082:
            r3 = r1
        L_0x0083:
            java.lang.String r4 = "_ev"
            r2 = 1
            if (r3 == 0) goto L_0x009f
            boolean r1 = a(r11, r3)
            if (r1 == 0) goto L_0x009a
            java.lang.String r1 = a(r15, r5, r2)
            r11.putString(r4, r1)
            if (r3 != r0) goto L_0x009a
            a(r11, r15)
        L_0x009a:
            r11.remove(r15)
            goto L_0x0147
        L_0x009f:
            java.lang.Object r3 = r8.get(r15)
            r17.d()
            if (r23 == 0) goto L_0x00dc
            boolean r0 = r3 instanceof android.os.Parcelable[]
            if (r0 == 0) goto L_0x00b1
            r0 = r3
            android.os.Parcelable[] r0 = (android.os.Parcelable[]) r0
            int r0 = r0.length
            goto L_0x00bc
        L_0x00b1:
            boolean r0 = r3 instanceof java.util.ArrayList
            if (r0 == 0) goto L_0x00d3
            r0 = r3
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            int r0 = r0.size()
        L_0x00bc:
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 <= r1) goto L_0x00d3
            j.c.a.a.g.a.n3 r1 = r17.a()
            j.c.a.a.g.a.p3 r1 = r1.f2049k
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r2 = "param"
            java.lang.String r5 = "Parameter array is too long; discarded. Value kind, name, array length"
            r1.a(r5, r2, r15, r0)
            r0 = 0
            goto L_0x00d4
        L_0x00d3:
            r0 = 1
        L_0x00d4:
            if (r0 != 0) goto L_0x00dc
            r0 = 17
            r10 = r4
            r13 = 40
            goto L_0x0127
        L_0x00dc:
            j.c.a.a.g.a.r4 r0 = r6.a
            j.c.a.a.g.a.i9 r0 = r0.g
            if (r0 == 0) goto L_0x018c
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.S
            boolean r0 = r0.d(r7, r1)
            if (r0 == 0) goto L_0x00f0
            boolean r0 = f(r19)
            if (r0 != 0) goto L_0x00f6
        L_0x00f0:
            boolean r0 = f(r15)
            if (r0 == 0) goto L_0x010e
        L_0x00f6:
            r5 = 256(0x100, float:3.59E-43)
            java.lang.String r1 = "param"
            r0 = r17
            r13 = 1
            r2 = r15
            r16 = r3
            r3 = r5
            r5 = r4
            r4 = r16
            r10 = r5
            r13 = 40
            r5 = r23
            boolean r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0122
        L_0x010e:
            r16 = r3
            r10 = r4
            r13 = 40
            r3 = 100
            java.lang.String r1 = "param"
            r0 = r17
            r2 = r15
            r4 = r16
            r5 = r23
            boolean r0 = r0.a(r1, r2, r3, r4, r5)
        L_0x0122:
            if (r0 == 0) goto L_0x0126
            r0 = 0
            goto L_0x0127
        L_0x0126:
            r0 = 4
        L_0x0127:
            if (r0 == 0) goto L_0x014a
            boolean r1 = r10.equals(r15)
            if (r1 != 0) goto L_0x014a
            boolean r0 = a(r11, r0)
            if (r0 == 0) goto L_0x0144
            r0 = 1
            java.lang.String r0 = a(r15, r13, r0)
            r11.putString(r10, r0)
            java.lang.Object r0 = r8.get(r15)
            a(r11, r0)
        L_0x0144:
            r11.remove(r15)
        L_0x0147:
            r10 = 0
            goto L_0x002f
        L_0x014a:
            boolean r0 = e(r15)
            if (r0 == 0) goto L_0x0189
            int r14 = r14 + 1
            r0 = 25
            if (r14 <= r0) goto L_0x0189
            r0 = 48
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Event can't contain more than 25 params"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            j.c.a.a.g.a.n3 r1 = r17.a()
            j.c.a.a.g.a.p3 r1 = r1.h
            j.c.a.a.g.a.l3 r2 = r17.g()
            r3 = r19
            java.lang.String r2 = r2.a(r3)
            j.c.a.a.g.a.l3 r4 = r17.g()
            java.lang.String r4 = r4.a(r8)
            r1.a(r0, r2, r4)
            r0 = 5
            a(r11, r0)
            r11.remove(r15)
            goto L_0x0147
        L_0x0189:
            r3 = r19
            goto L_0x0147
        L_0x018c:
            r0 = r10
            throw r0
        L_0x018e:
            r10 = r11
            goto L_0x0191
        L_0x0190:
            r0 = r10
        L_0x0191:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String, android.os.Bundle, java.util.List, boolean, boolean):android.os.Bundle");
    }

    public static boolean a(Bundle bundle, int i2) {
        if (bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i2);
        return true;
    }

    public static void a(Bundle bundle, Object obj) {
        ResourcesFlusher.b(bundle);
        if (obj == null) {
            return;
        }
        if ((obj instanceof String) || (obj instanceof CharSequence)) {
            bundle.putLong("_el", (long) String.valueOf(obj).length());
        }
    }

    public final void a(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (str != null) {
                a().f2049k.a("Not putting event parameter. Invalid value type. name, type", g().b(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    public final void a(int i2, String str, String str2, int i3) {
        a((String) null, i2, str, str2, i3);
    }

    public final void a(String str, int i2, String str2, String str3, int i3) {
        Bundle bundle = new Bundle();
        a(bundle, i2);
        if (this.a.g.d(str, k.o0)) {
            if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
                bundle.putString(str2, str3);
            }
        } else if (!TextUtils.isEmpty(str2)) {
            bundle.putString(str2, str3);
        }
        if (i2 == 6 || i2 == 7 || i2 == 2) {
            bundle.putLong("_el", (long) i3);
        }
        r4 r4Var = this.a;
        h9 h9Var = r4Var.f2086f;
        r4Var.m().a("auto", "_err", bundle);
    }

    public static long a(byte[] bArr) {
        ResourcesFlusher.b(bArr);
        int i2 = 0;
        ResourcesFlusher.b(bArr.length > 0);
        long j2 = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j2 += (((long) bArr[length]) & 255) << i2;
            i2 += 8;
            length--;
        }
        return j2;
    }

    public static boolean a(Context context) {
        ResourcesFlusher.b(context);
        if (Build.VERSION.SDK_INT >= 24) {
            return c(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return c(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    public final Bundle a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String next : bundle.keySet()) {
                Object a = a(next, bundle.get(next));
                if (a == null) {
                    a().f2049k.a("Param value can't be null", g().b(next));
                } else {
                    a(bundle2, next, a);
                }
            }
        }
        return bundle2;
    }

    public final i a(String str, String str2, Bundle bundle, String str3, long j2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (a(str2) == 0) {
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            bundle2.putString("_o", str3);
            return new i(str2, new h(a(a(str, str2, bundle2, Collections.singletonList("_o"), false, false))), str3, j2);
        }
        a().f2046f.a("Invalid conditional property event name", g().c(str2));
        throw new IllegalArgumentException();
    }

    public final long a(Context context, String str) {
        d();
        ResourcesFlusher.b(context);
        ResourcesFlusher.b(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest x = x();
        if (x == null) {
            a().f2046f.a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!b(context, str)) {
                    PackageInfo b = j.c.a.a.c.o.b.b(context).b(this.a.a.getPackageName(), 64);
                    if (b.signatures != null && b.signatures.length > 0) {
                        return a(x.digest(b.signatures[0].toByteArray()));
                    }
                    a().f2047i.a("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                a().f2046f.a("Package name not found", e2);
            }
        }
        return 0;
    }

    public static byte[] a(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    public static long a(long j2, long j3) {
        return ((j3 * 60000) + j2) / 86400000;
    }

    public final void a(Bundle bundle, long j2) {
        long j3 = bundle.getLong("_et");
        if (j3 != 0) {
            a().f2047i.a("Params already contained engagement", Long.valueOf(j3));
        }
        bundle.putLong("_et", j2 + j3);
    }

    public final void a(fb fbVar, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning string value to wrapper", e2);
        }
    }

    public final void a(fb fbVar, long j2) {
        Bundle bundle = new Bundle();
        bundle.putLong("r", j2);
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning long value to wrapper", e2);
        }
    }

    public final void a(fb fbVar, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i2);
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning int value to wrapper", e2);
        }
    }

    public final void a(fb fbVar, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning byte array to wrapper", e2);
        }
    }

    public final void a(fb fbVar, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z);
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning boolean value to wrapper", e2);
        }
    }

    public final void a(fb fbVar, Bundle bundle) {
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning bundle value to wrapper", e2);
        }
    }

    public static Bundle a(List<x8> list) {
        Bundle bundle = new Bundle();
        if (list == null) {
            return bundle;
        }
        for (x8 next : list) {
            String str = next.f2129f;
            if (str != null) {
                bundle.putString(next.c, str);
            } else {
                Long l2 = next.f2128e;
                if (l2 != null) {
                    bundle.putLong(next.c, l2.longValue());
                } else {
                    Double d2 = next.h;
                    if (d2 != null) {
                        bundle.putDouble(next.c, d2.doubleValue());
                    }
                }
            }
        }
        return bundle;
    }

    public final void a(fb fbVar, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning bundle list to wrapper", e2);
        }
    }
}
