package j.c.a.a.i;

import java.util.concurrent.Executor;

public final class l<TResult, TContinuationResult> implements b, c, d<TContinuationResult>, v<TResult> {
    public final Executor a;
    public final a<TResult, e<TContinuationResult>> b;
    public final y<TContinuationResult> c;

    public l(Executor executor, a<TResult, e<TContinuationResult>> aVar, y<TContinuationResult> yVar) {
        this.a = executor;
        this.b = aVar;
        this.c = yVar;
    }

    public final void a(e<TResult> eVar) {
        this.a.execute(new m(this, eVar));
    }

    public final void a(TContinuationResult tcontinuationresult) {
        this.c.a((Object) tcontinuationresult);
    }

    public final void a(Exception exc) {
        this.c.a(exc);
    }

    public final void a() {
        this.c.e();
    }
}
