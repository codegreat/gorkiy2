package j.c.a.a.g.a;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class b6 implements Runnable {
    public final /* synthetic */ long b;
    public final /* synthetic */ x5 c;

    public b6(x5 x5Var, long j2) {
        this.c = x5Var;
        this.b = j2;
    }

    public final void run() {
        x5 x5Var = this.c;
        long j2 = this.b;
        x5Var.d();
        x5Var.b();
        x5Var.w();
        x5Var.a().f2051m.a("Resetting analytics data (FE)");
        f8 u = x5Var.u();
        u.d();
        u.f1990f.b();
        u.g.b();
        u.d = 0;
        u.f1989e = 0;
        i9 i9Var = x5Var.a.g;
        g3 q2 = x5Var.q();
        q2.w();
        String str = q2.c;
        if (i9Var != null) {
            if (i9Var.d(str, k.U)) {
                x5Var.l().f2114j.a(j2);
            }
            boolean c2 = x5Var.a.c();
            if (!x5Var.a.g.q()) {
                x5Var.l().b(!c2);
            }
            z6 r2 = x5Var.r();
            r2.d();
            r2.b();
            r2.w();
            d9 a = r2.a(false);
            r2.C();
            r2.t().z();
            r2.a(new e7(r2, a));
            x5Var.h = !c2;
            z6 r3 = this.c.r();
            AtomicReference atomicReference = new AtomicReference();
            r3.d();
            r3.w();
            r3.a(new g7(r3, atomicReference, r3.a(false)));
            return;
        }
        throw null;
    }
}
