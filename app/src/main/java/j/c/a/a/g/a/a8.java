package j.c.a.a.g.a;

import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final /* synthetic */ class a8 implements Runnable {
    public final x7 b;
    public final int c;
    public final n3 d;

    /* renamed from: e  reason: collision with root package name */
    public final Intent f1937e;

    public a8(x7 x7Var, int i2, n3 n3Var, Intent intent) {
        this.b = x7Var;
        this.c = i2;
        this.d = n3Var;
        this.f1937e = intent;
    }

    public final void run() {
        x7 x7Var = this.b;
        int i2 = this.c;
        n3 n3Var = this.d;
        Intent intent = this.f1937e;
        if (((c8) x7Var.a).a(i2)) {
            n3Var.f2052n.a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i2));
            x7Var.a().f2052n.a("Completed wakeful intent.");
            ((c8) x7Var.a).a(intent);
        }
    }
}
