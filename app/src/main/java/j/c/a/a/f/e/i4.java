package j.c.a.a.f.e;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i4<K> implements Map.Entry<K, Object> {
    public Map.Entry<K, g4> b;

    public /* synthetic */ i4(Map.Entry entry, j4 j4Var) {
        this.b = entry;
    }

    public final K getKey() {
        return this.b.getKey();
    }

    public final Object getValue() {
        if (this.b.getValue() == null) {
            return null;
        }
        g4.c();
        throw null;
    }

    public final Object setValue(Object obj) {
        if (obj instanceof f5) {
            g4 value = this.b.getValue();
            f5 f5Var = value.a;
            value.b = null;
            value.a = (f5) obj;
            return f5Var;
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
