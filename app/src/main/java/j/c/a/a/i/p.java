package j.c.a.a.i;

import java.util.concurrent.Executor;
import javax.annotation.concurrent.GuardedBy;

public final class p<TResult> implements v<TResult> {
    public final Executor a;
    public final Object b = new Object();
    @GuardedBy("mLock")
    public c c;

    public p(Executor executor, c cVar) {
        this.a = executor;
        this.c = cVar;
    }

    public final void a(e<TResult> eVar) {
        if (!eVar.d() && !((y) eVar).d) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new q(this, eVar));
                }
            }
        }
    }
}
