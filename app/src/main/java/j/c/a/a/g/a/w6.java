package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class w6 {
    public final String a;
    public final String b;
    public final long c;
    public boolean d = false;

    public w6(String str, String str2, long j2) {
        this.a = str;
        this.b = str2;
        this.c = j2;
    }
}
