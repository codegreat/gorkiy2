package j.c.a.a.f.e;

import android.database.Cursor;
import i.e.ArrayMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final /* synthetic */ class h1 implements k1 {
    public final e1 a;

    public h1(e1 e1Var) {
        this.a = e1Var;
    }

    /* JADX INFO: finally extract failed */
    public final Object a() {
        Object obj;
        e1 e1Var = this.a;
        Cursor query = e1Var.a.query(e1Var.b, e1.h, null, null, null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                Map emptyMap = Collections.emptyMap();
                query.close();
                return emptyMap;
            }
            if (count <= 256) {
                obj = new ArrayMap(count);
            } else {
                obj = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                obj.put(query.getString(0), query.getString(1));
            }
            query.close();
            return obj;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }
}
