package j.c.a.a.g.a;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.n.e;
import j.c.a.a.c.o.b;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i9 extends o5 {
    public Boolean b;
    public k9 c = l9.a;
    public Boolean d;

    public i9(r4 r4Var) {
        super(r4Var);
        k.c = r4Var;
    }

    public static long u() {
        return k.I.a(null).longValue();
    }

    public static boolean v() {
        return k.f2009e.a(null).booleanValue();
    }

    public final int a(String str) {
        return b(str, k.f2022t);
    }

    public final int b(String str, b3<Integer> b3Var) {
        if (str == null) {
            return b3Var.a(null).intValue();
        }
        String a = this.c.a(str, b3Var.a);
        if (TextUtils.isEmpty(a)) {
            return b3Var.a(null).intValue();
        }
        try {
            return b3Var.a(Integer.valueOf(Integer.parseInt(a))).intValue();
        } catch (NumberFormatException unused) {
            return b3Var.a(null).intValue();
        }
    }

    public final double c(String str, b3<Double> b3Var) {
        if (str == null) {
            return b3Var.a(null).doubleValue();
        }
        String a = this.c.a(str, b3Var.a);
        if (TextUtils.isEmpty(a)) {
            return b3Var.a(null).doubleValue();
        }
        try {
            return b3Var.a(Double.valueOf(Double.parseDouble(a))).doubleValue();
        } catch (NumberFormatException unused) {
            return b3Var.a(null).doubleValue();
        }
    }

    public final boolean d(String str, b3<Boolean> b3Var) {
        if (str == null) {
            return b3Var.a(null).booleanValue();
        }
        String a = this.c.a(str, b3Var.a);
        if (TextUtils.isEmpty(a)) {
            return b3Var.a(null).booleanValue();
        }
        return b3Var.a(Boolean.valueOf(Boolean.parseBoolean(a))).booleanValue();
    }

    public final boolean e(String str, b3<Boolean> b3Var) {
        return d(str, b3Var);
    }

    public final boolean f(String str) {
        return d(str, k.W);
    }

    public final boolean g(String str) {
        return d(str, k.Z);
    }

    public final boolean h(String str) {
        return d(str, k.g0);
    }

    public final Bundle n() {
        try {
            if (super.a.a.getPackageManager() == null) {
                a().f2046f.a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a = b.b(super.a.a).a(super.a.a.getPackageName(), 128);
            if (a != null) {
                return a.metaData;
            }
            a().f2046f.a("Failed to load metadata: ApplicationInfo is null");
            return null;
        } catch (PackageManager.NameNotFoundException e2) {
            a().f2046f.a("Failed to load metadata: Package name not found", e2);
            return null;
        }
    }

    public final long o() {
        h9 h9Var = super.a.f2086f;
        return 18079;
    }

    public final boolean p() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    ApplicationInfo applicationInfo = super.a.a.getApplicationInfo();
                    String a = e.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.d = Boolean.valueOf(str != null && str.equals(a));
                    }
                    if (this.d == null) {
                        this.d = Boolean.TRUE;
                        a().f2046f.a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.d.booleanValue();
    }

    public final boolean q() {
        h9 h9Var = super.a.f2086f;
        Boolean b2 = b("firebase_analytics_collection_deactivated");
        return b2 != null && b2.booleanValue();
    }

    public final Boolean r() {
        h9 h9Var = super.a.f2086f;
        return b("firebase_analytics_collection_enabled");
    }

    public final Boolean s() {
        b();
        Boolean b2 = b("google_analytics_adid_collection_enabled");
        return Boolean.valueOf(b2 == null || b2.booleanValue());
    }

    public final boolean t() {
        if (this.b == null) {
            Boolean b2 = b("app_measurement_lite");
            this.b = b2;
            if (b2 == null) {
                this.b = false;
            }
        }
        if (this.b.booleanValue() || !super.a.f2085e) {
            return true;
        }
        return false;
    }

    public final long a(String str, b3<Long> b3Var) {
        if (str == null) {
            return b3Var.a(null).longValue();
        }
        String a = this.c.a(str, b3Var.a);
        if (TextUtils.isEmpty(a)) {
            return b3Var.a(null).longValue();
        }
        try {
            return b3Var.a(Long.valueOf(Long.parseLong(a))).longValue();
        } catch (NumberFormatException unused) {
            return b3Var.a(null).longValue();
        }
    }

    public final boolean e(String str) {
        return d(str, k.V);
    }

    public final boolean d(String str) {
        return d(str, k.T);
    }

    public final Boolean b(String str) {
        ResourcesFlusher.b(str);
        Bundle n2 = n();
        if (n2 == null) {
            a().f2046f.a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (!n2.containsKey(str)) {
            return null;
        } else {
            return Boolean.valueOf(n2.getBoolean(str));
        }
    }

    public final boolean c(String str) {
        return "1".equals(this.c.a(str, "measurement.event_sampling_enabled"));
    }

    public final boolean a(b3<Boolean> b3Var) {
        return d(null, b3Var);
    }

    public final String a(String str, String str2) {
        Class<String> cls = String.class;
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", cls, cls).invoke(null, str, str2);
        } catch (ClassNotFoundException e2) {
            a().f2046f.a("Could not find SystemProperties class", e2);
            return str2;
        } catch (NoSuchMethodException e3) {
            a().f2046f.a("Could not find SystemProperties.get() method", e3);
            return str2;
        } catch (IllegalAccessException e4) {
            a().f2046f.a("Could not access SystemProperties.get()", e4);
            return str2;
        } catch (InvocationTargetException e5) {
            a().f2046f.a("SystemProperties.get() threw an exception", e5);
            return str2;
        }
    }
}
