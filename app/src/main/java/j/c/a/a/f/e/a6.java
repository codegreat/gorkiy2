package j.c.a.a.f.e;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a6 implements Comparable<a6>, Map.Entry<K, V> {
    public final K b;
    public V c;
    public final /* synthetic */ x5 d;

    public a6(x5 x5Var, Map.Entry<K, V> entry) {
        V value = entry.getValue();
        this.d = x5Var;
        this.b = (Comparable) entry.getKey();
        this.c = value;
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return this.b.compareTo(((a6) obj).b);
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        K k2 = this.b;
        Object key = entry.getKey();
        if (k2 == null) {
            z = key == null;
        } else {
            z = k2.equals(key);
        }
        if (z) {
            V v = this.c;
            Object value = entry.getValue();
            if (v == null) {
                z2 = value == null;
            } else {
                z2 = v.equals(value);
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final /* synthetic */ Object getKey() {
        return this.b;
    }

    public final V getValue() {
        return this.c;
    }

    public final int hashCode() {
        K k2 = this.b;
        int i2 = 0;
        int hashCode = k2 == null ? 0 : k2.hashCode();
        V v = this.c;
        if (v != null) {
            i2 = v.hashCode();
        }
        return hashCode ^ i2;
    }

    public final V setValue(V v) {
        this.d.d();
        V v2 = this.c;
        this.c = v;
        return v2;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.b);
        String valueOf2 = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(valueOf2.length() + valueOf.length() + 1);
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        return sb.toString();
    }

    public a6(x5 x5Var, K k2, V v) {
        this.d = x5Var;
        this.b = k2;
        this.c = v;
    }
}
