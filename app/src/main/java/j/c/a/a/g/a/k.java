package j.c.a.a.g.a;

import android.content.Context;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.crashlytics.android.core.LogFileManager;
import j.c.a.a.c.f;
import j.c.a.a.c.g;
import j.c.a.a.f.e.e1;
import j.c.a.a.f.e.q1;
import j.c.a.a.f.e.s8;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class k {
    public static b3<Long> A = a("measurement.upload.debug_upload_interval", 1000L, 1000L, e0.a);
    public static b3<Boolean> A0 = a("measurement.service.audience.use_bundle_timestamp_for_property_filters", false, false, k2.a);
    public static b3<Long> B = a("measurement.upload.minimum_delay", 500L, 500L, d0.a);
    public static b3<Boolean> B0 = a("measurement.service.audience.not_prepend_timestamps_for_sequence_session_scoped_filters", false, false, j2.a);
    public static b3<Long> C = a("measurement.alarm_manager.minimum_interval", 60000L, 60000L, g0.a);
    public static b3<Boolean> C0 = a("measurement.sdk.collection.retrieve_deeplink_from_bow", false, false, m2.a);
    public static b3<Long> D = a("measurement.upload.stale_data_deletion_interval", 86400000L, 86400000L, f0.a);
    public static b3<Boolean> D0 = a("measurement.app_launch.event_ordering_fix", false, false, l2.a);
    public static b3<Long> E = a("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L, i0.a);
    public static b3<Boolean> E0 = a("measurement.sdk.collection.last_deep_link_referrer", false, false, o2.a);
    public static b3<Long> F = a("measurement.upload.initial_upload_delay_time", 15000L, 15000L, h0.a);
    public static b3<Boolean> F0 = a("measurement.sdk.collection.last_gclid_from_referrer", false, false, p2.a);
    public static b3<Long> G = a("measurement.upload.retry_time", 1800000L, 1800000L, k0.a);
    public static b3<Boolean> G0 = a("measurement.upload.file_lock_state_check", false, false, s2.a);
    public static b3<Integer> H = a("measurement.upload.retry_count", 6, 6, m0.a);
    public static b3<Boolean> H0 = a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true, true, r2.a);
    public static b3<Long> I = a("measurement.upload.max_queue_time", 2419200000L, 2419200000L, l0.a);
    public static b3<Boolean> I0 = a("measurement.lifecycle.app_backgrounded_tracking", false, false, t2.a);
    public static b3<Integer> J = a("measurement.lifetimevalue.max_currency_tracked", 4, 4, o0.a);
    public static b3<Boolean> J0 = a("measurement.lifecycle.app_in_background_parameter", false, false, w2.a);
    public static b3<Integer> K = a("measurement.audience.filter_result_max_count", 200, 200, n0.a);
    public static b3<Boolean> K0 = a("measurement.integration.disable_firebase_instance_id", false, false, v2.a);
    public static b3<Long> L = a("measurement.service_client.idle_disconnect_millis", 5000L, 5000L, q0.a);
    public static b3<Boolean> L0 = a("measurement.lifecycle.app_backgrounded_engagement", false, false, y2.a);
    public static b3<Boolean> M = a("measurement.test.boolean_flag", false, false, p0.a);
    public static b3<Boolean> M0 = a("measurement.service.fix_gmp_version", false, false, x2.a);
    public static b3<String> N = a("measurement.test.string_flag", "---", "---", s0.a);
    public static b3<Long> O = a("measurement.test.long_flag", -1L, -1L, r0.a);
    public static b3<Integer> P = a("measurement.test.int_flag", -2, -2, u0.a);
    public static b3<Double> Q;
    public static b3<Integer> R = a("measurement.experiment.max_ids", 50, 50, v0.a);
    public static b3<Boolean> S = a("measurement.validation.internal_limits_internal_event_params", false, false, y0.a);
    public static b3<Boolean> T = a("measurement.audience.dynamic_filters", true, true, x0.a);
    public static b3<Boolean> U = a("measurement.reset_analytics.persist_time", false, false, a1.a);
    public static b3<Boolean> V = a("measurement.validation.value_and_currency_params", true, true, z0.a);
    public static b3<Boolean> W = a("measurement.sampling.time_zone_offset_enabled", false, false, d1.a);
    public static b3<Boolean> X = a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false, false, c1.a);
    public static b3<Boolean> Y = a("measurement.client.sessions.session_id_enabled", false, false, f1.a);
    public static b3<Boolean> Z = a("measurement.service.sessions.session_number_enabled", true, true, e1.a);
    public static h9 a;
    public static b3<Boolean> a0 = a("measurement.client.sessions.immediate_start_enabled_foreground", false, false, h1.a);
    public static List<b3<?>> b = Collections.synchronizedList(new ArrayList());
    public static b3<Boolean> b0 = a("measurement.client.sessions.background_sessions_enabled", false, false, j1.a);
    public static volatile r4 c;
    public static b3<Boolean> c0 = a("measurement.client.sessions.remove_expired_session_properties_enabled", false, false, i1.a);
    public static Boolean d;
    public static b3<Boolean> d0 = a("measurement.service.sessions.session_number_backfill_enabled", true, true, l1.a);

    /* renamed from: e  reason: collision with root package name */
    public static b3<Boolean> f2009e = a("measurement.upload_dsid_enabled", false, false, m.a);
    public static b3<Boolean> e0 = a("measurement.service.sessions.remove_disabled_session_number", true, true, k1.a);

    /* renamed from: f  reason: collision with root package name */
    public static b3<String> f2010f = a("measurement.log_tag", "FA", "FA-SVC", z.a);
    public static b3<Boolean> f0 = a("measurement.collection.firebase_global_collection_flag_enabled", true, true, n1.a);
    public static b3<Long> g = a("measurement.ad_id_cache_time", 10000L, 10000L, j0.a);
    public static b3<Boolean> g0 = a("measurement.collection.efficient_engagement_reporting_enabled", false, false, m1.a);
    public static b3<Long> h = a("measurement.monitoring.sample_period_millis", 86400000L, 86400000L, w0.a);
    public static b3<Boolean> h0 = a("measurement.collection.redundant_engagement_removal_enabled", false, false, p1.a);

    /* renamed from: i  reason: collision with root package name */
    public static b3<Long> f2011i = a("measurement.config.cache_time", 86400000L, 3600000L, g1.a);
    public static b3<Boolean> i0 = a("measurement.personalized_ads_signals_collection_enabled", true, true, o1.a);

    /* renamed from: j  reason: collision with root package name */
    public static b3<String> f2012j = a("measurement.config.url_scheme", "https", "https", t1.a);
    public static b3<Boolean> j0 = a("measurement.personalized_ads_property_translation_enabled", true, true, r1.a);

    /* renamed from: k  reason: collision with root package name */
    public static b3<String> f2013k = a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com", d2.a);
    public static b3<Boolean> k0 = a("measurement.collection.init_params_control_enabled", true, true, q1.a);

    /* renamed from: l  reason: collision with root package name */
    public static b3<Integer> f2014l = a("measurement.upload.max_bundles", 100, 100, q2.a);
    public static b3<Boolean> l0 = a("measurement.upload.disable_is_uploader", true, true, s1.a);

    /* renamed from: m  reason: collision with root package name */
    public static b3<Integer> f2015m;
    public static b3<Boolean> m0 = a("measurement.experiment.enable_experiment_reporting", true, true, v1.a);

    /* renamed from: n  reason: collision with root package name */
    public static b3<Integer> f2016n;
    public static b3<Boolean> n0 = a("measurement.collection.log_event_and_bundle_v2", true, true, u1.a);

    /* renamed from: o  reason: collision with root package name */
    public static b3<Integer> f2017o;
    public static b3<Boolean> o0 = a("measurement.collection.null_empty_event_name_fix", true, true, x1.a);

    /* renamed from: p  reason: collision with root package name */
    public static b3<Integer> f2018p = a("measurement.upload.max_events_per_day", 100000, 100000, r.a);
    public static b3<Boolean> p0 = a("measurement.audience.sequence_filters", true, true, w1.a);

    /* renamed from: q  reason: collision with root package name */
    public static b3<Integer> f2019q;
    public static b3<Boolean> q0 = a("measurement.quality.checksum", false, false, null);

    /* renamed from: r  reason: collision with root package name */
    public static b3<Integer> f2020r = a("measurement.upload.max_public_events_per_day", 50000, 50000, t.a);
    public static b3<Boolean> r0 = a("measurement.module.collection.conditionally_omit_admob_app_id", true, true, z1.a);

    /* renamed from: s  reason: collision with root package name */
    public static b3<Integer> f2021s = a("measurement.upload.max_conversions_per_day", 500, 500, s.a);
    public static b3<Boolean> s0 = a("measurement.sdk.dynamite.use_dynamite2", false, false, y1.a);

    /* renamed from: t  reason: collision with root package name */
    public static b3<Integer> f2022t = a("measurement.upload.max_realtime_events_per_day", 10, 10, v.a);
    public static b3<Boolean> t0 = a("measurement.sdk.dynamite.allow_remote_dynamite", false, false, b2.a);
    public static b3<Integer> u = a("measurement.store.max_stored_events_per_app", 100000, 100000, u.a);
    public static b3<Boolean> u0 = a("measurement.sdk.collection.validate_param_names_alphabetical", false, false, a2.a);
    public static b3<String> v = a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a", x.a);
    public static b3<Boolean> v0 = a("measurement.collection.event_safelist", false, false, e2.a);
    public static b3<Long> w = a("measurement.upload.backoff_period", 43200000L, 43200000L, w.a);
    public static b3<Boolean> w0 = a("measurement.service.audience.scoped_filters_v27", false, false, g2.a);
    public static b3<Long> x = a("measurement.upload.window_interval", 3600000L, 3600000L, y.a);
    public static b3<Boolean> x0 = a("measurement.service.audience.session_scoped_event_aggregates", false, false, f2.a);
    public static b3<Long> y = a("measurement.upload.interval", 3600000L, 3600000L, c0.a);
    public static b3<Boolean> y0 = a("measurement.service.audience.session_scoped_user_engagement", false, false, i2.a);
    public static b3<Long> z = a("measurement.upload.realtime_upload_interval", 10000L, 10000L, b0.a);
    public static b3<Boolean> z0 = a("measurement.service.audience.remove_disabled_session_scoped_user_engagement", false, false, h2.a);

    static {
        Collections.synchronizedSet(new HashSet());
        a("measurement.log_androidId_enabled", false, false, n.a);
        Integer valueOf = Integer.valueOf((int) LogFileManager.MAX_LOG_SIZE);
        f2015m = a("measurement.upload.max_batch_size", valueOf, valueOf, a3.a);
        f2016n = a("measurement.upload.max_bundle_size", valueOf, valueOf, p.a);
        Integer valueOf2 = Integer.valueOf((int) AnswersRetryFilesSender.BACKOFF_MS);
        f2017o = a("measurement.upload.max_events_per_bundle", valueOf2, valueOf2, o.a);
        f2019q = a("measurement.upload.max_error_events_per_day", valueOf2, valueOf2, q.a);
        Double valueOf3 = Double.valueOf(-3.0d);
        Q = a("measurement.test.double_flag", valueOf3, valueOf3, t0.a);
        a("measurement.sdk.collection.last_deep_link_referrer_campaign", false, false, n2.a);
        s8.c.a();
        a("measurement.ga.ga_app_id", false, true, u2.a);
    }

    public static Map<String, String> a(Context context) {
        e1 a2 = e1.a(context.getContentResolver(), q1.a("com.google.android.gms.measurement"));
        return a2 == null ? Collections.emptyMap() : a2.a();
    }

    public static void a(Exception exc) {
        if (c != null) {
            Context context = c.a;
            if (d == null) {
                d = Boolean.valueOf(f.b.a(context, g.GOOGLE_PLAY_SERVICES_VERSION_CODE) == 0);
            }
            if (d.booleanValue()) {
                c.a().f2046f.a("Got Exception on PhenotypeFlag.get on Play device", exc);
            }
        }
    }

    public static <V> b3<V> a(String str, V v2, V v3, c3<V> c3Var) {
        b3 b3Var = new b3(str, v2, v3, c3Var, null);
        b.add(b3Var);
        return b3Var;
    }
}
