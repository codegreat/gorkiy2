package j.c.a.a.c.l;

import android.accounts.Account;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.api.Scope;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.c.a.a.c.k.e.n;
import j.c.a.a.c.k.e.o;
import j.c.a.a.c.l.h;
import j.c.a.a.c.l.k;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.concurrent.GuardedBy;

public abstract class b<T extends IInterface> {
    public static final j.c.a.a.c.d[] v = new j.c.a.a.c.d[0];
    public a0 a;
    public final Context b;
    public final h c;
    public final j.c.a.a.c.f d;

    /* renamed from: e  reason: collision with root package name */
    public final Handler f1800e;

    /* renamed from: f  reason: collision with root package name */
    public final Object f1801f = new Object();
    public final Object g = new Object();
    @GuardedBy("mServiceBrokerLock")
    public m h;

    /* renamed from: i  reason: collision with root package name */
    public c f1802i;
    @GuardedBy("mLock")

    /* renamed from: j  reason: collision with root package name */
    public T f1803j;

    /* renamed from: k  reason: collision with root package name */
    public final ArrayList<h<?>> f1804k = new ArrayList<>();
    @GuardedBy("mLock")

    /* renamed from: l  reason: collision with root package name */
    public j f1805l;
    @GuardedBy("mLock")

    /* renamed from: m  reason: collision with root package name */
    public int f1806m = 1;

    /* renamed from: n  reason: collision with root package name */
    public final a f1807n;

    /* renamed from: o  reason: collision with root package name */
    public final C0023b f1808o;

    /* renamed from: p  reason: collision with root package name */
    public final int f1809p;

    /* renamed from: q  reason: collision with root package name */
    public final String f1810q;

    /* renamed from: r  reason: collision with root package name */
    public j.c.a.a.c.b f1811r = null;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1812s = false;

    /* renamed from: t  reason: collision with root package name */
    public volatile u f1813t = null;
    public AtomicInteger u = new AtomicInteger(0);

    public interface a {
        void a(int i2);

        void b(Bundle bundle);
    }

    /* renamed from: j.c.a.a.c.l.b$b  reason: collision with other inner class name */
    public interface C0023b {
        void a(j.c.a.a.c.b bVar);
    }

    public interface c {
        void a(j.c.a.a.c.b bVar);
    }

    public class d implements c {
        public d() {
        }

        public void a(j.c.a.a.c.b bVar) {
            if (bVar.c()) {
                b bVar2 = b.this;
                bVar2.a((j) null, bVar2.m());
                return;
            }
            C0023b bVar3 = b.this.f1808o;
            if (bVar3 != null) {
                bVar3.a(bVar);
            }
        }
    }

    public interface e {
    }

    public abstract class f extends h<Boolean> {
        public final int d;

        /* renamed from: e  reason: collision with root package name */
        public final Bundle f1814e;

        public f(int i2, Bundle bundle) {
            super(true);
            this.d = i2;
            this.f1814e = bundle;
        }

        public abstract void a(j.c.a.a.c.b bVar);

        /* JADX WARN: Type inference failed for: r5v11, types: [android.os.Parcelable] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final /* synthetic */ void a(java.lang.Object r5) {
            /*
                r4 = this;
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                r0 = 1
                r1 = 0
                if (r5 != 0) goto L_0x000c
                j.c.a.a.c.l.b r5 = j.c.a.a.c.l.b.this
                r5.b(r0, r1)
                return
            L_0x000c:
                int r5 = r4.d
                if (r5 == 0) goto L_0x0061
                r2 = 10
                if (r5 == r2) goto L_0x0031
                j.c.a.a.c.l.b r5 = j.c.a.a.c.l.b.this
                r5.b(r0, r1)
                android.os.Bundle r5 = r4.f1814e
                if (r5 == 0) goto L_0x0026
                java.lang.String r0 = "pendingIntent"
                android.os.Parcelable r5 = r5.getParcelable(r0)
                r1 = r5
                android.app.PendingIntent r1 = (android.app.PendingIntent) r1
            L_0x0026:
                j.c.a.a.c.b r5 = new j.c.a.a.c.b
                int r0 = r4.d
                r5.<init>(r0, r1)
                r4.a(r5)
                goto L_0x0076
            L_0x0031:
                j.c.a.a.c.l.b r5 = j.c.a.a.c.l.b.this
                r5.b(r0, r1)
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                r1 = 3
                java.lang.Object[] r1 = new java.lang.Object[r1]
                r2 = 0
                java.lang.Class r3 = r4.getClass()
                java.lang.String r3 = r3.getSimpleName()
                r1[r2] = r3
                j.c.a.a.c.l.b r2 = j.c.a.a.c.l.b.this
                java.lang.String r2 = r2.p()
                r1[r0] = r2
                r0 = 2
                j.c.a.a.c.l.b r2 = j.c.a.a.c.l.b.this
                java.lang.String r2 = r2.o()
                r1[r0] = r2
                java.lang.String r0 = "A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. "
                java.lang.String r0 = java.lang.String.format(r0, r1)
                r5.<init>(r0)
                throw r5
            L_0x0061:
                boolean r5 = r4.d()
                if (r5 != 0) goto L_0x0076
                j.c.a.a.c.l.b r5 = j.c.a.a.c.l.b.this
                r5.b(r0, r1)
                j.c.a.a.c.b r5 = new j.c.a.a.c.b
                r0 = 8
                r5.<init>(r0, r1)
                r4.a(r5)
            L_0x0076:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.l.b.f.a(java.lang.Object):void");
        }

        public abstract boolean d();
    }

    public final class g extends j.c.a.a.f.c.d {
        public g(Looper looper) {
            super(looper);
        }

        public static void a(Message message) {
            h hVar = (h) message.obj;
            if (((f) hVar) != null) {
                hVar.b();
                return;
            }
            throw null;
        }

        public static boolean b(Message message) {
            int i2 = message.what;
            return i2 == 2 || i2 == 1 || i2 == 7;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
            if (r0 == 5) goto L_0x002b;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void handleMessage(android.os.Message r8) {
            /*
                r7 = this;
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                java.util.concurrent.atomic.AtomicInteger r0 = r0.u
                int r0 = r0.get()
                int r1 = r8.arg1
                if (r0 == r1) goto L_0x0016
                boolean r0 = b(r8)
                if (r0 == 0) goto L_0x0015
                a(r8)
            L_0x0015:
                return
            L_0x0016:
                int r0 = r8.what
                r1 = 4
                r2 = 1
                r3 = 5
                r4 = 0
                if (r0 == r2) goto L_0x002b
                r5 = 7
                if (r0 == r5) goto L_0x002b
                if (r0 != r1) goto L_0x0029
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                if (r0 == 0) goto L_0x0028
                goto L_0x002b
            L_0x0028:
                throw r4
            L_0x0029:
                if (r0 != r3) goto L_0x0037
            L_0x002b:
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                boolean r0 = r0.a()
                if (r0 != 0) goto L_0x0037
                a(r8)
                return
            L_0x0037:
                int r0 = r8.what
                r5 = 8
                r6 = 3
                if (r0 != r1) goto L_0x0077
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                j.c.a.a.c.b r1 = new j.c.a.a.c.b
                int r8 = r8.arg2
                r1.<init>(r8)
                r0.f1811r = r1
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                boolean r8 = j.c.a.a.c.l.b.b(r8)
                if (r8 == 0) goto L_0x005b
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                boolean r0 = r8.f1812s
                if (r0 != 0) goto L_0x005b
                r8.b(r6, r4)
                return
            L_0x005b:
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                j.c.a.a.c.b r8 = r8.f1811r
                if (r8 == 0) goto L_0x0062
                goto L_0x0067
            L_0x0062:
                j.c.a.a.c.b r8 = new j.c.a.a.c.b
                r8.<init>(r5)
            L_0x0067:
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                j.c.a.a.c.l.b$c r0 = r0.f1802i
                r0.a(r8)
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                if (r8 == 0) goto L_0x0076
                java.lang.System.currentTimeMillis()
                return
            L_0x0076:
                throw r4
            L_0x0077:
                if (r0 != r3) goto L_0x0095
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                j.c.a.a.c.b r8 = r8.f1811r
                if (r8 == 0) goto L_0x0080
                goto L_0x0085
            L_0x0080:
                j.c.a.a.c.b r8 = new j.c.a.a.c.b
                r8.<init>(r5)
            L_0x0085:
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                j.c.a.a.c.l.b$c r0 = r0.f1802i
                r0.a(r8)
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                if (r8 == 0) goto L_0x0094
                java.lang.System.currentTimeMillis()
                return
            L_0x0094:
                throw r4
            L_0x0095:
                if (r0 != r6) goto L_0x00b8
                java.lang.Object r0 = r8.obj
                boolean r1 = r0 instanceof android.app.PendingIntent
                if (r1 == 0) goto L_0x00a0
                android.app.PendingIntent r0 = (android.app.PendingIntent) r0
                goto L_0x00a1
            L_0x00a0:
                r0 = r4
            L_0x00a1:
                j.c.a.a.c.b r1 = new j.c.a.a.c.b
                int r8 = r8.arg2
                r1.<init>(r8, r0)
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                j.c.a.a.c.l.b$c r8 = r8.f1802i
                r8.a(r1)
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                if (r8 == 0) goto L_0x00b7
                java.lang.System.currentTimeMillis()
                return
            L_0x00b7:
                throw r4
            L_0x00b8:
                r1 = 6
                if (r0 != r1) goto L_0x00d9
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                r0.b(r3, r4)
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                j.c.a.a.c.l.b$a r0 = r0.f1807n
                if (r0 == 0) goto L_0x00cb
                int r8 = r8.arg2
                r0.a(r8)
            L_0x00cb:
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                if (r8 == 0) goto L_0x00d8
                java.lang.System.currentTimeMillis()
                j.c.a.a.c.l.b r8 = j.c.a.a.c.l.b.this
                r8.a(r3, r2, r4)
                return
            L_0x00d8:
                throw r4
            L_0x00d9:
                r1 = 2
                if (r0 != r1) goto L_0x00e8
                j.c.a.a.c.l.b r0 = j.c.a.a.c.l.b.this
                boolean r0 = r0.c()
                if (r0 != 0) goto L_0x00e8
                a(r8)
                return
            L_0x00e8:
                boolean r0 = b(r8)
                if (r0 == 0) goto L_0x00f6
                java.lang.Object r8 = r8.obj
                j.c.a.a.c.l.b$h r8 = (j.c.a.a.c.l.b.h) r8
                r8.c()
                return
            L_0x00f6:
                int r8 = r8.what
                r0 = 45
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>(r0)
                java.lang.String r0 = "Don't know how to handle message: "
                r1.append(r0)
                r1.append(r8)
                java.lang.String r8 = r1.toString()
                java.lang.Exception r0 = new java.lang.Exception
                r0.<init>()
                java.lang.String r1 = "GmsClient"
                android.util.Log.wtf(r1, r8, r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.l.b.g.handleMessage(android.os.Message):void");
        }
    }

    public abstract class h<TListener> {
        public TListener a;
        public boolean b = false;

        public h(TListener tlistener) {
            this.a = tlistener;
        }

        public final void a() {
            synchronized (this) {
                this.a = null;
            }
        }

        public abstract void a(TListener tlistener);

        public final void b() {
            a();
            synchronized (b.this.f1804k) {
                b.this.f1804k.remove(this);
            }
        }

        public final void c() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.a;
                if (this.b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e2) {
                    throw e2;
                }
            }
            synchronized (this) {
                this.b = true;
            }
            b();
        }
    }

    public static final class i extends k.a {
        public b a;
        public final int b;

        public i(b bVar, int i2) {
            this.a = bVar;
            this.b = i2;
        }

        public final void a(int i2, IBinder iBinder, Bundle bundle) {
            ResourcesFlusher.b(this.a, "onPostInitComplete can be called only once per call to getRemoteService");
            b bVar = this.a;
            int i3 = this.b;
            Handler handler = bVar.f1800e;
            handler.sendMessage(handler.obtainMessage(1, i3, -1, new k(i2, iBinder, bundle)));
            this.a = null;
        }
    }

    public final class j implements ServiceConnection {
        public final int a;

        public j(int i2) {
            this.a = i2;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            m mVar;
            if (iBinder == null) {
                b.a(b.this);
                return;
            }
            synchronized (b.this.g) {
                b bVar = b.this;
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                if (queryLocalInterface == null || !(queryLocalInterface instanceof m)) {
                    mVar = new l(iBinder);
                } else {
                    mVar = (m) queryLocalInterface;
                }
                bVar.h = mVar;
            }
            b bVar2 = b.this;
            int i2 = this.a;
            Handler handler = bVar2.f1800e;
            handler.sendMessage(handler.obtainMessage(7, i2, -1, new l(0)));
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (b.this.g) {
                b.this.h = null;
            }
            Handler handler = b.this.f1800e;
            handler.sendMessage(handler.obtainMessage(6, this.a, 1));
        }
    }

    public final class k extends f {
        public final IBinder g;

        public k(int i2, IBinder iBinder, Bundle bundle) {
            super(i2, bundle);
            this.g = iBinder;
        }

        public final void a(j.c.a.a.c.b bVar) {
            C0023b bVar2 = b.this.f1808o;
            if (bVar2 != null) {
                bVar2.a(bVar);
            }
            if (b.this != null) {
                int i2 = bVar.c;
                System.currentTimeMillis();
                return;
            }
            throw null;
        }

        public final boolean d() {
            try {
                String interfaceDescriptor = this.g.getInterfaceDescriptor();
                if (!b.this.o().equals(interfaceDescriptor)) {
                    String o2 = b.this.o();
                    StringBuilder sb = new StringBuilder(outline.a(interfaceDescriptor, outline.a(o2, 34)));
                    sb.append("service descriptor mismatch: ");
                    sb.append(o2);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                IInterface a = b.this.a(this.g);
                if (a == null || (!b.this.a(2, 4, a) && !b.this.a(3, 4, a))) {
                    return false;
                }
                b bVar = b.this;
                bVar.f1811r = null;
                a aVar = bVar.f1807n;
                if (aVar == null) {
                    return true;
                }
                aVar.b(null);
                return true;
            } catch (RemoteException unused) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    public final class l extends f {
        public l(int i2) {
            super(i2, null);
        }

        public final void a(j.c.a.a.c.b bVar) {
            b bVar2 = b.this;
            if (bVar2 != null) {
                bVar2.f1802i.a(bVar);
                if (b.this != null) {
                    int i2 = bVar.c;
                    System.currentTimeMillis();
                    return;
                }
                throw null;
            }
            throw null;
        }

        public final boolean d() {
            b.this.f1802i.a(j.c.a.a.c.b.f1774f);
            return true;
        }
    }

    public b(Context context, Looper looper, h hVar, j.c.a.a.c.f fVar, int i2, a aVar, C0023b bVar, String str) {
        ResourcesFlusher.b(context, "Context must not be null");
        this.b = context;
        ResourcesFlusher.b(looper, "Looper must not be null");
        ResourcesFlusher.b(hVar, "Supervisor must not be null");
        this.c = hVar;
        ResourcesFlusher.b(fVar, "API availability must not be null");
        this.d = fVar;
        this.f1800e = new g(looper);
        this.f1809p = i2;
        this.f1807n = aVar;
        this.f1808o = bVar;
        this.f1810q = str;
    }

    public abstract T a(IBinder iBinder);

    public void a(int i2, T t2) {
    }

    public final boolean a(int i2, int i3, T t2) {
        synchronized (this.f1801f) {
            if (this.f1806m != i2) {
                return false;
            }
            b(i3, t2);
            return true;
        }
    }

    public final j.c.a.a.c.d[] b() {
        u uVar = this.f1813t;
        if (uVar == null) {
            return null;
        }
        return uVar.c;
    }

    public boolean c() {
        boolean z;
        synchronized (this.f1801f) {
            z = this.f1806m == 4;
        }
        return z;
    }

    public String d() {
        a0 a0Var;
        if (c() && (a0Var = this.a) != null) {
            return a0Var.b;
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    public void e() {
        this.u.incrementAndGet();
        synchronized (this.f1804k) {
            int size = this.f1804k.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f1804k.get(i2).a();
            }
            this.f1804k.clear();
        }
        synchronized (this.g) {
            this.h = null;
        }
        b(1, null);
    }

    public boolean g() {
        return false;
    }

    public boolean h() {
        return true;
    }

    public abstract int i();

    public void j() {
        int a2 = this.d.a(this.b, i());
        if (a2 != 0) {
            b(1, null);
            d dVar = new d();
            ResourcesFlusher.b(dVar, "Connection progress callbacks cannot be null.");
            this.f1802i = dVar;
            Handler handler = this.f1800e;
            handler.sendMessage(handler.obtainMessage(3, this.u.get(), a2, null));
            return;
        }
        a(new d());
    }

    public Account k() {
        return null;
    }

    public Bundle l() {
        return new Bundle();
    }

    public Set<Scope> m() {
        return Collections.EMPTY_SET;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void */
    public final T n() {
        T t2;
        synchronized (this.f1801f) {
            if (this.f1806m == 5) {
                throw new DeadObjectException();
            } else if (c()) {
                ResourcesFlusher.a(this.f1803j != null, (Object) "Client is connected but service is null");
                t2 = this.f1803j;
            } else {
                throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
            }
        }
        return t2;
    }

    public abstract String o();

    public abstract String p();

    public final String q() {
        String str = this.f1810q;
        return str == null ? this.b.getClass().getName() : str;
    }

    public final boolean r() {
        boolean z;
        synchronized (this.f1801f) {
            z = this.f1806m == 3;
        }
        return z;
    }

    public final void b(int i2, T t2) {
        ResourcesFlusher.a((i2 == 4) == (t2 != null));
        synchronized (this.f1801f) {
            this.f1806m = i2;
            this.f1803j = t2;
            a(i2, t2);
            if (i2 != 1) {
                if (i2 == 2 || i2 == 3) {
                    if (!(this.f1805l == null || this.a == null)) {
                        String str = this.a.a;
                        String str2 = this.a.b;
                        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 70 + String.valueOf(str2).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(str);
                        sb.append(" on ");
                        sb.append(str2);
                        Log.e("GmsClient", sb.toString());
                        h hVar = this.c;
                        String str3 = this.a.a;
                        String str4 = this.a.b;
                        int i3 = this.a.c;
                        j jVar = this.f1805l;
                        String q2 = q();
                        if (hVar != null) {
                            hVar.b(new h.a(str3, str4, i3), jVar, q2);
                            this.u.incrementAndGet();
                        } else {
                            throw null;
                        }
                    }
                    this.f1805l = new j(this.u.get());
                    a0 a0Var = new a0("com.google.android.gms", p(), false);
                    this.a = a0Var;
                    if (!this.c.a(new h.a(a0Var.a, a0Var.b, a0Var.c), this.f1805l, q())) {
                        String str5 = this.a.a;
                        String str6 = this.a.b;
                        StringBuilder sb2 = new StringBuilder(String.valueOf(str5).length() + 34 + String.valueOf(str6).length());
                        sb2.append("unable to connect to service: ");
                        sb2.append(str5);
                        sb2.append(" on ");
                        sb2.append(str6);
                        Log.e("GmsClient", sb2.toString());
                        int i4 = this.u.get();
                        Handler handler = this.f1800e;
                        handler.sendMessage(handler.obtainMessage(7, i4, -1, new l(16)));
                    }
                } else if (i2 == 4) {
                    System.currentTimeMillis();
                }
            } else if (this.f1805l != null) {
                h hVar2 = this.c;
                String str7 = this.a.a;
                String str8 = this.a.b;
                int i5 = this.a.c;
                j jVar2 = this.f1805l;
                String q3 = q();
                if (hVar2 != null) {
                    hVar2.b(new h.a(str7, str8, i5), jVar2, q3);
                    this.f1805l = null;
                } else {
                    throw null;
                }
            }
        }
    }

    public void a(c cVar) {
        ResourcesFlusher.b(cVar, "Connection progress callbacks cannot be null.");
        this.f1802i = cVar;
        b(2, null);
    }

    public boolean a() {
        boolean z;
        synchronized (this.f1801f) {
            if (this.f1806m != 2) {
                if (this.f1806m != 3) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    public static /* synthetic */ void a(b bVar) {
        int i2;
        if (bVar.r()) {
            i2 = 5;
            bVar.f1812s = true;
        } else {
            i2 = 4;
        }
        Handler handler = bVar.f1800e;
        handler.sendMessage(handler.obtainMessage(i2, bVar.u.get(), 16));
    }

    public void a(j jVar, Set<Scope> set) {
        Bundle l2 = l();
        f fVar = new f(this.f1809p);
        fVar.f1818e = this.b.getPackageName();
        fVar.h = l2;
        if (set != null) {
            fVar.g = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (g()) {
            fVar.f1820i = k() != null ? k() : new Account("<<default account>>", "com.google");
            if (jVar != null) {
                fVar.f1819f = jVar.asBinder();
            }
        }
        j.c.a.a.c.d[] dVarArr = v;
        fVar.f1821j = dVarArr;
        fVar.f1822k = dVarArr;
        try {
            synchronized (this.g) {
                if (this.h != null) {
                    this.h.a(new i(this, this.u.get()), fVar);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            Handler handler = this.f1800e;
            handler.sendMessage(handler.obtainMessage(6, this.u.get(), 1));
        } catch (SecurityException e3) {
            throw e3;
        } catch (RemoteException | RuntimeException e4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e4);
            int i2 = this.u.get();
            Handler handler2 = this.f1800e;
            handler2.sendMessage(handler2.obtainMessage(1, i2, -1, new k(8, null, null)));
        }
    }

    public void a(e eVar) {
        n nVar = (n) eVar;
        nVar.a.f1794l.f1788l.post(new o(nVar));
    }

    public static /* synthetic */ boolean b(b bVar) {
        if (bVar.f1812s || TextUtils.isEmpty(bVar.o()) || TextUtils.isEmpty(null)) {
            return false;
        }
        try {
            Class.forName(bVar.o());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }
}
