package j.b.a.s;

import j.a.a.a.outline;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ContentLengthInputStream extends FilterInputStream {
    public final long b;
    public int c;

    public ContentLengthInputStream(InputStream inputStream, long j2) {
        super(inputStream);
        this.b = j2;
    }

    public final int a(int i2) {
        if (i2 >= 0) {
            this.c += i2;
        } else if (this.b - ((long) this.c) > 0) {
            StringBuilder a = outline.a("Failed to read all expected data, expected: ");
            a.append(this.b);
            a.append(", but read: ");
            a.append(this.c);
            throw new IOException(a.toString());
        }
        return i2;
    }

    public synchronized int available() {
        return (int) Math.max(this.b - ((long) this.c), (long) super.in.available());
    }

    public synchronized int read() {
        int read;
        read = super.read();
        a(read >= 0 ? 1 : -1);
        return read;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public synchronized int read(byte[] bArr, int i2, int i3) {
        int read;
        read = super.read(bArr, i2, i3);
        a(read);
        return read;
    }
}
