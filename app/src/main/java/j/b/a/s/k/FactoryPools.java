package j.b.a.s.k;

import android.util.Log;
import i.h.k.Pools;
import i.h.k.Pools1;
import j.a.a.a.outline;
import j.b.a.s.k.StateVerifier;
import j.b.a.s.k.a;
import java.util.List;

public final class FactoryPools {
    public static final e<Object> a = new a();

    public class a implements e<Object> {
        public void a(Object obj) {
        }
    }

    public interface b<T> {
        T a();
    }

    public interface d {
        StateVerifier g();
    }

    public interface e<T> {
        void a(T t2);
    }

    public static <T extends a.d> Pools<T> a(int i2, b<T> bVar) {
        return new c(new Pools1(i2), bVar, a);
    }

    public static <T> Pools<List<T>> a() {
        return new c(new Pools1(20), new FactoryPools0(), new FactoryPools1());
    }

    public static final class c<T> implements Pools<T> {
        public final b<T> a;
        public final e<T> b;
        public final Pools<T> c;

        public c(Pools<T> pools, b<T> bVar, e<T> eVar) {
            this.c = pools;
            this.a = bVar;
            this.b = eVar;
        }

        public T a() {
            T a2 = this.c.a();
            if (a2 == null) {
                a2 = this.a.a();
                if (Log.isLoggable("FactoryPools", 2)) {
                    StringBuilder a3 = outline.a("Created new ");
                    a3.append(a2.getClass());
                    Log.v("FactoryPools", a3.toString());
                }
            }
            if (a2 instanceof d) {
                ((StateVerifier.b) ((d) a2).g()).a = false;
            }
            return a2;
        }

        public boolean a(T t2) {
            if (t2 instanceof d) {
                ((StateVerifier.b) ((d) t2).g()).a = true;
            }
            this.b.a(t2);
            return this.c.a(t2);
        }
    }
}
