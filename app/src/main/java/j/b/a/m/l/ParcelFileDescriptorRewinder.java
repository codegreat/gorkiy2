package j.b.a.m.l;

import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import j.b.a.m.l.DataRewinder;
import java.io.IOException;

public final class ParcelFileDescriptorRewinder implements DataRewinder<ParcelFileDescriptor> {
    public final b a;

    public static final class b {
        public final ParcelFileDescriptor a;

        public b(ParcelFileDescriptor parcelFileDescriptor) {
            this.a = parcelFileDescriptor;
        }
    }

    public ParcelFileDescriptorRewinder(ParcelFileDescriptor parcelFileDescriptor) {
        this.a = new b(parcelFileDescriptor);
    }

    public void b() {
    }

    public static final class a implements DataRewinder.a<ParcelFileDescriptor> {
        public DataRewinder a(Object obj) {
            return new ParcelFileDescriptorRewinder((ParcelFileDescriptor) obj);
        }

        public Class<ParcelFileDescriptor> a() {
            return ParcelFileDescriptor.class;
        }
    }

    public ParcelFileDescriptor a() {
        b bVar = this.a;
        if (bVar != null) {
            try {
                Os.lseek(bVar.a.getFileDescriptor(), 0, OsConstants.SEEK_SET);
                return bVar.a;
            } catch (ErrnoException e2) {
                throw new IOException(e2);
            }
        } else {
            throw null;
        }
    }
}
