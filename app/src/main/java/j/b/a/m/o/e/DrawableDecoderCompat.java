package j.b.a.m.o.e;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import i.b.l.a.AppCompatResources;
import i.b.p.ContextThemeWrapper;
import i.h.e.ContextCompat;

public final class DrawableDecoderCompat {
    public static volatile boolean a = true;

    public static Drawable a(Context context, Context context2, int i2, Resources.Theme theme) {
        try {
            if (a) {
                return AppCompatResources.c(theme != null ? new ContextThemeWrapper(context2, theme) : context2, i2);
            }
        } catch (NoClassDefFoundError unused) {
            a = false;
        } catch (IllegalStateException e2) {
            if (!context.getPackageName().equals(context2.getPackageName())) {
                return ContextCompat.c(context2, i2);
            }
            throw e2;
        } catch (Resources.NotFoundException unused2) {
        }
        if (theme == null) {
            theme = context2.getTheme();
        }
        return context2.getResources().getDrawable(i2, theme);
    }
}
