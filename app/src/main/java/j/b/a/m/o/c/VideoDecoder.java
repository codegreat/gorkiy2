package j.b.a.m.o.c;

import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import j.b.a.m.Option;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class VideoDecoder<T> implements ResourceDecoder<T, Bitmap> {
    public static final Option<Long> d = new Option<>("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new a());

    /* renamed from: e  reason: collision with root package name */
    public static final Option<Integer> f1716e = new Option<>("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new b());

    /* renamed from: f  reason: collision with root package name */
    public static final e f1717f = new e();
    public final f<T> a;
    public final BitmapPool b;
    public final e c;

    public class a implements Option.b<Long> {
        public final ByteBuffer a = ByteBuffer.allocate(8);

        public void a(byte[] bArr, Long l2, MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.a) {
                this.a.position(0);
                messageDigest.update(this.a.putLong(l2.longValue()).array());
            }
        }
    }

    public class b implements Option.b<Integer> {
        public final ByteBuffer a = ByteBuffer.allocate(4);

        public void a(byte[] bArr, Integer num, MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.a) {
                    this.a.position(0);
                    messageDigest.update(this.a.putInt(num.intValue()).array());
                }
            }
        }
    }

    public static final class c implements f<AssetFileDescriptor> {
        public /* synthetic */ c(a aVar) {
        }

        public void a(MediaMetadataRetriever mediaMetadataRetriever, Object obj) {
            AssetFileDescriptor assetFileDescriptor = (AssetFileDescriptor) obj;
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    public static final class d implements f<ByteBuffer> {
        public void a(MediaMetadataRetriever mediaMetadataRetriever, Object obj) {
            mediaMetadataRetriever.setDataSource(new VideoDecoder0(this, (ByteBuffer) obj));
        }
    }

    public static class e {
    }

    public interface f<T> {
        void a(MediaMetadataRetriever mediaMetadataRetriever, T t2);
    }

    public static final class g implements f<ParcelFileDescriptor> {
        public void a(MediaMetadataRetriever mediaMetadataRetriever, Object obj) {
            mediaMetadataRetriever.setDataSource(((ParcelFileDescriptor) obj).getFileDescriptor());
        }
    }

    public VideoDecoder(j.b.a.m.m.b0.d dVar, f<T> fVar) {
        e eVar = f1717f;
        this.b = dVar;
        this.a = fVar;
        this.c = eVar;
    }

    public Resource<Bitmap> a(T t2, int i2, int i3, j.b.a.m.g gVar) {
        long longValue = ((Long) gVar.a(d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) gVar.a(f1716e);
            if (num == null) {
                num = 2;
            }
            DownsampleStrategy downsampleStrategy = (DownsampleStrategy) gVar.a(DownsampleStrategy.f1704f);
            if (downsampleStrategy == null) {
                downsampleStrategy = DownsampleStrategy.f1703e;
            }
            DownsampleStrategy downsampleStrategy2 = downsampleStrategy;
            if (this.c != null) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                try {
                    this.a.a(mediaMetadataRetriever, t2);
                    Bitmap a2 = a(mediaMetadataRetriever, longValue, num.intValue(), i2, i3, downsampleStrategy2);
                    mediaMetadataRetriever.release();
                    return BitmapResource.a(a2, this.b);
                } catch (RuntimeException e2) {
                    throw new IOException(e2);
                } catch (Throwable th) {
                    mediaMetadataRetriever.release();
                    throw th;
                }
            } else {
                throw null;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }

    public boolean a(T t2, j.b.a.m.g gVar) {
        return true;
    }

    public static Bitmap a(MediaMetadataRetriever mediaMetadataRetriever, long j2, int i2, int i3, int i4, DownsampleStrategy downsampleStrategy) {
        int i5 = i3;
        int i6 = i4;
        DownsampleStrategy downsampleStrategy2 = downsampleStrategy;
        Bitmap bitmap = null;
        if (!(Build.VERSION.SDK_INT < 27 || i5 == Integer.MIN_VALUE || i6 == Integer.MIN_VALUE || downsampleStrategy2 == DownsampleStrategy.d)) {
            try {
                int parseInt = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
                int parseInt2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
                int parseInt3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
                if (parseInt3 == 90 || parseInt3 == 270) {
                    int i7 = parseInt2;
                    parseInt2 = parseInt;
                    parseInt = i7;
                }
                float b2 = downsampleStrategy2.b(parseInt, parseInt2, i3, i4);
                bitmap = mediaMetadataRetriever.getScaledFrameAtTime(j2, i2, Math.round(((float) parseInt) * b2), Math.round(b2 * ((float) parseInt2)));
            } catch (Throwable th) {
                if (Log.isLoggable("VideoDecoder", 3)) {
                    Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
                }
            }
        }
        return bitmap == null ? mediaMetadataRetriever.getFrameAtTime(j2, i2) : bitmap;
    }
}
