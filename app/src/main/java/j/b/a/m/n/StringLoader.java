package j.b.a.m.n;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import j.b.a.m.Options;
import j.b.a.m.n.ModelLoader;
import java.io.File;
import java.io.InputStream;

public class StringLoader<Data> implements ModelLoader<String, Data> {
    public final ModelLoader<Uri, Data> a;

    public static final class a implements ModelLoaderFactory<String, AssetFileDescriptor> {
        public ModelLoader<String, AssetFileDescriptor> a(r rVar) {
            return new StringLoader(rVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    public static class b implements ModelLoaderFactory<String, ParcelFileDescriptor> {
        public ModelLoader<String, ParcelFileDescriptor> a(r rVar) {
            return new StringLoader(rVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    public static class c implements ModelLoaderFactory<String, InputStream> {
        public ModelLoader<String, InputStream> a(r rVar) {
            return new StringLoader(rVar.a(Uri.class, InputStream.class));
        }
    }

    public StringLoader(ModelLoader<Uri, Data> modelLoader) {
        this.a = modelLoader;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri;
        String str = (String) obj;
        if (TextUtils.isEmpty(str)) {
            uri = null;
        } else if (str.charAt(0) == '/') {
            uri = Uri.fromFile(new File(str));
        } else {
            Uri parse = Uri.parse(str);
            uri = parse.getScheme() == null ? Uri.fromFile(new File(str)) : parse;
        }
        if (uri == null || !this.a.a(uri)) {
            return null;
        }
        return this.a.a(uri, i2, i3, options);
    }

    public boolean a(Object obj) {
        String str = (String) obj;
        return true;
    }
}
