package j.b.a.m;

import java.nio.charset.Charset;
import java.security.MessageDigest;

public interface Key {
    public static final Charset a = Charset.forName("UTF-8");

    void a(MessageDigest messageDigest);

    boolean equals(Object obj);

    int hashCode();
}
