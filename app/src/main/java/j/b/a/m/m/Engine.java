package j.b.a.m.m;

import android.util.Log;
import i.b.k.ResourcesFlusher;
import i.h.k.Pools;
import j.a.a.a.outline;
import j.b.a.m.DataSource;
import j.b.a.m.Key;
import j.b.a.m.Transformation;
import j.b.a.m.e;
import j.b.a.m.g;
import j.b.a.m.m.DecodeJob;
import j.b.a.m.m.EngineResource;
import j.b.a.m.m.c0.DiskCache;
import j.b.a.m.m.c0.DiskCacheAdapter;
import j.b.a.m.m.c0.DiskLruCacheFactory;
import j.b.a.m.m.c0.DiskLruCacheWrapper;
import j.b.a.m.m.c0.InternalCacheDiskCacheFactory;
import j.b.a.m.m.c0.LruResourceCache;
import j.b.a.m.m.c0.MemoryCache;
import j.b.a.m.m.d0.GlideExecutor;
import j.b.a.m.m.l;
import j.b.a.q.ResourceCallback;
import j.b.a.q.SingleRequest;
import j.b.a.q.f;
import j.b.a.s.LogTime;
import j.b.a.s.k.FactoryPools;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Executor;

public class Engine implements EngineJobListener, MemoryCache.a, EngineResource.a {

    /* renamed from: i  reason: collision with root package name */
    public static final boolean f1637i = Log.isLoggable("Engine", 2);
    public final Jobs a = new Jobs();
    public final EngineKeyFactory b = new EngineKeyFactory();
    public final MemoryCache c;
    public final b d;

    /* renamed from: e  reason: collision with root package name */
    public final ResourceRecycler f1638e;

    /* renamed from: f  reason: collision with root package name */
    public final c f1639f;
    public final a g;
    public final ActiveResources h;

    public static class a {
        public final DecodeJob.d a;
        public final Pools<DecodeJob<?>> b = FactoryPools.a(150, new C0012a());
        public int c;

        /* renamed from: j.b.a.m.m.Engine$a$a  reason: collision with other inner class name */
        public class C0012a implements FactoryPools.b<DecodeJob<?>> {
            public C0012a() {
            }

            public Object a() {
                a aVar = a.this;
                return new DecodeJob(aVar.a, aVar.b);
            }
        }

        public a(DecodeJob.d dVar) {
            this.a = dVar;
        }
    }

    public static class b {
        public final GlideExecutor a;
        public final GlideExecutor b;
        public final GlideExecutor c;
        public final GlideExecutor d;

        /* renamed from: e  reason: collision with root package name */
        public final EngineJobListener f1640e;

        /* renamed from: f  reason: collision with root package name */
        public final EngineResource.a f1641f;
        public final Pools<EngineJob<?>> g = FactoryPools.a(150, new a());

        public class a implements FactoryPools.b<EngineJob<?>> {
            public a() {
            }

            public Object a() {
                b bVar = b.this;
                return new EngineJob(bVar.a, bVar.b, bVar.c, bVar.d, bVar.f1640e, bVar.f1641f, bVar.g);
            }
        }

        public b(GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, EngineJobListener engineJobListener, EngineResource.a aVar) {
            this.a = glideExecutor;
            this.b = glideExecutor2;
            this.c = glideExecutor3;
            this.d = glideExecutor4;
            this.f1640e = engineJobListener;
            this.f1641f = aVar;
        }
    }

    public static class c implements DecodeJob.d {
        public final DiskCache.a a;
        public volatile DiskCache b;

        public c(DiskCache.a aVar) {
            this.a = aVar;
        }

        public DiskCache a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        DiskLruCacheFactory diskLruCacheFactory = (DiskLruCacheFactory) this.a;
                        InternalCacheDiskCacheFactory internalCacheDiskCacheFactory = (InternalCacheDiskCacheFactory) diskLruCacheFactory.b;
                        File cacheDir = internalCacheDiskCacheFactory.a.getCacheDir();
                        DiskLruCacheWrapper diskLruCacheWrapper = null;
                        if (cacheDir == null) {
                            cacheDir = null;
                        } else if (internalCacheDiskCacheFactory.b != null) {
                            cacheDir = new File(cacheDir, internalCacheDiskCacheFactory.b);
                        }
                        if (cacheDir != null) {
                            if (!cacheDir.mkdirs()) {
                                if (cacheDir.exists()) {
                                    if (!cacheDir.isDirectory()) {
                                    }
                                }
                            }
                            diskLruCacheWrapper = new DiskLruCacheWrapper(cacheDir, diskLruCacheFactory.a);
                        }
                        this.b = diskLruCacheWrapper;
                    }
                    if (this.b == null) {
                        this.b = new DiskCacheAdapter();
                    }
                }
            }
            return this.b;
        }
    }

    public class d {
        public final EngineJob<?> a;
        public final ResourceCallback b;

        public d(f fVar, EngineJob<?> engineJob) {
            this.b = fVar;
            this.a = engineJob;
        }

        public void a() {
            synchronized (Engine.this) {
                this.a.a(this.b);
            }
        }
    }

    public Engine(MemoryCache memoryCache, DiskCache.a aVar, GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, boolean z) {
        this.c = memoryCache;
        this.f1639f = new c(aVar);
        ActiveResources activeResources = new ActiveResources(z);
        this.h = activeResources;
        activeResources.a(this);
        this.d = new b(glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, this, this);
        this.g = new a(this.f1639f);
        this.f1638e = new ResourceRecycler();
        ((LruResourceCache) memoryCache).d = this;
    }

    public synchronized void a(EngineJob<?> engineJob, e eVar, EngineResource<?> engineResource) {
        if (engineResource != null) {
            if (engineResource.b) {
                this.h.a(eVar, engineResource);
            }
        }
        Jobs jobs = this.a;
        if (jobs != null) {
            Map<e, EngineJob<?>> a2 = jobs.a(engineJob.f1652q);
            if (engineJob.equals(a2.get(eVar))) {
                a2.remove(eVar);
            }
        } else {
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.Engine.a(j.b.a.m.m.o, boolean, long):j.b.a.m.m.EngineResource<?>
     arg types: [j.b.a.m.m.EngineKey, boolean, long]
     candidates:
      j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void
      j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
      j.b.a.m.m.Engine.a(j.b.a.m.m.o, boolean, long):j.b.a.m.m.EngineResource<?> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.SingleRequest.a(j.b.a.m.m.Resource<?>, j.b.a.m.a):void
     arg types: [j.b.a.m.m.EngineResource<?>, j.b.a.m.DataSource]
     candidates:
      j.b.a.q.SingleRequest.a(int, int):void
      j.b.a.q.SingleRequest.a(com.bumptech.glide.load.engine.GlideException, int):void
      j.b.a.q.SingleRequest.a(j.b.a.m.m.Resource<?>, j.b.a.m.a):void */
    public <R> l.d a(j.b.a.d dVar, Object obj, e eVar, int i2, int i3, Class<?> cls, Class<R> cls2, j.b.a.f fVar, k kVar, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, g gVar, boolean z3, boolean z4, boolean z5, boolean z6, f fVar2, Executor executor) {
        long a2 = f1637i ? LogTime.a() : 0;
        if (this.b != null) {
            EngineKey engineKey = new EngineKey(obj, eVar, i2, i3, map, cls, cls2, gVar);
            synchronized (this) {
                EngineResource<?> a3 = a((o) engineKey, z3, a2);
                if (a3 == null) {
                    d a4 = a(dVar, obj, eVar, i2, i3, cls, cls2, fVar, kVar, map, z, z2, gVar, z3, z4, z5, z6, fVar2, executor, engineKey, a2);
                    return a4;
                }
                ((SingleRequest) fVar2).a((Resource<?>) a3, (j.b.a.m.a) DataSource.MEMORY_CACHE);
                return null;
            }
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.EngineJob.a(j.b.a.q.ResourceCallback, java.util.concurrent.Executor):void
     arg types: [j.b.a.q.f, java.util.concurrent.Executor]
     candidates:
      j.b.a.m.m.EngineJob.a(j.b.a.m.m.Resource, j.b.a.m.a):void
      j.b.a.m.m.EngineJob.a(j.b.a.q.ResourceCallback, java.util.concurrent.Executor):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void
     arg types: [java.lang.String, long, j.b.a.m.m.o]
     candidates:
      j.b.a.m.m.Engine.a(j.b.a.m.m.o, boolean, long):j.b.a.m.m.EngineResource<?>
      j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
      j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.m.EngineJob, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.m.DecodeJob, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public final <R> l.d a(j.b.a.d dVar, Object obj, e eVar, int i2, int i3, Class<?> cls, Class<R> cls2, j.b.a.f fVar, k kVar, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, g gVar, boolean z3, boolean z4, boolean z5, boolean z6, f fVar2, Executor executor, o oVar, long j2) {
        j.b.a.d dVar2 = dVar;
        Object obj2 = obj;
        e eVar2 = eVar;
        int i4 = i2;
        int i5 = i3;
        j.b.a.f fVar3 = fVar;
        k kVar2 = kVar;
        g gVar2 = gVar;
        boolean z7 = z6;
        f fVar4 = fVar2;
        Executor executor2 = executor;
        o oVar2 = oVar;
        long j3 = j2;
        Jobs jobs = this.a;
        EngineJob engineJob = (EngineJob) (z7 ? jobs.b : jobs.a).get(oVar2);
        if (engineJob != null) {
            engineJob.a((ResourceCallback) fVar4, executor2);
            if (f1637i) {
                a("Added to existing load", j3, (Key) oVar2);
            }
            return new d(fVar4, engineJob);
        }
        EngineJob a2 = this.d.g.a();
        ResourcesFlusher.a((Object) a2, "Argument must not be null");
        EngineJob engineJob2 = a2;
        a2.a(oVar, z3, z4, z5, z6);
        a aVar = this.g;
        DecodeJob a3 = aVar.b.a();
        ResourcesFlusher.a((Object) a3, "Argument must not be null");
        int i6 = aVar.c;
        aVar.c = i6 + 1;
        DecodeHelper<R> decodeHelper = a3.b;
        DecodeJob.d dVar3 = a3.f1622e;
        decodeHelper.c = dVar2;
        decodeHelper.d = obj2;
        decodeHelper.f1617n = eVar2;
        decodeHelper.f1610e = i4;
        decodeHelper.f1611f = i5;
        decodeHelper.f1619p = kVar2;
        decodeHelper.g = cls;
        decodeHelper.h = dVar3;
        decodeHelper.f1614k = cls2;
        decodeHelper.f1618o = fVar3;
        decodeHelper.f1612i = gVar2;
        decodeHelper.f1613j = map;
        decodeHelper.f1620q = z;
        decodeHelper.f1621r = z2;
        a3.f1624i = dVar2;
        a3.f1625j = eVar2;
        a3.f1626k = fVar3;
        o oVar3 = oVar;
        a3.f1627l = oVar3;
        a3.f1628m = i4;
        a3.f1629n = i5;
        a3.f1630o = kVar2;
        a3.v = z6;
        a3.f1631p = gVar2;
        EngineJob engineJob3 = engineJob2;
        a3.f1632q = engineJob3;
        a3.f1633r = i6;
        a3.f1635t = DecodeJob.f.INITIALIZE;
        a3.w = obj2;
        Jobs jobs2 = this.a;
        if (jobs2 != null) {
            jobs2.a(engineJob3.f1652q).put(oVar3, engineJob3);
            f fVar5 = fVar2;
            engineJob3.a((ResourceCallback) fVar5, executor);
            engineJob3.b(a3);
            if (f1637i) {
                a("Started new load", j2, (Key) oVar3);
            }
            return new d(fVar5, engineJob3);
        }
        throw null;
    }

    public static void a(String str, long j2, Key key) {
        StringBuilder b2 = outline.b(str, " in ");
        b2.append(LogTime.a(j2));
        b2.append("ms, key: ");
        b2.append(key);
        Log.v("Engine", b2.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void
     arg types: [java.lang.String, long, j.b.a.m.m.o]
     candidates:
      j.b.a.m.m.Engine.a(j.b.a.m.m.o, boolean, long):j.b.a.m.m.EngineResource<?>
      j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
      j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void */
    public final EngineResource<?> a(o oVar, boolean z, long j2) {
        EngineResource<?> engineResource;
        if (!z) {
            return null;
        }
        EngineResource<?> b2 = this.h.b(oVar);
        if (b2 != null) {
            b2.a();
        }
        if (b2 != null) {
            if (f1637i) {
                a("Loaded resource from active resources", j2, (Key) oVar);
            }
            return b2;
        }
        Resource a2 = ((LruResourceCache) this.c).a((Key) oVar);
        if (a2 == null) {
            engineResource = null;
        } else if (a2 instanceof EngineResource) {
            engineResource = (EngineResource) a2;
        } else {
            engineResource = new EngineResource<>(a2, true, true, oVar, this);
        }
        if (engineResource != null) {
            engineResource.a();
            this.h.a(oVar, engineResource);
        }
        if (engineResource == null) {
            return null;
        }
        if (f1637i) {
            a("Loaded resource from cache", j2, (Key) oVar);
        }
        return engineResource;
    }

    public void a(Resource<?> resource) {
        if (resource instanceof EngineResource) {
            ((EngineResource) resource).e();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    public synchronized void a(EngineJob<?> engineJob, e eVar) {
        Jobs jobs = this.a;
        if (jobs != null) {
            Map<e, EngineJob<?>> a2 = jobs.a(engineJob.f1652q);
            if (engineJob.equals(a2.get(eVar))) {
                a2.remove(eVar);
            }
        } else {
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.c0.LruResourceCache.a(j.b.a.m.Key, j.b.a.m.m.Resource):j.b.a.m.m.Resource
     arg types: [j.b.a.m.e, j.b.a.m.m.EngineResource<?>]
     candidates:
      j.b.a.m.m.c0.LruResourceCache.a(java.lang.Object, java.lang.Object):void
      j.b.a.s.LruCache.a(java.lang.Object, java.lang.Object):void
      j.b.a.m.m.c0.LruResourceCache.a(j.b.a.m.Key, j.b.a.m.m.Resource):j.b.a.m.m.Resource */
    public void a(e eVar, EngineResource<?> engineResource) {
        this.h.a((Key) eVar);
        if (engineResource.b) {
            ((LruResourceCache) this.c).a((Key) eVar, (Resource) engineResource);
        } else {
            this.f1638e.a(engineResource, false);
        }
    }
}
