package j.b.a.m.m.d0;

import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import j.a.a.a.outline;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class GlideExecutor implements ExecutorService {
    public static final long b = TimeUnit.SECONDS.toMillis(10);
    public static volatile int c;
    public final ExecutorService a;

    public static final class a implements ThreadFactory {
        public final String b;
        public final b c;
        public final boolean d;

        /* renamed from: e  reason: collision with root package name */
        public int f1687e;

        /* renamed from: j.b.a.m.m.d0.GlideExecutor$a$a  reason: collision with other inner class name */
        public class C0013a extends Thread {
            public C0013a(Runnable runnable, String str) {
                super(runnable, str);
            }

            public void run() {
                Process.setThreadPriority(9);
                if (a.this.d) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    a.this.c.a(th);
                }
            }
        }

        public a(String str, b bVar, boolean z) {
            this.b = str;
            this.c = bVar;
            this.d = z;
        }

        public synchronized Thread newThread(Runnable runnable) {
            C0013a aVar;
            aVar = new C0013a(runnable, "glide-" + this.b + "-thread-" + this.f1687e);
            this.f1687e = this.f1687e + 1;
            return aVar;
        }
    }

    public interface b {
        public static final b a = new a();
        public static final b b = a;

        public class a implements b {
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        void a(Throwable th);
    }

    public GlideExecutor(ExecutorService executorService) {
        this.a = executorService;
    }

    public static int a() {
        if (c == 0) {
            c = Math.min(4, Runtime.getRuntime().availableProcessors());
        }
        return c;
    }

    public static GlideExecutor b() {
        b bVar = b.b;
        if (!TextUtils.isEmpty("disk-cache")) {
            return new GlideExecutor(new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new a("disk-cache", bVar, true)));
        }
        throw new IllegalArgumentException(outline.a("Name must be non-null and non-empty, but given: ", "disk-cache"));
    }

    public static GlideExecutor c() {
        b bVar = b.b;
        int a2 = a();
        if (!TextUtils.isEmpty("source")) {
            return new GlideExecutor(new ThreadPoolExecutor(a2, a2, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new a("source", bVar, false)));
        }
        throw new IllegalArgumentException(outline.a("Name must be non-null and non-empty, but given: ", "source"));
    }

    public boolean awaitTermination(long j2, TimeUnit timeUnit) {
        return this.a.awaitTermination(j2, timeUnit);
    }

    public void execute(Runnable runnable) {
        this.a.execute(runnable);
    }

    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) {
        return this.a.invokeAll(collection);
    }

    public <T> T invokeAny(Collection<? extends Callable<T>> collection) {
        return this.a.invokeAny(collection);
    }

    public boolean isShutdown() {
        return this.a.isShutdown();
    }

    public boolean isTerminated() {
        return this.a.isTerminated();
    }

    public void shutdown() {
        this.a.shutdown();
    }

    public List<Runnable> shutdownNow() {
        return this.a.shutdownNow();
    }

    public Future<?> submit(Runnable runnable) {
        return this.a.submit(runnable);
    }

    public String toString() {
        return this.a.toString();
    }

    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j2, TimeUnit timeUnit) {
        return this.a.invokeAll(collection, j2, timeUnit);
    }

    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j2, TimeUnit timeUnit) {
        return this.a.invokeAny(collection, j2, timeUnit);
    }

    public <T> Future<T> submit(Runnable runnable, T t2) {
        return this.a.submit(runnable, t2);
    }

    public <T> Future<T> submit(Callable<T> callable) {
        return this.a.submit(callable);
    }
}
