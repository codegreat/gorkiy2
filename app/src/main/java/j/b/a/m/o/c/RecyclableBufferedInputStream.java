package j.b.a.m.o.c;

import com.crashlytics.android.core.LogFileManager;
import j.b.a.m.m.b0.ArrayPool;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RecyclableBufferedInputStream extends FilterInputStream {
    public volatile byte[] b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f1714e = -1;

    /* renamed from: f  reason: collision with root package name */
    public int f1715f;
    public final ArrayPool g;

    public static class a extends IOException {
        public a(String str) {
            super(str);
        }
    }

    public RecyclableBufferedInputStream(InputStream inputStream, ArrayPool arrayPool) {
        super(inputStream);
        this.g = arrayPool;
        this.b = (byte[]) arrayPool.b(LogFileManager.MAX_LOG_SIZE, byte[].class);
    }

    public static IOException g() {
        throw new IOException("BufferedInputStream is closed");
    }

    public synchronized void a() {
        this.d = this.b.length;
    }

    public synchronized int available() {
        InputStream inputStream;
        inputStream = super.in;
        if (this.b == null || inputStream == null) {
            g();
            throw null;
        }
        return (this.c - this.f1715f) + inputStream.available();
    }

    public void close() {
        if (this.b != null) {
            this.g.put(this.b);
            this.b = null;
        }
        InputStream inputStream = super.in;
        super.in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    public synchronized void f() {
        if (this.b != null) {
            this.g.put(this.b);
            this.b = null;
        }
    }

    public synchronized void mark(int i2) {
        this.d = Math.max(this.d, i2);
        this.f1714e = this.f1715f;
    }

    public boolean markSupported() {
        return true;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0026=Splitter:B:19:0x0026, B:11:0x0019=Splitter:B:11:0x0019, B:28:0x003b=Splitter:B:28:0x003b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int read() {
        /*
            r6 = this;
            monitor-enter(r6)
            byte[] r0 = r6.b     // Catch:{ all -> 0x003f }
            java.io.InputStream r1 = r6.in     // Catch:{ all -> 0x003f }
            r2 = 0
            if (r0 == 0) goto L_0x003b
            if (r1 == 0) goto L_0x003b
            int r3 = r6.f1715f     // Catch:{ all -> 0x003f }
            int r4 = r6.c     // Catch:{ all -> 0x003f }
            r5 = -1
            if (r3 < r4) goto L_0x0019
            int r1 = r6.a(r1, r0)     // Catch:{ all -> 0x003f }
            if (r1 != r5) goto L_0x0019
            monitor-exit(r6)
            return r5
        L_0x0019:
            byte[] r1 = r6.b     // Catch:{ all -> 0x003f }
            if (r0 == r1) goto L_0x0026
            byte[] r0 = r6.b     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0022
            goto L_0x0026
        L_0x0022:
            g()     // Catch:{ all -> 0x003f }
            throw r2
        L_0x0026:
            int r1 = r6.c     // Catch:{ all -> 0x003f }
            int r2 = r6.f1715f     // Catch:{ all -> 0x003f }
            int r1 = r1 - r2
            if (r1 <= 0) goto L_0x0039
            int r1 = r6.f1715f     // Catch:{ all -> 0x003f }
            int r2 = r1 + 1
            r6.f1715f = r2     // Catch:{ all -> 0x003f }
            byte r0 = r0[r1]     // Catch:{ all -> 0x003f }
            r0 = r0 & 255(0xff, float:3.57E-43)
            monitor-exit(r6)
            return r0
        L_0x0039:
            monitor-exit(r6)
            return r5
        L_0x003b:
            g()     // Catch:{ all -> 0x003f }
            throw r2
        L_0x003f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.o.c.RecyclableBufferedInputStream.read():int");
    }

    public synchronized void reset() {
        if (this.b == null) {
            throw new IOException("Stream is closed");
        } else if (-1 != this.f1714e) {
            this.f1715f = this.f1714e;
        } else {
            throw new a("Mark has been invalidated, pos: " + this.f1715f + " markLimit: " + this.d);
        }
    }

    public synchronized long skip(long j2) {
        if (j2 < 1) {
            return 0;
        }
        byte[] bArr = this.b;
        if (bArr != null) {
            InputStream inputStream = super.in;
            if (inputStream == null) {
                g();
                throw null;
            } else if (((long) (this.c - this.f1715f)) >= j2) {
                this.f1715f = (int) (((long) this.f1715f) + j2);
                return j2;
            } else {
                long j3 = ((long) this.c) - ((long) this.f1715f);
                this.f1715f = this.c;
                if (this.f1714e == -1 || j2 > ((long) this.d)) {
                    return j3 + inputStream.skip(j2 - j3);
                } else if (a(inputStream, bArr) == -1) {
                    return j3;
                } else {
                    if (((long) (this.c - this.f1715f)) >= j2 - j3) {
                        this.f1715f = (int) ((((long) this.f1715f) + j2) - j3);
                        return j2;
                    }
                    long j4 = (j3 + ((long) this.c)) - ((long) this.f1715f);
                    this.f1715f = this.c;
                    return j4;
                }
            }
        } else {
            g();
            throw null;
        }
    }

    public final int a(InputStream inputStream, byte[] bArr) {
        int i2;
        int i3 = this.f1714e;
        if (i3 == -1 || this.f1715f - i3 >= (i2 = this.d)) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                this.f1714e = -1;
                this.f1715f = 0;
                this.c = read;
            }
            return read;
        }
        if (i3 == 0 && i2 > bArr.length && this.c == bArr.length) {
            int length = bArr.length * 2;
            if (length <= i2) {
                i2 = length;
            }
            byte[] bArr2 = (byte[]) this.g.b(i2, byte[].class);
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            this.b = bArr2;
            this.g.put(bArr);
            bArr = bArr2;
        } else {
            int i4 = this.f1714e;
            if (i4 > 0) {
                System.arraycopy(bArr, i4, bArr, 0, bArr.length - i4);
            }
        }
        int i5 = this.f1715f - this.f1714e;
        this.f1715f = i5;
        this.f1714e = 0;
        this.c = 0;
        int read2 = inputStream.read(bArr, i5, bArr.length - i5);
        int i6 = this.f1715f;
        if (read2 > 0) {
            i6 += read2;
        }
        this.c = i6;
        return read2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003b, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0051, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x005e, code lost:
        return r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int read(byte[] r7, int r8, int r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            byte[] r0 = r6.b     // Catch:{ all -> 0x009c }
            r1 = 0
            if (r0 == 0) goto L_0x0098
            if (r9 != 0) goto L_0x000b
            r7 = 0
            monitor-exit(r6)
            return r7
        L_0x000b:
            java.io.InputStream r2 = r6.in     // Catch:{ all -> 0x009c }
            if (r2 == 0) goto L_0x0094
            int r3 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r4 = r6.c     // Catch:{ all -> 0x009c }
            if (r3 >= r4) goto L_0x003c
            int r3 = r6.c     // Catch:{ all -> 0x009c }
            int r4 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r3 = r3 - r4
            if (r3 < r9) goto L_0x001e
            r3 = r9
            goto L_0x0023
        L_0x001e:
            int r3 = r6.c     // Catch:{ all -> 0x009c }
            int r4 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r3 = r3 - r4
        L_0x0023:
            int r4 = r6.f1715f     // Catch:{ all -> 0x009c }
            java.lang.System.arraycopy(r0, r4, r7, r8, r3)     // Catch:{ all -> 0x009c }
            int r4 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r4 = r4 + r3
            r6.f1715f = r4     // Catch:{ all -> 0x009c }
            if (r3 == r9) goto L_0x003a
            int r4 = r2.available()     // Catch:{ all -> 0x009c }
            if (r4 != 0) goto L_0x0036
            goto L_0x003a
        L_0x0036:
            int r8 = r8 + r3
            int r3 = r9 - r3
            goto L_0x003d
        L_0x003a:
            monitor-exit(r6)
            return r3
        L_0x003c:
            r3 = r9
        L_0x003d:
            int r4 = r6.f1714e     // Catch:{ all -> 0x009c }
            r5 = -1
            if (r4 != r5) goto L_0x0052
            int r4 = r0.length     // Catch:{ all -> 0x009c }
            if (r3 < r4) goto L_0x0052
            int r4 = r2.read(r7, r8, r3)     // Catch:{ all -> 0x009c }
            if (r4 != r5) goto L_0x0084
            if (r3 != r9) goto L_0x004e
            goto L_0x0050
        L_0x004e:
            int r5 = r9 - r3
        L_0x0050:
            monitor-exit(r6)
            return r5
        L_0x0052:
            int r4 = r6.a(r2, r0)     // Catch:{ all -> 0x009c }
            if (r4 != r5) goto L_0x005f
            if (r3 != r9) goto L_0x005b
            goto L_0x005d
        L_0x005b:
            int r5 = r9 - r3
        L_0x005d:
            monitor-exit(r6)
            return r5
        L_0x005f:
            byte[] r4 = r6.b     // Catch:{ all -> 0x009c }
            if (r0 == r4) goto L_0x006c
            byte[] r0 = r6.b     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0068
            goto L_0x006c
        L_0x0068:
            g()     // Catch:{ all -> 0x009c }
            throw r1
        L_0x006c:
            int r4 = r6.c     // Catch:{ all -> 0x009c }
            int r5 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r4 = r4 - r5
            if (r4 < r3) goto L_0x0075
            r4 = r3
            goto L_0x007a
        L_0x0075:
            int r4 = r6.c     // Catch:{ all -> 0x009c }
            int r5 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r4 = r4 - r5
        L_0x007a:
            int r5 = r6.f1715f     // Catch:{ all -> 0x009c }
            java.lang.System.arraycopy(r0, r5, r7, r8, r4)     // Catch:{ all -> 0x009c }
            int r5 = r6.f1715f     // Catch:{ all -> 0x009c }
            int r5 = r5 + r4
            r6.f1715f = r5     // Catch:{ all -> 0x009c }
        L_0x0084:
            int r3 = r3 - r4
            if (r3 != 0) goto L_0x0089
            monitor-exit(r6)
            return r9
        L_0x0089:
            int r5 = r2.available()     // Catch:{ all -> 0x009c }
            if (r5 != 0) goto L_0x0092
            int r9 = r9 - r3
            monitor-exit(r6)
            return r9
        L_0x0092:
            int r8 = r8 + r4
            goto L_0x003d
        L_0x0094:
            g()     // Catch:{ all -> 0x009c }
            throw r1
        L_0x0098:
            g()     // Catch:{ all -> 0x009c }
            throw r1
        L_0x009c:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.o.c.RecyclableBufferedInputStream.read(byte[], int, int):int");
    }
}
