package j.b.a.m.m;

import android.util.Log;
import com.bumptech.glide.load.engine.GlideException;
import i.h.k.Pools;
import j.a.a.a.outline;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.g;
import j.b.a.m.l.DataRewinder;
import j.b.a.m.o.h.ResourceTranscoder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DecodePath<DataType, ResourceType, Transcode> {
    public final Class<DataType> a;
    public final List<? extends ResourceDecoder<DataType, ResourceType>> b;
    public final ResourceTranscoder<ResourceType, Transcode> c;
    public final Pools<List<Throwable>> d;

    /* renamed from: e  reason: collision with root package name */
    public final String f1636e;

    public interface a<ResourceType> {
    }

    public DecodePath(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends ResourceDecoder<DataType, ResourceType>> list, ResourceTranscoder<ResourceType, Transcode> resourceTranscoder, Pools<List<Throwable>> pools) {
        this.a = cls;
        this.b = list;
        this.c = resourceTranscoder;
        this.d = pools;
        StringBuilder a2 = outline.a("Failed DecodePath{");
        a2.append(cls.getSimpleName());
        a2.append("->");
        a2.append(cls2.getSimpleName());
        a2.append("->");
        a2.append(cls3.getSimpleName());
        a2.append("}");
        this.f1636e = a2.toString();
    }

    /* JADX INFO: finally extract failed */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: j.b.a.m.l.DataRewinder<DataType>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v2, resolved type: j.b.a.m.m.Resource<Z>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v13, resolved type: j.b.a.m.m.ResourceCacheKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v14, resolved type: j.b.a.m.m.DataCacheKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: j.b.a.m.m.Resource<Z>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: j.b.a.m.m.LockedResource<Z>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v27, resolved type: j.b.a.m.m.ResourceCacheKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: j.b.a.m.m.ResourceCacheKey} */
    /* JADX WARN: Type inference failed for: r0v6, types: [j.b.a.m.m.Resource] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.List, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public j.b.a.m.m.Resource<Transcode> a(j.b.a.m.l.DataRewinder<DataType> r11, int r12, int r13, j.b.a.m.g r14, j.b.a.m.m.DecodePath.a<ResourceType> r15) {
        /*
            r10 = this;
            i.h.k.Pools<java.util.List<java.lang.Throwable>> r0 = r10.d
            java.lang.Object r0 = r0.a()
            java.lang.String r1 = "Argument must not be null"
            i.b.k.ResourcesFlusher.a(r0, r1)
            java.util.List r0 = (java.util.List) r0
            r2 = r10
            r3 = r11
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r0
            j.b.a.m.m.Resource r11 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x011c }
            i.h.k.Pools<java.util.List<java.lang.Throwable>> r12 = r10.d
            r12.a(r0)
            j.b.a.m.m.DecodeJob$b r15 = (j.b.a.m.m.DecodeJob.b) r15
            j.b.a.m.m.DecodeJob r12 = j.b.a.m.m.DecodeJob.this
            j.b.a.m.DataSource r13 = r15.a
            r15 = 0
            if (r12 == 0) goto L_0x011b
            java.lang.Object r0 = r11.get()
            java.lang.Class r8 = r0.getClass()
            j.b.a.m.DataSource r0 = j.b.a.m.DataSource.RESOURCE_DISK_CACHE
            if (r13 == r0) goto L_0x0044
            j.b.a.m.m.DecodeHelper<R> r0 = r12.b
            j.b.a.m.Transformation r0 = r0.b(r8)
            j.b.a.GlideContext r1 = r12.f1624i
            int r2 = r12.f1628m
            int r3 = r12.f1629n
            j.b.a.m.m.Resource r1 = r0.a(r1, r11, r2, r3)
            r7 = r0
            r0 = r1
            goto L_0x0046
        L_0x0044:
            r0 = r11
            r7 = r15
        L_0x0046:
            boolean r1 = r11.equals(r0)
            if (r1 != 0) goto L_0x004f
            r11.d()
        L_0x004f:
            j.b.a.m.m.DecodeHelper<R> r11 = r12.b
            j.b.a.GlideContext r11 = r11.c
            com.bumptech.glide.Registry r11 = r11.b
            j.b.a.p.ResourceEncoderRegistry r11 = r11.d
            java.lang.Class r1 = r0.c()
            j.b.a.m.ResourceEncoder r11 = r11.a(r1)
            r1 = 0
            r2 = 1
            if (r11 == 0) goto L_0x0065
            r11 = 1
            goto L_0x0066
        L_0x0065:
            r11 = 0
        L_0x0066:
            if (r11 == 0) goto L_0x008b
            j.b.a.m.m.DecodeHelper<R> r11 = r12.b
            j.b.a.GlideContext r11 = r11.c
            com.bumptech.glide.Registry r11 = r11.b
            j.b.a.p.ResourceEncoderRegistry r11 = r11.d
            java.lang.Class r15 = r0.c()
            j.b.a.m.ResourceEncoder r15 = r11.a(r15)
            if (r15 == 0) goto L_0x0081
            j.b.a.m.Options r11 = r12.f1631p
            j.b.a.m.EncodeStrategy r11 = r15.a(r11)
            goto L_0x008d
        L_0x0081:
            com.bumptech.glide.Registry$NoResultEncoderAvailableException r11 = new com.bumptech.glide.Registry$NoResultEncoderAvailableException
            java.lang.Class r12 = r0.c()
            r11.<init>(r12)
            throw r11
        L_0x008b:
            j.b.a.m.EncodeStrategy r11 = j.b.a.m.EncodeStrategy.NONE
        L_0x008d:
            j.b.a.m.m.DecodeHelper<R> r3 = r12.b
            j.b.a.m.Key r4 = r12.y
            java.util.List r3 = r3.c()
            int r5 = r3.size()
            r6 = 0
        L_0x009a:
            if (r6 >= r5) goto L_0x00af
            java.lang.Object r9 = r3.get(r6)
            j.b.a.m.n.ModelLoader$a r9 = (j.b.a.m.n.ModelLoader.a) r9
            j.b.a.m.Key r9 = r9.a
            boolean r9 = r9.equals(r4)
            if (r9 == 0) goto L_0x00ac
            r1 = 1
            goto L_0x00af
        L_0x00ac:
            int r6 = r6 + 1
            goto L_0x009a
        L_0x00af:
            r1 = r1 ^ r2
            j.b.a.m.m.DiskCacheStrategy r3 = r12.f1630o
            boolean r13 = r3.a(r1, r13, r11)
            if (r13 == 0) goto L_0x0114
            if (r15 == 0) goto L_0x0106
            int r13 = r11.ordinal()
            if (r13 == 0) goto L_0x00f0
            if (r13 != r2) goto L_0x00d9
            j.b.a.m.m.ResourceCacheKey r11 = new j.b.a.m.m.ResourceCacheKey
            j.b.a.m.m.DecodeHelper<R> r13 = r12.b
            j.b.a.GlideContext r13 = r13.c
            j.b.a.m.m.b0.ArrayPool r2 = r13.a
            j.b.a.m.Key r3 = r12.y
            j.b.a.m.Key r4 = r12.f1625j
            int r5 = r12.f1628m
            int r6 = r12.f1629n
            j.b.a.m.Options r9 = r12.f1631p
            r1 = r11
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x00f9
        L_0x00d9:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "Unknown strategy: "
            r13.append(r14)
            r13.append(r11)
            java.lang.String r11 = r13.toString()
            r12.<init>(r11)
            throw r12
        L_0x00f0:
            j.b.a.m.m.DataCacheKey r11 = new j.b.a.m.m.DataCacheKey
            j.b.a.m.Key r13 = r12.y
            j.b.a.m.Key r1 = r12.f1625j
            r11.<init>(r13, r1)
        L_0x00f9:
            j.b.a.m.m.LockedResource r0 = j.b.a.m.m.LockedResource.a(r0)
            j.b.a.m.m.DecodeJob$c<?> r12 = r12.g
            r12.a = r11
            r12.b = r15
            r12.c = r0
            goto L_0x0114
        L_0x0106:
            com.bumptech.glide.Registry$NoResultEncoderAvailableException r11 = new com.bumptech.glide.Registry$NoResultEncoderAvailableException
            java.lang.Object r12 = r0.get()
            java.lang.Class r12 = r12.getClass()
            r11.<init>(r12)
            throw r11
        L_0x0114:
            j.b.a.m.o.h.ResourceTranscoder<ResourceType, Transcode> r11 = r10.c
            j.b.a.m.m.Resource r11 = r11.a(r0, r14)
            return r11
        L_0x011b:
            throw r15
        L_0x011c:
            r11 = move-exception
            i.h.k.Pools<java.util.List<java.lang.Throwable>> r12 = r10.d
            r12.a(r0)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.DecodePath.a(j.b.a.m.l.DataRewinder, int, int, j.b.a.m.Options, j.b.a.m.m.DecodePath$a):j.b.a.m.m.Resource");
    }

    public String toString() {
        StringBuilder a2 = outline.a("DecodePath{ dataClass=");
        a2.append(this.a);
        a2.append(", decoders=");
        a2.append(this.b);
        a2.append(", transcoder=");
        a2.append(this.c);
        a2.append('}');
        return a2.toString();
    }

    public final Resource<ResourceType> a(DataRewinder<DataType> dataRewinder, int i2, int i3, g gVar, List<Throwable> list) {
        int size = this.b.size();
        Resource<ResourceType> resource = null;
        for (int i4 = 0; i4 < size; i4++) {
            ResourceDecoder resourceDecoder = (ResourceDecoder) this.b.get(i4);
            try {
                if (resourceDecoder.a(dataRewinder.a(), gVar)) {
                    resource = resourceDecoder.a(dataRewinder.a(), i2, i3, gVar);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + resourceDecoder, e2);
                }
                list.add(e2);
            }
            if (resource != null) {
                break;
            }
        }
        if (resource != null) {
            return resource;
        }
        throw new GlideException(this.f1636e, new ArrayList(list));
    }
}
