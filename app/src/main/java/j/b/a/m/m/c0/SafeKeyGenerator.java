package j.b.a.m.m.c0;

import i.b.k.ResourcesFlusher;
import i.h.k.Pools;
import j.b.a.m.Key;
import j.b.a.m.e;
import j.b.a.m.m.c0.k;
import j.b.a.s.LruCache;
import j.b.a.s.Util;
import j.b.a.s.k.FactoryPools;
import j.b.a.s.k.StateVerifier;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SafeKeyGenerator {
    public final LruCache<e, String> a = new LruCache<>(1000);
    public final Pools<k.b> b = FactoryPools.a(10, new a(this));

    public class a implements FactoryPools.b<k.b> {
        public a(SafeKeyGenerator safeKeyGenerator) {
        }

        public Object a() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    public static final class b implements FactoryPools.d {
        public final MessageDigest b;
        public final StateVerifier c = new StateVerifier.b();

        public b(MessageDigest messageDigest) {
            this.b = messageDigest;
        }

        public StateVerifier g() {
            return this.c;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.m.c0.SafeKeyGenerator$b, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public String a(Key key) {
        String a2;
        synchronized (this.a) {
            a2 = this.a.a(key);
        }
        if (a2 == null) {
            b a3 = this.b.a();
            ResourcesFlusher.a((Object) a3, "Argument must not be null");
            b bVar = a3;
            try {
                key.a(bVar.b);
                a2 = Util.a(bVar.b.digest());
            } finally {
                this.b.a(bVar);
            }
        }
        synchronized (this.a) {
            this.a.b(key, a2);
        }
        return a2;
    }
}
