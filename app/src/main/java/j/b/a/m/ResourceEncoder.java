package j.b.a.m;

import j.b.a.m.m.Resource;

public interface ResourceEncoder<T> extends Encoder<Resource<T>> {
    EncodeStrategy a(Options options);
}
