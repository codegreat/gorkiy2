package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import j.b.a.s.Util;

public final class UnitBitmapDecoder implements ResourceDecoder<Bitmap, Bitmap> {

    public static final class a implements Resource<Bitmap> {
        public final Bitmap b;

        public a(Bitmap bitmap) {
            this.b = bitmap;
        }

        public int b() {
            return Util.a(this.b);
        }

        public Class<Bitmap> c() {
            return Bitmap.class;
        }

        public void d() {
        }

        public Object get() {
            return this.b;
        }
    }

    public Resource a(Object obj, int i2, int i3, Options options) {
        return new a((Bitmap) obj);
    }

    public boolean a(Object obj, Options options) {
        Bitmap bitmap = (Bitmap) obj;
        return true;
    }
}
