package j.b.a.m.m.b0;

import j.b.a.m.m.b0.l;
import j.b.a.s.Util;
import java.util.Queue;

public abstract class BaseKeyPool<T extends l> {
    public final Queue<T> a = Util.a(20);

    public abstract T a();

    public void a(T t2) {
        if (this.a.size() < 20) {
            this.a.offer(t2);
        }
    }

    public T b() {
        T t2 = (Poolable) this.a.poll();
        return t2 == null ? a() : t2;
    }
}
