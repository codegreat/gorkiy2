package j.b.a.m.n;

import android.util.Base64;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.g;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class DataUrlLoader<Model, Data> implements ModelLoader<Model, Data> {
    public final a<Data> a;

    public interface a<Data> {
    }

    public static final class c<Model> implements ModelLoaderFactory<Model, InputStream> {
        public final a<InputStream> a = new a(this);

        public class a implements a<InputStream> {
            public a(c cVar) {
            }

            public Object a(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }
        }

        public ModelLoader<Model, InputStream> a(r rVar) {
            return new DataUrlLoader(this.a);
        }
    }

    public DataUrlLoader(a<Data> aVar) {
        this.a = aVar;
    }

    public ModelLoader.a<Data> a(Model model, int i2, int i3, g gVar) {
        return new ModelLoader.a<>(new ObjectKey(model), new b(model.toString(), this.a));
    }

    public static final class b<Data> implements DataFetcher<Data> {
        public final String b;
        public final a<Data> c;
        public Data d;

        public b(String str, a<Data> aVar) {
            this.b = str;
            this.c = aVar;
        }

        public void a(f fVar, DataFetcher.a<? super Data> aVar) {
            try {
                Data a = ((c.a) this.c).a(this.b);
                this.d = a;
                aVar.a((Object) a);
            } catch (IllegalArgumentException e2) {
                aVar.a((Exception) e2);
            }
        }

        public void b() {
            try {
                a<Data> aVar = this.c;
                Data data = this.d;
                if (((c.a) aVar) != null) {
                    ((InputStream) data).close();
                    return;
                }
                throw null;
            } catch (IOException unused) {
            }
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Data> a() {
            if (((c.a) this.c) != null) {
                return InputStream.class;
            }
            throw null;
        }
    }

    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }
}
