package j.b.a.m.m;

import i.b.k.ResourcesFlusher;
import i.h.k.Pools;
import j.b.a.s.k.FactoryPools;
import j.b.a.s.k.StateVerifier;
import j.b.a.s.k.a;

public final class LockedResource<Z> implements Resource<Z>, a.d {

    /* renamed from: f  reason: collision with root package name */
    public static final Pools<LockedResource<?>> f1662f = FactoryPools.a(20, new a());
    public final StateVerifier b = new StateVerifier.b();
    public Resource<Z> c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1663e;

    public class a implements FactoryPools.b<LockedResource<?>> {
        public Object a() {
            return new LockedResource();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.m.LockedResource<Z>, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public static <Z> LockedResource<Z> a(Resource<Z> resource) {
        LockedResource<Z> a2 = f1662f.a();
        ResourcesFlusher.a((Object) a2, "Argument must not be null");
        a2.f1663e = false;
        a2.d = true;
        a2.c = resource;
        return a2;
    }

    public int b() {
        return this.c.b();
    }

    public Class<Z> c() {
        return this.c.c();
    }

    public synchronized void d() {
        this.b.a();
        this.f1663e = true;
        if (!this.d) {
            this.c.d();
            this.c = null;
            f1662f.a(this);
        }
    }

    public StateVerifier g() {
        return this.b;
    }

    public Z get() {
        return this.c.get();
    }

    public synchronized void a() {
        this.b.a();
        if (this.d) {
            this.d = false;
            if (this.f1663e) {
                d();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }
}
