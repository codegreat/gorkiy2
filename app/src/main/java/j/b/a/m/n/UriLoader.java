package j.b.a.m.n;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import j.b.a.m.Options;
import j.b.a.m.l.AssetFileDescriptorLocalUriFetcher;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.l.FileDescriptorLocalUriFetcher;
import j.b.a.m.l.StreamLocalUriFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UriLoader<Data> implements ModelLoader<Uri, Data> {
    public static final Set<String> b = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", "content")));
    public final c<Data> a;

    public static final class a implements ModelLoaderFactory<Uri, AssetFileDescriptor>, c<AssetFileDescriptor> {
        public final ContentResolver a;

        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        public ModelLoader<Uri, AssetFileDescriptor> a(r rVar) {
            return new UriLoader(this);
        }

        public DataFetcher<AssetFileDescriptor> a(Uri uri) {
            return new AssetFileDescriptorLocalUriFetcher(this.a, uri);
        }
    }

    public static class b implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, c<ParcelFileDescriptor> {
        public final ContentResolver a;

        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        public DataFetcher<ParcelFileDescriptor> a(Uri uri) {
            return new FileDescriptorLocalUriFetcher(this.a, uri);
        }

        public ModelLoader<Uri, ParcelFileDescriptor> a(r rVar) {
            return new UriLoader(this);
        }
    }

    public interface c<Data> {
        DataFetcher<Data> a(Uri uri);
    }

    public static class d implements ModelLoaderFactory<Uri, InputStream>, c<InputStream> {
        public final ContentResolver a;

        public d(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        public DataFetcher<InputStream> a(Uri uri) {
            return new StreamLocalUriFetcher(this.a, uri);
        }

        public ModelLoader<Uri, InputStream> a(r rVar) {
            return new UriLoader(this);
        }
    }

    public UriLoader(c<Data> cVar) {
        this.a = cVar;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri = (Uri) obj;
        return new ModelLoader.a(new ObjectKey(uri), this.a.a(uri));
    }

    public boolean a(Object obj) {
        return b.contains(((Uri) obj).getScheme());
    }
}
