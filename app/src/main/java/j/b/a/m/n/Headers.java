package j.b.a.m.n;

import j.b.a.m.n.LazyHeaders;
import java.util.Map;

public interface Headers {
    public static final Headers a;

    static {
        LazyHeaders.a aVar = new LazyHeaders.a();
        aVar.a = true;
        a = new LazyHeaders(aVar.b);
    }

    Map<String, String> a();
}
