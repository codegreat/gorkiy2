package j.b.a.m.n;

import i.h.k.Pools;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelLoaderRegistry {
    public final MultiModelLoaderFactory a;
    public final a b = new a();

    public static class a {
        public final Map<Class<?>, C0015a<?>> a = new HashMap();

        /* renamed from: j.b.a.m.n.ModelLoaderRegistry$a$a  reason: collision with other inner class name */
        public static class C0015a<Model> {
            public final List<ModelLoader<Model, ?>> a;

            public C0015a(List<ModelLoader<Model, ?>> list) {
                this.a = list;
            }
        }
    }

    public ModelLoaderRegistry(Pools<List<Throwable>> pools) {
        MultiModelLoaderFactory multiModelLoaderFactory = new MultiModelLoaderFactory(pools);
        this.a = multiModelLoaderFactory;
    }

    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        this.a.a(cls, cls2, modelLoaderFactory);
        this.b.a.clear();
    }

    public final synchronized <A> List<ModelLoader<A, ?>> b(Class<A> cls) {
        List<T> list;
        a.C0015a aVar = this.b.a.get(cls);
        if (aVar == null) {
            list = null;
        } else {
            list = aVar.a;
        }
        if (list == null) {
            list = Collections.unmodifiableList(this.a.a(cls));
            if (this.b.a.put(cls, new a.C0015a(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }
        return list;
    }

    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.a.b(cls);
    }
}
