package j.b.a.m.m.c0;

import android.content.Context;

/* compiled from: InternalCacheDiskCacheFactory */
public final class InternalCacheDiskCacheFactory0 extends DiskLruCacheFactory {
    public InternalCacheDiskCacheFactory0(Context context) {
        super(new InternalCacheDiskCacheFactory(context, "image_manager_disk_cache"), 262144000);
    }
}
