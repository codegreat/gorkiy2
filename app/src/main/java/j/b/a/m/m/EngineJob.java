package j.b.a.m.m;

import com.bumptech.glide.load.engine.GlideException;
import i.b.k.ResourcesFlusher;
import i.h.k.Pools;
import j.b.a.m.DataSource;
import j.b.a.m.Key;
import j.b.a.m.m.DecodeJob;
import j.b.a.m.m.EngineResource;
import j.b.a.m.m.d0.GlideExecutor;
import j.b.a.m.m.m;
import j.b.a.m.m.q;
import j.b.a.q.ResourceCallback;
import j.b.a.q.SingleRequest;
import j.b.a.s.Executors;
import j.b.a.s.k.StateVerifier;
import j.b.a.s.k.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public class EngineJob<R> implements DecodeJob.a<R>, a.d {
    public static final c z = new c();
    public final e b = new e();
    public final StateVerifier c = new StateVerifier.b();
    public final EngineResource.a d;

    /* renamed from: e  reason: collision with root package name */
    public final Pools<EngineJob<?>> f1642e;

    /* renamed from: f  reason: collision with root package name */
    public final c f1643f;
    public final EngineJobListener g;
    public final GlideExecutor h;

    /* renamed from: i  reason: collision with root package name */
    public final GlideExecutor f1644i;

    /* renamed from: j  reason: collision with root package name */
    public final GlideExecutor f1645j;

    /* renamed from: k  reason: collision with root package name */
    public final GlideExecutor f1646k;

    /* renamed from: l  reason: collision with root package name */
    public final AtomicInteger f1647l = new AtomicInteger();

    /* renamed from: m  reason: collision with root package name */
    public Key f1648m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f1649n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f1650o;

    /* renamed from: p  reason: collision with root package name */
    public boolean f1651p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1652q;

    /* renamed from: r  reason: collision with root package name */
    public Resource<?> f1653r;

    /* renamed from: s  reason: collision with root package name */
    public DataSource f1654s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f1655t;
    public GlideException u;
    public boolean v;
    public EngineResource<?> w;
    public DecodeJob<R> x;
    public volatile boolean y;

    public class a implements Runnable {
        public final ResourceCallback b;

        public a(ResourceCallback resourceCallback) {
            this.b = resourceCallback;
        }

        public void run() {
            SingleRequest singleRequest = (SingleRequest) this.b;
            singleRequest.b.a();
            synchronized (singleRequest.c) {
                synchronized (EngineJob.this) {
                    if (EngineJob.this.b.b.contains(new d(this.b, Executors.b))) {
                        EngineJob engineJob = EngineJob.this;
                        ResourceCallback resourceCallback = this.b;
                        if (engineJob != null) {
                            try {
                                ((SingleRequest) resourceCallback).a(engineJob.u, 5);
                            } catch (Throwable th) {
                                throw new CallbackException(th);
                            }
                        } else {
                            throw null;
                        }
                    }
                    EngineJob.this.a();
                }
            }
        }
    }

    public class b implements Runnable {
        public final ResourceCallback b;

        public b(ResourceCallback resourceCallback) {
            this.b = resourceCallback;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.b.a.q.SingleRequest.a(j.b.a.m.m.Resource<?>, j.b.a.m.a):void
         arg types: [j.b.a.m.m.EngineResource<?>, j.b.a.m.DataSource]
         candidates:
          j.b.a.q.SingleRequest.a(int, int):void
          j.b.a.q.SingleRequest.a(com.bumptech.glide.load.engine.GlideException, int):void
          j.b.a.q.SingleRequest.a(j.b.a.m.m.Resource<?>, j.b.a.m.a):void */
        public void run() {
            SingleRequest singleRequest = (SingleRequest) this.b;
            singleRequest.b.a();
            synchronized (singleRequest.c) {
                synchronized (EngineJob.this) {
                    if (EngineJob.this.b.b.contains(new d(this.b, Executors.b))) {
                        EngineJob.this.w.a();
                        EngineJob engineJob = EngineJob.this;
                        ResourceCallback resourceCallback = this.b;
                        if (engineJob != null) {
                            try {
                                ((SingleRequest) resourceCallback).a((Resource<?>) engineJob.w, (j.b.a.m.a) engineJob.f1654s);
                                EngineJob.this.a(this.b);
                            } catch (Throwable th) {
                                throw new CallbackException(th);
                            }
                        } else {
                            throw null;
                        }
                    }
                    EngineJob.this.a();
                }
            }
        }
    }

    public static class c {
    }

    public static final class d {
        public final ResourceCallback a;
        public final Executor b;

        public d(ResourceCallback resourceCallback, Executor executor) {
            this.a = resourceCallback;
            this.b = executor;
        }

        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.a.equals(((d) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    public static final class e implements Iterable<m.d> {
        public final List<m.d> b = new ArrayList(2);

        public boolean isEmpty() {
            return this.b.isEmpty();
        }

        public Iterator<m.d> iterator() {
            return this.b.iterator();
        }
    }

    public EngineJob(j.b.a.m.m.d0.a aVar, j.b.a.m.m.d0.a aVar2, j.b.a.m.m.d0.a aVar3, j.b.a.m.m.d0.a aVar4, n nVar, q.a aVar5, Pools<EngineJob<?>> pools) {
        c cVar = z;
        this.h = aVar;
        this.f1644i = aVar2;
        this.f1645j = aVar3;
        this.f1646k = aVar4;
        this.g = nVar;
        this.d = aVar5;
        this.f1642e = pools;
        this.f1643f = cVar;
    }

    public synchronized EngineJob<R> a(j.b.a.m.e eVar, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.f1648m = eVar;
        this.f1649n = z2;
        this.f1650o = z3;
        this.f1651p = z4;
        this.f1652q = z5;
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(j.b.a.m.m.DecodeJob<R> r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            r2.x = r3     // Catch:{ all -> 0x0035 }
            if (r3 == 0) goto L_0x0033
            j.b.a.m.m.DecodeJob$g r0 = j.b.a.m.m.DecodeJob.g.INITIALIZE     // Catch:{ all -> 0x0035 }
            j.b.a.m.m.DecodeJob$g r0 = r3.a(r0)     // Catch:{ all -> 0x0035 }
            j.b.a.m.m.DecodeJob$g r1 = j.b.a.m.m.DecodeJob.g.RESOURCE_CACHE     // Catch:{ all -> 0x0035 }
            if (r0 == r1) goto L_0x0016
            j.b.a.m.m.DecodeJob$g r1 = j.b.a.m.m.DecodeJob.g.DATA_CACHE     // Catch:{ all -> 0x0035 }
            if (r0 != r1) goto L_0x0014
            goto L_0x0016
        L_0x0014:
            r0 = 0
            goto L_0x0017
        L_0x0016:
            r0 = 1
        L_0x0017:
            if (r0 == 0) goto L_0x001c
            j.b.a.m.m.d0.GlideExecutor r0 = r2.h     // Catch:{ all -> 0x0035 }
            goto L_0x002c
        L_0x001c:
            boolean r0 = r2.f1650o     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x0023
            j.b.a.m.m.d0.GlideExecutor r0 = r2.f1645j     // Catch:{ all -> 0x0035 }
            goto L_0x002c
        L_0x0023:
            boolean r0 = r2.f1651p     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x002a
            j.b.a.m.m.d0.GlideExecutor r0 = r2.f1646k     // Catch:{ all -> 0x0035 }
            goto L_0x002c
        L_0x002a:
            j.b.a.m.m.d0.GlideExecutor r0 = r2.f1644i     // Catch:{ all -> 0x0035 }
        L_0x002c:
            java.util.concurrent.ExecutorService r0 = r0.a     // Catch:{ all -> 0x0035 }
            r0.execute(r3)     // Catch:{ all -> 0x0035 }
            monitor-exit(r2)
            return
        L_0x0033:
            r3 = 0
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x0035:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.EngineJob.b(j.b.a.m.m.DecodeJob):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
     arg types: [j.b.a.m.m.EngineJob, j.b.a.m.Key, ?[OBJECT, ARRAY]]
     candidates:
      j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void
      j.b.a.m.m.Engine.a(j.b.a.m.m.o, boolean, long):j.b.a.m.m.EngineResource<?>
      j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        ((j.b.a.m.m.Engine) r5.g).a((j.b.a.m.m.EngineJob<?>) r5, (j.b.a.m.e) r1, (j.b.a.m.m.EngineResource<?>) null);
        r0 = r4.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        if (r0.hasNext() == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0046, code lost:
        r1 = (j.b.a.m.m.EngineJob.d) r0.next();
        r1.b.execute(new j.b.a.m.m.EngineJob.a(r5, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0059, code lost:
        a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r5 = this;
            monitor-enter(r5)
            j.b.a.s.k.StateVerifier r0 = r5.c     // Catch:{ all -> 0x006e }
            r0.a()     // Catch:{ all -> 0x006e }
            boolean r0 = r5.y     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x000f
            r5.e()     // Catch:{ all -> 0x006e }
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            return
        L_0x000f:
            j.b.a.m.m.EngineJob$e r0 = r5.b     // Catch:{ all -> 0x006e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x006e }
            if (r0 != 0) goto L_0x0066
            boolean r0 = r5.v     // Catch:{ all -> 0x006e }
            if (r0 != 0) goto L_0x005e
            r0 = 1
            r5.v = r0     // Catch:{ all -> 0x006e }
            j.b.a.m.Key r1 = r5.f1648m     // Catch:{ all -> 0x006e }
            j.b.a.m.m.EngineJob$e r2 = r5.b     // Catch:{ all -> 0x006e }
            r3 = 0
            if (r2 == 0) goto L_0x005d
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x006e }
            java.util.List<j.b.a.m.m.m$d> r2 = r2.b     // Catch:{ all -> 0x006e }
            r4.<init>(r2)     // Catch:{ all -> 0x006e }
            int r2 = r4.size()     // Catch:{ all -> 0x006e }
            int r2 = r2 + r0
            r5.a(r2)     // Catch:{ all -> 0x006e }
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            j.b.a.m.m.EngineJobListener r0 = r5.g
            j.b.a.m.m.Engine r0 = (j.b.a.m.m.Engine) r0
            r0.a(r5, r1, r3)
            java.util.Iterator r0 = r4.iterator()
        L_0x0040:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0059
            java.lang.Object r1 = r0.next()
            j.b.a.m.m.EngineJob$d r1 = (j.b.a.m.m.EngineJob.d) r1
            java.util.concurrent.Executor r2 = r1.b
            j.b.a.m.m.EngineJob$a r3 = new j.b.a.m.m.EngineJob$a
            j.b.a.q.ResourceCallback r1 = r1.a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x0040
        L_0x0059:
            r5.a()
            return
        L_0x005d:
            throw r3     // Catch:{ all -> 0x006e }
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x006e }
            java.lang.String r1 = "Already failed once"
            r0.<init>(r1)     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x0066:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x006e }
            java.lang.String r1 = "Received an exception without any callbacks to notify"
            r0.<init>(r1)     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x006e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.EngineJob.c():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
     arg types: [j.b.a.m.m.EngineJob, j.b.a.m.Key, j.b.a.m.m.EngineResource<?>]
     candidates:
      j.b.a.m.m.Engine.a(java.lang.String, long, j.b.a.m.Key):void
      j.b.a.m.m.Engine.a(j.b.a.m.m.o, boolean, long):j.b.a.m.m.EngineResource<?>
      j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
        ((j.b.a.m.m.Engine) r8.g).a((j.b.a.m.m.EngineJob<?>) r8, (j.b.a.m.e) r0, r1);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0060, code lost:
        if (r0.hasNext() == false) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0062, code lost:
        r1 = (j.b.a.m.m.EngineJob.d) r0.next();
        r1.b.execute(new j.b.a.m.m.EngineJob.b(r8, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0075, code lost:
        a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0078, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r8 = this;
            monitor-enter(r8)
            j.b.a.s.k.StateVerifier r0 = r8.c     // Catch:{ all -> 0x008b }
            r0.a()     // Catch:{ all -> 0x008b }
            boolean r0 = r8.y     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0014
            j.b.a.m.m.Resource<?> r0 = r8.f1653r     // Catch:{ all -> 0x008b }
            r0.d()     // Catch:{ all -> 0x008b }
            r8.e()     // Catch:{ all -> 0x008b }
            monitor-exit(r8)     // Catch:{ all -> 0x008b }
            return
        L_0x0014:
            j.b.a.m.m.EngineJob$e r0 = r8.b     // Catch:{ all -> 0x008b }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x008b }
            if (r0 != 0) goto L_0x0083
            boolean r0 = r8.f1655t     // Catch:{ all -> 0x008b }
            if (r0 != 0) goto L_0x007b
            j.b.a.m.m.EngineJob$c r0 = r8.f1643f     // Catch:{ all -> 0x008b }
            j.b.a.m.m.Resource<?> r2 = r8.f1653r     // Catch:{ all -> 0x008b }
            boolean r3 = r8.f1649n     // Catch:{ all -> 0x008b }
            j.b.a.m.Key r5 = r8.f1648m     // Catch:{ all -> 0x008b }
            j.b.a.m.m.EngineResource$a r6 = r8.d     // Catch:{ all -> 0x008b }
            r7 = 0
            if (r0 == 0) goto L_0x007a
            j.b.a.m.m.EngineResource r0 = new j.b.a.m.m.EngineResource     // Catch:{ all -> 0x008b }
            r4 = 1
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x008b }
            r8.w = r0     // Catch:{ all -> 0x008b }
            r0 = 1
            r8.f1655t = r0     // Catch:{ all -> 0x008b }
            j.b.a.m.m.EngineJob$e r1 = r8.b     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x0079
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x008b }
            java.util.List<j.b.a.m.m.m$d> r1 = r1.b     // Catch:{ all -> 0x008b }
            r2.<init>(r1)     // Catch:{ all -> 0x008b }
            int r1 = r2.size()     // Catch:{ all -> 0x008b }
            int r1 = r1 + r0
            r8.a(r1)     // Catch:{ all -> 0x008b }
            j.b.a.m.Key r0 = r8.f1648m     // Catch:{ all -> 0x008b }
            j.b.a.m.m.EngineResource<?> r1 = r8.w     // Catch:{ all -> 0x008b }
            monitor-exit(r8)     // Catch:{ all -> 0x008b }
            j.b.a.m.m.EngineJobListener r3 = r8.g
            j.b.a.m.m.Engine r3 = (j.b.a.m.m.Engine) r3
            r3.a(r8, r0, r1)
            java.util.Iterator r0 = r2.iterator()
        L_0x005c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0075
            java.lang.Object r1 = r0.next()
            j.b.a.m.m.EngineJob$d r1 = (j.b.a.m.m.EngineJob.d) r1
            java.util.concurrent.Executor r2 = r1.b
            j.b.a.m.m.EngineJob$b r3 = new j.b.a.m.m.EngineJob$b
            j.b.a.q.ResourceCallback r1 = r1.a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x005c
        L_0x0075:
            r8.a()
            return
        L_0x0079:
            throw r7     // Catch:{ all -> 0x008b }
        L_0x007a:
            throw r7     // Catch:{ all -> 0x008b }
        L_0x007b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "Already have resource"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x0083:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "Received a resource without any callbacks to notify"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x008b:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x008b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.m.EngineJob.d():void");
    }

    public final synchronized void e() {
        if (this.f1648m != null) {
            this.b.b.clear();
            this.f1648m = null;
            this.w = null;
            this.f1653r = null;
            this.v = false;
            this.y = false;
            this.f1655t = false;
            DecodeJob<R> decodeJob = this.x;
            if (decodeJob.h.b(false)) {
                decodeJob.l();
            }
            this.x = null;
            this.u = null;
            this.f1654s = null;
            this.f1642e.a(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public StateVerifier g() {
        return this.c;
    }

    public synchronized void a(ResourceCallback resourceCallback, Executor executor) {
        this.c.a();
        this.b.b.add(new d(resourceCallback, executor));
        boolean z2 = true;
        if (this.f1655t) {
            a(1);
            executor.execute(new b(resourceCallback));
        } else if (this.v) {
            a(1);
            executor.execute(new a(resourceCallback));
        } else {
            if (this.y) {
                z2 = false;
            }
            ResourcesFlusher.a(z2, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    public final boolean b() {
        return this.v || this.f1655t || this.y;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e):void
     arg types: [j.b.a.m.m.EngineJob, j.b.a.m.Key]
     candidates:
      j.b.a.m.m.Engine.a(j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
      j.b.a.m.m.EngineResource.a.a(j.b.a.m.e, j.b.a.m.m.EngineResource<?>):void
      j.b.a.m.m.Engine.a(j.b.a.m.m.EngineJob<?>, j.b.a.m.e):void */
    public synchronized void a(ResourceCallback resourceCallback) {
        this.c.a();
        this.b.b.remove(new d(resourceCallback, Executors.b));
        if (this.b.isEmpty()) {
            boolean z2 = true;
            if (!b()) {
                this.y = true;
                DecodeJob<R> decodeJob = this.x;
                decodeJob.F = true;
                DataFetcherGenerator dataFetcherGenerator = decodeJob.D;
                if (dataFetcherGenerator != null) {
                    dataFetcherGenerator.cancel();
                }
                ((Engine) this.g).a((EngineJob<?>) this, (j.b.a.m.e) this.f1648m);
            }
            if (!this.f1655t) {
                if (!this.v) {
                    z2 = false;
                }
            }
            if (z2 && this.f1647l.get() == 0) {
                e();
            }
        }
    }

    public void a(DecodeJob<?> decodeJob) {
        GlideExecutor glideExecutor;
        if (this.f1650o) {
            glideExecutor = this.f1645j;
        } else {
            glideExecutor = this.f1651p ? this.f1646k : this.f1644i;
        }
        glideExecutor.a.execute(decodeJob);
    }

    public synchronized void a(int i2) {
        ResourcesFlusher.a(b(), "Not yet complete!");
        if (this.f1647l.getAndAdd(i2) == 0 && this.w != null) {
            this.w.a();
        }
    }

    public void a() {
        EngineResource<?> engineResource;
        synchronized (this) {
            this.c.a();
            ResourcesFlusher.a(b(), "Not yet complete!");
            int decrementAndGet = this.f1647l.decrementAndGet();
            ResourcesFlusher.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                engineResource = this.w;
                e();
            } else {
                engineResource = null;
            }
        }
        if (engineResource != null) {
            engineResource.e();
        }
    }

    public void a(Resource resource, j.b.a.m.a aVar) {
        synchronized (this) {
            this.f1653r = resource;
            this.f1654s = aVar;
        }
        d();
    }

    public void a(GlideException glideException) {
        synchronized (this) {
            this.u = glideException;
        }
        c();
    }
}
