package j.b.a.m.o.g;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import j.a.a.a.outline;
import j.b.a.l.GifHeader;
import j.b.a.l.GifHeaderParser;
import j.b.a.l.StandardGifDecoder;
import j.b.a.m.DecodeFormat;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.d;
import j.b.a.m.o.UnitTransformation;
import j.b.a.s.LogTime;
import j.b.a.s.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

public class ByteBufferGifDecoder implements ResourceDecoder<ByteBuffer, c> {

    /* renamed from: f  reason: collision with root package name */
    public static final a f1718f = new a();
    public static final b g = new b();
    public final Context a;
    public final List<ImageHeaderParser> b;
    public final b c;
    public final a d;

    /* renamed from: e  reason: collision with root package name */
    public final GifBitmapProvider f1719e;

    public static class a {
    }

    public ByteBufferGifDecoder(Context context, List<ImageHeaderParser> list, d dVar, j.b.a.m.m.b0.b bVar) {
        b bVar2 = g;
        a aVar = f1718f;
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.f1719e = new GifBitmapProvider(dVar, bVar);
        this.c = bVar2;
    }

    public Resource a(Object obj, int i2, int i3, Options options) {
        ByteBuffer byteBuffer = (ByteBuffer) obj;
        GifHeaderParser a2 = this.c.a(byteBuffer);
        try {
            return a(byteBuffer, i2, i3, a2, options);
        } finally {
            this.c.a(a2);
        }
    }

    public static class b {
        public final Queue<j.b.a.l.d> a = Util.a(0);

        public synchronized void a(GifHeaderParser gifHeaderParser) {
            gifHeaderParser.b = null;
            gifHeaderParser.c = null;
            this.a.offer(gifHeaderParser);
        }

        public synchronized GifHeaderParser a(ByteBuffer byteBuffer) {
            GifHeaderParser poll;
            poll = this.a.poll();
            if (poll == null) {
                poll = new GifHeaderParser();
            }
            poll.b = null;
            Arrays.fill(poll.a, (byte) 0);
            poll.c = new GifHeader();
            poll.d = 0;
            ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
            poll.b = asReadOnlyBuffer;
            asReadOnlyBuffer.position(0);
            poll.b.order(ByteOrder.LITTLE_ENDIAN);
            return poll;
        }
    }

    public boolean a(Object obj, Options options) {
        ImageHeaderParser.ImageType imageType;
        ByteBuffer byteBuffer = (ByteBuffer) obj;
        if (((Boolean) options.a(GifOptions.b)).booleanValue()) {
            return false;
        }
        List<ImageHeaderParser> list = this.b;
        if (byteBuffer == null) {
            imageType = ImageHeaderParser.ImageType.UNKNOWN;
        } else {
            int size = list.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    imageType = ImageHeaderParser.ImageType.UNKNOWN;
                    break;
                }
                ImageHeaderParser.ImageType a2 = list.get(i2).a(byteBuffer);
                if (a2 != ImageHeaderParser.ImageType.UNKNOWN) {
                    imageType = a2;
                    break;
                }
                i2++;
            }
        }
        if (imageType == ImageHeaderParser.ImageType.GIF) {
            return true;
        }
        return false;
    }

    public static int a(GifHeader gifHeader, int i2, int i3) {
        int i4;
        int min = Math.min(gifHeader.g / i3, gifHeader.f1577f / i2);
        if (min == 0) {
            i4 = 0;
        } else {
            i4 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i4);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i2 + "x" + i3 + "], actual dimens: [" + gifHeader.f1577f + "x" + gifHeader.g + "]");
        }
        return max;
    }

    public final GifDrawableResource a(ByteBuffer byteBuffer, int i2, int i3, GifHeaderParser gifHeaderParser, Options options) {
        Bitmap.Config config;
        long a2 = LogTime.a();
        try {
            GifHeader b2 = gifHeaderParser.b();
            if (b2.c > 0) {
                if (b2.b == 0) {
                    if (options.a(GifOptions.a) == DecodeFormat.PREFER_RGB_565) {
                        config = Bitmap.Config.RGB_565;
                    } else {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    int a3 = a(b2, i2, i3);
                    a aVar = this.d;
                    GifBitmapProvider gifBitmapProvider = this.f1719e;
                    if (aVar != null) {
                        StandardGifDecoder standardGifDecoder = new StandardGifDecoder(gifBitmapProvider, b2, byteBuffer, a3);
                        standardGifDecoder.a(config);
                        standardGifDecoder.d();
                        Bitmap c2 = standardGifDecoder.c();
                        if (c2 == null) {
                            if (Log.isLoggable("BufferGifDecoder", 2)) {
                                StringBuilder a4 = outline.a("Decoded GIF from stream in ");
                                a4.append(LogTime.a(a2));
                                Log.v("BufferGifDecoder", a4.toString());
                            }
                            return null;
                        }
                        GifDrawableResource gifDrawableResource = new GifDrawableResource(new GifDrawable(this.a, standardGifDecoder, (UnitTransformation) UnitTransformation.b, i2, i3, c2));
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            StringBuilder a5 = outline.a("Decoded GIF from stream in ");
                            a5.append(LogTime.a(a2));
                            Log.v("BufferGifDecoder", a5.toString());
                        }
                        return gifDrawableResource;
                    }
                    throw null;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                StringBuilder a6 = outline.a("Decoded GIF from stream in ");
                a6.append(LogTime.a(a2));
                Log.v("BufferGifDecoder", a6.toString());
            }
        }
    }
}
