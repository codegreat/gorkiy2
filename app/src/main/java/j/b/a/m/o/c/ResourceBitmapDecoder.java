package j.b.a.m.o.c;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.o.e.ResourceDrawableDecoder;

public class ResourceBitmapDecoder implements ResourceDecoder<Uri, Bitmap> {
    public final ResourceDrawableDecoder a;
    public final BitmapPool b;

    public ResourceBitmapDecoder(ResourceDrawableDecoder resourceDrawableDecoder, BitmapPool bitmapPool) {
        this.a = resourceDrawableDecoder;
        this.b = bitmapPool;
    }

    public Resource a(Object obj, int i2, int i3, Options options) {
        Resource a2 = this.a.a((Uri) obj);
        if (a2 == null) {
            return null;
        }
        return DrawableToBitmapConverter.a(this.b, (Drawable) a2.get(), i2, i3);
    }

    public boolean a(Object obj, Options options) {
        return "android.resource".equals(((Uri) obj).getScheme());
    }
}
