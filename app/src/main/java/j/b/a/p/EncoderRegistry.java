package j.b.a.p;

import j.b.a.m.Encoder;
import java.util.ArrayList;
import java.util.List;

public class EncoderRegistry {
    public final List<a<?>> a = new ArrayList();

    public static final class a<T> {
        public final Class<T> a;
        public final Encoder<T> b;

        public a(Class<T> cls, Encoder<T> encoder) {
            this.a = cls;
            this.b = encoder;
        }
    }

    public synchronized <T> Encoder<T> a(Class<T> cls) {
        for (a next : this.a) {
            if (next.a.isAssignableFrom(cls)) {
                return next.b;
            }
        }
        return null;
    }

    public synchronized <T> void a(Class<T> cls, Encoder<T> encoder) {
        this.a.add(new a(cls, encoder));
    }
}
