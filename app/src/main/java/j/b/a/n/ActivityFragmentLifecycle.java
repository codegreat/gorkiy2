package j.b.a.n;

import j.b.a.s.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public class ActivityFragmentLifecycle implements Lifecycle {
    public final Set<i> a = Collections.newSetFromMap(new WeakHashMap());
    public boolean b;
    public boolean c;

    public void a(LifecycleListener lifecycleListener) {
        this.a.add(lifecycleListener);
        if (this.c) {
            lifecycleListener.d();
        } else if (this.b) {
            lifecycleListener.c();
        } else {
            lifecycleListener.a();
        }
    }

    public void b(LifecycleListener lifecycleListener) {
        this.a.remove(lifecycleListener);
    }

    public void c() {
        this.b = false;
        Iterator it = ((ArrayList) Util.a(this.a)).iterator();
        while (it.hasNext()) {
            ((LifecycleListener) it.next()).a();
        }
    }

    public void b() {
        this.b = true;
        Iterator it = ((ArrayList) Util.a(this.a)).iterator();
        while (it.hasNext()) {
            ((LifecycleListener) it.next()).c();
        }
    }

    public void a() {
        this.c = true;
        Iterator it = ((ArrayList) Util.a(this.a)).iterator();
        while (it.hasNext()) {
            ((LifecycleListener) it.next()).d();
        }
    }
}
