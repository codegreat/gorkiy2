package j.b.a;

import android.annotation.SuppressLint;
import android.content.Context;
import i.b.k.ResourcesFlusher;
import j.b.a.m.m.DiskCacheStrategy;
import j.b.a.m.m.k;
import j.b.a.q.BaseRequestOptions;
import j.b.a.q.Request;
import j.b.a.q.RequestListener;
import j.b.a.q.RequestOptions;
import j.b.a.q.SingleRequest;
import j.b.a.q.b;
import j.b.a.q.c;
import j.b.a.q.h.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class RequestBuilder<TranscodeType> extends BaseRequestOptions<RequestBuilder<TranscodeType>> implements Cloneable {
    public final Context B;
    public final RequestManager C;
    public final Class<TranscodeType> D;
    public final GlideContext E;
    public TransitionOptions<?, ? super TranscodeType> F;
    public Object G;
    public List<RequestListener<TranscodeType>> H;
    public boolean I = true;
    public boolean J;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|5|6|7|8|9|(2:11|12)|13|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|32) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|8|9|11|12|13|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|32) */
        /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x003f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x005e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0069 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0074 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
        static {
            /*
                j.b.a.Priority[] r0 = j.b.a.Priority.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.b.a.RequestBuilder.a.b = r0
                r1 = 1
                r2 = 3
                j.b.a.Priority r3 = j.b.a.Priority.LOW     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.b.a.RequestBuilder.a.b     // Catch:{ NoSuchFieldError -> 0x0016 }
                j.b.a.Priority r4 = j.b.a.Priority.NORMAL     // Catch:{ NoSuchFieldError -> 0x0016 }
                r3[r0] = r0     // Catch:{ NoSuchFieldError -> 0x0016 }
            L_0x0016:
                int[] r3 = j.b.a.RequestBuilder.a.b     // Catch:{ NoSuchFieldError -> 0x001c }
                j.b.a.Priority r4 = j.b.a.Priority.HIGH     // Catch:{ NoSuchFieldError -> 0x001c }
                r3[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001c }
            L_0x001c:
                r3 = 4
                int[] r4 = j.b.a.RequestBuilder.a.b     // Catch:{ NoSuchFieldError -> 0x0024 }
                j.b.a.Priority r5 = j.b.a.Priority.IMMEDIATE     // Catch:{ NoSuchFieldError -> 0x0024 }
                r5 = 0
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0024 }
            L_0x0024:
                android.widget.ImageView$ScaleType[] r4 = android.widget.ImageView.ScaleType.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                j.b.a.RequestBuilder.a.a = r4
                android.widget.ImageView$ScaleType r5 = android.widget.ImageView.ScaleType.CENTER_CROP     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r1 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x003f }
                android.widget.ImageView$ScaleType r4 = android.widget.ImageView.ScaleType.CENTER_INSIDE     // Catch:{ NoSuchFieldError -> 0x003f }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x003f }
                r1[r4] = r0     // Catch:{ NoSuchFieldError -> 0x003f }
            L_0x003f:
                int[] r0 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x0049 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x0053 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_START     // Catch:{ NoSuchFieldError -> 0x0053 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0053 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0053 }
            L_0x0053:
                int[] r0 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x005e }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ NoSuchFieldError -> 0x005e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005e }
            L_0x005e:
                int[] r0 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x0069 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_XY     // Catch:{ NoSuchFieldError -> 0x0069 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0069 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0069 }
            L_0x0069:
                int[] r0 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x0074 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER     // Catch:{ NoSuchFieldError -> 0x0074 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0074 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0074 }
            L_0x0074:
                int[] r0 = j.b.a.RequestBuilder.a.a     // Catch:{ NoSuchFieldError -> 0x0080 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.MATRIX     // Catch:{ NoSuchFieldError -> 0x0080 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0080 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0080 }
            L_0x0080:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.b.a.RequestBuilder.a.<clinit>():void");
        }
    }

    static {
        RequestOptions requestOptions = (RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().a((k) DiskCacheStrategy.b)).a((f) Priority.LOW)).a(true);
    }

    @SuppressLint({"CheckResult"})
    public RequestBuilder(b bVar, i iVar, Class<TranscodeType> cls, Context context) {
        this.C = iVar;
        this.D = cls;
        this.B = context;
        GlideContext glideContext = iVar.b.d;
        TransitionOptions transitionOptions = glideContext.f1548f.get(cls);
        if (transitionOptions == null) {
            for (Map.Entry next : glideContext.f1548f.entrySet()) {
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    transitionOptions = (TransitionOptions) next.getValue();
                }
            }
        }
        this.F = transitionOptions == null ? GlideContext.f1546k : transitionOptions;
        this.E = bVar.d;
        for (RequestListener next2 : iVar.f1556k) {
            if (next2 != null) {
                if (this.H == null) {
                    this.H = new ArrayList();
                }
                this.H.add(next2);
            }
        }
        a((BaseRequestOptions<?>) iVar.e());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.q.BaseRequestOptions<?>, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public RequestBuilder<TranscodeType> a(BaseRequestOptions<?> baseRequestOptions) {
        ResourcesFlusher.a((Object) baseRequestOptions, "Argument must not be null");
        return (RequestBuilder) super.a(super);
    }

    public BaseRequestOptions clone() {
        RequestBuilder requestBuilder = (RequestBuilder) super.clone();
        requestBuilder.F = requestBuilder.F.clone();
        return super;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [Y, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.RequestBuilder.a(java.lang.Object, j.b.a.q.h.Target, j.b.a.q.RequestListener, j.b.a.q.c, j.b.a.TransitionOptions, j.b.a.f, int, int, j.b.a.q.BaseRequestOptions<?>, java.util.concurrent.Executor):j.b.a.q.b
     arg types: [java.lang.Object, Y, j.b.a.q.RequestListener<TranscodeType>, ?[OBJECT, ARRAY], j.b.a.TransitionOptions<?, ? super TranscodeType>, j.b.a.Priority, int, int, j.b.a.q.BaseRequestOptions<?>, java.util.concurrent.Executor]
     candidates:
      j.b.a.RequestBuilder.a(java.lang.Object, j.b.a.q.h.Target, j.b.a.q.RequestListener, j.b.a.q.BaseRequestOptions<?>, j.b.a.q.c, j.b.a.TransitionOptions, j.b.a.f, int, int, java.util.concurrent.Executor):j.b.a.q.b
      j.b.a.RequestBuilder.a(java.lang.Object, j.b.a.q.h.Target, j.b.a.q.RequestListener, j.b.a.q.c, j.b.a.TransitionOptions, j.b.a.f, int, int, j.b.a.q.BaseRequestOptions<?>, java.util.concurrent.Executor):j.b.a.q.b */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.q.Request, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public final <Y extends Target<TranscodeType>> Y a(Y y, RequestListener<TranscodeType> requestListener, BaseRequestOptions<?> superR, Executor executor) {
        Y y2 = y;
        BaseRequestOptions<?> baseRequestOptions = superR;
        ResourcesFlusher.a((Object) y2, "Argument must not be null");
        if (this.J) {
            Request a2 = a(new Object(), (Target) y, (RequestListener) requestListener, (c) null, (TransitionOptions) this.F, (f) super.f1744e, super.f1749l, super.f1748k, superR, executor);
            Request b = y.b();
            if (a2.a(b)) {
                if (!(!super.f1747j && b.c())) {
                    ResourcesFlusher.a((Object) b, "Argument must not be null");
                    if (!b.isRunning()) {
                        b.b();
                    }
                    return y2;
                }
            }
            this.C.a((Target<?>) y2);
            y2.a(a2);
            this.C.a(y2, a2);
            return y2;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    /* renamed from: clone  reason: collision with other method in class */
    public Object m10clone() {
        RequestBuilder requestBuilder = (RequestBuilder) super.clone();
        requestBuilder.F = requestBuilder.F.clone();
        return requestBuilder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
     arg types: [j.b.a.m.o.c.DownsampleStrategy, j.b.a.m.o.c.CenterInside]
     candidates:
      j.b.a.q.BaseRequestOptions.a(int, int):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
     arg types: [j.b.a.m.o.c.DownsampleStrategy, j.b.a.m.o.c.FitCenter]
     candidates:
      j.b.a.q.BaseRequestOptions.a(int, int):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
     arg types: [j.b.a.m.o.c.DownsampleStrategy, j.b.a.m.o.c.CenterCrop]
     candidates:
      j.b.a.q.BaseRequestOptions.a(int, int):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public j.b.a.q.h.ViewTarget<android.widget.ImageView, TranscodeType> a(android.widget.ImageView r6) {
        /*
            r5 = this;
            j.b.a.s.Util.a()
            java.lang.String r0 = "Argument must not be null"
            i.b.k.ResourcesFlusher.a(r6, r0)
            int r0 = r5.b
            r1 = 2048(0x800, float:2.87E-42)
            boolean r0 = j.b.a.q.BaseRequestOptions.b(r0, r1)
            r1 = 0
            if (r0 != 0) goto L_0x0080
            boolean r0 = r5.f1752o
            if (r0 == 0) goto L_0x0080
            android.widget.ImageView$ScaleType r0 = r6.getScaleType()
            if (r0 == 0) goto L_0x0080
            int[] r0 = j.b.a.RequestBuilder.a.a
            android.widget.ImageView$ScaleType r2 = r6.getScaleType()
            int r2 = r2.ordinal()
            r0 = r0[r2]
            r2 = 1
            switch(r0) {
                case 1: goto L_0x006d;
                case 2: goto L_0x0058;
                case 3: goto L_0x0043;
                case 4: goto L_0x0043;
                case 5: goto L_0x0043;
                case 6: goto L_0x002e;
                default: goto L_0x002d;
            }
        L_0x002d:
            goto L_0x0080
        L_0x002e:
            j.b.a.q.BaseRequestOptions r0 = r5.clone()
            if (r0 == 0) goto L_0x0042
            j.b.a.m.o.c.DownsampleStrategy r3 = j.b.a.m.o.c.DownsampleStrategy.b
            j.b.a.m.o.c.CenterInside r4 = new j.b.a.m.o.c.CenterInside
            r4.<init>()
            j.b.a.q.BaseRequestOptions r0 = r0.a(r3, r4)
            r0.z = r2
            goto L_0x0081
        L_0x0042:
            throw r1
        L_0x0043:
            j.b.a.q.BaseRequestOptions r0 = r5.clone()
            if (r0 == 0) goto L_0x0057
            j.b.a.m.o.c.DownsampleStrategy r3 = j.b.a.m.o.c.DownsampleStrategy.a
            j.b.a.m.o.c.FitCenter r4 = new j.b.a.m.o.c.FitCenter
            r4.<init>()
            j.b.a.q.BaseRequestOptions r0 = r0.a(r3, r4)
            r0.z = r2
            goto L_0x0081
        L_0x0057:
            throw r1
        L_0x0058:
            j.b.a.q.BaseRequestOptions r0 = r5.clone()
            if (r0 == 0) goto L_0x006c
            j.b.a.m.o.c.DownsampleStrategy r3 = j.b.a.m.o.c.DownsampleStrategy.b
            j.b.a.m.o.c.CenterInside r4 = new j.b.a.m.o.c.CenterInside
            r4.<init>()
            j.b.a.q.BaseRequestOptions r0 = r0.a(r3, r4)
            r0.z = r2
            goto L_0x0081
        L_0x006c:
            throw r1
        L_0x006d:
            j.b.a.q.BaseRequestOptions r0 = r5.clone()
            if (r0 == 0) goto L_0x007f
            j.b.a.m.o.c.DownsampleStrategy r2 = j.b.a.m.o.c.DownsampleStrategy.c
            j.b.a.m.o.c.CenterCrop r3 = new j.b.a.m.o.c.CenterCrop
            r3.<init>()
            j.b.a.q.BaseRequestOptions r0 = r0.a(r2, r3)
            goto L_0x0081
        L_0x007f:
            throw r1
        L_0x0080:
            r0 = r5
        L_0x0081:
            j.b.a.GlideContext r2 = r5.E
            java.lang.Class<TranscodeType> r3 = r5.D
            j.b.a.q.h.ImageViewTargetFactory r2 = r2.c
            if (r2 == 0) goto L_0x00c6
            java.lang.Class<android.graphics.Bitmap> r2 = android.graphics.Bitmap.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0097
            j.b.a.q.h.BitmapImageViewTarget r2 = new j.b.a.q.h.BitmapImageViewTarget
            r2.<init>(r6)
            goto L_0x00a4
        L_0x0097:
            java.lang.Class<android.graphics.drawable.Drawable> r2 = android.graphics.drawable.Drawable.class
            boolean r2 = r2.isAssignableFrom(r3)
            if (r2 == 0) goto L_0x00aa
            j.b.a.q.h.DrawableImageViewTarget r2 = new j.b.a.q.h.DrawableImageViewTarget
            r2.<init>(r6)
        L_0x00a4:
            java.util.concurrent.Executor r6 = j.b.a.s.Executors.a
            r5.a(r2, r1, r0, r6)
            return r2
        L_0x00aa:
            java.lang.IllegalArgumentException r6 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unhandled class: "
            r0.append(r1)
            r0.append(r3)
            java.lang.String r1 = ", try .as*(Class).transcode(ResourceTranscoder)"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r6.<init>(r0)
            throw r6
        L_0x00c6:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.RequestBuilder.a(android.widget.ImageView):j.b.a.q.h.ViewTarget");
    }

    public final b a(Object obj, Target<TranscodeType> target, RequestListener<TranscodeType> requestListener, c cVar, TransitionOptions<?, ? super TranscodeType> transitionOptions, f fVar, int i2, int i3, BaseRequestOptions<?> superR, Executor executor) {
        return a(obj, target, requestListener, superR, cVar, transitionOptions, fVar, i2, i3, executor);
    }

    public final b a(Object obj, Target<TranscodeType> target, RequestListener<TranscodeType> requestListener, BaseRequestOptions<?> superR, c cVar, TransitionOptions<?, ? super TranscodeType> transitionOptions, f fVar, int i2, int i3, Executor executor) {
        Context context = this.B;
        GlideContext glideContext = this.E;
        return new SingleRequest(context, glideContext, obj, this.G, this.D, superR, i2, i3, fVar, target, requestListener, this.H, cVar, glideContext.g, transitionOptions.b, executor);
    }
}
