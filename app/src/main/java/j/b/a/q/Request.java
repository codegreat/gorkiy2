package j.b.a.q;

public interface Request {
    void a();

    boolean a(Request request);

    void b();

    boolean c();

    void clear();

    boolean d();

    boolean isRunning();
}
