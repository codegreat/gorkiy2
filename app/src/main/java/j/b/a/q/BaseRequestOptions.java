package j.b.a.q;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import i.b.k.ResourcesFlusher;
import j.b.a.Priority;
import j.b.a.f;
import j.b.a.m.Key;
import j.b.a.m.Option;
import j.b.a.m.Options;
import j.b.a.m.Transformation;
import j.b.a.m.e;
import j.b.a.m.m.DiskCacheStrategy;
import j.b.a.m.m.k;
import j.b.a.m.o.c.CenterCrop;
import j.b.a.m.o.c.DownsampleStrategy;
import j.b.a.m.o.c.DrawableTransformation;
import j.b.a.m.o.g.GifDrawable;
import j.b.a.m.o.g.GifDrawableTransformation;
import j.b.a.q.BaseRequestOptions;
import j.b.a.r.EmptySignature;
import j.b.a.s.CachedHashCodeArrayMap;
import j.b.a.s.Util;
import java.util.Map;

public abstract class BaseRequestOptions<T extends BaseRequestOptions<T>> implements Cloneable {
    public boolean A;
    public int b;
    public float c = 1.0f;
    public DiskCacheStrategy d = DiskCacheStrategy.c;

    /* renamed from: e  reason: collision with root package name */
    public Priority f1744e = Priority.NORMAL;

    /* renamed from: f  reason: collision with root package name */
    public Drawable f1745f;
    public int g;
    public Drawable h;

    /* renamed from: i  reason: collision with root package name */
    public int f1746i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f1747j = true;

    /* renamed from: k  reason: collision with root package name */
    public int f1748k = -1;

    /* renamed from: l  reason: collision with root package name */
    public int f1749l = -1;

    /* renamed from: m  reason: collision with root package name */
    public Key f1750m = EmptySignature.b;

    /* renamed from: n  reason: collision with root package name */
    public boolean f1751n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f1752o = true;

    /* renamed from: p  reason: collision with root package name */
    public Drawable f1753p;

    /* renamed from: q  reason: collision with root package name */
    public int f1754q;

    /* renamed from: r  reason: collision with root package name */
    public Options f1755r = new Options();

    /* renamed from: s  reason: collision with root package name */
    public Map<Class<?>, Transformation<?>> f1756s = new CachedHashCodeArrayMap();

    /* renamed from: t  reason: collision with root package name */
    public Class<?> f1757t = Object.class;
    public boolean u;
    public Resources.Theme v;
    public boolean w;
    public boolean x;
    public boolean y;
    public boolean z = true;

    public static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.m.k, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public T a(k kVar) {
        if (this.w) {
            return clone().a(kVar);
        }
        ResourcesFlusher.a((Object) kVar, "Argument must not be null");
        this.d = kVar;
        this.b |= 4;
        c();
        return this;
    }

    public T b(boolean z2) {
        if (this.w) {
            return clone().b(z2);
        }
        this.A = z2;
        this.b |= 1048576;
        c();
        return this;
    }

    public final T c() {
        if (!this.u) {
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BaseRequestOptions)) {
            return false;
        }
        BaseRequestOptions baseRequestOptions = (BaseRequestOptions) obj;
        if (Float.compare(baseRequestOptions.c, this.c) == 0 && this.g == baseRequestOptions.g && Util.b(this.f1745f, baseRequestOptions.f1745f) && this.f1746i == baseRequestOptions.f1746i && Util.b(this.h, baseRequestOptions.h) && this.f1754q == baseRequestOptions.f1754q && Util.b(this.f1753p, baseRequestOptions.f1753p) && this.f1747j == baseRequestOptions.f1747j && this.f1748k == baseRequestOptions.f1748k && this.f1749l == baseRequestOptions.f1749l && this.f1751n == baseRequestOptions.f1751n && this.f1752o == baseRequestOptions.f1752o && this.x == baseRequestOptions.x && this.y == baseRequestOptions.y && this.d.equals(baseRequestOptions.d) && this.f1744e == baseRequestOptions.f1744e && this.f1755r.equals(baseRequestOptions.f1755r) && this.f1756s.equals(baseRequestOptions.f1756s) && this.f1757t.equals(baseRequestOptions.f1757t) && Util.b(this.f1750m, baseRequestOptions.f1750m) && Util.b(this.v, baseRequestOptions.v)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Util.a(this.v, Util.a(this.f1750m, Util.a(this.f1757t, Util.a(this.f1756s, Util.a(this.f1755r, Util.a(this.f1744e, Util.a(this.d, (((((((((((((Util.a(this.f1753p, (Util.a(this.h, (Util.a(this.f1745f, (Util.a(this.c) * 31) + this.g) * 31) + this.f1746i) * 31) + this.f1754q) * 31) + (this.f1747j ? 1 : 0)) * 31) + this.f1748k) * 31) + this.f1749l) * 31) + (this.f1751n ? 1 : 0)) * 31) + (this.f1752o ? 1 : 0)) * 31) + (this.x ? 1 : 0)) * 31) + (this.y ? 1 : 0))))))));
    }

    public T clone() {
        try {
            T t2 = (BaseRequestOptions) super.clone();
            Options options = new Options();
            t2.f1755r = options;
            options.a(this.f1755r);
            CachedHashCodeArrayMap cachedHashCodeArrayMap = new CachedHashCodeArrayMap();
            t2.f1756s = cachedHashCodeArrayMap;
            cachedHashCodeArrayMap.putAll(this.f1756s);
            t2.u = false;
            t2.w = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.b(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
     arg types: [j.b.a.m.o.c.DownsampleStrategy, j.b.a.m.o.c.CenterCrop]
     candidates:
      j.b.a.q.BaseRequestOptions.b(int, int):boolean
      j.b.a.q.BaseRequestOptions.b(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T */
    public T b() {
        return b((j.b.a.m.o.c.k) DownsampleStrategy.c, (Transformation<Bitmap>) new CenterCrop());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.f, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public T a(f fVar) {
        if (this.w) {
            return clone().a(fVar);
        }
        ResourcesFlusher.a((Object) fVar, "Argument must not be null");
        this.f1744e = fVar;
        this.b |= 8;
        c();
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.o.c.k, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T
     arg types: [j.b.a.m.Transformation<android.graphics.Bitmap>, int]
     candidates:
      j.b.a.q.BaseRequestOptions.a(int, int):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T */
    public final T b(j.b.a.m.o.c.k kVar, Transformation<Bitmap> transformation) {
        if (this.w) {
            return clone().b(kVar, transformation);
        }
        Option<j.b.a.m.o.c.k> option = DownsampleStrategy.f1704f;
        ResourcesFlusher.a((Object) kVar, "Argument must not be null");
        a(option, kVar);
        return a(transformation, true);
    }

    public T a(boolean z2) {
        if (this.w) {
            return clone().a(true);
        }
        this.f1747j = !z2;
        this.b |= 256;
        c();
        return this;
    }

    public T a(int i2, int i3) {
        if (this.w) {
            return clone().a(i2, i3);
        }
        this.f1749l = i2;
        this.f1748k = i3;
        this.b |= 512;
        c();
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.e, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public T a(e eVar) {
        if (this.w) {
            return clone().a(eVar);
        }
        ResourcesFlusher.a((Object) eVar, "Argument must not be null");
        this.f1750m = eVar;
        this.b |= 1024;
        c();
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [j.b.a.m.Option<Y>, java.lang.Object, j.b.a.m.Option] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [Y, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Y> T a(j.b.a.m.Option<Y> r2, Y r3) {
        /*
            r1 = this;
            boolean r0 = r1.w
            if (r0 == 0) goto L_0x000d
            j.b.a.q.BaseRequestOptions r0 = r1.clone()
            j.b.a.q.BaseRequestOptions r2 = r0.a(r2, r3)
            return r2
        L_0x000d:
            java.lang.String r0 = "Argument must not be null"
            i.b.k.ResourcesFlusher.a(r2, r0)
            i.b.k.ResourcesFlusher.a(r3, r0)
            j.b.a.m.Options r0 = r1.f1755r
            i.e.ArrayMap<j.b.a.m.Option<?>, java.lang.Object> r0 = r0.b
            r0.put(r2, r3)
            r1.c()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):j.b.a.q.BaseRequestOptions");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public T a(Class<?> cls) {
        if (this.w) {
            return clone().a(cls);
        }
        ResourcesFlusher.a((Object) cls, "Argument must not be null");
        this.f1757t = cls;
        this.b |= CodedOutputStream.DEFAULT_BUFFER_SIZE;
        c();
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.o.c.k, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T
     arg types: [j.b.a.m.Transformation<android.graphics.Bitmap>, int]
     candidates:
      j.b.a.q.BaseRequestOptions.a(int, int):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T */
    public final T a(j.b.a.m.o.c.k kVar, Transformation<Bitmap> transformation) {
        if (this.w) {
            return clone().a(kVar, transformation);
        }
        Option<j.b.a.m.o.c.k> option = DownsampleStrategy.f1704f;
        ResourcesFlusher.a((Object) kVar, "Argument must not be null");
        a(option, kVar);
        return a(transformation, false);
    }

    public T a(Transformation<Bitmap> transformation, boolean z2) {
        if (this.w) {
            return clone().a(transformation, z2);
        }
        DrawableTransformation drawableTransformation = new DrawableTransformation(transformation, z2);
        a(Bitmap.class, transformation, z2);
        a(Drawable.class, drawableTransformation, z2);
        a(BitmapDrawable.class, drawableTransformation, z2);
        a(GifDrawable.class, new GifDrawableTransformation(transformation), z2);
        c();
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARN: Type inference failed for: r3v0, types: [j.b.a.m.Transformation<Y>, j.b.a.m.Transformation, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Y> T a(java.lang.Class<Y> r2, j.b.a.m.Transformation<Y> r3, boolean r4) {
        /*
            r1 = this;
            boolean r0 = r1.w
            if (r0 == 0) goto L_0x000d
            j.b.a.q.BaseRequestOptions r0 = r1.clone()
            j.b.a.q.BaseRequestOptions r2 = r0.a(r2, r3, r4)
            return r2
        L_0x000d:
            java.lang.String r0 = "Argument must not be null"
            i.b.k.ResourcesFlusher.a(r2, r0)
            i.b.k.ResourcesFlusher.a(r3, r0)
            java.util.Map<java.lang.Class<?>, j.b.a.m.Transformation<?>> r0 = r1.f1756s
            r0.put(r2, r3)
            int r2 = r1.b
            r2 = r2 | 2048(0x800, float:2.87E-42)
            r1.b = r2
            r3 = 1
            r1.f1752o = r3
            r0 = 65536(0x10000, float:9.18355E-41)
            r2 = r2 | r0
            r1.b = r2
            r0 = 0
            r1.z = r0
            if (r4 == 0) goto L_0x0034
            r4 = 131072(0x20000, float:1.83671E-40)
            r2 = r2 | r4
            r1.b = r2
            r1.f1751n = r3
        L_0x0034:
            r1.c()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.q.BaseRequestOptions.a(java.lang.Class, j.b.a.m.Transformation, boolean):j.b.a.q.BaseRequestOptions");
    }

    public T a(BaseRequestOptions<?> baseRequestOptions) {
        if (this.w) {
            return clone().a(baseRequestOptions);
        }
        if (b(baseRequestOptions.b, 2)) {
            this.c = baseRequestOptions.c;
        }
        if (b(baseRequestOptions.b, 262144)) {
            this.x = baseRequestOptions.x;
        }
        if (b(baseRequestOptions.b, 1048576)) {
            this.A = baseRequestOptions.A;
        }
        if (b(baseRequestOptions.b, 4)) {
            this.d = baseRequestOptions.d;
        }
        if (b(baseRequestOptions.b, 8)) {
            this.f1744e = baseRequestOptions.f1744e;
        }
        if (b(baseRequestOptions.b, 16)) {
            this.f1745f = baseRequestOptions.f1745f;
            this.g = 0;
            this.b &= -33;
        }
        if (b(baseRequestOptions.b, 32)) {
            this.g = baseRequestOptions.g;
            this.f1745f = null;
            this.b &= -17;
        }
        if (b(baseRequestOptions.b, 64)) {
            this.h = baseRequestOptions.h;
            this.f1746i = 0;
            this.b &= -129;
        }
        if (b(baseRequestOptions.b, 128)) {
            this.f1746i = baseRequestOptions.f1746i;
            this.h = null;
            this.b &= -65;
        }
        if (b(baseRequestOptions.b, 256)) {
            this.f1747j = baseRequestOptions.f1747j;
        }
        if (b(baseRequestOptions.b, 512)) {
            this.f1749l = baseRequestOptions.f1749l;
            this.f1748k = baseRequestOptions.f1748k;
        }
        if (b(baseRequestOptions.b, 1024)) {
            this.f1750m = baseRequestOptions.f1750m;
        }
        if (b(baseRequestOptions.b, (int) CodedOutputStream.DEFAULT_BUFFER_SIZE)) {
            this.f1757t = baseRequestOptions.f1757t;
        }
        if (b(baseRequestOptions.b, 8192)) {
            this.f1753p = baseRequestOptions.f1753p;
            this.f1754q = 0;
            this.b &= -16385;
        }
        if (b(baseRequestOptions.b, 16384)) {
            this.f1754q = baseRequestOptions.f1754q;
            this.f1753p = null;
            this.b &= -8193;
        }
        if (b(baseRequestOptions.b, 32768)) {
            this.v = baseRequestOptions.v;
        }
        if (b(baseRequestOptions.b, (int) LogFileManager.MAX_LOG_SIZE)) {
            this.f1752o = baseRequestOptions.f1752o;
        }
        if (b(baseRequestOptions.b, 131072)) {
            this.f1751n = baseRequestOptions.f1751n;
        }
        if (b(baseRequestOptions.b, 2048)) {
            this.f1756s.putAll(baseRequestOptions.f1756s);
            this.z = baseRequestOptions.z;
        }
        if (b(baseRequestOptions.b, 524288)) {
            this.y = baseRequestOptions.y;
        }
        if (!this.f1752o) {
            this.f1756s.clear();
            int i2 = this.b & -2049;
            this.b = i2;
            this.f1751n = false;
            this.b = i2 & -131073;
            this.z = true;
        }
        this.b |= baseRequestOptions.b;
        this.f1755r.a(baseRequestOptions.f1755r);
        c();
        return this;
    }
}
