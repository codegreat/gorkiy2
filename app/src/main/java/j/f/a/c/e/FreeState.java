package j.f.a.c.e;

import j.a.a.a.outline;
import j.f.a.c.Next;
import j.f.a.c.State;
import n.n.c.Intrinsics;

/* compiled from: FreeState.kt */
public final class FreeState extends State {
    public final char b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FreeState(State state, char c) {
        super(super);
        if (state != null) {
            this.b = c;
            return;
        }
        Intrinsics.a("child");
        throw null;
    }

    public Next a(char c) {
        if (this.b == c) {
            return new Next(b(), Character.valueOf(c), true, null);
        }
        return new Next(b(), Character.valueOf(this.b), false, null);
    }

    public String toString() {
        String str;
        StringBuilder a = outline.a("");
        a.append(this.b);
        a.append(" -> ");
        State state = super.a;
        if (state == null) {
            str = "null";
        } else {
            str = super.toString();
        }
        a.append(str);
        return a.toString();
    }

    public Next a() {
        return new Next(b(), Character.valueOf(this.b), false, null);
    }
}
