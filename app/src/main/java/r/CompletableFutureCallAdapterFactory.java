package r;

import java.util.concurrent.CompletableFuture;
import r.CompletableFutureCallAdapterFactory2;

public class CompletableFutureCallAdapterFactory extends CompletableFuture<R> {
    public final /* synthetic */ Call b;

    public CompletableFutureCallAdapterFactory(CompletableFutureCallAdapterFactory2.a aVar, Call call) {
        this.b = call;
    }

    public boolean cancel(boolean z) {
        if (z) {
            this.b.cancel();
        }
        return super.cancel(z);
    }
}
