package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import o.Headers;
import o.HttpUrl;
import o.MediaType;

public final class RequestFactory {
    public final Method a;
    public final HttpUrl b;
    public final String c;
    @Nullable
    public final String d;
    @Nullable

    /* renamed from: e  reason: collision with root package name */
    public final Headers f3183e;
    @Nullable

    /* renamed from: f  reason: collision with root package name */
    public final MediaType f3184f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f3185i;

    /* renamed from: j  reason: collision with root package name */
    public final ParameterHandler<?>[] f3186j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f3187k;

    public RequestFactory(a aVar) {
        this.a = aVar.b;
        this.b = aVar.a.c;
        this.c = aVar.f3195n;
        this.d = aVar.f3199r;
        this.f3183e = aVar.f3200s;
        this.f3184f = aVar.f3201t;
        this.g = aVar.f3196o;
        this.h = aVar.f3197p;
        this.f3185i = aVar.f3198q;
        this.f3186j = aVar.v;
        this.f3187k = aVar.w;
    }

    public static final class a {
        public static final Pattern x = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
        public static final Pattern y = Pattern.compile("[a-zA-Z][a-zA-Z0-9_-]*");
        public final Retrofit0 a;
        public final Method b;
        public final Annotation[] c;
        public final Annotation[][] d;

        /* renamed from: e  reason: collision with root package name */
        public final Type[] f3188e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f3189f;
        public boolean g;
        public boolean h;

        /* renamed from: i  reason: collision with root package name */
        public boolean f3190i;

        /* renamed from: j  reason: collision with root package name */
        public boolean f3191j;

        /* renamed from: k  reason: collision with root package name */
        public boolean f3192k;

        /* renamed from: l  reason: collision with root package name */
        public boolean f3193l;

        /* renamed from: m  reason: collision with root package name */
        public boolean f3194m;
        @Nullable

        /* renamed from: n  reason: collision with root package name */
        public String f3195n;

        /* renamed from: o  reason: collision with root package name */
        public boolean f3196o;

        /* renamed from: p  reason: collision with root package name */
        public boolean f3197p;

        /* renamed from: q  reason: collision with root package name */
        public boolean f3198q;
        @Nullable

        /* renamed from: r  reason: collision with root package name */
        public String f3199r;
        @Nullable

        /* renamed from: s  reason: collision with root package name */
        public Headers f3200s;
        @Nullable

        /* renamed from: t  reason: collision with root package name */
        public MediaType f3201t;
        @Nullable
        public Set<String> u;
        @Nullable
        public ParameterHandler<?>[] v;
        public boolean w;

        public a(Retrofit0 retrofit0, Method method) {
            this.a = retrofit0;
            this.b = method;
            this.c = method.getAnnotations();
            this.f3188e = method.getGenericParameterTypes();
            this.d = method.getParameterAnnotations();
        }

        public final void a(String str, String str2, boolean z) {
            String str3 = this.f3195n;
            if (str3 == null) {
                this.f3195n = str;
                this.f3196o = z;
                if (!str2.isEmpty()) {
                    int indexOf = str2.indexOf(63);
                    if (indexOf != -1 && indexOf < str2.length() - 1) {
                        String substring = str2.substring(indexOf + 1);
                        if (x.matcher(substring).find()) {
                            throw Utils.a(this.b, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                        }
                    }
                    this.f3199r = str2;
                    Matcher matcher = x.matcher(str2);
                    LinkedHashSet linkedHashSet = new LinkedHashSet();
                    while (matcher.find()) {
                        linkedHashSet.add(matcher.group(1));
                    }
                    this.u = linkedHashSet;
                    return;
                }
                return;
            }
            throw Utils.a(this.b, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
        }

        public final void a(int i2, Type type) {
            if (Utils.c(type)) {
                throw Utils.a(this.b, i2, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }

        public static Class<?> a(Class<?> cls) {
            if (Boolean.TYPE == cls) {
                return Boolean.class;
            }
            if (Byte.TYPE == cls) {
                return Byte.class;
            }
            if (Character.TYPE == cls) {
                return Character.class;
            }
            if (Double.TYPE == cls) {
                return Double.class;
            }
            if (Float.TYPE == cls) {
                return Float.class;
            }
            if (Integer.TYPE == cls) {
                return Integer.class;
            }
            if (Long.TYPE == cls) {
                return Long.class;
            }
            return Short.TYPE == cls ? Short.class : cls;
        }
    }
}
