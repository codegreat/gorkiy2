package r;

import javax.annotation.Nullable;
import n.i.Collections;
import n.l.Continuation;
import o.Call;
import o.f;
import o.i0;

public abstract class HttpServiceMethod<ResponseT, ReturnT> extends ServiceMethod<ReturnT> {
    public final RequestFactory a;
    public final Call.a b;
    public final Converter<i0, ResponseT> c;

    public static final class a<ResponseT, ReturnT> extends HttpServiceMethod<ResponseT, ReturnT> {
        public final CallAdapter<ResponseT, ReturnT> d;

        public a(b0 b0Var, f.a aVar, Converter<i0, ResponseT> converter, CallAdapter<ResponseT, ReturnT> callAdapter) {
            super(b0Var, aVar, converter);
            this.d = callAdapter;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [r.Call, r.Call<ResponseT>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ReturnT a(r.Call<ResponseT> r1, java.lang.Object[] r2) {
            /*
                r0 = this;
                r.CallAdapter<ResponseT, ReturnT> r2 = r0.d
                java.lang.Object r1 = r2.a(r1)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: r.HttpServiceMethod.a.a(r.Call, java.lang.Object[]):java.lang.Object");
        }
    }

    public static final class b<ResponseT> extends HttpServiceMethod<ResponseT, Object> {
        public final CallAdapter<ResponseT, Call<ResponseT>> d;

        /* renamed from: e  reason: collision with root package name */
        public final boolean f3166e;

        public b(b0 b0Var, f.a aVar, Converter<i0, ResponseT> converter, CallAdapter<ResponseT, Call<ResponseT>> callAdapter, boolean z) {
            super(b0Var, aVar, converter);
            this.d = callAdapter;
            this.f3166e = z;
        }

        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            Continuation continuation = (Continuation) objArr[objArr.length - 1];
            try {
                if (this.f3166e) {
                    return Collections.b(call2, continuation);
                }
                return Collections.a(call2, continuation);
            } catch (Exception e2) {
                return Collections.a(e2, (Continuation<?>) continuation);
            }
        }
    }

    public static final class c<ResponseT> extends HttpServiceMethod<ResponseT, Object> {
        public final CallAdapter<ResponseT, Call<ResponseT>> d;

        public c(b0 b0Var, f.a aVar, Converter<i0, ResponseT> converter, CallAdapter<ResponseT, Call<ResponseT>> callAdapter) {
            super(b0Var, aVar, converter);
            this.d = callAdapter;
        }

        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            Continuation continuation = (Continuation) objArr[objArr.length - 1];
            try {
                return Collections.c(call2, continuation);
            } catch (Exception e2) {
                return Collections.a(e2, (Continuation<?>) continuation);
            }
        }
    }

    public HttpServiceMethod(b0 b0Var, f.a aVar, Converter<i0, ResponseT> converter) {
        this.a = b0Var;
        this.b = aVar;
        this.c = converter;
    }

    @Nullable
    public abstract ReturnT a(Call call, Object[] objArr);
}
