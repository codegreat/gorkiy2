package r;

import javax.annotation.Nullable;

/* compiled from: ParameterHandler */
public class ParameterHandler0 extends ParameterHandler<Iterable<T>> {
    public final /* synthetic */ ParameterHandler a;

    public ParameterHandler0(ParameterHandler parameterHandler) {
        this.a = super;
    }

    public void a(RequestBuilder requestBuilder, @Nullable Object obj) {
        Iterable<Object> iterable = (Iterable) obj;
        if (iterable != null) {
            for (Object a2 : iterable) {
                this.a.a(requestBuilder, a2);
            }
        }
    }
}
