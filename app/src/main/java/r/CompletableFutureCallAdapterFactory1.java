package r;

import java.util.concurrent.CompletableFuture;
import r.CompletableFutureCallAdapterFactory2;
import retrofit2.HttpException;

/* compiled from: CompletableFutureCallAdapterFactory */
public class CompletableFutureCallAdapterFactory1 implements Callback<R> {
    public final /* synthetic */ CompletableFuture b;

    public CompletableFutureCallAdapterFactory1(CompletableFutureCallAdapterFactory2.a aVar, CompletableFuture completableFuture) {
        this.b = completableFuture;
    }

    public void a(Call<R> call, Response<R> response) {
        if (response.a()) {
            this.b.complete(response.b);
        } else {
            this.b.completeExceptionally(new HttpException(response));
        }
    }

    public void a(Call<R> call, Throwable th) {
        this.b.completeExceptionally(th);
    }
}
