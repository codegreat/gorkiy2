package r;

import j.a.a.a.outline;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import n.i.Collections;
import o.Call;
import o.Callback;
import o.FormBody;
import o.HttpUrl;
import o.MediaType;
import o.MultipartBody;
import o.Request;
import o.RequestBody;
import o.Response;
import o.ResponseBody;
import o.f;
import o.h0;
import o.i0;
import o.m0.Util;
import p.Buffer;
import p.BufferedSource;
import p.ForwardingSource;
import p.Source;
import r.RequestBuilder;

public final class OkHttpCall<T> implements Call<T> {
    public final RequestFactory b;
    public final Object[] c;
    public final Call.a d;

    /* renamed from: e  reason: collision with root package name */
    public final Converter<i0, T> f3169e;

    /* renamed from: f  reason: collision with root package name */
    public volatile boolean f3170f;
    @GuardedBy("this")
    @Nullable
    public Call g;
    @GuardedBy("this")
    @Nullable
    public Throwable h;
    @GuardedBy("this")

    /* renamed from: i  reason: collision with root package name */
    public boolean f3171i;

    public class a implements Callback {
        public final /* synthetic */ Callback a;

        public a(Callback callback) {
            this.a = callback;
        }

        public void a(Call call, Response response) {
            try {
                try {
                    this.a.a(OkHttpCall.this, OkHttpCall.this.a((h0) response));
                } catch (Throwable th) {
                    Utils.a(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                Utils.a(th2);
                th2.printStackTrace();
            }
        }
    }

    public static final class b extends ResponseBody {
        public final ResponseBody d;

        /* renamed from: e  reason: collision with root package name */
        public final BufferedSource f3172e;
        @Nullable

        /* renamed from: f  reason: collision with root package name */
        public IOException f3173f;

        public class a extends ForwardingSource {
            public a(Source source) {
                super(source);
            }

            public long b(Buffer buffer, long j2) {
                try {
                    return super.b(buffer, j2);
                } catch (IOException e2) {
                    b.this.f3173f = e2;
                    throw e2;
                }
            }
        }

        public b(ResponseBody responseBody) {
            this.d = super;
            this.f3172e = Collections.a((Source) new a(super.g()));
        }

        public long a() {
            return this.d.a();
        }

        public void close() {
            this.d.close();
        }

        public MediaType f() {
            return this.d.f();
        }

        public BufferedSource g() {
            return this.f3172e;
        }
    }

    public static final class c extends ResponseBody {
        @Nullable
        public final MediaType d;

        /* renamed from: e  reason: collision with root package name */
        public final long f3174e;

        public c(@Nullable MediaType mediaType, long j2) {
            this.d = mediaType;
            this.f3174e = j2;
        }

        public long a() {
            return this.f3174e;
        }

        public MediaType f() {
            return this.d;
        }

        public BufferedSource g() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    public OkHttpCall(b0 b0Var, Object[] objArr, f.a aVar, Converter<i0, T> converter) {
        this.b = b0Var;
        this.c = objArr;
        this.d = aVar;
        this.f3169e = converter;
    }

    public Response<T> a(h0 h0Var) {
        ResponseBody responseBody = h0Var.f2902i;
        Response.a aVar = new Response.a(h0Var);
        aVar.g = new c(responseBody.f(), responseBody.a());
        Response a2 = aVar.a();
        int i2 = a2.f2901f;
        if (i2 < 200 || i2 >= 300) {
            try {
                ResponseBody a3 = Utils.a(responseBody);
                Utils.a(a3, "body == null");
                Utils.a(a2, "rawResponse == null");
                if (!a2.f()) {
                    return new Response<>(a2, null, a3);
                }
                throw new IllegalArgumentException("rawResponse should not be successful response");
            } finally {
                responseBody.close();
            }
        } else if (i2 == 204 || i2 == 205) {
            responseBody.close();
            return Response.a(null, a2);
        } else {
            b bVar = new b(responseBody);
            try {
                return Response.a(this.f3169e.a(bVar), a2);
            } catch (RuntimeException e2) {
                IOException iOException = bVar.f3173f;
                if (iOException == null) {
                    throw e2;
                }
                throw iOException;
            }
        }
    }

    public final Call b() {
        HttpUrl httpUrl;
        Call.a aVar = this.d;
        RequestFactory requestFactory = this.b;
        Object[] objArr = this.c;
        ParameterHandler<?>[] parameterHandlerArr = requestFactory.f3186j;
        int length = objArr.length;
        if (length == parameterHandlerArr.length) {
            RequestBuilder requestBuilder = new RequestBuilder(requestFactory.c, requestFactory.b, requestFactory.d, requestFactory.f3183e, requestFactory.f3184f, requestFactory.g, requestFactory.h, requestFactory.f3185i);
            if (requestFactory.f3187k) {
                length--;
            }
            ArrayList arrayList = new ArrayList(length);
            for (int i2 = 0; i2 < length; i2++) {
                arrayList.add(objArr[i2]);
                parameterHandlerArr[i2].a(requestBuilder, objArr[i2]);
            }
            HttpUrl.a aVar2 = requestBuilder.d;
            if (aVar2 != null) {
                httpUrl = aVar2.a();
            } else {
                httpUrl = requestBuilder.b.b(requestBuilder.c);
                if (httpUrl == null) {
                    StringBuilder a2 = outline.a("Malformed URL. Base: ");
                    a2.append(requestBuilder.b);
                    a2.append(", Relative: ");
                    a2.append(requestBuilder.c);
                    throw new IllegalArgumentException(a2.toString());
                }
            }
            RequestBuilder.a aVar3 = requestBuilder.f3182k;
            if (aVar3 == null) {
                FormBody.a aVar4 = requestBuilder.f3181j;
                if (aVar4 != null) {
                    aVar3 = new FormBody(aVar4.a, aVar4.b);
                } else {
                    MultipartBody.a aVar5 = requestBuilder.f3180i;
                    if (aVar5 != null) {
                        if (!aVar5.c.isEmpty()) {
                            aVar3 = new MultipartBody(aVar5.a, aVar5.b, Util.b(aVar5.c));
                        } else {
                            throw new IllegalStateException("Multipart body must have at least one part.".toString());
                        }
                    } else if (requestBuilder.h) {
                        aVar3 = RequestBody.a.a(new byte[0], null, 0, 0);
                    }
                }
            }
            MediaType mediaType = requestBuilder.g;
            if (mediaType != null) {
                if (aVar3 != null) {
                    aVar3 = new RequestBuilder.a(aVar3, mediaType);
                } else {
                    requestBuilder.f3179f.a("Content-Type", mediaType.a);
                }
            }
            Request.a aVar6 = requestBuilder.f3178e;
            aVar6.a = httpUrl;
            aVar6.a(requestBuilder.f3179f.a());
            aVar6.a(requestBuilder.a, aVar3);
            aVar6.a(Invocation.class, new Invocation(requestFactory.a, arrayList));
            Call a3 = aVar.a(aVar6.a());
            if (a3 != null) {
                return a3;
            }
            throw new NullPointerException("Call.Factory returned null.");
        }
        StringBuilder a4 = outline.a("Argument count (", length, ") doesn't match expected count (");
        a4.append(parameterHandlerArr.length);
        a4.append(")");
        throw new IllegalArgumentException(a4.toString());
    }

    public void cancel() {
        Call call;
        this.f3170f = true;
        synchronized (this) {
            call = this.g;
        }
        if (call != null) {
            call.cancel();
        }
    }

    public Object clone() {
        return new OkHttpCall(this.b, this.c, this.d, this.f3169e);
    }

    public synchronized Request f() {
        Call call = this.g;
        if (call != null) {
            return call.f();
        } else if (this.h == null) {
            try {
                Call b2 = b();
                this.g = b2;
                return b2.f();
            } catch (RuntimeException e2) {
                e = e2;
                Utils.a(e);
                this.h = e;
                throw e;
            } catch (Error e3) {
                e = e3;
                Utils.a(e);
                this.h = e;
                throw e;
            } catch (IOException e4) {
                this.h = e4;
                throw new RuntimeException("Unable to create request.", e4);
            }
        } else if (this.h instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.h);
        } else if (this.h instanceof RuntimeException) {
            throw ((RuntimeException) this.h);
        } else {
            throw ((Error) this.h);
        }
    }

    public Response<T> g() {
        Call call;
        synchronized (this) {
            if (!this.f3171i) {
                this.f3171i = true;
                if (this.h == null) {
                    call = this.g;
                    if (call == null) {
                        try {
                            call = b();
                            this.g = call;
                        } catch (IOException | Error | RuntimeException e2) {
                            Utils.a(e2);
                            this.h = e2;
                            throw e2;
                        }
                    }
                } else if (this.h instanceof IOException) {
                    throw ((IOException) this.h);
                } else if (this.h instanceof RuntimeException) {
                    throw ((RuntimeException) this.h);
                } else {
                    throw ((Error) this.h);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.f3170f) {
            call.cancel();
        }
        return a((h0) call.g());
    }

    public boolean h() {
        boolean z = true;
        if (this.f3170f) {
            return true;
        }
        synchronized (this) {
            if (this.g == null || !this.g.h()) {
                z = false;
            }
        }
        return z;
    }

    /* renamed from: clone  reason: collision with other method in class */
    public Call m32clone() {
        return new OkHttpCall(this.b, this.c, this.d, this.f3169e);
    }

    public void a(Callback<T> callback) {
        Call call;
        Throwable th;
        Utils.a(callback, "callback == null");
        synchronized (this) {
            if (!this.f3171i) {
                this.f3171i = true;
                call = this.g;
                th = this.h;
                if (call == null && th == null) {
                    try {
                        Call b2 = b();
                        this.g = b2;
                        call = b2;
                    } catch (Throwable th2) {
                        th = th2;
                        Utils.a(th);
                        this.h = th;
                    }
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            callback.a(this, th);
            return;
        }
        if (this.f3170f) {
            call.cancel();
        }
        call.a(new a(callback));
    }
}
