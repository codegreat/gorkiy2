package r;

public abstract class ServiceMethod<T> {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r10v13, types: [r.ParameterHandler$b] */
    /* JADX WARN: Type inference failed for: r10v22 */
    /* JADX WARN: Type inference failed for: r10v27, types: [r.ParameterHandler$h] */
    /* JADX WARN: Type inference failed for: r19v13 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x07cb  */
    /* JADX WARNING: Removed duplicated region for block: B:476:0x07cf A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> r.ServiceMethod<T> a(r.e0 r23, java.lang.reflect.Method r24) {
        /*
            r0 = r23
            r1 = r24
            r.RequestFactory$a r2 = new r.RequestFactory$a
            r2.<init>(r0, r1)
            java.lang.annotation.Annotation[] r3 = r2.c
            int r4 = r3.length
            r5 = 0
            r6 = 0
        L_0x000e:
            java.lang.String r7 = "HEAD"
            r8 = 1
            if (r6 >= r4) goto L_0x0149
            r9 = r3[r6]
            boolean r10 = r9 instanceof r.l0.DELETE
            if (r10 == 0) goto L_0x0026
            r.l0.DELETE r9 = (r.l0.DELETE) r9
            java.lang.String r7 = r9.value()
            java.lang.String r8 = "DELETE"
            r2.a(r8, r7, r5)
            goto L_0x0145
        L_0x0026:
            boolean r10 = r9 instanceof r.l0.GET
            if (r10 == 0) goto L_0x0037
            r.l0.GET r9 = (r.l0.GET) r9
            java.lang.String r7 = r9.value()
            java.lang.String r8 = "GET"
            r2.a(r8, r7, r5)
            goto L_0x0145
        L_0x0037:
            boolean r10 = r9 instanceof r.l0.HEAD
            if (r10 == 0) goto L_0x0046
            r.l0.HEAD r9 = (r.l0.HEAD) r9
            java.lang.String r8 = r9.value()
            r2.a(r7, r8, r5)
            goto L_0x0145
        L_0x0046:
            boolean r7 = r9 instanceof r.l0.PATCH
            if (r7 == 0) goto L_0x0057
            r.l0.PATCH r9 = (r.l0.PATCH) r9
            java.lang.String r7 = r9.value()
            java.lang.String r9 = "PATCH"
            r2.a(r9, r7, r8)
            goto L_0x0145
        L_0x0057:
            boolean r7 = r9 instanceof r.l0.POST
            if (r7 == 0) goto L_0x0068
            r.l0.POST r9 = (r.l0.POST) r9
            java.lang.String r7 = r9.value()
            java.lang.String r9 = "POST"
            r2.a(r9, r7, r8)
            goto L_0x0145
        L_0x0068:
            boolean r7 = r9 instanceof r.l0.PUT
            if (r7 == 0) goto L_0x0079
            r.l0.PUT r9 = (r.l0.PUT) r9
            java.lang.String r7 = r9.value()
            java.lang.String r9 = "PUT"
            r2.a(r9, r7, r8)
            goto L_0x0145
        L_0x0079:
            boolean r7 = r9 instanceof r.l0.OPTIONS
            if (r7 == 0) goto L_0x008a
            r.l0.OPTIONS r9 = (r.l0.OPTIONS) r9
            java.lang.String r7 = r9.value()
            java.lang.String r8 = "OPTIONS"
            r2.a(r8, r7, r5)
            goto L_0x0145
        L_0x008a:
            boolean r7 = r9 instanceof r.l0.HTTP
            if (r7 == 0) goto L_0x00a1
            r.l0.HTTP r9 = (r.l0.HTTP) r9
            java.lang.String r7 = r9.method()
            java.lang.String r8 = r9.path()
            boolean r9 = r9.hasBody()
            r2.a(r7, r8, r9)
            goto L_0x0145
        L_0x00a1:
            boolean r7 = r9 instanceof r.l0.Headers
            if (r7 == 0) goto L_0x011b
            r.l0.Headers r9 = (r.l0.Headers) r9
            java.lang.String[] r7 = r9.value()
            int r9 = r7.length
            if (r9 == 0) goto L_0x0110
            o.Headers$a r9 = new o.Headers$a
            r9.<init>()
            int r10 = r7.length
            r11 = 0
        L_0x00b5:
            if (r11 >= r10) goto L_0x0109
            r12 = r7[r11]
            r13 = 58
            int r13 = r12.indexOf(r13)
            r14 = -1
            if (r13 == r14) goto L_0x00fc
            if (r13 == 0) goto L_0x00fc
            int r14 = r12.length()
            int r14 = r14 - r8
            if (r13 == r14) goto L_0x00fc
            java.lang.String r14 = r12.substring(r5, r13)
            int r13 = r13 + 1
            java.lang.String r12 = r12.substring(r13)
            java.lang.String r12 = r12.trim()
            java.lang.String r13 = "Content-Type"
            boolean r13 = r13.equalsIgnoreCase(r14)
            if (r13 == 0) goto L_0x00f6
            o.MediaType r13 = o.MediaType.a(r12)     // Catch:{ IllegalArgumentException -> 0x00e8 }
            r2.f3201t = r13     // Catch:{ IllegalArgumentException -> 0x00e8 }
            goto L_0x00f9
        L_0x00e8:
            r0 = move-exception
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r2 = new java.lang.Object[r8]
            r2[r5] = r12
            java.lang.String r3 = "Malformed content type: %s"
            java.lang.RuntimeException r0 = r.Utils.a(r1, r0, r3, r2)
            throw r0
        L_0x00f6:
            r9.a(r14, r12)
        L_0x00f9:
            int r11 = r11 + 1
            goto L_0x00b5
        L_0x00fc:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r8]
            r1[r5] = r12
            java.lang.String r2 = "@Headers value must be in the form \"Name: Value\". Found: \"%s\""
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x0109:
            o.Headers r7 = r9.a()
            r2.f3200s = r7
            goto L_0x0145
        L_0x0110:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r2 = "@Headers annotation is empty."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x011b:
            boolean r7 = r9 instanceof r.l0.Multipart
            java.lang.String r10 = "Only one encoding annotation is allowed."
            if (r7 == 0) goto L_0x0131
            boolean r7 = r2.f3197p
            if (r7 != 0) goto L_0x0128
            r2.f3198q = r8
            goto L_0x0145
        L_0x0128:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r10, r1)
            throw r0
        L_0x0131:
            boolean r7 = r9 instanceof r.l0.FormUrlEncoded
            if (r7 == 0) goto L_0x0145
            boolean r7 = r2.f3198q
            if (r7 != 0) goto L_0x013c
            r2.f3197p = r8
            goto L_0x0145
        L_0x013c:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r10, r1)
            throw r0
        L_0x0145:
            int r6 = r6 + 1
            goto L_0x000e
        L_0x0149:
            java.lang.String r3 = r2.f3195n
            if (r3 == 0) goto L_0x099e
            boolean r3 = r2.f3196o
            if (r3 != 0) goto L_0x0170
            boolean r3 = r2.f3198q
            if (r3 != 0) goto L_0x0165
            boolean r3 = r2.f3197p
            if (r3 != 0) goto L_0x015a
            goto L_0x0170
        L_0x015a:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r2 = "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST)."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x0165:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r2 = "Multipart can only be specified on HTTP methods with request body (e.g., @POST)."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x0170:
            java.lang.annotation.Annotation[][] r3 = r2.d
            int r3 = r3.length
            r.ParameterHandler[] r4 = new r.ParameterHandler[r3]
            r2.v = r4
            int r4 = r3 + -1
            r5 = 0
        L_0x017a:
            if (r5 >= r3) goto L_0x081c
            r.ParameterHandler<?>[] r6 = r2.v
            java.lang.reflect.Type[] r8 = r2.f3188e
            r14 = r8[r5]
            java.lang.annotation.Annotation[][] r8 = r2.d
            r15 = r8[r5]
            if (r5 != r4) goto L_0x018c
            r8 = 1
            r16 = 1
            goto L_0x018f
        L_0x018c:
            r8 = 0
            r16 = 0
        L_0x018f:
            if (r15 == 0) goto L_0x07ec
            int r13 = r15.length
            r8 = 0
            r9 = 0
            r17 = r9
            r12 = 0
        L_0x0197:
            if (r12 >= r13) goto L_0x07e7
            r8 = r15[r12]
            java.lang.Class<java.lang.String> r9 = java.lang.String.class
            java.lang.Class<o.MultipartBody$c> r10 = o.MultipartBody.c.class
            boolean r11 = r8 instanceof r.l0.Path
            r18 = r3
            r3 = 2
            if (r11 == 0) goto L_0x0262
            r2.a(r5, r14)
            boolean r9 = r2.f3191j
            if (r9 != 0) goto L_0x0256
            boolean r9 = r2.f3192k
            if (r9 != 0) goto L_0x024a
            boolean r9 = r2.f3193l
            if (r9 != 0) goto L_0x023e
            boolean r9 = r2.f3194m
            if (r9 != 0) goto L_0x0232
            java.lang.String r9 = r2.f3199r
            if (r9 == 0) goto L_0x0221
            r9 = 1
            r2.f3190i = r9
            r.l0.Path r8 = (r.l0.Path) r8
            java.lang.String r11 = r8.value()
            java.util.regex.Pattern r9 = r.RequestFactory.a.y
            java.util.regex.Matcher r9 = r9.matcher(r11)
            boolean r9 = r9.matches()
            if (r9 == 0) goto L_0x020a
            java.util.Set<java.lang.String> r9 = r2.u
            boolean r9 = r9.contains(r11)
            if (r9 == 0) goto L_0x01f7
            r.Retrofit0 r3 = r2.a
            r.Converter r3 = r3.c(r14, r15)
            r.ParameterHandler$g r19 = new r.ParameterHandler$g
            java.lang.reflect.Method r9 = r2.b
            boolean r20 = r8.encoded()
            r8 = r19
            r10 = r5
            r21 = r12
            r12 = r3
            r22 = r13
            r13 = r20
            r8.<init>(r9, r10, r11, r12, r13)
            goto L_0x05f3
        L_0x01f7:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r3]
            java.lang.String r2 = r2.f3199r
            r3 = 0
            r1[r3] = r2
            r2 = 1
            r1[r2] = r11
            java.lang.String r2 = "URL \"%s\" does not contain \"{%s}\"."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r2, r1)
            throw r0
        L_0x020a:
            r0 = 0
            r1 = 1
            java.lang.reflect.Method r2 = r2.b
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.util.regex.Pattern r4 = r.RequestFactory.a.x
            java.lang.String r4 = r4.pattern()
            r3[r0] = r4
            r3[r1] = r11
            java.lang.String r0 = "@Path parameter name must match %s. Found: %s"
            java.lang.RuntimeException r0 = r.Utils.a(r2, r5, r0, r3)
            throw r0
        L_0x0221:
            r0 = 0
            r1 = 1
            java.lang.reflect.Method r3 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = r2.f3195n
            r1[r0] = r2
            java.lang.String r0 = "@Path can only be used with relative url on @%s"
            java.lang.RuntimeException r0 = r.Utils.a(r3, r5, r0, r1)
            throw r0
        L_0x0232:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@Path parameters may not be used with @Url."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x023e:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "A @Path parameter must not come after a @QueryMap."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x024a:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "A @Path parameter must not come after a @QueryName."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x0256:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "A @Path parameter must not come after a @Query."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x0262:
            r21 = r12
            r22 = r13
            boolean r3 = r8 instanceof r.l0.Query
            java.lang.String r11 = "<String>)"
            java.lang.String r12 = " must include generic type (e.g., "
            if (r3 == 0) goto L_0x02ea
            r2.a(r5, r14)
            r.l0.Query r8 = (r.l0.Query) r8
            java.lang.String r3 = r8.value()
            boolean r8 = r8.encoded()
            java.lang.Class r9 = r.Utils.b(r14)
            r10 = 1
            r2.f3191j = r10
            java.lang.Class<java.lang.Iterable> r10 = java.lang.Iterable.class
            boolean r10 = r10.isAssignableFrom(r9)
            if (r10 == 0) goto L_0x02bb
            boolean r10 = r14 instanceof java.lang.reflect.ParameterizedType
            if (r10 == 0) goto L_0x02a8
            r9 = r14
            java.lang.reflect.ParameterizedType r9 = (java.lang.reflect.ParameterizedType) r9
            r10 = 0
            java.lang.reflect.Type r9 = r.Utils.a(r10, r9)
            r.Retrofit0 r10 = r2.a
            r.Converter r9 = r10.c(r9, r15)
            r.ParameterHandler$h r10 = new r.ParameterHandler$h
            r10.<init>(r3, r9, r8)
            r.ParameterHandler0 r3 = new r.ParameterHandler0
            r3.<init>(r10)
            goto L_0x04c2
        L_0x02a8:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r1 = j.a.a.a.outline.a(r9, r1, r12, r11)
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x02bb:
            boolean r10 = r9.isArray()
            if (r10 == 0) goto L_0x02db
            java.lang.Class r9 = r9.getComponentType()
            java.lang.Class r9 = r.RequestFactory.a.a(r9)
            r.Retrofit0 r10 = r2.a
            r.Converter r9 = r10.c(r9, r15)
            r.ParameterHandler$h r10 = new r.ParameterHandler$h
            r10.<init>(r3, r9, r8)
            r.ParameterHandler1 r3 = new r.ParameterHandler1
            r3.<init>(r10)
            goto L_0x04c2
        L_0x02db:
            r.Retrofit0 r9 = r2.a
            r.Converter r9 = r9.c(r14, r15)
            r.ParameterHandler$h r10 = new r.ParameterHandler$h
            r10.<init>(r3, r9, r8)
        L_0x02e6:
            r19 = r10
            goto L_0x05f3
        L_0x02ea:
            boolean r3 = r8 instanceof r.l0.QueryName
            if (r3 == 0) goto L_0x0364
            r2.a(r5, r14)
            r.l0.QueryName r8 = (r.l0.QueryName) r8
            boolean r3 = r8.encoded()
            java.lang.Class r8 = r.Utils.b(r14)
            r9 = 1
            r2.f3192k = r9
            java.lang.Class<java.lang.Iterable> r9 = java.lang.Iterable.class
            boolean r9 = r9.isAssignableFrom(r8)
            if (r9 == 0) goto L_0x0337
            boolean r9 = r14 instanceof java.lang.reflect.ParameterizedType
            if (r9 == 0) goto L_0x0324
            r8 = r14
            java.lang.reflect.ParameterizedType r8 = (java.lang.reflect.ParameterizedType) r8
            r9 = 0
            java.lang.reflect.Type r8 = r.Utils.a(r9, r8)
            r.Retrofit0 r9 = r2.a
            r.Converter r8 = r9.c(r8, r15)
            r.ParameterHandler$j r9 = new r.ParameterHandler$j
            r9.<init>(r8, r3)
            r.ParameterHandler0 r3 = new r.ParameterHandler0
            r3.<init>(r9)
            goto L_0x04c2
        L_0x0324:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r1 = j.a.a.a.outline.a(r8, r1, r12, r11)
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x0337:
            boolean r9 = r8.isArray()
            if (r9 == 0) goto L_0x0357
            java.lang.Class r8 = r8.getComponentType()
            java.lang.Class r8 = r.RequestFactory.a.a(r8)
            r.Retrofit0 r9 = r2.a
            r.Converter r8 = r9.c(r8, r15)
            r.ParameterHandler$j r9 = new r.ParameterHandler$j
            r9.<init>(r8, r3)
            r.ParameterHandler1 r3 = new r.ParameterHandler1
            r3.<init>(r9)
            goto L_0x04c2
        L_0x0357:
            r.Retrofit0 r8 = r2.a
            r.Converter r8 = r8.c(r14, r15)
            r.ParameterHandler$j r9 = new r.ParameterHandler$j
            r9.<init>(r8, r3)
            goto L_0x05dc
        L_0x0364:
            boolean r3 = r8 instanceof r.l0.QueryMap
            java.lang.String r13 = "Map must include generic types (e.g., Map<String, String>)"
            if (r3 == 0) goto L_0x03d9
            r2.a(r5, r14)
            java.lang.Class r3 = r.Utils.b(r14)
            r10 = 1
            r2.f3193l = r10
            java.lang.Class<java.util.Map> r11 = java.util.Map.class
            boolean r11 = r11.isAssignableFrom(r3)
            if (r11 == 0) goto L_0x03cd
            java.lang.Class<java.util.Map> r11 = java.util.Map.class
            java.lang.reflect.Type r3 = r.Utils.b(r14, r3, r11)
            boolean r11 = r3 instanceof java.lang.reflect.ParameterizedType
            if (r11 == 0) goto L_0x03c3
            java.lang.reflect.ParameterizedType r3 = (java.lang.reflect.ParameterizedType) r3
            r11 = 0
            java.lang.reflect.Type r11 = r.Utils.a(r11, r3)
            if (r9 != r11) goto L_0x03a8
            java.lang.reflect.Type r3 = r.Utils.a(r10, r3)
            r.Retrofit0 r9 = r2.a
            r.Converter r3 = r9.c(r3, r15)
            r.ParameterHandler$i r9 = new r.ParameterHandler$i
            java.lang.reflect.Method r10 = r2.b
            r.l0.QueryMap r8 = (r.l0.QueryMap) r8
            boolean r8 = r8.encoded()
            r9.<init>(r10, r5, r3, r8)
            goto L_0x05dc
        L_0x03a8:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "@QueryMap keys must be of type String: "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r1 = r1.toString()
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x03c3:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r13, r0)
            throw r0
        L_0x03cd:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@QueryMap parameter type must be Map."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x03d9:
            boolean r3 = r8 instanceof r.l0.Header
            if (r3 == 0) goto L_0x0450
            r2.a(r5, r14)
            r.l0.Header r8 = (r.l0.Header) r8
            java.lang.String r3 = r8.value()
            java.lang.Class r8 = r.Utils.b(r14)
            java.lang.Class<java.lang.Iterable> r9 = java.lang.Iterable.class
            boolean r9 = r9.isAssignableFrom(r8)
            if (r9 == 0) goto L_0x0423
            boolean r9 = r14 instanceof java.lang.reflect.ParameterizedType
            if (r9 == 0) goto L_0x0410
            r8 = r14
            java.lang.reflect.ParameterizedType r8 = (java.lang.reflect.ParameterizedType) r8
            r9 = 0
            java.lang.reflect.Type r8 = r.Utils.a(r9, r8)
            r.Retrofit0 r9 = r2.a
            r.Converter r8 = r9.c(r8, r15)
            r.ParameterHandler$d r9 = new r.ParameterHandler$d
            r9.<init>(r3, r8)
            r.ParameterHandler0 r3 = new r.ParameterHandler0
            r3.<init>(r9)
            goto L_0x04c2
        L_0x0410:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r1 = j.a.a.a.outline.a(r8, r1, r12, r11)
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x0423:
            boolean r9 = r8.isArray()
            if (r9 == 0) goto L_0x0443
            java.lang.Class r8 = r8.getComponentType()
            java.lang.Class r8 = r.RequestFactory.a.a(r8)
            r.Retrofit0 r9 = r2.a
            r.Converter r8 = r9.c(r8, r15)
            r.ParameterHandler$d r9 = new r.ParameterHandler$d
            r9.<init>(r3, r8)
            r.ParameterHandler1 r3 = new r.ParameterHandler1
            r3.<init>(r9)
            goto L_0x04c2
        L_0x0443:
            r.Retrofit0 r8 = r2.a
            r.Converter r8 = r8.c(r14, r15)
            r.ParameterHandler$d r9 = new r.ParameterHandler$d
            r9.<init>(r3, r8)
            goto L_0x05dc
        L_0x0450:
            boolean r3 = r8 instanceof r.l0.Field
            if (r3 == 0) goto L_0x04df
            r2.a(r5, r14)
            boolean r3 = r2.f3197p
            if (r3 == 0) goto L_0x04d3
            r.l0.Field r8 = (r.l0.Field) r8
            java.lang.String r3 = r8.value()
            boolean r8 = r8.encoded()
            r9 = 1
            r2.f3189f = r9
            java.lang.Class r9 = r.Utils.b(r14)
            java.lang.Class<java.lang.Iterable> r10 = java.lang.Iterable.class
            boolean r10 = r10.isAssignableFrom(r9)
            if (r10 == 0) goto L_0x04a4
            boolean r10 = r14 instanceof java.lang.reflect.ParameterizedType
            if (r10 == 0) goto L_0x0491
            r9 = r14
            java.lang.reflect.ParameterizedType r9 = (java.lang.reflect.ParameterizedType) r9
            r10 = 0
            java.lang.reflect.Type r9 = r.Utils.a(r10, r9)
            r.Retrofit0 r10 = r2.a
            r.Converter r9 = r10.c(r9, r15)
            r.ParameterHandler$b r10 = new r.ParameterHandler$b
            r10.<init>(r3, r9, r8)
            r.ParameterHandler0 r3 = new r.ParameterHandler0
            r3.<init>(r10)
            goto L_0x04c2
        L_0x0491:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r1 = j.a.a.a.outline.a(r9, r1, r12, r11)
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x04a4:
            boolean r10 = r9.isArray()
            if (r10 == 0) goto L_0x04c6
            java.lang.Class r9 = r9.getComponentType()
            java.lang.Class r9 = r.RequestFactory.a.a(r9)
            r.Retrofit0 r10 = r2.a
            r.Converter r9 = r10.c(r9, r15)
            r.ParameterHandler$b r10 = new r.ParameterHandler$b
            r10.<init>(r3, r9, r8)
            r.ParameterHandler1 r3 = new r.ParameterHandler1
            r3.<init>(r10)
        L_0x04c2:
            r19 = r4
            goto L_0x079d
        L_0x04c6:
            r.Retrofit0 r9 = r2.a
            r.Converter r9 = r9.c(r14, r15)
            r.ParameterHandler$b r10 = new r.ParameterHandler$b
            r10.<init>(r3, r9, r8)
            goto L_0x02e6
        L_0x04d3:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "@Field parameters can only be used with form encoding."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r2, r1)
            throw r0
        L_0x04df:
            boolean r3 = r8 instanceof r.l0.FieldMap
            if (r3 == 0) goto L_0x0562
            r2.a(r5, r14)
            boolean r3 = r2.f3197p
            if (r3 == 0) goto L_0x0556
            java.lang.Class r3 = r.Utils.b(r14)
            java.lang.Class<java.util.Map> r10 = java.util.Map.class
            boolean r10 = r10.isAssignableFrom(r3)
            if (r10 == 0) goto L_0x054a
            java.lang.Class<java.util.Map> r10 = java.util.Map.class
            java.lang.reflect.Type r3 = r.Utils.b(r14, r3, r10)
            boolean r10 = r3 instanceof java.lang.reflect.ParameterizedType
            if (r10 == 0) goto L_0x0540
            java.lang.reflect.ParameterizedType r3 = (java.lang.reflect.ParameterizedType) r3
            r10 = 0
            java.lang.reflect.Type r10 = r.Utils.a(r10, r3)
            if (r9 != r10) goto L_0x0525
            r9 = 1
            java.lang.reflect.Type r3 = r.Utils.a(r9, r3)
            r.Retrofit0 r10 = r2.a
            r.Converter r3 = r10.c(r3, r15)
            r2.f3189f = r9
            r.ParameterHandler$c r9 = new r.ParameterHandler$c
            java.lang.reflect.Method r10 = r2.b
            r.l0.FieldMap r8 = (r.l0.FieldMap) r8
            boolean r8 = r8.encoded()
            r9.<init>(r10, r5, r3, r8)
            goto L_0x05dc
        L_0x0525:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "@FieldMap keys must be of type String: "
            r1.append(r2)
            r1.append(r10)
            java.lang.String r1 = r1.toString()
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x0540:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r13, r0)
            throw r0
        L_0x054a:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@FieldMap parameter type must be Map."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x0556:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@FieldMap parameters can only be used with form encoding."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x0562:
            boolean r3 = r8 instanceof r.l0.Part
            if (r3 == 0) goto L_0x06db
            r2.a(r5, r14)
            boolean r3 = r2.f3198q
            if (r3 == 0) goto L_0x06cf
            r.l0.Part r8 = (r.l0.Part) r8
            r3 = 1
            r2.g = r3
            java.lang.String r3 = r8.value()
            java.lang.Class r9 = r.Utils.b(r14)
            boolean r13 = r3.isEmpty()
            if (r13 == 0) goto L_0x05ff
            java.lang.Class<java.lang.Iterable> r3 = java.lang.Iterable.class
            boolean r3 = r3.isAssignableFrom(r9)
            java.lang.String r8 = "@Part annotation must supply a name or use MultipartBody.Part parameter type."
            if (r3 == 0) goto L_0x05c4
            boolean r3 = r14 instanceof java.lang.reflect.ParameterizedType
            if (r3 == 0) goto L_0x05b1
            r3 = r14
            java.lang.reflect.ParameterizedType r3 = (java.lang.reflect.ParameterizedType) r3
            r9 = 0
            java.lang.reflect.Type r3 = r.Utils.a(r9, r3)
            java.lang.Class r3 = r.Utils.b(r3)
            boolean r3 = r10.isAssignableFrom(r3)
            if (r3 == 0) goto L_0x05a8
            r.ParameterHandler$k r3 = r.ParameterHandler.k.a
            r.ParameterHandler0 r9 = new r.ParameterHandler0
            r9.<init>(r3)
            goto L_0x05dc
        L_0x05a8:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r9]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r8, r1)
            throw r0
        L_0x05b1:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r1 = j.a.a.a.outline.a(r9, r1, r12, r11)
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x05c4:
            boolean r3 = r9.isArray()
            if (r3 == 0) goto L_0x05ea
            java.lang.Class r3 = r9.getComponentType()
            boolean r3 = r10.isAssignableFrom(r3)
            if (r3 == 0) goto L_0x05e0
            r.ParameterHandler$k r3 = r.ParameterHandler.k.a
            r.ParameterHandler1 r8 = new r.ParameterHandler1
            r8.<init>(r3)
            r9 = r8
        L_0x05dc:
            r19 = r4
            goto L_0x07c8
        L_0x05e0:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r8, r1)
            throw r0
        L_0x05ea:
            r3 = 0
            boolean r9 = r10.isAssignableFrom(r9)
            if (r9 == 0) goto L_0x05f6
            r.ParameterHandler$k r19 = r.ParameterHandler.k.a
        L_0x05f3:
            r9 = r19
            goto L_0x05dc
        L_0x05f6:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r3]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r8, r1)
            throw r0
        L_0x05ff:
            r13 = 4
            java.lang.String[] r13 = new java.lang.String[r13]
            java.lang.String r20 = "Content-Disposition"
            r19 = 0
            r13[r19] = r20
            r19 = r4
            java.lang.String r4 = "form-data; name=\""
            java.lang.String r1 = "\""
            java.lang.String r1 = j.a.a.a.outline.a(r4, r3, r1)
            r3 = 1
            r13[r3] = r1
            java.lang.String r1 = "Content-Transfer-Encoding"
            r3 = 2
            r13[r3] = r1
            r1 = 3
            java.lang.String r3 = r8.encoding()
            r13[r1] = r3
            o.Headers$b r1 = o.Headers.c
            o.Headers r1 = r1.a(r13)
            java.lang.Class<java.lang.Iterable> r3 = java.lang.Iterable.class
            boolean r3 = r3.isAssignableFrom(r9)
            java.lang.String r4 = "@Part parameters using the MultipartBody.Part must not include a part name in the annotation."
            if (r3 == 0) goto L_0x067a
            boolean r3 = r14 instanceof java.lang.reflect.ParameterizedType
            if (r3 == 0) goto L_0x0667
            r3 = r14
            java.lang.reflect.ParameterizedType r3 = (java.lang.reflect.ParameterizedType) r3
            r8 = 0
            java.lang.reflect.Type r3 = r.Utils.a(r8, r3)
            java.lang.Class r8 = r.Utils.b(r3)
            boolean r8 = r10.isAssignableFrom(r8)
            if (r8 != 0) goto L_0x065d
            r.Retrofit0 r4 = r2.a
            java.lang.annotation.Annotation[] r8 = r2.c
            r.Converter r3 = r4.a(r3, r15, r8)
            r.ParameterHandler$e r4 = new r.ParameterHandler$e
            java.lang.reflect.Method r8 = r2.b
            r4.<init>(r8, r5, r1, r3)
            r.ParameterHandler0 r3 = new r.ParameterHandler0
            r3.<init>(r4)
            goto L_0x079d
        L_0x065d:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r4, r1)
            throw r0
        L_0x0667:
            java.lang.reflect.Method r0 = r2.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r1 = j.a.a.a.outline.a(r9, r1, r12, r11)
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r1, r2)
            throw r0
        L_0x067a:
            boolean r3 = r9.isArray()
            if (r3 == 0) goto L_0x06ae
            java.lang.Class r3 = r9.getComponentType()
            java.lang.Class r3 = r.RequestFactory.a.a(r3)
            boolean r8 = r10.isAssignableFrom(r3)
            if (r8 != 0) goto L_0x06a4
            r.Retrofit0 r4 = r2.a
            java.lang.annotation.Annotation[] r8 = r2.c
            r.Converter r3 = r4.a(r3, r15, r8)
            r.ParameterHandler$e r4 = new r.ParameterHandler$e
            java.lang.reflect.Method r8 = r2.b
            r4.<init>(r8, r5, r1, r3)
            r.ParameterHandler1 r3 = new r.ParameterHandler1
            r3.<init>(r4)
            goto L_0x079d
        L_0x06a4:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r4, r1)
            throw r0
        L_0x06ae:
            boolean r3 = r10.isAssignableFrom(r9)
            if (r3 != 0) goto L_0x06c5
            r.Retrofit0 r3 = r2.a
            java.lang.annotation.Annotation[] r4 = r2.c
            r.Converter r3 = r3.a(r14, r15, r4)
            r.ParameterHandler$e r9 = new r.ParameterHandler$e
            java.lang.reflect.Method r4 = r2.b
            r9.<init>(r4, r5, r1, r3)
            goto L_0x07c8
        L_0x06c5:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r4, r1)
            throw r0
        L_0x06cf:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@Part parameters can only be used with multipart encoding."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x06db:
            r19 = r4
            boolean r1 = r8 instanceof r.l0.PartMap
            if (r1 == 0) goto L_0x0778
            r2.a(r5, r14)
            boolean r1 = r2.f3198q
            if (r1 == 0) goto L_0x076c
            r1 = 1
            r2.g = r1
            java.lang.Class r1 = r.Utils.b(r14)
            java.lang.Class<java.util.Map> r3 = java.util.Map.class
            boolean r3 = r3.isAssignableFrom(r1)
            if (r3 == 0) goto L_0x0760
            java.lang.Class<java.util.Map> r3 = java.util.Map.class
            java.lang.reflect.Type r1 = r.Utils.b(r14, r1, r3)
            boolean r3 = r1 instanceof java.lang.reflect.ParameterizedType
            if (r3 == 0) goto L_0x0756
            java.lang.reflect.ParameterizedType r1 = (java.lang.reflect.ParameterizedType) r1
            r3 = 0
            java.lang.reflect.Type r3 = r.Utils.a(r3, r1)
            if (r9 != r3) goto L_0x073b
            r3 = 1
            java.lang.reflect.Type r1 = r.Utils.a(r3, r1)
            java.lang.Class r3 = r.Utils.b(r1)
            boolean r3 = r10.isAssignableFrom(r3)
            if (r3 != 0) goto L_0x072f
            r.Retrofit0 r3 = r2.a
            java.lang.annotation.Annotation[] r4 = r2.c
            r.Converter r1 = r3.a(r1, r15, r4)
            r.l0.PartMap r8 = (r.l0.PartMap) r8
            r.ParameterHandler$f r3 = new r.ParameterHandler$f
            java.lang.reflect.Method r4 = r2.b
            java.lang.String r8 = r8.encoding()
            r3.<init>(r4, r5, r1, r8)
            goto L_0x079d
        L_0x072f:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r2, r1)
            throw r0
        L_0x073b:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "@PartMap keys must be of type String: "
            r2.append(r4)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x0756:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r13, r0)
            throw r0
        L_0x0760:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@PartMap parameter type must be Map."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x076c:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@PartMap parameters can only be used with multipart encoding."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x0778:
            boolean r1 = r8 instanceof r.l0.Body
            if (r1 == 0) goto L_0x07c7
            r2.a(r5, r14)
            boolean r1 = r2.f3197p
            if (r1 != 0) goto L_0x07bb
            boolean r1 = r2.f3198q
            if (r1 != 0) goto L_0x07bb
            boolean r1 = r2.h
            if (r1 != 0) goto L_0x07af
            r.Retrofit0 r1 = r2.a     // Catch:{ RuntimeException -> 0x079f }
            java.lang.annotation.Annotation[] r3 = r2.c     // Catch:{ RuntimeException -> 0x079f }
            r.Converter r1 = r1.a(r14, r15, r3)     // Catch:{ RuntimeException -> 0x079f }
            r3 = 1
            r2.h = r3
            r.ParameterHandler$a r3 = new r.ParameterHandler$a
            java.lang.reflect.Method r4 = r2.b
            r3.<init>(r4, r5, r1)
        L_0x079d:
            r9 = r3
            goto L_0x07c8
        L_0x079f:
            r0 = move-exception
            java.lang.reflect.Method r1 = r2.b
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r2[r3] = r14
            java.lang.String r3 = "Unable to create @Body converter for %s"
            java.lang.RuntimeException r0 = r.Utils.a(r1, r0, r5, r3, r2)
            throw r0
        L_0x07af:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "Multiple @Body method annotations found."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x07bb:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "@Body parameters cannot be used with form or multi-part encoding."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r5, r2, r0)
            throw r0
        L_0x07c7:
            r9 = 0
        L_0x07c8:
            if (r9 != 0) goto L_0x07cb
            goto L_0x07cf
        L_0x07cb:
            if (r17 != 0) goto L_0x07db
            r17 = r9
        L_0x07cf:
            int r12 = r21 + 1
            r1 = r24
            r3 = r18
            r4 = r19
            r13 = r22
            goto L_0x0197
        L_0x07db:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "Multiple Retrofit annotations found, only one allowed."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r2, r1)
            throw r0
        L_0x07e7:
            r18 = r3
            r19 = r4
            goto L_0x07f2
        L_0x07ec:
            r18 = r3
            r19 = r4
            r17 = 0
        L_0x07f2:
            if (r17 != 0) goto L_0x0810
            if (r16 == 0) goto L_0x0804
            java.lang.Class r1 = r.Utils.b(r14)     // Catch:{ NoClassDefFoundError -> 0x0804 }
            java.lang.Class<n.l.Continuation> r3 = n.l.Continuation.class
            if (r1 != r3) goto L_0x0804
            r1 = 1
            r2.w = r1     // Catch:{ NoClassDefFoundError -> 0x0804 }
            r17 = 0
            goto L_0x0810
        L_0x0804:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "No Retrofit annotation found."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r5, r2, r1)
            throw r0
        L_0x0810:
            r6[r5] = r17
            int r5 = r5 + 1
            r1 = r24
            r3 = r18
            r4 = r19
            goto L_0x017a
        L_0x081c:
            java.lang.String r1 = r2.f3199r
            if (r1 != 0) goto L_0x0836
            boolean r1 = r2.f3194m
            if (r1 == 0) goto L_0x0825
            goto L_0x0836
        L_0x0825:
            java.lang.reflect.Method r0 = r2.b
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = r2.f3195n
            r3 = 0
            r1[r3] = r2
            java.lang.String r2 = "Missing either @%s URL or @Url parameter."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x0836:
            boolean r1 = r2.f3197p
            if (r1 != 0) goto L_0x0853
            boolean r1 = r2.f3198q
            if (r1 != 0) goto L_0x0853
            boolean r1 = r2.f3196o
            if (r1 != 0) goto L_0x0853
            boolean r1 = r2.h
            if (r1 != 0) goto L_0x0847
            goto L_0x0853
        L_0x0847:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "Non-body HTTP method cannot contain @Body."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x0853:
            boolean r1 = r2.f3197p
            if (r1 == 0) goto L_0x0868
            boolean r1 = r2.f3189f
            if (r1 == 0) goto L_0x085c
            goto L_0x0868
        L_0x085c:
            java.lang.reflect.Method r0 = r2.b
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "Form-encoded method must contain at least one @Field."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x0868:
            r1 = 0
            boolean r3 = r2.f3198q
            if (r3 == 0) goto L_0x087d
            boolean r3 = r2.g
            if (r3 == 0) goto L_0x0872
            goto L_0x087d
        L_0x0872:
            java.lang.reflect.Method r0 = r2.b
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "Multipart method must contain at least one @Part."
            java.lang.RuntimeException r0 = r.Utils.a(r0, r2, r1)
            throw r0
        L_0x087d:
            r.RequestFactory r3 = new r.RequestFactory
            r3.<init>(r2)
            java.lang.reflect.Type r1 = r24.getGenericReturnType()
            boolean r2 = r.Utils.c(r1)
            if (r2 != 0) goto L_0x098f
            java.lang.Class r2 = java.lang.Void.TYPE
            if (r1 == r2) goto L_0x0983
            java.lang.Class<r.Response> r1 = r.Response.class
            boolean r2 = r3.f3187k
            java.lang.annotation.Annotation[] r4 = r24.getAnnotations()
            if (r2 == 0) goto L_0x08dd
            java.lang.reflect.Type[] r5 = r24.getGenericParameterTypes()
            int r6 = r5.length
            int r6 = r6 + -1
            r5 = r5[r6]
            java.lang.reflect.ParameterizedType r5 = (java.lang.reflect.ParameterizedType) r5
            java.lang.reflect.Type[] r5 = r5.getActualTypeArguments()
            r6 = 0
            r5 = r5[r6]
            boolean r8 = r5 instanceof java.lang.reflect.WildcardType
            if (r8 == 0) goto L_0x08b8
            java.lang.reflect.WildcardType r5 = (java.lang.reflect.WildcardType) r5
            java.lang.reflect.Type[] r5 = r5.getLowerBounds()
            r5 = r5[r6]
        L_0x08b8:
            java.lang.Class r8 = r.Utils.b(r5)
            if (r8 != r1) goto L_0x08ca
            boolean r8 = r5 instanceof java.lang.reflect.ParameterizedType
            if (r8 == 0) goto L_0x08ca
            java.lang.reflect.ParameterizedType r5 = (java.lang.reflect.ParameterizedType) r5
            java.lang.reflect.Type r5 = r.Utils.a(r6, r5)
            r8 = 1
            goto L_0x08cb
        L_0x08ca:
            r8 = 0
        L_0x08cb:
            r.Utils$b r9 = new r.Utils$b
            java.lang.Class<r.Call> r10 = r.Call.class
            r11 = 1
            java.lang.reflect.Type[] r11 = new java.lang.reflect.Type[r11]
            r11[r6] = r5
            r5 = 0
            r9.<init>(r5, r10, r11)
            java.lang.annotation.Annotation[] r4 = r.SkipCallbackExecutorImpl.a(r4)
            goto L_0x08e2
        L_0x08dd:
            java.lang.reflect.Type r9 = r24.getGenericReturnType()
            r8 = 0
        L_0x08e2:
            r.CallAdapter r5 = r0.a(r9, r4)     // Catch:{ RuntimeException -> 0x0972 }
            java.lang.reflect.Type r4 = r5.a()
            java.lang.Class<o.Response> r6 = o.Response.class
            if (r4 == r6) goto L_0x094e
            if (r4 == r1) goto L_0x0942
            java.lang.String r1 = r3.c
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x090d
            java.lang.Class<java.lang.Void> r1 = java.lang.Void.class
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x0901
            goto L_0x090d
        L_0x0901:
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "HEAD method must use Void as response type."
            r6 = r24
            java.lang.RuntimeException r0 = r.Utils.a(r6, r1, r0)
            throw r0
        L_0x090d:
            r6 = r24
            java.lang.annotation.Annotation[] r1 = r24.getAnnotations()
            r.Converter r4 = r0.b(r4, r1)     // Catch:{ RuntimeException -> 0x0933 }
            o.Call$a r0 = r0.b
            if (r2 != 0) goto L_0x0921
            r.HttpServiceMethod$a r1 = new r.HttpServiceMethod$a
            r1.<init>(r3, r0, r4, r5)
            goto L_0x0932
        L_0x0921:
            if (r8 == 0) goto L_0x0929
            r.HttpServiceMethod$c r1 = new r.HttpServiceMethod$c
            r1.<init>(r3, r0, r4, r5)
            goto L_0x0932
        L_0x0929:
            r.HttpServiceMethod$b r7 = new r.HttpServiceMethod$b
            r6 = 0
            r1 = r7
            r2 = r3
            r3 = r0
            r1.<init>(r2, r3, r4, r5, r6)
        L_0x0932:
            return r1
        L_0x0933:
            r0 = move-exception
            r1 = r0
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r2 = 0
            r0[r2] = r4
            java.lang.String r2 = "Unable to create converter for %s"
            java.lang.RuntimeException r0 = r.Utils.a(r6, r1, r2, r0)
            throw r0
        L_0x0942:
            r6 = r24
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "Response must include generic type (e.g., Response<String>)"
            java.lang.RuntimeException r0 = r.Utils.a(r6, r1, r0)
            throw r0
        L_0x094e:
            r6 = r24
            java.lang.String r0 = "'"
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            java.lang.Class r1 = r.Utils.b(r4)
            java.lang.String r1 = r1.getName()
            r0.append(r1)
            java.lang.String r1 = "' is not a valid response body type. Did you mean ResponseBody?"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.RuntimeException r0 = r.Utils.a(r6, r0, r1)
            throw r0
        L_0x0972:
            r0 = move-exception
            r6 = r24
            r1 = r0
            r0 = 0
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r0] = r9
            java.lang.String r0 = "Unable to create call adapter for %s"
            java.lang.RuntimeException r0 = r.Utils.a(r6, r1, r0, r2)
            throw r0
        L_0x0983:
            r6 = r24
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "Service methods cannot return void."
            java.lang.RuntimeException r0 = r.Utils.a(r6, r1, r0)
            throw r0
        L_0x098f:
            r6 = r24
            r0 = 0
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r0] = r1
            java.lang.String r0 = "Method return type must not include a type variable or wildcard: %s"
            java.lang.RuntimeException r0 = r.Utils.a(r6, r0, r2)
            throw r0
        L_0x099e:
            r0 = 0
            java.lang.reflect.Method r1 = r2.b
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = "HTTP method annotation is required (e.g., @GET, @POST, etc.)."
            java.lang.RuntimeException r0 = r.Utils.a(r1, r2, r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: r.ServiceMethod.a(r.Retrofit0, java.lang.reflect.Method):r.ServiceMethod");
    }
}
