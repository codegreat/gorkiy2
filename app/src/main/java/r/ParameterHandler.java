package r;

import j.a.a.a.outline;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;
import javax.annotation.Nullable;
import o.Headers;
import o.MultipartBody;
import o.a0;
import o.g0;
import o.w;
import p.Buffer;

public abstract class ParameterHandler<T> {

    public static final class a<T> extends ParameterHandler<T> {
        public final Method a;
        public final int b;
        public final Converter<T, g0> c;

        public a(Method method, int i2, Converter<T, g0> converter) {
            this.a = method;
            this.b = i2;
            this.c = converter;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            if (t2 != null) {
                try {
                    a0Var.f3182k = this.c.a(t2);
                } catch (IOException e2) {
                    Method method = this.a;
                    int i2 = this.b;
                    throw Utils.a(method, e2, i2, "Unable to convert " + ((Object) t2) + " to RequestBody", new Object[0]);
                }
            } else {
                throw Utils.a(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    public static final class b<T> extends ParameterHandler<T> {
        public final String a;
        public final Converter<T, String> b;
        public final boolean c;

        public b(String str, Converter<T, String> converter, boolean z) {
            Utils.a(str, "name == null");
            this.a = str;
            this.b = converter;
            this.c = z;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            String a2;
            if (t2 != null && (a2 = this.b.a(t2)) != null) {
                a0Var.a(this.a, a2, this.c);
            }
        }
    }

    public static final class c<T> extends ParameterHandler<Map<String, T>> {
        public final Method a;
        public final int b;
        public final Converter<T, String> c;
        public final boolean d;

        public c(Method method, int i2, Converter<T, String> converter, boolean z) {
            this.a = method;
            this.b = i2;
            this.c = converter;
            this.d = z;
        }

        public void a(RequestBuilder requestBuilder, @Nullable Object obj) {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                requestBuilder.a(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i2 = this.b;
                                throw Utils.a(method, i2, "Field map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            throw Utils.a(this.a, this.b, outline.a("Field map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw Utils.a(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw Utils.a(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    public static final class d<T> extends ParameterHandler<T> {
        public final String a;
        public final Converter<T, String> b;

        public d(String str, Converter<T, String> converter) {
            Utils.a(str, "name == null");
            this.a = str;
            this.b = converter;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            String a2;
            if (t2 != null && (a2 = this.b.a(t2)) != null) {
                a0Var.a(this.a, a2);
            }
        }
    }

    public static final class e<T> extends ParameterHandler<T> {
        public final Method a;
        public final int b;
        public final Headers c;
        public final Converter<T, g0> d;

        public e(Method method, int i2, w wVar, Converter<T, g0> converter) {
            this.a = method;
            this.b = i2;
            this.c = wVar;
            this.d = converter;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            if (t2 != null) {
                try {
                    a0Var.a(this.c, this.d.a(t2));
                } catch (IOException e2) {
                    Method method = this.a;
                    int i2 = this.b;
                    throw Utils.a(method, i2, "Unable to convert " + ((Object) t2) + " to RequestBody", e2);
                }
            }
        }
    }

    public static final class f<T> extends ParameterHandler<Map<String, T>> {
        public final Method a;
        public final int b;
        public final Converter<T, g0> c;
        public final String d;

        public f(Method method, int i2, Converter<T, g0> converter, String str) {
            this.a = method;
            this.b = i2;
            this.c = converter;
            this.d = str;
        }

        public void a(RequestBuilder requestBuilder, @Nullable Object obj) {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            requestBuilder.a(Headers.c.a("Content-Disposition", outline.a("form-data; name=\"", str, "\""), "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            throw Utils.a(this.a, this.b, outline.a("Part map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw Utils.a(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw Utils.a(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    public static final class g<T> extends ParameterHandler<T> {
        public final Method a;
        public final int b;
        public final String c;
        public final Converter<T, String> d;

        /* renamed from: e  reason: collision with root package name */
        public final boolean f3175e;

        public g(Method method, int i2, String str, Converter<T, String> converter, boolean z) {
            this.a = method;
            this.b = i2;
            Utils.a(str, "name == null");
            this.c = str;
            this.d = converter;
            this.f3175e = z;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            String str;
            int i2;
            a0 a0Var2 = a0Var;
            T t3 = t2;
            if (t3 != null) {
                String str2 = this.c;
                String a2 = this.d.a(t3);
                boolean z = this.f3175e;
                if (a0Var2.c != null) {
                    int length = a2.length();
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            str = a2;
                            break;
                        }
                        int codePointAt = a2.codePointAt(i3);
                        i2 = 47;
                        if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                            Buffer buffer = new Buffer();
                            buffer.a(a2, 0, i3);
                            Buffer buffer2 = null;
                        } else {
                            i3 += Character.charCount(codePointAt);
                        }
                    }
                    Buffer buffer3 = new Buffer();
                    buffer3.a(a2, 0, i3);
                    Buffer buffer22 = null;
                    while (i3 < length) {
                        int codePointAt2 = a2.codePointAt(i3);
                        if (!z || !(codePointAt2 == 9 || codePointAt2 == 10 || codePointAt2 == 12 || codePointAt2 == 13)) {
                            if (codePointAt2 < 32 || codePointAt2 >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt2) != -1 || (!z && (codePointAt2 == i2 || codePointAt2 == 37))) {
                                if (buffer22 == null) {
                                    buffer22 = new Buffer();
                                }
                                buffer22.b(codePointAt2);
                                while (!buffer22.j()) {
                                    byte readByte = buffer22.readByte() & 255;
                                    buffer3.writeByte(37);
                                    buffer3.writeByte((int) RequestBuilder.f3176l[(readByte >> 4) & 15]);
                                    buffer3.writeByte((int) RequestBuilder.f3176l[readByte & 15]);
                                }
                            } else {
                                buffer3.b(codePointAt2);
                            }
                        }
                        i3 += Character.charCount(codePointAt2);
                        i2 = 47;
                    }
                    str = buffer3.o();
                    String str3 = a0Var2.c;
                    String replace = str3.replace("{" + str2 + "}", str);
                    if (!RequestBuilder.f3177m.matcher(replace).matches()) {
                        a0Var2.c = replace;
                        return;
                    }
                    throw new IllegalArgumentException(outline.a("@Path parameters shouldn't perform path traversal ('.' or '..'): ", a2));
                }
                throw new AssertionError();
            }
            throw Utils.a(this.a, this.b, outline.a(outline.a("Path parameter \""), this.c, "\" value must not be null."), new Object[0]);
        }
    }

    public static final class h<T> extends ParameterHandler<T> {
        public final String a;
        public final Converter<T, String> b;
        public final boolean c;

        public h(String str, Converter<T, String> converter, boolean z) {
            Utils.a(str, "name == null");
            this.a = str;
            this.b = converter;
            this.c = z;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            String a2;
            if (t2 != null && (a2 = this.b.a(t2)) != null) {
                a0Var.b(this.a, a2, this.c);
            }
        }
    }

    public static final class i<T> extends ParameterHandler<Map<String, T>> {
        public final Method a;
        public final int b;
        public final Converter<T, String> c;
        public final boolean d;

        public i(Method method, int i2, Converter<T, String> converter, boolean z) {
            this.a = method;
            this.b = i2;
            this.c = converter;
            this.d = z;
        }

        public void a(RequestBuilder requestBuilder, @Nullable Object obj) {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                requestBuilder.b(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i2 = this.b;
                                throw Utils.a(method, i2, "Query map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            throw Utils.a(this.a, this.b, outline.a("Query map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw Utils.a(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw Utils.a(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    public static final class j<T> extends ParameterHandler<T> {
        public final Converter<T, String> a;
        public final boolean b;

        public j(Converter<T, String> converter, boolean z) {
            this.a = converter;
            this.b = z;
        }

        public void a(a0 a0Var, @Nullable T t2) {
            if (t2 != null) {
                a0Var.b(this.a.a(t2), null, this.b);
            }
        }
    }

    public static final class k extends ParameterHandler<a0.c> {
        public static final k a = new k();

        public void a(RequestBuilder requestBuilder, @Nullable Object obj) {
            MultipartBody.c cVar = (MultipartBody.c) obj;
            if (cVar != null) {
                requestBuilder.f3180i.c.add(cVar);
            }
        }
    }

    public abstract void a(a0 a0Var, @Nullable T t2);
}
