package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.annotation.Nullable;

public interface CallAdapter<R, T> {

    public static abstract class a {
        @Nullable
        public abstract CallAdapter<?, ?> a(Type type, Annotation[] annotationArr, e0 e0Var);
    }

    T a(Call<R> call);

    Type a();
}
