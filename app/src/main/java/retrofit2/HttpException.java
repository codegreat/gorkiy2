package retrofit2;

import r.Response;
import r.Utils;

public class HttpException extends RuntimeException {
    public final int b;
    public final transient Response<?> c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HttpException(Response<?> response) {
        super("HTTP " + response.a.f2901f + " " + response.a.f2900e);
        Utils.a(response, "response == null");
        this.b = response.a.f2901f;
        this.c = response;
    }
}
