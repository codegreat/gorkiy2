package e.c.a.c.a.b;

import android.content.Context;
import android.content.SharedPreferences;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import e.c.a.a.ICryptoManager;
import n.n.c.Intrinsics;

/* compiled from: LegacyEncryptedPreferencesWrapper.kt */
public final class LegacyEncryptedPreferencesWrapper implements ISecuredPreferencesWrapper {
    public final Context a;
    public final ICryptoManager b;

    public LegacyEncryptedPreferencesWrapper(Context context, ICryptoManager iCryptoManager) {
        if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (iCryptoManager != null) {
            this.a = context;
            this.b = iCryptoManager;
        } else {
            Intrinsics.a("cryptoManager");
            throw null;
        }
    }

    public SharedPreferences a(String str) {
        if (str != null) {
            return new LegacyEncryptedSharedPreference(this.a, str, this.b);
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }
}
