package e.c.a.c.a.b;

import java.net.URLEncoder;
import kotlin.TypeCastException;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: LegacyEncryptedSharedPreference.kt */
public final class LegacyEncryptedSharedPreference0 extends j implements Functions0<Object, String> {
    public static final LegacyEncryptedSharedPreference0 c = new LegacyEncryptedSharedPreference0();

    public LegacyEncryptedSharedPreference0() {
        super(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Object a(Object obj) {
        if (obj != null) {
            String encode = URLEncoder.encode((String) obj, "UTF-8");
            Intrinsics.a((Object) encode, "URLEncoder.encode(it as String, \"UTF-8\")");
            return encode;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
    }
}
