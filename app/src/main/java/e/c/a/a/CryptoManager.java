package e.c.a.a;

import android.util.Base64;
import javax.crypto.Cipher;
import n.n.c.Intrinsics;

/* compiled from: CryptoManager.kt */
public final class CryptoManager implements ICryptoManager {
    public final KeyStoreManager a;

    public CryptoManager(KeyStoreManager keyStoreManager) {
        if (keyStoreManager != null) {
            this.a = keyStoreManager;
        } else {
            Intrinsics.a("keyStoreManager");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        if (bArr.length == 0) {
            return null;
        }
        Cipher instance = Cipher.getInstance("AES");
        instance.init(1, this.a.c);
        byte[] doFinal = instance.doFinal(bArr);
        Intrinsics.a((Object) doFinal, "cipher.doFinal(secret)");
        return Base64.encodeToString(doFinal, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public byte[] a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        byte[] decode = Base64.decode(str, 0);
        Intrinsics.a((Object) decode, "Base64.decode(encryptedString, Base64.DEFAULT)");
        Cipher instance = Cipher.getInstance("AES");
        instance.init(2, this.a.c);
        byte[] doFinal = instance.doFinal(decode);
        Intrinsics.a((Object) doFinal, "cipher.doFinal(secretEncrypted)");
        return doFinal;
    }
}
