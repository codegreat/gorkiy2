package e.c.d.b.c;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import e.c.d.b.BaveMvvmView;
import e.c.d.c.BaseVm;
import e.c.d.c.a;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.n.c;
import l.b.s.CompositeDisposable;
import n.Lazy;
import n.n.b.Functions;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.j;
import n.p.KProperty;

/* compiled from: BaseMvvmFragment.kt */
public abstract class BaseMvvmFragment<VM extends e.c.d.c.a> extends Fragment implements BaveMvvmView<VM> {
    public static final /* synthetic */ KProperty[] b0;
    public final Lazy X = c.a((Functions) new a(this));
    public boolean Y = true;
    public final CompositeDisposable Z = new CompositeDisposable();
    public final VmFactoryWrapper a0 = new VmFactoryWrapper();

    /* compiled from: BaseMvvmFragment.kt */
    public static final class a extends j implements Functions<VM> {
        public final /* synthetic */ BaseMvvmFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseMvvmFragment baseMvvmFragment) {
            super(0);
            this.c = baseMvvmFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
         arg types: [e.c.d.b.c.BaseMvvmFragment, i.o.ViewModelProvider$b]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider */
        public Object b() {
            BaseMvvmFragment baseMvvmFragment = this.c;
            return (BaseVm) ResourcesFlusher.a((Fragment) baseMvvmFragment, baseMvvmFragment.a0.a()).a(this.c.P());
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(BaseMvvmFragment.class), "vm", "getVm()Lru/waveaccess/wamvvmrx/vm/BaseVm;");
        Reflection.a(propertyReference1Impl);
        b0 = new KProperty[]{propertyReference1Impl};
    }

    public /* synthetic */ void C() {
        super.F = true;
        N();
    }

    public void E() {
        super.F = true;
        this.Z.a();
    }

    public void F() {
        super.F = true;
        O();
        if (this.Y) {
            Q();
            this.Y = false;
        }
    }

    public void N() {
    }

    public void O() {
    }

    public abstract Class<VM> P();

    public void Q() {
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        h().a(bundle);
        if (bundle != null) {
            this.Y = false;
        }
    }

    public VM h() {
        Lazy lazy = this.X;
        KProperty kProperty = b0[0];
        return (BaseVm) lazy.getValue();
    }

    public CompositeDisposable i() {
        return this.Z;
    }
}
