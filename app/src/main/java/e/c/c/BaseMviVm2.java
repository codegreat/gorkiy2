package e.c.c;

import l.b.Observable;
import l.b.ObservableSource;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: BaseMviVm.kt */
public final class BaseMviVm2<T, R> implements Function<Observable<T>, ObservableSource<R>> {
    public final /* synthetic */ BaseMviVm1 a;

    public BaseMviVm2(BaseMviVm1 baseMviVm1) {
        this.a = baseMviVm1;
    }

    public Object a(Object obj) {
        Observable observable = (Observable) obj;
        if (observable != null) {
            return this.a.a((Observable<? extends b>) observable);
        }
        Intrinsics.a("it");
        throw null;
    }
}
