package e.c.b;

import e.c.b.j.b;
import java.util.Map;
import l.b.t.Consumer;
import n.Unit;
import n.n.c.Intrinsics;

/* compiled from: RetrofitFactory.kt */
public final class RetrofitFactory0<T> implements Consumer<Map<b, ? extends String>> {
    public final /* synthetic */ RetrofitFactory b;

    public RetrofitFactory0(RetrofitFactory retrofitFactory) {
        this.b = retrofitFactory;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        Map map = (Map) obj;
        this.b.d.b((Map<b, String>) false);
        RetrofitFactory retrofitFactory = this.b;
        Intrinsics.a((Object) map, "it");
        retrofitFactory.a(map);
        this.b.d.b((Map<b, String>) true);
        this.b.c.b((Map<b, String>) Unit.a);
    }
}
