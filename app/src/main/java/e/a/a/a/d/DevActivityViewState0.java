package e.a.a.a.d;

import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: DevActivityViewState.kt */
public abstract class DevActivityViewState0 extends BaseViewState0 {

    /* compiled from: DevActivityViewState.kt */
    public static final class a extends DevActivityViewState0 {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: DevActivityViewState.kt */
    public static final class b extends DevActivityViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("value");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(this.a, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("ChangeEndpointEvent(value="), this.a, ")");
        }
    }

    /* compiled from: DevActivityViewState.kt */
    public static final class c extends DevActivityViewState0 {
        public static final c a = new c();

        public c() {
            super(null);
        }
    }

    /* compiled from: DevActivityViewState.kt */
    public static final class d extends DevActivityViewState0 {
        public static final d a = new d();

        public d() {
            super(null);
        }
    }

    /* compiled from: DevActivityViewState.kt */
    public static final class e extends DevActivityViewState0 {
        public static final e a = new e();

        public e() {
            super(null);
        }
    }

    /* compiled from: DevActivityViewState.kt */
    public static final class f extends DevActivityViewState0 {
        public final boolean a;

        public f(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof f) && this.a == ((f) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("ToggleSecureActivityEvent(toggled="), this.a, ")");
        }
    }

    public /* synthetic */ DevActivityViewState0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
