package e.a.a.a.b;

import android.os.Parcelable;
import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseMviVm1;
import e.c.c.k;
import java.util.List;
import l.b.Observable;
import l.b.s.Disposable;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableOnErrorReturn;
import n.Unit;
import n.g;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Lambda;
import n.n.c.j;

/* compiled from: BaseDpFragmentVm.kt */
public abstract class BaseDpFragmentVm1<VS> extends BaseMviVm1<VS> {

    /* renamed from: i  reason: collision with root package name */
    public final IFragmentCoordinator f586i;

    /* compiled from: BaseDpFragmentVm.kt */
    public static final class a extends j implements Functions0<a, g> {
        public final /* synthetic */ BaseDpFragmentVm1 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseDpFragmentVm1 baseDpFragmentVm1) {
            super(1);
            this.c = baseDpFragmentVm1;
        }

        public Object a(Object obj) {
            if (((BaseEvents) obj) != null) {
                this.c.f();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: BaseDpFragmentVm.kt */
    public static final class b extends j implements Functions0<o, g> {
        public final /* synthetic */ BaseDpFragmentVm1 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(BaseDpFragmentVm1 baseDpFragmentVm1) {
            super(1);
            this.c = baseDpFragmentVm1;
        }

        public Object a(Object obj) {
            if (((BaseEvents1) obj) != null) {
                this.c.f();
                this.c.f();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: BaseDpFragmentVm.kt */
    public static final class c extends Lambda implements Functions0 {
        public static final c c = new c();

        public c() {
            super(1);
        }

        public Object a(Object obj) {
            if (((Throwable) obj) != null) {
                return null;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public BaseDpFragmentVm1(IFragmentCoordinator iFragmentCoordinator) {
        if (iFragmentCoordinator != null) {
            this.f586i = iFragmentCoordinator;
        } else {
            Intrinsics.a("fragmentCoordinator");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(BaseEvents.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(BaseEvents1.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a4 = Observable.a(super.a(observable), BaseMviVm.a(a2, new a(this)), BaseMviVm.a(a3, new b(this)));
            Intrinsics.a((Object) a4, "Observable.mergeArray(\n …)\n            }\n        )");
            return a4;
        }
        Intrinsics.a("o");
        throw null;
    }

    public void f() {
        Collections.a(this.f586i, (Integer) null, (Object) null, 3, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(List<? extends Observable<? extends e.c.c.b>> list) {
        if (list != null) {
            super.a(list);
            Disposable a2 = this.f586i.b(null).a(new BaseDpFragmentVm(new BaseDpFragmentVm2(this)));
            Intrinsics.a((Object) a2, "fragmentCoordinator.addE…be(::onErrorScreenReturn)");
            j.c.a.a.c.n.c.a(a2, super.f722f);
            return;
        }
        Intrinsics.a("es");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.b.BaseDpFragmentVm0, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservableOnErrorReturn, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static /* synthetic */ Observable a(BaseDpFragmentVm1 baseDpFragmentVm1, Observable observable, Class cls, Parcelable parcelable, Parcelable parcelable2, Functions0 functions0, ErrorNavigationDto errorNavigationDto, int i2, Object obj) {
        Observable observable2 = observable;
        if (obj == null) {
            Parcelable parcelable3 = (i2 & 2) != 0 ? null : parcelable;
            Parcelable parcelable4 = (i2 & 4) != 0 ? null : parcelable2;
            c cVar = (i2 & 8) != 0 ? c.c : functions0;
            ErrorNavigationDto errorNavigationDto2 = (i2 & 16) != 0 ? null : errorNavigationDto;
            if (baseDpFragmentVm1 == null) {
                throw null;
            } else if (observable2 == null) {
                Intrinsics.a("$this$withErrorHandling");
                throw null;
            } else if (cls == null) {
                Intrinsics.a("resultClass");
                throw null;
            } else if (cVar != null) {
                BaseDpFragmentVm0 baseDpFragmentVm0 = new BaseDpFragmentVm0(baseDpFragmentVm1, cVar, errorNavigationDto2, cls, parcelable3, parcelable4);
                ObjectHelper.a((Object) baseDpFragmentVm0, "valueSupplier is null");
                ObservableOnErrorReturn observableOnErrorReturn = new ObservableOnErrorReturn(observable, baseDpFragmentVm0);
                Intrinsics.a((Object) observableOnErrorReturn, "this.onErrorReturn { err…}\n            }\n        }");
                return observableOnErrorReturn;
            } else {
                Intrinsics.a("customHandle");
                throw null;
            }
        } else {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: withErrorHandling");
        }
    }
}
