package e.a.a.a.c.b;

import e.a.a.a.e.o.c;
import e.a.a.a.e.q.b;
import k.a.Factory;
import m.a.Provider;

public final class ProcessingFragmentVm_Factory implements Factory<d> {
    public final Provider<b> a;
    public final Provider<c> b;

    public ProcessingFragmentVm_Factory(Provider<b> provider, Provider<c> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    public Object get() {
        return new ProcessingFragmentVm(this.a.get(), this.b.get());
    }
}
