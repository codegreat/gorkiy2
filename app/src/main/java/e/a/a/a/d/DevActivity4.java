package e.a.a.a.d;

import android.content.Intent;
import n.Unit;
import n.g;
import n.n.b.Functions1;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;
import ru.covid19.core.presentation.dev.DevActivity;

/* compiled from: DevActivity.kt */
public final /* synthetic */ class DevActivity4 extends h implements Functions1<Intent, Integer, g> {
    public DevActivity4(DevActivity devActivity) {
        super(2, devActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [n.n.c.CallableReference, e.a.a.a.d.DevActivity4] */
    public Object a(Object obj, Object obj2) {
        Intent intent = (Intent) obj;
        int intValue = ((Number) obj2).intValue();
        if (intent != null) {
            ((DevActivity) this.c).startActivityForResult(intent, intValue);
            return Unit.a;
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "openActivityForResult";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(DevActivity.class);
    }

    public final String i() {
        return "openActivityForResult(Landroid/content/Intent;I)V";
    }
}
