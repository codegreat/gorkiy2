package e.a.a.a.e.p;

import android.os.Parcel;
import android.os.Parcelable;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ErrorResultDto.kt */
public final class ErrorResultDto implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final String b;
    public final ErrorNavigationDto0 c;
    public final Parcelable d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f620e;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new ErrorResultDto(parcel.readString(), (ErrorNavigationDto0) Enum.valueOf(ErrorNavigationDto0.class, parcel.readString()), parcel.readParcelable(ErrorResultDto.class.getClassLoader()), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new ErrorResultDto[i2];
        }
    }

    public ErrorResultDto(String str, ErrorNavigationDto0 errorNavigationDto0, Parcelable parcelable, boolean z) {
        if (str == null) {
            Intrinsics.a("errorKey");
            throw null;
        } else if (errorNavigationDto0 != null) {
            this.b = str;
            this.c = errorNavigationDto0;
            this.d = parcelable;
            this.f620e = z;
        } else {
            Intrinsics.a("resultCode");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ErrorResultDto)) {
            return false;
        }
        ErrorResultDto errorResultDto = (ErrorResultDto) obj;
        return Intrinsics.a(this.b, errorResultDto.b) && Intrinsics.a(this.c, errorResultDto.c) && Intrinsics.a(this.d, errorResultDto.d) && this.f620e == errorResultDto.f620e;
    }

    public int hashCode() {
        String str = this.b;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ErrorNavigationDto0 errorNavigationDto0 = this.c;
        int hashCode2 = (hashCode + (errorNavigationDto0 != null ? errorNavigationDto0.hashCode() : 0)) * 31;
        Parcelable parcelable = this.d;
        if (parcelable != null) {
            i2 = parcelable.hashCode();
        }
        int i3 = (hashCode2 + i2) * 31;
        boolean z = this.f620e;
        if (z) {
            z = true;
        }
        return i3 + (z ? 1 : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("ErrorResultDto(errorKey=");
        a2.append(this.b);
        a2.append(", resultCode=");
        a2.append(this.c);
        a2.append(", errorEvent=");
        a2.append(this.d);
        a2.append(", leavePreviousScreen=");
        return outline.a(a2, this.f620e, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
            parcel.writeString(this.c.name());
            parcel.writeParcelable(this.d, i2);
            parcel.writeInt(this.f620e ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
