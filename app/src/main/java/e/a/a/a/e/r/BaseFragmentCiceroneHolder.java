package e.a.a.a.e.r;

import e.b.a.Cicerone;
import e.b.a.CommandBuffer;
import e.b.a.Router;
import e.b.a.e;
import java.util.HashMap;
import n.n.c.Intrinsics;

/* compiled from: BaseFragmentCiceroneHolder.kt */
public abstract class BaseFragmentCiceroneHolder<T extends e> {
    public final HashMap<String, Cicerone<T>> a = new HashMap<>();

    public abstract T a();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [T, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final T a(String str) {
        if (str != null) {
            T t2 = b(str).a;
            Intrinsics.a((Object) t2, "getCicerone(containerTag).router");
            return (Router) t2;
        }
        Intrinsics.a("containerTag");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.b.a.Cicerone, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final Cicerone<T> b(String str) {
        if (!this.a.containsKey(str)) {
            HashMap<String, Cicerone<T>> hashMap = this.a;
            Cicerone cicerone = new Cicerone(a());
            Intrinsics.a((Object) cicerone, "Cicerone.create<T>(\n    …     router\n            )");
            hashMap.put(str, cicerone);
        }
        Cicerone<T> cicerone2 = this.a.get(str);
        if (cicerone2 != null) {
            return cicerone2;
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.b.a.CommandBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final CommandBuffer c(String str) {
        if (str != null) {
            CommandBuffer commandBuffer = b(str).a.a;
            Intrinsics.a((Object) commandBuffer, "getCicerone(containerTag).navigatorHolder");
            return commandBuffer;
        }
        Intrinsics.a("containerTag");
        throw null;
    }
}
