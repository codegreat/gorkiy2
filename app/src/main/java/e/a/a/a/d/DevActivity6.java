package e.a.a.a.d;

import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;
import ru.covid19.core.presentation.dev.DevActivity;

/* compiled from: DevActivity.kt */
public final /* synthetic */ class DevActivity6 extends h implements Functions<g> {
    public DevActivity6(DevActivity devActivity) {
        super(0, devActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.d.DevActivity6, n.n.c.CallableReference] */
    public Object b() {
        ((DevActivity) this.c).moveTaskToBack(true);
        return Unit.a;
    }

    public final String f() {
        return "hideApp";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(DevActivity.class);
    }

    public final String i() {
        return "hideApp()V";
    }
}
