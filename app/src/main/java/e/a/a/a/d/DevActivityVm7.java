package e.a.a.a.d;

import e.a.a.a.d.DevActivityViewState0;
import e.a.a.a.d.y;
import l.b.Completable;
import l.b.Scheduler;
import l.b.d;
import l.b.r.b.AndroidSchedulers;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.a.CompletableObserveOn;
import l.b.w.Schedulers;
import n.n.c.Intrinsics;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm7<T, R> implements Function<y.c, d> {
    public final /* synthetic */ DevActivityVm2 a;

    public DevActivityVm7(DevActivityVm2 devActivityVm2) {
        this.a = devActivityVm2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Scheduler, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Object a(Object obj) {
        if (((DevActivityViewState0.c) obj) != null) {
            Completable a2 = Completable.a(new DevActivityVm(this)).a(Schedulers.b);
            Scheduler a3 = AndroidSchedulers.a();
            ObjectHelper.a((Object) a3, "scheduler is null");
            return new CompletableObserveOn(a2, a3).a(new DevActivityVm6(this));
        }
        Intrinsics.a("it");
        throw null;
    }
}
