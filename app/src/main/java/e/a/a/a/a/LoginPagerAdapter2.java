package e.a.a.a.a;

import android.view.View;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: LoginPagerAdapter.kt */
public final class LoginPagerAdapter2 extends j implements Functions0<CharSequence, g> {
    public final /* synthetic */ LoginPagerAdapter1 c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPagerAdapter2(LoginPagerAdapter1 loginPagerAdapter1, View view) {
        super(1);
        this.c = loginPagerAdapter1;
    }

    public Object a(Object obj) {
        if (((CharSequence) obj) != null) {
            this.c.g.a((Boolean) true);
            return Unit.a;
        }
        Intrinsics.a("it");
        throw null;
    }
}
