package e.a.a.a.c.c;

import android.view.View;
import e.a.a.a.c.c.BottomSheetSelectorDialogViewState0;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.c.c.b;

/* compiled from: BottomSheetSelectorDialog.kt */
public final class BottomSheetSelectorDialog implements View.OnClickListener {
    public final /* synthetic */ BottomSheetSelectorNavigationDto b;
    public final /* synthetic */ BottomSheetSelectorDialog0 c;

    public BottomSheetSelectorDialog(BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto, BottomSheetSelectorDialog0 bottomSheetSelectorDialog0) {
        this.b = bottomSheetSelectorNavigationDto;
        this.c = bottomSheetSelectorDialog0;
    }

    public final void onClick(View view) {
        this.c.i0.a((b) new BottomSheetSelectorDialogViewState0.c(this.b));
    }
}
