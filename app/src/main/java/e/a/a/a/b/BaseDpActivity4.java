package e.a.a.a.b;

import android.content.Intent;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.i.e.NavigationWrapper;
import e.b.a.CommandBuffer;
import e.b.a.Navigator;
import e.c.d.b.b.BaseMvvmActivity;
import e.c.d.b.c.VmFactoryWrapper;
import e.c.d.c.a;
import n.n.c.Intrinsics;

/* compiled from: BaseDpActivity.kt */
public abstract class BaseDpActivity4<VM extends a> extends BaseMvvmActivity<VM> {
    public final VmFactoryWrapper u = new VmFactoryWrapper();
    public final NavigationWrapper v = new NavigationWrapper();
    public Navigator w;

    /* JADX WARN: Type inference failed for: r1v0, types: [i.l.a.FragmentActivity, e.a.a.a.b.BaseDpActivity4] */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (!r().b.a(i3, intent)) {
            BaseDpActivity4.super.onActivityResult(i2, i3, intent);
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            super.onCreate(r8)
            e.a.a.a.e.o.ActivityNavigator r8 = new e.a.a.a.e.o.ActivityNavigator
            e.a.a.a.b.BaseDpActivity r2 = new e.a.a.a.b.BaseDpActivity
            r2.<init>(r7)
            e.a.a.a.b.BaseDpActivity0 r3 = new e.a.a.a.b.BaseDpActivity0
            r3.<init>(r7)
            e.a.a.a.b.BaseDpActivity1 r4 = new e.a.a.a.b.BaseDpActivity1
            r4.<init>(r7)
            e.a.a.a.b.BaseDpActivity2 r5 = new e.a.a.a.b.BaseDpActivity2
            r5.<init>(r7)
            e.a.a.a.b.BaseDpActivity3 r6 = new e.a.a.a.b.BaseDpActivity3
            r6.<init>(r7)
            r0 = r8
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r7.w = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.a.b.BaseDpActivity4.onCreate(android.os.Bundle):void");
    }

    public void onPause() {
        super.onPause();
        r().a().a((Navigator) null);
    }

    public void onResume() {
        super.onResume();
        CommandBuffer a = r().a();
        Navigator navigator = this.w;
        if (navigator != null) {
            a.a(navigator);
            AppCiceroneHolder r2 = r();
            if (getClass().getCanonicalName() == null) {
                Intrinsics.a();
                throw null;
            } else if (r2 == null) {
                throw null;
            }
        } else {
            Intrinsics.b("navigator");
            throw null;
        }
    }

    public final AppCiceroneHolder r() {
        AppCiceroneHolder appCiceroneHolder = this.v.b;
        if (appCiceroneHolder != null) {
            return appCiceroneHolder;
        }
        Intrinsics.b("appCiceroneHolder");
        throw null;
    }
}
