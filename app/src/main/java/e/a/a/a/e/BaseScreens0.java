package e.a.a.a.e;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import e.a.a.a.e.p.ErrorNavigationDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.core.presentation.common.error.ErrorFragment;

/* compiled from: BaseScreens.kt */
public final class BaseScreens0 extends BaseScreens2 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final ErrorNavigationDto d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new BaseScreens0((ErrorNavigationDto) ErrorNavigationDto.CREATOR.createFromParcel(parcel));
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new BaseScreens0[i2];
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseScreens0(ErrorNavigationDto errorNavigationDto) {
        super(errorNavigationDto);
        if (errorNavigationDto != null) {
            this.d = errorNavigationDto;
            return;
        }
        Intrinsics.a("dto");
        throw null;
    }

    public Fragment a() {
        return new ErrorFragment();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof BaseScreens0) && Intrinsics.a(this.d, ((BaseScreens0) obj).d);
        }
        return true;
    }

    public int hashCode() {
        ErrorNavigationDto errorNavigationDto = this.d;
        if (errorNavigationDto != null) {
            return errorNavigationDto.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("ErrorScreen(dto=");
        a2.append(this.d);
        a2.append(")");
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            this.d.writeToParcel(parcel, 0);
        } else {
            Intrinsics.a("parcel");
            throw null;
        }
    }
}
