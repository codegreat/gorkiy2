package e.a.a.a.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import e.c.d.a.ViewAction;
import e.c.d.a.ViewState;
import i.z.a.PagerAdapter;
import l.b.s.CompositeDisposable;
import n.n.c.Intrinsics;

/* compiled from: LoginPagerAdapter.kt */
public final class LoginPagerAdapter1 extends PagerAdapter {
    public final ViewAction<CharSequence> b;
    public final ViewAction<CharSequence> c;
    public final ViewState<Boolean> d;

    /* renamed from: e  reason: collision with root package name */
    public final ViewState<Boolean> f580e;

    /* renamed from: f  reason: collision with root package name */
    public final ViewState<Boolean> f581f;
    public final ViewState<Boolean> g;
    public CompositeDisposable h;

    /* renamed from: i  reason: collision with root package name */
    public CompositeDisposable f582i;

    /* renamed from: j  reason: collision with root package name */
    public final Context f583j;

    public LoginPagerAdapter1(Context context) {
        if (context != null) {
            this.f583j = context;
            this.b = new ViewAction<>();
            this.c = new ViewAction<>();
            this.d = new ViewState<>(null, 1);
            this.f580e = new ViewState<>(null, 1);
            this.f581f = new ViewState<>(null, 1);
            this.g = new ViewState<>(null, 1);
            return;
        }
        Intrinsics.a("context");
        throw null;
    }

    public void a(ViewGroup viewGroup, int i2, Object obj) {
        if (viewGroup == null) {
            Intrinsics.a("container");
            throw null;
        } else if (obj != null) {
            viewGroup.removeView((View) obj);
        } else {
            Intrinsics.a("view");
            throw null;
        }
    }

    public int a() {
        return LoginPagerAdapter.values().length;
    }
}
