package e.a.a.a.c.a;

import com.crashlytics.android.core.CrashlyticsController;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ErrorFragmentViewState.kt */
public final class ErrorFragmentViewState0 {
    public final ErrorFragmentViewState a;

    public ErrorFragmentViewState0() {
        this(null, 1);
    }

    public ErrorFragmentViewState0(ErrorFragmentViewState errorFragmentViewState) {
        if (errorFragmentViewState != null) {
            this.a = errorFragmentViewState;
        } else {
            Intrinsics.a(CrashlyticsController.EVENT_TYPE_LOGGED);
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof ErrorFragmentViewState0) && Intrinsics.a(this.a, ((ErrorFragmentViewState0) obj).a);
        }
        return true;
    }

    public int hashCode() {
        ErrorFragmentViewState errorFragmentViewState = this.a;
        if (errorFragmentViewState != null) {
            return errorFragmentViewState.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("ErrorFragmentViewState(error=");
        a2.append(this.a);
        a2.append(")");
        return a2.toString();
    }

    public /* synthetic */ ErrorFragmentViewState0(ErrorFragmentViewState errorFragmentViewState, int i2) {
        ErrorFragmentViewState errorFragmentViewState2 = (i2 & 1) != 0 ? new ErrorFragmentViewState(null, null, null, null, null, false, false, false, false, false, null, null, 4095) : errorFragmentViewState;
        if (errorFragmentViewState2 != null) {
            this.a = errorFragmentViewState2;
        } else {
            Intrinsics.a(CrashlyticsController.EVENT_TYPE_LOGGED);
            throw null;
        }
    }
}
