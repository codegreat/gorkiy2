package e.a.a.a.b;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import e.c.c.BaseMviView1;
import e.c.c.BaseMviVm1;
import e.c.c.BaseViewState2;
import e.c.c.b;
import e.c.d.b.c.VmFactoryWrapper;
import i.b.k.ResourcesFlusher;
import j.c.a.b.r.c;
import j.e.b.PublishRelay;
import java.util.List;
import l.b.Observable;
import l.b.s.CompositeDisposable;
import n.Lazy;
import n.i.Collections;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.j;
import n.p.KProperty;

/* compiled from: BaseBottomSheetMviDialog.kt */
public abstract class BaseBottomSheetMviDialog<VS, VM extends BaseMviVm1<VS>> extends c implements BaseMviView1<VS, VM> {
    public static final /* synthetic */ KProperty[] n0;
    public final PublishRelay<b> i0;
    public boolean j0 = true;
    public final CompositeDisposable k0 = new CompositeDisposable();
    public final Lazy l0 = j.c.a.a.c.n.c.a((Functions) new a(this));
    public final VmFactoryWrapper m0 = new VmFactoryWrapper();

    /* compiled from: BaseBottomSheetMviDialog.kt */
    public static final class a extends j implements Functions<VM> {
        public final /* synthetic */ BaseBottomSheetMviDialog c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseBottomSheetMviDialog baseBottomSheetMviDialog) {
            super(0);
            this.c = baseBottomSheetMviDialog;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [e.a.a.a.b.BaseBottomSheetMviDialog, androidx.fragment.app.Fragment] */
        public Object b() {
            ? r0 = this.c;
            return (BaseMviVm1) ResourcesFlusher.a((Fragment) r0, r0.m0.a()).a(this.c.O());
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(BaseBottomSheetMviDialog.class), "vm", "getVm()Lru/waveaccess/wamvirx/BaseMviVm;");
        Reflection.a(propertyReference1Impl);
        n0 = new KProperty[]{propertyReference1Impl};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay<e.c.c.b>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public BaseBottomSheetMviDialog() {
        PublishRelay<b> publishRelay = new PublishRelay<>();
        Intrinsics.a((Object) publishRelay, "PublishRelay.create()");
        this.i0 = publishRelay;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [e.a.a.a.b.BaseBottomSheetMviDialog, i.l.a.DialogFragment] */
    public /* synthetic */ void C() {
        BaseBottomSheetMviDialog.super.C();
        N();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.b.BaseBottomSheetMviDialog, androidx.fragment.app.Fragment] */
    public void E() {
        this.F = true;
        this.k0.a();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.b.BaseBottomSheetMviDialog, e.c.c.BaseMviView1, androidx.fragment.app.Fragment] */
    public void F() {
        this.F = true;
        Collections.a((BaseMviView1) this);
        if (this.j0) {
            P();
            this.j0 = false;
        }
    }

    public void N() {
    }

    public abstract Class<VM> O();

    public void P() {
    }

    public void a(BaseViewState2 baseViewState2) {
        if (baseViewState2 == null) {
            Intrinsics.a("ve");
            throw null;
        }
    }

    public void a(VS vs) {
    }

    public PublishRelay<b> b() {
        return this.i0;
    }

    public List<Observable<? extends b>> g() {
        return j.c.a.a.c.n.c.c(b());
    }

    public VM h() {
        Lazy lazy = this.l0;
        KProperty kProperty = n0[0];
        return (BaseMviVm1) lazy.getValue();
    }

    public CompositeDisposable i() {
        return this.k0;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.b.BaseBottomSheetMviDialog, i.l.a.DialogFragment] */
    public void b(Bundle bundle) {
        BaseBottomSheetMviDialog.super.b(bundle);
        h().c();
        if (bundle != null) {
            this.j0 = false;
        }
    }
}
