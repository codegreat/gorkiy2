package e.a.a.a.c.c;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.c;
import e.a.a.d;
import i.h.e.ContextCompat;
import java.util.HashMap;
import n.n.c.Intrinsics;

/* compiled from: BottomSheetSelectorView.kt */
public final class BottomSheetSelectorView extends ConstraintLayout {

    /* renamed from: q  reason: collision with root package name */
    public HashMap f600q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ BottomSheetSelectorView(android.content.Context r3, android.util.AttributeSet r4, int r5, int r6) {
        /*
            r2 = this;
            r0 = r6 & 2
            r1 = 0
            if (r0 == 0) goto L_0x0006
            r4 = r1
        L_0x0006:
            r6 = r6 & 4
            if (r6 == 0) goto L_0x000b
            r5 = 0
        L_0x000b:
            if (r3 == 0) goto L_0x0029
            r2.<init>(r3, r4, r5)
            int r4 = e.a.a.e.view_bottom_sheet_selector
            android.view.View r3 = android.view.ViewGroup.inflate(r3, r4, r1)
            java.lang.String r4 = "view"
            n.n.c.Intrinsics.a(r3, r4)
            androidx.constraintlayout.widget.ConstraintLayout$a r4 = new androidx.constraintlayout.widget.ConstraintLayout$a
            r5 = -1
            r6 = -2
            r4.<init>(r5, r6)
            r3.setLayoutParams(r4)
            r2.addView(r3)
            return
        L_0x0029:
            java.lang.String r3 = "context"
            n.n.c.Intrinsics.a(r3)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.a.c.c.BottomSheetSelectorView.<init>(android.content.Context, android.util.AttributeSet, int, int):void");
    }

    public View b(int i2) {
        if (this.f600q == null) {
            this.f600q = new HashMap();
        }
        View view = (View) this.f600q.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i2);
        this.f600q.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void setItem(BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto) {
        int i2;
        if (bottomSheetSelectorNavigationDto != null) {
            TextView textView = (TextView) b(d.view_bottom_sheet_selector_tv_label);
            Intrinsics.a((Object) textView, "view_bottom_sheet_selector_tv_label");
            textView.setText(bottomSheetSelectorNavigationDto.getLabel());
            ImageView imageView = (ImageView) b(d.view_bottom_sheet_selector_iv_selection);
            Context context = getContext();
            if (bottomSheetSelectorNavigationDto.isSelected()) {
                i2 = c.colorUiBlue;
            } else {
                i2 = c.colorUiLightBlue;
            }
            imageView.setColorFilter(ContextCompat.a(context, i2), PorterDuff.Mode.SRC_IN);
            return;
        }
        Intrinsics.a("item");
        throw null;
    }
}
