package e.a.a.a.b;

import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: BaseDpActivity.kt */
public final /* synthetic */ class BaseDpActivity2 extends h implements Functions<g> {
    public BaseDpActivity2(BaseDpActivity4 baseDpActivity4) {
        super(0, baseDpActivity4);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [e.a.a.a.b.BaseDpActivity4, android.app.Activity] */
    public Object b() {
        ((BaseDpActivity4) this.c).moveTaskToBack(true);
        return Unit.a;
    }

    public final String f() {
        return "hideApp";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(BaseDpActivity4.class);
    }

    public final String i() {
        return "hideApp()V";
    }
}
