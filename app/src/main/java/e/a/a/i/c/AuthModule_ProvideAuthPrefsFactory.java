package e.a.a.i.c;

import android.content.Context;
import e.a.a.g.a.Session;
import e.a.a.g.a.d;
import e.a.a.g.b.b.a.AuthPrefs0;
import e.a.a.g.b.b.a.b;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.a.c.a.a.a;
import j.c.a.a.c.n.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

public final class AuthModule_ProvideAuthPrefsFactory implements Factory<b> {
    public final AuthModule a;
    public final Provider<Context> b;
    public final Provider<a> c;
    public final Provider<d> d;

    public AuthModule_ProvideAuthPrefsFactory(h hVar, Provider<Context> provider, Provider<a> provider2, Provider<d> provider3) {
        this.a = hVar;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.g.b.b.a.AuthPrefs0, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        AuthModule authModule = this.a;
        Context context = this.b.get();
        IPrefsStorage iPrefsStorage = this.c.get();
        Session session = this.d.get();
        if (authModule == null) {
            throw null;
        } else if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (iPrefsStorage == null) {
            Intrinsics.a("prefsStorage");
            throw null;
        } else if (session != null) {
            AuthPrefs0 authPrefs0 = new AuthPrefs0(context, iPrefsStorage, session);
            c.a((Object) authPrefs0, "Cannot return null from a non-@Nullable @Provides method");
            return authPrefs0;
        } else {
            Intrinsics.a("session");
            throw null;
        }
    }
}
