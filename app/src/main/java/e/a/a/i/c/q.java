package e.a.a.i.c;

import e.a.a.h.DeeplinkManager;
import e.a.a.h.IDeeplinkConfig;
import e.a.a.h.d;
import e.a.a.h.e;
import j.c.a.a.c.n.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import k.a.Factory;
import kotlin.TypeCastException;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: CoreNavigationModule_ProvideDeeplinkManager$core_prodReleaseFactory */
public final class q implements Factory<e> {
    public final CoreNavigationModule a;
    public final Provider<d> b;

    public q(p pVar, Provider<d> provider) {
        this.a = pVar;
        this.b = provider;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.h.DeeplinkManager, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        CoreNavigationModule coreNavigationModule = this.a;
        IDeeplinkConfig iDeeplinkConfig = this.b.get();
        if (coreNavigationModule == null) {
            throw null;
        } else if (iDeeplinkConfig != null) {
            ArrayList arrayList = new ArrayList();
            List c = c.c(iDeeplinkConfig);
            if (c != null) {
                arrayList.addAll(c);
                DeeplinkManager deeplinkManager = new DeeplinkManager();
                Object[] array = arrayList.toArray(new IDeeplinkConfig[0]);
                if (array != null) {
                    IDeeplinkConfig[] iDeeplinkConfigArr = (IDeeplinkConfig[]) array;
                    IDeeplinkConfig[] iDeeplinkConfigArr2 = (IDeeplinkConfig[]) Arrays.copyOf(iDeeplinkConfigArr, iDeeplinkConfigArr.length);
                    if (iDeeplinkConfigArr2 != null) {
                        for (IDeeplinkConfig a2 : iDeeplinkConfigArr2) {
                            deeplinkManager.a.putAll(a2.a());
                        }
                        c.a((Object) deeplinkManager, "Cannot return null from a non-@Nullable @Provides method");
                        return deeplinkManager;
                    }
                    Intrinsics.a("configs");
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            Intrinsics.a("configs");
            throw null;
        } else {
            Intrinsics.a("config");
            throw null;
        }
    }
}
