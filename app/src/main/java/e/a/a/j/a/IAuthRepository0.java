package e.a.a.j.a;

import l.b.Completable;
import l.b.Single;
import ru.covid19.core.data.network.model.SessionResponse;

/* compiled from: IAuthRepository.kt */
public interface IAuthRepository0 {
    Completable a();

    Single<SessionResponse> a(String str, String str2, boolean z);

    Single<SessionResponse> b(String str, String str2, boolean z);
}
