package e.a.a.g.a.h;

import e.a.a.g.a.IAuthenticateRequestChecker;
import e.a.a.g.a.Session;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.q.Sequences1;
import n.r.Indent;
import n.r.MatchResult;
import n.r.Regex;
import n.r.Regex1;
import n.r.Regex2;
import o.Interceptor;
import o.Response;

/* compiled from: EpguSaveCookieInterceptor.kt */
public final class EpguSaveCookieInterceptor0 implements Interceptor {
    public final Session b;
    public final IAuthenticateRequestChecker c;

    public EpguSaveCookieInterceptor0(Session session, IAuthenticateRequestChecker iAuthenticateRequestChecker) {
        if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (iAuthenticateRequestChecker != null) {
            this.b = session;
            this.c = iAuthenticateRequestChecker;
        } else {
            Intrinsics.a("authenticateRequestChecker");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Response a(Interceptor.a aVar) {
        if (aVar != null) {
            Response a = aVar.a(aVar.f());
            if (this.c.c(aVar.f().b) && (!a.h.b("Set-Cookie").isEmpty())) {
                Regex regex = new Regex("(.+?)=(.*?)(?:;|$)");
                for (String next : a.h.b("Set-Cookie")) {
                    if (next != null) {
                        Regex1 regex1 = new Regex1(regex, next, 0);
                        Regex2 regex2 = Regex2.f2793f;
                        if (regex2 != null) {
                            Iterator it = new Sequences1(regex1, regex2).iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    List<String> a2 = ((MatchResult) it.next()).b().a.a();
                                    ArrayList arrayList = new ArrayList(Collections.a(a2, 10));
                                    for (String str : a2) {
                                        if (str != null) {
                                            arrayList.add(Indent.c(str).toString());
                                        } else {
                                            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                                        }
                                    }
                                    String str2 = (String) arrayList.get(1);
                                    String str3 = (String) arrayList.get(2);
                                    if (!Indent.b(str3)) {
                                        List<String> list = EpguSaveCookieInterceptor.a;
                                        Locale locale = Locale.getDefault();
                                        Intrinsics.a((Object) locale, "Locale.getDefault()");
                                        if (str2 != null) {
                                            String lowerCase = str2.toLowerCase(locale);
                                            Intrinsics.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                                            if (!list.contains(lowerCase)) {
                                                if (this.b.c.containsKey(str2)) {
                                                    this.b.c.remove(str2);
                                                }
                                                this.b.c.put(str2, str3);
                                            }
                                        } else {
                                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                        }
                                    }
                                }
                            }
                        } else {
                            Intrinsics.a("nextFunction");
                            throw null;
                        }
                    } else {
                        Intrinsics.a("input");
                        throw null;
                    }
                }
            }
            return a;
        }
        Intrinsics.a("chain");
        throw null;
    }
}
