package e.a.a.k;

import e.a.a.g.a.ClientErrorResponseDto;
import java.util.concurrent.Callable;

/* compiled from: RxExtensions.kt */
public final class RxExtensions0<V> implements Callable<Throwable> {
    public final /* synthetic */ ClientErrorResponseDto b;

    public RxExtensions0(ClientErrorResponseDto clientErrorResponseDto) {
        this.b = clientErrorResponseDto;
    }

    public Object call() {
        return new RxExtensions(this.b);
    }
}
