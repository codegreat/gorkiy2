package e.a.a.k;

import java.util.concurrent.Callable;

/* compiled from: RxExtensions.kt */
public final class RxExtensions1<V> implements Callable<Throwable> {
    public final /* synthetic */ Throwable b;

    public RxExtensions1(Throwable th) {
        this.b = th;
    }

    public Object call() {
        return this.b;
    }
}
