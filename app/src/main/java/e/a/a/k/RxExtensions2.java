package e.a.a.k;

import e.a.a.g.a.ClientErrorResponseDto;
import j.c.d.Gson;
import l.b.SingleSource;
import l.b.t.Function;
import l.b.u.e.d.SingleError;
import n.n.c.Intrinsics;
import o.ResponseBody;
import r.Response;
import retrofit2.HttpException;

/* compiled from: RxExtensions.kt */
public final class RxExtensions2<T, R> implements Function<Throwable, SingleSource<? extends T>> {
    public final /* synthetic */ Gson a;

    public RxExtensions2(Gson gson) {
        this.a = gson;
    }

    public Object a(Object obj) {
        ResponseBody responseBody;
        Throwable th = (Throwable) obj;
        String str = null;
        if (th != null) {
            if (!((th instanceof HttpException) && ((HttpException) th).b / 100 == 4)) {
                return new SingleError(new RxExtensions1(th));
            }
            Gson gson = this.a;
            Class<ClientErrorResponseDto> cls = ClientErrorResponseDto.class;
            if (gson != null) {
                Response<?> response = ((HttpException) th).c;
                if (!(response == null || (responseBody = response.c) == null)) {
                    str = responseBody.h();
                }
                return new SingleError(new RxExtensions0((ClientErrorResponseDto) gson.a(str, cls)));
            }
            Intrinsics.a("gson");
            throw null;
        }
        Intrinsics.a("t");
        throw null;
    }
}
