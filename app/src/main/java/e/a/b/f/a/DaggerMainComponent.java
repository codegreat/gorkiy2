package e.a.b.f.a;

import android.content.Context;
import e.a.a.a.c.a.ErrorFragmentVm;
import e.a.a.a.c.a.ErrorFragmentVm_Factory;
import e.a.a.a.c.b.ProcessingFragmentVm;
import e.a.a.a.c.b.ProcessingFragmentVm_Factory;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.a.g.a.Session;
import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.h.IDeeplinkManager;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.d.VmFactory_Factory;
import e.a.a.i.e.DeeplinkManagerWrapper;
import e.a.a.i.e.NavigationWrapper;
import e.a.a.i.e.NavigationWrapper0;
import e.a.a.j.a.IAuthRepository0;
import e.a.a.m.a.GlideHelper;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.f.c.MainModule_ProvideEsiaProfileApiServiceFactory;
import e.a.b.f.c.MainModule_ProvideMainCoordinatorFactory;
import e.a.b.f.c.MainModule_ProvideProfileDataApiServiceFactory;
import e.a.b.f.c.MainModule_ProvideProfileRepositoryFactory;
import e.a.b.h.b.MainActivityVm;
import e.a.b.h.b.MainActivityVm_Factory;
import e.a.b.h.b.d.AuthFragmentVm;
import e.a.b.h.b.d.AuthFragmentVm_Factory;
import e.a.b.h.b.e.BaseFaqBottomSheetDialogVm;
import e.a.b.h.b.e.BaseFaqBottomSheetDialogVm_Factory;
import e.a.b.h.b.f.SimpleMainFragmentVm;
import e.a.b.h.b.f.SimpleMainFragmentVm_Factory;
import e.a.b.h.b.h.ProfileFillingContainerFragmentVm;
import e.a.b.h.b.h.ProfileFillingContainerFragmentVm_Factory;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentVm;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentVm_Factory;
import e.a.b.h.b.h.m.DocumentsStepFragmentVm;
import e.a.b.h.b.h.m.DocumentsStepFragmentVm_Factory;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentVm;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentVm_Factory;
import e.a.b.h.b.h.o.TransportHealthStepVm;
import e.a.b.h.b.h.o.TransportHealthStepVm_Factory;
import e.a.b.h.b.i.QuestionnaireContainerFragmentVm;
import e.a.b.h.b.i.QuestionnaireContainerFragmentVm_Factory;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm_Factory;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentVm;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentVm_Factory;
import e.c.b.RetrofitFactory;
import e.c.d.b.c.VmFactoryWrapper;
import i.o.v;
import i.o.w;
import j.a.a.a.outline;
import java.util.LinkedHashMap;
import java.util.Map;
import k.a.DoubleCheck;
import k.a.MapProviderFactory;
import m.a.Provider;

public final class DaggerMainComponent implements MainComponent {
    public Provider<e.a.b.h.b.h.o.g> A;
    public Provider<e.a.b.h.b.i.c.m.c> B;
    public Provider<e.a.b.h.b.h.m.d> C;
    public Provider<e.a.b.h.b.h.l.c> D;
    public Provider<e.a.a.m.a.b> E;
    public Provider<e.a.b.h.b.h.n.g> F;
    public Provider<Map<Class<? extends v>, Provider<v>>> G;
    public Provider<e.a.a.i.d.a> H;
    public Provider<w.b> I;
    public final CoreComponent a;
    public Provider<e.a.a.a.e.o.c> b;
    public Provider<e.a.a.a.e.r.c> c;
    public Provider<e.a.b.h.b.g.g> d;

    /* renamed from: e  reason: collision with root package name */
    public Provider<e.a.b.h.b.b> f653e;

    /* renamed from: f  reason: collision with root package name */
    public Provider<e.a.a.j.a.c> f654f;
    public Provider<e.a.a.g.b.b.a.b> g;
    public Provider<e.a.a.g.a.d> h;

    /* renamed from: i  reason: collision with root package name */
    public Provider<e.c.b.h> f655i;

    /* renamed from: j  reason: collision with root package name */
    public Provider<e.a.a.g.a.f.d> f656j;

    /* renamed from: k  reason: collision with root package name */
    public Provider<Context> f657k;

    /* renamed from: l  reason: collision with root package name */
    public Provider<e.a.b.d.a.a.a> f658l;

    /* renamed from: m  reason: collision with root package name */
    public Provider<e.a.b.d.b.a> f659m;

    /* renamed from: n  reason: collision with root package name */
    public Provider<e.a.a.g.a.g.f> f660n;

    /* renamed from: o  reason: collision with root package name */
    public Provider<e.a.b.h.b.d.a> f661o;

    /* renamed from: p  reason: collision with root package name */
    public Provider<e.a.a.a.e.q.b> f662p;

    /* renamed from: q  reason: collision with root package name */
    public Provider<e.a.a.a.c.a.e> f663q;

    /* renamed from: r  reason: collision with root package name */
    public Provider<e.a.a.a.c.b.d> f664r = new ProcessingFragmentVm_Factory(this.f662p, this.b);

    /* renamed from: s  reason: collision with root package name */
    public Provider<e.a.b.h.b.f.h> f665s = new SimpleMainFragmentVm_Factory(this.d);

    /* renamed from: t  reason: collision with root package name */
    public Provider<e.a.b.h.b.i.a> f666t = new QuestionnaireContainerFragmentVm_Factory(this.d);
    public Provider<e.a.b.h.b.e.c> u = new BaseFaqBottomSheetDialogVm_Factory(this.d);
    public Provider<e.a.a.a.e.r.d> v;
    public Provider<e.a.b.h.c.a> w;
    public Provider<e.a.b.h.c.b> x;
    public Provider<e.a.b.h.b.h.g> y;
    public Provider<e.a.b.h.b.i.c.e> z;

    public static class b implements Provider<e.a.a.g.a.g.f> {
        public final CoreComponent a;

        public b(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.g.a.g.IApiConfig, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            IApiConfig e2 = this.a.e();
            j.c.a.a.c.n.c.a((Object) e2, "Cannot return null from a non-@Nullable component method");
            return e2;
        }
    }

    public static class c implements Provider<e.a.a.g.b.b.a.b> {
        public final CoreComponent a;

        public c(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.g.b.b.a.IAuthPrefs, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            IAuthPrefs g = this.a.g();
            j.c.a.a.c.n.c.a((Object) g, "Cannot return null from a non-@Nullable component method");
            return g;
        }
    }

    public static class d implements Provider<e.a.a.j.a.c> {
        public final CoreComponent a;

        public d(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.j.a.IAuthRepository0, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            IAuthRepository0 n2 = this.a.n();
            j.c.a.a.c.n.c.a((Object) n2, "Cannot return null from a non-@Nullable component method");
            return n2;
        }
    }

    public static class e implements Provider<Context> {
        public final CoreComponent a;

        public e(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [android.content.Context, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            Context b = this.a.b();
            j.c.a.a.c.n.c.a((Object) b, "Cannot return null from a non-@Nullable component method");
            return b;
        }
    }

    public static class f implements Provider<e.a.a.a.e.r.c> {
        public final CoreComponent a;

        public f(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.e.r.FragmentCiceroneHolder, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            FragmentCiceroneHolder m2 = this.a.m();
            j.c.a.a.c.n.c.a((Object) m2, "Cannot return null from a non-@Nullable component method");
            return m2;
        }
    }

    public static class g implements Provider<e.a.a.a.e.q.b> {
        public final CoreComponent a;

        public g(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.e.q.IFragmentCoordinator, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            IFragmentCoordinator c = this.a.c();
            j.c.a.a.c.n.c.a((Object) c, "Cannot return null from a non-@Nullable component method");
            return c;
        }
    }

    public static class h implements Provider<e.a.a.m.a.b> {
        public final CoreComponent a;

        public h(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.m.a.GlideHelper, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            GlideHelper o2 = this.a.o();
            j.c.a.a.c.n.c.a((Object) o2, "Cannot return null from a non-@Nullable component method");
            return o2;
        }
    }

    public static class i implements Provider<e.a.a.a.e.r.d> {
        public final CoreComponent a;

        public i(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.e.r.NestedFragmentCiceroneHolder, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            NestedFragmentCiceroneHolder k2 = this.a.k();
            j.c.a.a.c.n.c.a((Object) k2, "Cannot return null from a non-@Nullable component method");
            return k2;
        }
    }

    public static class j implements Provider<e.c.b.h> {
        public final CoreComponent a;

        public j(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.c.b.RetrofitFactory, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            RetrofitFactory i2 = this.a.i();
            j.c.a.a.c.n.c.a((Object) i2, "Cannot return null from a non-@Nullable component method");
            return i2;
        }
    }

    public static class k implements Provider<e.a.a.g.a.d> {
        public final CoreComponent a;

        public k(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.g.a.Session, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            Session h = this.a.h();
            j.c.a.a.c.n.c.a((Object) h, "Cannot return null from a non-@Nullable component method");
            return h;
        }
    }

    public static class l implements Provider<e.a.a.a.e.o.c> {
        public final CoreComponent a;

        public l(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.e.o.ITopLevelCoordinator, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            ITopLevelCoordinator d = this.a.d();
            j.c.a.a.c.n.c.a((Object) d, "Cannot return null from a non-@Nullable component method");
            return d;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<e.a.b.h.b.h.n.ProfileFillingResultFragmentVm>, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [m.a.Provider<e.a.b.h.b.h.n.g>, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public /* synthetic */ DaggerMainComponent(MainModule mainModule, AddProfileStepsNavigationModule addProfileStepsNavigationModule, CoreComponent coreComponent, a aVar) {
        MainModule mainModule2 = mainModule;
        AddProfileStepsNavigationModule addProfileStepsNavigationModule2 = addProfileStepsNavigationModule;
        CoreComponent coreComponent2 = coreComponent;
        this.a = coreComponent2;
        this.b = new l(coreComponent2);
        f fVar = new f(coreComponent2);
        this.c = fVar;
        Provider<e.a.b.h.b.g.g> a2 = DoubleCheck.a(new MainModule_ProvideMainCoordinatorFactory(mainModule2, fVar));
        this.d = a2;
        this.f653e = new MainActivityVm_Factory(this.b, a2);
        this.f654f = new d(coreComponent2);
        this.g = new c(coreComponent2);
        this.h = new k(coreComponent2);
        j jVar = new j(coreComponent2);
        this.f655i = jVar;
        this.f656j = DoubleCheck.a(new MainModule_ProvideEsiaProfileApiServiceFactory(mainModule2, jVar));
        this.f657k = new e(coreComponent2);
        Provider<e.a.b.d.a.a.a> a3 = DoubleCheck.a(new MainModule_ProvideProfileDataApiServiceFactory(mainModule2, this.f655i));
        this.f658l = a3;
        this.f659m = DoubleCheck.a(new MainModule_ProvideProfileRepositoryFactory(mainModule2, this.f656j, this.f657k, a3));
        b bVar = new b(coreComponent2);
        this.f660n = bVar;
        this.f661o = new AuthFragmentVm_Factory(this.f654f, this.g, this.h, this.f659m, this.d, this.b, bVar);
        g gVar = new g(coreComponent2);
        this.f662p = gVar;
        this.f663q = new ErrorFragmentVm_Factory(this.f657k, gVar, this.b);
        this.v = new i(coreComponent2);
        Provider<e.a.b.h.c.a> a4 = DoubleCheck.a(new e.a.b.f.c.c(addProfileStepsNavigationModule2));
        this.w = a4;
        e.a.b.f.c.b bVar2 = new e.a.b.f.c.b(addProfileStepsNavigationModule2, this.v, a4);
        this.x = bVar2;
        this.y = new ProfileFillingContainerFragmentVm_Factory(this.d, bVar2, this.f659m);
        this.z = new FillingProfileFullNamePageFragmentVm_Factory(this.f662p, this.d, this.b, this.f659m, this.g);
        this.A = new TransportHealthStepVm_Factory(this.d, this.f659m);
        this.B = new InformedConfirmationFragmentVm_Factory(this.f659m, this.d);
        this.C = new DocumentsStepFragmentVm_Factory(this.d, this.f659m, this.b, this.g);
        this.D = new ProfileFillingAddChildFragmentVm_Factory(this.f662p, this.d, this.f659m);
        h hVar = new h(coreComponent2);
        this.E = hVar;
        this.F = new ProfileFillingResultFragmentVm_Factory(this.f659m, hVar, this.g, this.d);
        MapProviderFactory.b a5 = MapProviderFactory.a(14);
        Class<MainActivityVm> cls = MainActivityVm.class;
        Provider<e.a.b.h.b.b> provider = this.f653e;
        outline.a(cls, "key", provider, "provider", a5.a, cls, provider);
        Class<AuthFragmentVm> cls2 = AuthFragmentVm.class;
        Provider<e.a.b.h.b.d.a> provider2 = this.f661o;
        String str = "provider";
        Class<AuthFragmentVm> cls3 = cls2;
        String str2 = "key";
        outline.a(cls2, "key", provider2, "provider", a5.a, cls3, provider2);
        Class<ErrorFragmentVm> cls4 = ErrorFragmentVm.class;
        Provider<e.a.a.a.c.a.e> provider3 = this.f663q;
        String str3 = str2;
        String str4 = str;
        outline.a(cls4, str3, provider3, str4, a5.a, cls4, provider3);
        Class<ProcessingFragmentVm> cls5 = ProcessingFragmentVm.class;
        Provider<e.a.a.a.c.b.d> provider4 = this.f664r;
        outline.a(cls5, str3, provider4, str4, a5.a, cls5, provider4);
        Class<SimpleMainFragmentVm> cls6 = SimpleMainFragmentVm.class;
        Provider<e.a.b.h.b.f.h> provider5 = this.f665s;
        outline.a(cls6, str3, provider5, str4, a5.a, cls6, provider5);
        Class<QuestionnaireContainerFragmentVm> cls7 = QuestionnaireContainerFragmentVm.class;
        Provider<e.a.b.h.b.i.a> provider6 = this.f666t;
        outline.a(cls7, str3, provider6, str4, a5.a, cls7, provider6);
        Class<BaseFaqBottomSheetDialogVm> cls8 = BaseFaqBottomSheetDialogVm.class;
        Provider<e.a.b.h.b.e.c> provider7 = this.u;
        outline.a(cls8, str3, provider7, str4, a5.a, cls8, provider7);
        Class<ProfileFillingContainerFragmentVm> cls9 = ProfileFillingContainerFragmentVm.class;
        Provider<e.a.b.h.b.h.g> provider8 = this.y;
        outline.a(cls9, str3, provider8, str4, a5.a, cls9, provider8);
        Class<FillingProfileFullNamePageFragmentVm> cls10 = FillingProfileFullNamePageFragmentVm.class;
        Provider<e.a.b.h.b.i.c.e> provider9 = this.z;
        outline.a(cls10, str3, provider9, str4, a5.a, cls10, provider9);
        Class<TransportHealthStepVm> cls11 = TransportHealthStepVm.class;
        Provider<e.a.b.h.b.h.o.g> provider10 = this.A;
        outline.a(cls11, str3, provider10, str4, a5.a, cls11, provider10);
        Class<InformedConfirmationFragmentVm> cls12 = InformedConfirmationFragmentVm.class;
        Provider<e.a.b.h.b.i.c.m.c> provider11 = this.B;
        outline.a(cls12, str3, provider11, str4, a5.a, cls12, provider11);
        Class<DocumentsStepFragmentVm> cls13 = DocumentsStepFragmentVm.class;
        Provider<e.a.b.h.b.h.m.d> provider12 = this.C;
        outline.a(cls13, str3, provider12, str4, a5.a, cls13, provider12);
        Class<ProfileFillingAddChildFragmentVm> cls14 = ProfileFillingAddChildFragmentVm.class;
        Provider<e.a.b.h.b.h.l.c> provider13 = this.D;
        outline.a(cls14, str3, provider13, str4, a5.a, cls14, provider13);
        Class<ProfileFillingResultFragmentVm> cls15 = ProfileFillingResultFragmentVm.class;
        Provider<e.a.b.h.b.h.n.g> provider14 = this.F;
        LinkedHashMap<K, Provider<V>> linkedHashMap = a5.a;
        j.c.a.a.c.n.c.a((Object) cls15, str2);
        j.c.a.a.c.n.c.a((Object) provider14, str);
        linkedHashMap.put(cls15, provider14);
        MapProviderFactory a6 = a5.a();
        this.G = a6;
        VmFactory_Factory vmFactory_Factory = new VmFactory_Factory(a6);
        this.H = vmFactory_Factory;
        this.I = DoubleCheck.a(vmFactory_Factory);
    }

    public void a(VmFactoryWrapper vmFactoryWrapper) {
        vmFactoryWrapper.a = this.I.get();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.FragmentCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.AppCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public void a(NavigationWrapper navigationWrapper) {
        FragmentCiceroneHolder m2 = this.a.m();
        j.c.a.a.c.n.c.a((Object) m2, "Cannot return null from a non-@Nullable component method");
        navigationWrapper.a = m2;
        AppCiceroneHolder a2 = this.a.a();
        j.c.a.a.c.n.c.a((Object) a2, "Cannot return null from a non-@Nullable component method");
        navigationWrapper.b = a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.NestedFragmentCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public void a(NavigationWrapper0 navigationWrapper0) {
        NestedFragmentCiceroneHolder k2 = this.a.k();
        j.c.a.a.c.n.c.a((Object) k2, "Cannot return null from a non-@Nullable component method");
        navigationWrapper0.a = k2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.h.IDeeplinkManager, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public void a(DeeplinkManagerWrapper deeplinkManagerWrapper) {
        IDeeplinkManager j2 = this.a.j();
        j.c.a.a.c.n.c.a((Object) j2, "Cannot return null from a non-@Nullable component method");
        deeplinkManagerWrapper.a = j2;
    }
}
