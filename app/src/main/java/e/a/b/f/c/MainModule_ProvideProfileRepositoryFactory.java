package e.a.b.f.c;

import android.content.Context;
import e.a.a.g.a.f.EsiaProfileApiService;
import e.a.a.g.a.f.d;
import e.a.b.d.a.a.ProfileDataApiService;
import e.a.b.d.b.IProfileRepository0;
import e.a.b.d.b.a;
import j.c.a.a.c.n.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

public final class MainModule_ProvideProfileRepositoryFactory implements Factory<a> {
    public final MainModule a;
    public final Provider<d> b;
    public final Provider<Context> c;
    public final Provider<e.a.b.d.a.a.a> d;

    public MainModule_ProvideProfileRepositoryFactory(l lVar, Provider<d> provider, Provider<Context> provider2, Provider<e.a.b.d.a.a.a> provider3) {
        this.a = lVar;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.d.b.IProfileRepository0, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        MainModule mainModule = this.a;
        EsiaProfileApiService esiaProfileApiService = this.b.get();
        Context context = this.c.get();
        ProfileDataApiService profileDataApiService = this.d.get();
        if (mainModule == null) {
            throw null;
        } else if (esiaProfileApiService == null) {
            Intrinsics.a("esiaProfileApiService");
            throw null;
        } else if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (profileDataApiService != null) {
            IProfileRepository0 iProfileRepository0 = new IProfileRepository0(esiaProfileApiService, context, profileDataApiService);
            c.a((Object) iProfileRepository0, "Cannot return null from a non-@Nullable @Provides method");
            return iProfileRepository0;
        } else {
            Intrinsics.a("profileDataApiService");
            throw null;
        }
    }
}
