package e.a.b.h.b.g;

import e.a.a.a.e.BaseScreens1;
import e.a.a.a.e.p.DeeplinkDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: Screens.kt */
public final class Screens5 extends BaseScreens1 {

    /* renamed from: e  reason: collision with root package name */
    public final Class<?> f677e;

    /* renamed from: f  reason: collision with root package name */
    public final DeeplinkDto f678f;

    public Screens5() {
        this(null, 1);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Screens5(e.a.a.a.e.p.DeeplinkDto r3, int r4) {
        /*
            r2 = this;
            r0 = 1
            r4 = r4 & r0
            r1 = 0
            if (r4 == 0) goto L_0x0006
            r3 = r1
        L_0x0006:
            r2.<init>(r1, r3, r0)
            r2.f678f = r3
            java.lang.Class<ru.covid19.droid.presentation.main.MainActivity> r3 = ru.covid19.droid.presentation.main.MainActivity.class
            r2.f677e = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.b.h.b.g.Screens5.<init>(e.a.a.a.e.p.DeeplinkDto, int):void");
    }

    public Class<?> a() {
        return this.f677e;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof Screens5) && Intrinsics.a(this.f678f, ((Screens5) obj).f678f);
        }
        return true;
    }

    public int hashCode() {
        DeeplinkDto deeplinkDto = this.f678f;
        if (deeplinkDto != null) {
            return deeplinkDto.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a = outline.a("MainActivityScreen(deeplinkDto=");
        a.append(this.f678f);
        a.append(")");
        return a.toString();
    }
}
