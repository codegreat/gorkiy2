package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.profileFilling.documentsStep.DocumentsStepFragment;

/* compiled from: Screens.kt */
public final class Screens8 extends BaseScreens2 {
    public static final Screens8 d = new Screens8();

    public Screens8() {
        super(null, 1);
    }

    public Fragment a() {
        return new DocumentsStepFragment();
    }
}
