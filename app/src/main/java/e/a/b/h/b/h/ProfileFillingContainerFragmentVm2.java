package e.a.b.h.b.h;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState;
import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState1;
import e.c.c.BaseMviVm0;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.t.Function;
import l.b.w.Schedulers;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingContainerFragmentVm.kt */
public final class ProfileFillingContainerFragmentVm2<T, R> implements Function<T, ObservableSource<? extends R>> {
    public final /* synthetic */ ProfileFillingContainerFragmentVm a;

    public ProfileFillingContainerFragmentVm2(ProfileFillingContainerFragmentVm profileFillingContainerFragmentVm) {
        this.a = profileFillingContainerFragmentVm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Object a(Object obj) {
        if (((ProfileFillingContainerFragmentViewState.c) obj) != null) {
            ProfileFillingContainerFragmentVm profileFillingContainerFragmentVm = this.a;
            Observable c = profileFillingContainerFragmentVm.f687l.d().a(Schedulers.b).a(new ProfileFillingContainerFragmentVm0(this)).b().c((Function) ProfileFillingContainerFragmentVm1.a);
            Intrinsics.a((Object) c, "profileRepository.postPr…aseResult>> { IgnoreLce }");
            return BaseDpFragmentVm1.a(profileFillingContainerFragmentVm, c, ProfileFillingContainerFragmentViewState1.c.class, ProfileFillingContainerFragmentViewState.c.b, null, null, null, 28, null).c(new BaseMviVm0.c());
        }
        Intrinsics.a("it");
        throw null;
    }
}
