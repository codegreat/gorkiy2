package e.a.b.h.b.h;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.c.d.a.ViewAction;
import e.c.d.a.ViewState;
import j.c.a.a.c.n.c;
import l.b.Single;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: BaseStepValidatedFragmentVm.kt */
public abstract class BaseStepValidatedFragmentVm<VS> extends BaseDpFragmentVm1<VS> {

    /* renamed from: j  reason: collision with root package name */
    public final ViewAction<g> f679j;

    /* renamed from: k  reason: collision with root package name */
    public final ViewAction<g> f680k;

    /* renamed from: l  reason: collision with root package name */
    public final ViewState<Boolean> f681l;

    /* renamed from: m  reason: collision with root package name */
    public final ViewState<Boolean> f682m;

    /* compiled from: BaseStepValidatedFragmentVm.kt */
    public static final class a extends j implements Functions0<g, g> {
        public final /* synthetic */ BaseStepValidatedFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseStepValidatedFragmentVm baseStepValidatedFragmentVm) {
            super(1);
            this.c = baseStepValidatedFragmentVm;
        }

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                BaseStepValidatedFragmentVm baseStepValidatedFragmentVm = this.c;
                baseStepValidatedFragmentVm.f681l.a(Boolean.valueOf(baseStepValidatedFragmentVm.h()));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseStepValidatedFragmentVm(IFragmentCoordinator iFragmentCoordinator) {
        super(iFragmentCoordinator);
        if (iFragmentCoordinator != null) {
            this.f679j = new ViewAction<>();
            this.f680k = new ViewAction<>();
            this.f681l = new ViewState<>(null, 1);
            this.f682m = new ViewState<>(null, 1);
            return;
        }
        Intrinsics.a("fragmentCoordinator");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [e.c.d.c.BaseVm, e.a.b.h.b.h.BaseStepValidatedFragmentVm] */
    public void c() {
        c.a(this.f679j.a(new a(this)), this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Single<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<Boolean> g() {
        Single<Boolean> a2 = Single.a((Object) true);
        Intrinsics.a((Object) a2, "Single.just(true)");
        return a2;
    }

    public abstract boolean h();
}
