package e.a.b.h.b.i.c;

import android.net.Uri;
import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.ProfileFillingBottomSheetDto;
import ru.covid19.droid.data.model.profileData.Passenger;

/* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
public final class FillingProfileFullNamePageFragmentViewState {
    public final ProfileFillingBottomSheetDto a;
    public Uri b;
    public Passenger c;
    public String d;

    public FillingProfileFullNamePageFragmentViewState() {
        this(null, null, null, null, 15);
    }

    public FillingProfileFullNamePageFragmentViewState(ProfileFillingBottomSheetDto profileFillingBottomSheetDto, Uri uri, Passenger passenger, String str) {
        if (profileFillingBottomSheetDto == null) {
            Intrinsics.a("bottomSheetDto");
            throw null;
        } else if (passenger == null) {
            Intrinsics.a("passenger");
            throw null;
        } else if (str != null) {
            this.a = profileFillingBottomSheetDto;
            this.b = uri;
            this.c = passenger;
            this.d = str;
        } else {
            Intrinsics.a("tmpPlacementAddress");
            throw null;
        }
    }

    public static /* synthetic */ FillingProfileFullNamePageFragmentViewState a(FillingProfileFullNamePageFragmentViewState fillingProfileFullNamePageFragmentViewState, ProfileFillingBottomSheetDto profileFillingBottomSheetDto, Uri uri, Passenger passenger, String str, int i2) {
        if ((i2 & 1) != 0) {
            profileFillingBottomSheetDto = fillingProfileFullNamePageFragmentViewState.a;
        }
        if ((i2 & 2) != 0) {
            uri = fillingProfileFullNamePageFragmentViewState.b;
        }
        if ((i2 & 4) != 0) {
            passenger = fillingProfileFullNamePageFragmentViewState.c;
        }
        if ((i2 & 8) != 0) {
            str = fillingProfileFullNamePageFragmentViewState.d;
        }
        if (fillingProfileFullNamePageFragmentViewState == null) {
            throw null;
        } else if (profileFillingBottomSheetDto == null) {
            Intrinsics.a("bottomSheetDto");
            throw null;
        } else if (passenger == null) {
            Intrinsics.a("passenger");
            throw null;
        } else if (str != null) {
            return new FillingProfileFullNamePageFragmentViewState(profileFillingBottomSheetDto, uri, passenger, str);
        } else {
            Intrinsics.a("tmpPlacementAddress");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FillingProfileFullNamePageFragmentViewState)) {
            return false;
        }
        FillingProfileFullNamePageFragmentViewState fillingProfileFullNamePageFragmentViewState = (FillingProfileFullNamePageFragmentViewState) obj;
        return Intrinsics.a(this.a, fillingProfileFullNamePageFragmentViewState.a) && Intrinsics.a(this.b, fillingProfileFullNamePageFragmentViewState.b) && Intrinsics.a(this.c, fillingProfileFullNamePageFragmentViewState.c) && Intrinsics.a(this.d, fillingProfileFullNamePageFragmentViewState.d);
    }

    public int hashCode() {
        ProfileFillingBottomSheetDto profileFillingBottomSheetDto = this.a;
        int i2 = 0;
        int hashCode = (profileFillingBottomSheetDto != null ? profileFillingBottomSheetDto.hashCode() : 0) * 31;
        Uri uri = this.b;
        int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
        Passenger passenger = this.c;
        int hashCode3 = (hashCode2 + (passenger != null ? passenger.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode3 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("FillingProfileFullNamePageFragmentViewState(bottomSheetDto=");
        a2.append(this.a);
        a2.append(", photoUri=");
        a2.append(this.b);
        a2.append(", passenger=");
        a2.append(this.c);
        a2.append(", tmpPlacementAddress=");
        return outline.a(a2, this.d, ")");
    }

    public /* synthetic */ FillingProfileFullNamePageFragmentViewState(ProfileFillingBottomSheetDto profileFillingBottomSheetDto, Uri uri, Passenger passenger, String str, int i2) {
        FillingProfileFullNamePageFragmentViewState fillingProfileFullNamePageFragmentViewState;
        String str2;
        ProfileFillingBottomSheetDto profileFillingBottomSheetDto2 = (i2 & 1) != 0 ? new ProfileFillingBottomSheetDto(null, null, null, null, 15, null) : profileFillingBottomSheetDto;
        Uri uri2 = (i2 & 2) != 0 ? null : uri;
        Passenger passenger2 = (i2 & 4) != 0 ? new Passenger(null, null, null, null, null, null, null, null, null, null, null, null, null, 8191, null) : passenger;
        if ((i2 & 8) != 0) {
            str2 = "";
            fillingProfileFullNamePageFragmentViewState = this;
        } else {
            fillingProfileFullNamePageFragmentViewState = this;
            str2 = str;
        }
        new FillingProfileFullNamePageFragmentViewState(profileFillingBottomSheetDto2, uri2, passenger2, str2);
    }
}
