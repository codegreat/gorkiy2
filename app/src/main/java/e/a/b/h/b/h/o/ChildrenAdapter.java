package e.a.b.h.b.h.o;

import android.view.View;
import e.a.b.h.b.h.o.ChildrenAdapter0;
import ru.covid19.droid.data.model.profileData.Children;

/* compiled from: ChildrenAdapter.kt */
public final class ChildrenAdapter implements View.OnClickListener {
    public final /* synthetic */ ChildrenAdapter0.a b;
    public final /* synthetic */ Children c;

    public ChildrenAdapter(ChildrenAdapter0.a aVar, Children children) {
        this.b = aVar;
        this.c = children;
    }

    public final void onClick(View view) {
        this.b.u.d.a((Boolean) this.c);
    }
}
