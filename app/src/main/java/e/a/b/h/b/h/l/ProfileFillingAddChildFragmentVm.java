package e.a.b.h.b.h.l;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState0;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState1;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState2;
import e.a.b.h.b.h.l.a;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState0;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState1;
import e.c.c.BaseViewState4;
import java.util.ArrayList;
import java.util.List;
import l.b.Observable;
import l.b.s.Disposable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.p.KDeclarationContainer;
import n.r.Indent;
import ru.covid19.droid.data.model.profileData.Children;
import ru.covid19.droid.data.model.profileData.Document;
import ru.covid19.droid.data.model.profileData.Document0;
import ru.covid19.droid.data.model.profileData.ProfileData;
import ru.covid19.droid.data.model.profileData.ProfileData0;

/* compiled from: ProfileFillingAddChildFragmentVm.kt */
public final class ProfileFillingAddChildFragmentVm extends BaseDpFragmentVm1<b> {

    /* renamed from: j  reason: collision with root package name */
    public final IFragmentCoordinator f690j;

    /* renamed from: k  reason: collision with root package name */
    public final IProfileRepository f691k;

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class a extends n.n.c.j implements Functions0<a.p, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.p pVar = (ProfileFillingAddChildFragmentViewState0.p) obj;
            if (pVar != null) {
                this.c.f690j.a(new BottomSheetSelectorNavigationDto0(pVar.a, pVar.b));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class b<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public b(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            String str;
            ProfileFillingAddChildFragmentViewState0.b bVar = (ProfileFillingAddChildFragmentViewState0.b) obj;
            if (bVar != null) {
                Children children = ProfileFillingAddChildFragmentVm.b(this.a).c;
                Document document = ProfileFillingAddChildFragmentVm.b(this.a).c.getDocument();
                Document0 document0 = bVar.a;
                if (document0 == Document0.foreignCitizenDoc) {
                    str = "";
                } else {
                    str = ProfileFillingAddChildFragmentVm.b(this.a).c.getDocument().getSeries();
                }
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(children, null, 0, null, Document.copy$default(document, null, str, document0, 1, null), null, null, null, null, null, null, 1015, null), true));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class c<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public c(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.i iVar = (ProfileFillingAddChildFragmentViewState0.i) obj;
            if (iVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, null, Document.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c.getDocument(), null, iVar.a, null, 5, null), null, null, null, null, null, null, 1015, null), false, 2));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class d<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public d(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.h hVar = (ProfileFillingAddChildFragmentViewState0.h) obj;
            if (hVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, null, Document.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c.getDocument(), hVar.a, null, null, 6, null), null, null, null, null, null, null, 1015, null), false, 2));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class e<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public e(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.g gVar = (ProfileFillingAddChildFragmentViewState0.g) obj;
            if (gVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, null, null, null, null, null, null, gVar.a, null, 767, null), false, 2));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class f extends n.n.c.j implements Functions0<a.r, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            if (((ProfileFillingAddChildFragmentViewState0.r) obj) != null) {
                if (ProfileFillingAddChildFragmentVm.c(this.c)) {
                    ProfileData f2 = this.c.f691k.f();
                    List<Children> children = f2.getChildren();
                    Children children2 = ProfileFillingAddChildFragmentVm.b(this.c).c;
                    StringBuilder sb = new StringBuilder();
                    sb.append(ProfileFillingAddChildFragmentVm.b(this.c).f688e);
                    sb.append(' ' + ProfileFillingAddChildFragmentVm.b(this.c).d);
                    String str = ProfileFillingAddChildFragmentVm.b(this.c).f689f;
                    if (str != null) {
                        sb.append(' ' + str);
                    }
                    String sb2 = sb.toString();
                    Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
                    Children copy$default = Children.copy$default(children2, null, 0, null, null, sb2, null, null, null, null, null, 1007, null);
                    if (children != null) {
                        ArrayList arrayList = new ArrayList(children.size() + 1);
                        arrayList.addAll(children);
                        arrayList.add(copy$default);
                        f2.setChildren(arrayList);
                        Collections.a(this.c.f690j, (Integer) null, (Object) null, 3, (Object) null);
                    } else {
                        Intrinsics.a("$this$plus");
                        throw null;
                    }
                } else {
                    this.c.f721e.a((e.c.c.l) ProfileFillingAddChildFragmentViewState2.a.a);
                }
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class g extends n.n.c.j implements Functions0<a.s, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.s sVar = (ProfileFillingAddChildFragmentViewState0.s) obj;
            if (sVar != null) {
                this.c.f690j.a(new BottomSheetSelectorNavigationDto0(sVar.a, sVar.b));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class h extends n.n.c.j implements Functions0<a.n, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.n nVar = (ProfileFillingAddChildFragmentViewState0.n) obj;
            if (nVar != null) {
                ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm = this.c;
                profileFillingAddChildFragmentVm.d.a(ProfileFillingAddChildFragmentViewState.a(ProfileFillingAddChildFragmentVm.b(profileFillingAddChildFragmentVm), null, false, null, null, nVar.a, null, 47));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class i extends n.n.c.j implements Functions0<a.k, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.k kVar = (ProfileFillingAddChildFragmentViewState0.k) obj;
            if (kVar != null) {
                ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm = this.c;
                profileFillingAddChildFragmentVm.d.a(ProfileFillingAddChildFragmentViewState.a(ProfileFillingAddChildFragmentVm.b(profileFillingAddChildFragmentVm), null, false, null, kVar.a, null, null, 55));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class j extends n.n.c.j implements Functions0<a.l, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.l lVar = (ProfileFillingAddChildFragmentViewState0.l) obj;
            if (lVar != null) {
                ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm = this.c;
                profileFillingAddChildFragmentVm.d.a(ProfileFillingAddChildFragmentViewState.a(ProfileFillingAddChildFragmentVm.b(profileFillingAddChildFragmentVm), null, false, null, null, null, lVar.a, 31));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class k<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public k(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.f fVar = (ProfileFillingAddChildFragmentViewState0.f) obj;
            if (fVar != null) {
                Children children = ProfileFillingAddChildFragmentVm.b(this.a).c;
                Integer a2 = Collections.a(fVar.a, (String) null, 1);
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(children, null, a2 != null ? a2.intValue() : 0, null, null, null, null, null, null, null, null, 1021, null), false, 2));
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class l<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public l(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.m mVar = (ProfileFillingAddChildFragmentViewState0.m) obj;
            if (mVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, null, null, null, null, null, mVar.a, null, null, 895, null), false, 2));
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class m<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public m(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.j jVar = (ProfileFillingAddChildFragmentViewState0.j) obj;
            if (jVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, null, null, null, null, jVar.a, null, null, null, 959, null), false, 2));
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class n<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public n(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.c cVar = (ProfileFillingAddChildFragmentViewState0.c) obj;
            if (cVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, null, null, null, cVar.a, null, null, null, null, 991, null), false, 2));
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class o<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public o(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.a aVar = (ProfileFillingAddChildFragmentViewState0.a) obj;
            if (aVar != null) {
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(ProfileFillingAddChildFragmentVm.b(this.a).c, null, 0, aVar.a.getCode(), null, null, null, null, null, null, aVar.a.getName(), 507, null), false, 2));
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class p extends n.n.c.j implements Functions0<a.q, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            if (((ProfileFillingAddChildFragmentViewState0.q) obj) != null) {
                IFragmentCoordinator iFragmentCoordinator = this.c.f690j;
                String genderBottomSheetTitle = ProfileFillingAddChildFragmentVm.b(this.c).a.getGenderBottomSheetTitle();
                FillingProfileFullNamePageFragmentViewState0[] fillingProfileFullNamePageFragmentViewState0Arr = new FillingProfileFullNamePageFragmentViewState0[2];
                boolean z = false;
                fillingProfileFullNamePageFragmentViewState0Arr[0] = new FillingProfileFullNamePageFragmentViewState0(ProfileData0.male, ProfileFillingAddChildFragmentVm.b(this.c).a.getGenderMaleLabel(), ProfileFillingAddChildFragmentVm.b(this.c).c.getGender() == ProfileData0.male);
                ProfileData0 profileData0 = ProfileData0.female;
                String genderFemaleLabel = ProfileFillingAddChildFragmentVm.b(this.c).a.getGenderFemaleLabel();
                if (ProfileFillingAddChildFragmentVm.b(this.c).c.getGender() == ProfileData0.female) {
                    z = true;
                }
                fillingProfileFullNamePageFragmentViewState0Arr[1] = new FillingProfileFullNamePageFragmentViewState0(profileData0, genderFemaleLabel, z);
                iFragmentCoordinator.a(new BottomSheetSelectorNavigationDto0(genderBottomSheetTitle, Collections.a((Object[]) fillingProfileFullNamePageFragmentViewState0Arr)));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class q extends n.n.c.j implements Functions0<a.o, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1);
            this.c = profileFillingAddChildFragmentVm;
        }

        public Object a(Object obj) {
            ProfileFillingAddChildFragmentViewState0.o oVar = (ProfileFillingAddChildFragmentViewState0.o) obj;
            if (oVar != null) {
                this.c.f690j.a(oVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final /* synthetic */ class r extends n.n.c.h implements Functions0<Observable<a.d>, Observable<BaseMviVm0<? extends e.c.c.k>>> {
        public r(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            super(1, profileFillingAddChildFragmentVm);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [e.a.b.h.b.h.l.ProfileFillingAddChildFragmentVm$r, n.n.c.CallableReference] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return ProfileFillingAddChildFragmentVm.a((ProfileFillingAddChildFragmentVm) this.c, observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "selectCitizenshipEventProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(ProfileFillingAddChildFragmentVm.class);
        }

        public final String i() {
            return "selectCitizenshipEventProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class s<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

        public s(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
            this.a = profileFillingAddChildFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            String str;
            String str2;
            ProfileFillingAddChildFragmentViewState0.e eVar = (ProfileFillingAddChildFragmentViewState0.e) obj;
            if (eVar != null) {
                Children children = ProfileFillingAddChildFragmentVm.b(this.a).c;
                Boolean bool = eVar.a;
                if (ProfileFillingAddChildFragmentVm.b(this.a).c.getAddressSimilarWithPassenger() != null || Intrinsics.a((Object) eVar.a, (Object) true)) {
                    str = "";
                } else {
                    str = ProfileFillingAddChildFragmentVm.b(this.a).c.getPlacementAddress();
                }
                if (ProfileFillingAddChildFragmentVm.b(this.a).c.getAddressSimilarWithPassenger() != null || Intrinsics.a((Object) eVar.a, (Object) true)) {
                    str2 = "";
                } else {
                    str2 = ProfileFillingAddChildFragmentVm.b(this.a).c.getRegistrationAddress();
                }
                return new BaseMviVm0.a(new ProfileFillingAddChildFragmentViewState1.a(Children.copy$default(children, bool, 0, null, null, null, null, str, str2, null, null, 830, null), true));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentVm.kt */
    public static final class t<T, R> implements Function<T, R> {
        public static final t a = new t();

        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                return new BaseMviVm0.a(ProfileFillingAddChildFragmentViewState1.b.a);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileFillingAddChildFragmentVm(IFragmentCoordinator iFragmentCoordinator, IMainCoordinator iMainCoordinator, IProfileRepository iProfileRepository) {
        super(iMainCoordinator);
        if (iFragmentCoordinator == null) {
            Intrinsics.a("fragmentCoordinator");
            throw null;
        } else if (iMainCoordinator == null) {
            Intrinsics.a("mainCoordinator");
            throw null;
        } else if (iProfileRepository != null) {
            this.f690j = iFragmentCoordinator;
            this.f691k = iProfileRepository;
        } else {
            Intrinsics.a("profileRepository");
            throw null;
        }
    }

    public static final /* synthetic */ ProfileFillingAddChildFragmentViewState b(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
        return (ProfileFillingAddChildFragmentViewState) profileFillingAddChildFragmentVm.e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Boolean, boolean]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public static final /* synthetic */ boolean c(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
        Children children = ((ProfileFillingAddChildFragmentViewState) profileFillingAddChildFragmentVm.e()).c;
        if (children.getAddressSimilarWithPassenger() == null || children.getSympthoms() == null) {
            return false;
        }
        String str = ((ProfileFillingAddChildFragmentViewState) profileFillingAddChildFragmentVm.e()).d;
        if (str == null || Indent.b(str)) {
            return false;
        }
        String str2 = ((ProfileFillingAddChildFragmentViewState) profileFillingAddChildFragmentVm.e()).f688e;
        if ((str2 == null || Indent.b(str2)) || children.getBirthDate() == 0 || children.getGender() == null || Indent.b(children.getCitizenship())) {
            return false;
        }
        if (((!Intrinsics.a((Object) children.getAddressSimilarWithPassenger(), (Object) true)) && (Indent.b(children.getPlacementAddress()) || Indent.b(children.getRegistrationAddress()))) || children.getDocument().getType() == null || Indent.b(children.getDocument().getNumber())) {
            return false;
        }
        if (!Indent.b(children.getDocument().getSeries()) || children.getDocument().getType() == Document0.foreignCitizenDoc) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS
     arg types: [e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState, e.c.c.BaseViewState1]
     candidates:
      e.a.b.h.b.h.l.ProfileFillingAddChildFragmentVm.a(e.a.b.h.b.h.l.ProfileFillingAddChildFragmentVm, l.b.Observable):l.b.Observable
      e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS */
    public Object a(Object obj, BaseViewState1 baseViewState1) {
        ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState = (ProfileFillingAddChildFragmentViewState) obj;
        if (profileFillingAddChildFragmentViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (Intrinsics.a(baseViewState1, ProfileFillingAddChildFragmentViewState1.b.a)) {
            return ProfileFillingAddChildFragmentViewState.a(profileFillingAddChildFragmentViewState, null, true, null, null, null, null, 61);
        } else {
            if (baseViewState1 instanceof ProfileFillingAddChildFragmentViewState1.a) {
                ProfileFillingAddChildFragmentViewState1.a aVar = (ProfileFillingAddChildFragmentViewState1.a) baseViewState1;
                return ProfileFillingAddChildFragmentViewState.a(profileFillingAddChildFragmentViewState, null, aVar.b, aVar.a, null, null, null, 57);
            }
            super.a((Object) profileFillingAddChildFragmentViewState, (e.c.c.k) baseViewState1);
            return profileFillingAddChildFragmentViewState;
        }
    }

    public Object d() {
        return new ProfileFillingAddChildFragmentViewState(null, false, null, null, null, null, 63);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends e.c.c.k>> b(Observable<e.c.c.p> observable) {
        if (observable != null) {
            Observable<R> c2 = observable.c((Function) t.a);
            Intrinsics.a((Object) c2, "observable.map {\n       …ult.InitResult)\n        }");
            return c2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(List<? extends Observable<? extends e.c.c.b>> list) {
        if (list != null) {
            super.a(list);
            Disposable a2 = this.f690j.f().a(new ProfileFillingAddChildFragmentVm3(this));
            Intrinsics.a((Object) a2, "fragmentCoordinator.addB…          }\n            }");
            j.c.a.a.c.n.c.a(a2, this.f722f);
            return;
        }
        Intrinsics.a("es");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends e.c.c.k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(ProfileFillingAddChildFragmentViewState0.f.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(ProfileFillingAddChildFragmentViewState0.m.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(ProfileFillingAddChildFragmentViewState0.j.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(ProfileFillingAddChildFragmentViewState0.c.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(ProfileFillingAddChildFragmentViewState0.a.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<U> a7 = observable.a(ProfileFillingAddChildFragmentViewState0.q.class);
            Intrinsics.a((Object) a7, "ofType(R::class.java)");
            Observable<U> a8 = observable.a(ProfileFillingAddChildFragmentViewState0.o.class);
            Intrinsics.a((Object) a8, "ofType(R::class.java)");
            Observable<U> a9 = observable.a(ProfileFillingAddChildFragmentViewState0.d.class);
            Intrinsics.a((Object) a9, "ofType(R::class.java)");
            Observable<U> a10 = observable.a(ProfileFillingAddChildFragmentViewState0.e.class);
            Intrinsics.a((Object) a10, "ofType(R::class.java)");
            Observable<U> a11 = observable.a(ProfileFillingAddChildFragmentViewState0.p.class);
            Intrinsics.a((Object) a11, "ofType(R::class.java)");
            Observable<U> a12 = observable.a(ProfileFillingAddChildFragmentViewState0.b.class);
            Intrinsics.a((Object) a12, "ofType(R::class.java)");
            Observable<U> a13 = observable.a(ProfileFillingAddChildFragmentViewState0.i.class);
            Intrinsics.a((Object) a13, "ofType(R::class.java)");
            Observable<U> a14 = observable.a(ProfileFillingAddChildFragmentViewState0.h.class);
            Intrinsics.a((Object) a14, "ofType(R::class.java)");
            Observable<U> a15 = observable.a(ProfileFillingAddChildFragmentViewState0.g.class);
            Intrinsics.a((Object) a15, "ofType(R::class.java)");
            Observable<U> a16 = observable.a(ProfileFillingAddChildFragmentViewState0.r.class);
            Intrinsics.a((Object) a16, "ofType(R::class.java)");
            Observable<U> a17 = observable.a(ProfileFillingAddChildFragmentViewState0.s.class);
            Intrinsics.a((Object) a17, "ofType(R::class.java)");
            Observable<U> a18 = observable.a(ProfileFillingAddChildFragmentViewState0.n.class);
            Intrinsics.a((Object) a18, "ofType(R::class.java)");
            Observable<U> a19 = observable.a(ProfileFillingAddChildFragmentViewState0.k.class);
            Intrinsics.a((Object) a19, "ofType(R::class.java)");
            Observable<U> a20 = observable.a(ProfileFillingAddChildFragmentViewState0.l.class);
            Intrinsics.a((Object) a20, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends e.c.c.k>> a21 = Observable.a(super.a(observable), a2.c((Function) new k(this)), a3.c((Function) new l(this)), a4.c((Function) new m(this)), a5.c((Function) new n(this)), a6.c((Function) new o(this)), BaseMviVm.a(a7, new p(this)), BaseMviVm.a(a8, new q(this)), BaseMviVm.b(a9, new r(this)), a10.c((Function) new s(this)), BaseMviVm.a(a11, new a(this)), a12.c((Function) new b(this)), a13.c((Function) new c(this)), a14.c((Function) new d(this)), a15.c((Function) new e(this)), BaseMviVm.a(a16, new f(this)), BaseMviVm.a(a17, new g(this)), BaseMviVm.a(a18, new h(this)), BaseMviVm.a(a19, new i(this)), BaseMviVm.a(a20, new j(this)));
            Intrinsics.a((Object) a21, "Observable.mergeArray(\n …)\n            }\n        )");
            return a21;
        }
        Intrinsics.a("o");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ Observable a(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm, Observable observable) {
        if (profileFillingAddChildFragmentVm != null) {
            Observable c2 = observable.b(new ProfileFillingAddChildFragmentVm0(profileFillingAddChildFragmentVm)).c((Function) new ProfileFillingAddChildFragmentVm1(profileFillingAddChildFragmentVm)).c((Function) ProfileFillingAddChildFragmentVm2.a);
            Intrinsics.a((Object) c2, "observable\n            .…       .map { IgnoreLce }");
            return c2;
        }
        throw null;
    }
}
