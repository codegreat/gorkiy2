package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.b.h.b.e.CannotCallToDoctorBottomDialog;

/* compiled from: Screens.kt */
public final class Screens1 extends BaseScreens2 {
    public static final Screens1 d = new Screens1();

    public Screens1() {
        super(null, 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [androidx.fragment.app.Fragment, e.a.b.h.b.e.CannotCallToDoctorBottomDialog] */
    public Fragment a() {
        return new CannotCallToDoctorBottomDialog();
    }
}
