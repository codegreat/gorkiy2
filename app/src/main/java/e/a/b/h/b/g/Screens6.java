package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.main.SimpleMainFragment;

/* compiled from: Screens.kt */
public final class Screens6 extends BaseScreens2 {
    public static final Screens6 d = new Screens6();

    public Screens6() {
        super(null, 1);
    }

    public Fragment a() {
        return new SimpleMainFragment();
    }
}
