package e.a.b.h.b.h;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.b.h.c.IStepsNestedFragmentCoordinator0;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingContainerFragmentViewState.kt */
public abstract class ProfileFillingContainerFragmentViewState extends BaseViewState0 {

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class a extends ProfileFillingContainerFragmentViewState {
        public final boolean a;

        public a(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && this.a == ((a) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("LoadingStateChangedEvent(isLoading="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class b extends ProfileFillingContainerFragmentViewState {
        public static final b a = new b();

        public b() {
            super(null);
        }
    }

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class c extends ProfileFillingContainerFragmentViewState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new a();
        public static final c b = new c();

        public static class a implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                if (parcel == null) {
                    Intrinsics.a("in");
                    throw null;
                } else if (parcel.readInt() != 0) {
                    return c.b;
                } else {
                    return null;
                }
            }

            public final Object[] newArray(int i2) {
                return new c[i2];
            }
        }

        public c() {
            super(null);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            if (parcel != null) {
                parcel.writeInt(1);
            } else {
                Intrinsics.a("parcel");
                throw null;
            }
        }
    }

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class d extends ProfileFillingContainerFragmentViewState {
        public static final d a = new d();

        public d() {
            super(null);
        }
    }

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class e extends ProfileFillingContainerFragmentViewState {
        public final IStepsNestedFragmentCoordinator0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(IStepsNestedFragmentCoordinator0 iStepsNestedFragmentCoordinator0) {
            super(null);
            if (iStepsNestedFragmentCoordinator0 != null) {
                this.a = iStepsNestedFragmentCoordinator0;
                return;
            }
            Intrinsics.a("stepInfo");
            throw null;
        }
    }

    public /* synthetic */ ProfileFillingContainerFragmentViewState(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
