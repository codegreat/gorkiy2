package e.a.b.h.b.e;

import android.view.View;
import com.minsvyaz.gosuslugi.stopcorona.R;
import java.util.HashMap;

/* compiled from: SymptomsBottomDialog.kt */
public final class SymptomsBottomDialog extends BaseFaqBottomDialog {
    public HashMap o0;

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.o0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public int Q() {
        return R.string.dlg_symptoms_content;
    }

    public int R() {
        return R.string.dlg_symptoms_title;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [androidx.fragment.app.Fragment, e.a.b.h.b.e.SymptomsBottomDialog] */
    public View c(int i2) {
        if (this.o0 == null) {
            this.o0 = new HashMap();
        }
        View view = (View) this.o0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.o0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }
}
