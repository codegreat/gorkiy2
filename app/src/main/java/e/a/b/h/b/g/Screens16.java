package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.b.h.b.e.SymptomsBottomDialog;

/* compiled from: Screens.kt */
public final class Screens16 extends BaseScreens2 {
    public static final Screens16 d = new Screens16();

    public Screens16() {
        super(null, 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [androidx.fragment.app.Fragment, e.a.b.h.b.e.SymptomsBottomDialog] */
    public Fragment a() {
        return new SymptomsBottomDialog();
    }
}
