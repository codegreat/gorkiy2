package e.a.b.h.d;

import e.a.a.a.e.o.ITopLevelCoordinator;
import e.c.d.c.BaseVm;
import n.n.c.Intrinsics;

/* compiled from: SplashActivityVm.kt */
public final class SplashActivityVm extends BaseVm {
    public final ITopLevelCoordinator d;

    public SplashActivityVm(ITopLevelCoordinator iTopLevelCoordinator) {
        if (iTopLevelCoordinator != null) {
            this.d = iTopLevelCoordinator;
        } else {
            Intrinsics.a("topLevelNavigator");
            throw null;
        }
    }

    public void c() {
        this.d.a();
    }
}
