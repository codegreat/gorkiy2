package e.a.b.h.b.h.l;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.a;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState0;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState;
import e.a.b.h.b.h.o.TransportHealthStepViewState;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState0;
import e.c.c.b;
import l.b.t.Consumer;
import ru.covid19.core.data.network.model.CitizenshipResponse0;

/* compiled from: ProfileFillingAddChildFragmentVm.kt */
public final class ProfileFillingAddChildFragmentVm3<T> implements Consumer<a> {
    public final /* synthetic */ ProfileFillingAddChildFragmentVm b;

    public ProfileFillingAddChildFragmentVm3(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
        this.b = profileFillingAddChildFragmentVm;
    }

    public void a(Object obj) {
        BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto = (BottomSheetSelectorNavigationDto) obj;
        if (bottomSheetSelectorNavigationDto instanceof FillingProfileFullNamePageFragmentViewState0) {
            this.b.g.a((b) new ProfileFillingAddChildFragmentViewState0.c(((FillingProfileFullNamePageFragmentViewState0) bottomSheetSelectorNavigationDto).b));
        } else if (bottomSheetSelectorNavigationDto instanceof CitizenshipResponse0) {
            this.b.g.a((b) new ProfileFillingAddChildFragmentViewState0.a(((CitizenshipResponse0) bottomSheetSelectorNavigationDto).getCitizenship()));
        } else if (bottomSheetSelectorNavigationDto instanceof DocumentsStepFragmentViewState) {
            this.b.g.a((b) new ProfileFillingAddChildFragmentViewState0.b(((DocumentsStepFragmentViewState) bottomSheetSelectorNavigationDto).b));
        } else if (bottomSheetSelectorNavigationDto instanceof TransportHealthStepViewState) {
            this.b.g.a((b) new ProfileFillingAddChildFragmentViewState0.g(Boolean.valueOf(((TransportHealthStepViewState) bottomSheetSelectorNavigationDto).b)));
        }
    }
}
