package e.a.b.h.b.f;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.b.d.b.IStatisticRepository;
import e.a.b.h.b.f.MainFragmentViewState0;
import e.a.b.h.b.f.MainFragmentViewState1;
import e.a.b.h.b.f.a;
import e.a.b.h.b.g.IMainCoordinator;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState1;
import e.c.c.BaseViewState4;
import e.c.c.k;
import e.c.c.p;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.t.Function;
import n.Unit;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: MainFragmentVm.kt */
public final class MainFragmentVm extends BaseDpFragmentVm1<c> {

    /* renamed from: j  reason: collision with root package name */
    public final IMainCoordinator f672j;

    /* renamed from: k  reason: collision with root package name */
    public final IStatisticRepository f673k;

    /* compiled from: MainFragmentVm.kt */
    public static final class a extends j implements Functions0<a.d, n.g> {
        public final /* synthetic */ MainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(MainFragmentVm mainFragmentVm) {
            super(1);
            this.c = mainFragmentVm;
        }

        public Object a(Object obj) {
            if (((MainFragmentViewState0.d) obj) != null) {
                this.c.f672j.k();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragmentVm.kt */
    public static final class b extends j implements Functions0<a.e, n.g> {
        public final /* synthetic */ MainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(MainFragmentVm mainFragmentVm) {
            super(1);
            this.c = mainFragmentVm;
        }

        public Object a(Object obj) {
            if (((MainFragmentViewState0.e) obj) != null) {
                this.c.f672j.m();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragmentVm.kt */
    public static final class c extends j implements Functions0<a.a, n.g> {
        public final /* synthetic */ MainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(MainFragmentVm mainFragmentVm) {
            super(1);
            this.c = mainFragmentVm;
        }

        public Object a(Object obj) {
            if (((MainFragmentViewState0.a) obj) != null) {
                this.c.f672j.i();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragmentVm.kt */
    public static final class d extends j implements Functions0<a.f, n.g> {
        public final /* synthetic */ MainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(MainFragmentVm mainFragmentVm) {
            super(1);
            this.c = mainFragmentVm;
        }

        public Object a(Object obj) {
            if (((MainFragmentViewState0.f) obj) != null) {
                this.c.f672j.g();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragmentVm.kt */
    public static final class e extends j implements Functions0<a.b, n.g> {
        public final /* synthetic */ MainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(MainFragmentVm mainFragmentVm) {
            super(1);
            this.c = mainFragmentVm;
        }

        public Object a(Object obj) {
            if (((MainFragmentViewState0.b) obj) != null) {
                this.c.f672j.d();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragmentVm.kt */
    public static final class f extends j implements Functions0<a.c, n.g> {
        public final /* synthetic */ MainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(MainFragmentVm mainFragmentVm) {
            super(1);
            this.c = mainFragmentVm;
        }

        public Object a(Object obj) {
            if (((MainFragmentViewState0.c) obj) != null) {
                this.c.f672j.b();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragmentVm.kt */
    public static final class g<T, R> implements Function<T, ObservableSource<? extends R>> {
        public final /* synthetic */ MainFragmentVm a;

        public g(MainFragmentVm mainFragmentVm) {
            this.a = mainFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Observable<R>, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                MainFragmentVm mainFragmentVm = this.a;
                Observable<R> c = mainFragmentVm.f673k.a().c().c((Function) MainFragmentVm0.a).c(new BaseMviVm0.c());
                Intrinsics.a((Object) c, "statisticRepository.getS….startWith(Lce.Loading())");
                String name = MainFragmentViewState1.a.class.getName();
                Intrinsics.a((Object) name, "MainFragmentResult.InitResult::class.java.name");
                return BaseDpFragmentVm1.a(mainFragmentVm, c, MainFragmentViewState1.a.class, null, null, null, new ErrorNavigationDto(name, null, null, null, null, null, 0, 0, 0, 0, true, false, true, false, false, false, null, null, 257022), 14, null);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public Object a(Object obj, BaseViewState1 baseViewState1) {
        MainFragmentViewState mainFragmentViewState = (MainFragmentViewState) obj;
        if (mainFragmentViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof MainFragmentViewState1.a) {
            return new MainFragmentViewState(false, ((MainFragmentViewState1.a) baseViewState1).a);
        } else {
            super.a(mainFragmentViewState, baseViewState1);
            return mainFragmentViewState;
        }
    }

    public Object b(Object obj) {
        MainFragmentViewState mainFragmentViewState = (MainFragmentViewState) obj;
        if (mainFragmentViewState != null) {
            return new MainFragmentViewState(true, mainFragmentViewState.b);
        }
        Intrinsics.a("vs");
        throw null;
    }

    public Object d() {
        return new MainFragmentViewState(false, null, 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> b(Observable<p> observable) {
        if (observable != null) {
            Observable<R> a2 = observable.a(new g(this));
            Intrinsics.a((Object) a2, "observable.flatMap {\n   …              )\n        }");
            return a2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(MainFragmentViewState0.d.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(MainFragmentViewState0.e.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(MainFragmentViewState0.a.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(MainFragmentViewState0.f.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(MainFragmentViewState0.b.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<U> a7 = observable.a(MainFragmentViewState0.c.class);
            Intrinsics.a((Object) a7, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a8 = Observable.a(super.a(observable), BaseMviVm.a(a2, new a(this)), BaseMviVm.a(a3, new b(this)), BaseMviVm.a(a4, new c(this)), BaseMviVm.a(a5, new d(this)), BaseMviVm.a(a6, new e(this)), BaseMviVm.a(a7, new f(this)));
            Intrinsics.a((Object) a8, "Observable.mergeArray(\n …)\n            }\n        )");
            return a8;
        }
        Intrinsics.a("o");
        throw null;
    }
}
