package e.a.b.h.b.h.o;

import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: TransportHealthStepViewState.kt */
public final class TransportHealthStepViewState1 {
    public ProfileData a;
    public String b;

    public TransportHealthStepViewState1() {
        this(null, null, 3);
    }

    public /* synthetic */ TransportHealthStepViewState1(ProfileData profileData, String str, int i2) {
        ProfileData profileData2 = (i2 & 1) != 0 ? new ProfileData(false, false, false, null, null, null, null, null, null, 511, null) : profileData;
        String str2 = (i2 & 2) != 0 ? "" : str;
        if (profileData2 == null) {
            Intrinsics.a("profileData");
            throw null;
        } else if (str2 != null) {
            this.a = profileData2;
            this.b = str2;
        } else {
            Intrinsics.a("tmpQuarantineAddress");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TransportHealthStepViewState1)) {
            return false;
        }
        TransportHealthStepViewState1 transportHealthStepViewState1 = (TransportHealthStepViewState1) obj;
        return Intrinsics.a(this.a, transportHealthStepViewState1.a) && Intrinsics.a(this.b, transportHealthStepViewState1.b);
    }

    public int hashCode() {
        ProfileData profileData = this.a;
        int i2 = 0;
        int hashCode = (profileData != null ? profileData.hashCode() : 0) * 31;
        String str = this.b;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("TransportHealthStepViewState(profileData=");
        a2.append(this.a);
        a2.append(", tmpQuarantineAddress=");
        return outline.a(a2, this.b, ")");
    }
}
