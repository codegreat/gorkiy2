package e.b.a;

import e.b.a.h.Back;
import e.b.a.h.BackTo;
import e.b.a.h.Command;
import e.b.a.h.Forward;
import e.b.a.h.Replace;

public class Router extends BaseRouter {
    public void a(Screen screen) {
        Command[] commandArr = {new Forward(screen)};
        CommandBuffer commandBuffer = super.a;
        Navigator navigator = commandBuffer.a;
        if (navigator != null) {
            navigator.a(commandArr);
        } else {
            commandBuffer.b.add(commandArr);
        }
    }

    public void b(Screen screen) {
        Command[] commandArr = {new Replace(screen)};
        CommandBuffer commandBuffer = super.a;
        Navigator navigator = commandBuffer.a;
        if (navigator != null) {
            navigator.a(commandArr);
        } else {
            commandBuffer.b.add(commandArr);
        }
    }

    public void a(Screen... screenArr) {
        int i2 = 1;
        Command[] commandArr = new Command[(screenArr.length + 1)];
        commandArr[0] = new BackTo(null);
        if (screenArr.length > 0) {
            commandArr[1] = new Replace(screenArr[0]);
            while (i2 < screenArr.length) {
                int i3 = i2 + 1;
                commandArr[i3] = new Forward(screenArr[i2]);
                i2 = i3;
            }
        }
        CommandBuffer commandBuffer = super.a;
        Navigator navigator = commandBuffer.a;
        if (navigator != null) {
            navigator.a(commandArr);
        } else {
            commandBuffer.b.add(commandArr);
        }
    }

    public void a() {
        super.a.a(new Command[]{new Back()});
    }
}
