package o;

import j.a.a.a.outline;
import java.net.InetSocketAddress;
import java.net.Proxy;
import n.n.c.Intrinsics;

/* compiled from: Route.kt */
public final class Route {
    public final Address a;
    public final Proxy b;
    public final InetSocketAddress c;

    public Route(Address address, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (address == null) {
            Intrinsics.a("address");
            throw null;
        } else if (proxy == null) {
            Intrinsics.a("proxy");
            throw null;
        } else if (inetSocketAddress != null) {
            this.a = address;
            this.b = proxy;
            this.c = inetSocketAddress;
        } else {
            Intrinsics.a("socketAddress");
            throw null;
        }
    }

    public final boolean a() {
        return this.a.f2797f != null && this.b.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Route) {
            Route route = (Route) obj;
            return Intrinsics.a(route.a, this.a) && Intrinsics.a(route.b, this.b) && Intrinsics.a(route.c, this.c);
        }
    }

    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + ((this.a.hashCode() + 527) * 31)) * 31);
    }

    public String toString() {
        StringBuilder a2 = outline.a("Route{");
        a2.append(this.c);
        a2.append('}');
        return a2.toString();
    }
}
