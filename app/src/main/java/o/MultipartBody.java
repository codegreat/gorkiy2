package o;

import com.crashlytics.android.answers.SessionEventTransform;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.MediaType;
import o.a0;
import p.BufferedSink;
import p.ByteString;
import p.j;

/* compiled from: MultipartBody.kt */
public final class MultipartBody extends RequestBody {
    public static final MediaType g = MediaType.a.a("multipart/mixed");
    public static final MediaType h = MediaType.a.a("multipart/form-data");

    /* renamed from: i  reason: collision with root package name */
    public static final byte[] f2860i = {(byte) 58, (byte) 32};

    /* renamed from: j  reason: collision with root package name */
    public static final byte[] f2861j = {(byte) 13, (byte) 10};

    /* renamed from: k  reason: collision with root package name */
    public static final byte[] f2862k;

    /* renamed from: l  reason: collision with root package name */
    public static final b f2863l = new b(null);
    public final MediaType b;
    public long c;
    public final ByteString d;

    /* renamed from: e  reason: collision with root package name */
    public final MediaType f2864e;

    /* renamed from: f  reason: collision with root package name */
    public final List<a0.c> f2865f;

    /* compiled from: MultipartBody.kt */
    public static final class a {
        public final ByteString a;
        public MediaType b;
        public final List<a0.c> c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public a() {
            String uuid = UUID.randomUUID().toString();
            Intrinsics.a((Object) uuid, "UUID.randomUUID().toString()");
            if (uuid != null) {
                this.a = ByteString.f3063f.b(uuid);
                this.b = MultipartBody.g;
                this.c = new ArrayList();
                return;
            }
            Intrinsics.a("boundary");
            throw null;
        }
    }

    /* compiled from: MultipartBody.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(StringBuilder sb, String str) {
            if (sb == null) {
                Intrinsics.a("$this$appendQuotedString");
                throw null;
            } else if (str != null) {
                sb.append('\"');
                int length = str.length();
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt = str.charAt(i2);
                    if (charAt == 10) {
                        sb.append("%0A");
                    } else if (charAt == 13) {
                        sb.append("%0D");
                    } else if (charAt == '\"') {
                        sb.append("%22");
                    } else {
                        sb.append(charAt);
                    }
                }
                sb.append('\"');
            } else {
                Intrinsics.a("key");
                throw null;
            }
        }
    }

    /* compiled from: MultipartBody.kt */
    public static final class c {
        public final Headers a;
        public final RequestBody b;

        public /* synthetic */ c(Headers headers, RequestBody requestBody, DefaultConstructorMarker defaultConstructorMarker) {
            this.a = headers;
            this.b = requestBody;
        }

        public static final c a(Headers headers, RequestBody requestBody) {
            if (requestBody != null) {
                boolean z = true;
                if ((headers != null ? headers.a("Content-Type") : null) == null) {
                    if ((headers != null ? headers.a("Content-Length") : null) != null) {
                        z = false;
                    }
                    if (z) {
                        return new c(headers, requestBody, null);
                    }
                    throw new IllegalArgumentException("Unexpected header: Content-Length".toString());
                }
                throw new IllegalArgumentException("Unexpected header: Content-Type".toString());
            }
            Intrinsics.a("body");
            throw null;
        }
    }

    static {
        MediaType.a aVar = MediaType.f2859f;
        MediaType.a aVar2 = MediaType.f2859f;
        MediaType.a.a("multipart/alternative");
        MediaType.a aVar3 = MediaType.f2859f;
        MediaType.a.a("multipart/digest");
        MediaType.a aVar4 = MediaType.f2859f;
        MediaType.a.a("multipart/parallel");
        MediaType.a aVar5 = MediaType.f2859f;
        byte b2 = (byte) 45;
        f2862k = new byte[]{b2, b2};
    }

    public MultipartBody(j jVar, z zVar, List<a0.c> list) {
        if (jVar == null) {
            Intrinsics.a("boundaryByteString");
            throw null;
        } else if (zVar == null) {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        } else if (list != null) {
            this.d = jVar;
            this.f2864e = zVar;
            this.f2865f = list;
            MediaType.a aVar = MediaType.f2859f;
            StringBuilder sb = new StringBuilder();
            sb.append(this.f2864e);
            sb.append("; boundary=");
            ByteString byteString = this.d;
            if (byteString != null) {
                sb.append(p.b0.ByteString.h(byteString));
                this.b = MediaType.a.a(sb.toString());
                this.c = -1;
                return;
            }
            throw null;
        } else {
            Intrinsics.a("parts");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.MultipartBody.a(p.BufferedSink, boolean):long
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      o.RequestBody.a(o.MediaType, p.ByteString):o.RequestBody
      o.MultipartBody.a(p.BufferedSink, boolean):long */
    public long a() {
        long j2 = this.c;
        if (j2 != -1) {
            return j2;
        }
        long a2 = a((BufferedSink) null, true);
        this.c = a2;
        return a2;
    }

    public MediaType b() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.MultipartBody.a(p.BufferedSink, boolean):long
     arg types: [p.BufferedSink, int]
     candidates:
      o.RequestBody.a(o.MediaType, p.ByteString):o.RequestBody
      o.MultipartBody.a(p.BufferedSink, boolean):long */
    public void a(BufferedSink bufferedSink) {
        if (bufferedSink != null) {
            a(bufferedSink, false);
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v1, resolved type: p.BufferedSink} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: p.Buffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: p.Buffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v4, resolved type: p.BufferedSink} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: p.Buffer} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long a(p.BufferedSink r14, boolean r15) {
        /*
            r13 = this;
            r0 = 0
            if (r15 == 0) goto L_0x000a
            p.Buffer r14 = new p.Buffer
            r14.<init>()
            r1 = r14
            goto L_0x000b
        L_0x000a:
            r1 = r0
        L_0x000b:
            java.util.List<o.a0$c> r2 = r13.f2865f
            int r2 = r2.size()
            r3 = 0
            r4 = 0
            r6 = 0
        L_0x0015:
            if (r6 >= r2) goto L_0x00b3
            java.util.List<o.a0$c> r7 = r13.f2865f
            java.lang.Object r7 = r7.get(r6)
            o.MultipartBody$c r7 = (o.MultipartBody.c) r7
            o.Headers r8 = r7.a
            o.RequestBody r7 = r7.b
            if (r14 == 0) goto L_0x00af
            byte[] r9 = o.MultipartBody.f2862k
            r14.write(r9)
            p.ByteString r9 = r13.d
            r14.a(r9)
            byte[] r9 = o.MultipartBody.f2861j
            r14.write(r9)
            if (r8 == 0) goto L_0x005b
            int r9 = r8.size()
            r10 = 0
        L_0x003b:
            if (r10 >= r9) goto L_0x005b
            java.lang.String r11 = r8.c(r10)
            p.BufferedSink r11 = r14.a(r11)
            byte[] r12 = o.MultipartBody.f2860i
            p.BufferedSink r11 = r11.write(r12)
            java.lang.String r12 = r8.d(r10)
            p.BufferedSink r11 = r11.a(r12)
            byte[] r12 = o.MultipartBody.f2861j
            r11.write(r12)
            int r10 = r10 + 1
            goto L_0x003b
        L_0x005b:
            o.MediaType r8 = r7.b()
            if (r8 == 0) goto L_0x0072
            java.lang.String r9 = "Content-Type: "
            p.BufferedSink r9 = r14.a(r9)
            java.lang.String r8 = r8.a
            p.BufferedSink r8 = r9.a(r8)
            byte[] r9 = o.MultipartBody.f2861j
            r8.write(r9)
        L_0x0072:
            long r8 = r7.a()
            r10 = -1
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 == 0) goto L_0x008c
            java.lang.String r10 = "Content-Length: "
            p.BufferedSink r10 = r14.a(r10)
            p.BufferedSink r10 = r10.h(r8)
            byte[] r11 = o.MultipartBody.f2861j
            r10.write(r11)
            goto L_0x009a
        L_0x008c:
            if (r15 == 0) goto L_0x009a
            if (r1 == 0) goto L_0x0096
            long r14 = r1.c
            r1.skip(r14)
            return r10
        L_0x0096:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x009a:
            byte[] r10 = o.MultipartBody.f2861j
            r14.write(r10)
            if (r15 == 0) goto L_0x00a3
            long r4 = r4 + r8
            goto L_0x00a6
        L_0x00a3:
            r7.a(r14)
        L_0x00a6:
            byte[] r7 = o.MultipartBody.f2861j
            r14.write(r7)
            int r6 = r6 + 1
            goto L_0x0015
        L_0x00af:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x00b3:
            if (r14 == 0) goto L_0x00d9
            byte[] r2 = o.MultipartBody.f2862k
            r14.write(r2)
            p.ByteString r2 = r13.d
            r14.a(r2)
            byte[] r2 = o.MultipartBody.f2862k
            r14.write(r2)
            byte[] r2 = o.MultipartBody.f2861j
            r14.write(r2)
            if (r15 == 0) goto L_0x00d8
            if (r1 == 0) goto L_0x00d4
            long r14 = r1.c
            long r4 = r4 + r14
            r1.skip(r14)
            goto L_0x00d8
        L_0x00d4:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x00d8:
            return r4
        L_0x00d9:
            n.n.c.Intrinsics.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.MultipartBody.a(p.BufferedSink, boolean):long");
    }
}
