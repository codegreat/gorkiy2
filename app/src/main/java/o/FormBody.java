package o;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import n.n.c.Intrinsics;
import o.MediaType;
import o.m0.Util;
import p.Buffer;
import p.BufferedSink;

/* compiled from: FormBody.kt */
public final class FormBody extends RequestBody {
    public static final MediaType d = MediaType.a.a("application/x-www-form-urlencoded");
    public final List<String> b;
    public final List<String> c;

    /* compiled from: FormBody.kt */
    public static final class a {
        public final List<String> a = new ArrayList();
        public final List<String> b = new ArrayList();
        public final Charset c = null;
    }

    static {
        MediaType.a aVar = MediaType.f2859f;
    }

    public FormBody(List<String> list, List<String> list2) {
        if (list == null) {
            Intrinsics.a("encodedNames");
            throw null;
        } else if (list2 != null) {
            this.b = Util.b(list);
            this.c = Util.b(list2);
        } else {
            Intrinsics.a("encodedValues");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.FormBody.a(p.BufferedSink, boolean):long
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      o.RequestBody.a(o.MediaType, p.ByteString):o.RequestBody
      o.FormBody.a(p.BufferedSink, boolean):long */
    public long a() {
        return a((BufferedSink) null, true);
    }

    public MediaType b() {
        return d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.FormBody.a(p.BufferedSink, boolean):long
     arg types: [p.BufferedSink, int]
     candidates:
      o.RequestBody.a(o.MediaType, p.ByteString):o.RequestBody
      o.FormBody.a(p.BufferedSink, boolean):long */
    public void a(BufferedSink bufferedSink) {
        if (bufferedSink != null) {
            a(bufferedSink, false);
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public final long a(BufferedSink bufferedSink, boolean z) {
        Buffer buffer;
        if (z) {
            buffer = new Buffer();
        } else if (bufferedSink != null) {
            buffer = bufferedSink.getBuffer();
        } else {
            Intrinsics.a();
            throw null;
        }
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (i2 > 0) {
                buffer.writeByte(38);
            }
            buffer.a(this.b.get(i2));
            buffer.writeByte(61);
            buffer.a(this.c.get(i2));
        }
        if (!z) {
            return 0;
        }
        long j2 = buffer.c;
        buffer.skip(j2);
        return j2;
    }
}
