package o.m0.i.g;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.net.ssl.SSLSocket;
import n.n.c.Intrinsics;
import o.c0;
import o.m0.i.AndroidPlatform;
import o.m0.i.Platform;

/* compiled from: AndroidSocketAdapter.kt */
public class AndroidSocketAdapter implements SocketAdapter {
    public final Method a;
    public final Method b;
    public final Method c;
    public final Method d;

    /* renamed from: e  reason: collision with root package name */
    public final Class<? super SSLSocket> f3053e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Method, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public AndroidSocketAdapter(Class<? super SSLSocket> cls) {
        if (cls != null) {
            this.f3053e = cls;
            Method declaredMethod = cls.getDeclaredMethod("setUseSessionTickets", Boolean.TYPE);
            Intrinsics.a((Object) declaredMethod, "sslSocketClass.getDeclar…:class.javaPrimitiveType)");
            this.a = declaredMethod;
            this.b = this.f3053e.getMethod("setHostname", String.class);
            this.c = this.f3053e.getMethod("getAlpnSelectedProtocol", new Class[0]);
            this.d = this.f3053e.getMethod("setAlpnProtocols", byte[].class);
            return;
        }
        Intrinsics.a("sslSocketClass");
        throw null;
    }

    public boolean a() {
        AndroidPlatform.b bVar = AndroidPlatform.g;
        return AndroidPlatform.f3044f;
    }

    public boolean b(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            return this.f3053e.isInstance(sSLSocket);
        }
        Intrinsics.a("sslSocket");
        throw null;
    }

    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list == null) {
            Intrinsics.a("protocols");
            throw null;
        } else if (b(sSLSocket)) {
            if (str != null) {
                try {
                    this.a.invoke(sSLSocket, true);
                    this.b.invoke(sSLSocket, str);
                } catch (IllegalAccessException e2) {
                    throw new AssertionError(e2);
                } catch (InvocationTargetException e3) {
                    throw new AssertionError(e3);
                }
            }
            this.d.invoke(sSLSocket, Platform.c.b(list));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.charset.Charset, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String a(SSLSocket sSLSocket) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (!b(sSLSocket)) {
            return null;
        } else {
            try {
                byte[] bArr = (byte[]) this.c.invoke(sSLSocket, new Object[0]);
                if (bArr == null) {
                    return null;
                }
                Charset charset = StandardCharsets.UTF_8;
                Intrinsics.a((Object) charset, "StandardCharsets.UTF_8");
                return new String(bArr, charset);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            } catch (InvocationTargetException e3) {
                throw new AssertionError(e3);
            }
        }
    }
}
