package o.m0.f;

import j.a.a.a.outline;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Address;
import o.CookieJar;
import o.Headers;
import o.HttpUrl;
import o.OkHttpClient;
import o.Request;
import o.RequestBody;
import o.Response;
import o.Route;
import o.m0.Util;
import o.m0.d.RealConnection;
import o.m0.e.ExchangeCode;
import o.m0.e.HttpHeaders;
import o.m0.e.StatusLine;
import o.x;
import p.Buffer;
import p.BufferedSink;
import p.BufferedSource;
import p.ForwardingTimeout;
import p.Sink;
import p.Source;
import p.Timeout;

/* compiled from: Http1ExchangeCodec.kt */
public final class Http1ExchangeCodec implements ExchangeCode {
    public int a;
    public long b;
    public Headers c;
    public final OkHttpClient d;

    /* renamed from: e  reason: collision with root package name */
    public final RealConnection f2981e;

    /* renamed from: f  reason: collision with root package name */
    public final BufferedSource f2982f;
    public final BufferedSink g;

    /* compiled from: Http1ExchangeCodec.kt */
    public abstract class a implements Source {
        public final ForwardingTimeout b;
        public boolean c;

        public a() {
            this.b = new ForwardingTimeout(Http1ExchangeCodec.this.f2982f.b());
        }

        public final void a() {
            Http1ExchangeCodec http1ExchangeCodec = Http1ExchangeCodec.this;
            int i2 = http1ExchangeCodec.a;
            if (i2 != 6) {
                if (i2 == 5) {
                    Http1ExchangeCodec.a(http1ExchangeCodec, this.b);
                    Http1ExchangeCodec.this.a = 6;
                    return;
                }
                StringBuilder a = outline.a("state: ");
                a.append(Http1ExchangeCodec.this.a);
                throw new IllegalStateException(a.toString());
            }
        }

        public Timeout b() {
            return this.b;
        }

        public long b(Buffer buffer, long j2) {
            if (buffer != null) {
                try {
                    return Http1ExchangeCodec.this.f2982f.b(buffer, j2);
                } catch (IOException e2) {
                    RealConnection realConnection = Http1ExchangeCodec.this.f2981e;
                    if (realConnection == null) {
                        Intrinsics.a();
                        throw null;
                    }
                    realConnection.c();
                    a();
                    throw e2;
                }
            } else {
                Intrinsics.a("sink");
                throw null;
            }
        }
    }

    /* compiled from: Http1ExchangeCodec.kt */
    public final class b implements Sink {
        public final ForwardingTimeout b;
        public boolean c;

        public b() {
            this.b = new ForwardingTimeout(Http1ExchangeCodec.this.g.b());
        }

        public void a(Buffer buffer, long j2) {
            if (buffer == null) {
                Intrinsics.a("source");
                throw null;
            } else if (!(!this.c)) {
                throw new IllegalStateException("closed".toString());
            } else if (j2 != 0) {
                Http1ExchangeCodec.this.g.a(j2);
                Http1ExchangeCodec.this.g.a("\r\n");
                Http1ExchangeCodec.this.g.a(buffer, j2);
                Http1ExchangeCodec.this.g.a("\r\n");
            }
        }

        public Timeout b() {
            return this.b;
        }

        public synchronized void close() {
            if (!this.c) {
                this.c = true;
                Http1ExchangeCodec.this.g.a("0\r\n\r\n");
                Http1ExchangeCodec.a(Http1ExchangeCodec.this, this.b);
                Http1ExchangeCodec.this.a = 3;
            }
        }

        public synchronized void flush() {
            if (!this.c) {
                Http1ExchangeCodec.this.g.flush();
            }
        }
    }

    /* compiled from: Http1ExchangeCodec.kt */
    public final class c extends a {

        /* renamed from: e  reason: collision with root package name */
        public long f2983e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f2984f;
        public final HttpUrl g;
        public final /* synthetic */ Http1ExchangeCodec h;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Http1ExchangeCodec http1ExchangeCodec, x xVar) {
            super();
            if (xVar != null) {
                this.h = http1ExchangeCodec;
                this.g = xVar;
                this.f2983e = -1;
                this.f2984f = true;
                return;
            }
            Intrinsics.a("url");
            throw null;
        }

        public long b(Buffer buffer, long j2) {
            if (buffer != null) {
                boolean z = true;
                if (!(j2 >= 0)) {
                    throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
                } else if (!(!super.c)) {
                    throw new IllegalStateException("closed".toString());
                } else if (!this.f2984f) {
                    return -1;
                } else {
                    long j3 = this.f2983e;
                    if (j3 == 0 || j3 == -1) {
                        if (this.f2983e != -1) {
                            this.h.f2982f.e();
                        }
                        try {
                            this.f2983e = this.h.f2982f.k();
                            String e2 = this.h.f2982f.e();
                            if (e2 != null) {
                                String obj = Indent.c(e2).toString();
                                if (this.f2983e >= 0) {
                                    if (obj.length() <= 0) {
                                        z = false;
                                    }
                                    if (!z || Indent.b(obj, ";", false, 2)) {
                                        if (this.f2983e == 0) {
                                            this.f2984f = false;
                                            Http1ExchangeCodec http1ExchangeCodec = this.h;
                                            http1ExchangeCodec.c = http1ExchangeCodec.e();
                                            Http1ExchangeCodec http1ExchangeCodec2 = this.h;
                                            OkHttpClient okHttpClient = http1ExchangeCodec2.d;
                                            if (okHttpClient != null) {
                                                CookieJar cookieJar = okHttpClient.f2870k;
                                                HttpUrl httpUrl = this.g;
                                                Headers headers = http1ExchangeCodec2.c;
                                                if (headers != null) {
                                                    HttpHeaders.a(cookieJar, httpUrl, headers);
                                                    a();
                                                } else {
                                                    Intrinsics.a();
                                                    throw null;
                                                }
                                            } else {
                                                Intrinsics.a();
                                                throw null;
                                            }
                                        }
                                        if (!this.f2984f) {
                                            return -1;
                                        }
                                    }
                                }
                                throw new ProtocolException("expected chunk size and optional extensions" + " but was \"" + this.f2983e + obj + '\"');
                            }
                            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                        } catch (NumberFormatException e3) {
                            throw new ProtocolException(e3.getMessage());
                        }
                    }
                    long b = super.b(buffer, Math.min(j2, this.f2983e));
                    if (b == -1) {
                        RealConnection realConnection = this.h.f2981e;
                        if (realConnection == null) {
                            Intrinsics.a();
                            throw null;
                        }
                        realConnection.c();
                        ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                        a();
                        throw protocolException;
                    }
                    this.f2983e -= b;
                    return b;
                }
            } else {
                Intrinsics.a("sink");
                throw null;
            }
        }

        public void close() {
            if (!super.c) {
                if (this.f2984f && !Util.a(this, 100, TimeUnit.MILLISECONDS)) {
                    RealConnection realConnection = this.h.f2981e;
                    if (realConnection != null) {
                        realConnection.c();
                        a();
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                super.c = true;
            }
        }
    }

    /* compiled from: Http1ExchangeCodec.kt */
    public final class d extends a {

        /* renamed from: e  reason: collision with root package name */
        public long f2985e;

        public d(long j2) {
            super();
            this.f2985e = j2;
            if (j2 == 0) {
                a();
            }
        }

        public long b(Buffer buffer, long j2) {
            if (buffer != null) {
                if (!(j2 >= 0)) {
                    throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
                } else if (true ^ super.c) {
                    long j3 = this.f2985e;
                    if (j3 == 0) {
                        return -1;
                    }
                    long b = super.b(buffer, Math.min(j3, j2));
                    if (b == -1) {
                        RealConnection realConnection = Http1ExchangeCodec.this.f2981e;
                        if (realConnection == null) {
                            Intrinsics.a();
                            throw null;
                        }
                        realConnection.c();
                        ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                        a();
                        throw protocolException;
                    }
                    long j4 = this.f2985e - b;
                    this.f2985e = j4;
                    if (j4 == 0) {
                        a();
                    }
                    return b;
                } else {
                    throw new IllegalStateException("closed".toString());
                }
            } else {
                Intrinsics.a("sink");
                throw null;
            }
        }

        public void close() {
            if (!super.c) {
                if (this.f2985e != 0 && !Util.a(this, 100, TimeUnit.MILLISECONDS)) {
                    RealConnection realConnection = Http1ExchangeCodec.this.f2981e;
                    if (realConnection != null) {
                        realConnection.c();
                        a();
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                super.c = true;
            }
        }
    }

    /* compiled from: Http1ExchangeCodec.kt */
    public final class e implements Sink {
        public final ForwardingTimeout b;
        public boolean c;

        public e() {
            this.b = new ForwardingTimeout(Http1ExchangeCodec.this.g.b());
        }

        public void a(Buffer buffer, long j2) {
            if (buffer == null) {
                Intrinsics.a("source");
                throw null;
            } else if (!this.c) {
                Util.a(buffer.c, 0, j2);
                Http1ExchangeCodec.this.g.a(buffer, j2);
            } else {
                throw new IllegalStateException("closed".toString());
            }
        }

        public Timeout b() {
            return this.b;
        }

        public void close() {
            if (!this.c) {
                this.c = true;
                Http1ExchangeCodec.a(Http1ExchangeCodec.this, this.b);
                Http1ExchangeCodec.this.a = 3;
            }
        }

        public void flush() {
            if (!this.c) {
                Http1ExchangeCodec.this.g.flush();
            }
        }
    }

    /* compiled from: Http1ExchangeCodec.kt */
    public final class f extends a {

        /* renamed from: e  reason: collision with root package name */
        public boolean f2987e;

        public f(Http1ExchangeCodec http1ExchangeCodec) {
            super();
        }

        public long b(Buffer buffer, long j2) {
            if (buffer != null) {
                if (!(j2 >= 0)) {
                    throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
                } else if (!(!super.c)) {
                    throw new IllegalStateException("closed".toString());
                } else if (this.f2987e) {
                    return -1;
                } else {
                    long b = super.b(buffer, j2);
                    if (b != -1) {
                        return b;
                    }
                    this.f2987e = true;
                    a();
                    return -1;
                }
            } else {
                Intrinsics.a("sink");
                throw null;
            }
        }

        public void close() {
            if (!super.c) {
                if (!this.f2987e) {
                    a();
                }
                super.c = true;
            }
        }
    }

    public Http1ExchangeCodec(OkHttpClient okHttpClient, RealConnection realConnection, BufferedSource bufferedSource, BufferedSink bufferedSink) {
        if (bufferedSource == null) {
            Intrinsics.a("source");
            throw null;
        } else if (bufferedSink != null) {
            this.d = okHttpClient;
            this.f2981e = realConnection;
            this.f2982f = bufferedSource;
            this.g = bufferedSink;
            this.b = (long) 262144;
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public static final /* synthetic */ void a(Http1ExchangeCodec http1ExchangeCodec, ForwardingTimeout forwardingTimeout) {
        if (http1ExchangeCodec != null) {
            Timeout timeout = forwardingTimeout.f3064e;
            forwardingTimeout.f3064e = Timeout.d;
            timeout.a();
            timeout.b();
            return;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public Source b(Response response) {
        if (response == null) {
            Intrinsics.a("response");
            throw null;
        } else if (!HttpHeaders.a(response)) {
            return a(0);
        } else {
            boolean z = true;
            if (Indent.a("chunked", Response.a(response, "Transfer-Encoding", null, 2), true)) {
                HttpUrl httpUrl = response.c.b;
                if (this.a != 4) {
                    z = false;
                }
                if (z) {
                    this.a = 5;
                    return new c(this, httpUrl);
                }
                StringBuilder a2 = outline.a("state: ");
                a2.append(this.a);
                throw new IllegalStateException(a2.toString().toString());
            }
            long a3 = Util.a(response);
            if (a3 != -1) {
                return a(a3);
            }
            if (this.a != 4) {
                z = false;
            }
            if (z) {
                this.a = 5;
                RealConnection realConnection = this.f2981e;
                if (realConnection != null) {
                    realConnection.c();
                    return new f(this);
                }
                Intrinsics.a();
                throw null;
            }
            StringBuilder a4 = outline.a("state: ");
            a4.append(this.a);
            throw new IllegalStateException(a4.toString().toString());
        }
    }

    public void c() {
        this.g.flush();
    }

    public void cancel() {
        Socket socket;
        RealConnection realConnection = this.f2981e;
        if (realConnection != null && (socket = realConnection.b) != null) {
            Util.a(socket);
        }
    }

    public final String d() {
        String c2 = this.f2982f.c(this.b);
        this.b -= (long) c2.length();
        return c2;
    }

    public final Headers e() {
        Headers.a aVar = new Headers.a();
        String d2 = d();
        while (true) {
            if (!(d2.length() > 0)) {
                return aVar.a();
            }
            aVar.a(d2);
            d2 = d();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public Sink a(Request request, long j2) {
        if (request != null) {
            RequestBody requestBody = request.f2896e;
            if (requestBody == null || requestBody != null) {
                boolean z = true;
                if (Indent.a("chunked", request.a("Transfer-Encoding"), true)) {
                    if (this.a != 1) {
                        z = false;
                    }
                    if (z) {
                        this.a = 2;
                        return new b();
                    }
                    StringBuilder a2 = outline.a("state: ");
                    a2.append(this.a);
                    throw new IllegalStateException(a2.toString().toString());
                } else if (j2 != -1) {
                    if (this.a != 1) {
                        z = false;
                    }
                    if (z) {
                        this.a = 2;
                        return new e();
                    }
                    StringBuilder a3 = outline.a("state: ");
                    a3.append(this.a);
                    throw new IllegalStateException(a3.toString().toString());
                } else {
                    throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
                }
            } else {
                throw null;
            }
        } else {
            Intrinsics.a("request");
            throw null;
        }
    }

    public void b() {
        this.g.flush();
    }

    public RealConnection a() {
        return this.f2981e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.net.Proxy$Type, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Request request) {
        if (request != null) {
            RealConnection realConnection = this.f2981e;
            if (realConnection != null) {
                Proxy.Type type = realConnection.f2959q.b.type();
                Intrinsics.a((Object) type, "realConnection!!.route().proxy.type()");
                StringBuilder sb = new StringBuilder();
                sb.append(request.c);
                sb.append(' ');
                if (!request.b.a && type == Proxy.Type.HTTP) {
                    sb.append(request.b);
                } else {
                    HttpUrl httpUrl = request.b;
                    if (httpUrl != null) {
                        String b2 = httpUrl.b();
                        String d2 = httpUrl.d();
                        if (d2 != null) {
                            b2 = b2 + '?' + d2;
                        }
                        sb.append(b2);
                    } else {
                        Intrinsics.a("url");
                        throw null;
                    }
                }
                sb.append(" HTTP/1.1");
                String sb2 = sb.toString();
                Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
                a(request.d, sb2);
                return;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("request");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public long a(Response response) {
        if (response == null) {
            Intrinsics.a("response");
            throw null;
        } else if (!HttpHeaders.a(response)) {
            return 0;
        } else {
            if (Indent.a("chunked", Response.a(response, "Transfer-Encoding", null, 2), true)) {
                return -1;
            }
            return Util.a(response);
        }
    }

    public final void a(Headers headers, String str) {
        if (headers == null) {
            Intrinsics.a("headers");
            throw null;
        } else if (str != null) {
            if (this.a == 0) {
                this.g.a(str).a("\r\n");
                int size = headers.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.g.a(headers.c(i2)).a(": ").a(headers.d(i2)).a("\r\n");
                }
                this.g.a("\r\n");
                this.a = 1;
                return;
            }
            StringBuilder a2 = outline.a("state: ");
            a2.append(this.a);
            throw new IllegalStateException(a2.toString().toString());
        } else {
            Intrinsics.a("requestLine");
            throw null;
        }
    }

    public Response.a a(boolean z) {
        String str;
        Route route;
        Address address;
        HttpUrl httpUrl;
        int i2 = this.a;
        boolean z2 = true;
        if (!(i2 == 1 || i2 == 3)) {
            z2 = false;
        }
        if (z2) {
            try {
                StatusLine a2 = StatusLine.a(d());
                Response.a aVar = new Response.a();
                aVar.a(a2.a);
                aVar.c = a2.b;
                aVar.a(a2.c);
                aVar.a(e());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.a = 3;
                    return aVar;
                }
                this.a = 4;
                return aVar;
            } catch (EOFException e2) {
                RealConnection realConnection = this.f2981e;
                if (realConnection == null || (route = realConnection.f2959q) == null || (address = route.a) == null || (httpUrl = address.a) == null || (str = httpUrl.f()) == null) {
                    str = "unknown";
                }
                throw new IOException(outline.a("unexpected end of stream on ", str), e2);
            }
        } else {
            StringBuilder a3 = outline.a("state: ");
            a3.append(this.a);
            throw new IllegalStateException(a3.toString().toString());
        }
    }

    public final Source a(long j2) {
        if (this.a == 4) {
            this.a = 5;
            return new d(j2);
        }
        StringBuilder a2 = outline.a("state: ");
        a2.append(this.a);
        throw new IllegalStateException(a2.toString().toString());
    }
}
