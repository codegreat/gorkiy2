package o.m0.g;

import com.crashlytics.android.core.CodedOutputStream;
import j.a.a.a.outline;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import n.n.c.Intrinsics;
import o.m0.Util;
import o.m0.g.Hpack;
import p.Buffer;
import p.BufferedSource;
import p.ByteString;
import p.Source;
import p.Timeout;

/* compiled from: Http2Reader.kt */
public final class Http2Reader implements Closeable {

    /* renamed from: f  reason: collision with root package name */
    public static final Logger f3021f;
    public static final Http2Reader g = null;
    public final a b;
    public final Hpack.a c;
    public final BufferedSource d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f3022e;

    /* compiled from: Http2Reader.kt */
    public interface b {
        void a();

        void a(int i2, int i3, int i4, boolean z);

        void a(int i2, int i3, List<b> list);

        void a(int i2, long j2);

        void a(int i2, ErrorCode errorCode);

        void a(int i2, ErrorCode errorCode, ByteString byteString);

        void a(boolean z, int i2, int i3);

        void a(boolean z, int i2, int i3, List<b> list);

        void a(boolean z, int i2, BufferedSource bufferedSource, int i3);

        void a(boolean z, Settings settings);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.logging.Logger, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    static {
        Logger logger = Logger.getLogger(Http2.class.getName());
        Intrinsics.a((Object) logger, "Logger.getLogger(Http2::class.java.name)");
        f3021f = logger;
    }

    public Http2Reader(BufferedSource bufferedSource, boolean z) {
        if (bufferedSource != null) {
            this.d = bufferedSource;
            this.f3022e = z;
            a aVar = new a(bufferedSource);
            this.b = aVar;
            this.c = new Hpack.a(aVar, CodedOutputStream.DEFAULT_BUFFER_SIZE, 0, 4);
            return;
        }
        Intrinsics.a("source");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.g.Http2Reader.a(boolean, o.m0.g.Http2Reader$b):boolean
     arg types: [int, o.m0.g.Http2Reader$b]
     candidates:
      o.m0.g.Http2Reader.a(o.m0.g.Http2Reader$b, int):void
      o.m0.g.Http2Reader.a(boolean, o.m0.g.Http2Reader$b):boolean */
    public final void a(b bVar) {
        if (bVar == null) {
            Intrinsics.a("handler");
            throw null;
        } else if (!this.f3022e) {
            ByteString b2 = this.d.b((long) Http2.a.g());
            if (f3021f.isLoggable(Level.FINE)) {
                Logger logger = f3021f;
                StringBuilder a2 = outline.a("<< CONNECTION ");
                a2.append(b2.h());
                logger.fine(Util.a(a2.toString(), new Object[0]));
            }
            if (!Intrinsics.a(Http2.a, b2)) {
                StringBuilder a3 = outline.a("Expected a connection header but was ");
                if (b2 != null) {
                    a3.append(p.b0.ByteString.h(b2));
                    throw new IOException(a3.toString());
                }
                throw null;
            }
        } else if (!a(true, bVar)) {
            throw new IOException("Required SETTINGS preface not received");
        }
    }

    public void close() {
        this.d.close();
    }

    /* JADX WARN: Type inference failed for: r1v7, types: [n.o.Ranges0, n.o.Progressions] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.g.Http2Reader.b.a(boolean, o.m0.g.Settings):void
     arg types: [int, o.m0.g.Settings]
     candidates:
      o.m0.g.Http2Reader.b.a(int, long):void
      o.m0.g.Http2Reader.b.a(int, o.m0.g.ErrorCode):void
      o.m0.g.Http2Reader.b.a(boolean, o.m0.g.Settings):void */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x019d, code lost:
        throw new java.io.IOException(j.a.a.a.outline.b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: ", r7));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(boolean r14, o.m0.g.Http2Reader.b r15) {
        /*
            r13 = this;
            if (r15 == 0) goto L_0x02b0
            r0 = 0
            p.BufferedSource r1 = r13.d     // Catch:{ EOFException -> 0x02af }
            r2 = 9
            r1.g(r2)     // Catch:{ EOFException -> 0x02af }
            p.BufferedSource r1 = r13.d
            int r1 = o.m0.Util.a(r1)
            r2 = 16384(0x4000, float:2.2959E-41)
            if (r1 > r2) goto L_0x02a3
            p.BufferedSource r2 = r13.d
            byte r2 = r2.readByte()
            r8 = r2 & 255(0xff, float:3.57E-43)
            r9 = 4
            if (r14 == 0) goto L_0x002e
            if (r8 != r9) goto L_0x0022
            goto L_0x002e
        L_0x0022:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "Expected a SETTINGS frame but was "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r8)
            r14.<init>(r15)
            throw r14
        L_0x002e:
            p.BufferedSource r14 = r13.d
            byte r14 = r14.readByte()
            r14 = r14 & 255(0xff, float:3.57E-43)
            p.BufferedSource r2 = r13.d
            int r2 = r2.readInt()
            r10 = 2147483647(0x7fffffff, float:NaN)
            r11 = r2 & r10
            java.util.logging.Logger r2 = o.m0.g.Http2Reader.f3021f
            java.util.logging.Level r3 = java.util.logging.Level.FINE
            boolean r2 = r2.isLoggable(r3)
            if (r2 == 0) goto L_0x005b
            java.util.logging.Logger r12 = o.m0.g.Http2Reader.f3021f
            o.m0.g.Http2 r2 = o.m0.g.Http2.f2997e
            r3 = 1
            r4 = r11
            r5 = r1
            r6 = r8
            r7 = r14
            java.lang.String r2 = r2.a(r3, r4, r5, r6, r7)
            r12.fine(r2)
        L_0x005b:
            r2 = 5
            r3 = 8
            r4 = 1
            switch(r8) {
                case 0: goto L_0x0264;
                case 1: goto L_0x0231;
                case 2: goto L_0x0212;
                case 3: goto L_0x01d9;
                case 4: goto L_0x0139;
                case 5: goto L_0x010d;
                case 6: goto L_0x00df;
                case 7: goto L_0x0096;
                case 8: goto L_0x006a;
                default: goto L_0x0062;
            }
        L_0x0062:
            p.BufferedSource r14 = r13.d
            long r0 = (long) r1
            r14.skip(r0)
            goto L_0x02a2
        L_0x006a:
            if (r1 != r9) goto L_0x008a
            p.BufferedSource r14 = r13.d
            int r14 = r14.readInt()
            r0 = 2147483647(0x7fffffff, double:1.060997895E-314)
            long r2 = (long) r14
            long r0 = r0 & r2
            r2 = 0
            int r14 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r14 == 0) goto L_0x0082
            r15.a(r11, r0)
            goto L_0x02a2
        L_0x0082:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "windowSizeIncrement was 0"
            r14.<init>(r15)
            throw r14
        L_0x008a:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_WINDOW_UPDATE length !=4: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1)
            r14.<init>(r15)
            throw r14
        L_0x0096:
            if (r1 < r3) goto L_0x00d3
            if (r11 != 0) goto L_0x00cb
            p.BufferedSource r14 = r13.d
            int r14 = r14.readInt()
            p.BufferedSource r0 = r13.d
            int r0 = r0.readInt()
            int r1 = r1 - r3
            o.m0.g.ErrorCode$a r2 = o.m0.g.ErrorCode.Companion
            o.m0.g.ErrorCode r2 = r2.a(r0)
            if (r2 == 0) goto L_0x00bf
            p.ByteString r0 = p.ByteString.f3062e
            if (r1 <= 0) goto L_0x00ba
            p.BufferedSource r0 = r13.d
            long r5 = (long) r1
            p.ByteString r0 = r0.b(r5)
        L_0x00ba:
            r15.a(r14, r2, r0)
            goto L_0x02a2
        L_0x00bf:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_GOAWAY unexpected error code: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r0)
            r14.<init>(r15)
            throw r14
        L_0x00cb:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_GOAWAY streamId != 0"
            r14.<init>(r15)
            throw r14
        L_0x00d3:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_GOAWAY length < 8: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1)
            r14.<init>(r15)
            throw r14
        L_0x00df:
            if (r1 != r3) goto L_0x0101
            if (r11 != 0) goto L_0x00f9
            p.BufferedSource r1 = r13.d
            int r1 = r1.readInt()
            p.BufferedSource r2 = r13.d
            int r2 = r2.readInt()
            r14 = r14 & 1
            if (r14 == 0) goto L_0x00f4
            r0 = 1
        L_0x00f4:
            r15.a(r0, r1, r2)
            goto L_0x02a2
        L_0x00f9:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_PING streamId != 0"
            r14.<init>(r15)
            throw r14
        L_0x0101:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_PING length != 8: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1)
            r14.<init>(r15)
            throw r14
        L_0x010d:
            if (r11 == 0) goto L_0x0131
            r2 = r14 & 8
            if (r2 == 0) goto L_0x011b
            p.BufferedSource r0 = r13.d
            byte r0 = r0.readByte()
            r0 = r0 & 255(0xff, float:3.57E-43)
        L_0x011b:
            p.BufferedSource r2 = r13.d
            int r2 = r2.readInt()
            r2 = r2 & r10
            int r1 = r1 + -4
            int r1 = a(r1, r14, r0)
            java.util.List r14 = r13.a(r1, r0, r14, r11)
            r15.a(r11, r2, r14)
            goto L_0x02a2
        L_0x0131:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"
            r14.<init>(r15)
            throw r14
        L_0x0139:
            if (r11 != 0) goto L_0x01d1
            r14 = r14 & 1
            if (r14 == 0) goto L_0x014e
            if (r1 != 0) goto L_0x0146
            r15.a()
            goto L_0x02a2
        L_0x0146:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "FRAME_SIZE_ERROR ack frame should be empty!"
            r14.<init>(r15)
            throw r14
        L_0x014e:
            int r14 = r1 % 6
            if (r14 != 0) goto L_0x01c5
            o.m0.g.Settings r14 = new o.m0.g.Settings
            r14.<init>()
            n.o.Ranges0 r1 = n.o.d.b(r0, r1)
            r3 = 6
            n.o.Progressions r1 = n.o.d.a(r1, r3)
            int r3 = r1.b
            int r5 = r1.c
            int r1 = r1.d
            if (r1 < 0) goto L_0x016b
            if (r3 > r5) goto L_0x01c0
            goto L_0x016d
        L_0x016b:
            if (r3 < r5) goto L_0x01c0
        L_0x016d:
            p.BufferedSource r6 = r13.d
            short r6 = r6.readShort()
            r7 = 65535(0xffff, float:9.1834E-41)
            r6 = r6 & r7
            p.BufferedSource r7 = r13.d
            int r7 = r7.readInt()
            r8 = 2
            if (r6 == r8) goto L_0x01ac
            r8 = 3
            if (r6 == r8) goto L_0x01aa
            if (r6 == r9) goto L_0x019e
            r8 = 16384(0x4000, float:2.2959E-41)
            if (r6 == r2) goto L_0x018a
            goto L_0x01b9
        L_0x018a:
            if (r7 < r8) goto L_0x0192
            r8 = 16777215(0xffffff, float:2.3509886E-38)
            if (r7 > r8) goto L_0x0192
            goto L_0x01b9
        L_0x0192:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r7)
            r14.<init>(r15)
            throw r14
        L_0x019e:
            r6 = 7
            if (r7 < 0) goto L_0x01a2
            goto L_0x01b9
        L_0x01a2:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1"
            r14.<init>(r15)
            throw r14
        L_0x01aa:
            r6 = 4
            goto L_0x01b9
        L_0x01ac:
            if (r7 == 0) goto L_0x01b9
            if (r7 != r4) goto L_0x01b1
            goto L_0x01b9
        L_0x01b1:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1"
            r14.<init>(r15)
            throw r14
        L_0x01b9:
            r14.a(r6, r7)
            if (r3 == r5) goto L_0x01c0
            int r3 = r3 + r1
            goto L_0x016d
        L_0x01c0:
            r15.a(r0, r14)
            goto L_0x02a2
        L_0x01c5:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_SETTINGS length % 6 != 0: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1)
            r14.<init>(r15)
            throw r14
        L_0x01d1:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_SETTINGS streamId != 0"
            r14.<init>(r15)
            throw r14
        L_0x01d9:
            if (r1 != r9) goto L_0x0204
            if (r11 == 0) goto L_0x01fc
            p.BufferedSource r14 = r13.d
            int r14 = r14.readInt()
            o.m0.g.ErrorCode$a r0 = o.m0.g.ErrorCode.Companion
            o.m0.g.ErrorCode r0 = r0.a(r14)
            if (r0 == 0) goto L_0x01f0
            r15.a(r11, r0)
            goto L_0x02a2
        L_0x01f0:
            java.io.IOException r15 = new java.io.IOException
            java.lang.String r0 = "TYPE_RST_STREAM unexpected error code: "
            java.lang.String r14 = j.a.a.a.outline.b(r0, r14)
            r15.<init>(r14)
            throw r15
        L_0x01fc:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_RST_STREAM streamId == 0"
            r14.<init>(r15)
            throw r14
        L_0x0204:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_RST_STREAM length: "
            java.lang.String r0 = " != 4"
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1, r0)
            r14.<init>(r15)
            throw r14
        L_0x0212:
            if (r1 != r2) goto L_0x0223
            if (r11 == 0) goto L_0x021b
            r13.a(r15, r11)
            goto L_0x02a2
        L_0x021b:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_PRIORITY streamId == 0"
            r14.<init>(r15)
            throw r14
        L_0x0223:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "TYPE_PRIORITY length: "
            java.lang.String r0 = " != 5"
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1, r0)
            r14.<init>(r15)
            throw r14
        L_0x0231:
            if (r11 == 0) goto L_0x025c
            r2 = r14 & 1
            if (r2 == 0) goto L_0x0239
            r2 = 1
            goto L_0x023a
        L_0x0239:
            r2 = 0
        L_0x023a:
            r3 = r14 & 8
            if (r3 == 0) goto L_0x0246
            p.BufferedSource r0 = r13.d
            byte r0 = r0.readByte()
            r0 = r0 & 255(0xff, float:3.57E-43)
        L_0x0246:
            r3 = r14 & 32
            if (r3 == 0) goto L_0x024f
            r13.a(r15, r11)
            int r1 = r1 + -5
        L_0x024f:
            int r1 = a(r1, r14, r0)
            java.util.List r14 = r13.a(r1, r0, r14, r11)
            r0 = -1
            r15.a(r2, r11, r0, r14)
            goto L_0x02a2
        L_0x025c:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"
            r14.<init>(r15)
            throw r14
        L_0x0264:
            if (r11 == 0) goto L_0x029a
            r2 = r14 & 1
            if (r2 == 0) goto L_0x026c
            r2 = 1
            goto L_0x026d
        L_0x026c:
            r2 = 0
        L_0x026d:
            r3 = r14 & 32
            if (r3 == 0) goto L_0x0273
            r3 = 1
            goto L_0x0274
        L_0x0273:
            r3 = 0
        L_0x0274:
            if (r3 != 0) goto L_0x0292
            r3 = r14 & 8
            if (r3 == 0) goto L_0x0282
            p.BufferedSource r0 = r13.d
            byte r0 = r0.readByte()
            r0 = r0 & 255(0xff, float:3.57E-43)
        L_0x0282:
            int r14 = a(r1, r14, r0)
            p.BufferedSource r1 = r13.d
            r15.a(r2, r11, r1, r14)
            p.BufferedSource r14 = r13.d
            long r0 = (long) r0
            r14.skip(r0)
            goto L_0x02a2
        L_0x0292:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA"
            r14.<init>(r15)
            throw r14
        L_0x029a:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "PROTOCOL_ERROR: TYPE_DATA streamId == 0"
            r14.<init>(r15)
            throw r14
        L_0x02a2:
            return r4
        L_0x02a3:
            java.io.IOException r14 = new java.io.IOException
            java.lang.String r15 = "FRAME_SIZE_ERROR: "
            java.lang.String r15 = j.a.a.a.outline.b(r15, r1)
            r14.<init>(r15)
            throw r14
        L_0x02af:
            return r0
        L_0x02b0:
            java.lang.String r14 = "handler"
            n.n.c.Intrinsics.a(r14)
            r14 = 0
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Reader.a(boolean, o.m0.g.Http2Reader$b):boolean");
    }

    /* compiled from: Http2Reader.kt */
    public static final class a implements Source {
        public int b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f3023e;

        /* renamed from: f  reason: collision with root package name */
        public int f3024f;
        public final BufferedSource g;

        public a(BufferedSource bufferedSource) {
            if (bufferedSource != null) {
                this.g = bufferedSource;
            } else {
                Intrinsics.a("source");
                throw null;
            }
        }

        public long b(Buffer buffer, long j2) {
            int i2;
            int readInt;
            if (buffer != null) {
                do {
                    int i3 = this.f3023e;
                    if (i3 == 0) {
                        this.g.skip((long) this.f3024f);
                        this.f3024f = 0;
                        if ((this.c & 4) != 0) {
                            return -1;
                        }
                        i2 = this.d;
                        int a = Util.a(this.g);
                        this.f3023e = a;
                        this.b = a;
                        byte readByte = this.g.readByte() & 255;
                        this.c = this.g.readByte() & 255;
                        Http2Reader http2Reader = Http2Reader.g;
                        if (Http2Reader.f3021f.isLoggable(Level.FINE)) {
                            Http2Reader http2Reader2 = Http2Reader.g;
                            Http2Reader.f3021f.fine(Http2.f2997e.a(true, this.d, this.b, readByte, this.c));
                        }
                        readInt = this.g.readInt() & Integer.MAX_VALUE;
                        this.d = readInt;
                        if (readByte != 9) {
                            throw new IOException(((int) readByte) + " != TYPE_CONTINUATION");
                        }
                    } else {
                        long b2 = this.g.b(buffer, Math.min(j2, (long) i3));
                        if (b2 == -1) {
                            return -1;
                        }
                        this.f3023e -= (int) b2;
                        return b2;
                    }
                } while (readInt == i2);
                throw new IOException("TYPE_CONTINUATION streamId changed");
            }
            Intrinsics.a("sink");
            throw null;
        }

        public void close() {
        }

        public Timeout b() {
            return this.g.b();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<o.m0.g.b> a(int r2, int r3, int r4, int r5) {
        /*
            r1 = this;
            o.m0.g.Http2Reader$a r0 = r1.b
            r0.f3023e = r2
            r0.b = r2
            r0.f3024f = r3
            r0.c = r4
            r0.d = r5
            o.m0.g.Hpack$a r2 = r1.c
        L_0x000e:
            p.BufferedSource r3 = r2.b
            boolean r3 = r3.j()
            if (r3 != 0) goto L_0x013c
            p.BufferedSource r3 = r2.b
            byte r3 = r3.readByte()
            r4 = 255(0xff, float:3.57E-43)
            int r3 = o.m0.Util.a(r3, r4)
            r4 = 128(0x80, float:1.794E-43)
            if (r3 == r4) goto L_0x0134
            r5 = r3 & 128(0x80, float:1.794E-43)
            if (r5 != r4) goto L_0x0084
            r4 = 127(0x7f, float:1.78E-43)
            int r3 = r2.a(r3, r4)
            int r3 = r3 + -1
            if (r3 < 0) goto L_0x003f
            o.m0.g.Hpack r4 = o.m0.g.Hpack.c
            o.m0.g.Header[] r4 = o.m0.g.Hpack.a
            int r4 = r4.length
            int r4 = r4 + -1
            if (r3 > r4) goto L_0x003f
            r4 = 1
            goto L_0x0040
        L_0x003f:
            r4 = 0
        L_0x0040:
            if (r4 == 0) goto L_0x004e
            o.m0.g.Hpack r4 = o.m0.g.Hpack.c
            o.m0.g.Header[] r4 = o.m0.g.Hpack.a
            r3 = r4[r3]
            java.util.List<o.m0.g.b> r4 = r2.a
            r4.add(r3)
            goto L_0x000e
        L_0x004e:
            o.m0.g.Hpack r4 = o.m0.g.Hpack.c
            o.m0.g.Header[] r4 = o.m0.g.Hpack.a
            int r4 = r4.length
            int r4 = r3 - r4
            int r4 = r2.a(r4)
            if (r4 < 0) goto L_0x006f
            o.m0.g.Header[] r5 = r2.c
            int r0 = r5.length
            if (r4 >= r0) goto L_0x006f
            java.util.List<o.m0.g.b> r3 = r2.a
            r4 = r5[r4]
            if (r4 == 0) goto L_0x006a
            r3.add(r4)
            goto L_0x000e
        L_0x006a:
            n.n.c.Intrinsics.a()
            r2 = 0
            throw r2
        L_0x006f:
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r4 = "Header index too large "
            java.lang.StringBuilder r4 = j.a.a.a.outline.a(r4)
            int r3 = r3 + 1
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r2.<init>(r3)
            throw r2
        L_0x0084:
            r4 = -1
            r5 = 64
            if (r3 != r5) goto L_0x00a0
            o.m0.g.Hpack r3 = o.m0.g.Hpack.c
            p.ByteString r5 = r2.b()
            r3.a(r5)
            p.ByteString r3 = r2.b()
            o.m0.g.Header r0 = new o.m0.g.Header
            r0.<init>(r5, r3)
            r2.a(r4, r0)
            goto L_0x000e
        L_0x00a0:
            r0 = r3 & 64
            if (r0 != r5) goto L_0x00be
            r5 = 63
            int r3 = r2.a(r3, r5)
            int r3 = r3 + -1
            p.ByteString r3 = r2.c(r3)
            p.ByteString r5 = r2.b()
            o.m0.g.Header r0 = new o.m0.g.Header
            r0.<init>(r3, r5)
            r2.a(r4, r0)
            goto L_0x000e
        L_0x00be:
            r4 = r3 & 32
            r5 = 32
            if (r4 != r5) goto L_0x00f8
            r4 = 31
            int r3 = r2.a(r3, r4)
            r2.h = r3
            if (r3 < 0) goto L_0x00e3
            int r4 = r2.g
            if (r3 > r4) goto L_0x00e3
            int r4 = r2.f2992f
            if (r3 >= r4) goto L_0x000e
            if (r3 != 0) goto L_0x00dd
            r2.a()
            goto L_0x000e
        L_0x00dd:
            int r4 = r4 - r3
            r2.b(r4)
            goto L_0x000e
        L_0x00e3:
            java.io.IOException r3 = new java.io.IOException
            java.lang.String r4 = "Invalid dynamic table size update "
            java.lang.StringBuilder r4 = j.a.a.a.outline.a(r4)
            int r2 = r2.h
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            r3.<init>(r2)
            throw r3
        L_0x00f8:
            r4 = 16
            if (r3 == r4) goto L_0x011b
            if (r3 != 0) goto L_0x00ff
            goto L_0x011b
        L_0x00ff:
            r4 = 15
            int r3 = r2.a(r3, r4)
            int r3 = r3 + -1
            p.ByteString r3 = r2.c(r3)
            p.ByteString r4 = r2.b()
            java.util.List<o.m0.g.b> r5 = r2.a
            o.m0.g.Header r0 = new o.m0.g.Header
            r0.<init>(r3, r4)
            r5.add(r0)
            goto L_0x000e
        L_0x011b:
            o.m0.g.Hpack r3 = o.m0.g.Hpack.c
            p.ByteString r4 = r2.b()
            r3.a(r4)
            p.ByteString r3 = r2.b()
            java.util.List<o.m0.g.b> r5 = r2.a
            o.m0.g.Header r0 = new o.m0.g.Header
            r0.<init>(r4, r3)
            r5.add(r0)
            goto L_0x000e
        L_0x0134:
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r3 = "index == 0"
            r2.<init>(r3)
            throw r2
        L_0x013c:
            o.m0.g.Hpack$a r2 = r1.c
            java.util.List<o.m0.g.b> r3 = r2.a
            java.util.List r3 = n.i._Arrays.a(r3)
            java.util.List<o.m0.g.b> r2 = r2.a
            r2.clear()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Reader.a(int, int, int, int):java.util.List");
    }

    public final void a(b bVar, int i2) {
        int readInt = this.d.readInt();
        bVar.a(i2, readInt & Integer.MAX_VALUE, Util.a(this.d.readByte(), 255) + 1, (readInt & ((int) 2147483648L)) != 0);
    }

    public static final int a(int i2, int i3, int i4) {
        if ((i3 & 8) != 0) {
            i2--;
        }
        if (i4 <= i2) {
            return i2 - i4;
        }
        throw new IOException(outline.a("PROTOCOL_ERROR padding ", i4, " > remaining length ", i2));
    }
}
