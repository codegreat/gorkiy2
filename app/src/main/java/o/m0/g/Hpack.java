package o.m0.g;

import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import n.i._Arrays;
import n.n.c.Intrinsics;
import o.m0.Util;
import o.m0.g.Huffman;
import p.Buffer;
import p.BufferedSource;
import p.ByteString;
import p.Source;
import p.j;

/* compiled from: Hpack.kt */
public final class Hpack {
    public static final Header[] a = {new Header(Header.f2990i, ""), new Header(Header.f2989f, "GET"), new Header(Header.f2989f, "POST"), new Header(Header.g, "/"), new Header(Header.g, "/index.html"), new Header(Header.h, "http"), new Header(Header.h, "https"), new Header(Header.f2988e, "200"), new Header(Header.f2988e, "204"), new Header(Header.f2988e, "206"), new Header(Header.f2988e, "304"), new Header(Header.f2988e, "400"), new Header(Header.f2988e, "404"), new Header(Header.f2988e, "500"), new Header("accept-charset", ""), new Header("accept-encoding", "gzip, deflate"), new Header("accept-language", ""), new Header("accept-ranges", ""), new Header("accept", ""), new Header("access-control-allow-origin", ""), new Header("age", ""), new Header("allow", ""), new Header("authorization", ""), new Header("cache-control", ""), new Header("content-disposition", ""), new Header("content-encoding", ""), new Header("content-language", ""), new Header("content-length", ""), new Header("content-location", ""), new Header("content-range", ""), new Header("content-type", ""), new Header("cookie", ""), new Header("date", ""), new Header("etag", ""), new Header("expect", ""), new Header("expires", ""), new Header("from", ""), new Header("host", ""), new Header("if-match", ""), new Header("if-modified-since", ""), new Header("if-none-match", ""), new Header("if-range", ""), new Header("if-unmodified-since", ""), new Header("last-modified", ""), new Header("link", ""), new Header("location", ""), new Header("max-forwards", ""), new Header("proxy-authenticate", ""), new Header("proxy-authorization", ""), new Header("range", ""), new Header("referer", ""), new Header("refresh", ""), new Header("retry-after", ""), new Header("server", ""), new Header("set-cookie", ""), new Header("strict-transport-security", ""), new Header("transfer-encoding", ""), new Header("user-agent", ""), new Header("vary", ""), new Header("via", ""), new Header("www-authenticate", "")};
    public static final Map<j, Integer> b;
    public static final Hpack c = new Hpack();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<p.j, java.lang.Integer>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    static {
        LinkedHashMap linkedHashMap = new LinkedHashMap(a.length);
        int length = a.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (!linkedHashMap.containsKey(a[i2].b)) {
                linkedHashMap.put(a[i2].b, Integer.valueOf(i2));
            }
        }
        Map<j, Integer> unmodifiableMap = Collections.unmodifiableMap(linkedHashMap);
        Intrinsics.a((Object) unmodifiableMap, "Collections.unmodifiableMap(result)");
        b = unmodifiableMap;
    }

    public final ByteString a(ByteString byteString) {
        if (byteString != null) {
            int g = byteString.g();
            for (int i2 = 0; i2 < g; i2++) {
                byte b2 = (byte) 65;
                byte b3 = (byte) 90;
                byte a2 = byteString.a(i2);
                if (b2 <= a2 && b3 >= a2) {
                    StringBuilder a3 = outline.a("PROTOCOL_ERROR response malformed: mixed case name: ");
                    a3.append(p.b0.ByteString.h(byteString));
                    throw new IOException(a3.toString());
                }
            }
            return byteString;
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    /* compiled from: Hpack.kt */
    public static final class a {
        public final List<b> a;
        public final BufferedSource b;
        public Header[] c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f2991e;

        /* renamed from: f  reason: collision with root package name */
        public int f2992f;
        public final int g;
        public int h;

        public /* synthetic */ a(Source source, int i2, int i3, int i4) {
            i3 = (i4 & 4) != 0 ? i2 : i3;
            if (source != null) {
                this.g = i2;
                this.h = i3;
                this.a = new ArrayList();
                this.b = n.i.Collections.a(source);
                this.c = new Header[8];
                this.d = 7;
                return;
            }
            Intrinsics.a("source");
            throw null;
        }

        public final void a() {
            _Arrays.a(this.c, null, 0, 0, 6);
            this.d = this.c.length - 1;
            this.f2991e = 0;
            this.f2992f = 0;
        }

        public final int b(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.c.length;
                while (true) {
                    length--;
                    if (length < this.d || i2 <= 0) {
                        Header[] headerArr = this.c;
                        int i4 = this.d;
                        System.arraycopy(headerArr, i4 + 1, headerArr, i4 + 1 + i3, this.f2991e);
                        this.d += i3;
                    } else {
                        Header header = this.c[length];
                        if (header != null) {
                            int i5 = header.a;
                            i2 -= i5;
                            this.f2992f -= i5;
                            this.f2991e--;
                            i3++;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                }
                Header[] headerArr2 = this.c;
                int i42 = this.d;
                System.arraycopy(headerArr2, i42 + 1, headerArr2, i42 + 1 + i3, this.f2991e);
                this.d += i3;
            }
            return i3;
        }

        /* JADX WARNING: Removed duplicated region for block: B:6:0x0010  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final p.ByteString c(int r4) {
            /*
                r3 = this;
                if (r4 < 0) goto L_0x000d
                o.m0.g.Hpack r0 = o.m0.g.Hpack.c
                o.m0.g.Header[] r0 = o.m0.g.Hpack.a
                int r0 = r0.length
                int r0 = r0 + -1
                if (r4 > r0) goto L_0x000d
                r0 = 1
                goto L_0x000e
            L_0x000d:
                r0 = 0
            L_0x000e:
                if (r0 == 0) goto L_0x0019
                o.m0.g.Hpack r0 = o.m0.g.Hpack.c
                o.m0.g.Header[] r0 = o.m0.g.Hpack.a
                r4 = r0[r4]
                p.ByteString r4 = r4.b
                goto L_0x0031
            L_0x0019:
                o.m0.g.Hpack r0 = o.m0.g.Hpack.c
                o.m0.g.Header[] r0 = o.m0.g.Hpack.a
                int r0 = r0.length
                int r0 = r4 - r0
                int r0 = r3.a(r0)
                if (r0 < 0) goto L_0x0037
                o.m0.g.Header[] r1 = r3.c
                int r2 = r1.length
                if (r0 >= r2) goto L_0x0037
                r4 = r1[r0]
                if (r4 == 0) goto L_0x0032
                p.ByteString r4 = r4.b
            L_0x0031:
                return r4
            L_0x0032:
                n.n.c.Intrinsics.a()
                r4 = 0
                throw r4
            L_0x0037:
                java.io.IOException r0 = new java.io.IOException
                java.lang.String r1 = "Header index too large "
                java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
                int r4 = r4 + 1
                r1.append(r4)
                java.lang.String r4 = r1.toString()
                r0.<init>(r4)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Hpack.a.c(int):p.ByteString");
        }

        public final int a(int i2) {
            return this.d + 1 + i2;
        }

        public final void a(int i2, Header header) {
            this.a.add(header);
            int i3 = header.a;
            if (i2 != -1) {
                Header header2 = this.c[this.d + 1 + i2];
                if (header2 != null) {
                    i3 -= header2.a;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            int i4 = this.h;
            if (i3 > i4) {
                a();
                return;
            }
            int b2 = b((this.f2992f + i3) - i4);
            if (i2 == -1) {
                int i5 = this.f2991e + 1;
                Header[] headerArr = this.c;
                if (i5 > headerArr.length) {
                    Header[] headerArr2 = new Header[(headerArr.length * 2)];
                    System.arraycopy(headerArr, 0, headerArr2, headerArr.length, headerArr.length);
                    this.d = this.c.length - 1;
                    this.c = headerArr2;
                }
                int i6 = this.d;
                this.d = i6 - 1;
                this.c[i6] = header;
                this.f2991e++;
            } else {
                this.c[this.d + 1 + i2 + b2 + i2] = header;
            }
            this.f2992f += i3;
        }

        public final ByteString b() {
            int a2 = Util.a(this.b.readByte(), 255);
            byte b2 = 0;
            boolean z = (a2 & 128) == 128;
            long a3 = (long) a(a2, 127);
            if (!z) {
                return this.b.b(a3);
            }
            Buffer buffer = new Buffer();
            Huffman huffman = Huffman.d;
            BufferedSource bufferedSource = this.b;
            if (bufferedSource != null) {
                Huffman.a aVar = Huffman.c;
                int i2 = 0;
                for (long j2 = 0; j2 < a3; j2++) {
                    b2 = (b2 << 8) | (bufferedSource.readByte() & 255);
                    i2 += 8;
                    while (i2 >= 8) {
                        int i3 = i2 - 8;
                        int i4 = (b2 >>> i3) & 255;
                        Huffman.a[] aVarArr = aVar.a;
                        if (aVarArr != null) {
                            aVar = aVarArr[i4];
                            if (aVar == null) {
                                Intrinsics.a();
                                throw null;
                            } else if (aVar.a == null) {
                                buffer.writeByte(aVar.b);
                                i2 -= aVar.c;
                                aVar = Huffman.c;
                            } else {
                                i2 = i3;
                            }
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                }
                while (i2 > 0) {
                    int i5 = (b2 << (8 - i2)) & 255;
                    Huffman.a[] aVarArr2 = aVar.a;
                    if (aVarArr2 != null) {
                        Huffman.a aVar2 = aVarArr2[i5];
                        if (aVar2 != null) {
                            if (aVar2.a != null || aVar2.c > i2) {
                                break;
                            }
                            buffer.writeByte(aVar2.b);
                            i2 -= aVar2.c;
                            aVar = Huffman.c;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return buffer.l();
            }
            Intrinsics.a("source");
            throw null;
        }

        public final int a(int i2, int i3) {
            int i4 = i2 & i3;
            if (i4 < i3) {
                return i4;
            }
            int i5 = 0;
            while (true) {
                int a2 = Util.a(this.b.readByte(), 255);
                if ((a2 & 128) == 0) {
                    return i3 + (a2 << i5);
                }
                i3 += (a2 & 127) << i5;
                i5 += 7;
            }
        }
    }

    /* compiled from: Hpack.kt */
    public static final class b {
        public int a;
        public boolean b;
        public int c;
        public Header[] d;

        /* renamed from: e  reason: collision with root package name */
        public int f2993e;

        /* renamed from: f  reason: collision with root package name */
        public int f2994f;
        public int g;
        public int h;

        /* renamed from: i  reason: collision with root package name */
        public final boolean f2995i;

        /* renamed from: j  reason: collision with root package name */
        public final Buffer f2996j;

        public /* synthetic */ b(int i2, boolean z, Buffer buffer, int i3) {
            i2 = (i3 & 1) != 0 ? CodedOutputStream.DEFAULT_BUFFER_SIZE : i2;
            z = (i3 & 2) != 0 ? true : z;
            if (buffer != null) {
                this.h = i2;
                this.f2995i = z;
                this.f2996j = buffer;
                this.a = Integer.MAX_VALUE;
                this.c = i2;
                this.d = new Header[8];
                this.f2993e = 7;
                return;
            }
            Intrinsics.a("out");
            throw null;
        }

        public final void a() {
            _Arrays.a(this.d, null, 0, 0, 6);
            this.f2993e = this.d.length - 1;
            this.f2994f = 0;
            this.g = 0;
        }

        public final int a(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.d.length;
                while (true) {
                    length--;
                    if (length < this.f2993e || i2 <= 0) {
                        Header[] headerArr = this.d;
                        int i4 = this.f2993e;
                        System.arraycopy(headerArr, i4 + 1, headerArr, i4 + 1 + i3, this.f2994f);
                        Header[] headerArr2 = this.d;
                        int i5 = this.f2993e;
                        Arrays.fill(headerArr2, i5 + 1, i5 + 1 + i3, (Object) null);
                        this.f2993e += i3;
                    } else {
                        Header[] headerArr3 = this.d;
                        Header header = headerArr3[length];
                        if (header != null) {
                            i2 -= header.a;
                            int i6 = this.g;
                            Header header2 = headerArr3[length];
                            if (header2 != null) {
                                this.g = i6 - header2.a;
                                this.f2994f--;
                                i3++;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                }
                Header[] headerArr4 = this.d;
                int i42 = this.f2993e;
                System.arraycopy(headerArr4, i42 + 1, headerArr4, i42 + 1 + i3, this.f2994f);
                Header[] headerArr22 = this.d;
                int i52 = this.f2993e;
                Arrays.fill(headerArr22, i52 + 1, i52 + 1 + i3, (Object) null);
                this.f2993e += i3;
            }
            return i3;
        }

        public final void a(Header header) {
            int i2 = header.a;
            int i3 = this.c;
            if (i2 > i3) {
                a();
                return;
            }
            a((this.g + i2) - i3);
            int i4 = this.f2994f + 1;
            Header[] headerArr = this.d;
            if (i4 > headerArr.length) {
                Header[] headerArr2 = new Header[(headerArr.length * 2)];
                System.arraycopy(headerArr, 0, headerArr2, headerArr.length, headerArr.length);
                this.f2993e = this.d.length - 1;
                this.d = headerArr2;
            }
            int i5 = this.f2993e;
            this.f2993e = i5 - 1;
            this.d[i5] = header;
            this.f2994f++;
            this.g += i2;
        }

        public final void a(List<b> list) {
            int i2;
            int i3;
            if (list != null) {
                if (this.b) {
                    int i4 = this.a;
                    if (i4 < this.c) {
                        a(i4, 31, 32);
                    }
                    this.b = false;
                    this.a = Integer.MAX_VALUE;
                    a(this.c, 31, 32);
                }
                int size = list.size();
                for (int i5 = 0; i5 < size; i5++) {
                    Header header = list.get(i5);
                    ByteString j2 = header.b.j();
                    ByteString byteString = header.c;
                    Hpack hpack = Hpack.c;
                    Integer num = Hpack.b.get(j2);
                    if (num != null) {
                        i3 = num.intValue() + 1;
                        if (2 <= i3 && 7 >= i3) {
                            Hpack hpack2 = Hpack.c;
                            if (Intrinsics.a(Hpack.a[i3 - 1].c, byteString)) {
                                i2 = i3;
                            } else {
                                Hpack hpack3 = Hpack.c;
                                if (Intrinsics.a(Hpack.a[i3].c, byteString)) {
                                    i2 = i3;
                                    i3++;
                                }
                            }
                        }
                        i2 = i3;
                        i3 = -1;
                    } else {
                        i3 = -1;
                        i2 = -1;
                    }
                    if (i3 == -1) {
                        int i6 = this.f2993e + 1;
                        int length = this.d.length;
                        while (true) {
                            if (i6 >= length) {
                                break;
                            }
                            Header header2 = this.d[i6];
                            if (header2 != null) {
                                if (Intrinsics.a(header2.b, j2)) {
                                    Header header3 = this.d[i6];
                                    if (header3 == null) {
                                        Intrinsics.a();
                                        throw null;
                                    } else if (Intrinsics.a(header3.c, byteString)) {
                                        int i7 = i6 - this.f2993e;
                                        Hpack hpack4 = Hpack.c;
                                        i3 = Hpack.a.length + i7;
                                        break;
                                    } else if (i2 == -1) {
                                        Hpack hpack5 = Hpack.c;
                                        i2 = (i6 - this.f2993e) + Hpack.a.length;
                                    }
                                }
                                i6++;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                    }
                    if (i3 != -1) {
                        a(i3, 127, 128);
                    } else if (i2 == -1) {
                        this.f2996j.writeByte(64);
                        a(j2);
                        a(byteString);
                        a(header);
                    } else {
                        ByteString byteString2 = Header.d;
                        if (j2 == null) {
                            throw null;
                        } else if (byteString2 == null) {
                            Intrinsics.a("prefix");
                            throw null;
                        } else if (!p.b0.ByteString.b(j2, byteString2) || !(!Intrinsics.a(Header.f2990i, j2))) {
                            a(i2, 63, 64);
                            a(byteString);
                            a(header);
                        } else {
                            a(i2, 15, 0);
                            a(byteString);
                        }
                    }
                }
                return;
            }
            Intrinsics.a("headerBlock");
            throw null;
        }

        public final void a(int i2, int i3, int i4) {
            if (i2 < i3) {
                this.f2996j.writeByte(i2 | i4);
                return;
            }
            this.f2996j.writeByte(i4 | i3);
            int i5 = i2 - i3;
            while (i5 >= 128) {
                this.f2996j.writeByte(128 | (i5 & 127));
                i5 >>>= 7;
            }
            this.f2996j.writeByte(i5);
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: long} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: byte} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(p.ByteString r13) {
            /*
                r12 = this;
                if (r13 == 0) goto L_0x0096
                boolean r0 = r12.f2995i
                r1 = 127(0x7f, float:1.78E-43)
                r2 = 0
                if (r0 == 0) goto L_0x0089
                o.m0.g.Huffman r0 = o.m0.g.Huffman.d
                int r0 = r13.g()
                r3 = 0
                r6 = r3
                r5 = 0
            L_0x0013:
                r8 = 255(0xff, float:3.57E-43)
                if (r5 >= r0) goto L_0x0028
                byte r9 = r13.a(r5)
                int r8 = o.m0.Util.a(r9, r8)
                byte[] r9 = o.m0.g.Huffman.b
                byte r8 = r9[r8]
                long r8 = (long) r8
                long r6 = r6 + r8
                int r5 = r5 + 1
                goto L_0x0013
            L_0x0028:
                r0 = 7
                long r9 = (long) r0
                long r6 = r6 + r9
                r0 = 3
                long r5 = r6 >> r0
                int r0 = (int) r5
                int r5 = r13.g()
                if (r0 >= r5) goto L_0x0089
                p.Buffer r0 = new p.Buffer
                r0.<init>()
                o.m0.g.Huffman r5 = o.m0.g.Huffman.d
                int r5 = r13.g()
                r6 = r3
                r3 = 0
            L_0x0042:
                r4 = 8
                if (r2 >= r5) goto L_0x0068
                byte r9 = r13.a(r2)
                int r9 = o.m0.Util.a(r9, r8)
                int[] r10 = o.m0.g.Huffman.a
                r10 = r10[r9]
                byte[] r11 = o.m0.g.Huffman.b
                byte r9 = r11[r9]
                long r6 = r6 << r9
                long r10 = (long) r10
                long r6 = r6 | r10
                int r3 = r3 + r9
            L_0x005a:
                if (r3 < r4) goto L_0x0065
                int r3 = r3 + -8
                long r9 = r6 >> r3
                int r10 = (int) r9
                r0.writeByte(r10)
                goto L_0x005a
            L_0x0065:
                int r2 = r2 + 1
                goto L_0x0042
            L_0x0068:
                if (r3 <= 0) goto L_0x0076
                int r4 = r4 - r3
                long r4 = r6 << r4
                r6 = 255(0xff, double:1.26E-321)
                long r2 = r6 >>> r3
                long r2 = r2 | r4
                int r13 = (int) r2
                r0.writeByte(r13)
            L_0x0076:
                p.ByteString r13 = r0.l()
                int r0 = r13.g()
                r2 = 128(0x80, float:1.794E-43)
                r12.a(r0, r1, r2)
                p.Buffer r0 = r12.f2996j
                r0.a(r13)
                goto L_0x0095
            L_0x0089:
                int r0 = r13.g()
                r12.a(r0, r1, r2)
                p.Buffer r0 = r12.f2996j
                r0.a(r13)
            L_0x0095:
                return
            L_0x0096:
                java.lang.String r13 = "data"
                n.n.c.Intrinsics.a(r13)
                r13 = 0
                throw r13
            */
            throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Hpack.b.a(p.ByteString):void");
        }
    }
}
