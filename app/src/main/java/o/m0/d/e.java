package o.m0.d;

import java.net.Proxy;
import o.m0.g.ErrorCode;

public final /* synthetic */ class e {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[Proxy.Type.values().length];
        a = iArr;
        iArr[Proxy.Type.DIRECT.ordinal()] = 1;
        a[Proxy.Type.HTTP.ordinal()] = 2;
        int[] iArr2 = new int[ErrorCode.values().length];
        b = iArr2;
        ErrorCode errorCode = ErrorCode.REFUSED_STREAM;
        iArr2[4] = 1;
        int[] iArr3 = b;
        ErrorCode errorCode2 = ErrorCode.CANCEL;
        iArr3[5] = 2;
    }
}
