package o.m0.d;

import n.AssertionsJVM;
import n.n.c.Intrinsics;
import o.Address;
import o.Call;
import o.EventListener;
import o.Route;
import o.m0.Util;
import o.m0.d.RouteSelector;

/* compiled from: ExchangeFinder.kt */
public final class ExchangeFinder {
    public RouteSelector.a a;
    public final RouteSelector b;
    public RealConnection c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public Route f2945e;

    /* renamed from: f  reason: collision with root package name */
    public final Transmitter f2946f;
    public final RealConnectionPool g;
    public final Address h;

    /* renamed from: i  reason: collision with root package name */
    public final Call f2947i;

    /* renamed from: j  reason: collision with root package name */
    public final EventListener f2948j;

    public ExchangeFinder(Transmitter transmitter, RealConnectionPool realConnectionPool, Address address, Call call, EventListener eventListener) {
        if (transmitter == null) {
            Intrinsics.a("transmitter");
            throw null;
        } else if (realConnectionPool == null) {
            Intrinsics.a("connectionPool");
            throw null;
        } else if (address == null) {
            Intrinsics.a("address");
            throw null;
        } else if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (eventListener != null) {
            this.f2946f = transmitter;
            this.g = realConnectionPool;
            this.h = address;
            this.f2947i = call;
            this.f2948j = eventListener;
            this.b = new RouteSelector(this.h, this.g.d, this.f2947i, this.f2948j);
        } else {
            Intrinsics.a("eventListener");
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        r3 = r0.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        if (r3 == null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        if (r1.isClosed() != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        if (r1.isInputShutdown() != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        if (r1.isOutputShutdown() == false) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        r2 = r0.f2950f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002e, code lost:
        if (r2 == null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0030, code lost:
        r4 = !r2.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
        if (r12 == false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r2 = r1.getSoTimeout();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r1.setSoTimeout(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0044, code lost:
        r3 = !r3.j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1.setSoTimeout(r2);
        r4 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004b, code lost:
        r1.setSoTimeout(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004e, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0051, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0059, code lost:
        n.n.c.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005c, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005d, code lost:
        n.n.c.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0060, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        r1 = r0.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r1 == null) goto L_0x005d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final o.m0.d.RealConnection a(int r7, int r8, int r9, int r10, boolean r11, boolean r12) {
        /*
            r6 = this;
        L_0x0000:
            o.m0.d.RealConnection r0 = r6.a(r7, r8, r9, r10, r11)
            o.m0.d.RealConnectionPool r1 = r6.g
            monitor-enter(r1)
            int r2 = r0.f2953k     // Catch:{ all -> 0x0061 }
            if (r2 != 0) goto L_0x000d
            monitor-exit(r1)
            return r0
        L_0x000d:
            monitor-exit(r1)
            java.net.Socket r1 = r0.c
            r2 = 0
            if (r1 == 0) goto L_0x005d
            p.BufferedSource r3 = r0.g
            if (r3 == 0) goto L_0x0059
            boolean r2 = r1.isClosed()
            r4 = 0
            r5 = 1
            if (r2 != 0) goto L_0x0052
            boolean r2 = r1.isInputShutdown()
            if (r2 != 0) goto L_0x0052
            boolean r2 = r1.isOutputShutdown()
            if (r2 == 0) goto L_0x002c
            goto L_0x0052
        L_0x002c:
            o.m0.g.Http2Connection r2 = r0.f2950f
            if (r2 == 0) goto L_0x0037
            boolean r1 = r2.a()
            r4 = r1 ^ 1
            goto L_0x0052
        L_0x0037:
            if (r12 == 0) goto L_0x0051
            int r2 = r1.getSoTimeout()     // Catch:{ SocketTimeoutException -> 0x0051, IOException -> 0x004f }
            r1.setSoTimeout(r5)     // Catch:{ all -> 0x004a }
            boolean r3 = r3.j()     // Catch:{ all -> 0x004a }
            r3 = r3 ^ r5
            r1.setSoTimeout(r2)     // Catch:{ SocketTimeoutException -> 0x0051, IOException -> 0x004f }
            r4 = r3
            goto L_0x0052
        L_0x004a:
            r3 = move-exception
            r1.setSoTimeout(r2)     // Catch:{ SocketTimeoutException -> 0x0051, IOException -> 0x004f }
            throw r3     // Catch:{ SocketTimeoutException -> 0x0051, IOException -> 0x004f }
        L_0x004f:
            goto L_0x0052
        L_0x0051:
            r4 = 1
        L_0x0052:
            if (r4 != 0) goto L_0x0058
            r0.c()
            goto L_0x0000
        L_0x0058:
            return r0
        L_0x0059:
            n.n.c.Intrinsics.a()
            throw r2
        L_0x005d:
            n.n.c.Intrinsics.a()
            throw r2
        L_0x0061:
            r7 = move-exception
            monitor-exit(r1)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.ExchangeFinder.a(int, int, int, int, boolean, boolean):o.m0.d.RealConnection");
    }

    public final boolean b() {
        boolean z;
        synchronized (this.g) {
            z = this.d;
        }
        return z;
    }

    public final boolean c() {
        RealConnection realConnection = this.f2946f.g;
        if (realConnection != null) {
            if (realConnection == null) {
                Intrinsics.a();
                throw null;
            } else if (realConnection.f2952j == 0) {
                if (realConnection == null) {
                    Intrinsics.a();
                    throw null;
                } else if (Util.a(realConnection.f2959q.a.a, this.h.a)) {
                    return true;
                }
            }
        }
        return false;
    }

    public final void d() {
        boolean z = !Thread.holdsLock(this.g);
        if (!AssertionsJVM.a || z) {
            synchronized (this.g) {
                this.d = true;
            }
            return;
        }
        throw new AssertionError("Assertion failed");
    }

    /* JADX WARN: Type inference failed for: r0v23, types: [java.lang.Object] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.List<o.k0>]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final o.m0.d.RealConnection a(int r19, int r20, int r21, int r22, boolean r23) {
        /*
            r18 = this;
            r1 = r18
            o.m0.d.RealConnectionPool r2 = r1.g
            monitor-enter(r2)
            o.m0.d.Transmitter r0 = r1.f2946f     // Catch:{ all -> 0x0346 }
            boolean r0 = r0.e()     // Catch:{ all -> 0x0346 }
            if (r0 != 0) goto L_0x033e
            r0 = 0
            r1.d = r0     // Catch:{ all -> 0x0346 }
            o.m0.d.Transmitter r3 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r3 = r3.g     // Catch:{ all -> 0x0346 }
            o.m0.d.Transmitter r4 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r4 = r4.g     // Catch:{ all -> 0x0346 }
            r5 = 0
            if (r4 == 0) goto L_0x0030
            o.m0.d.Transmitter r4 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r4 = r4.g     // Catch:{ all -> 0x0346 }
            if (r4 == 0) goto L_0x002c
            boolean r4 = r4.f2951i     // Catch:{ all -> 0x0346 }
            if (r4 == 0) goto L_0x0030
            o.m0.d.Transmitter r4 = r1.f2946f     // Catch:{ all -> 0x0346 }
            java.net.Socket r4 = r4.f()     // Catch:{ all -> 0x0346 }
            goto L_0x0031
        L_0x002c:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0346 }
            throw r5
        L_0x0030:
            r4 = r5
        L_0x0031:
            o.m0.d.Transmitter r6 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r6 = r6.g     // Catch:{ all -> 0x0346 }
            if (r6 == 0) goto L_0x003d
            o.m0.d.Transmitter r3 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r3 = r3.g     // Catch:{ all -> 0x0346 }
            r6 = r5
            goto L_0x003f
        L_0x003d:
            r6 = r3
            r3 = r5
        L_0x003f:
            r7 = 1
            if (r3 != 0) goto L_0x0072
            o.m0.d.RealConnectionPool r8 = r1.g     // Catch:{ all -> 0x0346 }
            o.Address r9 = r1.h     // Catch:{ all -> 0x0346 }
            o.m0.d.Transmitter r10 = r1.f2946f     // Catch:{ all -> 0x0346 }
            boolean r8 = r8.a(r9, r10, r5, r0)     // Catch:{ all -> 0x0346 }
            if (r8 == 0) goto L_0x0056
            o.m0.d.Transmitter r3 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r3 = r3.g     // Catch:{ all -> 0x0346 }
            r8 = 1
            r8 = r5
            r9 = 1
            goto L_0x0074
        L_0x0056:
            o.Route r8 = r1.f2945e     // Catch:{ all -> 0x0346 }
            if (r8 == 0) goto L_0x005f
            o.Route r8 = r1.f2945e     // Catch:{ all -> 0x0346 }
            r1.f2945e = r5     // Catch:{ all -> 0x0346 }
            goto L_0x0073
        L_0x005f:
            boolean r8 = r18.c()     // Catch:{ all -> 0x0346 }
            if (r8 == 0) goto L_0x0072
            o.m0.d.Transmitter r8 = r1.f2946f     // Catch:{ all -> 0x0346 }
            o.m0.d.RealConnection r8 = r8.g     // Catch:{ all -> 0x0346 }
            if (r8 == 0) goto L_0x006e
            o.Route r8 = r8.f2959q     // Catch:{ all -> 0x0346 }
            goto L_0x0073
        L_0x006e:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0346 }
            throw r5
        L_0x0072:
            r8 = r5
        L_0x0073:
            r9 = 0
        L_0x0074:
            monitor-exit(r2)
            if (r4 == 0) goto L_0x007a
            o.m0.Util.a(r4)
        L_0x007a:
            if (r6 == 0) goto L_0x0092
            o.EventListener r2 = r1.f2948j
            o.Call r4 = r1.f2947i
            if (r6 == 0) goto L_0x008e
            if (r2 == 0) goto L_0x008d
            if (r4 == 0) goto L_0x0087
            goto L_0x0092
        L_0x0087:
            java.lang.String r0 = "call"
            n.n.c.Intrinsics.a(r0)
            throw r5
        L_0x008d:
            throw r5
        L_0x008e:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x0092:
            if (r9 == 0) goto L_0x00a2
            o.EventListener r2 = r1.f2948j
            o.Call r4 = r1.f2947i
            if (r3 == 0) goto L_0x009e
            r2.a(r4, r3)
            goto L_0x00a2
        L_0x009e:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x00a2:
            if (r3 == 0) goto L_0x00a5
            return r3
        L_0x00a5:
            if (r8 != 0) goto L_0x025a
            o.m0.d.RouteSelector$a r2 = r1.a
            if (r2 == 0) goto L_0x00b1
            boolean r2 = r2.a()
            if (r2 != 0) goto L_0x025a
        L_0x00b1:
            o.m0.d.RouteSelector r2 = r1.b
            boolean r4 = r2.a()
            if (r4 == 0) goto L_0x0254
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
        L_0x00be:
            boolean r6 = r2.b()
            if (r6 == 0) goto L_0x023b
            boolean r6 = r2.b()
            java.lang.String r10 = "No route to "
            if (r6 == 0) goto L_0x021a
            java.util.List<? extends java.net.Proxy> r6 = r2.a
            int r11 = r2.b
            int r12 = r11 + 1
            r2.b = r12
            java.lang.Object r6 = r6.get(r11)
            java.net.Proxy r6 = (java.net.Proxy) r6
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r2.c = r11
            java.net.Proxy$Type r12 = r6.type()
            java.net.Proxy$Type r13 = java.net.Proxy.Type.DIRECT
            if (r12 == r13) goto L_0x013d
            java.net.Proxy$Type r12 = r6.type()
            java.net.Proxy$Type r13 = java.net.Proxy.Type.SOCKS
            if (r12 != r13) goto L_0x00f2
            goto L_0x013d
        L_0x00f2:
            java.net.SocketAddress r12 = r6.address()
            boolean r13 = r12 instanceof java.net.InetSocketAddress
            if (r13 == 0) goto L_0x0122
            java.net.InetSocketAddress r12 = (java.net.InetSocketAddress) r12
            if (r12 == 0) goto L_0x011c
            java.net.InetAddress r13 = r12.getAddress()
            if (r13 == 0) goto L_0x010e
            java.lang.String r13 = r13.getHostAddress()
            java.lang.String r14 = "address.hostAddress"
            n.n.c.Intrinsics.a(r13, r14)
            goto L_0x0117
        L_0x010e:
            java.lang.String r13 = r12.getHostName()
            java.lang.String r14 = "hostName"
            n.n.c.Intrinsics.a(r13, r14)
        L_0x0117:
            int r12 = r12.getPort()
            goto L_0x0145
        L_0x011c:
            java.lang.String r0 = "$this$socketHost"
            n.n.c.Intrinsics.a(r0)
            throw r5
        L_0x0122:
            java.lang.String r0 = "Proxy.address() is not an InetSocketAddress: "
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            java.lang.Class r2 = r12.getClass()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            throw r2
        L_0x013d:
            o.Address r12 = r2.f2963e
            o.HttpUrl r12 = r12.a
            java.lang.String r13 = r12.f2851e
            int r12 = r12.f2852f
        L_0x0145:
            r14 = 65535(0xffff, float:9.1834E-41)
            if (r7 > r12) goto L_0x01f8
            if (r14 < r12) goto L_0x01f8
            java.net.Proxy$Type r10 = r6.type()
            java.net.Proxy$Type r14 = java.net.Proxy.Type.SOCKS
            if (r10 != r14) goto L_0x015c
            java.net.InetSocketAddress r10 = java.net.InetSocketAddress.createUnresolved(r13, r12)
            r11.add(r10)
            goto L_0x0197
        L_0x015c:
            o.EventListener r10 = r2.h
            o.Call r14 = r2.g
            if (r10 == 0) goto L_0x01f7
            java.lang.String r10 = "call"
            if (r14 == 0) goto L_0x01f3
            if (r13 == 0) goto L_0x01ed
            o.Address r14 = r2.f2963e
            o.Dns r14 = r14.d
            java.util.List r14 = r14.a(r13)
            boolean r15 = r14.isEmpty()
            if (r15 != 0) goto L_0x01cf
            o.EventListener r13 = r2.h
            o.Call r15 = r2.g
            if (r13 == 0) goto L_0x01ce
            if (r15 == 0) goto L_0x01ca
            java.util.Iterator r10 = r14.iterator()
        L_0x0182:
            boolean r13 = r10.hasNext()
            if (r13 == 0) goto L_0x0197
            java.lang.Object r13 = r10.next()
            java.net.InetAddress r13 = (java.net.InetAddress) r13
            java.net.InetSocketAddress r14 = new java.net.InetSocketAddress
            r14.<init>(r13, r12)
            r11.add(r14)
            goto L_0x0182
        L_0x0197:
            java.util.List<? extends java.net.InetSocketAddress> r10 = r2.c
            java.util.Iterator r10 = r10.iterator()
        L_0x019d:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x01c2
            java.lang.Object r11 = r10.next()
            java.net.InetSocketAddress r11 = (java.net.InetSocketAddress) r11
            o.Route r12 = new o.Route
            o.Address r13 = r2.f2963e
            r12.<init>(r13, r6, r11)
            o.m0.d.RouteDatabase r11 = r2.f2964f
            boolean r11 = r11.c(r12)
            if (r11 == 0) goto L_0x01be
            java.util.List<o.k0> r11 = r2.d
            r11.add(r12)
            goto L_0x019d
        L_0x01be:
            r4.add(r12)
            goto L_0x019d
        L_0x01c2:
            boolean r6 = r4.isEmpty()
            r6 = r6 ^ r7
            if (r6 == 0) goto L_0x00be
            goto L_0x023b
        L_0x01ca:
            n.n.c.Intrinsics.a(r10)
            throw r5
        L_0x01ce:
            throw r5
        L_0x01cf:
            java.net.UnknownHostException r0 = new java.net.UnknownHostException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            o.Address r2 = r2.f2963e
            o.Dns r2 = r2.d
            r3.append(r2)
            java.lang.String r2 = " returned no addresses for "
            r3.append(r2)
            r3.append(r13)
            java.lang.String r2 = r3.toString()
            r0.<init>(r2)
            throw r0
        L_0x01ed:
            java.lang.String r0 = "domainName"
            n.n.c.Intrinsics.a(r0)
            throw r5
        L_0x01f3:
            n.n.c.Intrinsics.a(r10)
            throw r5
        L_0x01f7:
            throw r5
        L_0x01f8:
            java.net.SocketException r0 = new java.net.SocketException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r10)
            r2.append(r13)
            r3 = 58
            r2.append(r3)
            r2.append(r12)
            java.lang.String r3 = "; port is out of range"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            throw r0
        L_0x021a:
            java.net.SocketException r0 = new java.net.SocketException
            java.lang.StringBuilder r3 = j.a.a.a.outline.a(r10)
            o.Address r4 = r2.f2963e
            o.HttpUrl r4 = r4.a
            java.lang.String r4 = r4.f2851e
            r3.append(r4)
            java.lang.String r4 = "; exhausted proxy configurations: "
            r3.append(r4)
            java.util.List<? extends java.net.Proxy> r2 = r2.a
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r0.<init>(r2)
            throw r0
        L_0x023b:
            boolean r6 = r4.isEmpty()
            if (r6 == 0) goto L_0x024b
            java.util.List<o.k0> r6 = r2.d
            n.i.Collections.a(r4, r6)
            java.util.List<o.k0> r2 = r2.d
            r2.clear()
        L_0x024b:
            o.m0.d.RouteSelector$a r2 = new o.m0.d.RouteSelector$a
            r2.<init>(r4)
            r1.a = r2
            r2 = 1
            goto L_0x025b
        L_0x0254:
            java.util.NoSuchElementException r0 = new java.util.NoSuchElementException
            r0.<init>()
            throw r0
        L_0x025a:
            r2 = 0
        L_0x025b:
            o.m0.d.RealConnectionPool r4 = r1.g
            monitor-enter(r4)
            o.m0.d.Transmitter r6 = r1.f2946f     // Catch:{ all -> 0x033b }
            boolean r6 = r6.e()     // Catch:{ all -> 0x033b }
            if (r6 != 0) goto L_0x0333
            if (r2 == 0) goto L_0x0284
            o.m0.d.RouteSelector$a r2 = r1.a     // Catch:{ all -> 0x033b }
            if (r2 == 0) goto L_0x0280
            java.util.List<o.k0> r2 = r2.b     // Catch:{ all -> 0x033b }
            o.m0.d.RealConnectionPool r6 = r1.g     // Catch:{ all -> 0x033b }
            o.Address r10 = r1.h     // Catch:{ all -> 0x033b }
            o.m0.d.Transmitter r11 = r1.f2946f     // Catch:{ all -> 0x033b }
            boolean r0 = r6.a(r10, r11, r2, r0)     // Catch:{ all -> 0x033b }
            if (r0 == 0) goto L_0x0285
            o.m0.d.Transmitter r0 = r1.f2946f     // Catch:{ all -> 0x033b }
            o.m0.d.RealConnection r3 = r0.g     // Catch:{ all -> 0x033b }
            r9 = 1
            goto L_0x0285
        L_0x0280:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x033b }
            throw r5
        L_0x0284:
            r2 = r5
        L_0x0285:
            if (r9 != 0) goto L_0x02bd
            if (r8 != 0) goto L_0x02ad
            o.m0.d.RouteSelector$a r0 = r1.a     // Catch:{ all -> 0x033b }
            if (r0 == 0) goto L_0x02a9
            boolean r3 = r0.a()     // Catch:{ all -> 0x033b }
            if (r3 == 0) goto L_0x02a3
            java.util.List<o.k0> r3 = r0.b     // Catch:{ all -> 0x033b }
            int r6 = r0.a     // Catch:{ all -> 0x033b }
            int r8 = r6 + 1
            r0.a = r8     // Catch:{ all -> 0x033b }
            java.lang.Object r0 = r3.get(r6)     // Catch:{ all -> 0x033b }
            r8 = r0
            o.Route r8 = (o.Route) r8     // Catch:{ all -> 0x033b }
            goto L_0x02ad
        L_0x02a3:
            java.util.NoSuchElementException r0 = new java.util.NoSuchElementException     // Catch:{ all -> 0x033b }
            r0.<init>()     // Catch:{ all -> 0x033b }
            throw r0     // Catch:{ all -> 0x033b }
        L_0x02a9:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x033b }
            throw r5
        L_0x02ad:
            o.m0.d.RealConnection r3 = new o.m0.d.RealConnection     // Catch:{ all -> 0x033b }
            o.m0.d.RealConnectionPool r0 = r1.g     // Catch:{ all -> 0x033b }
            if (r8 == 0) goto L_0x02b9
            r3.<init>(r0, r8)     // Catch:{ all -> 0x033b }
            r1.c = r3     // Catch:{ all -> 0x033b }
            goto L_0x02bd
        L_0x02b9:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x033b }
            throw r5
        L_0x02bd:
            monitor-exit(r4)
            if (r9 == 0) goto L_0x02ce
            o.EventListener r0 = r1.f2948j
            o.Call r2 = r1.f2947i
            if (r3 == 0) goto L_0x02ca
            r0.a(r2, r3)
            return r3
        L_0x02ca:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x02ce:
            if (r3 == 0) goto L_0x032f
            o.Call r0 = r1.f2947i
            o.EventListener r4 = r1.f2948j
            r10 = r3
            r11 = r19
            r12 = r20
            r13 = r21
            r14 = r22
            r15 = r23
            r16 = r0
            r17 = r4
            r10.a(r11, r12, r13, r14, r15, r16, r17)
            o.m0.d.RealConnectionPool r0 = r1.g
            o.m0.d.RouteDatabase r0 = r0.d
            o.Route r4 = r3.f2959q
            r0.a(r4)
            o.m0.d.RealConnectionPool r4 = r1.g
            monitor-enter(r4)
            r1.c = r5     // Catch:{ all -> 0x032c }
            o.m0.d.RealConnectionPool r0 = r1.g     // Catch:{ all -> 0x032c }
            o.Address r6 = r1.h     // Catch:{ all -> 0x032c }
            o.m0.d.Transmitter r9 = r1.f2946f     // Catch:{ all -> 0x032c }
            boolean r0 = r0.a(r6, r9, r2, r7)     // Catch:{ all -> 0x032c }
            if (r0 == 0) goto L_0x030d
            r3.f2951i = r7     // Catch:{ all -> 0x032c }
            java.net.Socket r0 = r3.d()     // Catch:{ all -> 0x032c }
            o.m0.d.Transmitter r2 = r1.f2946f     // Catch:{ all -> 0x032c }
            o.m0.d.RealConnection r3 = r2.g     // Catch:{ all -> 0x032c }
            r1.f2945e = r8     // Catch:{ all -> 0x032c }
            goto L_0x0318
        L_0x030d:
            o.m0.d.RealConnectionPool r0 = r1.g     // Catch:{ all -> 0x032c }
            r0.a(r3)     // Catch:{ all -> 0x032c }
            o.m0.d.Transmitter r0 = r1.f2946f     // Catch:{ all -> 0x032c }
            r0.a(r3)     // Catch:{ all -> 0x032c }
            r0 = r5
        L_0x0318:
            monitor-exit(r4)
            if (r0 == 0) goto L_0x031e
            o.m0.Util.a(r0)
        L_0x031e:
            o.EventListener r0 = r1.f2948j
            o.Call r2 = r1.f2947i
            if (r3 == 0) goto L_0x0328
            r0.a(r2, r3)
            return r3
        L_0x0328:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x032c:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x032f:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x0333:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x033b }
            java.lang.String r2 = "Canceled"
            r0.<init>(r2)     // Catch:{ all -> 0x033b }
            throw r0     // Catch:{ all -> 0x033b }
        L_0x033b:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x033e:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0346 }
            java.lang.String r3 = "Canceled"
            r0.<init>(r3)     // Catch:{ all -> 0x0346 }
            throw r0     // Catch:{ all -> 0x0346 }
        L_0x0346:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.ExchangeFinder.a(int, int, int, int, boolean):o.m0.d.RealConnection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r4 = this;
            o.m0.d.RealConnectionPool r0 = r4.g
            monitor-enter(r0)
            o.Route r1 = r4.f2945e     // Catch:{ all -> 0x003a }
            r2 = 1
            if (r1 == 0) goto L_0x000a
            monitor-exit(r0)
            return r2
        L_0x000a:
            boolean r1 = r4.c()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0021
            o.m0.d.Transmitter r1 = r4.f2946f     // Catch:{ all -> 0x003a }
            o.m0.d.RealConnection r1 = r1.g     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x001c
            o.Route r1 = r1.f2959q     // Catch:{ all -> 0x003a }
            r4.f2945e = r1     // Catch:{ all -> 0x003a }
            monitor-exit(r0)
            return r2
        L_0x001c:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x003a }
            r0 = 0
            throw r0
        L_0x0021:
            o.m0.d.RouteSelector$a r1 = r4.a     // Catch:{ all -> 0x003a }
            r3 = 0
            if (r1 == 0) goto L_0x002b
            boolean r1 = r1.a()     // Catch:{ all -> 0x003a }
            goto L_0x002c
        L_0x002b:
            r1 = 0
        L_0x002c:
            if (r1 != 0) goto L_0x0038
            o.m0.d.RouteSelector r1 = r4.b     // Catch:{ all -> 0x003a }
            boolean r1 = r1.a()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0037
            goto L_0x0038
        L_0x0037:
            r2 = 0
        L_0x0038:
            monitor-exit(r0)
            return r2
        L_0x003a:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.ExchangeFinder.a():boolean");
    }
}
