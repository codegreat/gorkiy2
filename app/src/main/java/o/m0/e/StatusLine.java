package o.m0.e;

import j.a.a.a.outline;
import java.net.ProtocolException;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Protocol;

/* compiled from: StatusLine.kt */
public final class StatusLine {
    public final Protocol a;
    public final int b;
    public final String c;

    public StatusLine(Protocol protocol, int i2, String str) {
        if (protocol == null) {
            Intrinsics.a("protocol");
            throw null;
        } else if (str != null) {
            this.a = protocol;
            this.b = i2;
            this.c = str;
        } else {
            Intrinsics.a("message");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final StatusLine a(String str) {
        Protocol protocol;
        String str2;
        if (str != null) {
            int i2 = 9;
            if (Indent.b(str, "HTTP/1.", false, 2)) {
                if (str.length() < 9 || str.charAt(8) != ' ') {
                    throw new ProtocolException(outline.a("Unexpected status line: ", str));
                }
                int charAt = str.charAt(7) - '0';
                if (charAt == 0) {
                    protocol = Protocol.HTTP_1_0;
                } else if (charAt == 1) {
                    protocol = Protocol.HTTP_1_1;
                } else {
                    throw new ProtocolException(outline.a("Unexpected status line: ", str));
                }
            } else if (Indent.b(str, "ICY ", false, 2)) {
                protocol = Protocol.HTTP_1_0;
                i2 = 4;
            } else {
                throw new ProtocolException(outline.a("Unexpected status line: ", str));
            }
            int i3 = i2 + 3;
            if (str.length() >= i3) {
                try {
                    String substring = str.substring(i2, i3);
                    Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    int parseInt = Integer.parseInt(substring);
                    if (str.length() <= i3) {
                        str2 = "";
                    } else if (str.charAt(i3) == ' ') {
                        str2 = str.substring(i2 + 4);
                        Intrinsics.a((Object) str2, "(this as java.lang.String).substring(startIndex)");
                    } else {
                        throw new ProtocolException(outline.a("Unexpected status line: ", str));
                    }
                    return new StatusLine(protocol, parseInt, str2);
                } catch (NumberFormatException unused) {
                    throw new ProtocolException(outline.a("Unexpected status line: ", str));
                }
            } else {
                throw new ProtocolException(outline.a("Unexpected status line: ", str));
            }
        } else {
            Intrinsics.a("statusLine");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.a == Protocol.HTTP_1_0) {
            sb.append("HTTP/1.0");
        } else {
            sb.append("HTTP/1.1");
        }
        sb.append(' ');
        sb.append(this.b);
        sb.append(' ');
        sb.append(this.c);
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
