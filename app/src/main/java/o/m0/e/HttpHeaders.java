package o.m0.e;

import n.n.c.Intrinsics;
import n.r.Indent;
import o.Response;
import o.m0.Util;
import p.ByteString;

/* compiled from: HttpHeaders.kt */
public final class HttpHeaders {
    static {
        ByteString.f3063f.b("\"\\");
        ByteString.f3063f.b("\t ,=");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public static final boolean a(Response response) {
        if (response == null) {
            Intrinsics.a("$this$promisesBody");
            throw null;
        } else if (Intrinsics.a((Object) response.c.c, (Object) "HEAD")) {
            return false;
        } else {
            int i2 = response.f2901f;
            if (((i2 >= 100 && i2 < 200) || i2 == 204 || i2 == 304) && Util.a(response) == -1 && !Indent.a("chunked", Response.a(response, "Transfer-Encoding", null, 2), true)) {
                return false;
            }
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(java.lang.String, char, int, int):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      o.m0.Util.a(java.lang.String, int, int, int):int
      o.m0.Util.a(java.lang.String, java.lang.String, int, int):int
      o.m0.Util.a(java.lang.String, char, int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
     arg types: [java.lang.String, char[], int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0246, code lost:
        if (r0 == null) goto L_0x029d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01a0, code lost:
        if ((n.n.c.Intrinsics.a(r0, r11) || (n.r.Indent.a(r0, r11, false, 2) && r0.charAt((r0.length() - r11.length()) + -1) == '.' && !o.m0.Util.a(r0))) == false) goto L_0x029d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x02aa A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(o.CookieJar r40, o.HttpUrl r41, o.Headers r42) {
        /*
            r1 = r40
            r2 = r41
            r0 = r42
            if (r1 == 0) goto L_0x02e1
            if (r2 == 0) goto L_0x02da
            if (r0 == 0) goto L_0x02d3
            o.CookieJar r3 = o.CookieJar.a
            if (r1 != r3) goto L_0x0011
            return
        L_0x0011:
            o.Cookie r3 = o.Cookie.f2841n
            java.lang.String r3 = "Set-Cookie"
            java.util.List r3 = r0.b(r3)
            int r4 = r3.size()
            r0 = 0
            r5 = 0
            r6 = 0
        L_0x0020:
            if (r5 >= r4) goto L_0x02ba
            java.lang.Object r7 = r3.get(r5)
            java.lang.String r7 = (java.lang.String) r7
            if (r7 == 0) goto L_0x02b3
            long r8 = java.lang.System.currentTimeMillis()
            r10 = 59
            r11 = 6
            int r11 = o.m0.Util.a(r7, r10, r0, r0, r11)
            r12 = 61
            int r13 = o.m0.Util.a(r7, r12, r0, r11)
            if (r13 != r11) goto L_0x0043
        L_0x003d:
            r42 = r3
            r31 = r4
            goto L_0x029d
        L_0x0043:
            java.lang.String r15 = o.m0.Util.c(r7, r0, r13)
            int r0 = r15.length()
            r14 = 1
            if (r0 != 0) goto L_0x0050
            r0 = 1
            goto L_0x0051
        L_0x0050:
            r0 = 0
        L_0x0051:
            if (r0 != 0) goto L_0x003d
            int r0 = o.m0.Util.b(r15)
            r10 = -1
            if (r0 == r10) goto L_0x005b
            goto L_0x003d
        L_0x005b:
            int r13 = r13 + 1
            java.lang.String r16 = o.m0.Util.c(r7, r13, r11)
            int r0 = o.m0.Util.b(r16)
            if (r0 == r10) goto L_0x0068
            goto L_0x003d
        L_0x0068:
            int r11 = r11 + 1
            int r10 = r7.length()
            r17 = -1
            r0 = 0
            r13 = 0
            r19 = 0
            r20 = 0
            r21 = 1
            r22 = 0
            r23 = 253402300799999(0xe677d21fdbff, double:1.251973714024093E-309)
            r42 = r3
            r12 = r11
            r19 = r17
            r3 = r22
            r13 = 61
            r21 = 0
            r22 = 0
            r25 = 0
            r26 = 1
            r11 = r0
            r0 = 59
        L_0x0093:
            r27 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r29 = -9223372036854775808
            if (r12 >= r10) goto L_0x013c
            r31 = r4
            int r4 = o.m0.Util.a(r7, r0, r12, r10)
            int r0 = o.m0.Util.a(r7, r13, r12, r4)
            java.lang.String r12 = o.m0.Util.c(r7, r12, r0)
            if (r0 >= r4) goto L_0x00b3
            int r0 = r0 + 1
            java.lang.String r0 = o.m0.Util.c(r7, r0, r4)
            goto L_0x00b5
        L_0x00b3:
            java.lang.String r0 = ""
        L_0x00b5:
            r13 = r0
            java.lang.String r0 = "expires"
            boolean r0 = n.r.Indent.a(r12, r0, r14)
            if (r0 == 0) goto L_0x00c8
            int r0 = r13.length()     // Catch:{ IllegalArgumentException -> 0x0131 }
            r12 = 0
            long r23 = o.Cookie.a(r13, r12, r0)     // Catch:{ IllegalArgumentException -> 0x0131 }
            goto L_0x00fd
        L_0x00c8:
            java.lang.String r0 = "max-age"
            boolean r0 = n.r.Indent.a(r12, r0, r14)
            if (r0 == 0) goto L_0x0101
            long r12 = java.lang.Long.parseLong(r13)     // Catch:{ NumberFormatException -> 0x00e0 }
            r19 = 0
            int r0 = (r12 > r19 ? 1 : (r12 == r19 ? 0 : -1))
            if (r0 > 0) goto L_0x00db
            goto L_0x00dd
        L_0x00db:
            r29 = r12
        L_0x00dd:
            r19 = r29
            goto L_0x00fd
        L_0x00e0:
            r0 = move-exception
            r12 = r0
            n.r.Regex r0 = new n.r.Regex     // Catch:{  }
            java.lang.String r14 = "-?\\d+"
            r0.<init>(r14)     // Catch:{  }
            boolean r0 = r0.a(r13)     // Catch:{  }
            if (r0 == 0) goto L_0x0100
            java.lang.String r0 = "-"
            r12 = 2
            r14 = 0
            boolean r0 = n.r.Indent.b(r13, r0, r14, r12)     // Catch:{  }
            if (r0 == 0) goto L_0x00fb
            r27 = r29
        L_0x00fb:
            r19 = r27
        L_0x00fd:
            r25 = 1
            goto L_0x0131
        L_0x0100:
            throw r12     // Catch:{  }
        L_0x0101:
            java.lang.String r0 = "domain"
            r14 = 1
            boolean r0 = n.r.Indent.a(r12, r0, r14)
            if (r0 == 0) goto L_0x0112
            java.lang.String r0 = o.Cookie.a(r13)     // Catch:{ IllegalArgumentException -> 0x0131 }
            r26 = 0
            r11 = r0
            goto L_0x0131
        L_0x0112:
            java.lang.String r0 = "path"
            boolean r0 = n.r.Indent.a(r12, r0, r14)
            if (r0 == 0) goto L_0x011c
            r3 = r13
            goto L_0x0131
        L_0x011c:
            java.lang.String r0 = "secure"
            boolean r0 = n.r.Indent.a(r12, r0, r14)
            if (r0 == 0) goto L_0x0127
            r21 = 1
            goto L_0x0131
        L_0x0127:
            java.lang.String r0 = "httponly"
            boolean r0 = n.r.Indent.a(r12, r0, r14)
            if (r0 == 0) goto L_0x0131
            r22 = 1
        L_0x0131:
            int r12 = r4 + 1
            r14 = 1
            r0 = 59
            r13 = 61
            r4 = r31
            goto L_0x0093
        L_0x013c:
            r31 = r4
            int r0 = (r19 > r29 ? 1 : (r19 == r29 ? 0 : -1))
            if (r0 != 0) goto L_0x0145
            r17 = r29
            goto L_0x016f
        L_0x0145:
            int r0 = (r19 > r17 ? 1 : (r19 == r17 ? 0 : -1))
            if (r0 == 0) goto L_0x016d
            r12 = 9223372036854775(0x20c49ba5e353f7, double:4.663754807431093E-308)
            int r0 = (r19 > r12 ? 1 : (r19 == r12 ? 0 : -1))
            if (r0 > 0) goto L_0x0157
            r0 = 1000(0x3e8, float:1.401E-42)
            long r12 = (long) r0
            long r27 = r19 * r12
        L_0x0157:
            long r27 = r8 + r27
            int r0 = (r27 > r8 ? 1 : (r27 == r8 ? 0 : -1))
            r7 = 253402300799999(0xe677d21fdbff, double:1.251973714024093E-309)
            if (r0 < 0) goto L_0x016a
            int r0 = (r27 > r7 ? 1 : (r27 == r7 ? 0 : -1))
            if (r0 <= 0) goto L_0x0167
            goto L_0x016a
        L_0x0167:
            r17 = r27
            goto L_0x016f
        L_0x016a:
            r17 = r7
            goto L_0x016f
        L_0x016d:
            r17 = r23
        L_0x016f:
            java.lang.String r0 = r2.f2851e
            r4 = 46
            if (r11 != 0) goto L_0x0177
            r11 = r0
            goto L_0x01a4
        L_0x0177:
            boolean r7 = n.n.c.Intrinsics.a(r0, r11)
            if (r7 == 0) goto L_0x017e
            goto L_0x019d
        L_0x017e:
            r7 = 2
            r8 = 0
            boolean r7 = n.r.Indent.a(r0, r11, r8, r7)
            if (r7 == 0) goto L_0x019f
            int r7 = r0.length()
            int r8 = r11.length()
            int r7 = r7 - r8
            int r7 = r7 + -1
            char r7 = r0.charAt(r7)
            if (r7 != r4) goto L_0x019f
            boolean r7 = o.m0.Util.a(r0)
            if (r7 != 0) goto L_0x019f
        L_0x019d:
            r7 = 1
            goto L_0x01a0
        L_0x019f:
            r7 = 0
        L_0x01a0:
            if (r7 != 0) goto L_0x01a4
            goto L_0x029d
        L_0x01a4:
            int r0 = r0.length()
            int r7 = r11.length()
            if (r0 == r7) goto L_0x0264
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$a r0 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.h
            okhttp3.internal.publicsuffix.PublicSuffixDatabase r0 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.g
            if (r0 == 0) goto L_0x0262
            java.lang.String r7 = java.net.IDN.toUnicode(r11)
            java.lang.String r8 = "unicodeDomain"
            n.n.c.Intrinsics.a(r7, r8)
            r8 = 1
            char[] r8 = new char[r8]
            r9 = 0
            r8[r9] = r4
            r10 = 6
            java.util.List r7 = n.r.Indent.a(r7, r8, r9, r9, r10)
            java.util.List r0 = r0.a(r7)
            int r8 = r7.size()
            int r10 = r0.size()
            r12 = 33
            if (r8 != r10) goto L_0x01e6
            java.lang.Object r8 = r0.get(r9)
            java.lang.String r8 = (java.lang.String) r8
            char r8 = r8.charAt(r9)
            if (r8 == r12) goto L_0x01e6
            r0 = 0
            goto L_0x0246
        L_0x01e6:
            java.lang.Object r8 = r0.get(r9)
            java.lang.String r8 = (java.lang.String) r8
            char r8 = r8.charAt(r9)
            if (r8 != r12) goto L_0x01fc
            int r7 = r7.size()
            int r0 = r0.size()
            r8 = 1
            goto L_0x0206
        L_0x01fc:
            int r7 = r7.size()
            int r0 = r0.size()
            r8 = 1
            int r0 = r0 + r8
        L_0x0206:
            int r7 = r7 - r0
            char[] r0 = new char[r8]
            r0[r9] = r4
            r4 = 6
            java.util.List r0 = n.r.Indent.a(r11, r0, r9, r9, r4)
            if (r0 == 0) goto L_0x025b
            n.i.Sequences r4 = new n.i.Sequences
            r4.<init>(r0)
            if (r7 < 0) goto L_0x021a
            goto L_0x021b
        L_0x021a:
            r8 = 0
        L_0x021b:
            if (r8 == 0) goto L_0x0249
            if (r7 != 0) goto L_0x0220
            goto L_0x022a
        L_0x0220:
            boolean r0 = r4 instanceof n.q.Sequences0
            if (r0 == 0) goto L_0x022d
            n.q.Sequences0 r4 = (n.q.Sequences0) r4
            n.q.Sequence r4 = r4.a(r7)
        L_0x022a:
            r32 = r4
            goto L_0x0234
        L_0x022d:
            n.q.Sequences r0 = new n.q.Sequences
            r0.<init>(r4, r7)
            r32 = r0
        L_0x0234:
            r34 = 0
            r35 = 0
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = 62
            java.lang.String r33 = "."
            java.lang.String r0 = n.i.Collections.a(r32, r33, r34, r35, r36, r37, r38, r39)
        L_0x0246:
            if (r0 != 0) goto L_0x0264
            goto L_0x029d
        L_0x0249:
            java.lang.String r0 = "Requested element count "
            java.lang.String r1 = " is less than zero."
            java.lang.String r0 = j.a.a.a.outline.b(r0, r7, r1)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x025b:
            java.lang.String r0 = "$this$asSequence"
            n.n.c.Intrinsics.a(r0)
            r0 = 0
            throw r0
        L_0x0262:
            r0 = 0
            throw r0
        L_0x0264:
            java.lang.String r0 = "/"
            if (r3 == 0) goto L_0x0274
            r4 = 2
            r7 = 0
            boolean r4 = n.r.Indent.b(r3, r0, r7, r4)
            if (r4 != 0) goto L_0x0271
            goto L_0x0275
        L_0x0271:
            r20 = r3
            goto L_0x028d
        L_0x0274:
            r7 = 0
        L_0x0275:
            java.lang.String r3 = r41.b()
            r4 = 47
            r8 = 6
            int r4 = n.r.Indent.b(r3, r4, r7, r7, r8)
            if (r4 == 0) goto L_0x028b
            java.lang.String r0 = r3.substring(r7, r4)
            java.lang.String r3 = "(this as java.lang.Strin…ing(startIndex, endIndex)"
            n.n.c.Intrinsics.a(r0, r3)
        L_0x028b:
            r20 = r0
        L_0x028d:
            o.Cookie r0 = new o.Cookie
            r3 = 0
            r14 = r0
            r19 = r11
            r23 = r25
            r24 = r26
            r25 = r3
            r14.<init>(r15, r16, r17, r19, r20, r21, r22, r23, r24, r25)
            goto L_0x029e
        L_0x029d:
            r0 = 0
        L_0x029e:
            if (r0 == 0) goto L_0x02aa
            if (r6 != 0) goto L_0x02a7
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
        L_0x02a7:
            r6.add(r0)
        L_0x02aa:
            int r5 = r5 + 1
            r0 = 0
            r3 = r42
            r4 = r31
            goto L_0x0020
        L_0x02b3:
            java.lang.String r0 = "setCookie"
            n.n.c.Intrinsics.a(r0)
            r0 = 0
            throw r0
        L_0x02ba:
            if (r6 == 0) goto L_0x02c6
            java.util.List r0 = java.util.Collections.unmodifiableList(r6)
            java.lang.String r3 = "Collections.unmodifiableList(cookies)"
            n.n.c.Intrinsics.a(r0, r3)
            goto L_0x02c8
        L_0x02c6:
            n.i.Collections2 r0 = n.i.Collections2.b
        L_0x02c8:
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L_0x02cf
            return
        L_0x02cf:
            r1.a(r2, r0)
            return
        L_0x02d3:
            java.lang.String r0 = "headers"
            n.n.c.Intrinsics.a(r0)
            r0 = 0
            throw r0
        L_0x02da:
            r0 = 0
            java.lang.String r1 = "url"
            n.n.c.Intrinsics.a(r1)
            throw r0
        L_0x02e1:
            r0 = 0
            java.lang.String r1 = "$this$receiveHeaders"
            n.n.c.Intrinsics.a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.e.HttpHeaders.a(o.CookieJar, o.HttpUrl, o.Headers):void");
    }
}
