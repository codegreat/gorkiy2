package o.m0.e;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import n.n.c.Intrinsics;
import o.m0.Util;

/* compiled from: dates.kt */
public final class dates {
    public static final a a = new a();
    public static final String[] b;
    public static final DateFormat[] c;

    /* compiled from: dates.kt */
    public static final class a extends ThreadLocal<DateFormat> {
        public Object initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setLenient(false);
            simpleDateFormat.setTimeZone(Util.f2919e);
            return simpleDateFormat;
        }
    }

    static {
        String[] strArr = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z", "EEE MMM d yyyy HH:mm:ss z"};
        b = strArr;
        c = new DateFormat[strArr.length];
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static final Date a(String str) {
        if (str != null) {
            if (str.length() == 0) {
                return null;
            }
            ParsePosition parsePosition = new ParsePosition(0);
            Date parse = ((DateFormat) a.get()).parse(str, parsePosition);
            if (parsePosition.getIndex() == str.length()) {
                return parse;
            }
            synchronized (b) {
                int length = b.length;
                for (int i2 = 0; i2 < length; i2++) {
                    DateFormat dateFormat = c[i2];
                    SimpleDateFormat simpleDateFormat = dateFormat;
                    if (dateFormat == null) {
                        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(b[i2], Locale.US);
                        simpleDateFormat2.setTimeZone(Util.f2919e);
                        c[i2] = simpleDateFormat2;
                        simpleDateFormat = simpleDateFormat2;
                    }
                    parsePosition.setIndex(0);
                    Date parse2 = simpleDateFormat.parse(str, parsePosition);
                    if (parsePosition.getIndex() != 0) {
                        return parse2;
                    }
                }
                return null;
            }
        }
        Intrinsics.a("$this$toHttpDateOrNull");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String a(Date date) {
        if (date != null) {
            String format = ((DateFormat) a.get()).format(date);
            Intrinsics.a((Object) format, "STANDARD_DATE_FORMAT.get().format(this)");
            return format;
        }
        Intrinsics.a("$this$toHttpDateString");
        throw null;
    }
}
