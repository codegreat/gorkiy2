package o.m0.c;

import n.n.c.Intrinsics;
import o.Request;
import o.Response;

/* compiled from: CacheStrategy.kt */
public final class CacheStrategy {
    public final Request a;
    public final Response b;

    public CacheStrategy(Request request, Response response) {
        this.a = request;
        this.b = response;
    }

    public static final boolean a(Response response, Request request) {
        if (response == null) {
            Intrinsics.a("response");
            throw null;
        } else if (request != null) {
            int i2 = response.f2901f;
            if (!(i2 == 200 || i2 == 410 || i2 == 414 || i2 == 501 || i2 == 203 || i2 == 204)) {
                if (i2 != 307) {
                    if (!(i2 == 308 || i2 == 404 || i2 == 405)) {
                        switch (i2) {
                            case 300:
                            case 301:
                                break;
                            case 302:
                                break;
                            default:
                                return false;
                        }
                    }
                }
                if (Response.a(response, "Expires", null, 2) == null && response.a().c == -1 && !response.a().f2814f && !response.a().f2813e) {
                    return false;
                }
            }
            if (response.a().b || request.a().b) {
                return false;
            }
            return true;
        } else {
            Intrinsics.a("request");
            throw null;
        }
    }
}
