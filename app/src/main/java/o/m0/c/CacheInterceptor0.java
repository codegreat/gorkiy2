package o.m0.c;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import n.n.c.Intrinsics;
import o.m0.Util;
import p.Buffer;
import p.BufferedSink;
import p.BufferedSource;
import p.Source;
import p.Timeout;

/* compiled from: CacheInterceptor.kt */
public final class CacheInterceptor0 implements Source {
    public boolean b;
    public final /* synthetic */ BufferedSource c;
    public final /* synthetic */ CacheRequest d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ BufferedSink f2921e;

    public CacheInterceptor0(BufferedSource bufferedSource, CacheRequest cacheRequest, BufferedSink bufferedSink) {
        this.c = bufferedSource;
        this.d = cacheRequest;
        this.f2921e = bufferedSink;
    }

    public long b(Buffer buffer, long j2) {
        if (buffer != null) {
            try {
                long b2 = this.c.b(buffer, j2);
                if (b2 == -1) {
                    if (!this.b) {
                        this.b = true;
                        this.f2921e.close();
                    }
                    return -1;
                }
                buffer.a(this.f2921e.getBuffer(), buffer.c - b2, b2);
                this.f2921e.c();
                return b2;
            } catch (IOException e2) {
                if (!this.b) {
                    this.b = true;
                    this.d.a();
                }
                throw e2;
            }
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public void close() {
        if (!this.b && !Util.a(this, 100, TimeUnit.MILLISECONDS)) {
            this.b = true;
            this.d.a();
        }
        this.c.close();
    }

    public Timeout b() {
        return this.c.b();
    }
}
