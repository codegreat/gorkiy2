package o;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import n.i.Collections;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.m0.Util;
import o.m0.e.dates;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

/* compiled from: Cookie.kt */
public final class Cookie {

    /* renamed from: j  reason: collision with root package name */
    public static final Pattern f2837j = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: k  reason: collision with root package name */
    public static final Pattern f2838k = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");

    /* renamed from: l  reason: collision with root package name */
    public static final Pattern f2839l = Pattern.compile("(\\d{1,2})[^\\d]*");

    /* renamed from: m  reason: collision with root package name */
    public static final Pattern f2840m = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");

    /* renamed from: n  reason: collision with root package name */
    public static final Cookie f2841n = null;
    public final String a;
    public final String b;
    public final long c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2842e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f2843f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f2844i;

    public /* synthetic */ Cookie(String str, String str2, long j2, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = str;
        this.b = str2;
        this.c = j2;
        this.d = str3;
        this.f2842e = str4;
        this.f2843f = z;
        this.g = z2;
        this.h = z3;
        this.f2844i = z4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int */
    public static final long a(String str, int i2, int i3) {
        int a2 = a(str, i2, i3, false);
        Matcher matcher = f2840m.matcher(str);
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        while (a2 < i3) {
            int a3 = a(str, a2 + 1, i3, true);
            matcher.region(a2, a3);
            if (i5 == -1 && matcher.usePattern(f2840m).matches()) {
                i5 = Integer.parseInt(matcher.group(1));
                i8 = Integer.parseInt(matcher.group(2));
                i9 = Integer.parseInt(matcher.group(3));
            } else if (i6 == -1 && matcher.usePattern(f2839l).matches()) {
                i6 = Integer.parseInt(matcher.group(1));
            } else if (i7 == -1 && matcher.usePattern(f2838k).matches()) {
                String group = matcher.group(1);
                Intrinsics.a((Object) group, "matcher.group(1)");
                Locale locale = Locale.US;
                Intrinsics.a((Object) locale, "Locale.US");
                String lowerCase = group.toLowerCase(locale);
                Intrinsics.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                String pattern = f2838k.pattern();
                Intrinsics.a((Object) pattern, "MONTH_PATTERN.pattern()");
                i7 = Indent.a((CharSequence) pattern, lowerCase, 0, false, 6) / 4;
            } else if (i4 == -1 && matcher.usePattern(f2837j).matches()) {
                i4 = Integer.parseInt(matcher.group(1));
            }
            a2 = a(str, a3 + 1, i3, false);
        }
        if (70 <= i4 && 99 >= i4) {
            i4 += 1900;
        }
        if (i4 >= 0 && 69 >= i4) {
            i4 += RecyclerView.MAX_SCROLL_DURATION;
        }
        if (i4 >= 1601) {
            if (i7 != -1) {
                if (1 <= i6 && 31 >= i6) {
                    if (i5 >= 0 && 23 >= i5) {
                        if (i8 >= 0 && 59 >= i8) {
                            if (i9 >= 0 && 59 >= i9) {
                                GregorianCalendar gregorianCalendar = new GregorianCalendar(Util.f2919e);
                                gregorianCalendar.setLenient(false);
                                gregorianCalendar.set(1, i4);
                                gregorianCalendar.set(2, i7 - 1);
                                gregorianCalendar.set(5, i6);
                                gregorianCalendar.set(11, i5);
                                gregorianCalendar.set(12, i8);
                                gregorianCalendar.set(13, i9);
                                gregorianCalendar.set(14, 0);
                                return gregorianCalendar.getTimeInMillis();
                            }
                            throw new IllegalArgumentException("Failed requirement.".toString());
                        }
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    public boolean equals(Object obj) {
        if (obj instanceof Cookie) {
            Cookie cookie = (Cookie) obj;
            return Intrinsics.a(cookie.a, this.a) && Intrinsics.a(cookie.b, this.b) && cookie.c == this.c && Intrinsics.a(cookie.d, this.d) && Intrinsics.a(cookie.f2842e, this.f2842e) && cookie.f2843f == this.f2843f && cookie.g == this.g && cookie.h == this.h && cookie.f2844i == this.f2844i;
        }
    }

    @IgnoreJRERequirement
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.d.hashCode();
        return ((((((((this.f2842e.hashCode() + ((hashCode2 + ((((hashCode + ((this.a.hashCode() + 527) * 31)) * 31) + b.a(this.c)) * 31)) * 31)) * 31) + a.a(this.f2843f)) * 31) + a.a(this.g)) * 31) + a.a(this.h)) * 31) + a.a(this.f2844i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append('=');
        sb.append(this.b);
        if (this.h) {
            if (this.c == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=");
                sb.append(dates.a(new Date(this.c)));
            }
        }
        if (!this.f2844i) {
            sb.append("; domain=");
            sb.append(this.d);
        }
        sb.append("; path=");
        sb.append(this.f2842e);
        if (this.f2843f) {
            sb.append("; secure");
        }
        if (this.g) {
            sb.append("; httponly");
        }
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "toString()");
        return sb2;
    }

    public static final int a(String str, int i2, int i3, boolean z) {
        while (i2 < i3) {
            char charAt = str.charAt(i2);
            if (((charAt < ' ' && charAt != 9) || charAt >= 127 || ('0' <= charAt && '9' >= charAt) || (('a' <= charAt && 'z' >= charAt) || (('A' <= charAt && 'Z' >= charAt) || charAt == ':'))) == (!z)) {
                return i2;
            }
            i2++;
        }
        return i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String a(String str) {
        if (!Indent.a(str, ".", false, 2)) {
            if (Indent.b(str, ".", false, 2)) {
                str = str.substring(".".length());
                Intrinsics.a((Object) str, "(this as java.lang.String).substring(startIndex)");
            }
            String a2 = Collections.a(str);
            if (a2 != null) {
                return a2;
            }
            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
