package o;

import java.util.concurrent.TimeUnit;
import n.n.c.Intrinsics;
import o.m0.d.RealConnectionPool;

/* compiled from: ConnectionPool.kt */
public final class ConnectionPool {
    public final RealConnectionPool a;

    public ConnectionPool() {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        if (timeUnit != null) {
            this.a = new RealConnectionPool(5, 5, timeUnit);
        } else {
            Intrinsics.a("timeUnit");
            throw null;
        }
    }
}
