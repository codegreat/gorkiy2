package androidx.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import i.q.a;

public class DropDownPreference extends ListPreference {
    public final Context u;
    public final ArrayAdapter v;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DropDownPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, a.dropdownPreferenceStyle, 0);
        this.u = context;
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.u, 17367049);
        this.v = arrayAdapter;
        arrayAdapter.clear();
        CharSequence[] charSequenceArr = super.f253q;
        if (charSequenceArr != null) {
            for (CharSequence charSequence : charSequenceArr) {
                this.v.add(charSequence.toString());
            }
        }
    }

    public void h() {
        ArrayAdapter arrayAdapter = this.v;
        if (arrayAdapter != null) {
            arrayAdapter.notifyDataSetChanged();
        }
    }
}
