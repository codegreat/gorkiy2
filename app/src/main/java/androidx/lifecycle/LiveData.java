package androidx.lifecycle;

import i.c.a.a.ArchTaskExecutor;
import i.c.a.b.SafeIterableMap;
import i.o.Lifecycle;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;
import i.o.Observer;
import i.o.i;
import i.o.k;
import j.a.a.a.outline;
import java.util.Map;

public abstract class LiveData<T> {

    /* renamed from: i  reason: collision with root package name */
    public static final Object f247i = new Object();
    public final Object a = new Object();
    public SafeIterableMap<Observer<? super T>, LiveData<T>.defpackage.a> b = new SafeIterableMap<>();
    public int c = 0;
    public volatile Object d = f247i;

    /* renamed from: e  reason: collision with root package name */
    public volatile Object f248e = f247i;

    /* renamed from: f  reason: collision with root package name */
    public int f249f = -1;
    public boolean g;
    public boolean h;

    public class LifecycleBoundObserver extends LiveData<T>.defpackage.a implements i {

        /* renamed from: e  reason: collision with root package name */
        public final LifecycleOwner f250e;

        /* renamed from: f  reason: collision with root package name */
        public final /* synthetic */ LiveData f251f;

        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
            if (((LifecycleRegistry) this.f250e.a()).b == Lifecycle.b.DESTROYED) {
                this.f251f.a(super.a);
            } else {
                a(((LifecycleRegistry) this.f250e.a()).b.a(Lifecycle.b.STARTED));
            }
        }
    }

    public abstract class a {
        public final Observer<? super T> a;
        public boolean b;
        public int c;
        public final /* synthetic */ LiveData d;

        public void a(boolean z) {
            if (z != this.b) {
                this.b = z;
                int i2 = 1;
                boolean z2 = this.d.c == 0;
                LiveData liveData = this.d;
                int i3 = liveData.c;
                if (!this.b) {
                    i2 = -1;
                }
                liveData.c = i3 + i2;
                if (z2 && this.b) {
                    this.d.a();
                }
                LiveData liveData2 = this.d;
                if (liveData2.c == 0 && !this.b) {
                    liveData2.b();
                }
                if (this.b) {
                    this.d.b(this);
                }
            }
        }
    }

    public void a() {
    }

    public final void a(LiveData<T>.defpackage.a aVar) {
        if (aVar.b) {
            if (!((LifecycleRegistry) ((LifecycleBoundObserver) aVar).f250e.a()).b.a(Lifecycle.b.STARTED)) {
                aVar.a(false);
                return;
            }
            int i2 = aVar.c;
            int i3 = this.f249f;
            if (i2 < i3) {
                aVar.c = i3;
                aVar.a.a(this.d);
            }
        }
    }

    public void b() {
    }

    public void b(LiveData<T>.defpackage.a aVar) {
        if (this.g) {
            this.h = true;
            return;
        }
        this.g = true;
        do {
            this.h = false;
            if (aVar == null) {
                SafeIterableMap<K, V>.d c2 = this.b.c();
                while (c2.hasNext()) {
                    a((a) ((Map.Entry) c2.next()).getValue());
                    if (this.h) {
                        break;
                    }
                }
            } else {
                a(aVar);
                aVar = null;
            }
        } while (this.h);
        this.g = false;
    }

    public void a(Observer<? super k> observer) {
        a("removeObserver");
        a remove = this.b.remove(observer);
        if (remove != null) {
            LifecycleBoundObserver lifecycleBoundObserver = (LifecycleBoundObserver) remove;
            ((LifecycleRegistry) lifecycleBoundObserver.f250e.a()).a.remove(lifecycleBoundObserver);
            remove.a(false);
        }
    }

    public static void a(String str) {
        if (!ArchTaskExecutor.b().a.a()) {
            throw new IllegalStateException(outline.a("Cannot invoke ", str, " on a background thread"));
        }
    }
}
