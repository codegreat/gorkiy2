package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.recyclerview.widget.RecyclerView;
import i.b.j;
import i.b.k.ActionBar;
import i.b.l.a.AppCompatResources;
import i.b.p.CollapsibleActionView;
import i.b.p.SupportMenuInflater;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuPresenter;
import i.b.p.i.SubMenuBuilder;
import i.b.q.ActionMenuPresenter;
import i.b.q.AppCompatImageButton;
import i.b.q.AppCompatImageView;
import i.b.q.AppCompatTextView;
import i.b.q.DecorToolbar;
import i.b.q.RtlSpacingHelper;
import i.b.q.TintTypedArray;
import i.b.q.ToolbarWidgetWrapper0;
import i.b.q.ViewUtils;
import i.h.l.ViewCompat;
import i.j.a.AbsSavedState;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    public ColorStateList A;
    public ColorStateList B;
    public boolean C;
    public boolean D;
    public final ArrayList<View> E;
    public final ArrayList<View> F;
    public final int[] G;
    public f H;
    public final ActionMenuView.e I;
    public ToolbarWidgetWrapper0 J;
    public ActionMenuPresenter K;
    public d L;
    public MenuPresenter.a M;
    public MenuBuilder.a N;
    public boolean O;
    public final Runnable P;
    public ActionMenuView b;
    public TextView c;
    public TextView d;

    /* renamed from: e  reason: collision with root package name */
    public ImageButton f127e;

    /* renamed from: f  reason: collision with root package name */
    public ImageView f128f;
    public Drawable g;
    public CharSequence h;

    /* renamed from: i  reason: collision with root package name */
    public ImageButton f129i;

    /* renamed from: j  reason: collision with root package name */
    public View f130j;

    /* renamed from: k  reason: collision with root package name */
    public Context f131k;

    /* renamed from: l  reason: collision with root package name */
    public int f132l;

    /* renamed from: m  reason: collision with root package name */
    public int f133m;

    /* renamed from: n  reason: collision with root package name */
    public int f134n;

    /* renamed from: o  reason: collision with root package name */
    public int f135o;

    /* renamed from: p  reason: collision with root package name */
    public int f136p;

    /* renamed from: q  reason: collision with root package name */
    public int f137q;

    /* renamed from: r  reason: collision with root package name */
    public int f138r;

    /* renamed from: s  reason: collision with root package name */
    public int f139s;

    /* renamed from: t  reason: collision with root package name */
    public int f140t;
    public RtlSpacingHelper u;
    public int v;
    public int w;
    public int x;
    public CharSequence y;
    public CharSequence z;

    public class a implements ActionMenuView.e {
        public a() {
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            Toolbar.this.g();
        }
    }

    public class c implements View.OnClickListener {
        public c() {
        }

        public void onClick(View view) {
            d dVar = Toolbar.this.L;
            MenuItemImpl menuItemImpl = dVar == null ? null : dVar.c;
            if (menuItemImpl != null) {
                menuItemImpl.collapseActionView();
            }
        }
    }

    public interface f {
        boolean onMenuItemClick(MenuItem menuItem);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    private MenuInflater getMenuInflater() {
        return new SupportMenuInflater(getContext());
    }

    public void a() {
        if (this.f129i == null) {
            AppCompatImageButton appCompatImageButton = new AppCompatImageButton(getContext(), null, i.b.a.toolbarNavigationButtonStyle);
            this.f129i = appCompatImageButton;
            appCompatImageButton.setImageDrawable(this.g);
            this.f129i.setContentDescription(this.h);
            e generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = 8388611 | (this.f135o & 112);
            generateDefaultLayoutParams.b = 2;
            this.f129i.setLayoutParams(generateDefaultLayoutParams);
            this.f129i.setOnClickListener(new c());
        }
    }

    public final int b(View view, int i2, int[] iArr, int i3) {
        e eVar = (e) view.getLayoutParams();
        int i4 = eVar.rightMargin - iArr[1];
        int max = i2 - Math.max(0, i4);
        iArr[1] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, a2, max, view.getMeasuredHeight() + a2);
        return max - (measuredWidth + eVar.leftMargin);
    }

    public final void c() {
        d();
        ActionMenuView actionMenuView = this.b;
        if (actionMenuView.f86q == null) {
            MenuBuilder menuBuilder = (MenuBuilder) actionMenuView.getMenu();
            if (this.L == null) {
                this.L = new d();
            }
            this.b.setExpandedActionViewsExclusive(true);
            menuBuilder.a(this.L, this.f131k);
        }
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof e);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [androidx.appcompat.widget.ActionMenuView, int]
     candidates:
      androidx.appcompat.widget.Toolbar.a(android.view.View, int):int
      androidx.appcompat.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void */
    public final void d() {
        if (this.b == null) {
            ActionMenuView actionMenuView = new ActionMenuView(getContext());
            this.b = actionMenuView;
            actionMenuView.setPopupTheme(this.f132l);
            this.b.setOnMenuItemClickListener(this.I);
            ActionMenuView actionMenuView2 = this.b;
            MenuPresenter.a aVar = this.M;
            MenuBuilder.a aVar2 = this.N;
            actionMenuView2.v = aVar;
            actionMenuView2.w = aVar2;
            e generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = 8388613 | (this.f135o & 112);
            this.b.setLayoutParams(generateDefaultLayoutParams);
            a((View) this.b, false);
        }
    }

    public final void e() {
        if (this.f127e == null) {
            this.f127e = new AppCompatImageButton(getContext(), null, i.b.a.toolbarNavigationButtonStyle);
            e generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = 8388611 | (this.f135o & 112);
            this.f127e.setLayoutParams(generateDefaultLayoutParams);
        }
    }

    public boolean f() {
        ActionMenuView actionMenuView = this.b;
        if (actionMenuView != null) {
            ActionMenuPresenter actionMenuPresenter = actionMenuView.u;
            if (actionMenuPresenter != null && actionMenuPresenter.e()) {
                return true;
            }
        }
        return false;
    }

    public boolean g() {
        ActionMenuView actionMenuView = this.b;
        if (actionMenuView != null) {
            ActionMenuPresenter actionMenuPresenter = actionMenuView.u;
            if (actionMenuPresenter != null && actionMenuPresenter.f()) {
                return true;
            }
        }
        return false;
    }

    public CharSequence getCollapseContentDescription() {
        ImageButton imageButton = this.f129i;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public Drawable getCollapseIcon() {
        ImageButton imageButton = this.f129i;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public int getContentInsetEnd() {
        RtlSpacingHelper rtlSpacingHelper = this.u;
        if (rtlSpacingHelper != null) {
            return rtlSpacingHelper.g ? rtlSpacingHelper.a : rtlSpacingHelper.b;
        }
        return 0;
    }

    public int getContentInsetEndWithActions() {
        int i2 = this.w;
        return i2 != Integer.MIN_VALUE ? i2 : getContentInsetEnd();
    }

    public int getContentInsetLeft() {
        RtlSpacingHelper rtlSpacingHelper = this.u;
        if (rtlSpacingHelper != null) {
            return rtlSpacingHelper.a;
        }
        return 0;
    }

    public int getContentInsetRight() {
        RtlSpacingHelper rtlSpacingHelper = this.u;
        if (rtlSpacingHelper != null) {
            return rtlSpacingHelper.b;
        }
        return 0;
    }

    public int getContentInsetStart() {
        RtlSpacingHelper rtlSpacingHelper = this.u;
        if (rtlSpacingHelper != null) {
            return rtlSpacingHelper.g ? rtlSpacingHelper.b : rtlSpacingHelper.a;
        }
        return 0;
    }

    public int getContentInsetStartWithNavigation() {
        int i2 = this.v;
        return i2 != Integer.MIN_VALUE ? i2 : getContentInsetStart();
    }

    public int getCurrentContentInsetEnd() {
        MenuBuilder menuBuilder;
        ActionMenuView actionMenuView = this.b;
        if ((actionMenuView == null || (menuBuilder = actionMenuView.f86q) == null || !menuBuilder.hasVisibleItems()) ? false : true) {
            return Math.max(getContentInsetEnd(), Math.max(this.w, 0));
        }
        return getContentInsetEnd();
    }

    public int getCurrentContentInsetLeft() {
        if (ViewCompat.k(this) == 1) {
            return getCurrentContentInsetEnd();
        }
        return getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        if (ViewCompat.k(this) == 1) {
            return getCurrentContentInsetStart();
        }
        return getCurrentContentInsetEnd();
    }

    public int getCurrentContentInsetStart() {
        if (getNavigationIcon() != null) {
            return Math.max(getContentInsetStart(), Math.max(this.v, 0));
        }
        return getContentInsetStart();
    }

    public Drawable getLogo() {
        ImageView imageView = this.f128f;
        if (imageView != null) {
            return imageView.getDrawable();
        }
        return null;
    }

    public CharSequence getLogoDescription() {
        ImageView imageView = this.f128f;
        if (imageView != null) {
            return imageView.getContentDescription();
        }
        return null;
    }

    public Menu getMenu() {
        c();
        return this.b.getMenu();
    }

    public CharSequence getNavigationContentDescription() {
        ImageButton imageButton = this.f127e;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public Drawable getNavigationIcon() {
        ImageButton imageButton = this.f127e;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public ActionMenuPresenter getOuterActionMenuPresenter() {
        return this.K;
    }

    public Drawable getOverflowIcon() {
        c();
        return this.b.getOverflowIcon();
    }

    public Context getPopupContext() {
        return this.f131k;
    }

    public int getPopupTheme() {
        return this.f132l;
    }

    public CharSequence getSubtitle() {
        return this.z;
    }

    public final TextView getSubtitleTextView() {
        return this.d;
    }

    public CharSequence getTitle() {
        return this.y;
    }

    public int getTitleMarginBottom() {
        return this.f140t;
    }

    public int getTitleMarginEnd() {
        return this.f138r;
    }

    public int getTitleMarginStart() {
        return this.f137q;
    }

    public int getTitleMarginTop() {
        return this.f139s;
    }

    public final TextView getTitleTextView() {
        return this.c;
    }

    public DecorToolbar getWrapper() {
        if (this.J == null) {
            this.J = new ToolbarWidgetWrapper0(this, true);
        }
        return this.J;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.P);
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.D = false;
        }
        if (!this.D) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.D = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.D = false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:0x029f A[LOOP:0: B:101:0x029d->B:102:0x029f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02c1 A[LOOP:1: B:104:0x02bf->B:105:0x02c1, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02e6 A[LOOP:2: B:107:0x02e4->B:108:0x02e6, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0327  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0338 A[LOOP:3: B:115:0x0336->B:116:0x0338, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0225  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r20, int r21, int r22, int r23, int r24) {
        /*
            r19 = this;
            r0 = r19
            int r1 = i.h.l.ViewCompat.k(r19)
            r2 = 1
            r3 = 0
            if (r1 != r2) goto L_0x000c
            r1 = 1
            goto L_0x000d
        L_0x000c:
            r1 = 0
        L_0x000d:
            int r4 = r19.getWidth()
            int r5 = r19.getHeight()
            int r6 = r19.getPaddingLeft()
            int r7 = r19.getPaddingRight()
            int r8 = r19.getPaddingTop()
            int r9 = r19.getPaddingBottom()
            int r10 = r4 - r7
            int[] r11 = r0.G
            r11[r2] = r3
            r11[r3] = r3
            int r12 = r19.getMinimumHeight()
            if (r12 < 0) goto L_0x003a
            int r13 = r24 - r22
            int r12 = java.lang.Math.min(r12, r13)
            goto L_0x003b
        L_0x003a:
            r12 = 0
        L_0x003b:
            android.widget.ImageButton r13 = r0.f127e
            boolean r13 = r0.d(r13)
            if (r13 == 0) goto L_0x0055
            if (r1 == 0) goto L_0x004e
            android.widget.ImageButton r13 = r0.f127e
            int r13 = r0.b(r13, r10, r11, r12)
            r14 = r13
            r13 = r6
            goto L_0x0057
        L_0x004e:
            android.widget.ImageButton r13 = r0.f127e
            int r13 = r0.a(r13, r6, r11, r12)
            goto L_0x0056
        L_0x0055:
            r13 = r6
        L_0x0056:
            r14 = r10
        L_0x0057:
            android.widget.ImageButton r15 = r0.f129i
            boolean r15 = r0.d(r15)
            if (r15 == 0) goto L_0x006e
            if (r1 == 0) goto L_0x0068
            android.widget.ImageButton r15 = r0.f129i
            int r14 = r0.b(r15, r14, r11, r12)
            goto L_0x006e
        L_0x0068:
            android.widget.ImageButton r15 = r0.f129i
            int r13 = r0.a(r15, r13, r11, r12)
        L_0x006e:
            androidx.appcompat.widget.ActionMenuView r15 = r0.b
            boolean r15 = r0.d(r15)
            if (r15 == 0) goto L_0x0085
            if (r1 == 0) goto L_0x007f
            androidx.appcompat.widget.ActionMenuView r15 = r0.b
            int r13 = r0.a(r15, r13, r11, r12)
            goto L_0x0085
        L_0x007f:
            androidx.appcompat.widget.ActionMenuView r15 = r0.b
            int r14 = r0.b(r15, r14, r11, r12)
        L_0x0085:
            int r15 = r19.getCurrentContentInsetLeft()
            int r16 = r19.getCurrentContentInsetRight()
            int r2 = r15 - r13
            int r2 = java.lang.Math.max(r3, r2)
            r11[r3] = r2
            int r2 = r10 - r14
            int r2 = r16 - r2
            int r2 = java.lang.Math.max(r3, r2)
            r17 = 1
            r11[r17] = r2
            int r2 = java.lang.Math.max(r13, r15)
            int r10 = r10 - r16
            int r10 = java.lang.Math.min(r14, r10)
            android.view.View r13 = r0.f130j
            boolean r13 = r0.d(r13)
            if (r13 == 0) goto L_0x00c2
            if (r1 == 0) goto L_0x00bc
            android.view.View r13 = r0.f130j
            int r10 = r0.b(r13, r10, r11, r12)
            goto L_0x00c2
        L_0x00bc:
            android.view.View r13 = r0.f130j
            int r2 = r0.a(r13, r2, r11, r12)
        L_0x00c2:
            android.widget.ImageView r13 = r0.f128f
            boolean r13 = r0.d(r13)
            if (r13 == 0) goto L_0x00d9
            if (r1 == 0) goto L_0x00d3
            android.widget.ImageView r13 = r0.f128f
            int r10 = r0.b(r13, r10, r11, r12)
            goto L_0x00d9
        L_0x00d3:
            android.widget.ImageView r13 = r0.f128f
            int r2 = r0.a(r13, r2, r11, r12)
        L_0x00d9:
            android.widget.TextView r13 = r0.c
            boolean r13 = r0.d(r13)
            android.widget.TextView r14 = r0.d
            boolean r14 = r0.d(r14)
            if (r13 == 0) goto L_0x0100
            android.widget.TextView r15 = r0.c
            android.view.ViewGroup$LayoutParams r15 = r15.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r15 = (androidx.appcompat.widget.Toolbar.e) r15
            int r3 = r15.topMargin
            r23 = r7
            android.widget.TextView r7 = r0.c
            int r7 = r7.getMeasuredHeight()
            int r7 = r7 + r3
            int r3 = r15.bottomMargin
            int r7 = r7 + r3
            r3 = 0
            int r7 = r7 + r3
            goto L_0x0103
        L_0x0100:
            r23 = r7
            r7 = 0
        L_0x0103:
            if (r14 == 0) goto L_0x011d
            android.widget.TextView r3 = r0.d
            android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r3 = (androidx.appcompat.widget.Toolbar.e) r3
            int r15 = r3.topMargin
            r16 = r4
            android.widget.TextView r4 = r0.d
            int r4 = r4.getMeasuredHeight()
            int r4 = r4 + r15
            int r3 = r3.bottomMargin
            int r4 = r4 + r3
            int r7 = r7 + r4
            goto L_0x011f
        L_0x011d:
            r16 = r4
        L_0x011f:
            if (r13 != 0) goto L_0x012a
            if (r14 == 0) goto L_0x0124
            goto L_0x012a
        L_0x0124:
            r18 = r6
            r22 = r12
            goto L_0x028f
        L_0x012a:
            if (r13 == 0) goto L_0x012f
            android.widget.TextView r3 = r0.c
            goto L_0x0131
        L_0x012f:
            android.widget.TextView r3 = r0.d
        L_0x0131:
            if (r14 == 0) goto L_0x0136
            android.widget.TextView r4 = r0.d
            goto L_0x0138
        L_0x0136:
            android.widget.TextView r4 = r0.c
        L_0x0138:
            android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r3 = (androidx.appcompat.widget.Toolbar.e) r3
            android.view.ViewGroup$LayoutParams r4 = r4.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r4 = (androidx.appcompat.widget.Toolbar.e) r4
            if (r13 == 0) goto L_0x014e
            android.widget.TextView r15 = r0.c
            int r15 = r15.getMeasuredWidth()
            if (r15 > 0) goto L_0x0158
        L_0x014e:
            if (r14 == 0) goto L_0x015b
            android.widget.TextView r15 = r0.d
            int r15 = r15.getMeasuredWidth()
            if (r15 <= 0) goto L_0x015b
        L_0x0158:
            r17 = 1
            goto L_0x015d
        L_0x015b:
            r17 = 0
        L_0x015d:
            int r15 = r0.x
            r15 = r15 & 112(0x70, float:1.57E-43)
            r18 = r6
            r6 = 48
            if (r15 == r6) goto L_0x01a5
            r6 = 80
            if (r15 == r6) goto L_0x0197
            int r6 = r5 - r8
            int r6 = r6 - r9
            int r6 = r6 - r7
            int r6 = r6 / 2
            int r15 = r3.topMargin
            r22 = r12
            int r12 = r0.f139s
            r24 = r2
            int r2 = r15 + r12
            if (r6 >= r2) goto L_0x0180
            int r6 = r15 + r12
            goto L_0x0195
        L_0x0180:
            int r5 = r5 - r9
            int r5 = r5 - r7
            int r5 = r5 - r6
            int r5 = r5 - r8
            int r2 = r3.bottomMargin
            int r3 = r0.f140t
            int r2 = r2 + r3
            if (r5 >= r2) goto L_0x0195
            int r2 = r4.bottomMargin
            int r2 = r2 + r3
            int r2 = r2 - r5
            int r6 = r6 - r2
            r2 = 0
            int r6 = java.lang.Math.max(r2, r6)
        L_0x0195:
            int r8 = r8 + r6
            goto L_0x01b4
        L_0x0197:
            r24 = r2
            r22 = r12
            int r5 = r5 - r9
            int r2 = r4.bottomMargin
            int r5 = r5 - r2
            int r2 = r0.f140t
            int r5 = r5 - r2
            int r8 = r5 - r7
            goto L_0x01b4
        L_0x01a5:
            r24 = r2
            r22 = r12
            int r2 = r19.getPaddingTop()
            int r3 = r3.topMargin
            int r2 = r2 + r3
            int r3 = r0.f139s
            int r8 = r2 + r3
        L_0x01b4:
            if (r1 == 0) goto L_0x0225
            if (r17 == 0) goto L_0x01bb
            int r1 = r0.f137q
            goto L_0x01bc
        L_0x01bb:
            r1 = 0
        L_0x01bc:
            r2 = 1
            r3 = r11[r2]
            int r1 = r1 - r3
            r3 = 0
            int r4 = java.lang.Math.max(r3, r1)
            int r10 = r10 - r4
            int r1 = -r1
            int r1 = java.lang.Math.max(r3, r1)
            r11[r2] = r1
            if (r13 == 0) goto L_0x01f3
            android.widget.TextView r1 = r0.c
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r1 = (androidx.appcompat.widget.Toolbar.e) r1
            android.widget.TextView r2 = r0.c
            int r2 = r2.getMeasuredWidth()
            int r2 = r10 - r2
            android.widget.TextView r3 = r0.c
            int r3 = r3.getMeasuredHeight()
            int r3 = r3 + r8
            android.widget.TextView r4 = r0.c
            r4.layout(r2, r8, r10, r3)
            int r4 = r0.f138r
            int r2 = r2 - r4
            int r1 = r1.bottomMargin
            int r8 = r3 + r1
            goto L_0x01f4
        L_0x01f3:
            r2 = r10
        L_0x01f4:
            if (r14 == 0) goto L_0x021a
            android.widget.TextView r1 = r0.d
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r1 = (androidx.appcompat.widget.Toolbar.e) r1
            int r1 = r1.topMargin
            int r8 = r8 + r1
            android.widget.TextView r1 = r0.d
            int r1 = r1.getMeasuredWidth()
            int r1 = r10 - r1
            android.widget.TextView r3 = r0.d
            int r3 = r3.getMeasuredHeight()
            int r3 = r3 + r8
            android.widget.TextView r4 = r0.d
            r4.layout(r1, r8, r10, r3)
            int r1 = r0.f138r
            int r1 = r10 - r1
            goto L_0x021b
        L_0x021a:
            r1 = r10
        L_0x021b:
            if (r17 == 0) goto L_0x0222
            int r1 = java.lang.Math.min(r2, r1)
            r10 = r1
        L_0x0222:
            r2 = r24
            goto L_0x028f
        L_0x0225:
            if (r17 == 0) goto L_0x022a
            int r1 = r0.f137q
            goto L_0x022b
        L_0x022a:
            r1 = 0
        L_0x022b:
            r2 = 0
            r3 = r11[r2]
            int r1 = r1 - r3
            int r3 = java.lang.Math.max(r2, r1)
            int r3 = r3 + r24
            int r1 = -r1
            int r1 = java.lang.Math.max(r2, r1)
            r11[r2] = r1
            if (r13 == 0) goto L_0x0261
            android.widget.TextView r1 = r0.c
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r1 = (androidx.appcompat.widget.Toolbar.e) r1
            android.widget.TextView r2 = r0.c
            int r2 = r2.getMeasuredWidth()
            int r2 = r2 + r3
            android.widget.TextView r4 = r0.c
            int r4 = r4.getMeasuredHeight()
            int r4 = r4 + r8
            android.widget.TextView r5 = r0.c
            r5.layout(r3, r8, r2, r4)
            int r5 = r0.f138r
            int r2 = r2 + r5
            int r1 = r1.bottomMargin
            int r8 = r4 + r1
            goto L_0x0262
        L_0x0261:
            r2 = r3
        L_0x0262:
            if (r14 == 0) goto L_0x0286
            android.widget.TextView r1 = r0.d
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r1 = (androidx.appcompat.widget.Toolbar.e) r1
            int r1 = r1.topMargin
            int r8 = r8 + r1
            android.widget.TextView r1 = r0.d
            int r1 = r1.getMeasuredWidth()
            int r1 = r1 + r3
            android.widget.TextView r4 = r0.d
            int r4 = r4.getMeasuredHeight()
            int r4 = r4 + r8
            android.widget.TextView r5 = r0.d
            r5.layout(r3, r8, r1, r4)
            int r4 = r0.f138r
            int r1 = r1 + r4
            goto L_0x0287
        L_0x0286:
            r1 = r3
        L_0x0287:
            if (r17 == 0) goto L_0x028e
            int r2 = java.lang.Math.max(r2, r1)
            goto L_0x028f
        L_0x028e:
            r2 = r3
        L_0x028f:
            java.util.ArrayList<android.view.View> r1 = r0.E
            r3 = 3
            r0.a(r1, r3)
            java.util.ArrayList<android.view.View> r1 = r0.E
            int r1 = r1.size()
            r3 = r2
            r2 = 0
        L_0x029d:
            if (r2 >= r1) goto L_0x02b0
            java.util.ArrayList<android.view.View> r4 = r0.E
            java.lang.Object r4 = r4.get(r2)
            android.view.View r4 = (android.view.View) r4
            r12 = r22
            int r3 = r0.a(r4, r3, r11, r12)
            int r2 = r2 + 1
            goto L_0x029d
        L_0x02b0:
            r12 = r22
            java.util.ArrayList<android.view.View> r1 = r0.E
            r2 = 5
            r0.a(r1, r2)
            java.util.ArrayList<android.view.View> r1 = r0.E
            int r1 = r1.size()
            r2 = 0
        L_0x02bf:
            if (r2 >= r1) goto L_0x02d0
            java.util.ArrayList<android.view.View> r4 = r0.E
            java.lang.Object r4 = r4.get(r2)
            android.view.View r4 = (android.view.View) r4
            int r10 = r0.b(r4, r10, r11, r12)
            int r2 = r2 + 1
            goto L_0x02bf
        L_0x02d0:
            java.util.ArrayList<android.view.View> r1 = r0.E
            r2 = 1
            r0.a(r1, r2)
            java.util.ArrayList<android.view.View> r1 = r0.E
            r4 = 0
            r5 = r11[r4]
            r2 = r11[r2]
            int r4 = r1.size()
            r7 = r5
            r5 = 0
            r6 = 0
        L_0x02e4:
            if (r5 >= r4) goto L_0x0317
            java.lang.Object r8 = r1.get(r5)
            android.view.View r8 = (android.view.View) r8
            android.view.ViewGroup$LayoutParams r9 = r8.getLayoutParams()
            androidx.appcompat.widget.Toolbar$e r9 = (androidx.appcompat.widget.Toolbar.e) r9
            int r13 = r9.leftMargin
            int r13 = r13 - r7
            int r7 = r9.rightMargin
            int r7 = r7 - r2
            r2 = 0
            int r9 = java.lang.Math.max(r2, r13)
            int r14 = java.lang.Math.max(r2, r7)
            int r13 = -r13
            int r13 = java.lang.Math.max(r2, r13)
            int r7 = -r7
            int r7 = java.lang.Math.max(r2, r7)
            int r8 = r8.getMeasuredWidth()
            int r8 = r8 + r9
            int r8 = r8 + r14
            int r6 = r6 + r8
            int r5 = r5 + 1
            r2 = r7
            r7 = r13
            goto L_0x02e4
        L_0x0317:
            r2 = 0
            int r4 = r16 - r18
            int r4 = r4 - r23
            int r4 = r4 / 2
            int r4 = r4 + r18
            int r1 = r6 / 2
            int r4 = r4 - r1
            int r6 = r6 + r4
            if (r4 >= r3) goto L_0x0327
            goto L_0x032e
        L_0x0327:
            if (r6 <= r10) goto L_0x032d
            int r6 = r6 - r10
            int r3 = r4 - r6
            goto L_0x032e
        L_0x032d:
            r3 = r4
        L_0x032e:
            java.util.ArrayList<android.view.View> r1 = r0.E
            int r1 = r1.size()
            r2 = r3
            r3 = 0
        L_0x0336:
            if (r3 >= r1) goto L_0x0347
            java.util.ArrayList<android.view.View> r4 = r0.E
            java.lang.Object r4 = r4.get(r3)
            android.view.View r4 = (android.view.View) r4
            int r2 = r0.a(r4, r2, r11, r12)
            int r3 = r3 + 1
            goto L_0x0336
        L_0x0347:
            java.util.ArrayList<android.view.View> r1 = r0.E
            r1.clear()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.onLayout(boolean, int, int, int, int):void");
    }

    public void onMeasure(int i2, int i3) {
        char c2;
        char c3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int[] iArr = this.G;
        boolean z2 = true;
        int i11 = 0;
        if (ViewUtils.a(this)) {
            c3 = 1;
            c2 = 0;
        } else {
            c3 = 0;
            c2 = 1;
        }
        if (d(this.f127e)) {
            a(this.f127e, i2, 0, i3, 0, this.f136p);
            i6 = a(this.f127e) + this.f127e.getMeasuredWidth();
            i5 = Math.max(0, b(this.f127e) + this.f127e.getMeasuredHeight());
            i4 = View.combineMeasuredStates(0, this.f127e.getMeasuredState());
        } else {
            i6 = 0;
            i5 = 0;
            i4 = 0;
        }
        if (d(this.f129i)) {
            a(this.f129i, i2, 0, i3, 0, this.f136p);
            i6 = a(this.f129i) + this.f129i.getMeasuredWidth();
            i5 = Math.max(i5, b(this.f129i) + this.f129i.getMeasuredHeight());
            i4 = View.combineMeasuredStates(i4, this.f129i.getMeasuredState());
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max = Math.max(currentContentInsetStart, i6) + 0;
        iArr[c3] = Math.max(0, currentContentInsetStart - i6);
        if (d(this.b)) {
            a(this.b, i2, max, i3, 0, this.f136p);
            i7 = a(this.b) + this.b.getMeasuredWidth();
            i5 = Math.max(i5, b(this.b) + this.b.getMeasuredHeight());
            i4 = View.combineMeasuredStates(i4, this.b.getMeasuredState());
        } else {
            i7 = 0;
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max2 = Math.max(currentContentInsetEnd, i7) + max;
        iArr[c2] = Math.max(0, currentContentInsetEnd - i7);
        if (d(this.f130j)) {
            max2 += a(this.f130j, i2, max2, i3, 0, iArr);
            i5 = Math.max(i5, b(this.f130j) + this.f130j.getMeasuredHeight());
            i4 = View.combineMeasuredStates(i4, this.f130j.getMeasuredState());
        }
        if (d(this.f128f)) {
            max2 += a(this.f128f, i2, max2, i3, 0, iArr);
            i5 = Math.max(i5, b(this.f128f) + this.f128f.getMeasuredHeight());
            i4 = View.combineMeasuredStates(i4, this.f128f.getMeasuredState());
        }
        int childCount = getChildCount();
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt = getChildAt(i12);
            if (((e) childAt.getLayoutParams()).b == 0 && d(childAt)) {
                View view = childAt;
                max2 += a(childAt, i2, max2, i3, 0, iArr);
                View view2 = view;
                i5 = Math.max(i5, b(view2) + view.getMeasuredHeight());
                i4 = View.combineMeasuredStates(i4, view2.getMeasuredState());
            }
        }
        int i13 = this.f139s + this.f140t;
        int i14 = this.f137q + this.f138r;
        if (d(this.c)) {
            a(this.c, i2, max2 + i14, i3, i13, iArr);
            int a2 = a(this.c) + this.c.getMeasuredWidth();
            i8 = b(this.c) + this.c.getMeasuredHeight();
            i10 = View.combineMeasuredStates(i4, this.c.getMeasuredState());
            i9 = a2;
        } else {
            i10 = i4;
            i9 = 0;
            i8 = 0;
        }
        if (d(this.d)) {
            int i15 = i8 + i13;
            i9 = Math.max(i9, a(this.d, i2, max2 + i14, i3, i15, iArr));
            i8 = b(this.d) + this.d.getMeasuredHeight() + i8;
            i10 = View.combineMeasuredStates(i10, this.d.getMeasuredState());
        }
        int max3 = Math.max(i5, i8);
        int paddingRight = getPaddingRight() + getPaddingLeft();
        int paddingBottom = getPaddingBottom() + getPaddingTop() + max3;
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(paddingRight + max2 + i9, getSuggestedMinimumWidth()), i2, -16777216 & i10);
        int resolveSizeAndState2 = View.resolveSizeAndState(Math.max(paddingBottom, getSuggestedMinimumHeight()), i3, i10 << 16);
        if (this.O) {
            int childCount2 = getChildCount();
            int i16 = 0;
            while (true) {
                if (i16 >= childCount2) {
                    break;
                }
                View childAt2 = getChildAt(i16);
                if (d(childAt2) && childAt2.getMeasuredWidth() > 0 && childAt2.getMeasuredHeight() > 0) {
                    break;
                }
                i16++;
            }
        }
        z2 = false;
        if (!z2) {
            i11 = resolveSizeAndState2;
        }
        setMeasuredDimension(resolveSizeAndState, i11);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (!(parcelable instanceof g)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        g gVar = (g) parcelable;
        super.onRestoreInstanceState(gVar.b);
        ActionMenuView actionMenuView = this.b;
        MenuBuilder menuBuilder = actionMenuView != null ? actionMenuView.f86q : null;
        int i2 = gVar.d;
        if (!(i2 == 0 || this.L == null || menuBuilder == null || (findItem = menuBuilder.findItem(i2)) == null)) {
            findItem.expandActionView();
        }
        if (gVar.f141e) {
            removeCallbacks(this.P);
            post(this.P);
        }
    }

    public void onRtlPropertiesChanged(int i2) {
        super.onRtlPropertiesChanged(i2);
        b();
        RtlSpacingHelper rtlSpacingHelper = this.u;
        boolean z2 = true;
        if (i2 != 1) {
            z2 = false;
        }
        if (z2 != rtlSpacingHelper.g) {
            rtlSpacingHelper.g = z2;
            if (!rtlSpacingHelper.h) {
                rtlSpacingHelper.a = rtlSpacingHelper.f1015e;
                rtlSpacingHelper.b = rtlSpacingHelper.f1016f;
            } else if (z2) {
                int i3 = rtlSpacingHelper.d;
                if (i3 == Integer.MIN_VALUE) {
                    i3 = rtlSpacingHelper.f1015e;
                }
                rtlSpacingHelper.a = i3;
                int i4 = rtlSpacingHelper.c;
                if (i4 == Integer.MIN_VALUE) {
                    i4 = rtlSpacingHelper.f1016f;
                }
                rtlSpacingHelper.b = i4;
            } else {
                int i5 = rtlSpacingHelper.c;
                if (i5 == Integer.MIN_VALUE) {
                    i5 = rtlSpacingHelper.f1015e;
                }
                rtlSpacingHelper.a = i5;
                int i6 = rtlSpacingHelper.d;
                if (i6 == Integer.MIN_VALUE) {
                    i6 = rtlSpacingHelper.f1016f;
                }
                rtlSpacingHelper.b = i6;
            }
        }
    }

    public Parcelable onSaveInstanceState() {
        MenuItemImpl menuItemImpl;
        g gVar = new g(super.onSaveInstanceState());
        d dVar = this.L;
        if (!(dVar == null || (menuItemImpl = dVar.c) == null)) {
            gVar.d = menuItemImpl.a;
        }
        gVar.f141e = f();
        return gVar;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.C = false;
        }
        if (!this.C) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.C = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.C = false;
        }
        return true;
    }

    public void setCollapseContentDescription(int i2) {
        setCollapseContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setCollapseIcon(int i2) {
        setCollapseIcon(AppCompatResources.c(getContext(), i2));
    }

    public void setCollapsible(boolean z2) {
        this.O = z2;
        requestLayout();
    }

    public void setContentInsetEndWithActions(int i2) {
        if (i2 < 0) {
            i2 = RecyclerView.UNDEFINED_DURATION;
        }
        if (i2 != this.w) {
            this.w = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setContentInsetStartWithNavigation(int i2) {
        if (i2 < 0) {
            i2 = RecyclerView.UNDEFINED_DURATION;
        }
        if (i2 != this.v) {
            this.v = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setLogo(int i2) {
        setLogo(AppCompatResources.c(getContext(), i2));
    }

    public void setLogoDescription(int i2) {
        setLogoDescription(getContext().getText(i2));
    }

    public void setNavigationContentDescription(int i2) {
        setNavigationContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setNavigationIcon(int i2) {
        setNavigationIcon(AppCompatResources.c(getContext(), i2));
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        e();
        this.f127e.setOnClickListener(onClickListener);
    }

    public void setOnMenuItemClickListener(f fVar) {
        this.H = fVar;
    }

    public void setOverflowIcon(Drawable drawable) {
        c();
        this.b.setOverflowIcon(drawable);
    }

    public void setPopupTheme(int i2) {
        if (this.f132l != i2) {
            this.f132l = i2;
            if (i2 == 0) {
                this.f131k = getContext();
            } else {
                this.f131k = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public void setSubtitle(int i2) {
        setSubtitle(getContext().getText(i2));
    }

    public void setSubtitleTextColor(int i2) {
        setSubtitleTextColor(ColorStateList.valueOf(i2));
    }

    public void setTitle(int i2) {
        setTitle(getContext().getText(i2));
    }

    public void setTitleMarginBottom(int i2) {
        this.f140t = i2;
        requestLayout();
    }

    public void setTitleMarginEnd(int i2) {
        this.f138r = i2;
        requestLayout();
    }

    public void setTitleMarginStart(int i2) {
        this.f137q = i2;
        requestLayout();
    }

    public void setTitleMarginTop(int i2) {
        this.f139s = i2;
        requestLayout();
    }

    public void setTitleTextColor(int i2) {
        setTitleTextColor(ColorStateList.valueOf(i2));
    }

    public static class e extends ActionBar.a {
        public int b = 0;

        public e(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public e(int i2, int i3) {
            super(i2, i3);
            super.a = 8388627;
        }

        public e(e eVar) {
            super((ActionBar.a) super);
            this.b = eVar.b;
        }

        public e(ActionBar.a aVar) {
            super(super);
        }

        public e(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }

        public e(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.b.a.toolbarStyle);
    }

    public e generateDefaultLayoutParams() {
        return new e(-2, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new e(getContext(), attributeSet);
    }

    public void setCollapseContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            a();
        }
        ImageButton imageButton = this.f129i;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    public void setCollapseIcon(Drawable drawable) {
        if (drawable != null) {
            a();
            this.f129i.setImageDrawable(drawable);
            return;
        }
        ImageButton imageButton = this.f129i;
        if (imageButton != null) {
            imageButton.setImageDrawable(this.g);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.ImageView, int]
     candidates:
      androidx.appcompat.widget.Toolbar.a(android.view.View, int):int
      androidx.appcompat.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void */
    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            if (this.f128f == null) {
                this.f128f = new AppCompatImageView(getContext(), null, 0);
            }
            if (!c(this.f128f)) {
                a((View) this.f128f, true);
            }
        } else {
            ImageView imageView = this.f128f;
            if (imageView != null && c(imageView)) {
                removeView(this.f128f);
                this.F.remove(this.f128f);
            }
        }
        ImageView imageView2 = this.f128f;
        if (imageView2 != null) {
            imageView2.setImageDrawable(drawable);
        }
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence) && this.f128f == null) {
            this.f128f = new AppCompatImageView(getContext(), null, 0);
        }
        ImageView imageView = this.f128f;
        if (imageView != null) {
            imageView.setContentDescription(charSequence);
        }
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            e();
        }
        ImageButton imageButton = this.f127e;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.ImageButton, int]
     candidates:
      androidx.appcompat.widget.Toolbar.a(android.view.View, int):int
      androidx.appcompat.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void */
    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            e();
            if (!c(this.f127e)) {
                a((View) this.f127e, true);
            }
        } else {
            ImageButton imageButton = this.f127e;
            if (imageButton != null && c(imageButton)) {
                removeView(this.f127e);
                this.F.remove(this.f127e);
            }
        }
        ImageButton imageButton2 = this.f127e;
        if (imageButton2 != null) {
            imageButton2.setImageDrawable(drawable);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      androidx.appcompat.widget.Toolbar.a(android.view.View, int):int
      androidx.appcompat.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void */
    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.d == null) {
                Context context = getContext();
                AppCompatTextView appCompatTextView = new AppCompatTextView(context);
                this.d = appCompatTextView;
                appCompatTextView.setSingleLine();
                this.d.setEllipsize(TextUtils.TruncateAt.END);
                int i2 = this.f134n;
                if (i2 != 0) {
                    this.d.setTextAppearance(context, i2);
                }
                ColorStateList colorStateList = this.B;
                if (colorStateList != null) {
                    this.d.setTextColor(colorStateList);
                }
            }
            if (!c(this.d)) {
                a((View) this.d, true);
            }
        } else {
            TextView textView = this.d;
            if (textView != null && c(textView)) {
                removeView(this.d);
                this.F.remove(this.d);
            }
        }
        TextView textView2 = this.d;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.z = charSequence;
    }

    public void setSubtitleTextColor(ColorStateList colorStateList) {
        this.B = colorStateList;
        TextView textView = this.d;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      androidx.appcompat.widget.Toolbar.a(android.view.View, int):int
      androidx.appcompat.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      androidx.appcompat.widget.Toolbar.a(android.view.View, boolean):void */
    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.c == null) {
                Context context = getContext();
                AppCompatTextView appCompatTextView = new AppCompatTextView(context);
                this.c = appCompatTextView;
                appCompatTextView.setSingleLine();
                this.c.setEllipsize(TextUtils.TruncateAt.END);
                int i2 = this.f133m;
                if (i2 != 0) {
                    this.c.setTextAppearance(context, i2);
                }
                ColorStateList colorStateList = this.A;
                if (colorStateList != null) {
                    this.c.setTextColor(colorStateList);
                }
            }
            if (!c(this.c)) {
                a((View) this.c, true);
            }
        } else {
            TextView textView = this.c;
            if (textView != null && c(textView)) {
                removeView(this.c);
                this.F.remove(this.c);
            }
        }
        TextView textView2 = this.c;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.y = charSequence;
    }

    public void setTitleTextColor(ColorStateList colorStateList) {
        this.A = colorStateList;
        TextView textView = this.c;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public class d implements MenuPresenter {
        public MenuBuilder b;
        public MenuItemImpl c;

        public d() {
        }

        public void a(Context context, MenuBuilder menuBuilder) {
            MenuItemImpl menuItemImpl;
            MenuBuilder menuBuilder2 = this.b;
            if (!(menuBuilder2 == null || (menuItemImpl = this.c) == null)) {
                menuBuilder2.a(menuItemImpl);
            }
            this.b = menuBuilder;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
        }

        public void a(MenuPresenter.a aVar) {
        }

        public boolean a(SubMenuBuilder subMenuBuilder) {
            return false;
        }

        public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            Toolbar.this.a();
            ViewParent parent = Toolbar.this.f129i.getParent();
            Toolbar toolbar = Toolbar.this;
            if (parent != toolbar) {
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(toolbar.f129i);
                }
                Toolbar toolbar2 = Toolbar.this;
                toolbar2.addView(toolbar2.f129i);
            }
            Toolbar.this.f130j = menuItemImpl.getActionView();
            this.c = menuItemImpl;
            ViewParent parent2 = Toolbar.this.f130j.getParent();
            Toolbar toolbar3 = Toolbar.this;
            if (parent2 != toolbar3) {
                if (parent2 instanceof ViewGroup) {
                    ((ViewGroup) parent2).removeView(toolbar3.f130j);
                }
                e generateDefaultLayoutParams = Toolbar.this.generateDefaultLayoutParams();
                Toolbar toolbar4 = Toolbar.this;
                generateDefaultLayoutParams.a = 8388611 | (toolbar4.f135o & 112);
                generateDefaultLayoutParams.b = 2;
                toolbar4.f130j.setLayoutParams(generateDefaultLayoutParams);
                Toolbar toolbar5 = Toolbar.this;
                toolbar5.addView(toolbar5.f130j);
            }
            Toolbar toolbar6 = Toolbar.this;
            int childCount = toolbar6.getChildCount();
            while (true) {
                childCount--;
                if (childCount < 0) {
                    break;
                }
                View childAt = toolbar6.getChildAt(childCount);
                if (!(((e) childAt.getLayoutParams()).b == 2 || childAt == toolbar6.b)) {
                    toolbar6.removeViewAt(childCount);
                    toolbar6.F.add(childAt);
                }
            }
            Toolbar.this.requestLayout();
            menuItemImpl.C = true;
            menuItemImpl.f903n.b(false);
            View view = Toolbar.this.f130j;
            if (view instanceof CollapsibleActionView) {
                ((CollapsibleActionView) view).a();
            }
            return true;
        }

        public boolean c() {
            return false;
        }

        public void a(boolean z) {
            if (this.c != null) {
                MenuBuilder menuBuilder = this.b;
                boolean z2 = false;
                if (menuBuilder != null) {
                    int size = menuBuilder.size();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= size) {
                            break;
                        } else if (this.b.getItem(i2) == this.c) {
                            z2 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
                if (!z2) {
                    a(this.b, this.c);
                }
            }
        }

        public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            View view = Toolbar.this.f130j;
            if (view instanceof CollapsibleActionView) {
                ((CollapsibleActionView) view).b();
            }
            Toolbar toolbar = Toolbar.this;
            toolbar.removeView(toolbar.f130j);
            Toolbar toolbar2 = Toolbar.this;
            toolbar2.removeView(toolbar2.f129i);
            Toolbar toolbar3 = Toolbar.this;
            toolbar3.f130j = null;
            int size = toolbar3.F.size();
            while (true) {
                size--;
                if (size >= 0) {
                    toolbar3.addView(toolbar3.F.get(size));
                } else {
                    toolbar3.F.clear();
                    this.c = null;
                    Toolbar.this.requestLayout();
                    menuItemImpl.C = false;
                    menuItemImpl.f903n.b(false);
                    return true;
                }
            }
        }
    }

    public static class g extends AbsSavedState {
        public static final Parcelable.Creator<g> CREATOR = new a();
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f141e;

        public static class a implements Parcelable.ClassLoaderCreator<g> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new g(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new g[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new g(parcel, null);
            }
        }

        public g(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.d = parcel.readInt();
            this.f141e = parcel.readInt() != 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            parcel.writeInt(this.d);
            parcel.writeInt(this.f141e ? 1 : 0);
        }

        public g(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.x = 8388627;
        this.E = new ArrayList<>();
        this.F = new ArrayList<>();
        this.G = new int[2];
        this.I = new a();
        this.P = new b();
        TintTypedArray a2 = TintTypedArray.a(getContext(), attributeSet, j.Toolbar, i2, 0);
        this.f133m = a2.f(j.Toolbar_titleTextAppearance, 0);
        this.f134n = a2.f(j.Toolbar_subtitleTextAppearance, 0);
        this.x = a2.b.getInteger(j.Toolbar_android_gravity, this.x);
        this.f135o = a2.b.getInteger(j.Toolbar_buttonGravity, 48);
        int b2 = a2.b(j.Toolbar_titleMargin, 0);
        b2 = a2.f(j.Toolbar_titleMargins) ? a2.b(j.Toolbar_titleMargins, b2) : b2;
        this.f140t = b2;
        this.f139s = b2;
        this.f138r = b2;
        this.f137q = b2;
        int b3 = a2.b(j.Toolbar_titleMarginStart, -1);
        if (b3 >= 0) {
            this.f137q = b3;
        }
        int b4 = a2.b(j.Toolbar_titleMarginEnd, -1);
        if (b4 >= 0) {
            this.f138r = b4;
        }
        int b5 = a2.b(j.Toolbar_titleMarginTop, -1);
        if (b5 >= 0) {
            this.f139s = b5;
        }
        int b6 = a2.b(j.Toolbar_titleMarginBottom, -1);
        if (b6 >= 0) {
            this.f140t = b6;
        }
        this.f136p = a2.c(j.Toolbar_maxButtonHeight, -1);
        int b7 = a2.b(j.Toolbar_contentInsetStart, RecyclerView.UNDEFINED_DURATION);
        int b8 = a2.b(j.Toolbar_contentInsetEnd, RecyclerView.UNDEFINED_DURATION);
        int c2 = a2.c(j.Toolbar_contentInsetLeft, 0);
        int c3 = a2.c(j.Toolbar_contentInsetRight, 0);
        b();
        RtlSpacingHelper rtlSpacingHelper = this.u;
        rtlSpacingHelper.h = false;
        if (c2 != Integer.MIN_VALUE) {
            rtlSpacingHelper.f1015e = c2;
            rtlSpacingHelper.a = c2;
        }
        if (c3 != Integer.MIN_VALUE) {
            rtlSpacingHelper.f1016f = c3;
            rtlSpacingHelper.b = c3;
        }
        if (!(b7 == Integer.MIN_VALUE && b8 == Integer.MIN_VALUE)) {
            this.u.a(b7, b8);
        }
        this.v = a2.b(j.Toolbar_contentInsetStartWithNavigation, RecyclerView.UNDEFINED_DURATION);
        this.w = a2.b(j.Toolbar_contentInsetEndWithActions, RecyclerView.UNDEFINED_DURATION);
        this.g = a2.b(j.Toolbar_collapseIcon);
        this.h = a2.e(j.Toolbar_collapseContentDescription);
        CharSequence e2 = a2.e(j.Toolbar_title);
        if (!TextUtils.isEmpty(e2)) {
            setTitle(e2);
        }
        CharSequence e3 = a2.e(j.Toolbar_subtitle);
        if (!TextUtils.isEmpty(e3)) {
            setSubtitle(e3);
        }
        this.f131k = getContext();
        setPopupTheme(a2.f(j.Toolbar_popupTheme, 0));
        Drawable b9 = a2.b(j.Toolbar_navigationIcon);
        if (b9 != null) {
            setNavigationIcon(b9);
        }
        CharSequence e4 = a2.e(j.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(e4)) {
            setNavigationContentDescription(e4);
        }
        Drawable b10 = a2.b(j.Toolbar_logo);
        if (b10 != null) {
            setLogo(b10);
        }
        CharSequence e5 = a2.e(j.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(e5)) {
            setLogoDescription(e5);
        }
        if (a2.f(j.Toolbar_titleTextColor)) {
            setTitleTextColor(a2.a(j.Toolbar_titleTextColor));
        }
        if (a2.f(j.Toolbar_subtitleTextColor)) {
            setSubtitleTextColor(a2.a(j.Toolbar_subtitleTextColor));
        }
        if (a2.f(j.Toolbar_menu)) {
            getMenuInflater().inflate(a2.f(j.Toolbar_menu, 0), getMenu());
        }
        a2.b.recycle();
    }

    public e generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof e) {
            return new e((e) layoutParams);
        }
        if (layoutParams instanceof ActionBar.a) {
            return new e((ActionBar.a) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new e((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new e(layoutParams);
    }

    public final int b(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    public final boolean c(View view) {
        return view.getParent() == this || this.F.contains(view);
    }

    public final void a(View view, boolean z2) {
        e eVar;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            eVar = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(layoutParams)) {
            eVar = generateLayoutParams(layoutParams);
        } else {
            eVar = (e) layoutParams;
        }
        eVar.b = 1;
        if (!z2 || this.f130j == null) {
            addView(view, eVar);
            return;
        }
        view.setLayoutParams(eVar);
        this.F.add(view);
    }

    public final void b() {
        if (this.u == null) {
            this.u = new RtlSpacingHelper();
        }
    }

    public final boolean d(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    public final void a(View view, int i2, int i3, int i4, int i5, int i6) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(i2, getPaddingRight() + getPaddingLeft() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width);
        int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(i4, getPaddingBottom() + getPaddingTop() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i6 >= 0) {
            if (mode != 0) {
                i6 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i6);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i6, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    public final int a(View view, int i2, int i3, int i4, int i5, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i6 = marginLayoutParams.leftMargin - iArr[0];
        int i7 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i7) + Math.max(0, i6);
        iArr[0] = Math.max(0, -i6);
        iArr[1] = Math.max(0, -i7);
        view.measure(ViewGroup.getChildMeasureSpec(i2, getPaddingRight() + getPaddingLeft() + max + i3, marginLayoutParams.width), ViewGroup.getChildMeasureSpec(i4, getPaddingBottom() + getPaddingTop() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    public final int a(View view, int i2, int[] iArr, int i3) {
        e eVar = (e) view.getLayoutParams();
        int i4 = eVar.leftMargin - iArr[0];
        int max = Math.max(0, i4) + i2;
        iArr[0] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, a2, max + measuredWidth, view.getMeasuredHeight() + a2);
        return measuredWidth + eVar.rightMargin + max;
    }

    public final int a(View view, int i2) {
        e eVar = (e) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i3 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        int i4 = eVar.a & 112;
        if (!(i4 == 16 || i4 == 48 || i4 == 80)) {
            i4 = this.x & 112;
        }
        if (i4 == 48) {
            return getPaddingTop() - i3;
        }
        if (i4 == 80) {
            return (((getHeight() - getPaddingBottom()) - measuredHeight) - eVar.bottomMargin) - i3;
        }
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int height = getHeight();
        int i5 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
        int i6 = eVar.topMargin;
        if (i5 < i6) {
            i5 = i6;
        } else {
            int i7 = (((height - paddingBottom) - measuredHeight) - i5) - paddingTop;
            int i8 = eVar.bottomMargin;
            if (i7 < i8) {
                i5 = Math.max(0, i5 - (i8 - i7));
            }
        }
        return paddingTop + i5;
    }

    public final void a(List<View> list, int i2) {
        boolean z2 = ViewCompat.k(this) == 1;
        int childCount = getChildCount();
        int absoluteGravity = Gravity.getAbsoluteGravity(i2, getLayoutDirection());
        list.clear();
        if (z2) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                e eVar = (e) childAt.getLayoutParams();
                if (eVar.b == 0 && d(childAt) && a(eVar.a) == absoluteGravity) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            e eVar2 = (e) childAt2.getLayoutParams();
            if (eVar2.b == 0 && d(childAt2) && a(eVar2.a) == absoluteGravity) {
                list.add(childAt2);
            }
        }
    }

    public final int a(int i2) {
        int k2 = ViewCompat.k(this);
        int absoluteGravity = Gravity.getAbsoluteGravity(i2, k2) & 7;
        if (absoluteGravity == 1 || absoluteGravity == 3 || absoluteGravity == 5) {
            return absoluteGravity;
        }
        return k2 == 1 ? 5 : 3;
    }

    public final int a(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.getMarginStart() + marginLayoutParams.getMarginEnd();
    }
}
