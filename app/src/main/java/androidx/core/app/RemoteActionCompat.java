package androidx.core.app;

import android.app.PendingIntent;
import androidx.core.graphics.drawable.IconCompat;
import i.y.VersionedParcelable;

public final class RemoteActionCompat implements VersionedParcelable {
    public IconCompat a;
    public CharSequence b;
    public CharSequence c;
    public PendingIntent d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f199e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f200f;
}
