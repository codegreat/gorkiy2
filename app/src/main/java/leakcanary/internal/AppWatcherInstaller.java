package leakcanary.internal;

import android.app.Activity;
import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import f.ObjectWatcher;
import f.a.ActivityDestroyWatcher0;
import f.a.AndroidOFragmentDestroyWatcher;
import f.a.DefaultCanaryLog;
import f.a.FragmentDestroyWatcher;
import f.a.FragmentDestroyWatcher0;
import f.a.InternalAppWatcher;
import f.a.InternalAppWatcher1;
import j.a.a.a.outline;
import java.util.ArrayList;
import kotlin.TypeCastException;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import s.SharkLog;

/* compiled from: AppWatcherInstaller.kt */
public abstract class AppWatcherInstaller extends ContentProvider {

    /* compiled from: AppWatcherInstaller.kt */
    public static final class MainProcess extends AppWatcherInstaller {
    }

    public int delete(Uri uri, String str, String[] strArr) {
        if (uri != null) {
            return 0;
        }
        Intrinsics.a("uri");
        throw null;
    }

    public String getType(Uri uri) {
        if (uri != null) {
            return null;
        }
        Intrinsics.a("uri");
        throw null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        if (uri != null) {
            return null;
        }
        Intrinsics.a("uri");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.os.Looper, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public boolean onCreate() {
        Context context = getContext();
        if (context != null) {
            Intrinsics.a((Object) context, "context!!");
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                Application application = (Application) applicationContext;
                if (InternalAppWatcher.f732i != null) {
                    SharkLog.a = new DefaultCanaryLog();
                    SharkLog.a aVar = SharkLog.a;
                    if (aVar != null) {
                        aVar.a("Installing AppWatcher");
                    }
                    Looper mainLooper = Looper.getMainLooper();
                    Intrinsics.a((Object) mainLooper, "Looper.getMainLooper()");
                    if (mainLooper.getThread() != Thread.currentThread()) {
                        StringBuilder a = outline.a("Should be called from the main thread, not ");
                        a.append(Thread.currentThread());
                        throw new UnsupportedOperationException(a.toString());
                    } else if (InternalAppWatcher.d != null) {
                        return true;
                    } else {
                        InternalAppWatcher.d = application;
                        InternalAppWatcher1 internalAppWatcher1 = InternalAppWatcher1.c;
                        ObjectWatcher objectWatcher = InternalAppWatcher.h;
                        if (objectWatcher != null) {
                            application.registerActivityLifecycleCallbacks(new ActivityDestroyWatcher0(objectWatcher, internalAppWatcher1, null).a);
                            ObjectWatcher objectWatcher2 = InternalAppWatcher.h;
                            if (objectWatcher2 != null) {
                                ArrayList arrayList = new ArrayList();
                                if (Build.VERSION.SDK_INT >= 26) {
                                    arrayList.add(new AndroidOFragmentDestroyWatcher(objectWatcher2, internalAppWatcher1));
                                }
                                Functions0<Activity, g> a2 = FragmentDestroyWatcher0.a("androidx.fragment.app.Fragment", "leakcanary.internal.AndroidXFragmentDestroyWatcher", objectWatcher2, internalAppWatcher1);
                                if (a2 != null) {
                                    arrayList.add(a2);
                                }
                                Functions0<Activity, g> a3 = FragmentDestroyWatcher0.a("androidx.fragment.app.Fragment", "leakcanary.internal.AndroidSupportFragmentDestroyWatcher", objectWatcher2, internalAppWatcher1);
                                if (a3 != null) {
                                    arrayList.add(a3);
                                }
                                if (arrayList.size() != 0) {
                                    application.registerActivityLifecycleCallbacks(new FragmentDestroyWatcher(arrayList));
                                }
                                InternalAppWatcher.b.a(application);
                                return true;
                            }
                            Intrinsics.a("objectWatcher");
                            throw null;
                        }
                        Intrinsics.a("objectWatcher");
                        throw null;
                    }
                } else {
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.app.Application");
            }
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        if (uri != null) {
            return null;
        }
        Intrinsics.a("uri");
        throw null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        if (uri != null) {
            return 0;
        }
        Intrinsics.a("uri");
        throw null;
    }
}
