package l.a.a.a.o.d;

import java.io.File;
import java.util.List;

public interface FilesSender {
    boolean send(List<File> list);
}
