package l.a.a.a.o.e;

import android.util.Log;
import com.crashlytics.android.core.CrashlyticsPinningInfoProvider;
import io.fabric.sdk.android.services.network.HttpRequest;
import j.c.a.a.c.n.c;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import l.a.a.a.DefaultLogger;

public class DefaultHttpRequestFactory implements HttpRequestFactory {
    public final DefaultLogger a;
    public CrashlyticsPinningInfoProvider b;
    public SSLSocketFactory c;
    public boolean d;

    public DefaultHttpRequestFactory() {
        this.a = new DefaultLogger();
    }

    public HttpRequest a(b bVar, String str, Map<String, String> map) {
        HttpRequest httpRequest;
        SSLSocketFactory a2;
        HttpRequest httpRequest2;
        int ordinal = bVar.ordinal();
        boolean z = true;
        if (ordinal == 0) {
            httpRequest2 = new HttpRequest(HttpRequest.b(HttpRequest.a(str, map)), "GET");
        } else if (ordinal != 1) {
            if (ordinal == 2) {
                httpRequest = new HttpRequest(str, "PUT");
            } else if (ordinal == 3) {
                httpRequest = new HttpRequest(str, "DELETE");
            } else {
                throw new IllegalArgumentException("Unsupported HTTP method!");
            }
            if (str == null || !str.toLowerCase(Locale.US).startsWith("https")) {
                z = false;
            }
            if (!(!z || this.b == null || (a2 = a()) == null)) {
                ((HttpsURLConnection) httpRequest.d()).setSSLSocketFactory(a2);
            }
            return httpRequest;
        } else {
            httpRequest2 = new HttpRequest(HttpRequest.b(HttpRequest.a(str, map)), "POST");
        }
        httpRequest = httpRequest2;
        z = false;
        ((HttpsURLConnection) httpRequest.d()).setSSLSocketFactory(a2);
        return httpRequest;
    }

    public final synchronized SSLSocketFactory b() {
        SSLSocketFactory a2;
        this.d = true;
        try {
            a2 = c.a(this.b);
            if (this.a.a("Fabric", 3)) {
                Log.d("Fabric", "Custom SSL pinning enabled", null);
            }
        } catch (Exception e2) {
            if (this.a.a("Fabric", 6)) {
                Log.e("Fabric", "Exception while validating pinned certs", e2);
            }
            return null;
        }
        return a2;
    }

    public final synchronized void c() {
        this.d = false;
        this.c = null;
    }

    public DefaultHttpRequestFactory(DefaultLogger defaultLogger) {
        this.a = defaultLogger;
    }

    public final synchronized SSLSocketFactory a() {
        if (this.c == null && !this.d) {
            this.c = b();
        }
        return this.c;
    }
}
