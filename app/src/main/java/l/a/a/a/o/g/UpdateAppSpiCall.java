package l.a.a.a.o.g;

import l.a.a.a.Kit;
import l.a.a.a.o.e.HttpMethod;
import l.a.a.a.o.e.HttpRequestFactory;

public class UpdateAppSpiCall extends AbstractAppSpiCall {
    public UpdateAppSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.PUT);
    }
}
