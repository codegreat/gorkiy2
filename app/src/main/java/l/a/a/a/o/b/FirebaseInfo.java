package l.a.a.a.o.b;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import l.a.a.a.Fabric;

public class FirebaseInfo {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.a.a.a.o.b.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      l.a.a.a.o.b.CommonUtils.a(android.content.Context, java.lang.String, java.lang.String):int
      l.a.a.a.o.b.CommonUtils.a(java.lang.String, java.lang.String, int):long
      l.a.a.a.o.b.CommonUtils.a(android.content.Context, int, java.lang.String):void
      l.a.a.a.o.b.CommonUtils.a(java.io.InputStream, java.io.OutputStream, byte[]):void
      l.a.a.a.o.b.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean */
    public boolean a(Context context) {
        boolean z;
        boolean z2;
        if (CommonUtils.a(context, "com.crashlytics.useFirebaseAppId", false)) {
            return true;
        }
        int a = CommonUtils.a(context, "google_app_id", "string");
        if (a == 0) {
            z = false;
        } else {
            z = !TextUtils.isEmpty(context.getResources().getString(a));
        }
        if (!z) {
            return false;
        }
        if (!TextUtils.isEmpty(new ApiKey().a(context))) {
            z2 = true;
        } else {
            int a2 = CommonUtils.a(context, "io.fabric.ApiKey", "string");
            String str = null;
            if (a2 == 0) {
                if (Fabric.a().a("Fabric", 3)) {
                    Log.d("Fabric", "Falling back to Crashlytics key lookup from Strings", null);
                }
                a2 = CommonUtils.a(context, "com.crashlytics.ApiKey", "string");
            }
            if (a2 != 0) {
                str = context.getResources().getString(a2);
            }
            z2 = !TextUtils.isEmpty(str);
        }
        if (!z2) {
            return true;
        }
        return false;
    }
}
