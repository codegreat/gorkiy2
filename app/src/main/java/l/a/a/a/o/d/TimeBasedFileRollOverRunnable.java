package l.a.a.a.o.d;

import android.content.Context;
import l.a.a.a.o.b.CommonUtils;

public class TimeBasedFileRollOverRunnable implements Runnable {
    public final Context b;
    public final FileRollOverManager c;

    public TimeBasedFileRollOverRunnable(Context context, FileRollOverManager fileRollOverManager) {
        this.b = context;
        this.c = fileRollOverManager;
    }

    public void run() {
        try {
            CommonUtils.b(this.b, "Performing time based file roll over.");
            if (!this.c.rollFileOver()) {
                this.c.cancelTimeBasedFileRollOver();
            }
        } catch (Exception unused) {
            CommonUtils.c(this.b, "Failed to roll over file");
        }
    }
}
