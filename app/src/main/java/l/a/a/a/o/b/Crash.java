package l.a.a.a.o.b;

public abstract class Crash {
    public final String a;
    public final String b;

    public static class a extends Crash {
        public a(String str, String str2) {
            super(str, str2);
        }
    }

    public static class b extends Crash {
        public b(String str, String str2) {
            super(str, str2);
        }
    }

    public Crash(String str, String str2) {
        this.a = str;
        this.b = str2;
    }
}
