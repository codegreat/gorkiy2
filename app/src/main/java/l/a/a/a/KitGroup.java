package l.a.a.a;

import java.util.Collection;

public interface KitGroup {
    Collection<? extends k> getKits();
}
