package l.a.a.a.o.c;

public interface PriorityProvider<T> extends Comparable<T> {
    Priority getPriority();
}
