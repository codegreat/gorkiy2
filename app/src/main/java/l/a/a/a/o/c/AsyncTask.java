package l.a.a.a.o.c;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import io.fabric.sdk.android.InitializationException;
import j.a.a.a.outline;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import l.a.a.a.InitializationTask;
import l.a.a.a.o.b.TimingMetric;

public abstract class AsyncTask<Params, Progress, Result> {
    public static final int g;
    public static final int h;

    /* renamed from: i  reason: collision with root package name */
    public static final int f2643i;

    /* renamed from: j  reason: collision with root package name */
    public static final ThreadFactory f2644j = new a();

    /* renamed from: k  reason: collision with root package name */
    public static final BlockingQueue<Runnable> f2645k = new LinkedBlockingQueue(128);

    /* renamed from: l  reason: collision with root package name */
    public static final Executor f2646l = new ThreadPoolExecutor(h, f2643i, 1, TimeUnit.SECONDS, f2645k, f2644j);

    /* renamed from: m  reason: collision with root package name */
    public static final Executor f2647m = new f(null);

    /* renamed from: n  reason: collision with root package name */
    public static final e f2648n = new e();
    public final h<Params, Result> b = new b();
    public final FutureTask<Result> c = new c(this.b);
    public volatile g d = g.PENDING;

    /* renamed from: e  reason: collision with root package name */
    public final AtomicBoolean f2649e = new AtomicBoolean();

    /* renamed from: f  reason: collision with root package name */
    public final AtomicBoolean f2650f = new AtomicBoolean();

    public static class a implements ThreadFactory {
        public final AtomicInteger b = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            StringBuilder a = outline.a("AsyncTask #");
            a.append(this.b.getAndIncrement());
            return new Thread(runnable, a.toString());
        }
    }

    public class b extends h<Params, Result> {
        public b() {
            super(null);
        }

        public Result call() {
            AsyncTask.this.f2650f.set(true);
            Process.setThreadPriority(10);
            AsyncTask asyncTask = AsyncTask.this;
            Params[] paramsArr = super.b;
            InitializationTask initializationTask = (InitializationTask) asyncTask;
            Result result = null;
            if (initializationTask != null) {
                Void[] voidArr = (Void[]) paramsArr;
                TimingMetric a = initializationTask.a("doInBackground");
                if (!initializationTask.f2649e.get()) {
                    result = initializationTask.f2622p.doInBackground();
                }
                a.b();
                asyncTask.a(result);
                return result;
            }
            throw null;
        }
    }

    public class c extends FutureTask<Result> {
        public c(Callable callable) {
            super(callable);
        }

        public void done() {
            try {
                AsyncTask asyncTask = AsyncTask.this;
                Object obj = get();
                if (!asyncTask.f2650f.get()) {
                    asyncTask.a(obj);
                }
            } catch (InterruptedException e2) {
                Log.w("AsyncTask", e2);
            } catch (ExecutionException e3) {
                throw new RuntimeException("An error occured while executing doInBackground()", e3.getCause());
            } catch (CancellationException unused) {
                AsyncTask asyncTask2 = AsyncTask.this;
                if (!asyncTask2.f2650f.get()) {
                    asyncTask2.a((Object) null);
                }
            }
        }
    }

    public static class d<Data> {
        public final AsyncTask a;
        public final Data[] b;

        public d(a aVar, Data... dataArr) {
            this.a = aVar;
            this.b = dataArr;
        }
    }

    public static class e extends Handler {
        public e() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            d dVar = (d) message.obj;
            int i2 = message.what;
            if (i2 == 1) {
                AsyncTask asyncTask = dVar.a;
                Data data = dVar.b[0];
                if (asyncTask.f2649e.get()) {
                    InitializationTask initializationTask = (InitializationTask) asyncTask;
                    initializationTask.f2622p.onCancelled(data);
                    initializationTask.f2622p.initializationCallback.a(new InitializationException(initializationTask.f2622p.getIdentifier() + " Initialization was cancelled"));
                } else {
                    InitializationTask initializationTask2 = (InitializationTask) asyncTask;
                    initializationTask2.f2622p.onPostExecute(data);
                    initializationTask2.f2622p.initializationCallback.a((l.a.a.a.f) data);
                }
                asyncTask.d = g.FINISHED;
            } else if (i2 == 2 && dVar.a == null) {
                throw null;
            }
        }
    }

    public static class f implements Executor {
        public final LinkedList<Runnable> a = new LinkedList<>();
        public Runnable b;

        public class a implements Runnable {
            public final /* synthetic */ Runnable b;

            public a(Runnable runnable) {
                this.b = runnable;
            }

            public void run() {
                try {
                    this.b.run();
                } finally {
                    f.this.a();
                }
            }
        }

        public /* synthetic */ f(a aVar) {
        }

        public synchronized void a() {
            Runnable poll = this.a.poll();
            this.b = poll;
            if (poll != null) {
                AsyncTask.f2646l.execute(poll);
            }
        }

        public synchronized void execute(Runnable runnable) {
            this.a.offer(new a(runnable));
            if (this.b == null) {
                a();
            }
        }
    }

    public enum g {
        PENDING,
        RUNNING,
        FINISHED
    }

    public static abstract class h<Params, Result> implements Callable<Result> {
        public Params[] b;

        public /* synthetic */ h(a aVar) {
        }
    }

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        g = availableProcessors;
        h = availableProcessors + 1;
        f2643i = (availableProcessors * 2) + 1;
    }

    public final Result a(Object obj) {
        f2648n.obtainMessage(1, new d(this, obj)).sendToTarget();
        return obj;
    }

    public final boolean a(boolean z) {
        this.f2649e.set(true);
        return this.c.cancel(z);
    }
}
