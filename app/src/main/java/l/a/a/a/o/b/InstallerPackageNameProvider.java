package l.a.a.a.o.b;

import android.content.Context;
import l.a.a.a.o.a.MemoryValueCache;
import l.a.a.a.o.a.ValueLoader;

public class InstallerPackageNameProvider {
    public final ValueLoader<String> a = new a(this);
    public final MemoryValueCache<String> b = new MemoryValueCache<>();

    public class a implements ValueLoader<String> {
        public a(InstallerPackageNameProvider installerPackageNameProvider) {
        }

        public Object load(Context context) {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    }
}
