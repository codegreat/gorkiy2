package l.a.a.a.o.g;

import java.util.Collection;
import l.a.a.a.m;

public class AppRequestData {
    public final String a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2660e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2661f;
    public final int g;
    public final String h;

    /* renamed from: i  reason: collision with root package name */
    public final String f2662i;

    /* renamed from: j  reason: collision with root package name */
    public final IconRequest f2663j;

    /* renamed from: k  reason: collision with root package name */
    public final Collection<m> f2664k;

    public AppRequestData(String str, String str2, String str3, String str4, String str5, String str6, int i2, String str7, String str8, m mVar, Collection<m> collection) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.f2660e = str5;
        this.f2661f = str6;
        this.g = i2;
        this.h = str7;
        this.f2662i = str8;
        this.f2663j = mVar;
        this.f2664k = collection;
    }
}
