package l.a.a.a.o.b;

import android.os.Process;

public abstract class BackgroundPriorityRunnable implements Runnable {
    public abstract void onRun();

    public final void run() {
        Process.setThreadPriority(10);
        onRun();
    }
}
