package l.b.u.e.d;

import j.c.a.a.c.n.c;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;

public final class SingleMap<T, R> extends Single<R> {
    public final SingleSource<? extends T> a;
    public final Function<? super T, ? extends R> b;

    public static final class a<T, R> implements SingleObserver<T> {
        public final SingleObserver<? super R> b;
        public final Function<? super T, ? extends R> c;

        public a(SingleObserver<? super R> singleObserver, Function<? super T, ? extends R> function) {
            this.b = singleObserver;
            this.c = function;
        }

        public void a(Disposable disposable) {
            this.b.a(disposable);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends R, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void a(T t2) {
            try {
                Object a = this.c.a(t2);
                ObjectHelper.a((Object) a, "The mapper function returned a null value.");
                this.b.a(a);
            } catch (Throwable th) {
                c.c(th);
                a(th);
            }
        }

        public void a(Throwable th) {
            this.b.a(th);
        }
    }

    public SingleMap(SingleSource<? extends T> singleSource, Function<? super T, ? extends R> function) {
        this.a = singleSource;
        this.b = function;
    }

    public void b(SingleObserver<? super R> singleObserver) {
        this.a.a(new a(singleObserver, this.b));
    }
}
