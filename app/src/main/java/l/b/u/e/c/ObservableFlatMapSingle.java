package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;
import l.b.u.f.SpscLinkedArrayQueue;
import l.b.u.h.AtomicThrowable;
import l.b.u.h.ExceptionHelper;

public final class ObservableFlatMapSingle<T, R> extends AbstractObservableWithUpstream<T, R> {
    public final Function<? super T, ? extends SingleSource<? extends R>> c;
    public final boolean d;

    public ObservableFlatMapSingle(ObservableSource<T> observableSource, Function<? super T, ? extends SingleSource<? extends R>> function, boolean z) {
        super(observableSource);
        this.c = function;
        this.d = z;
    }

    public void b(Observer<? super R> observer) {
        super.b.a(new a(observer, this.c, this.d));
    }

    public static final class a<T, R> extends AtomicInteger implements Observer<T>, b {
        public final Observer<? super R> b;
        public final boolean c;
        public final CompositeDisposable d = new CompositeDisposable();

        /* renamed from: e  reason: collision with root package name */
        public final AtomicInteger f2729e = new AtomicInteger(1);

        /* renamed from: f  reason: collision with root package name */
        public final AtomicThrowable f2730f = new AtomicThrowable();
        public final Function<? super T, ? extends SingleSource<? extends R>> g;
        public final AtomicReference<SpscLinkedArrayQueue<R>> h = new AtomicReference<>();

        /* renamed from: i  reason: collision with root package name */
        public Disposable f2731i;

        /* renamed from: j  reason: collision with root package name */
        public volatile boolean f2732j;

        /* renamed from: l.b.u.e.c.ObservableFlatMapSingle$a$a  reason: collision with other inner class name */
        public final class C0037a extends AtomicReference<b> implements SingleObserver<R>, b {
            public C0037a() {
            }

            public void a(Disposable disposable) {
                DisposableHelper.b(super, disposable);
            }

            public void f() {
                DisposableHelper.a(super);
            }

            public boolean g() {
                return DisposableHelper.a((Disposable) get());
            }

            public void a(R r2) {
                a.this.a(this, r2);
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapSingle$a$a] */
            public void a(Throwable th) {
                a aVar = a.this;
                aVar.d.a((Disposable) this);
                AtomicThrowable atomicThrowable = aVar.f2730f;
                if (atomicThrowable == null) {
                    throw null;
                } else if (ExceptionHelper.a(super, th)) {
                    if (!aVar.c) {
                        aVar.f2731i.f();
                        aVar.d.f();
                    }
                    aVar.f2729e.decrementAndGet();
                    aVar.b();
                } else {
                    c.b(th);
                }
            }
        }

        public a(Observer<? super R> observer, Function<? super T, ? extends SingleSource<? extends R>> function, boolean z) {
            this.b = observer;
            this.g = function;
            this.c = z;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapSingle$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.f2731i, disposable)) {
                this.f2731i = disposable;
                this.b.a((Disposable) this);
            }
        }

        /* JADX WARN: Type inference failed for: r0v4, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapSingle$a$a, l.b.SingleObserver] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.SingleSource<? extends R>, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void b(T t2) {
            try {
                Object a = this.g.a(t2);
                ObjectHelper.a((Object) a, "The mapper returned a null SingleSource");
                SingleSource singleSource = (SingleSource) a;
                this.f2729e.getAndIncrement();
                ? aVar = new C0037a();
                if (!this.f2732j && this.d.c(aVar)) {
                    singleSource.a(aVar);
                }
            } catch (Throwable th) {
                c.c(th);
                this.f2731i.f();
                a(th);
            }
        }

        public void c() {
            Observer<? super R> observer = this.b;
            AtomicInteger atomicInteger = this.f2729e;
            AtomicReference<SpscLinkedArrayQueue<R>> atomicReference = this.h;
            int i2 = 1;
            while (!this.f2732j) {
                if (this.c || ((Throwable) this.f2730f.get()) == null) {
                    boolean z = false;
                    boolean z2 = super.get() == 0;
                    SpscLinkedArrayQueue spscLinkedArrayQueue = atomicReference.get();
                    Object poll = spscLinkedArrayQueue != null ? spscLinkedArrayQueue.poll() : null;
                    if (poll == null) {
                        z = true;
                    }
                    if (z2 && z) {
                        AtomicThrowable atomicThrowable = this.f2730f;
                        if (atomicThrowable != null) {
                            Throwable a = ExceptionHelper.a(atomicThrowable);
                            if (a != null) {
                                observer.a(a);
                                return;
                            } else {
                                observer.a();
                                return;
                            }
                        } else {
                            throw null;
                        }
                    } else if (z) {
                        i2 = addAndGet(-i2);
                        if (i2 == 0) {
                            return;
                        }
                    } else {
                        observer.b(poll);
                    }
                } else {
                    AtomicThrowable atomicThrowable2 = this.f2730f;
                    if (atomicThrowable2 != null) {
                        Throwable a2 = ExceptionHelper.a(atomicThrowable2);
                        SpscLinkedArrayQueue spscLinkedArrayQueue2 = this.h.get();
                        if (spscLinkedArrayQueue2 != null) {
                            spscLinkedArrayQueue2.clear();
                        }
                        observer.a(a2);
                        return;
                    }
                    throw null;
                }
            }
            SpscLinkedArrayQueue spscLinkedArrayQueue3 = this.h.get();
            if (spscLinkedArrayQueue3 != null) {
                spscLinkedArrayQueue3.clear();
            }
        }

        public void f() {
            this.f2732j = true;
            this.f2731i.f();
            this.d.f();
        }

        public boolean g() {
            return this.f2732j;
        }

        public void a(Throwable th) {
            this.f2729e.decrementAndGet();
            AtomicThrowable atomicThrowable = this.f2730f;
            if (atomicThrowable == null) {
                throw null;
            } else if (ExceptionHelper.a(atomicThrowable, th)) {
                if (!this.c) {
                    this.d.f();
                }
                b();
            } else {
                c.b(th);
            }
        }

        public void b() {
            if (getAndIncrement() == 0) {
                c();
            }
        }

        public void a() {
            this.f2729e.decrementAndGet();
            b();
        }

        /* JADX WARN: Type inference failed for: r4v0, types: [l.b.u.e.c.ObservableFlatMapSingle$a<T, R>$a, l.b.s.Disposable] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(l.b.u.e.c.ObservableFlatMapSingle.a<T, R>.defpackage.a r4, R r5) {
            /*
                r3 = this;
                l.b.s.CompositeDisposable r0 = r3.d
                r0.a(r4)
                int r4 = r3.get()
                r0 = 0
                if (r4 != 0) goto L_0x0053
                r4 = 1
                r1 = 0
                boolean r2 = r3.compareAndSet(r1, r4)
                if (r2 == 0) goto L_0x0053
                l.b.Observer<? super R> r2 = r3.b
                r2.b(r5)
                java.util.concurrent.atomic.AtomicInteger r5 = r3.f2729e
                int r5 = r5.decrementAndGet()
                if (r5 != 0) goto L_0x0022
                goto L_0x0023
            L_0x0022:
                r4 = 0
            L_0x0023:
                java.util.concurrent.atomic.AtomicReference<l.b.u.f.SpscLinkedArrayQueue<R>> r5 = r3.h
                java.lang.Object r5 = r5.get()
                l.b.u.f.SpscLinkedArrayQueue r5 = (l.b.u.f.SpscLinkedArrayQueue) r5
                if (r4 == 0) goto L_0x004c
                if (r5 == 0) goto L_0x0035
                boolean r4 = r5.isEmpty()
                if (r4 == 0) goto L_0x004c
            L_0x0035:
                l.b.u.h.AtomicThrowable r4 = r3.f2730f
                if (r4 == 0) goto L_0x004b
                java.lang.Throwable r4 = l.b.u.h.ExceptionHelper.a(r4)
                if (r4 == 0) goto L_0x0045
                l.b.Observer<? super R> r5 = r3.b
                r5.a(r4)
                goto L_0x004a
            L_0x0045:
                l.b.Observer<? super R> r4 = r3.b
                r4.a()
            L_0x004a:
                return
            L_0x004b:
                throw r0
            L_0x004c:
                int r4 = r3.decrementAndGet()
                if (r4 != 0) goto L_0x007e
                return
            L_0x0053:
                java.util.concurrent.atomic.AtomicReference<l.b.u.f.SpscLinkedArrayQueue<R>> r4 = r3.h
                java.lang.Object r4 = r4.get()
                l.b.u.f.SpscLinkedArrayQueue r4 = (l.b.u.f.SpscLinkedArrayQueue) r4
                if (r4 == 0) goto L_0x005e
                goto L_0x006d
            L_0x005e:
                l.b.u.f.SpscLinkedArrayQueue r4 = new l.b.u.f.SpscLinkedArrayQueue
                int r1 = l.b.Flowable.a
                r4.<init>(r1)
                java.util.concurrent.atomic.AtomicReference<l.b.u.f.SpscLinkedArrayQueue<R>> r1 = r3.h
                boolean r1 = r1.compareAndSet(r0, r4)
                if (r1 == 0) goto L_0x0053
            L_0x006d:
                monitor-enter(r4)
                r4.offer(r5)     // Catch:{ all -> 0x0082 }
                monitor-exit(r4)     // Catch:{ all -> 0x0082 }
                java.util.concurrent.atomic.AtomicInteger r4 = r3.f2729e
                r4.decrementAndGet()
                int r4 = r3.getAndIncrement()
                if (r4 == 0) goto L_0x007e
                return
            L_0x007e:
                r3.c()
                return
            L_0x0082:
                r5 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0082 }
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFlatMapSingle.a.a(l.b.u.e.c.ObservableFlatMapSingle$a$a, java.lang.Object):void");
        }
    }
}
