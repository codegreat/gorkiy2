package l.b.u.e.d;

import l.b.Observable;
import l.b.Observer;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.Disposable;
import l.b.u.a.DisposableHelper;
import l.b.u.d.DeferredScalarDisposable;

public final class SingleToObservable<T> extends Observable<T> {
    public final SingleSource<? extends T> b;

    public SingleToObservable(SingleSource<? extends T> singleSource) {
        this.b = singleSource;
    }

    public void b(Observer<? super T> observer) {
        this.b.a(new a(observer));
    }

    public static final class a<T> extends DeferredScalarDisposable<T> implements SingleObserver<T> {
        public Disposable d;

        public a(Observer<? super T> observer) {
            super(observer);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.e.d.SingleToObservable$a, l.b.s.Disposable, l.b.u.d.DeferredScalarDisposable] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                super.b.a((Disposable) this);
            }
        }

        public void f() {
            super.f();
            this.d.f();
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void a(T r4) {
            /*
                r3 = this;
                int r0 = r3.get()
                r1 = r0 & 54
                if (r1 == 0) goto L_0x0009
                goto L_0x002c
            L_0x0009:
                l.b.Observer<? super T> r1 = r3.b
                r2 = 8
                if (r0 != r2) goto L_0x001b
                r3.c = r4
                r4 = 16
                r3.lazySet(r4)
                r4 = 0
                r1.b(r4)
                goto L_0x0022
            L_0x001b:
                r0 = 2
                r3.lazySet(r0)
                r1.b(r4)
            L_0x0022:
                int r4 = r3.get()
                r0 = 4
                if (r4 == r0) goto L_0x002c
                r1.a()
            L_0x002c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.d.SingleToObservable.a.a(java.lang.Object):void");
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void a(java.lang.Throwable r2) {
            /*
                r1 = this;
                int r0 = r1.get()
                r0 = r0 & 54
                if (r0 == 0) goto L_0x000c
                j.c.a.a.c.n.c.b(r2)
                goto L_0x0015
            L_0x000c:
                r0 = 2
                r1.lazySet(r0)
                l.b.Observer<? super T> r0 = r1.b
                r0.a(r2)
            L_0x0015:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.d.SingleToObservable.a.a(java.lang.Throwable):void");
        }
    }
}
