package l.b.u.e.c;

import l.b.Observable;
import l.b.ObservableSource;

public abstract class AbstractObservableWithUpstream<T, U> extends Observable<U> {
    public final ObservableSource<T> b;

    public AbstractObservableWithUpstream(ObservableSource<T> observableSource) {
        this.b = observableSource;
    }
}
