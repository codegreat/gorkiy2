package l.b.u.e.c;

import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;

public final class ObservableFromUnsafeSource<T> extends Observable<T> {
    public final ObservableSource<T> b;

    public ObservableFromUnsafeSource(ObservableSource<T> observableSource) {
        this.b = observableSource;
    }

    public void b(Observer<? super T> observer) {
        this.b.a(observer);
    }
}
