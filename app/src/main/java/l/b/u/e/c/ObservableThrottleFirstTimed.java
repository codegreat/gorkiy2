package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.Scheduler;
import l.b.l;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;
import l.b.v.SerializedObserver;

public final class ObservableThrottleFirstTimed<T> extends AbstractObservableWithUpstream<T, T> {
    public final long c;
    public final TimeUnit d;

    /* renamed from: e  reason: collision with root package name */
    public final Scheduler f2747e;

    public ObservableThrottleFirstTimed(ObservableSource<T> observableSource, long j2, TimeUnit timeUnit, l lVar) {
        super(observableSource);
        this.c = j2;
        this.d = timeUnit;
        this.f2747e = lVar;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(new SerializedObserver(observer), this.c, this.d, this.f2747e.a()));
    }

    public static final class a<T> extends AtomicReference<b> implements Observer<T>, b, Runnable {
        public final Observer<? super T> b;
        public final long c;
        public final TimeUnit d;

        /* renamed from: e  reason: collision with root package name */
        public final Scheduler.b f2748e;

        /* renamed from: f  reason: collision with root package name */
        public Disposable f2749f;
        public volatile boolean g;
        public boolean h;

        public a(Observer<? super T> observer, long j2, TimeUnit timeUnit, l.b bVar) {
            this.b = observer;
            this.c = j2;
            this.d = timeUnit;
            this.f2748e = bVar;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableThrottleFirstTimed$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.f2749f, disposable)) {
                this.f2749f = disposable;
                this.b.a((Disposable) this);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
         arg types: [l.b.u.e.c.ObservableThrottleFirstTimed$a, l.b.s.Disposable]
         candidates:
          l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
          l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
        public void b(T t2) {
            if (!this.g && !this.h) {
                this.g = true;
                this.b.b(t2);
                Disposable disposable = (Disposable) get();
                if (disposable != null) {
                    disposable.f();
                }
                DisposableHelper.a((AtomicReference<b>) super, (b) this.f2748e.a(this, this.c, this.d));
            }
        }

        public void f() {
            this.f2749f.f();
            this.f2748e.f();
        }

        public boolean g() {
            return this.f2748e.g();
        }

        public void run() {
            this.g = false;
        }

        public void a(Throwable th) {
            if (this.h) {
                c.b(th);
                return;
            }
            this.h = true;
            this.b.a(th);
            this.f2748e.f();
        }

        public void a() {
            if (!this.h) {
                this.h = true;
                this.b.a();
                this.f2748e.f();
            }
        }
    }
}
