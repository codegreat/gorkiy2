package l.b.u.e.c;

import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class ObservableHide<T> extends AbstractObservableWithUpstream<T, T> {
    public ObservableHide(ObservableSource<T> observableSource) {
        super(observableSource);
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer));
    }

    public static final class a<T> implements Observer<T>, b {
        public final Observer<? super T> b;
        public Disposable c;

        public a(Observer<? super T> observer) {
            this.b = observer;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableHide$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            this.b.b(t2);
        }

        public void f() {
            this.c.f();
        }

        public boolean g() {
            return this.c.g();
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a() {
            this.b.a();
        }
    }
}
