package l.b.u.a;

import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.c.QueueDisposable;

public enum EmptyDisposable implements QueueDisposable<Object> {
    INSTANCE,
    NEVER;

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    public static void a(Throwable th, Observer<?> observer) {
        observer.a((Disposable) INSTANCE);
        observer.a(th);
    }

    public int a(int i2) {
        return i2 & 2;
    }

    public void clear() {
    }

    public void f() {
    }

    public boolean g() {
        return this == INSTANCE;
    }

    public boolean isEmpty() {
        return true;
    }

    public boolean offer(Object obj) {
        throw new UnsupportedOperationException("Should not be called!");
    }

    public Object poll() {
        return null;
    }
}
