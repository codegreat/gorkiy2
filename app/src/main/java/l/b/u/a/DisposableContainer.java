package l.b.u.a;

import l.b.s.Disposable;

public interface DisposableContainer {
    boolean a(Disposable disposable);

    boolean b(Disposable disposable);

    boolean c(Disposable disposable);
}
