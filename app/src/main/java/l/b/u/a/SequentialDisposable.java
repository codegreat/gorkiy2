package l.b.u.a;

import java.util.concurrent.atomic.AtomicReference;
import l.b.s.Disposable;
import l.b.s.b;

public final class SequentialDisposable extends AtomicReference<b> implements b {
    public void f() {
        DisposableHelper.a(super);
    }

    public boolean g() {
        return DisposableHelper.a((Disposable) get());
    }
}
