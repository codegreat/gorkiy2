package l.b.u.g;

import j.c.a.a.c.n.c;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import l.b.Scheduler;
import l.b.s.Disposable;
import l.b.u.a.DisposableContainer;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;

public class NewThreadWorker extends Scheduler.b implements Disposable {
    public final ScheduledExecutorService b;
    public volatile boolean c;

    public NewThreadWorker(ThreadFactory threadFactory) {
        this.b = SchedulerPoolFactory.a(threadFactory);
    }

    public Disposable a(Runnable runnable) {
        return a(runnable, 0, null);
    }

    public void f() {
        if (!this.c) {
            this.c = true;
            this.b.shutdownNow();
        }
    }

    public boolean g() {
        return this.c;
    }

    /* JADX WARN: Type inference failed for: r7v1, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable] */
    /* JADX WARN: Type inference failed for: r7v2, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
        if (this.c) {
            return EmptyDisposable.INSTANCE;
        }
        return a(runnable, j2, timeUnit, null);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable, java.util.concurrent.Callable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public ScheduledRunnable a(Runnable runnable, long j2, TimeUnit timeUnit, DisposableContainer disposableContainer) {
        Future future;
        ObjectHelper.a((Object) runnable, "run is null");
        ? scheduledRunnable = new ScheduledRunnable(runnable, disposableContainer);
        if (disposableContainer != null && !disposableContainer.c(scheduledRunnable)) {
            return scheduledRunnable;
        }
        if (j2 <= 0) {
            try {
                future = this.b.submit((Callable) scheduledRunnable);
            } catch (RejectedExecutionException e2) {
                if (disposableContainer != null) {
                    disposableContainer.b(scheduledRunnable);
                }
                c.b((Throwable) e2);
            }
        } else {
            future = this.b.schedule((Callable) scheduledRunnable, j2, timeUnit);
        }
        scheduledRunnable.a(future);
        return scheduledRunnable;
    }
}
