package l.b.u.g;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import l.b.s.Disposable;
import l.b.u.b.Functions;

public final class ScheduledDirectTask extends f implements Callable<Void>, Disposable {
    public static final FutureTask<Void> d = new FutureTask<>(Functions.b, null);

    /* renamed from: e  reason: collision with root package name */
    public static final FutureTask<Void> f2767e = new FutureTask<>(Functions.b, null);
    public final Runnable b;
    public Thread c;

    public ScheduledDirectTask(Runnable runnable) {
        this.b = runnable;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [l.b.u.g.ScheduledDirectTask, java.util.concurrent.atomic.AtomicReference] */
    public final void a(Future future) {
        Future future2;
        do {
            future2 = (Future) get();
            if (future2 != d) {
                if (future2 == f2767e) {
                    future.cancel(this.c != Thread.currentThread());
                    return;
                }
            } else {
                return;
            }
        } while (!compareAndSet(future2, future));
    }

    public Object call() {
        this.c = Thread.currentThread();
        try {
            this.b.run();
            return null;
        } finally {
            lazySet(d);
            this.c = null;
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [l.b.u.g.ScheduledDirectTask, java.util.concurrent.atomic.AtomicReference] */
    public final void f() {
        FutureTask<Void> futureTask;
        Future future = (Future) get();
        if (future != d && future != (futureTask = f2767e) && compareAndSet(future, futureTask) && future != null) {
            future.cancel(this.c != Thread.currentThread());
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [l.b.u.g.ScheduledDirectTask, java.util.concurrent.atomic.AtomicReference] */
    public final boolean g() {
        Future future = (Future) get();
        return future == d || future == f2767e;
    }
}
