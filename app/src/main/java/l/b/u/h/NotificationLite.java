package l.b.u.h;

import j.a.a.a.outline;
import java.io.Serializable;
import l.b.s.Disposable;
import l.b.u.b.ObjectHelper;

public enum NotificationLite {
    COMPLETE;

    public static final class a implements Serializable {
        public final Disposable b;

        public String toString() {
            return "NotificationLite.Disposable[null]";
        }
    }

    public static final class b implements Serializable {
        public final Throwable b;

        public b(Throwable th) {
            this.b = th;
        }

        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return ObjectHelper.a(this.b, ((b) obj).b);
            }
            return false;
        }

        public int hashCode() {
            return this.b.hashCode();
        }

        public String toString() {
            StringBuilder a = outline.a("NotificationLite.Error[");
            a.append(this.b);
            a.append("]");
            return a.toString();
        }
    }

    public static <T> Object a(Object obj) {
        return obj;
    }

    public static Object a(Throwable th) {
        return new b(th);
    }

    public String toString() {
        return "NotificationLite.Complete";
    }
}
