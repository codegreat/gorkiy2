package l.b.u.h;

import j.c.a.a.c.n.c;

public final class OpenHashSet<T> {
    public final float a;
    public int b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public T[] f2772e;

    public OpenHashSet(int i2, float f2) {
        this.a = f2;
        int d2 = c.d(i2);
        this.b = d2 - 1;
        this.d = (int) (f2 * ((float) d2));
        this.f2772e = new Object[d2];
    }

    public static int a(int i2) {
        int i3 = i2 * -1640531527;
        return i3 ^ (i3 >>> 16);
    }

    public boolean a(Object obj) {
        Object obj2;
        Object[] objArr = this.f2772e;
        int i2 = this.b;
        int a2 = a(obj.hashCode()) & i2;
        Object obj3 = objArr[a2];
        if (obj3 != null) {
            if (obj3.equals(obj)) {
                return false;
            }
            do {
                a2 = (a2 + 1) & i2;
                obj2 = objArr[a2];
                if (obj2 == null) {
                }
            } while (!obj2.equals(obj));
            return false;
        }
        objArr[a2] = obj;
        int i3 = this.c + 1;
        this.c = i3;
        if (i3 >= this.d) {
            T[] tArr = this.f2772e;
            int length = tArr.length;
            int i4 = length << 1;
            int i5 = i4 - 1;
            T[] tArr2 = new Object[i4];
            while (true) {
                int i6 = i3 - 1;
                if (i3 == 0) {
                    break;
                }
                do {
                    length--;
                } while (tArr[length] == null);
                int a3 = a(tArr[length].hashCode()) & i5;
                if (tArr2[a3] != null) {
                    do {
                        a3 = (a3 + 1) & i5;
                    } while (tArr2[a3] != null);
                }
                tArr2[a3] = tArr[length];
                i3 = i6;
            }
            this.b = i5;
            this.d = (int) (((float) i4) * this.a);
            this.f2772e = tArr2;
        }
        return true;
    }

    public boolean a(int i2, T[] tArr, int i3) {
        int i4;
        T t2;
        this.c--;
        while (true) {
            int i5 = i2 + 1;
            while (true) {
                i4 = i5 & i3;
                t2 = tArr[i4];
                if (t2 == null) {
                    tArr[i2] = null;
                    return true;
                }
                int a2 = a(t2.hashCode()) & i3;
                if (i2 <= i4) {
                    if (i2 >= a2 || a2 > i4) {
                        break;
                    }
                    i5 = i4 + 1;
                } else {
                    if (i2 >= a2 && a2 > i4) {
                        break;
                    }
                    i5 = i4 + 1;
                }
            }
            tArr[i2] = t2;
            i2 = i4;
        }
    }
}
