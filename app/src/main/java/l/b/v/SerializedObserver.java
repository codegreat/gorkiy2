package l.b.v;

import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;
import l.b.u.h.AppendOnlyLinkedArrayList;
import l.b.u.h.NotificationLite;

public final class SerializedObserver<T> implements Observer<T>, b {
    public final Observer<? super T> b;
    public final boolean c = false;
    public Disposable d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f2773e;

    /* renamed from: f  reason: collision with root package name */
    public AppendOnlyLinkedArrayList<Object> f2774f;
    public volatile boolean g;

    public SerializedObserver(Observer<? super T> observer) {
        this.b = observer;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.v.SerializedObserver] */
    public void a(Disposable disposable) {
        if (DisposableHelper.a(this.d, disposable)) {
            this.d = disposable;
            this.b.a((Disposable) this);
        }
    }

    public void b(T t2) {
        if (!this.g) {
            if (t2 == null) {
                this.d.f();
                a(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
                return;
            }
            synchronized (this) {
                if (!this.g) {
                    if (this.f2773e) {
                        AppendOnlyLinkedArrayList<Object> appendOnlyLinkedArrayList = this.f2774f;
                        if (appendOnlyLinkedArrayList == null) {
                            appendOnlyLinkedArrayList = new AppendOnlyLinkedArrayList<>(4);
                            this.f2774f = appendOnlyLinkedArrayList;
                        }
                        NotificationLite.a((Object) t2);
                        appendOnlyLinkedArrayList.a(t2);
                        return;
                    }
                    this.f2773e = true;
                    this.b.b(t2);
                    b();
                }
            }
        }
    }

    public void f() {
        this.d.f();
    }

    public boolean g() {
        return this.d.g();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        if (r1 == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        j.c.a.a.c.n.c.b(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0040, code lost:
        r3.b.a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0045, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Throwable r4) {
        /*
            r3 = this;
            boolean r0 = r3.g
            if (r0 == 0) goto L_0x0008
            j.c.a.a.c.n.c.b(r4)
            return
        L_0x0008:
            monitor-enter(r3)
            boolean r0 = r3.g     // Catch:{ all -> 0x0046 }
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0011
            r1 = 1
            goto L_0x0039
        L_0x0011:
            boolean r0 = r3.f2773e     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0035
            r3.g = r2     // Catch:{ all -> 0x0046 }
            l.b.u.h.AppendOnlyLinkedArrayList<java.lang.Object> r0 = r3.f2774f     // Catch:{ all -> 0x0046 }
            if (r0 != 0) goto L_0x0023
            l.b.u.h.AppendOnlyLinkedArrayList r0 = new l.b.u.h.AppendOnlyLinkedArrayList     // Catch:{ all -> 0x0046 }
            r2 = 4
            r0.<init>(r2)     // Catch:{ all -> 0x0046 }
            r3.f2774f = r0     // Catch:{ all -> 0x0046 }
        L_0x0023:
            java.lang.Object r4 = l.b.u.h.NotificationLite.a(r4)     // Catch:{ all -> 0x0046 }
            boolean r2 = r3.c     // Catch:{ all -> 0x0046 }
            if (r2 == 0) goto L_0x002f
            r0.a(r4)     // Catch:{ all -> 0x0046 }
            goto L_0x0033
        L_0x002f:
            java.lang.Object[] r0 = r0.b     // Catch:{ all -> 0x0046 }
            r0[r1] = r4     // Catch:{ all -> 0x0046 }
        L_0x0033:
            monitor-exit(r3)     // Catch:{ all -> 0x0046 }
            return
        L_0x0035:
            r3.g = r2     // Catch:{ all -> 0x0046 }
            r3.f2773e = r2     // Catch:{ all -> 0x0046 }
        L_0x0039:
            monitor-exit(r3)     // Catch:{ all -> 0x0046 }
            if (r1 == 0) goto L_0x0040
            j.c.a.a.c.n.c.b(r4)
            return
        L_0x0040:
            l.b.Observer<? super T> r0 = r3.b
            r0.a(r4)
            return
        L_0x0046:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0046 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.v.SerializedObserver.a(java.lang.Throwable):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        if (r3 == null) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0018, code lost:
        if (r5 >= r0) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001a, code lost:
        r6 = r3[r5];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001c, code lost:
        if (r6 != null) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0021, code lost:
        if (r6 != l.b.u.h.NotificationLite.COMPLETE) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0023, code lost:
        r2.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
        if ((r6 instanceof l.b.u.h.NotificationLite.b) == false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002b, code lost:
        r2.a(((l.b.u.h.NotificationLite.b) r6).b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0032, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0036, code lost:
        if ((r6 instanceof l.b.u.h.NotificationLite.a) == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0038, code lost:
        r2.a(((l.b.u.h.NotificationLite.a) r6).b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0040, code lost:
        r2.b(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0043, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
        if (r6 == false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0046, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0048, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004b, code lost:
        r3 = (java.lang.Object[]) r3[r0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0050, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000e, code lost:
        r2 = r8.b;
        r3 = r0.b;
        r0 = r0.a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r8 = this;
        L_0x0000:
            monitor-enter(r8)
            l.b.u.h.AppendOnlyLinkedArrayList<java.lang.Object> r0 = r8.f2774f     // Catch:{ all -> 0x0053 }
            r1 = 0
            if (r0 != 0) goto L_0x000a
            r8.f2773e = r1     // Catch:{ all -> 0x0053 }
            monitor-exit(r8)     // Catch:{ all -> 0x0053 }
            return
        L_0x000a:
            r2 = 0
            r8.f2774f = r2     // Catch:{ all -> 0x0053 }
            monitor-exit(r8)     // Catch:{ all -> 0x0053 }
            l.b.Observer<? super T> r2 = r8.b
            java.lang.Object[] r3 = r0.b
            int r0 = r0.a
        L_0x0014:
            r4 = 1
            if (r3 == 0) goto L_0x0050
            r5 = 0
        L_0x0018:
            if (r5 >= r0) goto L_0x004b
            r6 = r3[r5]
            if (r6 != 0) goto L_0x001f
            goto L_0x004b
        L_0x001f:
            l.b.u.h.NotificationLite r7 = l.b.u.h.NotificationLite.COMPLETE
            if (r6 != r7) goto L_0x0027
            r2.a()
            goto L_0x0032
        L_0x0027:
            boolean r7 = r6 instanceof l.b.u.h.NotificationLite.b
            if (r7 == 0) goto L_0x0034
            l.b.u.h.NotificationLite$b r6 = (l.b.u.h.NotificationLite.b) r6
            java.lang.Throwable r6 = r6.b
            r2.a(r6)
        L_0x0032:
            r6 = 1
            goto L_0x0044
        L_0x0034:
            boolean r7 = r6 instanceof l.b.u.h.NotificationLite.a
            if (r7 == 0) goto L_0x0040
            l.b.u.h.NotificationLite$a r6 = (l.b.u.h.NotificationLite.a) r6
            l.b.s.Disposable r6 = r6.b
            r2.a(r6)
            goto L_0x0043
        L_0x0040:
            r2.b(r6)
        L_0x0043:
            r6 = 0
        L_0x0044:
            if (r6 == 0) goto L_0x0048
            r1 = 1
            goto L_0x0050
        L_0x0048:
            int r5 = r5 + 1
            goto L_0x0018
        L_0x004b:
            r3 = r3[r0]
            java.lang.Object[] r3 = (java.lang.Object[]) r3
            goto L_0x0014
        L_0x0050:
            if (r1 == 0) goto L_0x0000
            return
        L_0x0053:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0053 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.v.SerializedObserver.b():void");
    }

    public void a() {
        if (!this.g) {
            synchronized (this) {
                if (!this.g) {
                    if (this.f2773e) {
                        AppendOnlyLinkedArrayList<Object> appendOnlyLinkedArrayList = this.f2774f;
                        if (appendOnlyLinkedArrayList == null) {
                            appendOnlyLinkedArrayList = new AppendOnlyLinkedArrayList<>(4);
                            this.f2774f = appendOnlyLinkedArrayList;
                        }
                        appendOnlyLinkedArrayList.a(NotificationLite.COMPLETE);
                        return;
                    }
                    this.g = true;
                    this.f2773e = true;
                    this.b.a();
                }
            }
        }
    }
}
