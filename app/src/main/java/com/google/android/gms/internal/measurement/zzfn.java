package com.google.android.gms.internal.measurement;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class zzfn extends IOException {
    public zzfn(String str) {
        super(str);
    }

    public static zzfn a() {
        return new zzfn("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    public static zzfn b() {
        return new zzfn("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    public static zzfn c() {
        return new zzfn("Protocol message contained an invalid tag (zero).");
    }

    public static zzfm d() {
        return new zzfm("Protocol message tag had invalid wire type.");
    }

    public static zzfn e() {
        return new zzfn("Failed to parse the message.");
    }

    public static zzfn f() {
        return new zzfn("Protocol message had invalid UTF-8.");
    }
}
