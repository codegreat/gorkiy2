package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import i.n.a.WakefulBroadcastReceiver;
import j.c.a.a.g.a.i4;
import j.c.a.a.g.a.j4;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class AppMeasurementReceiver extends WakefulBroadcastReceiver implements j4 {
    public i4 c;

    public final void a(Context context, Intent intent) {
        WakefulBroadcastReceiver.b(context, intent);
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.c == null) {
            this.c = new i4(this);
        }
        this.c.a(context, intent);
    }

    public final BroadcastReceiver.PendingResult a() {
        return goAsync();
    }
}
