package com.google.android.gms.common.api.internal;

import androidx.annotation.Keep;
import j.c.a.a.c.k.e.d;
import j.c.a.a.c.k.e.e;

public class LifecycleCallback {
    @Keep
    public static e getChimeraLifecycleFragmentImpl(d dVar) {
        throw new IllegalStateException("Method not available in SDK.");
    }
}
