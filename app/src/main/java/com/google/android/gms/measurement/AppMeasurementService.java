package com.google.android.gms.measurement;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import i.n.a.WakefulBroadcastReceiver;
import j.c.a.a.f.e.nb;
import j.c.a.a.g.a.a8;
import j.c.a.a.g.a.c8;
import j.c.a.a.g.a.n3;
import j.c.a.a.g.a.q8;
import j.c.a.a.g.a.r4;
import j.c.a.a.g.a.s4;
import j.c.a.a.g.a.x7;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class AppMeasurementService extends Service implements c8 {
    public x7<AppMeasurementService> b;

    public final x7<AppMeasurementService> a() {
        if (this.b == null) {
            this.b = new x7<>(this);
        }
        return this.b;
    }

    public final IBinder onBind(Intent intent) {
        x7<AppMeasurementService> a = a();
        if (a == null) {
            throw null;
        } else if (intent == null) {
            a.a().f2046f.a("onBind called with null intent");
            return null;
        } else {
            String action = intent.getAction();
            if ("com.google.android.gms.measurement.START".equals(action)) {
                return new s4(q8.a((Context) a.a));
            }
            a.a().f2047i.a("onBind received unknown action", action);
            return null;
        }
    }

    public final void onCreate() {
        super.onCreate();
        r4.a(a().a, (nb) null).a().f2052n.a("Local AppMeasurementService is starting up");
    }

    public final void onDestroy() {
        r4.a(a().a, (nb) null).a().f2052n.a("Local AppMeasurementService is shutting down");
        super.onDestroy();
    }

    public final void onRebind(Intent intent) {
        a().b(intent);
    }

    public final int onStartCommand(Intent intent, int i2, int i3) {
        x7<AppMeasurementService> a = a();
        n3 a2 = r4.a(a.a, (nb) null).a();
        if (intent == null) {
            a2.f2047i.a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        a2.f2052n.a("Local AppMeasurementService called. startId, action", Integer.valueOf(i3), action);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(action)) {
            return 2;
        }
        a.a(new a8(a, i3, a2, intent));
        return 2;
    }

    public final boolean onUnbind(Intent intent) {
        a().a(intent);
        return true;
    }

    public final boolean a(int i2) {
        return stopSelfResult(i2);
    }

    public final void a(JobParameters jobParameters, boolean z) {
        throw new UnsupportedOperationException();
    }

    public final void a(Intent intent) {
        WakefulBroadcastReceiver.a(intent);
    }
}
