package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.google.android.gms.common.internal.ReflectedParcelable;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.k.g;
import j.c.a.a.c.l.n;
import j.c.a.a.c.l.p.a;
import java.util.Arrays;

public final class Status extends a implements ReflectedParcelable {
    public static final Parcelable.Creator<Status> CREATOR = new g();

    /* renamed from: f  reason: collision with root package name */
    public static final Status f375f = new Status(0, null);
    public static final Status g = new Status(15, null);
    public static final Status h = new Status(16, null);
    public final int b;
    public final int c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final PendingIntent f376e;

    static {
        new Status(14, null);
        new Status(8, null);
        new Status(17, null);
        new Status(18, null);
    }

    public Status(int i2, int i3, String str, PendingIntent pendingIntent) {
        this.b = i2;
        this.c = i3;
        this.d = str;
        this.f376e = pendingIntent;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        if (this.b != status.b || this.c != status.c || !ResourcesFlusher.c(this.d, status.d) || !ResourcesFlusher.c(this.f376e, status.f376e)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.b), Integer.valueOf(this.c), this.d, this.f376e});
    }

    public final String toString() {
        n d2 = ResourcesFlusher.d(this);
        String str = this.d;
        if (str == null) {
            int i2 = this.c;
            switch (i2) {
                case -1:
                    str = "SUCCESS_CACHE";
                    break;
                case 0:
                    str = "SUCCESS";
                    break;
                case 1:
                case 9:
                case 11:
                case 12:
                default:
                    StringBuilder sb = new StringBuilder(32);
                    sb.append("unknown status code: ");
                    sb.append(i2);
                    str = sb.toString();
                    break;
                case 2:
                    str = "SERVICE_VERSION_UPDATE_REQUIRED";
                    break;
                case 3:
                    str = "SERVICE_DISABLED";
                    break;
                case 4:
                    str = "SIGN_IN_REQUIRED";
                    break;
                case 5:
                    str = "INVALID_ACCOUNT";
                    break;
                case 6:
                    str = "RESOLUTION_REQUIRED";
                    break;
                case 7:
                    str = "NETWORK_ERROR";
                    break;
                case 8:
                    str = "INTERNAL_ERROR";
                    break;
                case 10:
                    str = "DEVELOPER_ERROR";
                    break;
                case 13:
                    str = "ERROR";
                    break;
                case 14:
                    str = "INTERRUPTED";
                    break;
                case 15:
                    str = "TIMEOUT";
                    break;
                case 16:
                    str = "CANCELED";
                    break;
                case 17:
                    str = "API_NOT_CONNECTED";
                    break;
                case 18:
                    str = "DEAD_CLIENT";
                    break;
            }
        }
        d2.a("statusCode", str);
        d2.a("resolution", this.f376e);
        return d2.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.app.PendingIntent, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int, java.lang.String):android.animation.PropertyValuesHolder
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 1, this.c);
        ResourcesFlusher.a(parcel, 2, this.d, false);
        ResourcesFlusher.a(parcel, 3, (Parcelable) this.f376e, i2, false);
        ResourcesFlusher.a(parcel, (int) AnswersRetryFilesSender.BACKOFF_MS, this.b);
        ResourcesFlusher.k(parcel, a);
    }

    public Status(int i2, String str) {
        this.b = 1;
        this.c = i2;
        this.d = str;
        this.f376e = null;
    }
}
