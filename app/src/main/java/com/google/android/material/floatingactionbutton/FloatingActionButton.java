package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import i.b.k.ResourcesFlusher;
import i.b.q.AppCompatDrawableManager;
import i.h.l.ViewCompat;
import j.c.a.b.a0.FloatingActionButtonImpl;
import j.c.a.b.a0.FloatingActionButtonImpl0;
import j.c.a.b.a0.FloatingActionButtonImpl1;
import j.c.a.b.a0.FloatingActionButtonImpl3;
import j.c.a.b.a0.FloatingActionButtonImplLollipop;
import j.c.a.b.a0.e;
import j.c.a.b.b0.DescendantOffsetUtils;
import j.c.a.b.b0.VisibilityAwareImageButton;
import j.c.a.b.d;
import j.c.a.b.f0.ShadowViewDelegate;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.g0.Shapeable;
import j.c.a.b.i0.ExtendableSavedState;
import j.c.a.b.l;
import j.c.a.b.m.MotionSpec;
import j.c.a.b.m.TransformationCallback;
import j.c.a.b.z.ExpandableTransformationWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FloatingActionButton extends VisibilityAwareImageButton implements ExpandableTransformationWidget, Shapeable, CoordinatorLayout.b {
    public ColorStateList c;
    public PorterDuff.Mode d;

    /* renamed from: e  reason: collision with root package name */
    public ColorStateList f473e;

    /* renamed from: f  reason: collision with root package name */
    public PorterDuff.Mode f474f;
    public ColorStateList g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f475i;

    /* renamed from: j  reason: collision with root package name */
    public int f476j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f477k;

    /* renamed from: l  reason: collision with root package name */
    public final Rect f478l;

    /* renamed from: m  reason: collision with root package name */
    public FloatingActionButtonImpl1 f479m;

    public static class Behavior extends BaseBehavior<FloatingActionButton> {
        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }
    }

    public static abstract class a {
        public void a(FloatingActionButton floatingActionButton) {
        }

        public void b(FloatingActionButton floatingActionButton) {
        }
    }

    public class b implements ShadowViewDelegate {
        public b() {
        }
    }

    public class c<T extends FloatingActionButton> implements e.e {
        public final TransformationCallback<T> a;

        public c(TransformationCallback<T> transformationCallback) {
            this.a = transformationCallback;
        }

        public void a() {
            this.a.a(FloatingActionButton.this);
        }

        public void b() {
            this.a.b(FloatingActionButton.this);
        }

        public boolean equals(Object obj) {
            return (obj instanceof c) && ((c) obj).a.equals(this.a);
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    private FloatingActionButtonImpl1 getImpl() {
        if (this.f479m == null) {
            this.f479m = new FloatingActionButtonImplLollipop(this, new b());
        }
        return this.f479m;
    }

    public void b(a aVar, boolean z) {
        j.c.a.b.a0.FloatingActionButton floatingActionButton;
        FloatingActionButtonImpl1 impl = getImpl();
        if (aVar == null) {
            floatingActionButton = null;
        } else {
            floatingActionButton = new j.c.a.b.a0.FloatingActionButton(this, aVar);
        }
        if (!impl.c()) {
            Animator animator = impl.f2153k;
            if (animator != null) {
                animator.cancel();
            }
            if (impl.j()) {
                if (impl.f2162t.getVisibility() != 0) {
                    impl.f2162t.setAlpha(0.0f);
                    impl.f2162t.setScaleY(0.0f);
                    impl.f2162t.setScaleX(0.0f);
                    impl.a(0.0f);
                }
                MotionSpec motionSpec = impl.f2154l;
                if (motionSpec == null) {
                    if (impl.f2151i == null) {
                        impl.f2151i = MotionSpec.a(impl.f2162t.getContext(), j.c.a.b.a.design_fab_show_motion_spec);
                    }
                    motionSpec = impl.f2151i;
                    ResourcesFlusher.a(motionSpec);
                }
                AnimatorSet a2 = impl.a(motionSpec, 1.0f, 1.0f, 1.0f);
                a2.addListener(new FloatingActionButtonImpl0(impl, z, floatingActionButton));
                ArrayList<Animator.AnimatorListener> arrayList = impl.f2159q;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            impl.f2162t.a(0, z);
            impl.f2162t.setAlpha(1.0f);
            impl.f2162t.setScaleY(1.0f);
            impl.f2162t.setScaleX(1.0f);
            impl.a(1.0f);
            if (floatingActionButton != null) {
                floatingActionButton.a.b(floatingActionButton.b);
            }
        }
    }

    public boolean c() {
        return getImpl().c();
    }

    public final void d() {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            ColorStateList colorStateList = this.f473e;
            if (colorStateList == null) {
                ResourcesFlusher.a(drawable);
                return;
            }
            int colorForState = colorStateList.getColorForState(getDrawableState(), 0);
            PorterDuff.Mode mode = this.f474f;
            if (mode == null) {
                mode = PorterDuff.Mode.SRC_IN;
            }
            drawable.mutate().setColorFilter(AppCompatDrawableManager.a(colorForState, mode));
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        getImpl().a(getDrawableState());
    }

    public ColorStateList getBackgroundTintList() {
        return this.c;
    }

    public PorterDuff.Mode getBackgroundTintMode() {
        return this.d;
    }

    public CoordinatorLayout.c<FloatingActionButton> getBehavior() {
        return new Behavior();
    }

    public float getCompatElevation() {
        return getImpl().a();
    }

    public float getCompatHoveredFocusedTranslationZ() {
        return getImpl().f2150f;
    }

    public float getCompatPressedTranslationZ() {
        return getImpl().g;
    }

    public Drawable getContentBackground() {
        if (getImpl() != null) {
            return null;
        }
        throw null;
    }

    public int getCustomSize() {
        return this.f475i;
    }

    public int getExpandedComponentIdHint() {
        throw null;
    }

    public MotionSpec getHideMotionSpec() {
        return getImpl().f2155m;
    }

    @Deprecated
    public int getRippleColor() {
        ColorStateList colorStateList = this.g;
        if (colorStateList != null) {
            return colorStateList.getDefaultColor();
        }
        return 0;
    }

    public ColorStateList getRippleColorStateList() {
        return this.g;
    }

    public ShapeAppearanceModel getShapeAppearanceModel() {
        ShapeAppearanceModel shapeAppearanceModel = getImpl().a;
        ResourcesFlusher.a(shapeAppearanceModel);
        return shapeAppearanceModel;
    }

    public MotionSpec getShowMotionSpec() {
        return getImpl().f2154l;
    }

    public int getSize() {
        return this.h;
    }

    public int getSizeDimension() {
        return a(this.h);
    }

    public ColorStateList getSupportBackgroundTintList() {
        return getBackgroundTintList();
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        return getBackgroundTintMode();
    }

    public ColorStateList getSupportImageTintList() {
        return this.f473e;
    }

    public PorterDuff.Mode getSupportImageTintMode() {
        return this.f474f;
    }

    public boolean getUseCompatPadding() {
        return this.f477k;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        getImpl().d();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        FloatingActionButtonImpl1 impl = getImpl();
        if (impl.h()) {
            ViewTreeObserver viewTreeObserver = impl.f2162t.getViewTreeObserver();
            if (impl.z == null) {
                impl.z = new FloatingActionButtonImpl3(impl);
            }
            viewTreeObserver.addOnPreDrawListener(impl.z);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        FloatingActionButtonImpl1 impl = getImpl();
        ViewTreeObserver viewTreeObserver = impl.f2162t.getViewTreeObserver();
        ViewTreeObserver.OnPreDrawListener onPreDrawListener = impl.z;
        if (onPreDrawListener != null) {
            viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            impl.z = null;
        }
    }

    public void onMeasure(int i2, int i3) {
        int sizeDimension = getSizeDimension();
        this.f476j = (sizeDimension + 0) / 2;
        getImpl().m();
        Math.min(a(sizeDimension, i2), a(sizeDimension, i3));
        throw null;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof ExtendableSavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        ExtendableSavedState extendableSavedState = (ExtendableSavedState) parcelable;
        super.onRestoreInstanceState(extendableSavedState.b);
        ResourcesFlusher.a(extendableSavedState.d.getOrDefault("expandableWidgetHelper", null));
        throw null;
    }

    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            onSaveInstanceState = new Bundle();
        }
        new ExtendableSavedState(onSaveInstanceState);
        throw null;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            a((Rect) null);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setBackgroundColor(int i2) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundResource(int i2) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        if (this.c != colorStateList) {
            this.c = colorStateList;
            if (getImpl() == null) {
                throw null;
            }
        }
    }

    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.d != mode) {
            this.d = mode;
            if (getImpl() == null) {
                throw null;
            }
        }
    }

    public void setCompatElevation(float f2) {
        FloatingActionButtonImpl1 impl = getImpl();
        if (impl.f2149e != f2) {
            impl.f2149e = f2;
            impl.a(f2, impl.f2150f, impl.g);
        }
    }

    public void setCompatElevationResource(int i2) {
        setCompatElevation(getResources().getDimension(i2));
    }

    public void setCompatHoveredFocusedTranslationZ(float f2) {
        FloatingActionButtonImpl1 impl = getImpl();
        if (impl.f2150f != f2) {
            impl.f2150f = f2;
            impl.a(impl.f2149e, f2, impl.g);
        }
    }

    public void setCompatHoveredFocusedTranslationZResource(int i2) {
        setCompatHoveredFocusedTranslationZ(getResources().getDimension(i2));
    }

    public void setCompatPressedTranslationZ(float f2) {
        FloatingActionButtonImpl1 impl = getImpl();
        if (impl.g != f2) {
            impl.g = f2;
            impl.a(impl.f2149e, impl.f2150f, f2);
        }
    }

    public void setCompatPressedTranslationZResource(int i2) {
        setCompatPressedTranslationZ(getResources().getDimension(i2));
    }

    public void setCustomSize(int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Custom size must be non-negative");
        } else if (i2 != this.f475i) {
            this.f475i = i2;
            requestLayout();
        }
    }

    public void setElevation(float f2) {
        super.setElevation(f2);
        if (getImpl() == null) {
            throw null;
        }
    }

    public void setEnsureMinTouchTargetSize(boolean z) {
        if (z != getImpl().c) {
            getImpl().c = z;
            requestLayout();
        }
    }

    public void setExpandedComponentIdHint(int i2) {
        throw null;
    }

    public void setHideMotionSpec(MotionSpec motionSpec) {
        getImpl().f2155m = motionSpec;
    }

    public void setHideMotionSpecResource(int i2) {
        setHideMotionSpec(MotionSpec.a(getContext(), i2));
    }

    public void setImageDrawable(Drawable drawable) {
        if (getDrawable() != drawable) {
            super.setImageDrawable(drawable);
            FloatingActionButtonImpl1 impl = getImpl();
            impl.a(impl.f2157o);
            if (this.f473e != null) {
                d();
            }
        }
    }

    public void setImageResource(int i2) {
        throw null;
    }

    public void setRippleColor(int i2) {
        setRippleColor(ColorStateList.valueOf(i2));
    }

    public void setScaleX(float f2) {
        super.setScaleX(f2);
        getImpl().f();
    }

    public void setScaleY(float f2) {
        super.setScaleY(f2);
        getImpl().f();
    }

    public void setShadowPaddingEnabled(boolean z) {
        FloatingActionButtonImpl1 impl = getImpl();
        impl.d = z;
        impl.m();
    }

    public void setShapeAppearanceModel(ShapeAppearanceModel shapeAppearanceModel) {
        getImpl().a = shapeAppearanceModel;
    }

    public void setShowMotionSpec(MotionSpec motionSpec) {
        getImpl().f2154l = motionSpec;
    }

    public void setShowMotionSpecResource(int i2) {
        setShowMotionSpec(MotionSpec.a(getContext(), i2));
    }

    public void setSize(int i2) {
        this.f475i = 0;
        if (i2 != this.h) {
            this.h = i2;
            requestLayout();
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        setBackgroundTintList(colorStateList);
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        setBackgroundTintMode(mode);
    }

    public void setSupportImageTintList(ColorStateList colorStateList) {
        if (this.f473e != colorStateList) {
            this.f473e = colorStateList;
            d();
        }
    }

    public void setSupportImageTintMode(PorterDuff.Mode mode) {
        if (this.f474f != mode) {
            this.f474f = mode;
            d();
        }
    }

    public void setTranslationX(float f2) {
        super.setTranslationX(f2);
        getImpl().g();
    }

    public void setTranslationY(float f2) {
        super.setTranslationY(f2);
        getImpl().g();
    }

    public void setTranslationZ(float f2) {
        super.setTranslationZ(f2);
        getImpl().g();
    }

    public void setUseCompatPadding(boolean z) {
        if (this.f477k != z) {
            this.f477k = z;
            getImpl().e();
        }
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public static class BaseBehavior<T extends FloatingActionButton> extends CoordinatorLayout.c<T> {
        public Rect a;
        public boolean b;

        public BaseBehavior() {
            this.b = true;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, Rect rect) {
            FloatingActionButton floatingActionButton = (FloatingActionButton) view;
            Rect rect2 = floatingActionButton.f478l;
            rect.set(floatingActionButton.getLeft() + rect2.left, floatingActionButton.getTop() + rect2.top, floatingActionButton.getRight() - rect2.right, floatingActionButton.getBottom() - rect2.bottom);
            return true;
        }

        public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
            FloatingActionButton floatingActionButton = (FloatingActionButton) view;
            if (view2 instanceof AppBarLayout) {
                a(coordinatorLayout, (AppBarLayout) view2, floatingActionButton);
            } else {
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                if (layoutParams instanceof CoordinatorLayout.f ? ((CoordinatorLayout.f) layoutParams).a instanceof BottomSheetBehavior : false) {
                    b(view2, floatingActionButton);
                }
            }
            return false;
        }

        public BaseBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.FloatingActionButton_Behavior_Layout);
            this.b = obtainStyledAttributes.getBoolean(l.FloatingActionButton_Behavior_Layout_behavior_autoHide, true);
            obtainStyledAttributes.recycle();
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2) {
            int i3;
            FloatingActionButton floatingActionButton = (FloatingActionButton) view;
            List<View> a2 = coordinatorLayout.a(floatingActionButton);
            int size = a2.size();
            int i4 = 0;
            for (int i5 = 0; i5 < size; i5++) {
                View view2 = a2.get(i5);
                if (!(view2 instanceof AppBarLayout)) {
                    ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                    if ((layoutParams instanceof CoordinatorLayout.f ? ((CoordinatorLayout.f) layoutParams).a instanceof BottomSheetBehavior : false) && b(view2, floatingActionButton)) {
                        break;
                    }
                } else if (a(coordinatorLayout, (AppBarLayout) view2, floatingActionButton)) {
                    break;
                }
            }
            coordinatorLayout.b(floatingActionButton, i2);
            Rect rect = floatingActionButton.f478l;
            if (rect == null || rect.centerX() <= 0 || rect.centerY() <= 0) {
                return true;
            }
            CoordinatorLayout.f fVar = (CoordinatorLayout.f) floatingActionButton.getLayoutParams();
            if (floatingActionButton.getRight() >= coordinatorLayout.getWidth() - fVar.rightMargin) {
                i3 = rect.right;
            } else {
                i3 = floatingActionButton.getLeft() <= fVar.leftMargin ? -rect.left : 0;
            }
            if (floatingActionButton.getBottom() >= coordinatorLayout.getHeight() - fVar.bottomMargin) {
                i4 = rect.bottom;
            } else if (floatingActionButton.getTop() <= fVar.topMargin) {
                i4 = -rect.top;
            }
            if (i4 != 0) {
                ViewCompat.e(floatingActionButton, i4);
            }
            if (i3 == 0) {
                return true;
            }
            ViewCompat.d(floatingActionButton, i3);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton$a, boolean):void
         arg types: [?[OBJECT, ARRAY], int]
         candidates:
          com.google.android.material.floatingactionbutton.FloatingActionButton.a(int, int):int
          com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton, android.graphics.drawable.Drawable):void
          j.c.a.b.b0.VisibilityAwareImageButton.a(int, boolean):void
          com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton$a, boolean):void */
        public final boolean b(View view, FloatingActionButton floatingActionButton) {
            if (!a(view, floatingActionButton)) {
                return false;
            }
            if (view.getTop() < (floatingActionButton.getHeight() / 2) + ((CoordinatorLayout.f) floatingActionButton.getLayoutParams()).topMargin) {
                floatingActionButton.a((a) null, false);
                return true;
            }
            floatingActionButton.b(null, false);
            return true;
        }

        public void a(CoordinatorLayout.f fVar) {
            if (fVar.h == 0) {
                fVar.h = 80;
            }
        }

        public final boolean a(View view, FloatingActionButton floatingActionButton) {
            CoordinatorLayout.f fVar = (CoordinatorLayout.f) floatingActionButton.getLayoutParams();
            if (this.b && fVar.f189f == view.getId() && floatingActionButton.getUserSetVisibility() == 0) {
                return true;
            }
            return false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton$a, boolean):void
         arg types: [?[OBJECT, ARRAY], int]
         candidates:
          com.google.android.material.floatingactionbutton.FloatingActionButton.a(int, int):int
          com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton, android.graphics.drawable.Drawable):void
          j.c.a.b.b0.VisibilityAwareImageButton.a(int, boolean):void
          com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton$a, boolean):void */
        public final boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, FloatingActionButton floatingActionButton) {
            if (!a(appBarLayout, floatingActionButton)) {
                return false;
            }
            if (this.a == null) {
                this.a = new Rect();
            }
            Rect rect = this.a;
            DescendantOffsetUtils.a(coordinatorLayout, appBarLayout, rect);
            if (rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                floatingActionButton.a((a) null, false);
                return true;
            }
            floatingActionButton.b(null, false);
            return true;
        }
    }

    public void a(a aVar, boolean z) {
        j.c.a.b.a0.FloatingActionButton floatingActionButton;
        FloatingActionButtonImpl1 impl = getImpl();
        if (aVar == null) {
            floatingActionButton = null;
        } else {
            floatingActionButton = new j.c.a.b.a0.FloatingActionButton(this, aVar);
        }
        if (!impl.b()) {
            Animator animator = impl.f2153k;
            if (animator != null) {
                animator.cancel();
            }
            if (impl.j()) {
                MotionSpec motionSpec = impl.f2155m;
                if (motionSpec == null) {
                    if (impl.f2152j == null) {
                        impl.f2152j = MotionSpec.a(impl.f2162t.getContext(), j.c.a.b.a.design_fab_hide_motion_spec);
                    }
                    motionSpec = impl.f2152j;
                    ResourcesFlusher.a(motionSpec);
                }
                AnimatorSet a2 = impl.a(motionSpec, 0.0f, 0.0f, 0.0f);
                a2.addListener(new FloatingActionButtonImpl(impl, z, floatingActionButton));
                ArrayList<Animator.AnimatorListener> arrayList = impl.f2160r;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            impl.f2162t.a(z ? 8 : 4, z);
            if (floatingActionButton != null) {
                floatingActionButton.a.a(floatingActionButton.b);
            }
        }
    }

    public void setRippleColor(ColorStateList colorStateList) {
        if (this.g != colorStateList) {
            this.g = colorStateList;
            getImpl().a(this.g);
        }
    }

    public void a(Animator.AnimatorListener animatorListener) {
        FloatingActionButtonImpl1 impl = getImpl();
        if (impl.f2160r == null) {
            impl.f2160r = new ArrayList<>();
        }
        impl.f2160r.add(animatorListener);
    }

    public boolean a() {
        throw null;
    }

    public final int a(int i2) {
        int i3 = this.f475i;
        if (i3 != 0) {
            return i3;
        }
        Resources resources = getResources();
        if (i2 != -1) {
            if (i2 != 1) {
                return resources.getDimensionPixelSize(d.design_fab_size_normal);
            }
            return resources.getDimensionPixelSize(d.design_fab_size_mini);
        } else if (Math.max(resources.getConfiguration().screenWidthDp, resources.getConfiguration().screenHeightDp) < 470) {
            return a(1);
        } else {
            return a(0);
        }
    }

    public void b(Animator.AnimatorListener animatorListener) {
        FloatingActionButtonImpl1 impl = getImpl();
        if (impl.f2159q == null) {
            impl.f2159q = new ArrayList<>();
        }
        impl.f2159q.add(animatorListener);
    }

    public boolean b() {
        return getImpl().b();
    }

    @Deprecated
    public boolean a(Rect rect) {
        if (!ViewCompat.w(this)) {
            return false;
        }
        rect.set(0, 0, getWidth(), getHeight());
        throw null;
    }

    public static int a(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode == Integer.MIN_VALUE) {
            return Math.min(i2, size);
        }
        if (mode == 0) {
            return i2;
        }
        if (mode == 1073741824) {
            return size;
        }
        throw new IllegalArgumentException();
    }

    public void a(TransformationCallback<? extends FloatingActionButton> transformationCallback) {
        FloatingActionButtonImpl1 impl = getImpl();
        c cVar = new c(transformationCallback);
        if (impl.f2161s == null) {
            impl.f2161s = new ArrayList<>();
        }
        impl.f2161s.add(cVar);
    }
}
