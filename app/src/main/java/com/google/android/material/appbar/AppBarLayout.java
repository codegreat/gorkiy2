package com.google.android.material.appbar;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.h.l.NestedScrollingChild;
import i.h.l.OnApplyWindowInsetsListener;
import i.h.l.ViewCompat;
import i.h.l.WindowInsetsCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.j.a.AbsSavedState;
import j.c.a.a.c.n.c;
import j.c.a.b.d;
import j.c.a.b.g;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.k;
import j.c.a.b.l;
import j.c.a.b.m.AnimationUtils;
import j.c.a.b.n.AppBarLayout0;
import j.c.a.b.n.AppBarLayout1;
import j.c.a.b.n.AppBarLayout2;
import j.c.a.b.n.HeaderBehavior;
import j.c.a.b.n.HeaderScrollingViewBehavior;
import j.c.a.b.n.ViewUtilsLollipop;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

public class AppBarLayout extends LinearLayout implements CoordinatorLayout.b {

    /* renamed from: q  reason: collision with root package name */
    public static final int f384q = k.Widget_Design_AppBarLayout;
    public int b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f385e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f386f;
    public int g;
    public WindowInsetsCompat h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f387i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f388j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f389k;

    /* renamed from: l  reason: collision with root package name */
    public int f390l;

    /* renamed from: m  reason: collision with root package name */
    public WeakReference<View> f391m;

    /* renamed from: n  reason: collision with root package name */
    public ValueAnimator f392n;

    /* renamed from: o  reason: collision with root package name */
    public int[] f393o;

    /* renamed from: p  reason: collision with root package name */
    public Drawable f394p;

    public static class Behavior extends BaseBehavior<AppBarLayout> {
        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }
    }

    public static class ScrollingViewBehavior extends HeaderScrollingViewBehavior {
        public ScrollingViewBehavior() {
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 instanceof AppBarLayout;
        }

        public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
            CoordinatorLayout.c cVar = ((CoordinatorLayout.f) view2.getLayoutParams()).a;
            if (cVar instanceof BaseBehavior) {
                ViewCompat.e(view, (((view2.getBottom() - view.getTop()) + ((BaseBehavior) cVar).f395k) + super.f2302f) - a(view2));
            }
            if (!(view2 instanceof AppBarLayout)) {
                return false;
            }
            AppBarLayout appBarLayout = (AppBarLayout) view2;
            if (!appBarLayout.f389k) {
                return false;
            }
            appBarLayout.a(appBarLayout.a(view));
            return false;
        }

        public void c(CoordinatorLayout coordinatorLayout, View view, View view2) {
            if (view2 instanceof AppBarLayout) {
                ViewCompat.f(coordinatorLayout, AccessibilityNodeInfoCompat.a.f1203f.a());
                ViewCompat.f(coordinatorLayout, AccessibilityNodeInfoCompat.a.g.a());
            }
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.ScrollingViewBehavior_Layout);
            super.g = obtainStyledAttributes.getDimensionPixelSize(l.ScrollingViewBehavior_Layout_behavior_overlapTop, 0);
            obtainStyledAttributes.recycle();
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, Rect rect, boolean z) {
            AppBarLayout a = a(coordinatorLayout.a(view));
            if (a != null) {
                rect.offset(view.getLeft(), view.getTop());
                Rect rect2 = super.d;
                rect2.set(0, 0, coordinatorLayout.getWidth(), coordinatorLayout.getHeight());
                if (!rect2.contains(rect)) {
                    a.a(false, !z, true);
                    return true;
                }
            }
            return false;
        }

        public AppBarLayout a(List<View> list) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = list.get(i2);
                if (view instanceof AppBarLayout) {
                    return (AppBarLayout) view;
                }
            }
            return null;
        }
    }

    public class a implements OnApplyWindowInsetsListener {
        public a() {
        }

        public WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
            AppBarLayout appBarLayout = AppBarLayout.this;
            WindowInsetsCompat windowInsetsCompat2 = null;
            if (appBarLayout != null) {
                if (ViewCompat.h(appBarLayout)) {
                    windowInsetsCompat2 = windowInsetsCompat;
                }
                if (!Objects.equals(appBarLayout.h, windowInsetsCompat2)) {
                    appBarLayout.h = windowInsetsCompat2;
                    appBarLayout.b();
                    appBarLayout.requestLayout();
                }
                return windowInsetsCompat;
            }
            throw null;
        }
    }

    public AppBarLayout(Context context) {
        this(context, null);
    }

    public final void a(boolean z, boolean z2, boolean z3) {
        int i2 = 0;
        int i3 = (z ? 1 : 2) | (z2 ? 4 : 0);
        if (z3) {
            i2 = 8;
        }
        this.g = i3 | i2;
        requestLayout();
    }

    public final void b() {
        setWillNotDraw(!(this.f394p != null && getTopInset() > 0));
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof b;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f394p != null && getTopInset() > 0) {
            int save = canvas.save();
            canvas.translate(0.0f, (float) (-this.b));
            this.f394p.draw(canvas);
            canvas.restoreToCount(save);
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.f394p;
        if (drawable != null && drawable.isStateful() && drawable.setState(drawableState)) {
            invalidateDrawable(drawable);
        }
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new b(-1, -2);
    }

    public CoordinatorLayout.c<AppBarLayout> getBehavior() {
        return new Behavior();
    }

    public int getDownNestedPreScrollRange() {
        int i2;
        int l2;
        int i3 = this.d;
        if (i3 != -1) {
            return i3;
        }
        int i4 = 0;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            b bVar = (b) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = bVar.a;
            if ((i5 & 5) == 5) {
                int i6 = bVar.topMargin + bVar.bottomMargin;
                if ((i5 & 8) != 0) {
                    l2 = ViewCompat.l(childAt);
                } else if ((i5 & 2) != 0) {
                    l2 = measuredHeight - ViewCompat.l(childAt);
                } else {
                    i2 = i6 + measuredHeight;
                    if (childCount == 0 && ViewCompat.h(childAt)) {
                        i2 = Math.min(i2, measuredHeight - getTopInset());
                    }
                    i4 += i2;
                }
                i2 = l2 + i6;
                i2 = Math.min(i2, measuredHeight - getTopInset());
                i4 += i2;
            } else if (i4 > 0) {
                break;
            }
        }
        int max = Math.max(0, i4);
        this.d = max;
        return max;
    }

    public int getDownNestedScrollRange() {
        int i2 = this.f385e;
        if (i2 != -1) {
            return i2;
        }
        int childCount = getChildCount();
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt = getChildAt(i3);
            b bVar = (b) childAt.getLayoutParams();
            int measuredHeight = bVar.topMargin + bVar.bottomMargin + childAt.getMeasuredHeight();
            int i5 = bVar.a;
            if ((i5 & 1) == 0) {
                break;
            }
            i4 += measuredHeight;
            if ((i5 & 2) != 0) {
                i4 -= ViewCompat.l(childAt);
                break;
            }
            i3++;
        }
        int max = Math.max(0, i4);
        this.f385e = max;
        return max;
    }

    public int getLiftOnScrollTargetViewId() {
        return this.f390l;
    }

    public final int getMinimumHeightForVisibleOverlappingContent() {
        int topInset = getTopInset();
        int l2 = ViewCompat.l(this);
        if (l2 == 0) {
            int childCount = getChildCount();
            l2 = childCount >= 1 ? getChildAt(childCount - 1).getMinimumHeight() : 0;
            if (l2 == 0) {
                return getHeight() / 3;
            }
        }
        return (l2 * 2) + topInset;
    }

    public int getPendingAction() {
        return this.g;
    }

    public Drawable getStatusBarForeground() {
        return this.f394p;
    }

    @Deprecated
    public float getTargetElevation() {
        return 0.0f;
    }

    public final int getTopInset() {
        WindowInsetsCompat windowInsetsCompat = this.h;
        if (windowInsetsCompat != null) {
            return windowInsetsCompat.d();
        }
        return 0;
    }

    public final int getTotalScrollRange() {
        int i2 = this.c;
        if (i2 != -1) {
            return i2;
        }
        int childCount = getChildCount();
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt = getChildAt(i3);
            b bVar = (b) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = bVar.a;
            if ((i5 & 1) == 0) {
                break;
            }
            int i6 = measuredHeight + bVar.topMargin + bVar.bottomMargin + i4;
            if (i3 == 0 && ViewCompat.h(childAt)) {
                i6 -= getTopInset();
            }
            i4 = i6;
            if ((i5 & 2) != 0) {
                i4 -= ViewCompat.l(childAt);
                break;
            }
            i3++;
        }
        int max = Math.max(0, i4);
        this.c = max;
        return max;
    }

    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Drawable background = getBackground();
        if (background instanceof MaterialShapeDrawable) {
            c.a(this, (MaterialShapeDrawable) background);
        }
    }

    public int[] onCreateDrawableState(int i2) {
        if (this.f393o == null) {
            this.f393o = new int[4];
        }
        int[] iArr = this.f393o;
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + iArr.length);
        iArr[0] = this.f387i ? j.c.a.b.b.state_liftable : -j.c.a.b.b.state_liftable;
        iArr[1] = (!this.f387i || !this.f388j) ? -j.c.a.b.b.state_lifted : j.c.a.b.b.state_lifted;
        iArr[2] = this.f387i ? j.c.a.b.b.state_collapsible : -j.c.a.b.b.state_collapsible;
        iArr[3] = (!this.f387i || !this.f388j) ? -j.c.a.b.b.state_collapsed : j.c.a.b.b.state_collapsed;
        return LinearLayout.mergeDrawableStates(onCreateDrawableState, iArr);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        WeakReference<View> weakReference = this.f391m;
        if (weakReference != null) {
            weakReference.clear();
        }
        this.f391m = null;
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        boolean z2;
        super.onLayout(z, i2, i3, i4, i5);
        boolean z3 = true;
        if (ViewCompat.h(this) && a()) {
            int topInset = getTopInset();
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                ViewCompat.e(getChildAt(childCount), topInset);
            }
        }
        this.c = -1;
        this.d = -1;
        this.f385e = -1;
        this.f386f = false;
        int childCount2 = getChildCount();
        int i6 = 0;
        while (true) {
            if (i6 >= childCount2) {
                break;
            } else if (((b) getChildAt(i6).getLayoutParams()).b != null) {
                this.f386f = true;
                break;
            } else {
                i6++;
            }
        }
        Drawable drawable = this.f394p;
        if (drawable != null) {
            drawable.setBounds(0, 0, getWidth(), getTopInset());
        }
        if (!this.f389k) {
            int childCount3 = getChildCount();
            int i7 = 0;
            while (true) {
                if (i7 >= childCount3) {
                    z2 = false;
                    break;
                }
                int i8 = ((b) getChildAt(i7).getLayoutParams()).a;
                if ((i8 & 1) == 1 && (i8 & 10) != 0) {
                    z2 = true;
                    break;
                }
                i7++;
            }
            if (!z2) {
                z3 = false;
            }
        }
        if (this.f387i != z3) {
            this.f387i = z3;
            refreshDrawableState();
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i3);
        if (mode != 1073741824 && ViewCompat.h(this) && a()) {
            int measuredHeight = getMeasuredHeight();
            if (mode == Integer.MIN_VALUE) {
                measuredHeight = ResourcesFlusher.a(getTopInset() + getMeasuredHeight(), 0, View.MeasureSpec.getSize(i3));
            } else if (mode == 0) {
                measuredHeight += getTopInset();
            }
            setMeasuredDimension(getMeasuredWidth(), measuredHeight);
        }
        this.c = -1;
        this.d = -1;
        this.f385e = -1;
    }

    public void setElevation(float f2) {
        super.setElevation(f2);
        c.a(this, f2);
    }

    public void setExpanded(boolean z) {
        a(z, ViewCompat.w(this), true);
    }

    public void setLiftOnScroll(boolean z) {
        this.f389k = z;
    }

    public void setLiftOnScrollTargetViewId(int i2) {
        this.f390l = i2;
        WeakReference<View> weakReference = this.f391m;
        if (weakReference != null) {
            weakReference.clear();
        }
        this.f391m = null;
    }

    public void setOrientation(int i2) {
        if (i2 == 1) {
            super.setOrientation(i2);
            return;
        }
        throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
    }

    public void setStatusBarForeground(Drawable drawable) {
        Drawable drawable2 = this.f394p;
        if (drawable2 != drawable) {
            Drawable drawable3 = null;
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            if (drawable != null) {
                drawable3 = drawable.mutate();
            }
            this.f394p = drawable3;
            if (drawable3 != null) {
                if (drawable3.isStateful()) {
                    this.f394p.setState(getDrawableState());
                }
                ResourcesFlusher.a(this.f394p, ViewCompat.k(this));
                this.f394p.setVisible(getVisibility() == 0, false);
                this.f394p.setCallback(this);
            }
            b();
            ViewCompat.A(this);
        }
    }

    public void setStatusBarForegroundColor(int i2) {
        setStatusBarForeground(new ColorDrawable(i2));
    }

    public void setStatusBarForegroundResource(int i2) {
        setStatusBarForeground(AppCompatResources.c(getContext(), i2));
    }

    @Deprecated
    public void setTargetElevation(float f2) {
        ViewUtilsLollipop.a(this, f2);
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        boolean z = i2 == 0;
        Drawable drawable = this.f394p;
        if (drawable != null) {
            drawable.setVisible(z, false);
        }
    }

    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f394p;
    }

    public static class BaseBehavior<T extends AppBarLayout> extends HeaderBehavior<T> {

        /* renamed from: k  reason: collision with root package name */
        public int f395k;

        /* renamed from: l  reason: collision with root package name */
        public int f396l;

        /* renamed from: m  reason: collision with root package name */
        public ValueAnimator f397m;

        /* renamed from: n  reason: collision with root package name */
        public int f398n = -1;

        /* renamed from: o  reason: collision with root package name */
        public boolean f399o;

        /* renamed from: p  reason: collision with root package name */
        public float f400p;

        /* renamed from: q  reason: collision with root package name */
        public WeakReference<View> f401q;

        public BaseBehavior() {
        }

        public static boolean a(int i2, int i3) {
            return (i2 & i3) == i3;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, boolean):void
         arg types: [androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, int]
         candidates:
          com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, android.view.View, int, int[]):void
          j.c.a.b.n.HeaderBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int, int, int):int
          androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, float, float):boolean
          com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, boolean):void */
        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2) {
            int i3;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            super.a(coordinatorLayout, appBarLayout, i2);
            int pendingAction = appBarLayout.getPendingAction();
            int i4 = this.f398n;
            if (i4 >= 0 && (pendingAction & 8) == 0) {
                View childAt = appBarLayout.getChildAt(i4);
                int i5 = -childAt.getBottom();
                if (this.f399o) {
                    i3 = appBarLayout.getTopInset() + childAt.getMinimumHeight() + i5;
                } else {
                    i3 = Math.round(((float) childAt.getHeight()) * this.f400p) + i5;
                }
                c(coordinatorLayout, appBarLayout, i3);
            } else if (pendingAction != 0) {
                boolean z = (pendingAction & 4) != 0;
                if ((pendingAction & 2) != 0) {
                    int i6 = -appBarLayout.getUpNestedPreScrollRange();
                    if (z) {
                        a(coordinatorLayout, appBarLayout, i6, 0.0f);
                    } else {
                        c(coordinatorLayout, appBarLayout, i6);
                    }
                } else if ((pendingAction & 1) != 0) {
                    if (z) {
                        a(coordinatorLayout, appBarLayout, 0, 0.0f);
                    } else {
                        c(coordinatorLayout, appBarLayout, 0);
                    }
                }
            }
            appBarLayout.g = 0;
            this.f398n = -1;
            a(ResourcesFlusher.a(b(), -appBarLayout.getTotalScrollRange(), 0));
            a(coordinatorLayout, appBarLayout, b(), 0, true);
            appBarLayout.b = b();
            if (!appBarLayout.willNotDraw()) {
                ViewCompat.A(appBarLayout);
            }
            b(coordinatorLayout, appBarLayout);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, boolean):void
         arg types: [androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, char, int]
         candidates:
          com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, android.view.View, int, int[]):void
          j.c.a.b.n.HeaderBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int, int, int):int
          androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, float, float):boolean
          com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, boolean):void */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x00d2  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x00d7  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x00da  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int b(androidx.coordinatorlayout.widget.CoordinatorLayout r8, android.view.View r9, int r10, int r11, int r12) {
            /*
                r7 = this;
                com.google.android.material.appbar.AppBarLayout r9 = (com.google.android.material.appbar.AppBarLayout) r9
                int r0 = r7.c()
                r1 = 0
                if (r11 == 0) goto L_0x00e5
                if (r0 < r11) goto L_0x00e5
                if (r0 > r12) goto L_0x00e5
                int r3 = i.b.k.ResourcesFlusher.a(r10, r11, r12)
                if (r0 == r3) goto L_0x00e7
                boolean r10 = r9.f386f
                if (r10 == 0) goto L_0x0086
                int r10 = java.lang.Math.abs(r3)
                int r11 = r9.getChildCount()
                r12 = 0
            L_0x0020:
                if (r12 >= r11) goto L_0x0086
                android.view.View r2 = r9.getChildAt(r12)
                android.view.ViewGroup$LayoutParams r4 = r2.getLayoutParams()
                com.google.android.material.appbar.AppBarLayout$b r4 = (com.google.android.material.appbar.AppBarLayout.b) r4
                android.view.animation.Interpolator r5 = r4.b
                int r6 = r2.getTop()
                if (r10 < r6) goto L_0x0083
                int r6 = r2.getBottom()
                if (r10 > r6) goto L_0x0083
                if (r5 == 0) goto L_0x0086
                int r11 = r4.a
                r12 = r11 & 1
                if (r12 == 0) goto L_0x0057
                int r12 = r2.getHeight()
                int r6 = r4.topMargin
                int r12 = r12 + r6
                int r4 = r4.bottomMargin
                int r12 = r12 + r4
                int r12 = r12 + r1
                r11 = r11 & 2
                if (r11 == 0) goto L_0x0058
                int r11 = i.h.l.ViewCompat.l(r2)
                int r12 = r12 - r11
                goto L_0x0058
            L_0x0057:
                r12 = 0
            L_0x0058:
                boolean r11 = i.h.l.ViewCompat.h(r2)
                if (r11 == 0) goto L_0x0063
                int r11 = r9.getTopInset()
                int r12 = r12 - r11
            L_0x0063:
                if (r12 <= 0) goto L_0x0086
                int r11 = r2.getTop()
                int r10 = r10 - r11
                float r11 = (float) r12
                float r10 = (float) r10
                float r10 = r10 / r11
                float r10 = r5.getInterpolation(r10)
                float r10 = r10 * r11
                int r10 = java.lang.Math.round(r10)
                int r11 = java.lang.Integer.signum(r3)
                int r12 = r2.getTop()
                int r12 = r12 + r10
                int r12 = r12 * r11
                goto L_0x0087
            L_0x0083:
                int r12 = r12 + 1
                goto L_0x0020
            L_0x0086:
                r12 = r3
            L_0x0087:
                boolean r10 = r7.a(r12)
                int r11 = r0 - r3
                int r12 = r3 - r12
                r7.f395k = r12
                if (r10 != 0) goto L_0x00c6
                boolean r10 = r9.f386f
                if (r10 == 0) goto L_0x00c6
                i.g.d.DirectedAcyclicGraph<android.view.View> r10 = r8.c
                i.e.SimpleArrayMap<T, java.util.ArrayList<T>> r10 = r10.b
                r12 = 0
                java.lang.Object r10 = r10.getOrDefault(r9, r12)
                java.util.List r10 = (java.util.List) r10
                if (r10 == 0) goto L_0x00c6
                boolean r12 = r10.isEmpty()
                if (r12 != 0) goto L_0x00c6
            L_0x00aa:
                int r12 = r10.size()
                if (r1 >= r12) goto L_0x00c6
                java.lang.Object r12 = r10.get(r1)
                android.view.View r12 = (android.view.View) r12
                android.view.ViewGroup$LayoutParams r2 = r12.getLayoutParams()
                androidx.coordinatorlayout.widget.CoordinatorLayout$f r2 = (androidx.coordinatorlayout.widget.CoordinatorLayout.f) r2
                androidx.coordinatorlayout.widget.CoordinatorLayout$c r2 = r2.a
                if (r2 == 0) goto L_0x00c3
                r2.b(r8, r12, r9)
            L_0x00c3:
                int r1 = r1 + 1
                goto L_0x00aa
            L_0x00c6:
                int r10 = r7.b()
                r9.b = r10
                boolean r10 = r9.willNotDraw()
                if (r10 != 0) goto L_0x00d5
                i.h.l.ViewCompat.A(r9)
            L_0x00d5:
                if (r3 >= r0) goto L_0x00da
                r10 = -1
                r4 = -1
                goto L_0x00dc
            L_0x00da:
                r10 = 1
                r4 = 1
            L_0x00dc:
                r5 = 0
                r0 = r7
                r1 = r8
                r2 = r9
                r0.a(r1, r2, r3, r4, r5)
                r1 = r11
                goto L_0x00e7
            L_0x00e5:
                r7.f395k = r1
            L_0x00e7:
                r7.b(r8, r9)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.BaseBehavior.b(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int, int, int):int");
        }

        public int c() {
            return b() + this.f395k;
        }

        public BaseBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public static class a extends AbsSavedState {
            public static final Parcelable.Creator<a> CREATOR = new C0003a();
            public int d;

            /* renamed from: e  reason: collision with root package name */
            public float f402e;

            /* renamed from: f  reason: collision with root package name */
            public boolean f403f;

            /* renamed from: com.google.android.material.appbar.AppBarLayout$BaseBehavior$a$a  reason: collision with other inner class name */
            public static class C0003a implements Parcelable.ClassLoaderCreator<a> {
                public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return new a(parcel, classLoader);
                }

                public Object[] newArray(int i2) {
                    return new a[i2];
                }

                public Object createFromParcel(Parcel parcel) {
                    return new a(parcel, null);
                }
            }

            public a(Parcel parcel, ClassLoader classLoader) {
                super(parcel, classLoader);
                this.d = parcel.readInt();
                this.f402e = parcel.readFloat();
                this.f403f = parcel.readByte() != 0;
            }

            public void writeToParcel(Parcel parcel, int i2) {
                parcel.writeParcelable(super.b, i2);
                parcel.writeInt(this.d);
                parcel.writeFloat(this.f402e);
                parcel.writeByte(this.f403f ? (byte) 1 : 0);
            }

            public a(Parcelable parcelable) {
                super(parcelable);
            }
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2, int i3, int i4, int i5) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (((CoordinatorLayout.f) appBarLayout.getLayoutParams()).height != -2) {
                return false;
            }
            coordinatorLayout.a(appBarLayout, i2, i3, View.MeasureSpec.makeMeasureSpec(0, 0), i5);
            return true;
        }

        public /* bridge */ /* synthetic */ void a(CoordinatorLayout coordinatorLayout, View view, View view2, int i2, int i3, int[] iArr, int i4) {
            a(coordinatorLayout, (AppBarLayout) view, view2, i3, iArr);
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, View view2, int i2, int i3, int i4, int i5, int i6, int[] iArr) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (i5 < 0) {
                iArr[1] = a(coordinatorLayout, appBarLayout, i5, -appBarLayout.getDownNestedScrollRange(), 0);
            }
            if (i5 == 0) {
                b(coordinatorLayout, appBarLayout);
            }
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, Parcelable parcelable) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (parcelable instanceof a) {
                a aVar = (a) parcelable;
                Parcelable parcelable2 = aVar.b;
                this.f398n = aVar.d;
                this.f400p = aVar.f402e;
                this.f399o = aVar.f403f;
                return;
            }
            this.f398n = -1;
        }

        public final void b(CoordinatorLayout coordinatorLayout, T t2) {
            ViewCompat.f(coordinatorLayout, AccessibilityNodeInfoCompat.a.f1203f.a());
            ViewCompat.f(coordinatorLayout, AccessibilityNodeInfoCompat.a.g.a());
            View a2 = a(coordinatorLayout);
            if (a2 != null && t2.getTotalScrollRange() != 0 && (((CoordinatorLayout.f) a2.getLayoutParams()).a instanceof ScrollingViewBehavior)) {
                if (c() != (-t2.getTotalScrollRange()) && a2.canScrollVertically(1)) {
                    ViewCompat.a(coordinatorLayout, AccessibilityNodeInfoCompat.a.f1203f, null, new AppBarLayout2(this, t2, false));
                }
                if (c() == 0) {
                    return;
                }
                if (a2.canScrollVertically(-1)) {
                    int i2 = -t2.getDownNestedPreScrollRange();
                    if (i2 != 0) {
                        ViewCompat.a(coordinatorLayout, AccessibilityNodeInfoCompat.a.g, null, new AppBarLayout1(this, coordinatorLayout, t2, a2, i2));
                        return;
                    }
                    return;
                }
                ViewCompat.a(coordinatorLayout, AccessibilityNodeInfoCompat.a.g, null, new AppBarLayout2(this, t2, true));
            }
        }

        public Parcelable a(CoordinatorLayout coordinatorLayout, View view) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            android.view.AbsSavedState absSavedState = View.BaseSavedState.EMPTY_STATE;
            int b = b();
            int childCount = appBarLayout.getChildCount();
            boolean z = false;
            int i2 = 0;
            while (i2 < childCount) {
                View childAt = appBarLayout.getChildAt(i2);
                int bottom = childAt.getBottom() + b;
                if (childAt.getTop() + b > 0 || bottom < 0) {
                    i2++;
                } else {
                    a aVar = new a(absSavedState);
                    aVar.d = i2;
                    if (bottom == appBarLayout.getTopInset() + ViewCompat.l(childAt)) {
                        z = true;
                    }
                    aVar.f403f = z;
                    aVar.f402e = ((float) bottom) / ((float) childAt.getHeight());
                    return aVar;
                }
            }
            return absSavedState;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
            if (((r3.getTotalScrollRange() != 0) && r2.getHeight() - r4.getHeight() <= r3.getHeight()) != false) goto L_0x002b;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(androidx.coordinatorlayout.widget.CoordinatorLayout r2, android.view.View r3, android.view.View r4, android.view.View r5, int r6, int r7) {
            /*
                r1 = this;
                com.google.android.material.appbar.AppBarLayout r3 = (com.google.android.material.appbar.AppBarLayout) r3
                r5 = r6 & 2
                r6 = 0
                r0 = 1
                if (r5 == 0) goto L_0x002c
                boolean r5 = r3.f389k
                if (r5 != 0) goto L_0x002b
                int r5 = r3.getTotalScrollRange()
                if (r5 == 0) goto L_0x0014
                r5 = 1
                goto L_0x0015
            L_0x0014:
                r5 = 0
            L_0x0015:
                if (r5 == 0) goto L_0x0028
                int r2 = r2.getHeight()
                int r4 = r4.getHeight()
                int r2 = r2 - r4
                int r3 = r3.getHeight()
                if (r2 > r3) goto L_0x0028
                r2 = 1
                goto L_0x0029
            L_0x0028:
                r2 = 0
            L_0x0029:
                if (r2 == 0) goto L_0x002c
            L_0x002b:
                r6 = 1
            L_0x002c:
                if (r6 == 0) goto L_0x0035
                android.animation.ValueAnimator r2 = r1.f397m
                if (r2 == 0) goto L_0x0035
                r2.cancel()
            L_0x0035:
                r2 = 0
                r1.f401q = r2
                r1.f396l = r7
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, android.view.View, int, int):boolean");
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, View view2, int i2) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (this.f396l == 0 || i2 == 1) {
                a(coordinatorLayout, appBarLayout);
                if (appBarLayout.f389k) {
                    appBarLayout.a(appBarLayout.a(view2));
                }
            }
            this.f401q = new WeakReference<>(view2);
        }

        public void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i2, int[] iArr) {
            int i3;
            int i4;
            if (i2 != 0) {
                if (i2 < 0) {
                    int i5 = -appBarLayout.getTotalScrollRange();
                    i4 = i5;
                    i3 = appBarLayout.getDownNestedPreScrollRange() + i5;
                } else {
                    i4 = -appBarLayout.getUpNestedPreScrollRange();
                    i3 = 0;
                }
                if (i4 != i3) {
                    iArr[1] = a(coordinatorLayout, appBarLayout, i2, i4, i3);
                }
            }
            if (appBarLayout.f389k) {
                appBarLayout.a(appBarLayout.a(view));
            }
        }

        public final void a(CoordinatorLayout coordinatorLayout, T t2, int i2, float f2) {
            int i3;
            int abs = Math.abs(c() - i2);
            float abs2 = Math.abs(f2);
            if (abs2 > 0.0f) {
                i3 = Math.round((((float) abs) / abs2) * 1000.0f) * 3;
            } else {
                i3 = (int) (((((float) abs) / ((float) t2.getHeight())) + 1.0f) * 150.0f);
            }
            int c = c();
            if (c == i2) {
                ValueAnimator valueAnimator = this.f397m;
                if (valueAnimator != null && valueAnimator.isRunning()) {
                    this.f397m.cancel();
                    return;
                }
                return;
            }
            ValueAnimator valueAnimator2 = this.f397m;
            if (valueAnimator2 == null) {
                ValueAnimator valueAnimator3 = new ValueAnimator();
                this.f397m = valueAnimator3;
                valueAnimator3.setInterpolator(AnimationUtils.f2294e);
                this.f397m.addUpdateListener(new AppBarLayout0(this, coordinatorLayout, t2));
            } else {
                valueAnimator2.cancel();
            }
            this.f397m.setDuration((long) Math.min(i3, 600));
            this.f397m.setIntValues(c, i2);
            this.f397m.start();
        }

        public final void a(CoordinatorLayout coordinatorLayout, T t2) {
            int c = c();
            int childCount = t2.getChildCount();
            int i2 = 0;
            while (true) {
                if (i2 >= childCount) {
                    i2 = -1;
                    break;
                }
                View childAt = t2.getChildAt(i2);
                int top = childAt.getTop();
                int bottom = childAt.getBottom();
                b bVar = (b) childAt.getLayoutParams();
                if (a(bVar.a, 32)) {
                    top -= bVar.topMargin;
                    bottom += bVar.bottomMargin;
                }
                int i3 = -c;
                if (top <= i3 && bottom >= i3) {
                    break;
                }
                i2++;
            }
            if (i2 >= 0) {
                View childAt2 = t2.getChildAt(i2);
                b bVar2 = (b) childAt2.getLayoutParams();
                int i4 = bVar2.a;
                if ((i4 & 17) == 17) {
                    int i5 = -childAt2.getTop();
                    int i6 = -childAt2.getBottom();
                    if (i2 == t2.getChildCount() - 1) {
                        i6 += t2.getTopInset();
                    }
                    if (a(i4, 2)) {
                        i6 += ViewCompat.l(childAt2);
                    } else if (a(i4, 5)) {
                        int l2 = ViewCompat.l(childAt2) + i6;
                        if (c < l2) {
                            i5 = l2;
                        } else {
                            i6 = l2;
                        }
                    }
                    if (a(i4, 32)) {
                        i5 += bVar2.topMargin;
                        i6 -= bVar2.bottomMargin;
                    }
                    if (c < (i6 + i5) / 2) {
                        i5 = i6;
                    }
                    a(coordinatorLayout, t2, ResourcesFlusher.a(i5, -t2.getTotalScrollRange(), 0), 0.0f);
                }
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:25:0x0061  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x006f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(androidx.coordinatorlayout.widget.CoordinatorLayout r7, T r8, int r9, int r10, boolean r11) {
            /*
                r6 = this;
                int r0 = java.lang.Math.abs(r9)
                int r1 = r8.getChildCount()
                r2 = 0
                r3 = 0
            L_0x000a:
                if (r3 >= r1) goto L_0x0020
                android.view.View r4 = r8.getChildAt(r3)
                int r5 = r4.getTop()
                if (r0 < r5) goto L_0x001d
                int r5 = r4.getBottom()
                if (r0 > r5) goto L_0x001d
                goto L_0x0021
            L_0x001d:
                int r3 = r3 + 1
                goto L_0x000a
            L_0x0020:
                r4 = 0
            L_0x0021:
                if (r4 == 0) goto L_0x009e
                android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
                com.google.android.material.appbar.AppBarLayout$b r0 = (com.google.android.material.appbar.AppBarLayout.b) r0
                int r0 = r0.a
                r1 = r0 & 1
                r3 = 1
                if (r1 == 0) goto L_0x0049
                int r1 = i.h.l.ViewCompat.l(r4)
                if (r10 <= 0) goto L_0x004b
                r10 = r0 & 12
                if (r10 == 0) goto L_0x004b
                int r9 = -r9
                int r10 = r4.getBottom()
                int r10 = r10 - r1
                int r0 = r8.getTopInset()
                int r10 = r10 - r0
                if (r9 < r10) goto L_0x0049
            L_0x0047:
                r9 = 1
                goto L_0x005d
            L_0x0049:
                r9 = 0
                goto L_0x005d
            L_0x004b:
                r10 = r0 & 2
                if (r10 == 0) goto L_0x0049
                int r9 = -r9
                int r10 = r4.getBottom()
                int r10 = r10 - r1
                int r0 = r8.getTopInset()
                int r10 = r10 - r0
                if (r9 < r10) goto L_0x0049
                goto L_0x0047
            L_0x005d:
                boolean r10 = r8.f389k
                if (r10 == 0) goto L_0x0069
                android.view.View r9 = r6.a(r7)
                boolean r9 = r8.a(r9)
            L_0x0069:
                boolean r9 = r8.a(r9)
                if (r11 != 0) goto L_0x009b
                if (r9 == 0) goto L_0x009e
                java.util.List r7 = r7.b(r8)
                int r9 = r7.size()
                r10 = 0
            L_0x007a:
                if (r10 >= r9) goto L_0x0099
                java.lang.Object r11 = r7.get(r10)
                android.view.View r11 = (android.view.View) r11
                android.view.ViewGroup$LayoutParams r11 = r11.getLayoutParams()
                androidx.coordinatorlayout.widget.CoordinatorLayout$f r11 = (androidx.coordinatorlayout.widget.CoordinatorLayout.f) r11
                androidx.coordinatorlayout.widget.CoordinatorLayout$c r11 = r11.a
                boolean r0 = r11 instanceof com.google.android.material.appbar.AppBarLayout.ScrollingViewBehavior
                if (r0 == 0) goto L_0x0096
                com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior r11 = (com.google.android.material.appbar.AppBarLayout.ScrollingViewBehavior) r11
                int r7 = r11.g
                if (r7 == 0) goto L_0x0099
                r2 = 1
                goto L_0x0099
            L_0x0096:
                int r10 = r10 + 1
                goto L_0x007a
            L_0x0099:
                if (r2 == 0) goto L_0x009e
            L_0x009b:
                r8.jumpDrawablesToCurrentState()
            L_0x009e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.BaseBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, com.google.android.material.appbar.AppBarLayout, int, int, boolean):void");
        }

        public final View a(CoordinatorLayout coordinatorLayout) {
            int childCount = coordinatorLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = coordinatorLayout.getChildAt(i2);
                if ((childAt instanceof NestedScrollingChild) || (childAt instanceof ListView) || (childAt instanceof ScrollView)) {
                    return childAt;
                }
            }
            return null;
        }
    }

    public AppBarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, j.c.a.b.b.appBarLayoutStyle);
    }

    /* renamed from: generateDefaultLayoutParams  reason: collision with other method in class */
    public LinearLayout.LayoutParams m2generateDefaultLayoutParams() {
        return new b(-1, -2);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppBarLayout(android.content.Context r14, android.util.AttributeSet r15, int r16) {
        /*
            r13 = this;
            r1 = r13
            r0 = r15
            r8 = r16
            int r2 = com.google.android.material.appbar.AppBarLayout.f384q
            r3 = r14
            android.content.Context r2 = j.c.a.b.m0.a.MaterialThemeOverlay.a(r14, r15, r8, r2)
            r13.<init>(r2, r15, r8)
            r9 = -1
            r1.c = r9
            r1.d = r9
            r1.f385e = r9
            r10 = 0
            r1.g = r10
            android.content.Context r11 = r13.getContext()
            r2 = 1
            r13.setOrientation(r2)
            android.view.ViewOutlineProvider r2 = android.view.ViewOutlineProvider.BOUNDS
            r13.setOutlineProvider(r2)
            int r6 = com.google.android.material.appbar.AppBarLayout.f384q
            android.content.Context r12 = r13.getContext()
            int[] r4 = j.c.a.b.n.ViewUtilsLollipop.a
            int[] r7 = new int[r10]
            r2 = r12
            r3 = r15
            r5 = r16
            android.content.res.TypedArray r2 = j.c.a.b.b0.ThemeEnforcement.b(r2, r3, r4, r5, r6, r7)
            boolean r3 = r2.hasValue(r10)     // Catch:{ all -> 0x00ff }
            if (r3 == 0) goto L_0x0048
            int r3 = r2.getResourceId(r10, r10)     // Catch:{ all -> 0x00ff }
            android.animation.StateListAnimator r3 = android.animation.AnimatorInflater.loadStateListAnimator(r12, r3)     // Catch:{ all -> 0x00ff }
            r13.setStateListAnimator(r3)     // Catch:{ all -> 0x00ff }
        L_0x0048:
            r2.recycle()
            int[] r4 = j.c.a.b.l.AppBarLayout
            int r6 = com.google.android.material.appbar.AppBarLayout.f384q
            int[] r7 = new int[r10]
            r2 = r11
            r3 = r15
            r5 = r16
            android.content.res.TypedArray r0 = j.c.a.b.b0.ThemeEnforcement.b(r2, r3, r4, r5, r6, r7)
            int r2 = j.c.a.b.l.AppBarLayout_android_background
            android.graphics.drawable.Drawable r2 = r0.getDrawable(r2)
            i.h.l.ViewCompat.a(r13, r2)
            android.graphics.drawable.Drawable r2 = r13.getBackground()
            boolean r2 = r2 instanceof android.graphics.drawable.ColorDrawable
            if (r2 == 0) goto L_0x008f
            android.graphics.drawable.Drawable r2 = r13.getBackground()
            android.graphics.drawable.ColorDrawable r2 = (android.graphics.drawable.ColorDrawable) r2
            j.c.a.b.g0.MaterialShapeDrawable r3 = new j.c.a.b.g0.MaterialShapeDrawable
            r3.<init>()
            int r2 = r2.getColor()
            android.content.res.ColorStateList r2 = android.content.res.ColorStateList.valueOf(r2)
            r3.a(r2)
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r3.b
            j.c.a.b.y.ElevationOverlayProvider r4 = new j.c.a.b.y.ElevationOverlayProvider
            r4.<init>(r11)
            r2.b = r4
            r3.j()
            r13.setBackground(r3)
        L_0x008f:
            int r2 = j.c.a.b.l.AppBarLayout_expanded
            boolean r2 = r0.hasValue(r2)
            if (r2 == 0) goto L_0x00a0
            int r2 = j.c.a.b.l.AppBarLayout_expanded
            boolean r2 = r0.getBoolean(r2, r10)
            r13.a(r2, r10, r10)
        L_0x00a0:
            int r2 = j.c.a.b.l.AppBarLayout_elevation
            boolean r2 = r0.hasValue(r2)
            if (r2 == 0) goto L_0x00b2
            int r2 = j.c.a.b.l.AppBarLayout_elevation
            int r2 = r0.getDimensionPixelSize(r2, r10)
            float r2 = (float) r2
            j.c.a.b.n.ViewUtilsLollipop.a(r13, r2)
        L_0x00b2:
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 26
            if (r2 < r3) goto L_0x00da
            int r2 = j.c.a.b.l.AppBarLayout_android_keyboardNavigationCluster
            boolean r2 = r0.hasValue(r2)
            if (r2 == 0) goto L_0x00c9
            int r2 = j.c.a.b.l.AppBarLayout_android_keyboardNavigationCluster
            boolean r2 = r0.getBoolean(r2, r10)
            r13.setKeyboardNavigationCluster(r2)
        L_0x00c9:
            int r2 = j.c.a.b.l.AppBarLayout_android_touchscreenBlocksFocus
            boolean r2 = r0.hasValue(r2)
            if (r2 == 0) goto L_0x00da
            int r2 = j.c.a.b.l.AppBarLayout_android_touchscreenBlocksFocus
            boolean r2 = r0.getBoolean(r2, r10)
            r13.setTouchscreenBlocksFocus(r2)
        L_0x00da:
            int r2 = j.c.a.b.l.AppBarLayout_liftOnScroll
            boolean r2 = r0.getBoolean(r2, r10)
            r1.f389k = r2
            int r2 = j.c.a.b.l.AppBarLayout_liftOnScrollTargetViewId
            int r2 = r0.getResourceId(r2, r9)
            r1.f390l = r2
            int r2 = j.c.a.b.l.AppBarLayout_statusBarForeground
            android.graphics.drawable.Drawable r2 = r0.getDrawable(r2)
            r13.setStatusBarForeground(r2)
            r0.recycle()
            com.google.android.material.appbar.AppBarLayout$a r0 = new com.google.android.material.appbar.AppBarLayout$a
            r0.<init>()
            i.h.l.ViewCompat.a(r13, r0)
            return
        L_0x00ff:
            r0 = move-exception
            r2.recycle()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.appbar.AppBarLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public boolean a(boolean z) {
        if (this.f388j == z) {
            return false;
        }
        this.f388j = z;
        refreshDrawableState();
        if (this.f389k && (getBackground() instanceof MaterialShapeDrawable)) {
            MaterialShapeDrawable materialShapeDrawable = (MaterialShapeDrawable) getBackground();
            float dimension = getResources().getDimension(d.design_appbar_elevation);
            float f2 = z ? 0.0f : dimension;
            if (!z) {
                dimension = 0.0f;
            }
            ValueAnimator valueAnimator = this.f392n;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(f2, dimension);
            this.f392n = ofFloat;
            ofFloat.setDuration((long) getResources().getInteger(g.app_bar_elevation_anim_duration));
            this.f392n.setInterpolator(AnimationUtils.a);
            this.f392n.addUpdateListener(new j.c.a.b.n.AppBarLayout(this, materialShapeDrawable));
            this.f392n.start();
        }
        return true;
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new b(getContext(), attributeSet);
    }

    /* renamed from: generateLayoutParams  reason: collision with other method in class */
    public LinearLayout.LayoutParams m3generateLayoutParams(AttributeSet attributeSet) {
        return new b(getContext(), attributeSet);
    }

    public b generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LinearLayout.LayoutParams) {
            return new b((LinearLayout.LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    public static class b extends LinearLayout.LayoutParams {
        public int a = 1;
        public Interpolator b;

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.AppBarLayout_Layout);
            this.a = obtainStyledAttributes.getInt(l.AppBarLayout_Layout_layout_scrollFlags, 0);
            if (obtainStyledAttributes.hasValue(l.AppBarLayout_Layout_layout_scrollInterpolator)) {
                this.b = android.view.animation.AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(l.AppBarLayout_Layout_layout_scrollInterpolator, 0));
            }
            obtainStyledAttributes.recycle();
        }

        public b(int i2, int i3) {
            super(i2, i3);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public b(LinearLayout.LayoutParams layoutParams) {
            super(super);
        }
    }

    public boolean a(View view) {
        int i2;
        View view2 = null;
        if (this.f391m == null && (i2 = this.f390l) != -1) {
            View findViewById = view != null ? view.findViewById(i2) : null;
            if (findViewById == null && (getParent() instanceof ViewGroup)) {
                findViewById = ((ViewGroup) getParent()).findViewById(this.f390l);
            }
            if (findViewById != null) {
                this.f391m = new WeakReference<>(findViewById);
            }
        }
        WeakReference<View> weakReference = this.f391m;
        if (weakReference != null) {
            view2 = weakReference.get();
        }
        if (view2 != null) {
            view = view2;
        }
        return view != null && (view.canScrollVertically(-1) || view.getScrollY() > 0);
    }

    public final boolean a() {
        if (getChildCount() <= 0) {
            return false;
        }
        View childAt = getChildAt(0);
        if (childAt.getVisibility() == 8 || ViewCompat.h(childAt)) {
            return false;
        }
        return true;
    }
}
