package com.google.android.material.transformation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import i.h.l.ViewCompat;
import j.c.a.b.z.ExpandableWidget;
import java.util.List;

public abstract class ExpandableBehavior extends CoordinatorLayout.c<View> {
    public int a = 0;

    public class a implements ViewTreeObserver.OnPreDrawListener {
        public final /* synthetic */ View b;
        public final /* synthetic */ int c;
        public final /* synthetic */ ExpandableWidget d;

        public a(View view, int i2, ExpandableWidget expandableWidget) {
            this.b = view;
            this.c = i2;
            this.d = expandableWidget;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.transformation.ExpandableBehavior.a(android.view.View, android.view.View, boolean, boolean):boolean
         arg types: [android.view.View, android.view.View, boolean, int]
         candidates:
          androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
          com.google.android.material.transformation.ExpandableBehavior.a(android.view.View, android.view.View, boolean, boolean):boolean */
        public boolean onPreDraw() {
            this.b.getViewTreeObserver().removeOnPreDrawListener(this);
            ExpandableBehavior expandableBehavior = ExpandableBehavior.this;
            if (expandableBehavior.a == this.c) {
                ExpandableWidget expandableWidget = this.d;
                expandableBehavior.a((View) expandableWidget, this.b, expandableWidget.a(), false);
            }
            return false;
        }
    }

    public ExpandableBehavior() {
    }

    public abstract boolean a(View view, View view2, boolean z, boolean z2);

    public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2) {
        ExpandableWidget expandableWidget;
        if (!ViewCompat.w(view)) {
            List<View> a2 = coordinatorLayout.a(view);
            int size = a2.size();
            int i3 = 0;
            while (true) {
                if (i3 >= size) {
                    expandableWidget = null;
                    break;
                }
                View view2 = a2.get(i3);
                if (a(coordinatorLayout, view, view2)) {
                    expandableWidget = (ExpandableWidget) view2;
                    break;
                }
                i3++;
            }
            if (expandableWidget != null && a(expandableWidget.a())) {
                int i4 = expandableWidget.a() ? 1 : 2;
                this.a = i4;
                view.getViewTreeObserver().addOnPreDrawListener(new a(view, i4, expandableWidget));
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.transformation.ExpandableBehavior.a(android.view.View, android.view.View, boolean, boolean):boolean
     arg types: [android.view.View, android.view.View, boolean, int]
     candidates:
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
      com.google.android.material.transformation.ExpandableBehavior.a(android.view.View, android.view.View, boolean, boolean):boolean */
    public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
        ExpandableWidget expandableWidget = (ExpandableWidget) view2;
        if (!a(expandableWidget.a())) {
            return false;
        }
        this.a = expandableWidget.a() ? 1 : 2;
        return a((View) expandableWidget, view, expandableWidget.a(), true);
    }

    public ExpandableBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final boolean a(boolean z) {
        if (z) {
            int i2 = this.a;
            return i2 == 0 || i2 == 2;
        } else if (this.a == 1) {
            return true;
        } else {
            return false;
        }
    }
}
