package com.crashlytics.android.core;

import android.os.AsyncTask;
import android.util.Log;
import j.a.a.a.outline;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;

public class CrashTest {
    private void privateMethodThatThrowsException(String str) {
        throw new RuntimeException(str);
    }

    public void crashAsyncTask(final long j2) {
        new AsyncTask<Void, Void, Void>() {
            /* class com.crashlytics.android.core.CrashTest.AnonymousClass1 */

            public Void doInBackground(Void... voidArr) {
                try {
                    Thread.sleep(j2);
                } catch (InterruptedException unused) {
                }
                CrashTest.this.throwRuntimeException("Background thread crash");
                return null;
            }
        }.execute(null);
    }

    public void indexOutOfBounds() {
        int i2 = new int[2][10];
        DefaultLogger a = Fabric.a();
        String b = outline.b("Out of bounds value: ", i2);
        if (a.a(CrashlyticsCore.TAG, 3)) {
            Log.d(CrashlyticsCore.TAG, b, null);
        }
    }

    public int stackOverflow() {
        return stackOverflow() + ((int) Math.random());
    }

    public void throwFiveChainedExceptions() {
        try {
            privateMethodThatThrowsException("1");
        } catch (Exception e2) {
            throw new RuntimeException("2", e2);
        } catch (Exception e3) {
            try {
                throw new RuntimeException("3", e3);
            } catch (Exception e4) {
                try {
                    throw new RuntimeException("4", e4);
                } catch (Exception e5) {
                    throw new RuntimeException("5", e5);
                }
            }
        }
    }

    public void throwRuntimeException(String str) {
        throw new RuntimeException(str);
    }
}
