package com.crashlytics.android.answers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import l.a.a.a.o.f.PreferenceStore;
import l.a.a.a.o.f.PreferenceStoreImpl;

public class AnswersPreferenceManager {
    public static final String PREFKEY_ANALYTICS_LAUNCHED = "analytics_launched";
    public static final String PREF_STORE_NAME = "settings";
    public final PreferenceStore prefStore;

    public AnswersPreferenceManager(PreferenceStore preferenceStore) {
        this.prefStore = preferenceStore;
    }

    public static AnswersPreferenceManager build(Context context) {
        return new AnswersPreferenceManager(new PreferenceStoreImpl(context, PREF_STORE_NAME));
    }

    @SuppressLint({"CommitPrefEdits"})
    public boolean hasAnalyticsLaunched() {
        return ((PreferenceStoreImpl) this.prefStore).a.getBoolean(PREFKEY_ANALYTICS_LAUNCHED, false);
    }

    @SuppressLint({"CommitPrefEdits"})
    public void setAnalyticsLaunched() {
        PreferenceStore preferenceStore = this.prefStore;
        SharedPreferences.Editor putBoolean = ((PreferenceStoreImpl) preferenceStore).a().putBoolean(PREFKEY_ANALYTICS_LAUNCHED, true);
        if (((PreferenceStoreImpl) preferenceStore) != null) {
            putBoolean.apply();
            return;
        }
        throw null;
    }
}
