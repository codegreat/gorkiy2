package f.a;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.crashlytics.android.answers.SessionEvent;
import java.lang.reflect.Proxy;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;

/* compiled from: ActivityDestroyWatcher.kt */
public final class ActivityDestroyWatcher implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ Application.ActivityLifecycleCallbacks b;
    public final /* synthetic */ ActivityDestroyWatcher0 c;

    public ActivityDestroyWatcher(ActivityDestroyWatcher0 activityDestroyWatcher0) {
        this.c = activityDestroyWatcher0;
        InternalAppWatcher internalAppWatcher = InternalAppWatcher.f732i;
        Class<Application.ActivityLifecycleCallbacks> cls = Application.ActivityLifecycleCallbacks.class;
        InternalAppWatcher0 internalAppWatcher0 = InternalAppWatcher0.a;
        Object newProxyInstance = Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, internalAppWatcher0);
        if (newProxyInstance != null) {
            this.b = (Application.ActivityLifecycleCallbacks) newProxyInstance;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.Application.ActivityLifecycleCallbacks");
    }

    public void onActivityCreated(@RecentlyNonNull Activity activity, @RecentlyNullable Bundle bundle) {
        this.b.onActivityCreated(activity, bundle);
    }

    public void onActivityDestroyed(Activity activity) {
        if (activity == null) {
            Intrinsics.a(SessionEvent.ACTIVITY_KEY);
            throw null;
        } else if (this.c.c.b().b) {
            this.c.b.a(activity);
        }
    }

    public void onActivityPaused(@RecentlyNonNull Activity activity) {
        this.b.onActivityPaused(activity);
    }

    public void onActivityResumed(@RecentlyNonNull Activity activity) {
        this.b.onActivityResumed(activity);
    }

    public void onActivitySaveInstanceState(@RecentlyNonNull Activity activity, @RecentlyNonNull Bundle bundle) {
        this.b.onActivitySaveInstanceState(activity, bundle);
    }

    public void onActivityStarted(@RecentlyNonNull Activity activity) {
        this.b.onActivityStarted(activity);
    }

    public void onActivityStopped(@RecentlyNonNull Activity activity) {
        this.b.onActivityStopped(activity);
    }
}
