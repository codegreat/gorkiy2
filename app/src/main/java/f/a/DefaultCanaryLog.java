package f.a;

import android.util.Log;
import j.c.a.a.c.n.c;
import java.util.ArrayList;
import java.util.regex.Matcher;
import n.n.c.Intrinsics;
import n.r.Regex;
import s.SharkLog;

/* compiled from: DefaultCanaryLog.kt */
public final class DefaultCanaryLog implements SharkLog.a {
    public static final Regex a = new Regex("\n");

    public void a(String str) {
        Iterable<String> iterable;
        if (str == null) {
            Intrinsics.a("message");
            throw null;
        } else if (str.length() < 4000) {
            Log.d("LeakCanary", str);
        } else {
            Regex regex = a;
            if (regex != null) {
                int i2 = 0;
                Matcher matcher = regex.b.matcher(str);
                if (matcher.find()) {
                    iterable = new ArrayList<>(10);
                    do {
                        iterable.add(str.subSequence(i2, matcher.start()).toString());
                        i2 = matcher.end();
                    } while (matcher.find());
                    iterable.add(str.subSequence(i2, str.length()).toString());
                } else {
                    iterable = c.c((Object) str.toString());
                }
                for (String d : iterable) {
                    Log.d("LeakCanary", d);
                }
                return;
            }
            throw null;
        }
    }
}
