package org.bouncycastle.jcajce.provider.symmetric.util;

import java.security.InvalidKeyException;

public class BaseWrapCipher$InvalidKeyOrParametersException extends InvalidKeyException {
    public Throwable getCause() {
        return null;
    }
}
