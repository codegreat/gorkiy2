package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: UserOrganization.kt */
public final class UserOrganization0 {
    public final List<UserOrganization> elements;
    public final int size;

    public UserOrganization0(int i2, List<UserOrganization> list) {
        if (list != null) {
            this.size = i2;
            this.elements = list;
            return;
        }
        Intrinsics.a("elements");
        throw null;
    }

    public static /* synthetic */ UserOrganization0 copy$default(UserOrganization0 userOrganization0, int i2, List list, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i2 = userOrganization0.size;
        }
        if ((i3 & 2) != 0) {
            list = userOrganization0.elements;
        }
        return userOrganization0.copy(i2, list);
    }

    public final int component1() {
        return this.size;
    }

    public final List<UserOrganization> component2() {
        return this.elements;
    }

    public final UserOrganizationsWithCount copy(int i2, List<UserOrganization> list) {
        if (list != null) {
            return new UserOrganization0(i2, list);
        }
        Intrinsics.a("elements");
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserOrganization0)) {
            return false;
        }
        UserOrganization0 userOrganization0 = (UserOrganization0) obj;
        return this.size == userOrganization0.size && Intrinsics.a(this.elements, userOrganization0.elements);
    }

    public final List<UserOrganization> getElements() {
        return this.elements;
    }

    public final int getSize() {
        return this.size;
    }

    public int hashCode() {
        int i2 = this.size * 31;
        List<UserOrganization> list = this.elements;
        return i2 + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        StringBuilder a = outline.a("UserOrganizationsWithCount(size=");
        a.append(this.size);
        a.append(", elements=");
        a.append(this.elements);
        a.append(")");
        return a.toString();
    }
}
