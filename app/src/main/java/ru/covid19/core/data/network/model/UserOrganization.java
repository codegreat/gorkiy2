package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: UserOrganization.kt */
public final class UserOrganization {
    public final boolean active;
    public final String branchName;
    public final String branchOid;
    public final boolean chief;
    public final String email;
    public final String fullName;
    public final String ogrn;
    public final String oid;
    public final String phone;
    public final String shortName;

    public UserOrganization(String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, boolean z2, String str8) {
        if (str == null) {
            Intrinsics.a("oid");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str5 == null) {
            Intrinsics.a("shortName");
            throw null;
        } else if (str6 == null) {
            Intrinsics.a("phone");
            throw null;
        } else if (str7 != null) {
            this.oid = str;
            this.fullName = str2;
            this.branchOid = str3;
            this.branchName = str4;
            this.shortName = str5;
            this.chief = z;
            this.phone = str6;
            this.email = str7;
            this.active = z2;
            this.ogrn = str8;
        } else {
            Intrinsics.a("email");
            throw null;
        }
    }

    public static /* synthetic */ UserOrganization copy$default(UserOrganization userOrganization, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, boolean z2, String str8, int i2, Object obj) {
        UserOrganization userOrganization2 = userOrganization;
        int i3 = i2;
        return userOrganization.copy((i3 & 1) != 0 ? userOrganization2.oid : str, (i3 & 2) != 0 ? userOrganization2.fullName : str2, (i3 & 4) != 0 ? userOrganization2.branchOid : str3, (i3 & 8) != 0 ? userOrganization2.branchName : str4, (i3 & 16) != 0 ? userOrganization2.shortName : str5, (i3 & 32) != 0 ? userOrganization2.chief : z, (i3 & 64) != 0 ? userOrganization2.phone : str6, (i3 & 128) != 0 ? userOrganization2.email : str7, (i3 & 256) != 0 ? userOrganization2.active : z2, (i3 & 512) != 0 ? userOrganization2.ogrn : str8);
    }

    public final String component1() {
        return this.oid;
    }

    public final String component10() {
        return this.ogrn;
    }

    public final String component2() {
        return this.fullName;
    }

    public final String component3() {
        return this.branchOid;
    }

    public final String component4() {
        return this.branchName;
    }

    public final String component5() {
        return this.shortName;
    }

    public final boolean component6() {
        return this.chief;
    }

    public final String component7() {
        return this.phone;
    }

    public final String component8() {
        return this.email;
    }

    public final boolean component9() {
        return this.active;
    }

    public final UserOrganization copy(String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, boolean z2, String str8) {
        if (str == null) {
            Intrinsics.a("oid");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str5 == null) {
            Intrinsics.a("shortName");
            throw null;
        } else if (str6 == null) {
            Intrinsics.a("phone");
            throw null;
        } else if (str7 != null) {
            return new UserOrganization(str, str2, str3, str4, str5, z, str6, str7, z2, str8);
        } else {
            Intrinsics.a("email");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserOrganization)) {
            return false;
        }
        UserOrganization userOrganization = (UserOrganization) obj;
        return Intrinsics.a(this.oid, userOrganization.oid) && Intrinsics.a(this.fullName, userOrganization.fullName) && Intrinsics.a(this.branchOid, userOrganization.branchOid) && Intrinsics.a(this.branchName, userOrganization.branchName) && Intrinsics.a(this.shortName, userOrganization.shortName) && this.chief == userOrganization.chief && Intrinsics.a(this.phone, userOrganization.phone) && Intrinsics.a(this.email, userOrganization.email) && this.active == userOrganization.active && Intrinsics.a(this.ogrn, userOrganization.ogrn);
    }

    public final boolean getActive() {
        return this.active;
    }

    public final String getBranchName() {
        return this.branchName;
    }

    public final String getBranchOid() {
        return this.branchOid;
    }

    public final boolean getChief() {
        return this.chief;
    }

    public final String getEmail() {
        return this.email;
    }

    public final String getFullName() {
        return this.fullName;
    }

    public final String getOgrn() {
        return this.ogrn;
    }

    public final String getOid() {
        return this.oid;
    }

    public final String getPhone() {
        return this.phone;
    }

    public final String getShortName() {
        return this.shortName;
    }

    public int hashCode() {
        String str = this.oid;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.fullName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.branchOid;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.branchName;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.shortName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        boolean z = this.chief;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i3 = (hashCode5 + (z ? 1 : 0)) * 31;
        String str6 = this.phone;
        int hashCode6 = (i3 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.email;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        boolean z3 = this.active;
        if (!z3) {
            z2 = z3;
        }
        int i4 = (hashCode7 + (z2 ? 1 : 0)) * 31;
        String str8 = this.ogrn;
        if (str8 != null) {
            i2 = str8.hashCode();
        }
        return i4 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("UserOrganization(oid=");
        a.append(this.oid);
        a.append(", fullName=");
        a.append(this.fullName);
        a.append(", branchOid=");
        a.append(this.branchOid);
        a.append(", branchName=");
        a.append(this.branchName);
        a.append(", shortName=");
        a.append(this.shortName);
        a.append(", chief=");
        a.append(this.chief);
        a.append(", phone=");
        a.append(this.phone);
        a.append(", email=");
        a.append(this.email);
        a.append(", active=");
        a.append(this.active);
        a.append(", ogrn=");
        return outline.a(a, this.ogrn, ")");
    }
}
