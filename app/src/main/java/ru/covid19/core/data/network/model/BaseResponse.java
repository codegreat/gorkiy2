package ru.covid19.core.data.network.model;

import ru.covid19.core.data.network.model.BaseResponse0;

/* compiled from: BaseResponse.kt */
public class BaseResponse implements BaseResponse0 {
    public ApiError error;

    public ApiError getError() {
        return this.error;
    }

    public boolean getHasError() {
        return BaseResponse0.DefaultImpls.getHasError(this);
    }

    public void setError(ApiError apiError) {
        this.error = apiError;
    }
}
