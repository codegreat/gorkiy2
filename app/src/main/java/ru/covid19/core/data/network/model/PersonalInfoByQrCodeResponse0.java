package ru.covid19.core.data.network.model;

/* compiled from: PersonalInfoByQrCodeResponse.kt */
public enum PersonalInfoByQrCodeResponse0 {
    RF_PASSPORT,
    INN,
    SNILS,
    AGE_CONFIRMATION
}
