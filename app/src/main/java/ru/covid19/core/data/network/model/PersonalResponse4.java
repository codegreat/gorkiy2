package ru.covid19.core.data.network.model;

import e.a.a.f;

/* compiled from: PersonalResponse.kt */
public enum PersonalResponse4 {
    M(f.profile_gender_m),
    F(f.profile_gender_f);
    
    public final int resId;

    /* access modifiers changed from: public */
    PersonalResponse4(int i2) {
        this.resId = i2;
    }

    public final int getResId() {
        return this.resId;
    }
}
