package ru.covid19.core.data.network.model;

/* compiled from: AddressType.kt */
public enum AddressType {
    NONE("NONE"),
    PLV("PLV"),
    PRG("PRG");
    
    public final String type;

    /* access modifiers changed from: public */
    AddressType(String str) {
        this.type = str;
    }

    public final String getType() {
        return this.type;
    }
}
