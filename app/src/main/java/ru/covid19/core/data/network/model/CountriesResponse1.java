package ru.covid19.core.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: CountriesResponse.kt */
public final class CountriesResponse1 implements BottomSheetSelectorNavigationDto {
    public static final Parcelable.Creator CREATOR = new Creator();
    public final CountriesResponse0 country;
    public final boolean isSelected;
    public final String label;

    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new CountriesResponse1((CountriesResponse0) CountriesResponse0.CREATOR.createFromParcel(parcel), parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new CountriesResponse1[i2];
        }
    }

    public CountriesResponse1(CountriesResponse0 countriesResponse0, String str, boolean z) {
        if (countriesResponse0 == null) {
            Intrinsics.a("country");
            throw null;
        } else if (str != null) {
            this.country = countriesResponse0;
            this.label = str;
            this.isSelected = z;
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public static /* synthetic */ CountriesResponse1 copy$default(CountriesResponse1 countriesResponse1, CountriesResponse0 countriesResponse0, String str, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            countriesResponse0 = countriesResponse1.country;
        }
        if ((i2 & 2) != 0) {
            str = countriesResponse1.getLabel();
        }
        if ((i2 & 4) != 0) {
            z = countriesResponse1.isSelected();
        }
        return countriesResponse1.copy(countriesResponse0, str, z);
    }

    public final CountriesResponse0 component1() {
        return this.country;
    }

    public final String component2() {
        return getLabel();
    }

    public final boolean component3() {
        return isSelected();
    }

    public final CountriesResponse1 copy(CountriesResponse0 countriesResponse0, String str, boolean z) {
        if (countriesResponse0 == null) {
            Intrinsics.a("country");
            throw null;
        } else if (str != null) {
            return new CountriesResponse1(countriesResponse0, str, z);
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CountriesResponse1)) {
            return false;
        }
        CountriesResponse1 countriesResponse1 = (CountriesResponse1) obj;
        return Intrinsics.a(this.country, countriesResponse1.country) && Intrinsics.a(getLabel(), countriesResponse1.getLabel()) && isSelected() == countriesResponse1.isSelected();
    }

    public final CountriesResponse0 getCountry() {
        return this.country;
    }

    public String getLabel() {
        return this.label;
    }

    public int hashCode() {
        CountriesResponse0 countriesResponse0 = this.country;
        int i2 = 0;
        int hashCode = (countriesResponse0 != null ? countriesResponse0.hashCode() : 0) * 31;
        String label2 = getLabel();
        if (label2 != null) {
            i2 = label2.hashCode();
        }
        int i3 = (hashCode + i2) * 31;
        boolean isSelected2 = isSelected();
        if (isSelected2) {
            isSelected2 = true;
        }
        return i3 + (isSelected2 ? 1 : 0);
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public String toString() {
        StringBuilder a = outline.a("CountrySelectorItem(country=");
        a.append(this.country);
        a.append(", label=");
        a.append(getLabel());
        a.append(", isSelected=");
        a.append(isSelected());
        a.append(")");
        return a.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            this.country.writeToParcel(parcel, 0);
            parcel.writeString(this.label);
            parcel.writeInt(this.isSelected ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
