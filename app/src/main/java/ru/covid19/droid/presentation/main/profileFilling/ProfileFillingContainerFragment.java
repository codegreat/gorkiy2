package ru.covid19.droid.presentation.main.profileFilling;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import com.google.android.material.snackbar.Snackbar;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.a.b.BaseEvents;
import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.e.NavigationWrapper0;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.h.BaseStepValidatedFragmentVm;
import e.a.b.h.b.h.ProfileFillingContainerFragment0;
import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState;
import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState0;
import e.a.b.h.b.h.ProfileFillingContainerFragmentVm;
import e.a.b.h.c.IStepsNestedFragmentCoordinator0;
import e.a.b.h.c.StepsNavigator;
import e.a.b.h.c.StepsNavigator0;
import e.b.a.CommandBuffer;
import e.c.c.BaseMviView1;
import e.c.c.BaseMviVm1;
import e.c.d.a.ViewAction;
import i.l.a.FragmentManager;
import j.e.a.b.AnyToUnit;
import j.e.b.BehaviorRelay;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import l.b.Observable;
import l.b.Single;
import l.b.SingleSource;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.t.Predicate;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableHide;
import l.b.u.e.d.SingleDoOnSuccess;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import ru.covid19.core.presentation.base.BaseDpFragment;

/* compiled from: ProfileFillingContainerFragment.kt */
public final class ProfileFillingContainerFragment extends BaseDpFragment<e.a.b.h.b.h.f, e.a.b.h.b.h.g> {
    public final NavigationWrapper0 d0 = new NavigationWrapper0();
    public StepsNavigator0 e0;
    public final CompositeDisposable f0 = new CompositeDisposable();
    public HashMap g0;

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ProfileFillingContainerFragmentViewState.d.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class b<T, R> implements Function<T, SingleSource<? extends R>> {
        public final /* synthetic */ ProfileFillingContainerFragment a;

        public b(ProfileFillingContainerFragment profileFillingContainerFragment) {
            this.a = profileFillingContainerFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Single<T>, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.b.h.c.StepsNavigator, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Single, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            BaseStepValidatedFragment baseStepValidatedFragment = null;
            if (((Unit) obj) != null) {
                StepsNavigator0 stepsNavigator0 = this.a.e0;
                if (stepsNavigator0 != null) {
                    Fragment a2 = stepsNavigator0.a();
                    if (a2 instanceof BaseStepValidatedFragment) {
                        baseStepValidatedFragment = a2;
                    }
                    BaseStepValidatedFragment baseStepValidatedFragment2 = baseStepValidatedFragment;
                    if (baseStepValidatedFragment2 != null) {
                        ViewAction<n.g> viewAction = ((BaseStepValidatedFragmentVm) baseStepValidatedFragment2.h()).f679j;
                        viewAction.a().a(Unit.a);
                        Single<T> b = ((BaseStepValidatedFragmentVm) baseStepValidatedFragment2.h()).f681l.a.b();
                        Intrinsics.a((Object) b, "vm.validationViewState.relay.firstOrError()");
                        StepsNavigator stepsNavigator = new StepsNavigator(stepsNavigator0);
                        ObjectHelper.a((Object) stepsNavigator, "onSuccess is null");
                        return new SingleDoOnSuccess(b, stepsNavigator);
                    }
                    Single a3 = Single.a((Object) true);
                    Intrinsics.a((Object) a3, "Single.just(true)");
                    return a3;
                }
                Intrinsics.b("stepsNavigator");
                throw null;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class c<T, R> implements Function<T, SingleSource<? extends R>> {
        public final /* synthetic */ ProfileFillingContainerFragment a;

        public c(ProfileFillingContainerFragment profileFillingContainerFragment) {
            this.a = profileFillingContainerFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Observable, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.s.Disposable, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Single, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            Observable observable;
            Boolean bool = (Boolean) obj;
            BaseStepValidatedFragment baseStepValidatedFragment = null;
            if (bool == null) {
                Intrinsics.a("isValid");
                throw null;
            } else if (bool.booleanValue()) {
                ProfileFillingContainerFragment profileFillingContainerFragment = this.a;
                profileFillingContainerFragment.f0.a();
                StepsNavigator0 stepsNavigator0 = profileFillingContainerFragment.e0;
                if (stepsNavigator0 != null) {
                    Fragment a2 = stepsNavigator0.a();
                    if (!(a2 instanceof BaseStepValidatedFragment)) {
                        a2 = null;
                    }
                    BaseStepValidatedFragment baseStepValidatedFragment2 = (BaseStepValidatedFragment) a2;
                    if (baseStepValidatedFragment2 != null) {
                        BehaviorRelay<T> behaviorRelay = ((BaseStepValidatedFragmentVm) baseStepValidatedFragment2.h()).f682m.a;
                        if (behaviorRelay != null) {
                            observable = new ObservableHide(behaviorRelay);
                            Intrinsics.a((Object) observable, "vm.processingViewState.relay.hide()");
                        } else {
                            throw null;
                        }
                    } else {
                        observable = Observable.d(false);
                        Intrinsics.a((Object) observable, "Observable.just(false)");
                    }
                    Disposable a3 = observable.a(new e.a.b.h.b.h.ProfileFillingContainerFragment(profileFillingContainerFragment), ProfileFillingContainerFragment0.b);
                    Intrinsics.a((Object) a3, "stepsNavigator.isLoading…\n            {}\n        )");
                    j.c.a.a.c.n.c.a(a3, profileFillingContainerFragment.f0);
                    StepsNavigator0 stepsNavigator02 = this.a.e0;
                    if (stepsNavigator02 != null) {
                        Fragment a4 = stepsNavigator02.a();
                        if (a4 instanceof BaseStepValidatedFragment) {
                            baseStepValidatedFragment = a4;
                        }
                        BaseStepValidatedFragment baseStepValidatedFragment3 = baseStepValidatedFragment;
                        if (baseStepValidatedFragment3 != null) {
                            ((BaseStepValidatedFragmentVm) baseStepValidatedFragment3.h()).f680k.a().a(Unit.a);
                            Single<Boolean> g = ((BaseStepValidatedFragmentVm) baseStepValidatedFragment3.h()).g();
                            if (g != null) {
                                return g;
                            }
                        }
                        Single a5 = Single.a((Object) true);
                        Intrinsics.a((Object) a5, "Single.just(true)");
                        return a5;
                    }
                    Intrinsics.b("stepsNavigator");
                    throw null;
                }
                Intrinsics.b("stepsNavigator");
                throw null;
            } else {
                Single a6 = Single.a((Object) false);
                Intrinsics.a((Object) a6, "Single.just(false)");
                return a6;
            }
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class d<T> implements Predicate<Boolean> {
        public static final d b = new d();

        public boolean a(Object obj) {
            Boolean bool = (Boolean) obj;
            if (bool != null) {
                return bool.booleanValue();
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            if (((Boolean) obj) != null) {
                return ProfileFillingContainerFragmentViewState.b.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class f<T, R> implements Function<T, R> {
        public static final f a = new f();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return BaseEvents.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class g extends j implements Functions0<e.a.b.h.c.c, n.g> {
        public final /* synthetic */ ProfileFillingContainerFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ProfileFillingContainerFragment profileFillingContainerFragment) {
            super(1);
            this.c = profileFillingContainerFragment;
        }

        public Object a(Object obj) {
            IStepsNestedFragmentCoordinator0 iStepsNestedFragmentCoordinator0 = (IStepsNestedFragmentCoordinator0) obj;
            if (iStepsNestedFragmentCoordinator0 != null) {
                this.c.c0.a((e.c.c.b) new ProfileFillingContainerFragmentViewState.e(iStepsNestedFragmentCoordinator0));
                ((ScrollView) this.c.c(e.a.b.b.frag_profile_filling_sv)).scrollTo(0, 0);
                return Unit.a;
            }
            Intrinsics.a("nextStepInfo");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragment.kt */
    public static final class h extends j implements Functions<n.g> {
        public final /* synthetic */ ProfileFillingContainerFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ProfileFillingContainerFragment profileFillingContainerFragment) {
            super(0);
            this.c = profileFillingContainerFragment;
        }

        public Object b() {
            Snackbar.a((ConstraintLayout) this.c.c(e.a.b.b.frag_profile_filling_root), R.string.frag_filling_profile_container_invalid_step_message, -1).f();
            return Unit.a;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void E() {
        this.F = true;
        this.Z.a();
        ((BaseMviVm1) h()).f722f.a();
        NestedFragmentCiceroneHolder nestedFragmentCiceroneHolder = this.d0.a;
        if (nestedFragmentCiceroneHolder != null) {
            String name = h().getClass().getName();
            Intrinsics.a((Object) name, "vm::class.java.name");
            nestedFragmentCiceroneHolder.c(name).a = null;
            return;
        }
        Intrinsics.b("nestedFragmentCiceroneHolder");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void F() {
        this.F = true;
        Collections.a((BaseMviView1) this);
        if (this.Y) {
            this.Y = false;
        }
        NestedFragmentCiceroneHolder nestedFragmentCiceroneHolder = this.d0.a;
        if (nestedFragmentCiceroneHolder != null) {
            String name = h().getClass().getName();
            Intrinsics.a((Object) name, "vm::class.java.name");
            CommandBuffer c2 = nestedFragmentCiceroneHolder.c(name);
            StepsNavigator0 stepsNavigator0 = this.e0;
            if (stepsNavigator0 != null) {
                c2.a(stepsNavigator0);
            } else {
                Intrinsics.b("stepsNavigator");
                throw null;
            }
        } else {
            Intrinsics.b("nestedFragmentCiceroneHolder");
            throw null;
        }
    }

    public void N() {
        HashMap hashMap = this.g0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e.a.b.h.b.h.g> P() {
        return ProfileFillingContainerFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        ProfileFillingContainerFragmentViewState0 profileFillingContainerFragmentViewState0 = (ProfileFillingContainerFragmentViewState0) obj;
        if (profileFillingContainerFragmentViewState0 != null) {
            if (profileFillingContainerFragmentViewState0.d) {
                ((Button) c(e.a.b.b.frag_profile_filling_next_step_btn)).setText((int) R.string.frag_filling_profile_container_accept_form_button);
            } else {
                ((Button) c(e.a.b.b.frag_profile_filling_next_step_btn)).setText((int) R.string.frag_filling_profile_container_next_button);
            }
            View c2 = c(e.a.b.b.frag_profile_filling_container_processing);
            Intrinsics.a((Object) c2, "frag_profile_filling_container_processing");
            Collections.a(c2, profileFillingContainerFragmentViewState0.f684f);
            return;
        }
        Intrinsics.a("vs");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [i.l.a.FragmentManager, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.util.SparseArray<androidx.fragment.app.Fragment$d>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void b(Bundle bundle) {
        SparseArray<Fragment.d> sparseParcelableArray;
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.d0);
            mainComponent.a(this.a0);
            super.b(bundle);
            FragmentManager o2 = o();
            Intrinsics.a((Object) o2, "childFragmentManager");
            this.e0 = new StepsNavigator0(R.id.frag_profile_filling_steps_container, o2, new g(this), new h(this));
            if (bundle != null && (sparseParcelableArray = bundle.getSparseParcelableArray("PAGES_SAVED_STATE_KEY")) != null) {
                StepsNavigator0 stepsNavigator0 = this.e0;
                if (stepsNavigator0 != null) {
                    Intrinsics.a((Object) sparseParcelableArray, "it");
                    stepsNavigator0.a = sparseParcelableArray;
                    return;
                }
                Intrinsics.b("stepsNavigator");
                throw null;
            }
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.g0 == null) {
            this.g0 = new HashMap();
        }
        View view = (View) this.g0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.g0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public void d(Bundle bundle) {
        if (bundle != null) {
            ((BaseMviVm1) h()).b(bundle);
            StepsNavigator0 stepsNavigator0 = this.e0;
            if (stepsNavigator0 != null) {
                bundle.putSparseParcelableArray("PAGES_SAVED_STATE_KEY", stepsNavigator0.a);
            } else {
                Intrinsics.b("stepsNavigator");
                throw null;
            }
        } else {
            Intrinsics.a("outState");
            throw null;
        }
    }

    public boolean e() {
        this.c0.a((e.c.c.b) ProfileFillingContainerFragmentViewState.d.a);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageButton, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g2 = super.g();
        ConstraintLayout constraintLayout = (ConstraintLayout) c(e.a.b.b.frag_profile_filling_cl_back);
        Intrinsics.a((Object) constraintLayout, "frag_profile_filling_cl_back");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) constraintLayout).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        Button button = (Button) c(e.a.b.b.frag_profile_filling_next_step_btn);
        Intrinsics.a((Object) button, "frag_profile_filling_next_step_btn");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) button).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        ImageButton imageButton = (ImageButton) c(e.a.b.b.frag_profile_filling_btn_exit);
        Intrinsics.a((Object) imageButton, "frag_profile_filling_btn_exit");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) imageButton).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) g2, (Iterable) Collections.a((Object[]) new Observable[]{c2.a(400, TimeUnit.MILLISECONDS).c((Function) a.a), c3.a(400, TimeUnit.MILLISECONDS).b(new b(this)).b(new c(this)).a(d.b).c((Function) e.a), c4.c((Function) f.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_profile_filling_container, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
