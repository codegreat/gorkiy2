package ru.covid19.droid.data.model.profileData;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: Travel.kt */
public final class Travel {
    public int arrivalDate;
    public String arrivalPlace;
    public String city;
    public String country;
    public String countryName;

    public Travel() {
        this(0, null, null, null, null, 31, null);
    }

    public Travel(int i2, String str, String str2, String str3, String str4) {
        if (str2 == null) {
            Intrinsics.a("arrivalPlace");
            throw null;
        } else if (str3 != null) {
            this.arrivalDate = i2;
            this.countryName = str;
            this.arrivalPlace = str2;
            this.country = str3;
            this.city = str4;
        } else {
            Intrinsics.a("country");
            throw null;
        }
    }

    public static /* synthetic */ Travel copy$default(Travel travel, int i2, String str, String str2, String str3, String str4, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i2 = travel.arrivalDate;
        }
        if ((i3 & 2) != 0) {
            str = travel.countryName;
        }
        String str5 = str;
        if ((i3 & 4) != 0) {
            str2 = travel.arrivalPlace;
        }
        String str6 = str2;
        if ((i3 & 8) != 0) {
            str3 = travel.country;
        }
        String str7 = str3;
        if ((i3 & 16) != 0) {
            str4 = travel.city;
        }
        return travel.copy(i2, str5, str6, str7, str4);
    }

    public final int component1() {
        return this.arrivalDate;
    }

    public final String component2() {
        return this.countryName;
    }

    public final String component3() {
        return this.arrivalPlace;
    }

    public final String component4() {
        return this.country;
    }

    public final String component5() {
        return this.city;
    }

    public final Travel copy(int i2, String str, String str2, String str3, String str4) {
        if (str2 == null) {
            Intrinsics.a("arrivalPlace");
            throw null;
        } else if (str3 != null) {
            return new Travel(i2, str, str2, str3, str4);
        } else {
            Intrinsics.a("country");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Travel)) {
            return false;
        }
        Travel travel = (Travel) obj;
        return this.arrivalDate == travel.arrivalDate && Intrinsics.a(this.countryName, travel.countryName) && Intrinsics.a(this.arrivalPlace, travel.arrivalPlace) && Intrinsics.a(this.country, travel.country) && Intrinsics.a(this.city, travel.city);
    }

    public final int getArrivalDate() {
        return this.arrivalDate;
    }

    public final String getArrivalPlace() {
        return this.arrivalPlace;
    }

    public final String getCity() {
        return this.city;
    }

    public final String getCountry() {
        return this.country;
    }

    public final String getCountryName() {
        return this.countryName;
    }

    public int hashCode() {
        int i2 = this.arrivalDate * 31;
        String str = this.countryName;
        int i3 = 0;
        int hashCode = (i2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.arrivalPlace;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.country;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.city;
        if (str4 != null) {
            i3 = str4.hashCode();
        }
        return hashCode3 + i3;
    }

    public final void setArrivalDate(int i2) {
        this.arrivalDate = i2;
    }

    public final void setArrivalPlace(String str) {
        if (str != null) {
            this.arrivalPlace = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setCity(String str) {
        this.city = str;
    }

    public final void setCountry(String str) {
        if (str != null) {
            this.country = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setCountryName(String str) {
        this.countryName = str;
    }

    public String toString() {
        StringBuilder a = outline.a("Travel(arrivalDate=");
        a.append(this.arrivalDate);
        a.append(", countryName=");
        a.append(this.countryName);
        a.append(", arrivalPlace=");
        a.append(this.arrivalPlace);
        a.append(", country=");
        a.append(this.country);
        a.append(", city=");
        return outline.a(a, this.city, ")");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Date, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Travel(int r4, java.lang.String r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, int r9, n.n.c.DefaultConstructorMarker r10) {
        /*
            r3 = this;
            r10 = r9 & 1
            if (r10 == 0) goto L_0x0022
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            java.lang.String r10 = "Calendar.getInstance()"
            n.n.c.Intrinsics.a(r4, r10)
            java.util.Date r4 = r4.getTime()
            java.lang.String r10 = "Calendar.getInstance().time"
            n.n.c.Intrinsics.a(r4, r10)
            r10 = 6
            r0 = 0
            java.lang.String r1 = "yyyyMMdd"
            java.lang.String r4 = n.i.Collections.a(r4, r1, r0, r0, r10)
            int r4 = java.lang.Integer.parseInt(r4)
        L_0x0022:
            r10 = r9 & 2
            java.lang.String r0 = ""
            if (r10 == 0) goto L_0x002a
            r10 = r0
            goto L_0x002b
        L_0x002a:
            r10 = r5
        L_0x002b:
            r5 = r9 & 4
            if (r5 == 0) goto L_0x0031
            r1 = r0
            goto L_0x0032
        L_0x0031:
            r1 = r6
        L_0x0032:
            r5 = r9 & 8
            if (r5 == 0) goto L_0x0038
            r2 = r0
            goto L_0x0039
        L_0x0038:
            r2 = r7
        L_0x0039:
            r5 = r9 & 16
            if (r5 == 0) goto L_0x003e
            goto L_0x003f
        L_0x003e:
            r0 = r8
        L_0x003f:
            r5 = r3
            r6 = r4
            r7 = r10
            r8 = r1
            r9 = r2
            r10 = r0
            r5.<init>(r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.data.model.profileData.Travel.<init>(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, n.n.c.DefaultConstructorMarker):void");
    }
}
