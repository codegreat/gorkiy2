package ru.covid19.droid.data.storage.db;

import e.a.a.g.b.a.b.CacheDao;
import e.a.a.g.b.a.b.CacheDao_Impl;

public final class DpDatabase_Impl extends CovidDatabase {

    /* renamed from: j  reason: collision with root package name */
    public volatile CacheDao f3208j;

    public CacheDao a() {
        CacheDao cacheDao;
        if (this.f3208j != null) {
            return this.f3208j;
        }
        synchronized (this) {
            if (this.f3208j == null) {
                this.f3208j = new CacheDao_Impl(this);
            }
            cacheDao = this.f3208j;
        }
        return cacheDao;
    }
}
